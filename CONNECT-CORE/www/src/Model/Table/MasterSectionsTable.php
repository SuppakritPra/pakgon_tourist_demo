<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterSections Model
 *
 * @property \App\Model\Table\MasterDepartmentsTable|\Cake\ORM\Association\BelongsTo $MasterDepartments
 * @property \App\Model\Table\ArticleAccessesTable|\Cake\ORM\Association\HasMany $ArticleAccesses
 *
 * @method \App\Model\Entity\MasterSection get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterSection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterSection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterSection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterSection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterSection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterSection findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterSectionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('master.master_sections');
        $this->setDisplayField('section_name_th');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterDepartments', [
            'foreignKey' => 'master_department_id'
        ]);
        $this->hasMany('ArticleAccesses', [
            'foreignKey' => 'master_section_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('section_name_th')
                ->allowEmpty('section_name_th');

        $validator
                ->scalar('section_name_en')
                ->allowEmpty('section_name_en');

        $validator
                ->scalar('section_abbr_th')
                ->allowEmpty('section_abbr_th');

        $validator
                ->scalar('section_abbr_en')
                ->allowEmpty('section_abbr_en');

        $validator
                ->boolean('is_used')
                ->requirePresence('is_used', 'create')
                ->notEmpty('is_used');

        $validator
                ->requirePresence('created_by', 'create')
                ->notEmpty('created_by');

        $validator
                ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['master_department_id'], 'MasterDepartments'));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     * @author  pakgon.Ltd
     * @return string
     */
    public static function defaultConnectionName() {
        return 'db_master';
    }

}
