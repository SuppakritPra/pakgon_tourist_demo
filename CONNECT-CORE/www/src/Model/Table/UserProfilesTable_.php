<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserProfiles Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Users
 * @property |\Cake\ORM\Association\BelongsTo $Organizes
 * @property |\Cake\ORM\Association\BelongsTo $Depts
 * @property |\Cake\ORM\Association\BelongsTo $Sections
 * @property |\Cake\ORM\Association\BelongsTo $UserTypes
 * @property |\Cake\ORM\Association\BelongsTo $MasterBusinessTypes
 *
 * @method \App\Model\Entity\UserProfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserProfile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserProfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserProfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserProfilesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('core.user_profiles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('UserPersonals', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('MasterOrganizations', [
            'foreignKey' => 'organize_id',
        ]);
        $this->belongsTo('MasterDepartments', [
            'foreignKey' => 'dept_id',
        ]);
        $this->belongsTo('MasterSections', [
            'foreignKey' => 'section_id',
        ]);
//        $this->belongsTo('MasterOrganizationPositions', [
//            'foreignKey' => 'organize_id',
//            'joinType' => 'LEFT'
//        ]);
        $this->belongsTo('UserTypes', [
            'foreignKey' => 'user_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MasterBusinessTypes', [
            'foreignKey' => 'master_bussiness_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('card_code')
                ->allowEmpty('card_code');

        $validator
                ->scalar('img_path')
                ->allowEmpty('img_path');

        $validator
                ->boolean('is_used')
                ->requirePresence('is_used', 'create')
                ->notEmpty('is_used');

        $validator
                ->allowEmpty('created_by');

        $validator
                ->allowEmpty('modified_by');

        $validator
                ->allowEmpty('position_org');

        $validator
                ->allowEmpty('position_edu');

        $validator
                ->date('enter_date')
                ->allowEmpty('enter_date');

        $validator
                ->scalar('address')
                ->allowEmpty('address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['organize_id'], 'MasterOrganizations'));
        $rules->add($rules->existsIn(['dept_id'], 'MasterDepartments'));
        $rules->add($rules->existsIn(['section_id'], 'MasterSections'));
        $rules->add($rules->existsIn(['user_type_id'], 'UserTypes'));
        $rules->add($rules->existsIn(['master_business_type_id'], 'MasterBusinessTypes'));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     * @author  pakgon.Ltd
     * @return string
     */
    public static function defaultConnectionName() {
        return 'core';
    }

}
