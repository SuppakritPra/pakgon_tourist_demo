<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\ArticleLikesTable|\Cake\ORM\Association\HasMany $ArticleLikes
 * @property \App\Model\Table\ArticleTakeQuestionsTable|\Cake\ORM\Association\HasMany $ArticleTakeQuestions
 * @property \App\Model\Table\UserDefaultLanguagesTable|\Cake\ORM\Association\HasMany $UserDefaultLanguages
 * @property \App\Model\Table\UserHomeplacesTable|\Cake\ORM\Association\HasMany $UserHomeplaces
 * @property \App\Model\Table\UserPersonalsTable|\Cake\ORM\Association\HasMany $UserPersonals
 * @property \App\Model\Table\UserProfilesTable|\Cake\ORM\Association\HasMany $UserProfiles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('core.users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('ArticleLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ArticleTakeQuestions', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserDefaultLanguages', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserHomeplaces', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserPersonals', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserProfiles', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //         ->allowEmpty('id', 'create');

        // $validator
        //         ->scalar('username')
        //         ->allowEmpty('username');

        // $validator
        //         ->scalar('password')
        //         ->allowEmpty('password');

        // $validator
        //         ->integer('point')
        //         ->allowEmpty('point');

        // $validator
        //         ->boolean('is_used')
        //         ->allowEmpty('is_used');

        // $validator
        //         ->scalar('dynamic_key')
        //         ->allowEmpty('dynamic_key');

        // $validator
        //         ->date('dynamic_key_expiry')
        //         ->allowEmpty('dynamic_key_expiry');

        // $validator
        //         ->scalar('token')
        //         ->allowEmpty('token');

        // $validator
        //         ->date('token_expiry')
        //         ->allowEmpty('token_expiry');

        // $validator
        //         ->requirePresence('created_by', 'create')
        //         ->notEmpty('created_by');

        // $validator
        //         ->allowEmpty('modified_by');

        // $validator
        //         ->scalar('pin_code')
        //         ->allowEmpty('pin_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }
    

    /**
     *
     * Returns the database connection name to use by default.
     * @author  pakgon.Ltd
     * @return string
     */
    public static function defaultConnectionName() {
        return 'core';
    }

}
