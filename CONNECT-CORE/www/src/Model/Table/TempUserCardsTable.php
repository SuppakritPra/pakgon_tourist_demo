<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TempUserCardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('temp.user_cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // $validator
        //     ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('building_code')
        //     ->maxLength('building_code', 20)
        //     ->requirePresence('building_code', 'create')
        //     ->notEmpty('building_code');

        // $validator
        //     ->scalar('building_name')
        //     ->maxLength('building_name', 100)
        //     ->allowEmpty('building_name');

        // $validator
        //     ->boolean('is_used')
        //     ->requirePresence('is_used', 'create')
        //     ->notEmpty('is_used');

        // $validator
        //     ->requirePresence('create_uid', 'create')
        //     ->notEmpty('create_uid');

        // $validator
        //     ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_temp';
    }
}
