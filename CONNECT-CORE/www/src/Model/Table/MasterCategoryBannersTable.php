<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterCategoryBanners Model
 *
 * @property |\Cake\ORM\Association\HasMany $MasterProvinces
 * @property |\Cake\ORM\Association\HasMany $UserHomeplaces
 *
 * @method \App\Model\Entity\MasterCategorie get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterCategorie newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterCategorie[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterCategorie|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterCategorie patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterCategorie[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterCategorie findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterCategoryBannersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_category_banners');
        $this->setDisplayField('id');
        $this->setPrimaryKey('master_category_id');

        $this->addBehavior('Timestamp');
        $this->belongsTo('MasterCategories', [
            'foreignKey' => 'id',
            'joinType' => 'INNER'
        ]);
//        $this->hasMany('MasterApps', [
//            'foreignKey' => 'master_category_id'
//        ]);
//        $this->hasMany('AppAccess', [
//            'foreignKey' => 'master_app_id'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('master_category_id')
            ->allowEmpty('master_category_id');

        $validator
            ->scalar('banner_path')
            ->allowEmpty('banner_path');

        $validator
            ->scalar('seq_no')
            ->allowEmpty('seq_no');
        
        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
