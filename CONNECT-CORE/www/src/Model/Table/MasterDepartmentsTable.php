<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterDepartments Model
 *
 * @property \App\Model\Table\MasterOrganizationsTable|\Cake\ORM\Association\BelongsTo $MasterOrganizations
 * @property \App\Model\Table\ArticleAccessesTable|\Cake\ORM\Association\HasMany $ArticleAccesses
 * @property \App\Model\Table\MasterOrganizationPositionsTable|\Cake\ORM\Association\HasMany $MasterOrganizationPositions
 * @property \App\Model\Table\MasterSectionsTable|\Cake\ORM\Association\HasMany $MasterSections
 *
 * @method \App\Model\Entity\MasterDepartment get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterDepartment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterDepartment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterDepartment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterDepartment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterDepartment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterDepartment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterDepartmentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_departments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterOrganizations', [
            'foreignKey' => 'master_organization_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ArticleAccesses', [
            'foreignKey' => 'master_department_id'
        ]);
        $this->hasMany('MasterOrganizationPositions', [
            'foreignKey' => 'master_department_id'
        ]);
        $this->hasMany('MasterSections', [
            'foreignKey' => 'master_department_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('dept_name_th')
            ->allowEmpty('dept_name_th');

        $validator
            ->scalar('dept_name_en')
            ->allowEmpty('dept_name_en');

        $validator
            ->scalar('dept_abbr_th')
            ->allowEmpty('dept_abbr_th');

        $validator
            ->scalar('dept_abbr_en')
            ->allowEmpty('dept_abbr_en');

        $validator
            ->integer('order_seq')
            ->allowEmpty('order_seq');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->integer('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['master_organization_id'], 'MasterOrganizations'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
