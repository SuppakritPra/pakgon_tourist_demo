<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterUserTypes Model
 *
 * @property \App\Model\Table\MasterPrefixesTable|\Cake\ORM\Association\HasMany $MasterPrefixes
 *
 * @method \App\Model\Entity\MasterUserType get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterUserType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterUserType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterUserType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterUserType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterUserType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterUserType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterUserTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_user_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('MasterPrefixes', [
            'foreignKey' => 'master_user_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // $validator
        //     ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('user_type_name_th')
        //     ->maxLength('user_type_name_th', 50)
        //     ->allowEmpty('user_type_name_th');

        // $validator
        //     ->scalar('user_type_name_en')
        //     ->maxLength('user_type_name_en', 50)
        //     ->allowEmpty('user_type_name_en');

        // $validator
        //     ->boolean('is_used')
        //     ->requirePresence('is_used', 'create')
        //     ->notEmpty('is_used');

        // $validator
        //     ->integer('created_by')
        //     ->requirePresence('created_by', 'create')
        //     ->notEmpty('created_by');

        // $validator
        //     ->integer('modified_by')
        //     ->allowEmpty('modified_by');

        return $validator;
    }

    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
