<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserPersonLists Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Users
 * @property |\Cake\ORM\Association\BelongsTo $Organizes
 * @property |\Cake\ORM\Association\BelongsTo $Depts
 * @property |\Cake\ORM\Association\BelongsTo $Sections
 * @property |\Cake\ORM\Association\BelongsTo $UserTypes
 * @property |\Cake\ORM\Association\BelongsTo $MasterBusinessTypes
 *
 * @method \App\Model\Entity\UserProfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserProfile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserProfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserProfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserProfile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserPersonListsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_person_lists');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        //$this->belongsTo('MasterOrganizations', [
        //    'foreignKey' => 'master_organization_id',
        //    'joinType' => 'INNER'
        //]);
        $this->belongsTo('MasterDepartments', [
            'foreignKey' => 'master_department_id'
        ]);
        $this->belongsTo('MasterSections', [
            'foreignKey' => 'master_section_id'
        ]);
        //$this->belongsTo('MasterUserTypes', [
        //    'foreignKey' => 'master_user_type_id'
        //]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('card_code')
            ->maxLength('card_code', 20)
            ->allowEmpty('card_code');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 50)
            ->allowEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 50)
            ->allowEmpty('last_name');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('create_uid', 'create')
            ->notEmpty('create_uid');

        $validator
            ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['master_organization_id'], 'MasterOrganizations'));
        $rules->add($rules->existsIn(['master_department_id'], 'MasterDepartments'));
        $rules->add($rules->existsIn(['master_section_id'], 'MasterSections'));
        //$rules->add($rules->existsIn(['master_user_type_id'], 'MasterUserTypes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     * @author  pakgon.Ltd
     * @return string
     */
//    public static function defaultConnectionName()
//    {
//        return 'core';
//    }
}
