<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserNames Model
 *
 * @property \App\Model\Table\FacultiesTable|\Cake\ORM\Association\BelongsTo $Faculties
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\NamePrefixesTable|\Cake\ORM\Association\BelongsTo $NamePrefixes
 *
 * @method \App\Model\Entity\UserName get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserName newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserName[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserName|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserName patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserName[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserName findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserNamesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_names');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Faculties', [
            'foreignKey' => 'faculty_id'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id'
        ]);
        $this->belongsTo('NamePrefixes', [
            'foreignKey' => 'name_prefix_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('ref_code')
            ->allowEmpty('ref_code');

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('first_name')
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('office_phone')
            ->allowEmpty('office_phone');

        $validator
            ->scalar('mobile_phone')
            ->allowEmpty('mobile_phone');

        $validator
            ->date('birth_date')
            ->allowEmpty('birth_date');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        $validator
            ->scalar('moo')
            ->allowEmpty('moo');

        $validator
            ->scalar('road')
            ->allowEmpty('road');

        $validator
            ->scalar('sub_district')
            ->allowEmpty('sub_district');

        $validator
            ->scalar('district')
            ->allowEmpty('district');

        $validator
            ->scalar('province')
            ->allowEmpty('province');

        $validator
            ->scalar('zipcode')
            ->allowEmpty('zipcode');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('picture_path')
            ->allowEmpty('picture_path');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['faculty_id'], 'Faculties'));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->existsIn(['name_prefix_id'], 'NamePrefixes'));

        return $rules;
    }
}
