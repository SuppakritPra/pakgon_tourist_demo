<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserCards Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\OrganizesTable|\Cake\ORM\Association\BelongsTo $Organizes
 *
 * @method \App\Model\Entity\UserCard get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserCard newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserCard[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserCard|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserCard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserCard[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserCard findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserCardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Organizes', [
            'foreignKey' => 'organize_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // $validator
        //     ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('card_code')
        //     ->allowEmpty('card_code');

        // $validator
        //     ->scalar('img_path')
        //     ->allowEmpty('img_path');

        // $validator
        //     ->scalar('prefix_name_th')
        //     ->allowEmpty('prefix_name_th');

        // $validator
        //     ->scalar('firstname_th')
        //     ->allowEmpty('firstname_th');

        // $validator
        //     ->scalar('lastname_th')
        //     ->allowEmpty('lastname_th');

        // $validator
        //     ->scalar('prefix_name_en')
        //     ->allowEmpty('prefix_name_en');

        // $validator
        //     ->scalar('firstname_en')
        //     ->allowEmpty('firstname_en');

        // $validator
        //     ->scalar('lastname_en')
        //     ->allowEmpty('lastname_en');

        // $validator
        //     ->scalar('department_name')
        //     ->allowEmpty('department_name');

        // $validator
        //     ->scalar('section_name')
        //     ->allowEmpty('section_name');

        // $validator
        //     ->scalar('position_name')
        //     ->allowEmpty('position_name');

        // $validator
        //     ->scalar('gender')
        //     ->allowEmpty('gender');

        // $validator
        //     ->scalar('blood_group')
        //     ->allowEmpty('blood_group');

        // $validator
        //     ->date('birthdate')
        //     ->allowEmpty('birthdate');

        // $validator
        //     ->date('date_issued')
        //     ->allowEmpty('date_issued');

        // $validator
        //     ->date('date_expiry')
        //     ->allowEmpty('date_expiry');

        // $validator
        //     ->scalar('signature')
        //     ->allowEmpty('signature');

        // $validator
        //     ->boolean('is_used')
        //     ->requirePresence('is_used', 'create')
        //     ->notEmpty('is_used');

        // $validator
        //     ->requirePresence('created_by', 'create')
        //     ->notEmpty('created_by');

        // $validator
        //     ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['user_id'], 'Users'));
        // $rules->add($rules->existsIn(['organize_id'], 'Organizes'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'core';
    }

}
