<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubjectEnroll Entity
 *
 * @property int $id
 * @property int $subject_id
 * @property int $user_id
 * @property int $master_organization_id
 * @property string $card_code
 * @property bool $is_used
 * @property \Cake\I18n\FrozenTime $created
 * @property int $create_uid
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $update_uid
 *
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\MasterOrganization $master_organization
 */
class SubjectEnroll extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'subject_id' => true,
        'user_id' => true,
        'master_organization_id' => true,
        'card_code' => true,
        'is_used' => true,
        'created' => true,
        'create_uid' => true,
        'modified' => true,
        'update_uid' => true
        /*'subject' => true,
        'user' => true,
        'master_organization' => true*/
    ];
}
