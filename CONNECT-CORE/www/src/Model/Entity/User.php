<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $organize_id
 * @property string $card_code
 * @property string $img_path
 * @property string $prefix_name_th
 * @property string $firstname_th
 * @property string $lastname_th
 * @property string $prefix_name_en
 * @property string $firstname_en
 * @property string $lastname_en
 * @property string $department_name
 * @property string $section_name
 * @property string $position_name
 * @property string $gender
 * @property string $blood_group
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property \Cake\I18n\FrozenDate $date_issued
 * @property \Cake\I18n\FrozenDate $date_expiry
 * @property string $signature
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Organize $organize
 */
class User extends Entity {

    
        /**
     *
     * Fields that are excluded from JSON versions of the entity.
     * @author  pakgon.Ltd
     * @var     array
     */
    protected $_hidden = [
        'password',
        'token',
        'token_expiry',
        'dynamic_key',
        'dynamic_key_expiry'
    ];
    
    /**
     * 
     * Function set username when try to save / register / update profile
     * @author sarawutt.b
     * @param type $username
     * @return type
     */
    protected function _setUsername($username) {
        return strtolower(trim($username));
    }

    /**
     * 
     * Function get for Officer full name
     * @author sarawutt.b
     * @return string
     */
    protected function _getFullName() {
        return $this->first_name . '  ' . $this->last_name;
    }


    /**
     * 
     * Function trigger save database
     * @author sarawutt.b
     * @param type $password
     * @return type
     */
    protected function _setPassword($password) {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    /**
     * 
     * Function checking for owner of the article | Example
     * @param type $articleId
     * @param type $userId
     * @return type
     */
    protected function isOwnedBy($articleId, $userId) {
        return $this->exists(['id' => $articleId, 'user_id' => $userId]);
    }

}
