<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserCard Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $organize_id
 * @property string $card_code
 * @property string $img_path
 * @property string $prefix_name_th
 * @property string $firstname_th
 * @property string $lastname_th
 * @property string $prefix_name_en
 * @property string $firstname_en
 * @property string $lastname_en
 * @property string $department_name
 * @property string $section_name
 * @property string $position_name
 * @property string $gender
 * @property string $blood_group
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property \Cake\I18n\FrozenDate $date_issued
 * @property \Cake\I18n\FrozenDate $date_expiry
 * @property string $signature
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Organize $organize
 */
class UserCard extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'organize_id' => true,
        'card_code' => true,
        'img_path' => true,
        'prefix_name_th' => true,
        'firstname_th' => true,
        'lastname_th' => true,
        'prefix_name_en' => true,
        'firstname_en' => true,
        'lastname_en' => true,
        'department_name' => true,
        'section_name' => true,
        'position_name' => true,
        'gender' => true,
        'blood_group' => true,
        'birthdate' => true,
        'date_issued' => true,
        'date_expiry' => true,
        'signature' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'user' => true,
        'organize' => true,
        'user_type_id' => true
    ];
}
