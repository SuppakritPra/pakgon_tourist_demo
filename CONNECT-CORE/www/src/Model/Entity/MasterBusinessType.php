<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterBusinessType Entity
 *
 * @property int $id
 * @property int $master_language_id
 * @property string $business_type_name
 * @property int $seq_no
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\MasterLanguage $master_language
 */
class MasterBusinessType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_language_id' => true,
        'business_type_name' => true,
        'seq_no' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'master_language' => true
    ];
}
