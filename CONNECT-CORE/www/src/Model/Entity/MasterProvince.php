<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterProvince Entity
 *
 * @property int $id
 * @property int $master_country_id
 * @property string $province_code
 * @property string $province_name_th
 * @property string $province_name_en
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\MasterCountry $master_country
 * @property \App\Model\Entity\MasterDistrict[] $master_districts
 */
class MasterProvince extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_country_id' => true,
        'province_code' => true,
        'province_name_th' => true,
        'province_name_en' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'master_country' => true,
        'master_districts' => true
    ];
}
