<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterUserType Entity
 *
 * @property int $id
 * @property string $user_type_name_th
 * @property string $user_type_name_en
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\MasterPrefix[] $master_prefixes
 */
class MasterUserType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_type_name_th' => true,
        'user_type_name_en' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'master_prefixes' => true
    ];
}
