<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserPersonList Entity
 *
 * @property int $id
 * @property int $master_organization_id
 * @property int $master_department_id
 * @property int $master_section_id
 * @property string $card_code
 * @property string $first_name
 * @property string $last_name
 * @property int $master_user_type_id
 * @property int $user_id
 * @property bool $is_used
 * @property \Cake\I18n\FrozenTime $created
 * @property int $create_uid
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $update_uid
 *
 * @property \App\Model\Entity\MasterOrganization $master_organization
 * @property \App\Model\Entity\MasterDepartment $master_department
 * @property \App\Model\Entity\MasterSection $master_section
 * @property \App\Model\Entity\MasterUserType $master_user_type
 * @property \App\Model\Entity\User $user
 */
class UserPersonList extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_organization_id' => true,
        'master_department_id' => true,
        'master_section_id' => true,
        'card_code' => true,
        'first_name' => true,
        'last_name' => true,
        'master_user_type_id' => true,
        'user_id' => true,
        'is_used' => true,
        'created' => true,
        'create_uid' => true,
        'modified' => true,
        'update_uid' => true,
	'master_prefix_id' => true
        /*'master_organization' => true,
        'master_department' => true,
        'master_section' => true,
        'master_user_type' => true,
        'user' => true*/
    ];
}
