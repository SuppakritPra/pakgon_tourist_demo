<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $organize_id
 * @property int $dept_id
 * @property int $section_id
 * @property string $card_code
 * @property int $user_type_id
 * @property string $img_path
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $position_org
 * @property int $position_edu
 * @property \Cake\I18n\FrozenDate $enter_date
 * @property string $address
 * @property int $master_business_type_id
 * @property string $phone_no
 * @property int $master_organization_position_id
 */
class UserProfile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'organize_id' => true,
        'dept_id' => true,
        'section_id' => true,
        'card_code' => true,
        'user_type_id' => true,
        'img_path' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'position_org' => true,
        'position_edu' => true,
        'enter_date' => true,
        'address' => true,
        'master_business_type_id' => true,
        'phone_no' => true,
        'master_organization_position_id' => true
    ];
}
