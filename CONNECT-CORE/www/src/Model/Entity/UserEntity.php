<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Building Entity
 */
class UserEntity extends Entity {
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    /**
     * 
     * Function get for Officer full name
     * @author sarawutt.b
     * @return string
     */
    protected function _getFullName() {
        return $this->first_name . '  ' . $this->last_name;
    }

    /**
     *
     * Fields that are excluded from JSON versions of the entity.
     * @author  pakgon.Ltd
     * @var     array
     */
    protected $_hidden = [
        'password',
        'token',
        'token_expiry',
        'dynamic_key',
        'dynamic_key_expiry'
    ];

    /**
     * 
     * Function trigger save database
     * @author sarawutt.b
     * @param type $password
     * @return type
     */
    protected function _setPassword($password) {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    /**
     * 
     * Function checking for owner of the article | Example
     * @param type $articleId
     * @param type $userId
     * @return type
     */
    protected function isOwnedBy($articleId, $userId) {
        return $this->exists(['id' => $articleId, 'user_id' => $userId]);
    }

}
