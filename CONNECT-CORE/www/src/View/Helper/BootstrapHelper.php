<?php

/**
 * 
 * Bootstrap Helper.
 * @author  sarawutt.b
 * @since   2018-03-26 14:10:30
 * @license Zicure Corp
 */

namespace App\View\Helper;

use Cake\View\Helper;

class BootstrapHelper extends Helper {

    public $selectEmptyMsg = '---- please select ----';

    /**
     *
     * Documentation 
     * Link : https://holt59.github.io/cakephp3-bootstrap-helpers/
     * Git : https://github.com/Holt59/cakephp3-bootstrap-helpers
     * @var type 
     */
    public $helpers = [
        'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
        'Html' => ['className' => 'Bootstrap.Html'],
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Navbar' => ['className' => 'Bootstrap.Navbar', 'autoActiveLink' => true],
        'Paginator',
        'Panel' => ['className' => 'Bootstrap.Panel']
    ];

    /**
     * Function get for empty option in DDL
     * @author sarawutt.b
     * @return array() of empty select DDL
     */
    public function getEmptySelect() {
        return ['' => __($this->selectEmptyMsg)];
    }

    /**
     * Function get for empty message select
     * @author sarawutt.b
     * @return string of empty select message
     */
    public function getTextEmptySelect() {
        return __($this->selectEmptyMsg);
    }

    public function iButton($icon = null, $title = null, $options = []) {
        $selfOptions = ['class' => 'btn btn-default'];
        if (is_array($options)) {
            $selfOptions = array_merge($selfOptions, $options);
        }
        return $this->Form->button($this->Html->icon($icon) . ' ' . __($title), $selfOptions);
    }

    /**
     * 
     * Function make bootstrap icon support Font Awesome and Gryph Icon
     * @author  sarawutt.b
     * @param   type $icon as string icon name class
     * @param   type $vendor as string of vendor name posible value fa|glyphicon
     * @param   type $wrapElement as string HTNL wrapping element
     * @param   type $message as a string message after the icon
     * @return  string of HTML wrapped icon
     */
    public function icon($icon = null, $message = null, $vendor = 'fa', $wrapElement = 'i') {
        return !is_null($icon) ? "<{$wrapElement} class=\"{$vendor} {$icon}\"></{$wrapElement}>" . __($message) : null;
    }

    /**
     * 
     * Function make for bootstrap label or badge
     * @author  sarawutt.b
     * @param   type $msg as a string of wrapped message
     * @param   type $class as a string bootstrap color class
     * @param   type $wrapElement as string HTNL wrapping element
     * @return  string of HTML wrapped label
     */
    public function badge($msg = null, $class = 'warning', $paragraph = null, $wrapElement = 'span') {
        return !is_null($msg) ? (is_null($paragraph) ? null : "<{$paragraph}>") . "<{$wrapElement} class=\"label label-{$class}\">" . __($msg) . "</{$wrapElement}>" . (is_null($paragraph) ? null : "</{$paragraph}>") : null;
    }

    /**
     * 
     * Function make for bootstrap label or badge
     * @author  sarawutt.b
     * @param   type $msg as a string of wrapped message
     * @param   type $class as a string bootstrap color class
     * @param   type $wrapElement as string HTNL wrapping element
     * @return  string of HTML wrapped label
     */
    public function label($msg = null, $class = 'warning', $paragraph = null, $wrapElement = 'span') {
        return $this->badge($msg, $class, $paragraph, $wrapElement);
    }

    public function callout($title = 'Bootstrap Collout Helper', $msg = 'Bootstrap Collout Helper', $class = 'info') {
        $petern = sprintf("<div class=\"bs-callout bs-callout-{$class}\"> 
                    <h4>%s</h4> 
                    <p>%s</p> 
                </div>", __($title), __($msg));
        return $petern;
    }

    public function modal($title = 'Bootstrap Modal Helper', $msg = 'Bootstrap Modal Helper', $class = 'lg', $options = array()) {
        $petern = sprintf("<div class=\"modal fade modal-immediately\" aria-hidden=\"false\" tabindex=\"-1\" role=\"dialog\">
                            <div class=\"modal-dialog modal-{$class}\" role=\"document\">
                              <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"%s\"><span aria-hidden=\"true\">&times;</span></button>
                                  <h4 class=\"modal-title\">%s</h4>
                                </div>
                                <div class=\"modal-body\">
                                  <p>%s</p>
                                </div>
                                <div class=\"modal-footer\">
                                  <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">%s</button>
                                  <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
                                </div>
                              </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          </div><!-- /.modal -->", __('Close'), __($title), __($msg), __('Close'));
        return $petern;
    }

    public function alert($title = 'Bootstrap Collout Helper', $msg = 'Bootstrap Collout Helper', $class = 'danger') {
        $petern = sprintf("<div class=\"alert alert-{$class} alert-dismissible fade in\" role=\"alert\"> 
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"%s\">
                                        <span aria-hidden=\"true\">×</span>
                                </button> 
                                <strong>%s</strong> %s 
                        </div>", __('Close'), __($title), __($msg));
        return $petern;
    }

    /**
     * 
     * Function Bootstrap generate main color class
     * @author  sarawutt.b
     * @param   type $index as integer of number class index
     * @return  string name color class
     */
    public function bootstrapBoxClass($index = null) {
        $bclass = array('warning', 'success', 'info', 'default', 'primary', 'danger');
        return array_key_exists($index, $bclass) ? $bclass[$index] : $bclass[$index % count($bclass)];
    }

    /**
     * 
     * Function css bootstrap clearfix
     * @author sarawutt.b
     * @return string
     */
    public function creafix() {
        return '<div class="clearfix"></div>';
    }

    public function _wrapClass($title = '', $tag = 'p'){
        return "<{$tag}>{$title}</$tag>";
    }
}
