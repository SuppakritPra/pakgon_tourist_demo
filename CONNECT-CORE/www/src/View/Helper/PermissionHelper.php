<?php

/**
  | ------------------------------------------------------------------------------------------------------------------
  | Helper Permission Helper
  | @author  sarawutt.b
  | @since   2018/02/08
  | @license Zicure Corp
  |	------------------------------------------------------------------------------------------------------------------
  | 1. isPermission
  | 2. button_submit
  | 3. link_view
  | 4. link_edit
  | 5. link_del
  | 6. link_button
  | 7. link_munues
  | 8. link_permissions
  | 9. link_accept
  | 10. link_reject
  | 11. link_button_popup
  | 12. add_area
  | 13. link_inventory
  | 14. link_close_job
  | 15. link_cancel
  | 16. attachment
  | 17. link_print make print button no need to redirect only acl access check
  |------------------------------------------------------------------------------------------------------------------------
 */

namespace App\View\Helper;

use Cake\View\Helper;

class PermissionHelper extends Helper {

    public $helpers = array('Html', 'Form', 'Bootstrap');

    /**
     * This function display for java script button when you click the button they turn back page
     * @author  sarawutt.b
     * @param   $align as a string of button position alignment possible pull-right|pull-left default pull-right
     * @param   $class as a string of css class
     * @since   2018-03-28
     * @return  html string of button history.back()
     */
    public function buttonBack($align = 'pull-right', $options = []) {
        return $this->Form->button($this->__icon(['icon'=>'fa-arrow-left']) . __('Back'), array_merge(['type' => 'button', 'class' => "btn btn-flat btn-default btn-back separate-button {$align}", 'onclick' => 'window.history.back();', 'name' => 'btnBack'], $options));
    }

    /**
     * This function display for direct back link when you click the button link they turn back page
     * @author  sarawutt.b
     * @param   $link as a string of back where you want to come back
     * @param   $class as a string of css class
     * @param   $align as string of button position alignment possible right|left default right
     * @since   2018-03-28
     * @return  html string of button back link as specific
     */
    public function linkBack($link = '#', $class = 'btn btn-flat btn-default btn-back', $align = 'pull-right') {
        return $this->link(__('Back'), $link, ['class' => "{$class} separate-button {$align}", 'name' => 'linkBack']);
    }

    /**
     * This function display for direct back link when you click the button link they turn back page
     * @author  sarawutt.b
     * @param   $title as a string of link title
     * @param   $link as a string of link where you want to go
     * @param   $class as a string of css class
     * @param   $align as string of button position alignment possible right|left default right
     * @since   2018-03-28
     * @return  html string of button back link as specific
     */
    public function customLink($title = 'Back', $link = '#', $class = 'btn btn-default btn-flat btn-custome-link', $align = 'pull-right') {
        return $this->link(__($title), $link, ['class' => $class, 'name' => 'btnCustomeLink']);
    }

    /**
     * This function for check current user where logedin can use any cnotroller/action
     * @author sarawutt.b
     * @since 20/10/2013
     * @params Permission Mode True in has permission FALSE in not has
     * @return void
     */
    public function isPermission($url = null) {
        //return ClassRegistry::init('Utility')->isPermission(Configure::read('Permission.ModePermission'), $url);
        return true;
    }

    public function buttonOption($name, $url, $msg = null, $options = []) {
        if (empty($msg)) {
            $onclick = "window.location='{$url}'";
        } else {
            $onclick = "if(confirm('{$msg}')) window.location='{$url}'; else return false;";
        }
        $_selfOptions = ['div' => false, 'label' => false, 'type' => 'button', 'class' => 'btn btn-default btn-flat', 'onclick' => $onclick];
        $options = array_merge($_selfOptions, $options);
        return $this->isPermission($url) ? $this->Form->button($name, $options) : null;
    }

    public function link_button($name, $url, $msg = null, $options = []) {
        if (empty($msg)) {
            $onclick = "window.location='{$url}'";
        } else {
            $onclick = "if(confirm('$msg')) window.location='{$url}'; else return false;";
        }

        $_selfOptions = ['div' => false, 'label' => false, 'type' => 'button', 'onclick' => $onclick, 'class' => 'btn btn-active'];
        $options = array_merge($_selfOptions, $options);
        return $this->isPermission($url) ? $this->Form->button($name, $options) : null;
    }

    public function linkMunues($url) {
        return $this->isPermission($url) ? $this->Html->link($this->Html->image("task_menu.png", ['alt' => "แก้ไขเพิ่มเมนูให้กับกลุ่มผู้ใช้", 'height' => 24, 'width' => 24]), $url, ['class' => 'editLink', 'escape' => false, 'title' => 'แก้ไขเพิ่มเมนูให้กับกลุ่มผู้ใช้']) : null;
    }

    /**
     * 
     * Grant / Revoke like on the permission page
     * @author sarawutt.b
     * @param type $url
     * @return type
     */
    public function linkPermissions($url) {
        return $this->isPermission($url) ? $this->Html->link($this->Html->image("xsecurity_keyandlock.png", ["alt" => __('Grant / Revoke'), 'height' => 24, 'width' => 24]), $url, ['class' => 'editLink', 'escape' => false, 'title' => __('Grant / Revoke')]) : null;
    }

    /*
     * 
     * ----------------------------------------------------------------------------------------------------------------------------------------
     * Function link and button permission helper
     * ----------------------------------------------------------------------------------------------------------------------------------------
     * @author  sarawutt.b
     * @since   2016/05/18
     * ----------------------------------------------------------------------------------------------------------------------------------------
     */

    /**
     * 
     * Function make submit button for submit for the form if check pass permission
     * @author  sarawutt.b
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP code style
     * @return  string as HTML submit tag if has permission otherwise return null
     */
    public function submit($title, $url, $options = []) {
        $selfOption = ['type' => 'submit', 'class' => 'btn btn-primary btn-flat'];
        $options = array_merge($selfOption, $options);
        return $this->isPermission($url) ? $this->Form->button($this->__icon($options) . __($title), $options) : null;
    }

    /**
     * 
     * Function make submit button for submit for the form if check pass permission
     * @author  sarawutt.b
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP code style
     * @return  string as HTML submit tag if has permission otherwise return null
     */
    public function buttonSubmit($title = 'Submit', $url, $options = []) {
        $selfOption = ['type' => 'submit', 'class' => 'btn btn-primary'];
        return $this->isPermission($url) ? $this->Form->button($this->__icon($options) . __($title), array_merge($selfOption, $options)) : null;
    }

    /**
     * 
     * Function make submit button for submit for the form if check pass permission
     * @author  sarawutt.b
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP code style
     * @return  string as HTML submit tag if has permission otherwise return null
     */
    public function submitOption($title = 'Submit', $url, $options = []) {
        $selfOption = ['type' => 'submit'];
        return $this->isPermission($url) ? $this->Form->button($this->__icon($options) . __($title), array_merge($selfOption, $options)) : null;
    }

    /**
     * 
     * Function make for the permission button if has permission this display for the button otherwise display none
     * @author  sarawutt.b
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @return  string HTML button if has permission otherwise return NULL
     */
    public function button($title, $url = null, $options = []) {
        $checkPermission = false;
        if (!is_null($url)) {
            if (array_key_exists('data-confirm-title', $options)) {
                $options['action'] = $url;
            } else {
                $options['onclick'] = "window.location='{$url}'";
            }
        } else {
            $checkPermission = true;
        }
        $selfOption = ['type' => 'button', 'class' => 'btn btn-active'];
        $options = array_merge($selfOption, $options);
        return ($this->isPermission($url) || ($checkPermission)) ? $this->Form->button($this->__icon($options) . __($title), $options) : null;
    }

    /**
     * 
     * Function make confirm button display popup button befor redirect to the destination
     * @author  sarawutt.b
     * @param   type $title as string 
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @param   type $msg as string of confirmation message
     * @return  string HTML button if has permission otherwise return NULL
     */
    public function confirmButton($title, $url, $options = [], $msg = 'Do you want confirm for the action ?') {
        $selfOption = array('type' => 'button', 'class' => 'btn btn-active confirmModal', 'data-confirm-message' => __($msg), 'action' => $url);
        $options = array_merge($selfOption, $options);
        return $this->isPermission($url) ? $this->Form->button($this->__icon($options) . __($title), $options) : null;
    }

    /**
     * 
     * Function make confirmation submit form
     * @author  sarawutt.b
     * @param   type $title as string 
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @param   type $msg as string of confirmation message
     * @return  string HTML button if has permission otherwise return NULL
     */
    public function confirmSubmit($title, $url, $options = [], $msg = 'Do you want confirm for the action ?') {
        $selfOption = ['type' => 'submit', 'class' => 'btn btn-primary confirmModal', 'data-confirm-message' => __($msg)];
        $options = array_merge($selfOption, $options);
        return $this->isPermission($url) ? $this->Form->button($this->__icon($options) . __($title), $options) : null;
    }

    /**
     * 
     * Function make for display HTML link incrude with permission
     * @author  sarawutt.b
     * @param   type $title as string of title on the link
     * @param   type $url as string of the destination path
     * @param   array $options as array of HTML option
     * @return  string of the output link
     */
    public function link($title = null, $url, $options = []) {
        $options['escape'] = false;
        return ($this->isPermission($url) && !empty($url)) ? $this->Html->link($this->__icon($options) . __($title), $url, $options) : $this->Html->link($this->__icon($options) . __($title), 'javascript:;', $options);
    }

    /**
     * 
     * Function make the POST Link benerfit for action link only allow post method
     * @param   type $title as string of title on the link
     * @param   type $url as string of the destination path
     * @param   type $options as array of HTML option
     * @param   type $confirmMessage as string if has a confirmation message
     * @return  string of the output link
     */
    public function postLink($title = null, $url, $options = []) {
        $options['escape'] = false;
        return $this->isPermission($url) ? $this->Form->postLink($this->__icon($options) . __($title), $url, $options) : null;
    }

    /**
     * 
     * Function getAction make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $controller as string of link destination controller
     * @param   type $linkView as boolean of is show view link
     * @param   type $linkEdit as boolean of is show edit link
     * @param   type $linkDelete as boolean of is show delete link
     * @param   type $linkUnlock as boolean of is show unblock link
     * @return  string
     */
    public function getActions($id, $controller = null, $linkView = true, $linkEdit = true, $linkDelete = true, $linkUnlock = false) {
        $list = '';
        $patern = '<li>%s</li>';
        $controller = empty($controller) ? $this->request->controller : $controller;

        if ($linkView === true) {
            $list .= sprintf($patern, $this->link('View', ['controller' => $controller, 'action' => 'view', $id]));
        }
        if ($linkEdit === true) {
            $list .= sprintf($patern, $this->link('Edit', ['controller' => $controller, 'action' => 'edit', $id]));
        }
        if ($linkDelete === true) {
            $list .= sprintf($patern, $this->postLink('Delete', ['controller' => $controller, 'action' => 'delete', $id], ['class' => 'action-delete btnGroupMenu', 'data-confirm-title' => __('Are you sure you want to delete ?'), 'data-confirm-message' => __('Are you sure you want to delete # {0}?', __($controller))]));
        }
        if ($linkUnlock === true) {
            $list .= sprintf($patern, $this->link('Unlock', '/' . $controller . '/unlock/' . $id));
        }
        return $this->__getActionMenu($list);
    }

    /**
     * 
     * Function generate html link
     * @author  sarawutt.b
     * @param   $list as a string of menu list
     * @return  string of a output list menu
     */
    public function __getActionMenu($list = '', $class = 'bg-olive btn-flat') {
        $menu = '<div class="btn-group">
                    <button class="btn ' . $class . ' col-xs-9" type="button"><i class="fa fa-gears hidden-xs"></i>' . __('Action') . '</button>
                    <button data-toggle="dropdown" class="btn ' . $class . ' dropdown-toggle col-xs-3" type="button" aria-expanded="true">
                        <span class="caret"></span>
                        <span class="sr-only">' . __('Toggle Dropdown') . '</span>
                    </button>
                    <ul role="menu" class="dropdown-menu">
                      ' . $list . '
                    </ul>
              </div>';
        return $menu;
    }

    /**
     * 
     * Delete link button style used this for render delete button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function linkDelete($url, $options = []) {
        return $this->link('Delete', $url, array_merge(['icon' => 'fa fa-remove', 'class' => 'btn btn-danger'], $options));
    }

    /**
     * 
     * Delete link button style used this for render print button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function linkPrint($url, $options = []) {
        return $this->link('Print', $url, array_merge(['icon' => 'fa fa-print', 'class' => 'btn btn-warning display=none;'], $options));
    }

    /**
     * 
     * Delete link button style used this for render print button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function linkAttachmentFile($url, $options = []) {
        return $this->link('Add Another File', $url, array_merge(['icon' => 'fa fa-file-text', 'class' => 'btn btn-primary'], $options));
    }

    /**
     * 
     * Add / Plus link button style used this for render add / plus button
     * @author  sarawutt.b
     * @param   type $url as string where destination link
     * @param   type $class option may make other button style
     * @return  string
     */
    public function linkAdd($url, $class = 'btn btn-primary') {
        return $this->link('Add', $url, ['icon' => 'fa fa-plus', 'class' => $class]);
    }

    /**
     * 
     * Add / Plus button style used this for render add / plus button
     * @author  sarawutt.b
     * @param   type $url as string where destination link
     * @param   type $class option may make other button style
     * @return  string
     */
    public function buttonAdd($url, $class = 'btn btn-primary') {
        return $this->isPermission($url) ? $this->Form->button($this->__icon(['icon' => 'fa-plus']) . __('Add'), ['type' => 'button', 'class' => 'btn btn-primary']) : null;
    }

    /**
     * 
     * Add / Plus submit button style used this for render add / plus submit button
     * @author  sarawutt.b
     * @param   type $url as string where destination link
     * @param   type $options is array() of cake options style
     * @return  string
     */
    public function submitAdd($url, $options = []) {
        return $this->isPermission($url) ? $this->Form->button($this->__icon(['icon' => 'fa-plus']) . __('Add'), array_merge(['type' => 'submit', 'class' => 'btn btn-primary', 'name' => 'btnSubmitAdd', 'id' => 'btnSubmitAdd'], $options)) : null;
    }

    /**
     * 
     * submit save button style used this for render add / plus submit button
     * @author  sarawutt.b
     * @param   type $url as string where destination link
     * @param   type $options is array() of cake options style
     * @return  string
     */
    public function submitSave($url, $options = []) {
        return $this->isPermission($url) ? $this->Form->button($this->__icon(['icon' => 'fa-save']) . __('Save'), array_merge(['name' => 'btnsubmit', 'id' => 'btnsubmit', 'class' => 'btn btn-primary'], $options)) : null;
    }

    /**
     * 
     * Buton added for Document Attachment input
     * @author  sarawutt.b
     * @param   type $options is array() of cake options style
     * @return  string
     */
    public function buttonAttachmentFile($options = []) {
        return $this->Form->button($this->__icon(['icon' => 'fa-file-text']) . __('Add Another File'), array_merge(['name' => 'btnAddAttachment', 'id' => 'btnAddAttachment', 'class' => 'btn btn-success', 'type' => 'button'], $options));
    }

    /**
     * 
     * Check for array parameter has exists icon
     * @author  sarawutt.b
     * @param   $options as an array html options
     * @return  string icon if has icon otherwise return null
     */
    private function __icon($options = []) {
        return (is_array($options) && array_key_exists('icon', $options)) ? $this->Bootstrap->icon($options['icon']) : null;
    }

}
