<?php

/**
 * 
 * Function Helper for any Cake PHP Configure
 * @author sarawutt.b
 */

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;

class ConfigureHelper extends Helper {

    /**
     * 
     * Function read configure
     * @author sarawutt.b
     * @param type $key as a string of configure key
     * @return mix configure value
     */
    public function read($key) {
        return Configure::read($key);
    }

    /**
     * 
     * Function write configure
     * @author sarawutt.b
     * @param type $key as a string of configure key
     * @return boolean
     */
    public function write($key) {
        return Configure::write($key);
    }
}
