<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserCard $userCard
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Card'), ['action' => 'edit', $userCard->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Card'), ['action' => 'delete', $userCard->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userCard->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Cards'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Card'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userCards view large-9 medium-8 columns content">
    <h3><?= h($userCard->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $userCard->has('user') ? $this->Html->link($userCard->user->username, ['controller' => 'Users', 'action' => 'view', $userCard->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Card Code') ?></th>
            <td><?= h($userCard->card_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Img Path') ?></th>
            <td><?= h($userCard->img_path) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prefix Name Th') ?></th>
            <td><?= h($userCard->prefix_name_th) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Firstname Th') ?></th>
            <td><?= h($userCard->firstname_th) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lastname Th') ?></th>
            <td><?= h($userCard->lastname_th) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prefix Name En') ?></th>
            <td><?= h($userCard->prefix_name_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Firstname En') ?></th>
            <td><?= h($userCard->firstname_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lastname En') ?></th>
            <td><?= h($userCard->lastname_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Department Name') ?></th>
            <td><?= h($userCard->department_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Section Name') ?></th>
            <td><?= h($userCard->section_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Position Name') ?></th>
            <td><?= h($userCard->position_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gender') ?></th>
            <td><?= h($userCard->gender) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Blood Group') ?></th>
            <td><?= h($userCard->blood_group) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Signature') ?></th>
            <td><?= h($userCard->signature) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userCard->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Organize Id') ?></th>
            <td><?= $this->Number->format($userCard->organize_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $this->Number->format($userCard->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($userCard->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Birthdate') ?></th>
            <td><?= h($userCard->birthdate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Issued') ?></th>
            <td><?= h($userCard->date_issued) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Expiry') ?></th>
            <td><?= h($userCard->date_expiry) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userCard->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userCard->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Used') ?></th>
            <td><?= $userCard->is_used ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
