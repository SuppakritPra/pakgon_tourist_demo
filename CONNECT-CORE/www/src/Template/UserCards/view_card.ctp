<?php
use Cake\I18n\Time;
?>
    <div class="row">
        <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}">
			<?php 
				$i=0; 
				foreach ($UserCards as $value) { ?>
		
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 box-arrow left">
                    <i class="fa fa-angle-left prev"> </i>
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="box-card">
                        <div data-toggle="modal" data-target="#card_<?php echo $i;?>"  style="cursor: pointer; padding-right: 10px; padding-left: 10px;">
                            <div class="row">  
                                <div class="col-xs-8">
                                    <div style="font-size: 17px">
                                        <br>
                                        <label><?php echo __('id') ?><label>
                                    </div>
                                    <div>
                                        <?php echo $value['card_code'] ?>
                                    </div>
                                    <div>
                                        <label style="font-size: 17px"><?php echo __('Name') ?></label>
                                        <?php echo $value['prefix_name_th'].$value['firstname_th'].' '.$value['lastname_th'] ?>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <img src="/img/core/img/user-profile@3x.png" class="img-responsive">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10">
                                    <label style="font-size: 17px"><?php echo __('Position') ?></label>
                                    <?php echo $value['position_name'] ?>
                                </div>
                            </div>
                            <div class="row text-center">
                                <br>
                                <div class="col-xs-6">
                                    <?php 
	                        $now = new Time($value['date_issued']);
	                        $dateIssued = $now->i18nFormat('yyyy-MM-dd');
	                        echo $this->DateFormat->formatDateThai($dateIssued);                  
	                    ?>
                                </div>
                                <div class="col-xs-6">
                                    <?php 
	                        $now = new Time($value['date_expiry']);
	                        $dateExpiry = $now->i18nFormat('yyyy-MM-dd');
	                        echo $this->DateFormat->formatDateThai($dateExpiry);                  
	                    ?>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-xs-6">
                                    <label style="font-size: 17px"><?php echo __('Card issue') ?></label>
                                </div>
                                <div class="col-xs-6">
                                    <label style="font-size: 17px"><?php echo __('Old age') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.box-card -->
                <div class="col-xs-1 col-sm-1 col-md-1 box-arrow right">
                    <i class="fa fa-angle-right next"></i>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <h4><?php echo __('Company Information') ?></h4>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Firstname') ?></label>
                            <?php echo $value['prefix_name_th'].$value['firstname_th'] ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Lastname') ?></label>
                            <?php echo $value['lastname_th'] ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Company Name') ?></label>
                            <?php echo $value['morg']['org_name_th'] ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Department') ?></label>
                            <?php echo $value['section_name'] ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Position') ?></label>
                            <?php echo $value['position_name'] ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Card issue') ?></label>
                            <?php 
	                        $now = new Time($value['date_issued']);
	                        $dateIssued = $now->i18nFormat('yyyy-MM-dd');
	                        echo $this->DateFormat->formatDateThai($dateIssued);                  
	                    ?>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label style="font-size: 17px" class="col-xs-4 col-sm-4 col-md-4"><?php echo __('Old age') ?></label>
                            <?php 
	                        $now = new Time($value['date_expiry']);
	                        $dateExpiry = $now->i18nFormat('yyyy-MM-dd');
	                        echo $this->DateFormat->formatDateThai($dateExpiry);                  
	                    ?>
                        </div>
                    </div>
                </div>
				<!--/.col-xs-12 -->
			
            </div>
			<!--/.row -->
            
			<?php
				$i++; 
				} 
			?>

            <div class="col-xs-12">
                <div class="col-xs-1 col-sm-1 col-md-1 box-arrow left">
                    <i class="fa fa-angle-left prev"> </i>
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10">
                    <img src="/img/core/img/bloc-plus@3x.png" class="img-responsive" style="width: 100%">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 box-arrow right"></div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <?php echo $this->Form->create('UserCards', [
                        'id' => 'frmSignIn', 
                        'type' => 'file', 
                        'onsubmit' => 'return check();',
                        'url' => ['controller' => 'user-cards', 'action' => 'view-card']
                        ]); ?>
                        <div class="form-group has-feedback">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php echo $this->Flash->render(); ?>
                                <?php echo $this->Form->hidden('UserCards.user_id', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'userId', 'type' => 'text', 'placeholder' => 'USERNAME']); ?>
                                <?php echo $this->Form->hidden('UserCards.organize_id', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'organize_id', 'type' => 'text', 'label' => __('ID Active'), 'placeholder' => 'USERNAME', 'required']); ?>
                                <?php echo $this->Form->input('UserCards.organize_code', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'organize_code', 'type' => 'text', 'label' => __('Company Code'), 'placeholder' => 'Company Code', 'required']); ?>
                                <label id="nameorg_th" style="color: green"></label>
                                <label id="noorg_th" style="color: red"></label>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <?php echo $this->Form->input('UserCards.employee', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'employee', 'type' => 'text', 'label' => __('Ref 1'), 'placeholder' => 'Ref 1', 'disabled']); ?>
                                <label id="nameemp_th" style="color: green"></label>
                                <label id="noepm_th" style="color: red"></label>
                            </div>
                        </div>
                        <div class="form-group has-feedback bootstrap-iso">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label class="label-text-sub">
                                    <?php echo __('Ref 2');?>
                                </label>
                                <input type="text" name="UserCards[birthdate]" id="date" placeholder="Ref 2" value="" readonly placeholder="DD-MM-YYYY" data-date-format="mm/dd/yyyy"
                                    class="form-control-reg border-bottom-from" disabled>
                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-quaternary mr-xs mb-sm button-text-profile" id="checkbutton"><?php echo __('Check') ?></button>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
			<!--/.row -->
        </div>
		<!--/.owl-carousel --> 
        	<?php 
		$i=0; 
		foreach ($UserCards as $value) {  
			echo $this->element('card_from/from_0',array('value' => $value,'i'=> $i));
			$i++; 
		} 
		?>
		
    </div>
    
    
    <style>
        #alertBox {
            position: relative;
            width: 300px;
            min-height: 100px;
            /*margin-top:50px;*/
            border: 1px solid #666;
            background-color: #fff;
            background-repeat: no-repeat;
            background-position: 20px 30px;
            left: 0;
            right: 0;
            top: 100px;
            z-index: 2;
        }

        #modalContainer>#alertBox {
            position: fixed;
        }

        #alertBox h1 {
            margin: 0;
            font: bold 0.9em verdana, arial;
            background-color: #000;
            color: #FFF;
            border-bottom: 1px solid #000;
            padding: 2px 0 2px 5px;
        }

        #alertBox p {
            font-size: 14px;
            height: 30px;
            padding: 10px;
            /* margin-left: 55px; */
            text-align: center;
        }

        #alertBox #closeBtn {
            display: block;
            position: relative;
            margin: 5px auto;
            padding: 7px;
            border: 0 none;
            width: 70px;
            font: 0.7em verdana, arial;
            text-transform: uppercase;
            text-align: center;
            color: #FFF;
            background-color: #000;
            border-radius: 3px;
            text-decoration: none;
        }

        /* unrelated styles */

        #mContainer {
            position: relative;
            width: 600px;
            margin: auto;
            padding: 5px;
            border-top: 2px solid #000;
            border-bottom: 2px solid #000;
            font: 0.7em verdana, arial;
        }

        h1,
        h2 {
            margin: 0;
            padding: 4px;
            font: bold 1.5em verdana;
            border-bottom: 1px solid #000;
            text-align: center;
        }

        code {
            font-size: 1.2em;
            color: #069;
        }

        #credits {
            position: relative;
            margin: 25px auto 0px auto;
            width: 350px;
            font: 0.7em verdana;
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
            height: 90px;
            padding-top: 4px;
        }

        #credits img {
            float: left;
            margin: 5px 10px 5px 0px;
            border: 1px solid #000000;
            width: 80px;
            height: 79px;
        }

        .important {
            background-color: #F5FCC8;
            padding: 2px;
        }

        code span {
            color: green;
        }

    </style>

    <style>
        .box-card {
            border: 1px solid #ccc;
            height: 200px;
            margin-top: 10px;
            border-radius: 20px;
            box-shadow: 2px 2px 4px #ccc;
            margin-bottom: 10px;
        }

        .box-arrow {

            font-size: 36px;
            height: 200px;
            padding-top: 20%;
            cursor: pointer;


        }

        .box-arrow.left {
            padding-right: 0px;
            text-align: right;

        }

        .box-arrow.right {
            padding-left: 0px;
        }


        .box-ads {
            width: 1%;
            display: table;
            margin: auto;
            position: relative;
            padding-top: 10px;
            margin-top: 5px;
        }

        .box-ads .close-modal {
            position: absolute;
            top: 0;
            right: 0;
            width: 40px;
            height: 40px;
            background-color: #000;
            color: #fff;
            cursor: pointer;
            font-size: 20px;
            border-radius: 20px;
            padding-top: 10px;
            margin-right: 10px;
            text-align: center;
            z-index: 9999;
        }

        .box-ads .close-modal:hover {
            text-decoration: none;
        }

        .rotate270 .row {
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            writing-mode: rl-tb;
            position: absolute;
            font-size: 30px;
        }



        .form_1 .date-name {
            width: 500px;
            margin: 55% 0% 0% -40%;
            border-bottom: 0px solid red;
        }

        .date-name div div {
            height: 35px;
        }

        .form_1 .date-position {
            width: 500px;
            margin: 70% 0% 0% -15%;
            border-bottom: 0px solid #000;
        }

        .form_1 .date-time {
            width: 500px;
            margin: 70% 0% 0% 10%;
            border-bottom: 0px solid #000;
        }

        .form_1 .date-label {
            width: 500px;
            margin: 70% 0% 0% 20%;
            border-bottom: 0px solid #000;
        }

    </style>

    <?php $this->append('scriptBottom'); ?>
    <script>
        var ALERT_TITLE = "";
        var ALERT_BUTTON_TEXT = "Ok";

        if (document.getElementById) {
            window.alert = function (txt) {
                createCustomAlert(txt);
            }
        }

        function createCustomAlert(txt) {
            d = document;
            if (d.getElementById("modalContainer"))
                return;
            mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
            mObj.id = "modalContainer";
            mObj.style.height = d.documentElement.scrollHeight + "px";

            alertObj = mObj.appendChild(d.createElement("div"));
            alertObj.id = "alertBox";
            if (d.all && !window.opera)
                alertObj.style.top = document.documentElement.scrollTop + "px";
            alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
            alertObj.style.visiblity = "visible";

            h1 = alertObj.appendChild(d.createElement("h1"));
            h1.appendChild(d.createTextNode(ALERT_TITLE));

            msg = alertObj.appendChild(d.createElement("p"));
            //msg.appendChild(d.createTextNode(txt));
            msg.innerHTML = txt;

            btn = alertObj.appendChild(d.createElement("a"));
            btn.id = "closeBtn";
            btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
            btn.href = "#";
            btn.focus();
            btn.onclick = function () {
                removeCustomAlert();
                return false;
            }
            alertObj.style.display = "block";
        }

        function removeCustomAlert() {
            document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
        }

        function ful() {
            alert('Alert this pages');
        }

    </script>
    <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            loop: false,
            items: 1,
            rewindNav: false,
            onTranslated: callBack
        });
        owl = $('.owl-carousel').owlCarousel();
        $(".prev").click(function () {
            owl.trigger('prev.owl.carousel');
        });

        $(".next").click(function () {
            owl.trigger('next.owl.carousel');
        });

        function callBack() {
            if ($('.owl-carousel .owl-item').last().hasClass('active')) {
                $('.next').hide();
                $('.prev').show();

            } else if ($('.owl-carousel .owl-item').first().hasClass('active')) {
                $('.prev').hide();
                $('.next').show();
            }
        }

        var organize_id = $("#organize_id");
        var employee = $("#employee");
        var date = $("#date");
        $(document).ready(function () {
            $("#organize_code").on('change', function () {
                var organize_code = $("#organize_code").val();
                $('#employee').attr("disabled", true);
                $('#date').attr("disabled", true);
                $('#checkbutton').attr("disabled", true);
                organize_id.val('');
                employee.val('');
                date.val('');
                $.post("/UserCards/checkOrg", {
                    organize_code: organize_code
                }, function (data) {
                    if (data != 'false') {
                        data = jQuery.parseJSON(data);
                        if (data['chkuser'] == true) {
                            $("#noorg_th").text(<?php echo __('Register with') ?> + data[0]['org_name_th'] +
                            <?php echo __('already') ?>);
                            $("#nameorg_th").text('');
                            $('#employee').attr("disabled", true);
                            organize_id.val('');
                            employee.val('');
                        } else if (data['chkuser'] == false) {
                        	$("#nameorg_th").text('');
                            $("#nameorg_th").text(data[0]['org_name_th']);
                            $("#noorg_th").text('');
                            $("#checkbutton").removeAttr("disabled");
                            $("#employee").removeAttr("disabled");
                            organize_id.val(data[0]['id']);
                        }
                    } else {
                        $("#noorg_th").text(<?php echo __('Data not found') ?>);
                        $("#nameorg_th").text('');
                        $('#employee').attr("disabled", true);
                        organize_id.val('');
                        employee.val('');
                    }
                });
            });
            $("#employee").on('change', function () {
                var employee_val = employee.val();
                var organize_code = $("#organize_code").val();
                $('#date').attr("disabled", true);
                date.val('');
                $.post("/UserCards/checkEmp", {
                    employee_val: employee_val,
                    organize_code: organize_code
                }, function (data) {
                    if (data != 'false') {
                        data = jQuery.parseJSON(data);
                        if (data['chkuser'] == true) {
                            $("#noepm_th").text(<?php echo __('Registered') ?>);
                            $("#nameemp_th").text('');
                            $('#date').attr("disabled", true);
                            date.val('');
                        } else if (data['chkuser'] == false) {
                        	$("#nameemp_th").text('');
                            $("#nameemp_th").text(data['prefix_name_th'] + data['firstname_th'] +
                                ' ' + data['lastname_th']);
                            $("#noepm_th").text('');
                            $("#date").removeAttr("disabled");
                        }
                    } else {
                    	$("#noepm_th").text('');
                        $("#noepm_th").text(
                            <?php echo __('"Did not find the information you need", Please enter the correct information and press the check.') ?>
                        );
                        $("#nameemp_th").text('');
                        $('#date').attr("disabled", true);
                        date.val('');
                    }
                });
            });
        });

        function check() {
        	if ($("#organize_code").val() == '') {
                alert(<?php echo __('Please enter Company Code.') ?>);
                organize_id.focus();
                return false;
            }
            if (employee.val() == '') {
                alert(<?php echo __('Please enter Ref1.') ?>);
                employee.focus();
                return false;
            }
            if (date.val() == '') {
                alert(<?php echo __('Please enter Ref2.') ?>);
                date.focus();
                return false;
            }
        }
        $(document).ready(function () {
            var date_input = $('input[id="date"]'); //our date input has the name "date"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'dd/mm/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
	    
	   
        })

    </script>
    <?php $this->end();?>
    
