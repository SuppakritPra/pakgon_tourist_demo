<div class="row">
    <div class="col-xs-1 col-sm-1 col-md-1">
    <
    </div>   
    <div class="col-xs-10 col-sm-10 col-md-10">
        <img src="/img/core/img/bloc-plus@3x.png" class="img-responsive" style="width: 100%">
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <?php echo $this->Form->create('UserCards', ['id' => 'frmSignIn', 'type' => 'file', 'onsubmit' => 'return check();']); ?>
       <!--  <div style="position: absolute;z-index: 2;">
            <a href="profile_setting"><img src="/img/core/img/setting-icon@3x.png"/></a>
        </div> -->
       
        <!-- <div class="bg-profile"></div> -->
        <!--<div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 20px;">-->
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->hidden('UserCards.user_id', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'userId', 'type' => 'text', 'placeholder' => 'USERNAME']); ?>
                <?php echo $this->Form->hidden('UserCards.organize_id', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'organize_id', 'type' => 'text', 'label' => __('ไอดีใช้งาน'), 'placeholder' => 'USERNAME', 'required']); ?>
                <?php echo $this->Form->input('UserCards.organize_code', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'organize_code', 'type' => 'text', 'label' => __('Company Code'), 'placeholder' => 'Company Code', 'required']); ?>
                <label id = "nameorg_th" style="color: green"></label>
                <label id = "noorg_th" style="color: red"></label>
            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->input('UserCards.employee', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'id' => 'employee', 'type' => 'text', 'label' => __('รหัสพนักงาน / รหัสนักศึกษา'), 'placeholder' => 'รหัสพนักงาน / รหัสนักศึกษา', 'disabled', 'required']); ?>
            </div>
        </div>
        <div class="form-group has-feedback bootstrap-iso">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label class="label-text-sub"><?php echo __('วันเกิด');?></label>
                <input type="text" name="UserCards[birthdate]" id="date" placeholder="กรุณาระบุวันเดือนปีเกิด" value="" placeholder="DD-MM-YYYY" data-date-format="mm/dd/yyyy" class="form-control-reg border-bottom-from" disabled>
                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-quaternary mr-xs mb-sm button-text-profile">ตรวจสอบ</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script>
    var organize_id = document.getElementById("organize_id");
    var employee = document.getElementById("employee");
    var date = document.getElementById("date");
    $(document).ready(function() {
        $("#organize_code").on('change',function(){
            var organize_code = document.getElementById("organize_code").value;
            $.post("/UserCards/checkOrg", {organize_code: organize_code}, function(data) {
                if(data!='false'){
                    data = jQuery.parseJSON(data);
                    console.log(data);
                    if(data['chkuser'] == true){
                        $("#noorg_th").text('ลงทะเบียนกับ '+data[0]['org_name_th']+' แล้ว');
                        $("#nameorg_th").text('');
                        $('#employee').attr("disabled",true);
                        $('#date').attr("disabled",true);
                        organize_id.value = '';
                        employee.value = '';
                        date.value = '';
                    }else if(data['chkuser'] == false){
                        $("#nameorg_th").text(data[0]['org_name_th']);
                        $("#noorg_th").text('');
                        $("#employee").removeAttr("disabled");
                        $("#date").removeAttr("disabled");
                        organize_id.value = data[0]['id'];
                    }
                }else{
                    $("#noorg_th").text('ไม่พบข้อมูล');
                    $("#nameorg_th").text('');
                    $('#employee').attr("disabled",true);
                    $('#date').attr("disabled",true);
                    organize_id.value = '';
                    employee.value = '';
                    date.value = '';
                }
            });
        });
    });
    function check() {
        if (employee.value == '') {
            alert("รหัสพนักงาน / รหัสนักศึกษา");
            employee.focus();
            return false;
        }
        if (date.value == '') {
            alert("วันเกิด");
            return false;
            date.focus();
        }
    }

    $(document).ready(function(){
      var date_input=$('input[id="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>