<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserCard $userCard
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userCard->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userCard->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Cards'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userCards form large-9 medium-8 columns content">
    <?= $this->Form->create($userCard) ?>
    <fieldset>
        <legend><?= __('Edit User Card') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('organize_id');
            echo $this->Form->control('card_code');
            echo $this->Form->control('img_path');
            echo $this->Form->control('prefix_name_th');
            echo $this->Form->control('firstname_th');
            echo $this->Form->control('lastname_th');
            echo $this->Form->control('prefix_name_en');
            echo $this->Form->control('firstname_en');
            echo $this->Form->control('lastname_en');
            echo $this->Form->control('department_name');
            echo $this->Form->control('section_name');
            echo $this->Form->control('position_name');
            echo $this->Form->control('gender');
            echo $this->Form->control('blood_group');
            echo $this->Form->control('birthdate', ['empty' => true]);
            echo $this->Form->control('date_issued', ['empty' => true]);
            echo $this->Form->control('date_expiry', ['empty' => true]);
            echo $this->Form->control('signature');
            echo $this->Form->control('is_used');
            echo $this->Form->control('created_by');
            echo $this->Form->control('modified_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
