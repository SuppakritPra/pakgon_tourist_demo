<div class="row">
    <div class="col-md-12">
    <?php echo $this->Form->create('subject',['url' => ['controller' => 'UserCards', 'action' => 'add'],'enctype' => 'multipart/form-data', 'id' => 'form-upload', 'type' => 'file']);?>
        <div style="padding: 5px;">
            <div class="form-group has-feedback">
                <div class="col-md-12">
                    <label class="label-text-sub"><?php echo __('Organize');?><em>* </em></label>
                    <?php 
                        echo $this->Form->input( 'Organize_id', array(
                        'name'=>'Organize_id',
                        'id'=>'Organize_id',
                        'label' => false,
                        'type' => 'select',
                        'options' => $organizeId,
                        'empty' => '---Select---',
                        'default' => '',
                        'class' => 'form-control border-bottom-from label-text-sub required',
                        'style' => 'width:100%'
                        )); 
                    ?>
                </div>
            </div>
        </div>
        <?php
            echo $this->Form->input('file_upload', array(
                'id' => 'fileupload',
                'type' => 'file',
                'label' => false,
                #'default' => !empty($data['after_start_time'])? $data['after_start_time']:'',
                'class' => 'form-control input-sm file_upload',
                #'required' => 'true'
            ));	
        ?>
        <br>
        <div class="col-md-12">
            <?php
                echo $this->Form->submit(__('Add'), ['label' => false, 'name' => 'submit', 'class' => 'btn btn-warning confirmModal', 'alt' => 'Submit', 'error' => false]);
            ?>
        </div>
        <?php 
            echo $this->Form->end(); 
            echo $this->Form->create(false, ['url' => ['controller'=> 'UserCards','action' => 'save'],'id'=>'DateSelect']);
            if(!empty($dataExcels)){ //pr($dataExcels);die;
        ?>
            <div class="col-md-12">
                <div class="content animate-panel">
                    <div class="groups form hpanel hblue">
                        <div class="panel-body" style="overflow: scroll;">
                            <table class="table-bordered table-striped">
                                <input type="hidden"
                                    id="str_var"
                                    name="dataExcel"
                                    value="<?php echo base64_encode(serialize($dataExcels)) ?>"
                                >
                                <input type="hidden"
                                    name="orgId"
                                    value="<?php echo $orgId ?>"
                                >
                                <?php
                                $i = 0; 
                                    foreach ($dataExcels as $key => $value){ 
                                    if($key == 1){?>
                                        <thead>
                                            <!-- <tr>
                                                <td style="max-width: 500px"><?php echo __('ลำดับทั้งหมด');?></td>                                            
                                            </tr> -->
                                            <tr>
                                                <th style="width:100px"><?php echo ('ลำดับ'); ?></th>
                                                <th style="width:200px">
                                                    <?php
                                                        echo $this->Form->input('Checkall',[
                                                            'id' => 'checkAll',
                                                            'type' => 'checkbox',
                                                            'label' => false
                                                        ])
                                                    ?><br>
                                                    <?php echo __('checkAll');?>
                                                </th>
                                                <th style="width:200px"><?php echo $value['A']; ?></th>
                                                <th style="width:400px"><?php echo $value['B']; ?></th>
                                                <th style="width:300px"><?php echo $value['C']; ?></th>
                                                <th style="width:500px"><?php echo $value['D']; ?></th>
                                                <th style="width:500px"><?php echo $value['E']; ?></th>
                                                <th style="width:100px"><?php echo $value['F']; ?></th>
                                                <th style="width:200px"><?php echo $value['G']; ?></th>
                                                <th style="width:200px"><?php echo $value['H']; ?></th>
                                                <th style="width:100px"><?php echo $value['I']; ?></th>
                                                <th style="width:100px"><?php echo $value['J']; ?></th>
                                                <th style="width:200px"><?php echo $value['K']; ?></th>
                                                <th style="width:200px"><?php echo $value['L']; ?></th>
                                                <th style="width:200px"><?php echo $value['M']; ?></th>
                                                <th style="width:100px"><?php echo $value['N']; ?></th>
                                                <th style="width:100px"><?php echo $value['O']; ?></th>
                                                <th style="width:100px"><?php echo $value['P']; ?></th>
                                                <th style="width:300px"><?php echo $value['Q']; ?></th>
                                                <th style="width:400px"><?php echo $value['R']; ?></th>
                                            </tr>
                                        </thead>
                                    <?php } else{
                                        $checkEmpty =  array_filter($value);
                                        if(!empty($checkEmpty)){
                                            $style = '';
                                            $disabled = '';
                                            $textAlert = '';
                                            if(empty($value['A'])||(empty($value['N']))){
                                                $style = 'style="background-color: yellow;"';
                                                $disabled = 'disabled';
                                                $textAlert = 'มีข้อมูลไม่ครบ';
                                            }else{
                                                if(in_array($value['A'],$usercards)){
                                                    $style = 'style="background-color: #ff0000;"';
                                                    $disabled = 'disabled';
                                                    $textAlert = 'มีข้อมูลอยู่แล้ว';
                                                }
                                            }
                                    ?>
                                        <tbody>
                                            <tr <?php echo $style ?>>
                                                <td> <?php $i++; echo $i; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        echo $this->Form->input('CheckCard[]',[
                                                            'class' => 'checkCard',
                                                            'type' => 'checkbox',
                                                            'label' => false,
                                                            'value' => $key,
                                                            $disabled
                                                        ]);
                                                        echo "<br>".$textAlert;
                                                    ?>
                                                </td>
                                                <td><?php echo $value['A']; ?></td>
                                                <td><?php echo $value['B']; ?></td>
                                                <td><?php echo $value['C']; ?></td>
                                                <td><?php echo $value['D']; ?></td>
                                                <td><?php echo $value['E']; ?></td>
                                                <td><?php echo $value['F']; ?></td>
                                                <td><?php echo $value['G']; ?></td>
                                                <td><?php echo $value['H']; ?></td>
                                                <td><?php echo $value['I']; ?></td>
                                                <td><?php echo $value['J']; ?></td>
                                                <td><?php echo $value['K']; ?></td>
                                                <td><?php echo $value['L']; ?></td>
                                                <td><?php echo $value['M']; ?></td>
                                                <td><?php echo $value['N']; ?></td>
                                                <td><?php echo $value['O']; ?></td>
                                                <td><?php echo $value['P']; ?></td>
                                                <td><?php echo $value['Q']; ?></td>
                                                <td><?php echo $value['R']; ?></td>
                                            </tr>
                                        </tbody>
                                    <?php }}?>
                                <?php  }?>
                            </table>
                        </div>
                        <div style=" clear: both;"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group pull-right">
                            <?php echo $this->Form->submit(__('Save'), [
                                'label' => false, 
                                'name' => 'submit', 
                                'class' => 'btn btn-primary confirmModal',
                                'data-confirm-modal' => __('Are you sure for save this Import ?'),
                                'alt' => 'Submit', 
                                'error' => false, 
                                'icon' => 'fa-save'
                            ]);
                            echo $this->Form->button(__('Cancel'), ['class' => 'btn btn-default', 'icon' => 'fa-close']);?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php echo $this->Form->end();?> 
    </div>
</div>

<style>

    th{
        text-align: center;
        color:#000000;
        background-color:#ebebe0;
    }

    td{
        text-align: center;
        color:#000000;
    }
    table{
        max-width:auto !important;
        width:auto !important;
    }
    #checkAll{
        /* margin-left: 1px; */
    }
    .checkCard{
        /* margin-left: 1px; */
    }

</style>

<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        $(document).on('click','#checkAll',function () {
          //  alert ('assasasaaa');
           $(".checkCard").not(this).prop('checked', this.checked);
        });
    });

</script>
<?php $this->end();?>