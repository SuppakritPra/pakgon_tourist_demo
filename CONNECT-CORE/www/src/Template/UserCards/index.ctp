<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserCard[]|\Cake\Collection\CollectionInterface $userCards
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Card'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userCards index large-9 medium-8 columns content">
    <h3><?= __('User Cards') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('organize_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('card_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('img_path') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prefix_name_th') ?></th>
                <th scope="col"><?= $this->Paginator->sort('firstname_th') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lastname_th') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prefix_name_en') ?></th>
                <th scope="col"><?= $this->Paginator->sort('firstname_en') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lastname_en') ?></th>
                <th scope="col"><?= $this->Paginator->sort('department_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('section_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('position_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gender') ?></th>
                <th scope="col"><?= $this->Paginator->sort('blood_group') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birthdate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_issued') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_expiry') ?></th>
                <th scope="col"><?= $this->Paginator->sort('signature') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_used') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userCards as $userCard): ?>
            <tr>
                <td><?= $this->Number->format($userCard->id) ?></td>
                <td><?= $userCard->has('user') ? $this->Html->link($userCard->user->username, ['controller' => 'Users', 'action' => 'view', $userCard->user->id]) : '' ?></td>
                <td><?= $this->Number->format($userCard->organize_id) ?></td>
                <td><?= h($userCard->card_code) ?></td>
                <td><?= h($userCard->img_path) ?></td>
                <td><?= h($userCard->prefix_name_th) ?></td>
                <td><?= h($userCard->firstname_th) ?></td>
                <td><?= h($userCard->lastname_th) ?></td>
                <td><?= h($userCard->prefix_name_en) ?></td>
                <td><?= h($userCard->firstname_en) ?></td>
                <td><?= h($userCard->lastname_en) ?></td>
                <td><?= h($userCard->department_name) ?></td>
                <td><?= h($userCard->section_name) ?></td>
                <td><?= h($userCard->position_name) ?></td>
                <td><?= h($userCard->gender) ?></td>
                <td><?= h($userCard->blood_group) ?></td>
                <td><?= h($userCard->birthdate) ?></td>
                <td><?= h($userCard->date_issued) ?></td>
                <td><?= h($userCard->date_expiry) ?></td>
                <td><?= h($userCard->signature) ?></td>
                <td><?= h($userCard->is_used) ?></td>
                <td><?= $this->Number->format($userCard->created_by) ?></td>
                <td><?= h($userCard->created) ?></td>
                <td><?= $this->Number->format($userCard->modified_by) ?></td>
                <td><?= h($userCard->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userCard->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userCard->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userCard->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userCard->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
