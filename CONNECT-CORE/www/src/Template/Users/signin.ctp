<?php $this->layout = 'PakgonConnect.mobile'; ?>
<?php echo $this->element('common/topContent'); ?>
<div class="row">
    <div class="col">
        <form action="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'verify']); ?>" method="post" id="frmUserLogin">
            <div class="row">
                <div class="col">
                    <div class="form-group has-feedback">
                        <input class="form-control form-control-lg required username" maxlength="14" autocomplete="new-user" placeholder="<?php echo 'username'; ?>" name="data[username]" id='txtUsername'>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control form-control-lg xckpassword required" autocomplete="new-password" placeholder="<?php echo __('********'); ?>" name="data[password]" id='txtPassword'>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <?php echo $this->Permission->submit(__('Login'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block', 'id' => 'btnLogin']); ?>
                </div>
            </div>

            <br>
            <div class="row">
                <?php echo '<div class="col"><h4>' . $this->Html->link(__('Forgot Password'), ['controller' => 'Users', 'action' => 'forgotPassword'], ['class' => 'text-body text-bold']) . '</h4></div>'; ?>
                <?php echo '<div class="col"><h4>' . $this->Html->link(__('Register'), ['controller' => 'Users', 'action' => 'signup'], ['class' => 'text-body text-bold pull-right']) . '</h4></div>'; ?>
            </div>
        </form>
    </div>
</div>

<?php $this->append('scriptBottom');?>
<script>
$("#frmUserLogin").validate({
  messages: {
    'data[username]': {
        required: '<?php echo __("Please enter username.") ?>'
    },
    'data[password]': '<?php echo __("Please enter password.") ?>'
  }
});
</script>

<?php $this->end();?>
