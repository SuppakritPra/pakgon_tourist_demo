<?php $this->layout = 'PakgonConnect.mobile'; ?>    
    <div style="padding-top: 20px;">
        <a href="/users/signin">
            <?php echo $this->Html->image('/img/core/img/back-icon@3x.png', ['style' => 'float: left;padding-top: 5px;']); ?>
        </a>
    </div>
    <?php echo $this->element('common/topContent', ['title' => __('Registration')]); ?>
    <?php echo $this->Form->create($user,['id'=>'register']); ?>
    <?php echo $this->Form->control('username', ['class' => 'form-control-lg required username','maxlength'=>14 ,  'label' => ['class' => 'pakgon-label'], 'rangelength' => '6,14', 'autocomplete' => 'new-user']); ?>
    <?php echo $this->Form->control('password', ['name'=>'password','class' => 'form-control-lg required ckpassword', 'type' => 'password', 'label' => ['class' => 'pakgon-label'], 'autocomplete' => 'new-password']); ?>
    <?php echo $this->Form->control('confirm_password', ['class' => 'form-control-lg required', 'type' => 'password', 'equalto' => '#password', 'label' => ['class' => 'pakgon-label'], 'autocomplete' => 'new-confirm-password']); ?>
    <?php echo $this->Form->control('firstname', ['class' => 'form-control-lg required letters', 'maxlength' => 64, 'label' => ['class' => 'pakgon-label']]); ?>
    <?php echo $this->Form->control('lastname', ['class' => 'form-control-lg required letters', 'maxlength' => 64, 'label' => ['class' => 'pakgon-label']]); ?>
    <?php echo $this->Form->control('email', ['class' => 'form-control-lg required email', 'type' => 'email', 'label' => ['class' => 'pakgon-label']]); ?>
    <?php echo $this->Form->control('phone_no', ['class' => 'form-control-lg required digits', 'maxlength' => 10, 'length' => '10', 'label' => ['class' => 'pakgon-label']]); ?>
    <?php //------teeradone---------- ?>
    <label class = 'pakgon-label'><?php echo __('Citizen Province')?><span class='red-star'>*</span></label>
    <?php //------------------------- ?>
    <!-- 'label' => ['class' => 'pakgon-label', 'text' => __('Citizen Province')] -->
    <?php echo $this->Form->control('master_province_id', ['id'=>'master_province_id','name'=>'master_province_id','class' => 'form-control-lg required', 'options' => $this->Utility->findMasterProvinceList(['conditions' => ['master_country_id' => 1]]), 'label' => false]); ?>
    <div class="row">
        <div class="col">
            <?php echo $this->Permission->submit(__('Registration'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block','id'=>'btn-registration']); ?>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
    <br>
    <div id="backdrop"></div>


<script type="text/javascript">
    $(function () {
        $("#password,#confirm-password").val("");
    });
</script>

<?php $this->append('scriptBottom');?>
    <script>
        var chkSubmit;

        $("#master_province_id").change(function(){
            $(this).valid()
        });

        $("#register").validate({
            messages: {
                'password': {
                    ckpassword: '<?php echo __("Passwords must be at least 6 characters long consisting of numbers and letters.") ?>'
                }
            }
        });
        $("#btn-registration").on("click", function() {
          
            if($("#register").valid()==true){            
                if(chkSubmit==undefined){
                    chkSubmit = true;
                    $("#backdrop").addClass('backdrop');                  
                }else{  
                    return false;
                }
            } 
           
        });
        
    </script>
    <style>
    .select2-container--bootstrap4 .select2-selection--single {
        height: calc(2.25rem + 10px);
        line-height: 1.9 !important;
    }

    .backdrop{
        position: fixed;
        z-index:99999 !important;
        background-color:#fff;
        opacity: 0.1 !important; 
        width:100%;
        height:100%;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }
    </style>
<?php $this->end();?>