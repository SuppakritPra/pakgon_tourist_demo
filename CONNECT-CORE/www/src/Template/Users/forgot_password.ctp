<?php $this->layout = 'PakgonConnect.mobile'; ?>
<div class="row" style="padding-top: 20px;">
    <div class="col">
	<a href="/users/signin">
            <?php echo $this->Html->image('/img/core/img/back-icon@3x.png', ['style' => 'float: left;padding-top: 5px;']); ?>
        </a>
	</div>
</div>
<?php echo $this->element('common/topContent', ['title' => __('Forgot Password')]); ?>
    <?php echo $this->Form->create(); ?>
    <div class="row">
        <div class="col">
		 <?php echo $this->Form->control('email', ['class' => 'form-control-lg required email', 'type' => 'email', 'label' => false, 'placeholder' => __('mail@youmail.com')]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <?php echo $this->Permission->submit(__('Get "Password"'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block', 'escap' => false]); ?>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>

    <br>
<div class="row text-center">
    <div class="col">
    <h3><?php //echo __('Contact : {0}', '02 xxx - xxxx'); ?></h3>
    </div>
</div>