<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserName $userName
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Name'), ['action' => 'edit', $userName->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Name'), ['action' => 'delete', $userName->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userName->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Names'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Name'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Faculties'), ['controller' => 'Faculties', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Faculty'), ['controller' => 'Faculties', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Name Prefixes'), ['controller' => 'NamePrefixes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Name Prefix'), ['controller' => 'NamePrefixes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userNames view large-9 medium-8 columns content">
    <h3><?= h($userName->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Faculty') ?></th>
            <td><?= $userName->has('faculty') ? $this->Html->link($userName->faculty->name, ['controller' => 'Faculties', 'action' => 'view', $userName->faculty->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $userName->has('role') ? $this->Html->link($userName->role->name, ['controller' => 'Roles', 'action' => 'view', $userName->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ref Code') ?></th>
            <td><?= h($userName->ref_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($userName->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($userName->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Prefix') ?></th>
            <td><?= $userName->has('name_prefix') ? $this->Html->link($userName->name_prefix->name, ['controller' => 'NamePrefixes', 'action' => 'view', $userName->name_prefix->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($userName->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($userName->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($userName->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Office Phone') ?></th>
            <td><?= h($userName->office_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile Phone') ?></th>
            <td><?= h($userName->mobile_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($userName->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Moo') ?></th>
            <td><?= h($userName->moo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Road') ?></th>
            <td><?= h($userName->road) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sub District') ?></th>
            <td><?= h($userName->sub_district) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('District') ?></th>
            <td><?= h($userName->district) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Province') ?></th>
            <td><?= h($userName->province) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Zipcode') ?></th>
            <td><?= h($userName->zipcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($userName->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Picture Path') ?></th>
            <td><?= h($userName->picture_path) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userName->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Birth Date') ?></th>
            <td><?= h($userName->birth_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userName->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userName->modified) ?></td>
        </tr>
    </table>
</div>
