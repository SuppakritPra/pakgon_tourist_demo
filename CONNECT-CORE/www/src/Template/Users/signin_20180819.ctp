<?php $this->layout = 'PakgonConnect.mobile'; ?>
<?php echo $this->element('common/topContent'); ?>
<div class="row">
    <div class="col">
        <form action="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'verify']); ?>" method="post" id="frmUserLogin">
            <div class="row">
                <div class="col">
                    <div class="form-group has-feedback">
                        <input class="form-control form-control-lg required" placeholder="<?php echo __('Username'); ?>" name="data[username]" id='txtUsername'>
                        <!-- <i class="fa fa-user form-control-feedback"></i> -->
                    </div>
                </div>
            </div>
            <?php echo $this->Form->control('username', ['name' => 'data[username]', 'class' => 'form-control-lg required letters', 'maxlength' => '14', 'label' => ['class' => 'pakgon-label'], 'rangelength' => '6,14', 'autocomplete' => 'new-user']); ?>
            <div class="row">
                <div class="col">
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control form-control-lg required" placeholder="<?php echo __('********'); ?>" name="data[password]" id='txtPassword'>
                        <!-- <i class="fa fa-lock form-control-feedback"></i> -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <?php echo $this->Permission->submit(__('Login'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block', 'id' => 'btnLogin']); ?>
                </div>
            </div>

            <br>
            <div class="row">
                <?php echo '<div class="col"><h4>' . $this->Html->link(__('Forgot Password'), ['controller' => 'Users', 'action' => 'forgotPassword'], ['class' => 'text-body text-bold']) . '</h4></div>'; ?>
                <?php echo '<div class="col"><h4>' . $this->Html->link(__('Register'), ['controller' => 'Users', 'action' => 'signup'], ['class' => 'text-body text-bold pull-right']) . '</h4></div>'; ?>
            </div>
        </form>
    </div>
</div>