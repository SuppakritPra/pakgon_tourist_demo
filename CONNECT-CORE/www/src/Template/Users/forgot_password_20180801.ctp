<div class="row">
    <div class="center">
        <div class="title_header"><a href="javascript:history.go(-1)"><?php echo $this->Html->image('/img/core/img/back-icon@3x.png', ['style' => 'float: left;padding-left: 15px;padding-top: 5px;']); ?></a><?php echo __('Forgot password');?></div>
    </div>
    <div class="col-md-12">

	<?php echo $this->Form->create(null, ['url' => ['controller' => 'Users','action' => 'forgotPassword'],'class' => 'form-horizontal','id' => '','name' => 'forgotPressword','role' => 'form', 'onsubmit' => 'return validateForm()']);?>
        <div style="padding: 5px;">
            <?php echo $this->Flash->render(); ?>  
        </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub"><?php echo __('Email');?><em>* </em></label>
                        <input type="text" value="" name="email" placeholder="mail@xxx.com" class="form-control-reg border-bottom-from" id="fremail">
                    </div>
                </div>
            </div>
            <div>
                <?php
                    echo $this->Form->submit(__('submit'), array('div' => false, 'class' => 'btn btn-quaternary mr-xs mb-sm button-text',
                    'name' => 'btn', 'id' => '', 'title' => 'Title'));
                ?>
            </div>
           
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- ------------------------------- popup ----------------------------------- -->
<script>
    function validateForm() {
        var email = document.forms["forgotPressword"]["email"].value;
        if (email == null || email == "") {
            alert("<?php echo __('Please enter your email.');?>");
            return false;
        }
            // Validate Email
            var email = $("#fremail").val();
            if ((/(.+)@(.+){2,}\.(.+){2,}/.test(email)) || email=="" || email==null) { } else {
                alert("<?php echo __('Email address format wrong Please enter the correct.');?>");
                return false;
            }
    }
</script>

<style>
    #alertBox {
        position:relative;
        width:300px;
        min-height:100px;
        /*margin-top:50px;*/
        border:1px solid #666;
        background-color:#fff;
        background-repeat:no-repeat;
        background-position:20px 30px;
        left: 0;
        right: 0;
        top: 100px;
        z-index: 2;
    }

    #modalContainer > #alertBox {
        position:fixed;
    }

    #alertBox h1 {
        margin:0;
        font:bold 0.9em verdana,arial;
        background-color:#000;
        color:#FFF;
        border-bottom:1px solid #000;
        padding:2px 0 2px 5px;
    }

    #alertBox p {
        font-size: 14px;
        height: 30px;
        padding: 10px;
        /* margin-left: 55px; */
        text-align: center;
    }

    #alertBox #closeBtn {
        display:block;
        position:relative;
        margin:5px auto;
        padding:7px;
        border:0 none;
        width:70px;
        font:0.7em verdana,arial;
        text-transform:uppercase;
        text-align:center;
        color:#FFF;
        background-color:#000;
        border-radius: 3px;
        text-decoration:none;
    }

    /* unrelated styles */

    #mContainer {
        position:relative;
        width:600px;
        margin:auto;
        padding:5px;
        border-top:2px solid #000;
        border-bottom:2px solid #000;
        font:0.7em verdana,arial;
    }

    h1,h2 {
        margin:0;
        padding:4px;
        font:bold 1.5em verdana;
        border-bottom:1px solid #000;
        text-align: center;
    }

    code {
        font-size:1.2em;
        color:#069;
    }

    #credits {
        position:relative;
        margin:25px auto 0px auto;
        width:350px; 
        font:0.7em verdana;
        border-top:1px solid #000;
        border-bottom:1px solid #000;
        height:90px;
        padding-top:4px;
    }

    #credits img {
        float:left;
        margin:5px 10px 5px 0px;
        border:1px solid #000000;
        width:80px;
        height:79px;
    }

    .important {
        background-color:#F5FCC8;
        padding:2px;
    }

    code span {
        color:green;
    }
</style>

<script>
    var ALERT_TITLE = "";
    var ALERT_BUTTON_TEXT = "Ok";

    if (document.getElementById) {
        window.alert = function (txt) {
            createCustomAlert(txt);
        }
    }
    function createCustomAlert(txt) {
        d = document;

        if (d.getElementById("modalContainer"))
            return;

        mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";

        alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if (d.all && !window.opera)
            alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
        alertObj.style.visiblity = "visible";

        h1 = alertObj.appendChild(d.createElement("h1"));
        h1.appendChild(d.createTextNode(ALERT_TITLE));

        msg = alertObj.appendChild(d.createElement("p"));
        //msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;

        btn = alertObj.appendChild(d.createElement("a"));
        btn.id = "closeBtn";
        btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
        btn.href = "#";
        btn.focus();
        btn.onclick = function () {
            removeCustomAlert();
            return false;
        }

        alertObj.style.display = "block";

    }

    function removeCustomAlert() {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    }
    function ful() {
        alert('Alert this pages');
    }
</script>


<!--------------------------------- date ------------------------------------->
<script>
    $(document).ready(function(){
      var date_input=$('input[id="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>

