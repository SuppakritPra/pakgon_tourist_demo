<?php $this->layout = 'mobile'; ?>
    <div class="col-12 text-center">
        <div class="title_header" style="padding-top: 20px;">
            <a href="javascript:history.go(-1)"><?php echo $this->Html->image('/img/core/img/back-icon@3x.png', ['style' => 'float: left;padding-left: 15px;padding-top: 5px;']); ?></a>
            <?php echo $this->element('common/topContent', ['title' => __('change password')]); ?>
        </div>
    </div>
    <?php echo $this->Form->create(null, ['url' => ['controller' => 'Users','action' => 'changeForgotpassword/'.$token],'class' => 'form-horizontal','id' => '','name' => 'changeForgotpassword','role' => 'form', 'onsubmit' => 'return validateForm()']);?>
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Form->control('password', ['class' => 'form-control-lg required ckpassword', 'type' => 'password', 'label' => ['class' => 'pakgon-label', 'text' => __('new password')], 'autocomplete' => 'new-password', 'id' => 'password', 'name' => 'password']); ?>
    <?php echo $this->Form->control('confirm_password', ['class' => 'form-control-lg required ckpassword', 'equalTo' => '#password', 'type' => 'password', 'label' => ['class' => 'pakgon-label'], 'autocomplete' => 'new-password']); ?>
<div class="row">
    <div class="col">
        <?php echo $this->Permission->submit(__('submit'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block']); ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>
