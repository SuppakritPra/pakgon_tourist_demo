<?php $this->layout = 'PakgonConnect.mobile'; ?>
<div class="row">
    <!-- <div class="center">
        <div class="title_header" style="padding-top: 20px;">
            <a href="javascript:history.go(-1)">
                <?php //echo $this->Html->image('/img/core/img/back-icon@3x.png', ['style' => 'float: left;padding-left: 15px;padding-top: 5px;']); ?>
            </a>
            <br>
        </div>
    </div> -->

    <div class="col-12">
        <?php echo $this->element('common/topContent', ['title' => __('change password')]); ?>
        <?php echo $this->Form->create(null, ['url' => ['controller' => 'Users','action' => 'changePassword/'],'class' => 'form-horizontal','id' => 'changePass','name' => 'changePressword','role' => 'form']);?>
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->Form->control('oldpassword', ['class' => 'form-control-lg required', 'label' => ['class' => 'pakgon-label', 'text' => __('OldPassword')], 'type' => 'password', 'name'=> 'oldpassword', 'placeholder' => '********']); ?>
        <?php echo $this->Form->control('password', ['class' => 'form-control-lg required', 'label' => ['class' => 'pakgon-label', 'text' => __('new password')], 'type' => 'password', 'name'=> 'password', 'placeholder' => '********']); ?>
        <?php echo $this->Form->control('confirm_password', ['class' => 'form-control-lg required', 'label' => ['class' => 'pakgon-label', 'text' => __('Confirm Password')], 'equalto' => '#password', 'type' => 'password', 'name'=> 'confirm_password', 'id' => 'confirm_password', 'placeholder' => '********']); ?>

    <div class="row">
        <div class="col">
            <?php echo $this->Permission->submit(__('SAVE'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block']); ?>
        </div>
    </div>
           
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<?php $this->append('scriptBottom');?>
<script>
$("#changePass").validate({
  messages: {
    'confirm_password': {
        equalTo: "โปรดระบุรหัสผ่านให้ตรงกัน"
    }
  }
});
</script>
<?php $this->end();?>