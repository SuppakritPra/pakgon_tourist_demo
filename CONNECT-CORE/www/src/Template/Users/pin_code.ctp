<?php $this->layout = 'mobile'; ?>
<div class="text-center">
    <br>
    <div class="row">
        <div class="col-12">
            <div class="title_header"><h2><?php echo __('Verify PIN'); ?></div></h2>
        </div>
    </div>
    <div style="padding-top: 20px;"></div>

    <div class="row">
        <div class="col-12">
            <?php echo $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'pinCode/' . $token], 'id' => 'PinCode', 'name' => 'PinCode', 'role' => 'form']); ?>
            <?php echo $this->Flash->render(); ?>

            <div class="form-group has-feedback">
                <div class="col-12 text-center">
                    <?php echo __('4 digit PIN in the email address specified for access'); ?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <div class="col-12 text-center">  
                    <div class="data-pin">
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                        <?php for ($i = 1; $i <= 4; $i++) { ?>
                            <input type="password" pattern="[0-9]*" maxlength="1" min="0" name="pin_code_<?php echo $i; ?>" id="pin_code_<?php echo $i; ?>" onkeyup="verifyPinCode('pin_code')">
                        <?php } ?>
                    </div> 
                </div>
            </div>

            <div class="form-group has-feedback">
                <div class="col-12 text-center">
                    <?php echo $this->Html->link(__('Re-send pin code'), ['controller' => 'Users', 'action' => 'registersendpin/' . $token]); ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>


<!---------------------------------------------------------------------->
<style>
    input[type="number"], input[type="password"] {
        width: 50px;
        height: 50px;
        margin-right: 5px;
        margin-bottom: 0;
        text-align: center;
        /* font-size: 81px;
     padding: 0px 0px 30px 0px;*/
    }
</style>

<script>
    function verifyPinCode(obj) {

        if ($('#' + obj + '_1').val() != '' && $('#' + obj + '_2').val() != '' && $('#' + obj + '_3').val() != '' && $('#' + obj + '_4').val() != '') {
            $("#PinCode").submit();
        } else {
            if ($('#' + obj + '_1').val() != '')
                $('#' + obj + '_2').focus();
            if ($('#' + obj + '_2').val() != '')
                $('#' + obj + '_3').focus();
            if ($('#' + obj + '_3').val() != '')
                $('#' + obj + '_4').focus();
            return false;
        }

    }
</script>
