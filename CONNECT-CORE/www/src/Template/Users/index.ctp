<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserName[]|\Cake\Collection\CollectionInterface $userNames
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Name'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Faculties'), ['controller' => 'Faculties', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Faculty'), ['controller' => 'Faculties', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Name Prefixes'), ['controller' => 'NamePrefixes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Name Prefix'), ['controller' => 'NamePrefixes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userNames index large-9 medium-8 columns content">
    <h3><?= __('User Names') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('faculty_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ref_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_prefix_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('office_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birth_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('moo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('road') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sub_district') ?></th>
                <th scope="col"><?= $this->Paginator->sort('district') ?></th>
                <th scope="col"><?= $this->Paginator->sort('province') ?></th>
                <th scope="col"><?= $this->Paginator->sort('zipcode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('picture_path') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userNames as $userName): ?>
            <tr>
                <td><?= $this->Number->format($userName->id) ?></td>
                <td><?= $userName->has('faculty') ? $this->Html->link($userName->faculty->name, ['controller' => 'Faculties', 'action' => 'view', $userName->faculty->id]) : '' ?></td>
                <td><?= $userName->has('role') ? $this->Html->link($userName->role->name, ['controller' => 'Roles', 'action' => 'view', $userName->role->id]) : '' ?></td>
                <td><?= h($userName->ref_code) ?></td>
                <td><?= h($userName->username) ?></td>
                <td><?= h($userName->password) ?></td>
                <td><?= $userName->has('name_prefix') ? $this->Html->link($userName->name_prefix->name, ['controller' => 'NamePrefixes', 'action' => 'view', $userName->name_prefix->id]) : '' ?></td>
                <td><?= h($userName->first_name) ?></td>
                <td><?= h($userName->last_name) ?></td>
                <td><?= h($userName->email) ?></td>
                <td><?= h($userName->office_phone) ?></td>
                <td><?= h($userName->mobile_phone) ?></td>
                <td><?= h($userName->birth_date) ?></td>
                <td><?= h($userName->address) ?></td>
                <td><?= h($userName->moo) ?></td>
                <td><?= h($userName->road) ?></td>
                <td><?= h($userName->sub_district) ?></td>
                <td><?= h($userName->district) ?></td>
                <td><?= h($userName->province) ?></td>
                <td><?= h($userName->zipcode) ?></td>
                <td><?= h($userName->status) ?></td>
                <td><?= h($userName->picture_path) ?></td>
                <td><?= h($userName->created) ?></td>
                <td><?= h($userName->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userName->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userName->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userName->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userName->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
