<div class="se-pre-con"></div>
<div class="row">
    <div class="row3">
        <?php echo $this->Html->image('/img/core/img/banner-connect1@3x.png',array('style' => 'width:100%'), ['alt' => '']); ?>
    </div>
    <div class="row3">
        <?php echo $this->Html->image('/img/core/img/banner-store-connect@3x.png',array('style' => 'width:100%'), ['alt' => '']); ?>
    </div>
    <div class="col-md-12">
        <div class="text-header center"><?php echo __('หมวดหมู่'); ?></div>
    </div>
    <!-------------------------------------->
    <div id="owl-demo" class="owl-carousel owl-theme">
        <?php
        foreach ($Categories as $key => $home_categories):
            if (is_array($home_categories)) {
                foreach ($home_categories as $key2 => $view3):
                    foreach ($view3 as $key3 => $categories):
                    //debug($categories);exit();
                        ?>
        <a href="../core/Homes/catalogue/<?php echo $categories['categories_id']; ?>">
                        <div class="item">
                            <img src="<?php echo $categories['cat_img_path']; ?>" style="-webkit-border-radius: 50px;-moz-border-radius: 50px; border-radius: 50px;" alt="Owl Image">
                            <div class="center">
                                <div class="text-cat"><?php echo $categories['cat_name_th']; ?></div>
                              </div>
                        </div>
        </a>
                    <?php endforeach; ?> 
                <?php endforeach; ?> 
            <?php } ?>
        <?php endforeach; ?>
<!--  <div class="item">
  <img src="img/core/img/tourism-icon@3x.png" alt="Owl Image">
      <div class="center">
            <div class="text-cat"><?php echo __('การท่องเที่ยว'); ?></div>
        </div>
  </div>
    <div class="item">
  <img src="img/core/img/finance-icon@3x.png" alt="Owl Image">
      <div class="center">
            <div class="text-cat"><?php echo __('การเงิน'); ?></div>
        </div>
  </div>
    <div class="item">
  <img src="img/core/img/office-icon@3x.png" alt="Owl Image">
      <div class="center">
            <div class="text-cat"><?php echo __('ออฟฟิต'); ?></div>
        </div>
  </div>-->
    
</div>
 
    <style>
        .text-header{
            font-size:19px;color: #1c1c1c; padding: 10px 0px 0px 0px;
        }
        .text-cat{
            font-size:17px;color: #8d8d8d;
        }
    </style>
    
  
    <div style="border-bottom: 3px solid #eee;padding: 5px;"></div> 
    <style type="text/css">
        #owl-demo .item{
 /* background: #3fbf79;*/
  padding: 3px 0px;
  margin: 4px;
  color: #FFF;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  text-align: center;
}

    </style>
    <script>
$(document).ready(function() {
 
  var owl = $("#owl-demo");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,4], // betweem 900px and 601px
      //itemsTablet: [600,4], //2 items between 600 and 0
      itemsTablet: [500,4], //2 items between 600 and 0
     // itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  });
 
 
 
});
    </script>
    
    <!-------------------------------------->
    <div class="col-md-12">
<!--        <div class="center">-->
            <div class="col-md-12">
                <div class="text-header center"><?php echo __('ข่าวสารใหม่ประจำวัน'); ?></div>
            </div>
            <div class="tab-pane active" id="popularPosts">
                <ul class="simple-post-list">
                    <li>
                        <div class="row">
                            <?php
                            foreach ($response as $key => $new_home):
                                if (is_array($new_home)) {
                                    foreach ($new_home as $key1 => $view2):
                                        foreach ($view2 as $key2 => $view):
                                            ?>
                                            <div class="col-xs-6">
                                                <a href="<?php echo $view['url'];?>">
                                                    <img src="<?php echo $view['img_path']; ?>" style="width: 100%;/*height: 122px;*/ -webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px;"/>
                                                    <?php //echo $this->Html->image('../img/core/img/thumbs-New1@3x.png', ['style' => 'width:100%']);?>					
                                                    <div class="promotion-title" style=" text-align: left;padding-top: 8px;"><?php echo $view['article_title']; ?></div>
                                                    <div class="promotion-des" style="text-align: left;"><?php echo iconv_substr($view['article_detail'], 0, 25, "UTF-8") . " ..."; ?><?php //echo $view['article_detail'];  ?></div>
                                                </a>
                                            </div>
                                        <?php endforeach; ?> 
                                    <?php endforeach; ?> 
                                <?php } ?>
                            <?php endforeach; ?>

                        </div>
                    </li>
                </ul>
            </div>
            <style>
                .border-text{text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}
            </style>

            <div style="border-radius: 0 0 4px 4px;
                 box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.04);
                 background-color: #FFF;
                 border: 1px solid #EEE;
                 border-top: 0;"></div>

<!--        </div>-->
    </div>
</div>
        <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(img/loader.gif) center no-repeat #fff;
        }
        </style>
        <script>
                $(window).load(function() {
                        $(".se-pre-con").fadeOut("slow");;
                });
        </script>