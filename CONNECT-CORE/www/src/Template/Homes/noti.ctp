<div class="se-pre-con"></div>
<div class="row">
    <!--    <div class="center">
            <h2>การแจ้งเตือน</h2>
        </div>-->
    <div class="col-md-12">
        <ul class="list list-icons list-primary list-borders">
            <?php
            foreach ($response as $key => $noti_show):
                if (is_array($noti_show)) {
                foreach ($noti_show as $key1 => $view_show):
                    foreach ($view_show as $key2 => $view):
                        ?>
                        <li class="promotion-li" style="height: 75px;">
                            <a href="../Homes/promotion/<?php echo $view['master_notification_type_id']; ?>">
                                <div class="images-left" style="padding-right: 5px;">
                                    <img src="<?php echo $view['icon_img_path']; ?>" width="60" height="60"/>
                                </div>
                                <div class="promotion-title">
                                    <?php echo $view['master_notification_type_translations_name']; ?> 
                                    <?php if ($view['is_new_flge'] === 'Y') { ?>
                                        <i class="fa fa-circle" style=" color: #FF0000;"></i>
                                    <?php } else { ?>
                                        <?php echo __(''); ?>
                                    <?php } ?>
                                    
                                </div>
                                <div class="promotion-des">
                                    <?php echo $view['master_notification_type_translations_name']; ?></div>
                                <span class="glyphicon glyphicon-menu-right form-control-feedback icon-menu-right"></span>
                            </a>
                        </li>
                    <?php endforeach; ?> 
                <?php endforeach; ?> 
                <?php }?>
            <?php endforeach; ?> 
                        <div style="margin-top: 9px; margin-bottom: 0; padding-bottom: 9px;"></div>
        </ul>
    </div>
</div>
        <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(img/loader.gif) center no-repeat #fff;
        }
        </style>
        <script>
                $(window).load(function() {
                        $(".se-pre-con").fadeOut("slow");;
                });
        </script>

