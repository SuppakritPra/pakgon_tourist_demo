<div class="row">
    <div class="center">
        <h2>การแจ้งเตือน</h2>
    </div>
    <div class="col-md-12">

                    <ul class="list list-icons list-primary list-borders">
                        
                            <li class="promotion-li" style="height: 75px;">
                                <a href="../Homes/promotion">
                                    <div class="images-left">
                                        <img src="../porto/core/img/promotion-icon@3x.png" width="70" height="70"/>
                                    </div>
                                    <div class="promotion-title">
                                        โปรโมชั่น <i class="fa fa-circle" style=" color: #FF0000;"></i>
                                    </div>
                                    <div class="promotion-des">
                                        ช้อปสินค้าคอลเลคชั่น พิเศษเฉพาะคุณ</div>
                                    <span class="glyphicon glyphicon-menu-right form-control-feedback icon-menu-right"></span>
                                </a>
                            </li>
                            <li class="promotion-li" style="height: 75px;">
                                <a href="../Homes/contact">
                                    <div class="images-left">
                                        <img src="../porto/core/img/Update-user-icon@3x.png" width="70" height="70"/>
                                    </div>
                                    <div class="promotion-title">
                                        อัพเดทผู้ใช้งาน <i class="fa fa-circle" style=" color: #FF0000;"></i>
                                    </div>
                                    <div class="promotion-des">
                                        อัพเดตรูปโปรไฟลใหม่</div>
                                    <span class="glyphicon glyphicon-menu-right form-control-feedback icon-menu-right"></span>
                                </a>
                            </li>
                            <li class="promotion-li" style="height: 75px;">
                                <a href="../Homes/promotion">
                                    <div class="images-left">
                                        <img src="../porto/core/img/Update-list-icon@3x.png" width="70" height="70"/></div>
                                    <div class="promotion-title">
                                        รายการอัพเดตจาก Connect
                                    </div>
                                    <div class="promotion-des">
                                        ปรับฟีเจอร์ใหม่ๆให้ใช้งานสะดวกขึ้น</div>
                                    <span class="glyphicon glyphicon-menu-right form-control-feedback icon-menu-right"></span>
                                </a>
                            </li>
                            
                    </ul>
    </div>
</div>


