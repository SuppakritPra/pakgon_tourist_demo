<div class="se-pre-con"></div>
<div class="row">
    <div class="center">
        <h2>รายชื่อติดต่อ</h2>
    </div>
    <div class="col-md-12">

        <ul class="list list-icons list-primary list-borders">
            <?php
            foreach ($response as $key => $contact_show):
                if (is_array($contact_show)) {
                    foreach ($contact_show as $key1 => $view_contact):
                        foreach ($view_contact as $key2 => $view2):
                            foreach ($view2 as $key3 => $view):
                                ?>
                                <li class="promotion-li" style="height: 85px;">
                                    <a href="<?php echo !empty($view['moblie_no'])? 'tel:'.$view['moblie_no']:'#';?>" class="text-color-dark text-decoration-none" target="_blank">
                                        <div class="images-left" style="padding-right: 5px;">
                                            <img src="<?php echo $view['img_path']; ?>" width="75" height="75"/>
                                        </div>
                                        <div class="promotion-title">
                                            <?php echo $view['name']; ?>
                                        </div>

                                        <div class="promotion-date">
                                            <?php echo $view['department'] ?>
                                        </div>
					<?php if(!empty($view['moblie_no'])){?>
                                        	<span class="glyphicon glyphicon-earphone form-control-feedback icon-earphone"></span>
					<?php }?>
                                    </a>
                                </li>
                               

                            <?php endforeach; ?> 
                        <?php endforeach; ?> 
                    <?php endforeach; ?> 
                <?php } ?>
            <?php endforeach; ?>
            <div style="margin-top: 9px; margin-bottom: 0; padding-bottom: 9px;"></div>
        </ul>
	
    </div>
</div>
        <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(img/loader.gif) center no-repeat #fff;
        }
        </style>
        <script>
                $(window).load(function() {
                        $(".se-pre-con").fadeOut("slow");;
                });
        </script>

