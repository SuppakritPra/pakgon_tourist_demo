<div class="se-pre-con"></div>
<div class="row">
    <!--    <div class="center">
            <h2>โปรโมชัน</h2>
        </div>-->
    <div class="col-md-12">

        <ul class="list list-icons list-primary list-borders">
           <?php
            foreach ($response as $key => $promotion_show):
                if (is_array($promotion_show)) {
                    foreach ($promotion_show as $key1 => $view_promotion):
                        foreach ($view_promotion as $key2 => $view):
                            ?>
                            <li class="promotion-li">
				<a href="http://<?php echo $view['url'];?>" data-noti-new = <?php echo $view['is_new_flge']; ?> data-noti-id = <?php echo $view['notification_id'];?> >
                                    <div class="images-left" style="padding: 5px;">
                                        <img src="<?php echo $view['icon_img_path']; ?>" width="68" height="68" style="margin-bottom: 10px;"/>
                                    </div>
                                    <div class="promotion-title" style="width: 70%;">
                                        <?php echo $view['topic']; ?> 
                                        <?php if ($view['is_new_flge'] === 'Y') { ?>
                                        <i class="fa fa-circle" style=" color: #FF0000;"></i>
                                    <?php } else { ?>
                                        <?php echo __(''); ?>
                                    <?php }?>
                                    </div>
                                    <div class="promotion-des">
                                        <?php echo iconv_substr($view['short_description'], 0, 50, "UTF-8") . " ..."; ?></div>
                                    <div class="promotion-date" style="/*margin: 0% 0% 0% 27%;*/">
                                        <?php echo iconv_substr($view['created'], 0, 10, "UTF-8"); ?>
                                    </div>
                                </a>
                            </li>
                            <?php endforeach; ?> 
                    <?php endforeach; ?> 
                <?php } ?>
            <?php endforeach; ?>
           <div style="margin-top: 9px; margin-bottom: 0; padding-bottom: 9px;"></div>
        </ul>
    </div>
</div>
        <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(img/loader.gif) center no-repeat #fff;
        }
        </style>
        <script>
                $(window).load(function() {
                        $(".se-pre-con").fadeOut("slow");;
                });
		
		$(document).on('click','.promotion-li a',function () {
			var element = $(this);
			var noti_new = element.data('noti-new');
			if(noti_new ==='Y'){ 
				var goto = element.attr('href');
				var noti_id = element.data('noti-id'); 
				var updateNoti = 'http://connect06.pakgon.com/communication/api/update-read-status/'+noti_id;
				$.post(updateNoti).done(function(result) {				
					element.data('noti-new','N');
					element.find('i').remove();			 	
					if(goto !== ''){				
						location.href = goto;
					}
				});
				return false;		 
			 }//.if(noti_new==='Y')
		});
        </script>

