<div class="se-pre-con"></div>
<div class="row">
        <div class="center">
            <h2><?php echo __('รายชื่อติดต่อ');?></h2>
        </div>
    <div class="col-md-12">
        <ul class="list list-icons list-primary list-borders">
            <?php if(!empty($response)) {?>
            <?php
            foreach ($response as $key => $contact_show):
                if (is_array($contact_show)) {
                    foreach ($contact_show as $key1 => $view_contact):
                        foreach ($view_contact as $key2 => $view2):
                            foreach ($view2 as $key3 => $view):
                                ?>
                                <?php if (!empty($view['moblie_no'])) { ?>
                                <li class="promotion-li" style="height: 85px;">
                                    <a href="tel:<?php echo $view['moblie_no']; ?>" class="text-color-dark text-decoration-none" target="_blank">
                                        <div class="images-left" style="padding-right: 5px;">
                                            <?php if(!empty($view['img_path'])) {?>
                                            <img src="<?php echo $view['img_path']; ?>" width="75" height="75"/>
                                            <?php }else{?>
                                            <?php echo $this->Html->image('/img/core/img/user-profile@3x.png', array('div' => false, 'class' => '','id'=>'','style'=>'width: 75px;height: 75px;-webkit-border-radius: 85px;
    -moz-border-radius: 85px;
    border-radius: 85px;')); ?>
                                            <?php }?>
                                        </div>
                                        <div class="promotion-title">
                                            <?php echo $view['name']; ?>
                                        </div>

                                        <div class="promotion-date">
                                            <?php echo $view['department'] ?>
                                        </div>

                                        <span class="glyphicon glyphicon-earphone form-control-feedback icon-earphone"></span>
                                    </a>
                                </li>
                                <?php  }else{ ?>
                                <li class="promotion-li" style="height: 85px;">
                                    <a href="#" class="text-color-dark text-decoration-none" target="_blank">
                                        <div class="images-left" style="padding-right: 5px;">
                                            <?php if(!empty($view['img_path'])) {?>
                                            <img src="<?php echo $view['img_path']; ?>" width="75" height="75"/>
                                             <?php }else{?>
                                            <?php echo $this->Html->image('/img/core/img/user-profile@3x.png', array('div' => false, 'class' => '','id'=>'','style'=>'width: 75px;height: 75px;-webkit-border-radius: 85px;
    -moz-border-radius: 85px;
    border-radius: 85px;')); ?>
                                            <?php }?>
                                        </div>
                                        <div class="promotion-title">
                                            <?php echo $view['name']; ?>
                                        </div>

                                        <div class="promotion-date">
                                            <?php echo $view['department'] ?>
                                        </div>
                                    </a>
                                </li>
                                <?php }?>
                                
                            <?php endforeach; ?> 
                        <?php endforeach; ?> 
                    <?php endforeach; ?> 
                <?php } ?>
            <?php endforeach; ?>
                                <div style="margin-top: 9px; margin-bottom: 0; padding-bottom: 9px;"></div>
            <?php }?>                   
        </ul>
    </div>
</div>
        <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(img/loader.gif) center no-repeat #fff;
        }
        </style>
        <script>
                $(window).load(function() {
                        $(".se-pre-con").fadeOut("slow");;
                });
        </script>

