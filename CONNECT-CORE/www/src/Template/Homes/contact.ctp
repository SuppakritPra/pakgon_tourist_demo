<?php $this->layout = 'PakgonConnect.mobile'; ?>

<br/>
<div class="row">
    <div class="col">
        <h2 class="text-center text-bold"><?php echo __('รายชื่อติดต่อ'); ?></h2>
    </div>
</div>

<div class="row">
    <div class="col">


        <ul class="list-group list-group-flush">

            <?php if (!empty($response)): ?>
                <?php
                foreach ($response as $key => $contact_show):
                    if (is_array($contact_show)) {
                        foreach ($contact_show as $key1 => $view_contact):
                            foreach ($view_contact as $key2 => $view2):
                                foreach ($view2 as $key3 => $view):
                                    ?>
                                    <?php if (!empty($view['moblie_no'])) { ?>
                                        <li class="list-group-item">
                                            <a href="tel:<?php echo $view['moblie_no']; ?>" class="text-secondary" target="_blank">
                                                <div class="images-left p-r-5">
                                                    <?php if (!empty($view['img_path'])) { ?>
                                                        <img src="<?php echo $view['img_path']; ?>" width="75" height="75"/>
                                                    <?php } else { ?>
                                                        <?php echo $this->Html->image('/img/core/img/user-profile@3x.png', array('div' => false, 'class' => '', 'id' => '', 'style' => 'width: 75px;height: 75px;-webkit-border-radius: 85px; -moz-border-radius: 85px; border-radius: 85px;')); ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="promotion-title">
                                                    <?php echo $view['name']; ?>
                                                </div>

                                                <div class="promotion-date">
                                                    <?php echo $view['department'] ?>
                                                </div>
                                                <span class="glyphicon glyphicon-earphone form-control-feedback icon-earphone"></span>
                                            </a>
                                        </li>
                                    <?php } else { ?>
                                        <li class="list-group-item">
                                            <a href="#" class="text-secondary" target="_blank">
                                                <div class="images-left p-r-5">
                                                    <?php if (!empty($view['img_path'])) { ?>
                                                        <img src="<?php echo $view['img_path']; ?>" width="75" height="75"/>
                                                    <?php } else { ?>
                                                        <?php echo $this->Html->image('/img/core/img/user-profile@3x.png', array('div' => false, 'class' => '', 'id' => '', 'style' => 'width: 75px;height: 75px;-webkit-border-radius: 85px; -moz-border-radius: 85px; border-radius: 85px;')); ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="promotion-title">
                                                    <?php echo $view['name']; ?>
                                                </div>

                                                <div class="promotion-date">
                                                    <?php echo $view['department'] ?>
                                                </div>
                                            </a>
                                        </li>
                                    <?php } ?>
                                <?php endforeach; ?> 
                            <?php endforeach; ?> 
                        <?php endforeach; ?> 
                    <?php } ?>
                <?php endforeach; ?>

            <?php else: ?>
                <li class="list-group-item"><h6 class="text-bold text-body"><?php echo __('Not found your contact.'); ?></h6></li>
                <?php endif; ?>
        </ul>
    </div>
</div>

