<?php #pr($Profiles);?>
<div class="row">
    <div style="position: absolute;z-index: 2;">
        <a href="../Profiles/index"><?php echo $this->Html->image('/img/core/img/setting-icon@3x.png', ['alt' => 'setting-icon']); ?></a>
        <!--<a href="settingProfile"><img src="../webroot/porto/core/img/setting-icon@3x.png"/></a>-->
    </div>
    
    
  
<!--<input type="file" accept="image/*" onchange="loadFile(event)" style="border: solid;">-->

<script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
</script>  
    
    
    
    
    <div class="box">
        <div class="absolute-right">
            <!--<img src="../webroot/porto/core/img/location-profile-icon@3x.png"/>-->
            <?php echo $this->Html->image('/img/core/img/location-profile-icon@3x.png', ['alt' => 'location-profile-icon']); ?> สีลม ,บางรัก</div>
    </div>
    <div style="text-align: center;">
        <?php if (empty($Profiles['UserProfiles'][0]['img_path'])) { ?>
        <?php echo $this->Html->image('/img/core/img/user-profile@3x.png', array('div' => false, 'class' => 'profile-img','id'=>'output','style'=>'width: 169px;height: 169px;-webkit-border-radius: 85px;
    -moz-border-radius: 85px;
    border-radius: 85px;')); ?>
        <?php  }else{ ?>
        <img src="<?php echo $Profiles['UserProfiles']['img_path'];?>" class="profile-img" id="output" style="width: 169px;height: 169px;-webkit-border-radius: 85px;-moz-border-radius: 85px;border-radius: 85px;"/>
        <?php //echo $this->Html->image('/img/core/img/profile-img@3x.png', array('div' => false, 'class' => 'profile-img','id'=>'output','style'=>'width: 169px;height: 169px;-webkit-border-radius: 85px;-moz-border-radius: 85px;border-radius: 85px;')); ?>
        <?php }?>
        
        
        

        
        
        
        
<?php //echo $this->Html->image('/img/core/img/profile-img@3x.png', array('type' => 'file','accept' => 'image/*','onchange' => 'loadFile(event)','div' => false, 'class' => 'profile-img','id'=>'output','style'=>'width: 169px;height: 169px;-webkit-border-radius: 85px;-moz-border-radius: 85px;border-radius: 85px;')); ?>
        <?php echo $this->Html->image('/img/core/img/profile-img-edit@3x.png', array('style' => 'top: 130px;', 'div' => false, 'class' => 'profile-img')); ?>
        <input id="upload" type="file"/>
        <script>
        $(function(){
$("#upload_link").on('click', function(e){
    e.preventDefault();
    $("#upload:hidden").trigger('click');
});
});
        </script>
        <style type="text/css">
            #upload_link{
                text-decoration:none;
            }
            #upload{
                display:none
            }
        </style>
        <a href="#" id="upload_link" onchange="loadFile(event)" style="color: #fff; font-size: 16px; position: absolute; margin: 136px 0px 0px -10px;"><?php echo __('แก้ไข');?></a>
        <!--<input type="file" accept="image/*" onchange="loadFile(event)" style="border: solid;">-->
        
        
        
    	<?php
	//$img_path = $Profiles['UserProfiles']['education']['img_path'];
	//if(!empty($Profiles['UserProfiles']['work']['img_path'])) $img_path = $Profiles['UserProfiles']['work']['img_path'];
	?>
        <!--<img src="<?php echo $img_path?>" class="profile-img"/>-->
    </div>
    <div style="text-align: center;">
        <?php echo $this->Html->image('/img/core/img/bg-cover-button@3x.png', array('div' => false, 'class' => 'id-card-button','style'=>'width:100%;')); ?>
        <!--<img src="../webroot/porto/core/img/bg-cover-button@3x.png" class="id-card-button"/>-->
    </div>
    <div class="bg-profile"></div>
    <div class="col-md-12" style="padding-top: 20px;">

<?php echo $this->Form->create(null, ['url' => ['controller' => 'Profiles', 'action' => 'updateProfile'],
    'id' => '', 'name' => 'Profiles', 'role' => 'form', 'onsubmit' => 'return validateForm()']);
?>
<?php //echo $this->Form->create(null, ['url' => ['controller' => 'Profiles','action' => 'updateProfile'],'class' => 'form-horizontal','id' => 'frmSignIn','name' => 'updateProfile','role' => 'form', 'OnSubmit' => 'return validateForm()']);?>
        <!--<form action="updateProfile" class="form-horizontal" id="frmSignIn" method="post">-->
            <input type="hidden" name="data[Users][id]" value="<?php echo $Profiles['Users']['id']?>">
	    <input type="hidden" name="data[UserPersonals][id]" value="<?php echo $Profiles['UserPersonals']['id']?>">
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">ชื่อผู้ใช้งาน</label>			
                        <input type="text" name="data[Users][username]" value="<?php echo $Profiles['Users']['username']?>" placeholder="USERNAME" class="form-control-reg border-bottom-from" readonly>
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">เพศ</label>
                        <div class="radio-group">
                                <label class="radio-inline">
                                        <input type="radio" name="data[UserPersonals][gender]" value="M" <?php if($Profiles['UserPersonals']['gender']=='M') echo 'checked';?>> ชาย
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="data[UserPersonals][gender]" value="F" <?php if($Profiles['UserPersonals']['gender']=='F') echo 'checked';?>> หญิง
                                </label>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">ชื่อ - นามสกุล</label>
                        <input type="text" name="data[UserPersonals][name]" value="<?php echo $Profiles['UserPersonals']['firstname_th']?> <?php echo $Profiles['UserPersonals']['lastname_th']?> " placeholder="" class="form-control-reg border-bottom-from">

                    </div>
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub"><?php echo __('กรุณาระบุวันเดือนปีเกิด');?></label>
                        <input type="text" name="data[UserPersonals][birthdate]" value="<?php echo date('d-m-Y',strtotime($Profiles['UserPersonals']['birthdate']));?>" id="date" placeholder="กรุณาระบุวันเดือนปีเกิด" value="" placeholder="DD-MM-YYYY" data-date-format="mm/dd/yyyy" class="form-control-reg border-bottom-from">
                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                    </div>
<!--                    <div class="col-md-12">
                        <label class="label-text-sub">กรุณาระบุวันเดือนปีเกิด</label>
                        <input type="text" name="data[UserPersonals][birthdate]" value="<?php echo date('d-m-Y',strtotime($Profiles['UserPersonals']['birthdate']));?>" placeholder="dd-mm-yy" class="form-control-reg border-bottom-from">
                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                    </div>-->
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">กรุ๊ปเลือด</label>
                        <div class="radio-group">
                                <label class="radio-inline">
                                        <input type="radio" name="data[UserPersonals][blood_group]" value="A" <?php if($Profiles['UserPersonals']['blood_group']=='A') echo 'checked';?>> เอ
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="data[UserPersonals][blood_group]" value="B"  <?php if($Profiles['UserPersonals']['blood_group']=='B') echo 'checked';?>> บี
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="data[UserPersonals][blood_group]" value="AB"  <?php if($Profiles['UserPersonals']['blood_group']=='AB') echo 'checked';?>> เอบี
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="data[UserPersonals][blood_group]" value="O"  <?php if($Profiles['UserPersonals']['blood_group']=='O') echo 'checked';?>> โอ
                                </label>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">เบอร์โทรศัพท์</label>
                        <input type="text" name="data[UserProfiles][education][phone_no]" value="<?php echo $Profiles['UserProfiles']['education']['phone_no']?>" placeholder="000-000-0000" class="form-control-reg border-bottom-from" maxlength="12">

                    </div>
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">Email</label>
                        <input type="text" name="data[UserPersonals][email]" value="<?php echo $Profiles['UserPersonals']['email']?>" placeholder="mail@xxx.com" class="form-control-reg border-bottom-from">

                    </div>
                </div>
            </div>
            <div>
                <div class="form-group has-feedback">
                    <div class="col-md-12">
                        <label class="label-text-sub">ที่อยู่อาศัย</label>
                        <input type="text" name="data[UserProfiles][education][address]" value="<?php echo $Profiles['UserProfiles']['education']['address']?>" placeholder="" class="form-control-reg border-bottom-from">
                    </div>
                </div>
            </div>
            
           <!-------------------------------------------------->
	   
           <div class="row">
                    <div class="col-md-12">
                            <div class="toggle" data-plugin-toggle>
			    
                                    <input type="hidden" name="data[UserProfiles][education][id]" value="<?php echo $Profiles['UserProfiles']['education']['id']?>">
                                    <section class="toggle">
                                            <label>สถานภาพทางการศึกษา</label>
                                            <div class="toggle-content">
                                                <div>
                                                    <!------------------------Sub--------------------------->
                                                    <div class="col-md-12">
                                                    
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">รหัสนักศึกษา</label>
                                                            <input type="text" name="data[UserProfiles][education][card_code]" value="<?php echo $Profiles['UserProfiles']['education']['card_code']?>" placeholder="" class="form-control-reg border-bottom-from">

                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">มหาวิทยาลัย</label>
                                                            <div>
							    	<?php echo $this->Form->control('data.UserProfiles.education.organize_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
												'options' => $MasterOrganizationEducations,
												'default' => $Profiles['UserProfiles']['education']['organize_id'],
												'empty' => '--- Select ---',
												'label' => false
												]
												);
								?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                            <label class="label-text-sub">คณะ</label>
                                                            <div>
							    	<?php echo $this->Form->control('data.UserProfiles.education.dept_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
                                                    'options' => $MasterDepartments,
												'default' => $Profiles['UserProfiles']['education']['dept_id'],
												'empty' => '--- Select ---',
												'label' => false
												]
												);
								?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                            <label class="label-text-sub">สาขาวิชาการศึกษา</label>
                                                            <div>
							    	<?php echo $this->Form->control('data.UserProfiles.education.section_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
                                                    'options' => $MasterSections,
												'default' => $Profiles['UserProfiles']['education']['section_id'],
												'empty' => '--- Select ---',
												'label' => false
												]
												);												
								?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <!------------------------End Sub--------------------------->
                                                </div>
                                            </div>
                                    </section>
				    
				    
				    <input type="hidden" name="data[UserProfiles][work][id]" value="<?php echo !empty($Profiles['UserProfiles']['work']['id'])? $Profiles['UserProfiles']['work']['id']:''?>">
                                    <section class="toggle">
                                            <label>สถานภาพทางการทำงาน</label>
                                            <div class="toggle-content">
                                                <div>
                                                    <!------------------------Sub--------------------------->
                                                    <div class="col-md-12">
                                                    <div>
                                                        <div class="form-group has-feedback">
                                                            <div class="col-md-12">
                                                                <label class="label-text-sub">ประเภทงาน</label>
								<div>
								<?php foreach($MasterBusinessTypes as $key => $value){?>
                                                                        <label class="radio-inline">
                                                                                <input type="radio" name="data[UserProfiles][work][master_business_type_id]" value="<?php echo $key;?>" <?php if(!empty($Profiles['UserProfiles']['work']['master_business_type_id'])) if($key==$Profiles['UserProfiles']['work']['master_business_type_id']) echo 'checked';?>> <?php echo $value;?>
                                                                        </label>
								<?php }?>
								</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">ชื่อบริษัท/ห้างร้าน</label>
							    	<?php echo $this->Form->control('data.UserProfiles.work.organize_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
												'options' => $MasterOrganizationWorks,
												'default' => !empty($Profiles['UserProfiles']['work']['organize_id'])? $Profiles['UserProfiles']['work']['organize_id']:'',
												'empty' => '--- Select ---',
												'label' => false
												]
												);
								?>					    

                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">ที่อยู่ที่ทำงาน</label>
                                                            <input type="text" value="<?php echo !empty($Profiles['UserProfiles']['work']['address'])? $Profiles['UserProfiles']['work']['address']:''?>" placeholder="" class="form-control-reg border-bottom-from">

                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">เบอร์โทรศัพท์</label>
                                                            <input type="text" value="<?php echo !empty($Profiles['UserProfiles']['work']['phone_no'])? $Profiles['UserProfiles']['work']['phone_no']:''?>" placeholder="000-000-0000" class="form-control-reg border-bottom-from" maxlength="12">

                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">ตำแหน่ง</label>
                                                            <div>
 							    	<?php echo $this->Form->control('data.UserProfiles.work.master_organization_position_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
												'options' => $MasterOrganizationPositions,
												'default' => !empty($Profiles['UserProfiles']['work']['master_organization_position_id'])? $Profiles['UserProfiles']['work']['master_organization_position_id']:'',
												'empty' => '--- Select ---',
												'label' => false
												]
												);
								?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">ประเภทธุรกิจ</label>
                                                            <div>
							    	<?php echo $this->Form->control('data.UserProfiles.work.master_business_type_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
												'options' => $MasterBusinessTypes,
												'default' => !empty($Profiles['UserProfiles']['work']['master_business_type_id'])? $Profiles['UserProfiles']['work']['master_business_type_id']:'',
												'empty' => '--- Select ---',
												'label' => false
												]
												);
								?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="col-md-12">
                                                            <label class="label-text-sub">ฝ่าย/แผนก</label>
                                                            <div>
							    	<?php echo $this->Form->control('data.UserProfiles.work.section_id',
												[
												'type' => 'select',
												'class' => 'form-control select-from',
												'style' => 'width:100%;',
												'options' => $MasterSectionWorks,
												'default' => !empty($Profiles['UserProfiles']['work']['section_id'])? $Profiles['UserProfiles']['work']['section_id']:'',
												'empty' => '--- Select ---',
												'label' => false
												]
												);												
								?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <!------------------------End Sub--------------------------->
                                                </div>
                                            </div>
                                    </section>
                            </div>
                    </div>
            </div>
           <!-------------------------------------------------->
            
            <div>
                <?php
                    echo $this->Form->submit('บันทึก', array('div' => false, 'class' => 'btn btn-quaternary mr-xs mb-sm button-text-profile',
                    'name' => 'btn', 'id' => '', 'title' => 'Title'));
                ?>
                <!--<button type="submit" class="btn btn-quaternary mr-xs mb-sm button-text-profile">บันทึก</button>-->
            </div>
           <?php echo $this->Form->end(); ?>
        <!--</form>-->
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#moblieNo').inputmask('999-999-9999');
        $('#officePhone').inputmask('99-999-9999');
//==================================================
//        CHANGE UNIVERSITY PROFILES
//==================================================
        $("#OrganizetionId").change(function () {
            getDepartment();
        });
        $("#DepartmentId").change(function () {
            getSection();
        });
        $("#SectionId").change(function () {
            updateChosen();
        });
//==================================================
//        CHANGE COMPAMY PROFILES
//==================================================
        $("#companyName").change(function () {
            getCompanyDepartment();
        });
        $("#department").change(function () {
            getCompanySection();
        });
        $("#cottonId").change(function () {
            getCompanyPosition();
        });
        $(".datepicker").change(function () {
            updateChosen();
        });
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
//==================================================
//        CHANGE PROFILES PICTURE
//==================================================
        $("#uploadProfile").change(function () {
            readURL(this);
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>



<script>
    $(document).ready(function(){
      var date_input=$('input[id="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>

