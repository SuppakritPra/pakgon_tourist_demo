<div class="row">
    <!--
    <div style="position: absolute;z-index: 2;">
        <a href="settingProfile"><img src="webroot/porto/core/img/setting-icon@3x.png"/></a>
    </div>
    -->
    <div class="box">
        <div class="absolute-right"><img src="webroot/porto/core/img/location-profile-icon@3x.png"/> สีลม ,บางรัก</div>
    </div>
    <div style="text-align: center;">
        <img src="<?php echo !empty($Profiles['UserProfiles']['img_path'])? $Profiles['UserProfiles']['img_path']:'http://connect06.pakgon.com/core/webroot/img/user/user128x128.png'?>" class="profile-img"/>
    </div>
    <div data-toggle="modal" data-target="#defaultModal" style="text-align: center;">
        <a href="#"><img src="webroot/porto/core/img/id-card-button@3x.png" class="id-card-button"/></a>
    </div>
    <div class="bg-profile"></div>
    <div style="padding: 40px 20px 0px 20px;">
        <ul class="list list-icons list-primary list-borders">
            <li class="promotion-li">
                <a href="../Homes/promotion">
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 color-profile">
                            <!--<img src="webroot/porto/core/img/View-the-latest-icon@3x.png" width="53" height="53" style="vertical-align: middle;"/> -->
                            <?php echo __('ดูล่าสุด'); ?>
                        </div>
                        <div class="col-xs-6 col-sm-5 right-icon-view">
                            ที่เคยดูล่าสุด <img src="webroot/porto/core/img/next-icon-profile@3x.png" style="vertical-align: middle;"/>
                        </div>
                    </div>
                </a>
            </li>
            <li class="promotion-li">
                <a href="../Homes/promotion">
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 color-profile">
                            <!--<img src="webroot/porto/core/img/eWallet-icon@3x.png" width="53" height="53" style="vertical-align: middle;"/> -->
                            My eWallet by Connect
                        </div>
                        <div class="col-xs-6 col-sm-5 right-icon-view">
                            $150 <img src="webroot/porto/core/img/next-icon-profile@3x.png" style="vertical-align: middle;"/>
                        </div>
                    </div>
                </a>
            </li>
            <li class="promotion-li">
                <a href="../Homes/promotion">
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 color-profile">
                            <!--<img src="webroot/porto/core/img/point-icon@3x.png" width="53" height="53" style="vertical-align: middle;"/> -->
                            Point ของฉัน
                        </div>
                        <div class="col-xs-6 col-sm-5 right-icon-view">
                            P200 <img src="webroot/porto/core/img/next-icon-profile@3x.png" style="vertical-align: middle;"/>
                        </div>
                    </div>
                </a>
            </li>
            <li class="promotion-li">
                <a href="../Homes/promotion">
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 color-profile">
                            <!--<img src="webroot/porto/core/img/Transcript@3x.png" width="53" height="53" style="vertical-align: middle;"/> -->
                            ผลการศึกษา
                        </div>
                        <div class="col-xs-6 col-sm-5 right-icon-view">
                            <img src="webroot/porto/core/img/next-icon-profile@3x.png" style="vertical-align: middle;"/>
                        </div>
                    </div>
                </a>
            </li>
            <li class="promotion-li">
                <a href="../Homes/promotion">
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 color-profile">
                            <!--<img src="webroot/porto/core/img/Talent-icon@3x.png" width="53" height="53" style="vertical-align: middle;"/> -->
                            แบบทดสอบพรสวรรค์
                        </div>
                        <div class="col-xs-6 col-sm-5 right-icon-view">
                            <img src="webroot/porto/core/img/next-icon-profile@3x.png" style="vertical-align: middle;"/>
                        </div>
                    </div>
                </a>
            </li>
            <li class="promotion-li">
                <a href="../Homes/promotion">
                    <div class="row">
                        <div class="col-xs-6 col-sm-7 color-profile">
                            <!--<img src="webroot/porto/core/img/graph-icon@3x.png" width="53" height="53" style="vertical-align: middle;"/> -->
                            ผลกราฟพรสวรรค์
                        </div>
                        <div class="col-xs-6 col-sm-5 right-icon-view">
                            <img src="webroot/porto/core/img/next-icon-profile@3x.png" style="vertical-align: middle;"/>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>

<!--------------------------------------------------------->


<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                <div class="">
                        <div class="">
                            <div class="col-md-6">
                                    <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10}">
                                            <div>
                                                    <img alt="" class="img-responsive img-rounded" src="webroot/porto/core/img/crad01@3x.png">
                                            </div>
                                            <div>
                                                    <img alt="" class="img-responsive img-rounded" src="webroot/porto/core/img/crad02@3x.png">
                                            </div>
                                    </div>
                            </div>
                        </div>
                       
                </div>
        </div>
</div>
