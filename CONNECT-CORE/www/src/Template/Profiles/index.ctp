<?php
use Cake\I18n\Time;

    class QRGenerator { 

        protected $size; 
        protected $data; 
        protected $encoding; 
        protected $errorCorrectionLevel; 
        protected $marginInRows; 
        protected $debug; 

        public function __construct($data='1334554 5454 545',$size='300',$encoding='UTF-8',$errorCorrectionLevel='L',$marginInRows=4,$debug=false) { 

            $this->data=urlencode($data); 
            $this->size=($size>100 && $size<800)? $size : 300; 
            $this->encoding=($encoding == 'Shift_JIS' || $encoding == 'ISO-8859-1' || $encoding == 'UTF-8') ? $encoding : 'UTF-8'; 
            $this->errorCorrectionLevel=($errorCorrectionLevel == 'L' || $errorCorrectionLevel == 'M' || $errorCorrectionLevel == 'Q' || $errorCorrectionLevel == 'H') ?  $errorCorrectionLevel : 'L';
            $this->marginInRows=($marginInRows>0 && $marginInRows<10) ? $marginInRows:4; 
            $this->debug = ($debug==true)? true:false;     
        }
        public function generate(){ 

            $QRLink = "https://chart.googleapis.com/chart?cht=qr&chs=".$this->size."x".$this->size. "&chl=" . $this->data .  
                "&choe=" . $this->encoding . 
                "&chld=" . $this->errorCorrectionLevel . "|" . $this->marginInRows; 
            if ($this->debug) echo   $QRLink;          
            return $QRLink; 
        }
    }
?>
<?php $this->layout = 'PakgonConnect.mobile'; ?>
<br>
<div class="row">
    <div class="owl-carousel owl-theme">        
            <div class="col">
                <div class="row">
                    <div class="col-1 box-arrow left">   
                        <!-- <i class="fa fa-angle-left prev"></i>-->
                    </div><!--/.col-1 -->
                    <div class="col-10">
                        <div data-toggle="modal" data-target="#defaultModal">        
                            <img src="/img/core/img/connect-card-v3.png"  class="img-fluid" alt="connect-card">                
                        </div><!--/.modal -->
                    </div><!--/.modal -->
                    <div class="col-1 box-arrow right next">
                        <i class="fa fa-angle-right "></i>
                    </div><!--/.col-1 -->
                </div><!--/.row -->
                <br><br> 
                <div>
                    <div class="row text-center">
                        <div class="col">
                            <h2><?php echo __('user profile');?></h2>
                        </div><!--/.col -->
                    </div><!--/.row -->
                    <br>
                    <?php echo $this->Flash->render('profile') ?>
                    <?php echo $this->Form->create('UserProfiles', [ 'id' => 'frmSignIn','name' => 'frmSignIn','role' => 'form'  ]); ?>               
                    <?php echo $this->Form->control('Users.username', ['value' => $username,'class' => 'form-control-lg required username', 'label' => ['class' => 'pakgon-label'], 'rangelength' => '6,14','readonly'=>'readonly']); ?>
                    <?php echo $this->Form->control('firstname_th', ['value' => $responseUserPersonal['firstname_th'], 'class' => 'form-control-lg required letters' ,'label' => [ 'text' => __('firstname') ,'class' => 'pakgon-label']]); ?>
                    <?php echo $this->Form->control('lastname_th', ['value' => $responseUserPersonal['lastname_th'], 'class' => 'form-control-lg required letters', 'label' => ['text' => __('lastname') ,'class' => 'pakgon-label']]); ?>
                    <?php echo $this->Form->control('email', ['value' => $responseUserPersonal['email'],'class' => 'form-control-lg required email', 'type' => 'email', 'label' => ['text'=> __('email'),'class' => 'pakgon-label']]); ?>
                    <?php echo $this->Form->control('phone_no', ['value' => $responseUserPersonal['phone_no'],'class' => 'form-control-lg required digits', 'maxlength' => 10, 'length' => '10', 'label' => ['class' => 'pakgon-label']]); ?>
                    <label class="pakgon-label" for="master_province_id"><?php echo __('Citizen Province')?> <span class="red-star">*</span></label>
                    <?php echo $this->Form->control('master_province_id', ['value'=> $responseUserPersonal['master_province_id'], 'class' => 'input-lg required', 'options' => $this->Utility->findMasterProvinceList(['conditions' => ['master_country_id' => 1]]), 'label' => false ]); ?>
                    <div class="row">
                        <div class="col">
                            <?php echo $this->Permission->submit( __('SAVE'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block']); ?>
                        </div><!--/.col -->
                    </div><!--/.row -->           
                    <?php echo $this->Form->end(); ?>
                </div><!--/. -->
            </div><!-- /.col -->

            <?php 
            $i=0; 
            foreach ($UserCards as $value) { 
            ?>   
                <div class="item col">
                    <div class="row">
                        <div class="col-1 box-arrow left">  
                            <i class="fa fa-angle-left prev"> </i>
                        </div><!--/.col-1 -->
                        <div class="col-10">
                            <div class="box-card">
                                <div data-toggle="modal" data-target="#card_<?php echo $i;?>"  style="">
                                    <div class="row">  
                                        <div class="col-12">
                                            <div>
                                                <br>
                                                <label class="pakgon-label"><?php echo __('id');?><label>
                                            </div><!--/. -->
                                            <div>
                                                <?php echo $value['card_code'] ?>
                                            </div><!--/. -->
                                            <br>
                                            <div>
                                                <label class="pakgon-label"><?php echo __('Firstname') ?></label>
                                                <?php echo $value['prefix_name_th'].$value['firstname_th'].' '.$value['lastname_th'] ?>
                                            </div><!--/. -->
                                        </div><!--/.col-12 -->
                                        <div id='box-img-profile'>
                                            <img src="/img/core/img/user-profile@3x.png" class="img-fluid">
                                        </div><!--/#box-img-profile -->
                                    </div><!--/.row -->
                                    <div class="row">
                                        <div class="col-10">
                                            <label class="pakgon-label"><?php echo __('Position') ?></label>
                                            <?php echo $value['position_name'] ?>
                                        </div><!--/.col-10 -->
                                    </div><!--/.row -->
                                    <div class="row text-center">
                                        <br>
                                        <div class="col-6">
                                            <?php 
                                                $now = new Time($value['date_issued']);
                                                $dateIssued = $now->i18nFormat('yyyy-MM-dd');
                                                echo $this->DateFormat->formatDateThai($dateIssued);                  
                                            ?>
                                        </div><!--/.col-6 -->
                                        <div class="col-6">
                                            <?php 
                                                $now = new Time($value['date_expiry']);
                                                $dateExpiry = $now->i18nFormat('yyyy-MM-dd');
                                                echo $this->DateFormat->formatDateThai($dateExpiry);                  
                                            ?>
                                        </div><!--/.col-6 -->
                                    </div><!--/.row -->
                                    <div class="row text-center">
                                        <div class="col-6">
                                            <label class="pakgon-label"><?php echo __('Card issue') ?></label>
                                        </div><!--/.col-6 -->
                                        <div class="col-6">
                                            <label class="pakgon-label"><?php echo __('Expiry date') ?></label>
                                        </div><!--/.col-6 -->
                                    </div><!--/.row -->
                                </div><!--/modal -->
                            </div><!--/.box-card -->
                        </div><!--/.col-10 -->
                    
                        <div class="col-1 box-arrow right">
                            <i class="fa fa-angle-right next"></i>
                        </div><!--/.col-1 -->
                    </div><!--/.row -->
                        <br><br> 
                    <div> 
                        <div class="row text-center">
                            <div class="col">
                                <h2><?php echo __('Company Information');?></h2>
                            </div><!--/.col -->
                        </div><!--/.row -->
                        <br>
                        <div class="box-detial">
                            <div class="row">
                                <label  class="pakgon-label col-4"><?php echo __('Firstname') ?></label>
                                <div class="col-8"><?php echo $value['prefix_name_th'].$value['firstname_th'] ?></div><!--/.col-8 -->
                            </div><!--/.row -->
                            <div class="row">
                                <label  class="pakgon-label col-4"><?php echo __('Lastname') ?></label>
                                <div class="col-8"><?php echo $value['lastname_th'] ?></div><!--/.col-8 -->
                            </div><!--/.row -->
                            <div class="row"> 
                                <label  class="pakgon-label col-4 col-sm-4 col-md-4"><?php echo __('Company Name') ?></label>
                                <div class="col-8"><?php echo $value['morg']['org_name_th'] ?></div><!--/.col-8 -->  
                            </div><!--/.row -->
                            <div class="row">                           
                                <label  class="pakgon-label col-4"><?php echo __('Department') ?></label>
                                <div class="col-8"><?php echo $value['section_name'] ?></div>
                            </div><!--/.row -->
                            <div class="row">
                                    <label  class="pakgon-label col-4"><?php echo __('Position') ?></label>
                                    <div class="col-8"><?php echo $value['position_name'] ?></div><!--/.col-8 -->
                            </div><!--/.row -->
                            <div class="row">       
                                <label  class="pakgon-label col-4 col-sm-4 col-md-4"><?php echo __('Card issue') ?></label>
                                <div class="col-8"> 
                                    <?php 
                                        $now = new Time($value['date_issued']);
                                        $dateIssued = $now->i18nFormat('yyyy-MM-dd');
                                        echo $this->DateFormat->formatDateThai($dateIssued);                  
                                    ?>
                                </div><!--/.col-8 -->
                            </div><!--/.row -->
                            <div class="row">
                                <label  class="pakgon-label col-4 col-sm-4 col-md-4"><?php echo __('Expiry date') ?></label>
                                <div class="col-8">
                                    <?php 
                                        $now = new Time($value['date_expiry']);
                                        $dateExpiry = $now->i18nFormat('yyyy-MM-dd');
                                        echo $this->DateFormat->formatDateThai($dateExpiry);                  
                                    ?>
                                </div><!--/.col-12 -->
                            </div><!--/.frow -->
                        </div><!--/.box-detial -->
                    </div><!--/. -->
                </div><!--/.item -->

            <?php
                $i++; 
            } 
            ?>

        <?php  echo $this->element('card_from/from_add_card'); ?>
    </div><!--/.owl-carousel -->

        <div class="row">
            <div class="col" >
                <div class="modal" id="defaultModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content-card">
                            <a class="close-modal" data-dismiss="modal">X </a>  
                            <div class="modal-body card-connect">
                                <div class="owl-carousel owl-theme" >
                                    <div class="item"> 
                                        <img src="/img/core/img/connect-card-v2.png" class="img-fluid" alt="connect-card" >
                                    </div> 
                                    <div class="item">
                                        <div class="qrcode-profile text-center rotate270">
                                            <?php echo __('Show QR Code to get permission');?>
                                        </div><!--/.qrcode-profile -->
                                        <div class="qrcode-name">
                                            <?php
                                            $ex1 = new QRGenerator(); 
                                            echo "<img src=".$ex1->generate()." class='img-qrcode' style=' width: 150px;'>";
                                            ?>
                                        </div><!--/.qrcode-name -->
                                        <img src="/img/core/img/connect-card-back.png" class="img-fluid" alt="connect-card">   
                                    </div><!--/.item -->
                                </div><!--/.owl-carousel -->
                            </div><!--/.modal-body -->
                        </div><!--/.modal-content-card -->
                    </div><!--/.modal-dialog -->
                </div><!--/.modal-dialog -->
            </div><!--/.col -->
        </div><!--/.row -->

        <?php
            if(!empty($UserCards)){ 
                $i=0; 
                foreach ($UserCards as $value) {  
                    echo $this->element('card_from/from_0',array('value' => $value,'i'=> $i));
                    $i++; 
                } 
            }//.if(!empty($UserCards))
        ?>

    
</div><!--/.row -->

<?php $this->append('scriptBottom');?>
<script>
    $('.owl-carousel').owlCarousel({
        loop: false,
        items: 1,
        rewindNav: false,     
        
        startPosition: 'URLHash', 
    });
    owl = $('.owl-carousel').owlCarousel();
    $(".prev").click(function () {
        owl.trigger('prev.owl.carousel');
    });
    $(".next").click(function () {
         owl.trigger('next.owl.carousel');
    });

    $("#master_province_id").change(function(){
        $(this).valid()
    });
</script>
<?php $this->end();?>
