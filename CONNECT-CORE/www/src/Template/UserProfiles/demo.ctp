<!--<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
</style>

<div class="container">
    <div class="col-md-6">
        <div class="form-group">
            <label>Upload Image</label>
            <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                        Browse… <input type="file" id="imgInp">
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
            <img id='img-upload'/>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(document).on('change', '.btn-file :file', function () {
            var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function (event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                    log = label;

            if (input.length) {
                input.val(log);
            } else {
                if (log)
                    alert(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    });
</script>-->


<style>
    .full-width{
        float:left;width:100%;margin-top:30px;min-height:100px;position:relative;
    }
    .form-style-fake{position:absolute;top:0px;}
    .form-style-base{position:absolute;top:0px;z-index: 999;opacity: 0;}
    .imgCircle{border-radius: 50%;}
    .form-control{padding: 10px 50px;}
    .form-input{height:50px;border-radius: 0px;margin-top: 20px;}
    .Profile-input-file{
        height:180px;width:180px;left:33%;
        position: absolute;
        top: 0px;
        z-index: 999;
        opacity: 0 !important;
    }
    .mg-auto{
        margin:0 auto;max-width: 200px;overflow: hidden;
    }
    .fake-styled-btn{
        background: #006cad;
        padding: 10px;
        color: #fff;
    }
    #main-input{width:250px;}
    .input-place{
        position: absolute;top:35px;left: 20px;font-size:23px;color:gray;}
    .margin{margin-top:10px;margin-bottom:10px;}
    .truncate {
        width: 250px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .bg-form{
        float:left;width:100%;
        position:relative;
        /*background: url("http://lorempixel.com/200/200/abstract/");*/
        background-repeat: no-repeat;
        background-size: cover;
        margin-top: 0px;
    }
    .bg-transparent{
        background: rgba(0,0,0,0.5);float: left;
        width: 100%;margin-top: 0px;
    }
    .container{
        /*background: url("http://lorempixel.com/800/800/nature/");*/
        background-repeat: no-repeat;
        background-size: cover;
    }
    .custom-form{float: left;width:100%;border-radius: 20px;box-shadow: 0 0 16px #fff;overflow: hidden;
                 background: rgba(255,255,255,0.6);
    }
    .img-section{
        float: left;width: 100%;padding-top: 15px;padding-bottom: 15px;background: rgba(0,0,0,0.7);position: relative;
    }
    .img-section h4{color:#fff;}
    #PicUpload{
        color: #ffffff;
        width: 180px;
        height: 180px;
        background: rgba(255,255,255,0.4);
        padding: 100px;
        position: absolute;
        left: 30.5%;
        border-radius: 50%;
        display: none;
        top:15px;
    }
    .camera{
        font-size: 50px;
        color: #333;
    }
    .custom-btn{
        margin-top: 15px;
        border-radius: 0px;
        padding: 10px 60px;
        margin-bottom: 15px;
    }
    #checker{
        opacity: 0;
        position: absolute;
        top: 0px;
        cursor: pointer;
    }
    .color{
        color:#fff;
    }

    /*====== style for placeholder ========*/

    .form-control::-webkit-input-placeholder {
        color:lightgray;
        font-size:18px;
    }
    .form-control:-moz-placeholder {
        color:lightgray;
        font-size:18px;
    }
    .form-control::-moz-placeholder {
        color:lightgray;
        font-size:18px;
    }
    .form-control:-ms-input-placeholder {
        color:lightgray;
        font-size:18px;
    }
</style>

<div class="full-width">
    <h1 class="text-center color">Edit Profile Snippet</h1>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="custom-form">
            <div class="text-center bg-form">
                <div class="img-section">
                    <img src="http://lorempixel.com/200/200/nature/" class="imgCircle" id="img-upload" alt="Profile picture">
                </div>
                <input type="file" id="xxx" class="form-control form-input Profile-input-file" >
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#xxx").change(function () {
            readURL(this);
        });
    });
    function readURL(input) {
        console.log(input)
//        alert('xxxxxx');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>