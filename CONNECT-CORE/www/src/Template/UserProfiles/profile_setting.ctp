<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <?php echo $this->Form->create('UserProfiles', ['id' => 'frmSignIn', 'type' => 'file']); ?>
        <div style="position: absolute;z-index: 2;">
            <a href="profile_setting"><img src="/porto/core/img/setting-icon@3x.png"/></a>
        </div>
        <div class="box">
            <div class="absolute-right"><img src="/porto/core/img/location-profile-icon@3x.png"/> สีลม ,บางรัก</div>
        </div>
        <div style="text-align: center;">
            <!--<div class="">-->
            <?php // echo $this->Html->image($responseUserProfile['UserProfiles']['img_path'], ['class' => 'imgCircle profile-img', 'id' => 'img-upload', 'alt' => Profile picture']); ?>
            <img src="/porto/core/img/profile-img@3x.png" class="imgCircle profile-img" id="img-upload" alt="Profile picture">
            <!--</div>-->
            <input type="file" id="uploadProfile" class="profile-img form-control form-input Profile-input-file" name="data[UserProfiles][img_path]" >
        </div>
        <div class="bg-profile"></div>
        <!--<div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 20px;">-->
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->hidden('Users.user_id', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['Users']['id'], 'id' => 'userId', 'type' => 'text', 'label' => __('ชื่อผู้ใช้งาน'), 'placeholder' => 'USERNAME']); ?>
                <?php echo $this->Form->input('Users.username', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['Users']['username'], 'id' => 'userId', 'type' => 'text', 'label' => __('ชื่อผู้ใช้งาน'), 'placeholder' => 'USERNAME', 'readonly']); ?>
                <?php // echo $this->Form->input('user_id', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $userProfile->username, 'id' => 'userId', 'type' => 'text', 'label' => __('ชื่อผู้ใช้งาน'), 'placeholder' => 'USERNAME']); ?>
            </div>
        </div>
        <?php // foreach ($userProfile->user['user_personals'] as $k => $userProfiles): ?>
        <?php // debug($userProfiles); exit;?>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <!--<label class="label-text-sub">เพศ</label>-->
                <?php echo __('เพศ'); ?>
                <div class="radio-group">
                    <label class="radio-inline">
                        <input type="radio" name="UserPersonals[gender]" id="male" value="M" <?php echo ($responseUserProfile['UserPersonals']['gender'] == 'M') ? 'checked="checked"' : null; ?>> <?php echo __('ชาย'); ?>
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="UserPersonals[gender]" id="female" value="F" <?php echo ($responseUserProfile['UserPersonals']['gender'] == 'F') ? 'checked="checked"' : null; ?>> <?php echo __('หญิง'); ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->input('UserPersonals.fullname', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserPersonals']['firstname_th'] . ' ' . $responseUserProfile['UserPersonals']['lastname_th'], 'id' => 'fullName', 'type' => 'text', 'label' => __('ชื่อ - นามสกุล'), 'placeholder' => 'ชื่อ - นามสกุล', 'required']); ?>
                <?php // echo $this->Form->input('fullname', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfiles->firstname_th . ' ' . $userProfiles->lastname_th, 'id' => 'fullName', 'type' => 'text', 'label' => __('ชื่อ - นามสกุล'), 'placeholder' => 'ชื่อ - นามสกุล', 'required']); ?>

            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->input('UserPersonals.birthdate', ['class' => 'form-control-reg border-bottom-from required', 'value' => '', 'id' => 'datepicker', 'type' => 'text', 'label' => __('กรุณาระบุวันเดือนปีเกิด'), 'placeholder' => 'DD-MM-YYYY', 'required']); ?>
                <span class="glyphicon glyphicon-calendar form-control-feedback datepicker"></span>
            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo __('กรุ๊ปเลือด'); ?>
                <div class="radio-group">
                    <label class="radio-inline">
                        <input type="radio" name="UserPersonals[blood_group]" id="blood-A" value="A" <?php echo ($responseUserProfile['UserPersonals']['blood_group'] == 'A') ? 'checked="checked"' : null; ?>> <?php echo __('A'); ?>
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="UserPersonals[blood_group]" id="blood-B" value="B" <?php echo ($responseUserProfile['UserPersonals']['blood_group'] == 'B') ? 'checked="checked"' : null; ?>> <?php echo __('B'); ?>
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="UserPersonals[blood_group]" id="blood-AB" value="AB" <?php echo ($responseUserProfile['UserPersonals']['blood_group'] == 'AB') ? 'checked="checked"' : null; ?>> <?php echo __('AB'); ?>
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="UserPersonals[blood_group]" id="blood-O" value="O" <?php echo ($responseUserProfile['UserPersonals']['blood_group'] == 'O') ? 'checked="checked"' : null; ?>> <?php echo __('O'); ?>
                    </label>
                </div>

            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->input('UserPersonals.moblie_no', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserPersonals']['moblie_no'], 'id' => 'moblieNo', 'type' => 'text', 'label' => __('เบอร์โทรศัพท์'), 'placeholder' => '000-000-0000']); ?>
                <?php // echo $this->Form->input('moblie_no', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfiles->moblie_no, 'id' => 'moblieNo', 'type' => 'text', 'label' => __('เบอร์โทรศัพท์'), 'placeholder' => '000-000-0000']); ?>

            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->input('UserPersonals.email', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserPersonals']['email'], 'id' => 'email', 'type' => 'email', 'label' => __('Email'), 'placeholder' => 'mail@xxx.com']); ?>
                <?php // echo $this->Form->input('email', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfiles->email, 'id' => 'email', 'type' => 'email', 'label' => __('Email'), 'placeholder' => 'mail@xxx.com']); ?>

            </div>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php echo $this->Form->input('UserPersonals.address', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserPersonals']['address'], 'id' => 'address', 'type' => 'text', 'label' => __('ที่อยู่อาศัย'), 'placeholder' => __('112/3 สุขุมวิท 22 ทองหล่อ กทม 10523')]); ?>
                <?php // echo $this->Form->input('address', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfiles->address, 'id' => 'address', 'type' => 'text', 'label' => __('ที่อยู่อาศัย'), 'placeholder' => __('112/3 สุขุมวิท 22 ทองหล่อ กทม 10523')]); ?>
            </div>
        </div>
        <?php // endforeach; ?>
        <!-------------------------------------------------->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="toggle" data-plugin-toggle>
                    <?php // foreach ($userProfile as $k => $userProfiles): ?>
                    <section class="toggle">
                        <label><?php echo __('สถานภาพทางการศึกษา'); ?></label>
                        <div class="toggle-content">
                            <!------------------------Sub--------------------------->
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12">
                                        <?php echo $this->Form->input('UserProfiles.0.card_code', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserProfiles']['card_code'], 'id' => 'cardCode', 'type' => 'text', 'label' => __('รหัสนักศึกษา'), 'placeholder' => __('545223092010'), 'readonly']); ?>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <?php echo $this->Form->input('UserProfiles.0.organize_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['organize_id'], 'id' => 'OrganizetionId', 'type' => 'select', 'options' => $organizations, 'label' => __('มหาวิทยาลัย'), 'placeholder' => __('มหาวิทยาลัยราชมงคล'), 'required']); ?>
                                        <?php // echo $this->Form->input('organize_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12', 'default' => $userProfile->master_organization->id, 'id' => 'OrganizetionId', 'type' => 'select', 'options' => $organizations, 'label' => __('มหาวิทยาลัย'), 'placeholder' => __('มหาวิทยาลัยราชมงคล'), 'required']); ?>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <?php echo $this->Form->input('UserProfiles.0.dept_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['dept_id'], 'id' => 'DepartmentId', 'type' => 'select', 'options' => $departments, 'label' => __('คณะ'), 'empty' => __(' --- Please Select --- ')]); ?>
                                        <?php // echo $this->Form->input('dept_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $userProfile->master_department->id, 'id' => 'DepartmentId', 'type' => 'select', 'label' => __('คณะ'), 'empty' => __(' --- Please Select --- ')]); ?>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <?php echo $this->Form->input('UserProfiles.0.section_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['section_id'], 'id' => 'SectionId', 'type' => 'select', 'options' => $sections, 'label' => __('สาขาวิชาการศึกษา'), 'empty' => __(' --- Please Select --- ')]); ?>
                                        <?php // echo $this->Form->input('section_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $userProfile->master_section->id, 'id' => 'SectionId', 'type' => 'select', 'label' => __('สาขาวิชาการศึกษา'), 'empty' => __(' --- Please Select --- ')]); ?>
                                    </div>
                                </div>
                            </div>
                            <!------------------------End Sub--------------------------->
                        </div>
                    </section>
                    <?php // endforeach; ?>
                    <?php // foreach ($userProfile as $k => $userProfiles): ?>
                    <section class="toggle">
                        <label><?php echo __('สถานภาพทางการทำงาน'); ?></label>
                        <div class="toggle-content">
                            <!------------------------Sub--------------------------->
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div>
                                    <div class="form-group has-feedback">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <!--<label class="label-text-sub">ประเภทงาน</label>-->
                                            <?php echo __('ประเภทงาน'); ?>
                                            <div class="radio-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name="UserProfiles[1][master_business_type_id]" id="businessType-A" value="1" <?php echo ($responseUserProfile['UserProfiles']['master_business_type_id'] == 1) ? 'checked="checked"' : null; ?>> <?php echo __('เจ้าหน้าที่บริษัท'); ?> 
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="UserProfiles[1][master_business_type_id]" id="businessType-B" value="2" <?php echo ($responseUserProfile['UserProfiles']['master_business_type_id'] == 2) ? 'checked="checked"' : null; ?>> <?php echo __('ธุรกิจส่วนตัว'); ?> 
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="UserProfiles[1][master_business_type_id]" id="businessType-C" value="3" <?php echo ($responseUserProfile['UserProfiles']['master_business_type_id'] == 3) ? 'checked="checked"' : null; ?>> <?php echo __('รับราชการ'); ?> 
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="UserProfiles[1][master_business_type_id]" id="businessType-D" value="4" <?php echo ($responseUserProfile['UserProfiles']['master_business_type_id'] == 4) ? 'checked="checked"' : null; ?>> <?php echo __('รัฐวิสาหกิจ'); ?> 
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="UserProfiles[1][master_business_type_id]" id="businessType-E" value="5" <?php echo ($responseUserProfile['UserProfiles']['master_business_type_id'] == 5) ? 'checked="checked"' : null; ?>> <?php echo __('พนักงานชั่วคราว'); ?>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <!--<label class="label-text-sub">ชื่อบริษัท/ห้างร้าน</label>-->
                                        <?php // echo $this->Form->input('company_name', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfile->master_organization->org_name_th, 'id' => 'companyName', 'type' => 'text', 'label' => __('ชื่อบริษัท/ห้างร้าน'), 'placeholder' => __('วิศวกรรมคอมพิวเตอร์')]); ?>
                                        <?php echo $this->Form->input('UserProfiles.1.organize_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['organize_id'], 'id' => 'companyName', 'type' => 'select', 'options' => $companys, 'label' => __('ชื่อบริษัท/ห้างร้าน'), 'empty' => __(' --- Please Select --- ')]); ?>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <!--<label class="label-text-sub">ที่อยู่ที่ทำงาน</label>-->
                                        <?php echo $this->Form->input('UserProfiles.1.address', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserProfiles']['address'], 'id' => 'officeAddress', 'type' => 'text', 'label' => __('ที่อยู่ที่ทำงาน'), 'placeholder' => __('วิศวกรรมคอมพิวเตอร์')]); ?>
                                        <?php // echo $this->Form->input('address', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfile->address, 'id' => 'officeAddress', 'type' => 'text', 'label' => __('ที่อยู่ที่ทำงาน'), 'placeholder' => __('วิศวกรรมคอมพิวเตอร์')]); ?>

                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <!--<label class="label-text-sub">เบอร์โทรศัพท์</label>-->
                                        <?php echo $this->Form->input('UserProfiles.1.phone_no', ['class' => 'form-control-reg border-bottom-from label-text-sub required', 'value' => $responseUserProfile['UserProfiles']['phone_no'], 'id' => 'officePhone', 'type' => 'text', 'label' => __('เบอร์โทรศัพท์'), 'placeholder' => __('00-000-0000')]); ?>
                                        <?php // echo $this->Form->input('phone_no', ['class' => 'form-control-reg border-bottom-from label-text-sub', 'value' => $userProfile->phone_no, 'id' => 'officePhone', 'type' => 'text', 'label' => __('เบอร์โทรศัพท์'), 'placeholder' => __('วิศวกรรมคอมพิวเตอร์')]); ?>

                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <?php echo $this->Form->input('UserProfiles.1.dept_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['dept_id'], 'id' => 'department', 'type' => 'select', 'options' => $departments, 'label' => __('ฝ่าย'), 'empty' => __(' --- Please Select --- ')]); ?>
                                        <?php // echo $this->Form->input('department_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12', 'default' => $userProfile->master_department->id, 'id' => 'department', 'type' => 'select', 'options' => '', 'label' => __('ฝ่าย'), 'placeholder' => __('ฝ่าย')]); ?>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <!--<label class="label-text-sub">ฝ่าย/แผนก</label>-->
                                        <?php echo $this->Form->input('UserProfiles.1.section_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['section_id'], 'id' => 'cottonId', 'type' => 'select', 'options' => $sections, 'label' => __('แผนก'), 'empty' => __(' --- Please Select --- ')]); ?>
                                        <?php // echo $this->Form->input('cotton', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12', 'default' => $userProfile->master_section->id, 'id' => 'cottonId', 'type' => 'select', 'options' => '', 'label' => __('แผนก'), 'placeholder' => __('แผนก')]); ?>
                                    </div>
                                </div>
                                <?php // foreach($userProfile->master_organization['master_organization_positions'] as $k => $masterPosition):?>
                                <?php // debug($masterPosition); exit;?>
                                <div class="form-group has-feedback">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <?php echo $this->Form->input('UserProfiles.1.master_organization_position_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12 required', 'default' => $responseUserProfile['UserProfiles']['master_organization_position_id'], 'id' => 'positionOrganization', 'type' => 'select', 'options' => '', 'label' => __('ตำแหน่ง'), 'empty' => __(' --- Please Select --- ')]); ?>
                                        <?php // echo $this->Form->input('position_organization_id', ['class' => 'form-control select-from label-text-sub col-xs-12 col-sm-12 col-md-12', 'default' => $userProfile->master_organization['master_organization_positions'][0]['id'], 'id' => 'positionOrganization', 'type' => 'select', 'options' => '', 'label' => __('ตำแหน่ง'), 'placeholder' => __('ตำแหน่ง')]); ?>
                                    </div>
                                </div>
                                <?php // endforeach;?>

                            </div>
                            <!------------------------End Sub--------------------------->
                        </div>
                    </section>
                    <?php // endforeach; ?>
                </div>
            </div>
        </div>
        <!-------------------------------------------------->

        <div>
            <?php // echo $this->Form->submit(__('SAVE')) ?>
            <button type="submit" class="btn btn-quaternary mr-xs mb-sm button-text-profile">บันทึก</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#moblieNo').inputmask('999-999-9999');
        $('#officePhone').inputmask('99-999-9999');
//==================================================
//        CHANGE UNIVERSITY PROFILES
//==================================================
        $("#OrganizetionId").change(function () {
            getDepartment();
        });
        $("#DepartmentId").change(function () {
            getSection();
        });
        $("#SectionId").change(function () {
            updateChosen();
        });
//==================================================
//        CHANGE COMPAMY PROFILES
//==================================================
        $("#companyName").change(function () {
            getCompanyDepartment();
        });
        $("#department").change(function () {
            getCompanySection();
        });
        $("#cottonId").change(function () {
            getCompanyPosition();
        });
        $(".datepicker").change(function () {
            updateChosen();
        });
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
//==================================================
//        CHANGE PROFILES PICTURE
//==================================================
        $("#uploadProfile").change(function () {
            readURL(this);
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
