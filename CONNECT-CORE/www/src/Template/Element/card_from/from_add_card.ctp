<div class="col" data-hash="box-add-card" id="box-add-card">
    <div class="row">
        <div class="col-1 box-arrow left">
            <i class="fa fa-angle-left prev"> </i>
        </div>
        <div class="col-10">
            <img src="/img/core/img/bloc-plus@3x.png" class="img-fluid">
        </div>
        <div class="col-1 box-arrow right"></div>
    </div><!-- /.row -->
    <br><br> 
    <div>
        <div class="row text-center">
            <div class="col">
                <h2><?php echo __('add card');?></h2>
            </div>
        </div><!-- /.row text-center -->
        <br>
        <?php echo $this->Form->create('UserCards', [ 'id'=>'UserCards', 'name' => 'frmSignIn','role' => 'form','url' => ['controller' => 'user-cards', 'action' => 'add-card']  ]); ?>
        <?php echo $this->Form->control('organize_code', ['class' => 'form-control-lg required alphanumeric' ,'id' => 'organize_code','label' => ['class' => 'pakgon-label', 'text'=> __('Company Code')]]); ?>
        <?php echo $this->Form->control('employee', ['class' => 'form-control-lg required alphanumeric' ,'id' => 'employee','label' => ['class' => 'pakgon-label','text'=>__('Ref 1')]]); ?>
        <?php echo $this->Form->control('birthdate', ['class' => 'form-control-lg required' ,'readonly'=> 'readonly','data-date-format'=>'mm/dd/yyyy','id' => 'date','label' => ['class' => 'pakgon-label','text'=>__('Ref 2')]]); ?>
        <?php echo $this->Form->hidden('organize_id', ['id' => 'organize_id']); ?>
        <div class="row">
            <div class="col">
                <?php echo $this->Permission->submit( __('Check'), $this->request->here, ['id' => 'checkbutton', 'class' => 'btn btn-primary btn-lg btn-block']); ?>
            </div>
        </div><!-- /.row -->          
        <?php echo $this->Form->end(); ?>
    </div><!-- /. -->
</div><!--/.col -->

<?php $this->append('scriptBottom'); ?>

<script type="text/javascript">
    var organize_id = $("#organize_id");
    var employee = $("#employee");
    var date = $("#date");

    $(document).ready(function () {

        $('#checkbutton').attr("disabled", true);
        $('#employee').attr("disabled", true);
        $("#organize_code").on('keyup', function () {  

            var organize_code = $("#organize_code").val();
            $('#employee').attr("disabled", true);
            $('#date').attr("disabled", true);
            $('#checkbutton').attr("disabled", true);
            organize_id.val('');
            employee.val('');

            if ( $("#organize_code").valid() ) {

                if(organize_code){
                    $.post("/UserCards/checkOrg", { 'organize_code' : organize_code }, function (data) {
                        
                        if (data != 'false') {
                            data = jQuery.parseJSON(data);
                            if (data['chkuser'] == true) {
                                $(".message-org").remove();
                                $("#organize_code").after( '<div class="add-card-message message-org add-card-message-error">'+"<?php echo __('Register with') ?>" + data[0]['org_name_th'] + "<?php echo __('already') ?>"+'</div>' );
                                $('#employee').attr("disabled", true);
                                organize_id.val('');
                                employee.val('');
                            } else if (data['chkuser'] == false) {
                                $(".message-org").remove();
                                $("#organize_code").after( '<div class="add-card-message message-org add-card-message-true">'+data[0]['org_name_th']+'</div>' );
                                // $("#checkbutton").removeAttr("disabled");
                                $("#employee").removeAttr("disabled");
                                organize_id.val(data[0]['id']);
                            }
                        } else {                      
                            $(".message-org").remove();
                            $("#organize_code").after( '<div class="add-card-message message-org add-card-message-error">'+"<?php echo __('Data not found') ?>"+'</div>' );
                            $('#employee').attr("disabled", true);
                            organize_id.val('');
                            employee.val('');
                        }
                    });
                }else{
                    setTimeout(function(){ $(".message-org").remove(); }, 200);                  
                }
            }else{  
                setTimeout(function(){ $(".message-org").remove(); }, 200);              
            }
        });

        $("#employee").on('keyup', function () {  
            if ( $("#employee").valid() ) {
                var employee_val = employee.val();
                var organize_code = $("#organize_code").val();
                $('#checkbutton').attr("disabled", true);
                $('#date').attr("disabled", true);
                if(employee_val){
                    $.post("/UserCards/checkEmp", { 'employee_val': employee_val, 'organize_code' : organize_code }, function (data) {
                        if (data != 'false') {
                            data = jQuery.parseJSON(data);
                            if (data['chkuser'] == true) {
                                var message = '<?php echo __('Registered') ?>';
                                $(".message-employee").remove();
                                $("#employee").after( '<div class="add-card-message message-employee add-card-message-error">'+message+'</div>' );
                                $('#date').attr("disabled", true);
                            } else if (data['chkuser'] == false) {                         
                            var message = data['prefix_name_th'] + data['firstname_th'] + ' ' + data['lastname_th'];
                                $(".message-employee").remove();
                                $("#employee").after( '<div class="add-card-message message-employee add-card-message-true">'+message+'</div>' );
                                $("#date").removeAttr("disabled");
                            }
                        } else {
                            var message = '<?php echo __('"Did not find the information you need", Please enter the correct information and press the check.') ?>';
                            $(".message-employee").remove();
                            $("#employee").after( '<div class="add-card-message message-employee add-card-message-error">'+message+'</div>' );
                            $('#date').attr("disabled", true);
                        }
                    });

                }else{
                    setTimeout(function(){ $(".message-employee").remove(); }, 200);          
                }
            }else{
                setTimeout(function(){ $(".message-employee").remove(); }, 200);   
            }
        });

        $(document).on('change','#date',function () {
            $("#checkbutton").removeAttr("disabled");
        });

        var date_input = $('input[id="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);

    });
</script>

<?php $this->end();?>