<?php
use Cake\I18n\Time;
?>


<div class="row">
    <div class="col">
        <div class="modal" id="card_<?php echo $i;?>" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content-card">
                    <a class="close-modal" data-dismiss="modal">X </a>
                    <div class="modal-body form_1">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="f-c">                                
                                    <div class="row box-datial rotate270">
                                        <div class="row date-name ">
                                            <div class="col-8 ">
                                                <br>
                                                <div>
                                                    <label class="pakgon-label"><?php echo __('id');?></label>
                                                </div>
                                                <div>
                                                    <?php echo $value['card_code'] ?>
                                                </div>
                                                <br>
                                                <div>
                                                    <label class="pakgon-label"><?php echo __('Firstname') ?></label>
                                                    <?php echo $value['prefix_name_th'].$value['firstname_th'].' '.$value['lastname_th'] ?>
                                                </div>
                                            </div>
                                            <!--/.col-8 -->
                                            <div class="col-4">
                                                <img src="/img/core/img/user-profile@3x.png" class="img-fluid">
                                            </div>
                                            <!--/.col-4 -->
                                        </div>
                                        <!--/.date-name -->
                                        <div class="row date-position">
                                            <div class="col-12">
                                                <label class="pakgon-label"><?php echo __('Position') ?></label>
                                                <?php echo $value['position_name'] ?>
                                            </div>
                                            <!--/.col-12 -->
                                        </div>
                                        <!--/.date-position -->
                                        <div class="row text-center date-time">
                                            <div class="col-6">
                                                <?php 
                                                    $now = new Time($value['date_issued']);
                                                    $dateIssued = $now->i18nFormat('yyyy-MM-dd');
                                                    echo $this->DateFormat->formatDateThai($dateIssued);                  
                                                ?>
                                            </div>
                                            <!--/.col-6 -->
                                            <div class="col-6">
                                                <?php 
                                                    $now = new Time($value['date_expiry']);
                                                    $dateExpiry = $now->i18nFormat('yyyy-MM-dd');
                                                    echo $this->DateFormat->formatDateThai($dateExpiry);                  
                                                ?>
                                            </div>
                                            <!--/.col-6 -->
                                        </div>
                                        <!--/.date-time -->
                                        <div class="row text-center date-label">
                                            <div class="col-6">
                                                <label class="pakgon-label"><?php echo __('Card issue') ?></label>
                                            </div>
                                            <!--/.col-6 -->
                                            <div class="col-6">
                                                <label class="pakgon-label"><?php echo __('Expiry date') ?></label>
                                            </div>
                                            <!--/.col-6 -->
                                        </div>
                                        <!--/.date-label -->
                                        
                                    </div>
                                    <!--/.box-datial -->
                                    <?php echo $this->Html->image('/img/core/img/card-bg-front@3x.png', array('div' => false, 'class' => 'img-fluid img-rounded')); ?>
                                </div><!--/.f-c -->
                            </div>
                            <!--/.item font-card-->
                            <div class="item">
                                <?php  echo $this->Html->image('/img/core/img/card-bg-back@3x.png', array('div' => false, 'class' => 'img-fluid img-rounded')); ?>
                            </div>
                            <!--/.item black-card-->
                        </div>
                        <!--/.owl-carousel -->
                    </div>
                    <!--/.modal-body -->
                </div>
                <!--/.modal-content-card -->
            </div>
            <!--/.modal-dialog -->
        </div>
        <!--/.modal-dialog -->
    </div>
    <!--/.col -->
</div>
<!--/.row -->
<?php $this->append('scriptBottom');?>
<style>
.f-c {
    position: relative;
    border: 1px solid #000;
}

.f-c img {
    position: absolute;
}

.box-datial {
    position: fixed !important;
    box-sizing: border-box;
    width: 130%;
}

/* iPads (landscape) ----------- */

@media screen and (min-width: 1024px) {
    .box-datial {
        font-size: 3vw !important;
        margin: 32% 0% 0% -16%;
        border: 0px solid red;
    }
}

/* Add your styles for devices with a maximum width of 768 */

@media screen and (max-width: 768px) {
    .box-datial {
        font-size: 4vw !important;
        margin: 32% 0% 0% -16%;
        border: 0px solid red;
    }
}

/* Add your styles for devices with a maximum width of 480 */

@media screen and (max-width: 480px) {
    .box-datial {
        font-size: 5.3vw !important;
        margin: 32% 0% 0% -16%;
        border: 0px solid red;
    }
}

/* Add your styles for devices with a maximum width of 320 */

@media screen and (max-width: 320px) {
    .box-datial {
        font-size: 5vw !important;
        margin: 32% 0% 0% -16%;
        border: 0px solid red;
    }
}
</style>
<?php $this->end();?>