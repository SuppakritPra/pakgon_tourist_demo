<?php
$params = $this->Paginator->params();
$showSummary = isset($showSummary) ? $showSummary : true;
?>
<?php if ($params['pageCount'] > 1): ?>
    <div class="paginator">
        <ul class="pagination pull-right">
            <?php echo $this->Paginator->options(array('url' => $this->passedArgs)); ?>
            <?php echo $this->Paginator->first('<< ' . __('first')); ?>
            <?php echo $this->Paginator->prev('< ' . __('previous')); ?>
            <?php echo $this->Paginator->numbers(['before' => '', 'after' => '']); ?>
            <?php echo $this->Paginator->next(__('next') . ' >'); ?>
            <?php echo $this->Paginator->last(__('last') . ' >>'); ?>
        </ul>

        <?php if ($showSummary === true): ?>
            <p><?php echo $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]); ?></p>
        <?php endif; ?>

        <?php //if (isset($options)): ?>
            <?php
            //debug($this->passedArgs);exit;
            //echo $this->Paginator->options(array('url' => $this->passedArgs)); 
            
            ?>
        <?php //endif; ?>
    </div>
<?php endif; ?>