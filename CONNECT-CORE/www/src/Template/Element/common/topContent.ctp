<?php
$title = isset($title) ? $title : null;
?>
<div class="row text-center">
    <div class="col">
        <?php echo $this->Html->image('/img/core/img/logo-connect-login@3x.png', ['alt' => 'Connect Logo', 'class' => 'w-50']); ?>
    </div>
</div>

<?php if (!is_null($title)): ?>
    <div class="row text-center">
        <div class="col">
            <h2><?php echo $title; ?></h2>
        </div>
    </div>
<?php endif; ?>

<br/>