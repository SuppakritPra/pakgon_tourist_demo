<!DOCTYPE html>
<html>
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">	
        <title><?php echo $this->name ?></title>	
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Porto - Responsive HTML5 Template">
        <meta name="author" content="okler.net">
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
        <!-- Vendor CSS -->
        <?php echo $this->Html->css('/css/pakgon.css'); ?>
        <?php echo $this->Html->css('/css/bootstrap/css/bootstrap.min'); ?>
        <?php echo $this->Html->css('/css/font-awesome/css/font-awesome.min'); ?>
        <?php echo $this->Html->css('/css/animate/animate.min'); ?>
        <?php echo $this->Html->css('/css/simple-line-icons/css/simple-line-icons.min'); ?>
        <?php echo $this->Html->css('/css/owl.carousel/assets/owl.carousel.min'); ?>
        <?php echo $this->Html->css('/css/owl.carousel/assets/owl.theme.default.min'); ?>
        <?php echo $this->Html->css('/css/magnific-popup/magnific-popup.min'); ?>
        <!-- Theme CSS -->
        <?php echo $this->Html->css('/css/css/theme'); ?>
        <?php echo $this->Html->css('/css/css/theme-elements'); ?>
        <?php echo $this->Html->css('/css/css/theme-blog'); ?>
        <?php echo $this->Html->css('/css/css/theme-shop'); ?>
        <!-- Skin CSS -->
        <?php echo $this->Html->css('/css/css/skins/default'); ?>
        <?php echo $this->Html->script('/js/master/style-switcher/style.switcher.localstorage'); ?>	
        <!-- Theme Custom CSS -->
        <?php echo $this->Html->css('/css/css/custom'); ?>
        <!-- Head Libs -->
        <?php echo $this->Html->script('/js/modernizr/modernizr.min'); ?>	
        <!-- APP  -->
        <?php echo $this->Html->script('custom/core'); ?>
        <?php echo $this->Html->css('/css/pakgon.css'); ?>
        <style type="text/css">
            @font-face {
                font-family: 'Conv_supermarket';
                src: url('/font/fonts/supermarket.eot');
                src: local('☺'), url('/font/fonts/supermarket.woff') format('woff'), url('/font/fonts/supermarket.ttf') format('truetype'), url('/font/fonts/supermarket.svg') format('svg');
                font-weight: normal;
                font-style: normal;
            }
        </style>
        <?php echo $this->Html->script('/js/jquery/jquery-1.11.3.min'); ?>
        <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
        <?php echo $this->Html->css('/css/css/bootstrap-iso'); ?>
        <?php echo $this->Html->script('/js/jquery/alex-date-time'); ?>
        <?php echo $this->Html->css('/css/chosen_v151/bootstrap-chosen/bootstrap-chosen.css'); ?>
        <?php echo $this->Html->script('/css/chosen_v151/chosen.jquery.min.js'); //Chosen select box?>
        <!-- Bootstrap Date-Picker Plugin -->	
        <?php echo $this->Html->css('/css/bootstrap-datepicker-thai-thai/css/datepicker.css'); ?>	
        <?php echo $this->Html->script('/css/datepicker/bootstrap-datepicker.js'); ?>	
        <?php echo $this->Html->script('/css/datepicker/bootstrap-datepicker-thai.js'); ?>	
        <?php echo $this->Html->css('/css/datepicker/datepicker3.css'); ?>	
        <!-------- Bootstrap Date-Picker Plugin -------->
        <!-------- Bootstrap Input mask Plugin --------->
        <?php echo $this->Html->script('/js/input-mask/jquery.inputmask.min.js'); ?>	
        <?php echo $this->Html->script('/js/input-mask/jquery.inputmask.phone.extensions.js'); ?>	
        <!-------- Bootstrap Input mask Plugin --------->
    </head>
    <body>
        <div class="body">
            <div role="main" class="main">
                <style>
                    .login-container{max-width: 420px; margin: auto;}
                </style>
                <div class="container">
                    <?php echo $this->fetch('content') ?>
                </div>
            </div>
        </div>
        <?php echo $this->Html->script('/js/cleave-phone.th'); ?>
        <?php echo $this->Html->script('/js/cleave.min'); ?>
        <?php echo $this->Html->script('jquery.appear/jquery.appear.min'); ?>
        <?php echo $this->Html->script('jquery.easing/jquery.easing.min'); ?>
        <?php echo $this->Html->script('jquery-cookie/jquery-cookie.min'); ?>
        <?php //echo $this->Html->script('/js/master/style-switcher/style.switcher'); ?>
        <?php echo $this->Html->script('/css/bootstrap/js/bootstrap.min'); ?>
        <?php echo $this->Html->script('/css/bootstrap/js/bootstrap-show-password'); ?>
        <?php echo $this->Html->script('/js/common/common.min'); ?>
        <?php echo $this->Html->script('/js/jquery.validation/jquery.validation.min'); ?>
        <?php echo $this->Html->script('/js/jquery.easy-pie-chart/jquery.easy-pie-chart.min'); ?>
        <?php echo $this->Html->script('/js/jquery.gmap/jquery.gmap.min'); ?>
        <?php echo $this->Html->script('/js/jquery.lazyload/jquery.lazyload.min'); ?>
        <?php echo $this->Html->script('/js/isotope/jquery.isotope.min'); ?>
        <?php echo $this->Html->script('/js/owl.carousel/owl.carousel.min'); ?>
        <?php echo $this->Html->script('/js/magnific-popup/jquery.magnific-popup.min'); ?>
        <?php echo $this->Html->script('/js/vide/vide.min'); ?>	
        <!-- Theme Base, Components and Settings -->
        <?php echo $this->Html->script('theme'); ?>
        <!-- Theme Custom -->
        <?php echo $this->Html->script('custom'); ?>
        <!-- Theme Initialization Files -->
        <?php echo $this->Html->script('theme.init'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
