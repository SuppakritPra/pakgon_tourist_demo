<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    protected $selectEmptyMsg = '---- please select ----';

    /**
     *
     * Documentation 
     * Link : https://holt59.github.io/cakephp3-bootstrap-helpers/
     * Git : https://github.com/Holt59/cakephp3-bootstrap-helpers
     * @var type 
     */
    public $helpers = [
        'Utility',
        'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
        'Html' => ['className' => 'Bootstrap.Html'],
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Navbar' => ['className' => 'Bootstrap.Navbar'],
        'Paginator' => ['template' => 'paginator-template']
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        I18n::setLocale('th_TH');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => ['controller' => 'Homes', 'action' => 'index'],
            'logoutRedirect' => ['controller' => 'Users', 'action' => 'signin'],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'userModel' => 'Users'
                ]
            ],
            'loginAction' => ['controller' => 'Users', 'action' => 'signin'],
            'authorize' => ['Controller'],
            'unauthorizedRedirect' => $this->referer()// If unauthorized, return them to page they were just on
        ]);


        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * 
     * Function trigger before filter process
     * @author sarawutt.b
     * @param Event $event
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['signin', 'signout', 'signup', 'verify']);
        // $this->viewBuilder()->setTheme('AdminLTE204');
        $this->viewBuilder()->setTheme('PakgonConnect');
        /**
         * 
         * Set appication language this can be thai|english
         * @author Sarawutt.b
         * @since 2018-02-28
         * @return void
         */
        if ($this->request->session()->check('SessionLanguage') == false) {
            $this->request->session()->write('SessionLanguage', 'tha');
        }

        $this->checkSessionAuth();
    }

    /**
     * 
     * Function check for session
     * @author sarawutt.b
     * @return void
     */
    public function checkSessionAuth() {
        //Check has existing to Authorize

        $currentPath = strtolower(trim($this->request->here));
        if (!empty($this->Auth->user('id')) && ($currentPath == '/')) {
            return $this->redirect(Configure::read('RedirectApplicationURL'));
        } else {
            //$this->request->session()->delete('Flash');
            //$this->request->session()->delete('Flash.auth');
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])) {
            $this->set('_serialize', true);
        }
    }

    /**
     * 
     * Function check authorize
     * @author sarawutt.b
     * @param type $user
     * @return boolean
     */
    public function isAuthorized($user) {
        return true;
    }

    /**
     * 
     * Function check fore token
     * @return type
     */
    function checkToken() {
        if (empty($this->request->getHeaderLine('Authorization'))) {
            return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
        }
    }

    /**
     * Set language used this in mutiple language application concept
     * @author Sarawutt.b
     * @since 2016/03/21 10:23:33
     * @return void
     */
    public function _setLanguage() {
        $this->L10n = new L10n();
        $language = $this->request->session()->read('SessionLanguage');
        Configure::write('Config.language', $language);
        $this->L10n->get($language);
    }

    /**
     * 
     * Function get for current session user language
     * @author sarawutt.b
     * @return string
     */
    public function getCurrentLanguage() {
        return $this->request->session()->read('SessionLanguage');
    }

    /**
     *
     * Function used fro generate _VERSION_
     * @author  sarawutt.b
     * @return  biginteger of the version number
     */
    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    /**
     *
     * Function used for generate UUID key patern
     * @author  sarawutt.b
     * @return  string uuid in version
     */
    public function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    /**
     * 
     * Function get for current session user authentication full name
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user full name
     */
    protected function getAuthFullname() {
        return $this->readAuth('Auth.User.first_name') . ' ' . $this->readAuth('Auth.User.last_name');
    }

    /**
     * 
     * Function get for current session user authentication user id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    protected function getAuthUserId() {
        return $this->readAuth('Auth.User.id');
    }

    /**
     * 
     * Function get for current session user authentication role id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    protected function getAuthUserRoleId() {
        return $this->readAuth('Auth.User.role_id');
    }

    /**
     * 
     * Function get for current session with user authentication
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication session info
     */
    protected function readAuth($name = null) {
        return $this->request->session()->read($name);
    }

    /**
     * Function get for empty option in DDL
     * @author sarawutt.b
     * @return array() of empty select DDL
     */
    public function getEmptySelect() {
        return ['' => __($this->selectEmptyMsg)];
    }

    /**
     *
     * This function for uload attachment excel file  from view of information style list
     * @author  sarawutt.b
     * @param   string name of target upload path
     * @param   array() file attribute option from upload form
     * @since   2017/10/30
     * @return  array()
     */
    function uploadFiles($folder, $file, $itemId = null) {
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;
        if (!is_dir($folder_url)) {
            mkdir($folder_url, 0777);
        }
        //Bould new path if $itemId to be not null
        if ($itemId) {
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            $rel_url = $folder . '/' . $itemId;
            if (!is_dir($folder_url)) {
                mkdir($folder_url, 0777);
            }
        }
        //define for file type where it allow to upload
        $map = [
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'application/pdf' => '.pdf',
            'application/octet-stream' => '.rar',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/excel' => '.xls',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx'
        ];
        //Bould file extension keep to the database
        $userfile_extn = substr($file['name'], strrpos($file['name'], '.') + 1);
        $typeOK = false;
        if (array_key_exists($file['type'], $map)) {
            $typeOK = true;
        }
        //debug($file);exit;
        //Rename for the file if not change of the upload file makbe duplicate
        $filename = $this->VERSION() . $map[$file['type']];
        //debug($filename);exit;
        if ($typeOK) {
            switch ($file['error']) {
                case 0:
                    @unlink($folder_url . '/' . $filename); //Delete the file if it already existing
                    $full_url = $folder_url . '/' . $filename;
                    $url = $rel_url . '/' . $filename;
                    $success = move_uploaded_file($file['tmp_name'], $url);
                    chmod($url, 0777);
                    if ($success) {
                        $result['path'] = $folder_url;
                        $result['uploadPaths'][] = '/' . $url;
                        $result['uploadFileNames'][] = $filename;
                        $result['uploadExts'][] = $userfile_extn;
                        $result['uploadOriginFileNames'][] = $file['name'];
                        $result['uploadFileTypes'][] = $file['type'];
                    } else {
                        $result['uploadErrors'][] = __("Error uploaded {$filename}. Please try again.");
                    }
                    break;
                case 3:
                    $result['uploadErrors'][] = __("Error uploading {$filename}. Please try again.");
                    break;
                case 4:
                    $result['noFiles'][] = __("No file Selected");
                    break;
                default:
                    $result['uploadErrors'][] = __("System error uploading {$filename}. Contact webmaster.");
                    break;
            }
        } else {
            $permiss = '';
            foreach ($map as $k => $v) {
                $permiss .= "{$v}, ";
            }
            $result['uploadErrors'][] = __("{$filename} cannot be uploaded. Acceptable file types in : %s", trim($permiss, ','));
        }
        return $result;
    }

}
