<?php
namespace App\Controller;
use App\Controller\AppController;

class ProvincesController extends AppController
{
public function initialize() {
        parent::initialize();
        $this->Auth->allow();
    }	
	public function getProvince() {

		$this->autoRender = false;
		$this->loadModel('MasterProvinces');
		$states = array();
	 $id = $this->request->data('id');
	  $states = $this->MasterProvinces->find('list', array(
				'keyField' => 'id',
				'valueField' => 'province_name_th',
	   'conditions' => array(
		'master_country_id' =>$id
	   )
			))->toArray();
		$arr = array();
	   if(!empty($states)){
		asort($states);
	 echo '<option value=""> ---Select--- </option>';
	 foreach($states as $key => $val){
	  echo '<option value="' . $key . '">' . $val. '</option>';
	 }
	}else{
	 return false;
	}
   }
	 
}
