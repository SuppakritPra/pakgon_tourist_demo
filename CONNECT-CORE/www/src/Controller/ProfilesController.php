<?php
namespace App\Controller;
use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Http\Client;

class ProfilesController extends AppController
{

    public function index(){

        $this->viewBuilder()->layout('blank');

	    $user_id = $this->Auth->user('id');
	
        $this->loadModel('Users');
        $responseUserProfile = $this->Users->get($user_id, [
            'contain' => []
        ]);
        $username = $responseUserProfile['username'];

        $this->loadModel('UserPersonals');
        $responseUserPersonal = $this->UserPersonals->find('all', [
            'conditions' => ['user_id' => $user_id ]
        ])->first();

        // $this->loadModel('MasterCountries');
        // $Country = $this->MasterCountries->find('list', [
        //     'conditions' => [
        //         'is_used' => true],
        //     'keyField' => 'id',
        //     'valueField' => 'country_name_th'
        // ]);
        // if(!empty($Country)) $Country = $Country->toArray();
        // asort($Country);
    
        $this->loadModel('MasterProvinces');
        $Province = $this->MasterProvinces->find('list', [
            'conditions' => [
            'is_used' => true],
            'keyField' => 'id',
            'valueField' => 'province_name_th'
        ])
        ->where(['master_country_id =' => $responseUserPersonal['master_country_id']])
        ->order(['province_name_th']);

        if(!empty($Province)) $Province = $Province->toArray();
        asort($Province);
            $res = $responseUserPersonal['birthdate'];
        if($res){
            $dateNow = @$res->i18nFormat('dd/MM/yyyy');
        }else{
            $dateNow = "";
        }

        $this->loadModel('UserCards');
        $UserCards = $this->UserCards->find('all')
         ->select($this->UserCards)
        ->select(
            'morg.org_name_th'
        )
        ->join([             
            'morg' => [
                'table' => 'master.master_organizations',
                'type' => 'INNER',
                'conditions' => [
                    'morg.id = userCards.organize_id'
                ],
            ]
        ])
        ->where([
            'userCards.user_id' => $user_id   
        ])
        ->order(['userCards.id' => 'ASC'])
        ->toArray();


        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;

            // pr($data);die;
            $this->loadModel('UserPersonals');
            $userPersonals = $this->UserPersonals->find('all',
                [      
                    'limit' => 1,
                    'conditions' => [
                        'user_id' => $user_id
                    ]
                ]
            )->first();
 
            // if($userPersonals['email'] == $data['email']){
            //     $chkemail = true;
            // }else{
            //     $useremails = $this->UserPersonals->find('all',
            //     [      
            //         'limit' => 1,
            //         'conditions' => [
            //             'email' => $data['email']
            //         ]
            //     ]
            //     )->first();

            //     if(empty($useremails)){
            //         $chkemail = true;
                    
            //     }else{
            //     $chkemail = false;
            //     }
            // }

            //if($chkemail == true){
            
                // $birthdate = explode("/", $data['UserPersonals']['birthdate']);
                // $birthdate = $birthdate[2].'-'.$birthdate[1].'-'.$birthdate[0];
                $userPersonals = $this->UserPersonals->patchEntity($userPersonals, $data);
                //$userPersonals['firstname_en'] = $data['UserPersonals']['firstname_th'];
                //$userPersonals['lastname_en'] = $data['UserPersonals']['lastname_th'];
                // $userPersonals['firstname_en'] = $data['UserPersonals']['firstname_en'];
                // $userPersonals['lastname_en'] = $data['UserPersonals']['lastname_en'];
                $userPersonals['firstname_en'] = $data['firstname_th'];
                $userPersonals['lastname_en'] = $data['lastname_th'];
               // $userPersonals['email'] = $data['email'];
               // $userPersonals['birthdate'] = $birthdate;
               // $userPersonals['user_id'] = $user_id;
              //  $userPersonals['master_country_id'] = $data['master_country_id'];
                $userPersonals['master_province_id'] = $data['master_province_id'];
                $userPersonals['modified_by'] = $user_id;
                
                if ($this->UserPersonals->save($userPersonals)) {
                    $this->Flash->success(__('Update Complete.'));
                    return $this->redirect(['action' => 'index']);
                }
            // }else{
            //     $this->Flash->error(__('Email Address already used by others.'));
            // }
            $this->Flash->error(__('Not update. Please, try again.'));
        }

        $this->set(compact('userPersonals', 'responseUserProfile',  'dateNow', 'username', 'Country', 'Province', 'responseUserPersonal','UserCards'));
        $this->set('_serialize', ['userPersonals', 'responseUserProfile', 'dateNow', 'username', 'Country', 'Province', 'responseUserPersonal','UserCards']);
    }

    
}

