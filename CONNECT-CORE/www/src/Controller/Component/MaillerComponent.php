<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\Core\Configure;

class MaillerComponent extends Component {

    public function initialize(array $config) {
        
    }

    /**
     * 
     * Function send mail
     * @author sarawutt.b
     * @param type $params
     * @return boolean
     */
    public function sendMail($params = null) {
        if (is_null($params) || !is_array($params)) {
            return false;
        }
        $userName = $params['firstname'] . " " . $params['lastname'];
        $email = new Email();
        $email->transport('mailjet');
        try {
            $res = $email->from([$params['email'] => $userName])
                    ->to([Configure::read('MAILLER.FROM_EMAIL') => Configure::read('MAILLER.FROM_USERNAME')])
                    ->subject('Contact')
                    ->send($params['message']);
        } catch (\Exception $ex) {
            $this->log($ex);
            echo 'Exception : ', $ex->getMessage(), "\n";
            return false;
        }
    }

}
