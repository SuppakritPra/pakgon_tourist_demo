<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class ExcelComponent extends Component
{

	public function initialize(array $config) {
		parent::initialize($config);
		// Load PHPExcel from vender location
		// pr($config);
		require_once ROOT . DS . 'vendor' . DS . 'phpexcel' . DS . 'PHPExcel.php';
		//require_once ROOT . DS . 'vendor' . DS . 'phpexcel' . DS . 'PHPExcel/IOFactory.php';
	}

	
	function readData($data_file) {
	
		// pr($data_file);
		$file = $data_file['path'].$data_file['file_name'];#prr($file);

		#--------------------------------------------------------------------------------		
		$objPHPExcel = \PHPExcel_IOFactory::load($file);
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$dataExcels = [];
		for ($row = 0; $row <= $highestRow; ++$row) {
			$data = $objWorksheet->rangeToArray('A' . $row . ':' . 'R' . $row, null, true, true, true);
			$dataExcels[] = $data[$row];
		}
		// pr($dataExcels);
		#prd($dataExcels);
		
		return $dataExcels;

	
	}

	function readFileCSV($filename=''){
		// pr($filename);die;
	
		$fh = fopen($filename,'r');
		while ($line = fgets($fh)) {

			$line = trim($line);
			if($line != ''){
				$line = trim($line);
				$line = iconv('TIS-620', 'UTF-8', $line);
				$line_of_text[]	= explode(',',$line);	
			}
		}
		fclose($fh);
		
		return $line_of_text;
	
	}

}