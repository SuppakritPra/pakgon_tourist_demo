<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Core\Configure;

class TestsController extends AppController
{
  
    public function initialize() {
        parent::initialize();
        // $this->Auth->allow(['signin', 'signout', 'signup', 'verify', 'forgotPassword', 'createAccount', 'pinCode', 'pinCodepassword', 'changeForgotpassword']);
        $this->Auth->allow(['importScheduleEnroll', 'uploadFile']);
    }
    function edit(){

		$this->viewBuilder()->layout('import');
		$this->request->data();
    	$data = $this->request->data();
		// pr($data);die;

		$this->loadModel('MasterOrganizations');
		$this->loadModel('TempUserCards');
		$organizeId = $this->MasterOrganizations->find('list', [
			'keyField' => 'id',
            'valueField' => 'org_name_th'
		])
		->where([
			'org_code != ' => ''
		])
		->toArray();
		// pr($organizeId);die;

		if(!empty($data)){

			$usercards = $this->TempUserCards->find('list', [
				'keyField' => 'card_code',
				'valueField' => 'card_code'
			])
			->where([
				'organize_id' => $data['Organize_id']
			])
			->toArray();

			$orgId = $data['Organize_id'];
			// pr($usercards);die;

			$folder = 'files';
			$item_id = '';
			$result = $this->uploadFiles($folder, $data['file_upload'], $item_id);
			// pr($result);die;

			$file['path'] = $result['path'].'/';
			$file['file_name'] = $result['uploadFileNames'][0];

			$this->loadComponent('Excel');
			$dataExcels = $this->Excel->readData($file);
			unset($dataExcels[0]);
			// unset($dataExcels[1]);
			// pr($dataExcels);die;

		}
		// pr($dataExcels);die;
		// $this->getScheduleEnroll($aa,$data)

		$this->set(compact('dataExcels', 'organizeId', 'usercards', 'tempUsers', 'orgId'));

	}
	
	public function save(){

		$this->loadModel('TempUserCards');
		
		// $this->getScheduleEnroll($dataExcels);

		$this->loadModel('MasterUserTypes');
		$userTypes = $this->MasterUserTypes->find('all')
		->select([
			'id',
			'user_type_name_th'
		])
		->toArray();
		// pr($userTypes);die;
				
			if (!empty($this->request->data)) {
				// pr($this->request->data);die;

				$dataExcel = $this->request->data["dataExcel"];
				$dataExcel = unserialize(base64_decode($dataExcel));
				$orgId = $this->request->data["orgId"];
				unset($dataExcel[1]);

				foreach($dataExcel as $key => $value){
					// pr($dataExcel);die;
					
					if(in_array($key,$this->request->data['CheckCard'])){
					$dateBirth = explode('"-"', $value['N'] );
					$dateIssue = explode('"-"', $value['O'] );
					$dateExpire = explode('"-"', $value['P'] );
					$dateBirths = $dateBirth[0].'-'.$dateBirth[1].'-'.$dateBirth[2];
					$dateIssues = $dateIssue[0].'-'.$dateIssue[1].'-'.$dateIssue[2];
					$dateExpires = $dateExpire[0].'-'.$dateExpire[1].'-'.$dateExpire[2];
					// pr($dateBirths);die;
					foreach ($userTypes as $k => $type) {
						
						if($type['user_type_name_th'] == $value['R']){
							$typeID = $type['id'];
						}
					}

					
						$tempUsers = $this->TempUserCards->newEntity();
						$tempUsers = $this->TempUserCards->patchEntity($tempUsers, $this->request->getData());
						$tempUsers['organize_id'] = $orgId;
						$tempUsers['card_code'] = $value['A'];
						$tempUsers['img_path'] = $value['B'];
						$tempUsers['prefix_name_th'] = $value['C'];
						$tempUsers['firstname_th'] = $value['D'];
						$tempUsers['lastname_th'] = $value['E'];
						$tempUsers['prefix_name_en'] = $value['F'];
						$tempUsers['firstname_en'] = $value['G'];
						$tempUsers['lastname_en'] = $value['H'];
						$tempUsers['department_name'] = $value['I'];
						$tempUsers['section_name'] = $value['J'];
						$tempUsers['position_name'] = $value['K'];
						$tempUsers['gender'] = $value['L'];
						$tempUsers['blood_group'] = $value['M'];
						$tempUsers['birthdate'] = $dateBirths;
						$tempUsers['date_issued'] = $dateIssues;
						$tempUsers['date_expiry'] = $dateExpires;
						$tempUsers['signature'] = $value['Q'];
						$tempUsers['user_type_id'] = !empty($typeID) ? $typeID : 5;
						$tempUsers['is_used'] = TRUE;
						$tempUsers['created_by'] = 1;

						// pr($tempUsers);die;
						if ($this->TempUserCards->save($tempUsers)) {
							$this->Flash->success(__('The TempUserCards has been saved.'));
						}
							$this->Flash->error(__('The TempUserCards could not be saved. Please, try again.'));
					}
				}
				return $this->redirect(['action' => 'edit']);
			}
	}
    
    function getScheduleEnroll($subject_id=null,$dataExcels=[]){
    
    }    
}