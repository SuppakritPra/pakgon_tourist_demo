<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * UserPersonals Controller
 *
 *
 * @method \App\Model\Entity\UserPersonal[] paginate($object = null, array $settings = [])
 */
class UserPersonalsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $userPersonals = $this->paginate($this->UserPersonals);
        $this->set(compact('userPersonals'));
        $this->set('_serialize', ['userPersonals']);
    }

    /**
     * View method
     *
     * @param string|null $id User Personal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $userPersonal = $this->UserPersonals->get($id, [
            'contain' => []
        ]);

        $this->set('userPersonal', $userPersonal);
        $this->set('_serialize', ['userPersonal']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $userPersonal = $this->UserPersonals->newEntity();
        if ($this->request->is('post')) {
            $userPersonal = $this->UserPersonals->patchEntity($userPersonal, $this->request->getData());
            if ($this->UserPersonals->save($userPersonal)) {
                $this->Flash->success(__('The user personal has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user personal could not be saved. Please, try again.'));
        }
        $this->set(compact('userPersonal'));
        $this->set('_serialize', ['userPersonal']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Personal id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $userPersonal = $this->UserPersonals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userPersonal = $this->UserPersonals->patchEntity($userPersonal, $this->request->getData());
            if ($this->UserPersonals->save($userPersonal)) {
                $this->Flash->success(__('The user personal has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user personal could not be saved. Please, try again.'));
        }
        $this->set(compact('userPersonal'));
        $this->set('_serialize', ['userPersonal']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Personal id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $userPersonal = $this->UserPersonals->get($id);
        if ($this->UserPersonals->delete($userPersonal)) {
            $this->Flash->success(__('The user personal has been deleted.'));
        } else {
            $this->Flash->error(__('The user personal could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
