<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * UserCards Controller
 *
 * @property \App\Model\Table\UserCardsTable $UserCards
 *
 * @method \App\Model\Entity\UserCard[] paginate($object = null, array $settings = [])
 */
class UserCardsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function initialize() {
        parent::initialize();
        // $this->Auth->allow(['signin', 'signout', 'signup', 'verify', 'forgotPassword', 'createAccount', 'pinCode', 'pinCodepassword', 'changeForgotpassword']);
        $this->Auth->allow(['importScheduleEnroll', 'uploadFile']);
    }

    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'Organizes']
        ];
        $userCards = $this->paginate($this->UserCards);

        $this->set(compact('userCards'));
        $this->set('_serialize', ['userCards']);
    }

    /**
     * View method
     *
     * @param string|null $id User Card id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $userCard = $this->UserCards->get($id, [
            'contain' => ['Users', 'Organizes']
        ]);

        $this->set('userCard', $userCard);
        $this->set('_serialize', ['userCard']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->viewBuilder()->layout('import');
		$this->request->data();
    	$data = $this->request->data();
		// pr($data);die;

		$this->loadModel('MasterOrganizations');
		$this->loadModel('TempUserCards');
		$organizeId = $this->MasterOrganizations->find('list', [
			'keyField' => 'id',
            'valueField' => 'org_name_th'
		])
		->where([
			'org_code != ' => ''
		])
		->toArray();
		// pr($organizeId);die;

		if(!empty($data)){

			$usercards = $this->TempUserCards->find('list', [
				'keyField' => 'card_code',
				'valueField' => 'card_code'
			])
			->where([
				'organize_id' => $data['Organize_id']
			])
			->toArray();

			$orgId = $data['Organize_id'];
			// pr($usercards);die;

			$folder = 'files';
			$item_id = '';
			$result = $this->uploadFiles($folder, $data['file_upload'], $item_id);
			// pr($result);die;

			$file['path'] = $result['path'].'/';
			$file['file_name'] = $result['uploadFileNames'][0];

			$this->loadComponent('Excel');
			$dataExcels = $this->Excel->readData($file);
			unset($dataExcels[0]);
			// unset($dataExcels[1]);
			// pr($dataExcels);die;

		}
		// pr($dataExcels);die;
		// $this->getScheduleEnroll($aa,$data)

		$this->set(compact('dataExcels', 'organizeId', 'usercards', 'tempUsers', 'orgId'));
    }

    public function save(){

		$this->loadModel('TempUserCards');
		
		// $this->getScheduleEnroll($dataExcels);

		$this->loadModel('MasterUserTypes');
		$userTypes = $this->MasterUserTypes->find('all')
		->select([
			'id',
			'user_type_name_th'
		])
		->toArray();
		// pr($userTypes);die;
				
			if (!empty($this->request->data)) {
				// pr($this->request->data);die;

				$dataExcel = $this->request->data["dataExcel"];
				$dataExcel = unserialize(base64_decode($dataExcel));
				$orgId = $this->request->data["orgId"];
				unset($dataExcel[1]);

				foreach($dataExcel as $key => $value){
					// pr($dataExcel);die;
					
					if(in_array($key,$this->request->data['CheckCard'])){
					$dateBirth = explode('"-"', $value['N'] );
					$dateIssue = explode('"-"', $value['O'] );
					$dateExpire = explode('"-"', $value['P'] );
					$dateBirths = $dateBirth[0].'-'.$dateBirth[1].'-'.$dateBirth[2];
					$dateIssues = $dateIssue[0].'-'.$dateIssue[1].'-'.$dateIssue[2];
					$dateExpires = $dateExpire[0].'-'.$dateExpire[1].'-'.$dateExpire[2];
					// pr($dateBirths);die;
					foreach ($userTypes as $k => $type) {
						
						if($type['user_type_name_th'] == $value['R']){
							$typeID = $type['id'];
						}
					}
						$tempUsers = $this->TempUserCards->newEntity();
						$tempUsers = $this->TempUserCards->patchEntity($tempUsers, $this->request->getData());
						$tempUsers['organize_id'] = $orgId;
						$tempUsers['card_code'] = $value['A'];
						$tempUsers['img_path'] = $value['B'];
						$tempUsers['prefix_name_th'] = $value['C'];
						$tempUsers['firstname_th'] = $value['D'];
						$tempUsers['lastname_th'] = $value['E'];
						$tempUsers['prefix_name_en'] = $value['F'];
						$tempUsers['firstname_en'] = $value['G'];
						$tempUsers['lastname_en'] = $value['H'];
						$tempUsers['department_name'] = $value['I'];
						$tempUsers['section_name'] = $value['J'];
						$tempUsers['position_name'] = $value['K'];
						$tempUsers['gender'] = $value['L'];
						$tempUsers['blood_group'] = $value['M'];
						$tempUsers['birthdate'] = $dateBirths;
						$tempUsers['date_issued'] = $dateIssues;
						$tempUsers['date_expiry'] = $dateExpires;
						$tempUsers['signature'] = $value['Q'];
						$tempUsers['user_type_id'] = !empty($typeID) ? $typeID : 5;
						$tempUsers['is_used'] = TRUE;
						$tempUsers['created_by'] = 1;

						// pr($tempUsers);die;
						if ($this->TempUserCards->save($tempUsers)) {
							$this->Flash->success(__('The TempUserCards has been saved.'));
						}
							$this->Flash->error(__('The TempUserCards could not be saved. Please, try again.'));
					}
				}
				return $this->redirect(['action' => 'add']);
			}
	}

    /**
     * Edit method
     *
     * @param string|null $id User Card id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $userCard = $this->UserCards->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userCard = $this->UserCards->patchEntity($userCard, $this->request->getData());
            if ($this->UserCards->save($userCard)) {
                $this->Flash->success(__('The user card has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user card could not be saved. Please, try again.'));
        }
        $users = $this->UserCards->Users->find('list', ['limit' => 200]);
        $organizes = $this->UserCards->Organizes->find('list', ['limit' => 200]);
        $this->set(compact('userCard', 'users', 'organizes'));
        $this->set('_serialize', ['userCard']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Card id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $userCard = $this->UserCards->get($id);
        if ($this->UserCards->delete($userCard)) {
            $this->Flash->success(__('The user card has been deleted.'));
        } else {
            $this->Flash->error(__('The user card could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function checkOrg() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $this->loadModel('MasterOrganizations');
            $MasterOrganizations = $this->MasterOrganizations->find('all', [
                        'fields' => [
                            'id',
                            'org_code',
                            'org_name_th'
                        ], 'conditions' => [
                            'org_code' => $this->request->data['organize_code']
                        ]
                    ])->toArray();
            if (!empty($MasterOrganizations)) {
                $UserCards = $this->UserCards->find('all', [
                            'fields' => [
                                'id',
                                'user_id',
                                'organize_id'
                            ], 'conditions' => [
                                'user_id' => $this->Auth->user('id'),
                                'organize_id' => $MasterOrganizations[0]['id']
                            ]
                        ])->toArray();
                if (empty($UserCards)) {
                    $MasterOrganizations['chkuser'] = false;
                    $data = json_encode($MasterOrganizations);
                    echo $data;
                } else {
                    $MasterOrganizations['chkuser'] = true;
                    $data = json_encode($MasterOrganizations);
                    echo $data;
                }
            } else {
                echo 'false';
            }
        }
    }

    public function checkEmp() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $this->loadModel('MasterOrganizations');
            $MasterOrganizations = $this->MasterOrganizations->find('all', [
                        'fields' => [
                            'id',
                            'org_code',
                            'org_name_th'
                        ], 'conditions' => [
                            'org_code' => $this->request->data['organize_code']
                        ]
                    ])->first();
            if (!empty($MasterOrganizations)) {
                $this->loadModel('TempUserCards');
                $TempUserCards = $this->TempUserCards->find('all', [
                            'conditions' => [
                                'organize_id' => $MasterOrganizations['id'],
                                'card_code' => $this->request->data['employee_val']
                            ]
                        ])->first();
                if (!empty($TempUserCards)) {
                    $UserCards = $this->UserCards->find('all', [
                                'fields' => [
                                    'id',
                                    'user_id',
                                    'organize_id'
                                ], 'conditions' => [
                                    'user_id' => $this->Auth->user('id'),
                                    'organize_id' => $MasterOrganizations['id'],
                                    'card_code' => $this->request->data['employee_val']
                                ]
                            ])->first();
                    if (empty($UserCards)) {
                        $TempUserCards['chkuser'] = false;
                        $data = json_encode($TempUserCards);
                        echo $data;
                    } else {
                        $TempUserCards['chkuser'] = true;
                        $data = json_encode($TempUserCards);
                        echo $data;
                    }
                } else {
                    echo 'false';
                }
            } else {
                echo 'false';
            }
        }
    }

    public function addCard($user_id = null) {
        $this->viewBuilder()->layout('blank');
        
        // -----------------------------ในส่วนของการ add-------------------------
        $this->autoRender = false;
        $this->loadModel('TempUserCards');
        $this->loadModel('MasterOrganizations');
        if ($this->request->is('post')) {
            // pr($this->request->data());
            // pr($this->Auth->user('id'));die;
            $birthdate = explode("/", $this->request->data['birthdate']);
            $birthdate = $birthdate['2'] . '-' . $birthdate['1'] . '-' . $birthdate['0'];
            $TempUserCards = $this->TempUserCards->find('all', [
                        'conditions' => [
                            'organize_id' => $this->request->data['organize_id'],
                            'card_code' => $this->request->data['employee'],
                            'birthdate' => $birthdate
                        ]
                    ])->first();
             //pr($TempUserCards);die;
            //0000
            if (!empty($TempUserCards)) {
                $UserCards = $this->UserCards->find('all', [
                            'conditions' => [
                                'organize_id' => $TempUserCards['organize_id'],
                                'card_code' => $TempUserCards['card_code']
                            ]
                        ])->first();
                // pr($UserCards);die;
                if (empty($UserCards)) {
                    $userCard = $this->UserCards->newEntity();
                    $userCard['organize_id'] = $TempUserCards['organize_id'];
                    $userCard['card_code'] = $TempUserCards['card_code'];
                    $userCard['img_path'] = $TempUserCards['img_path'];
                    $userCard['prefix_name_th'] = $TempUserCards['prefix_name_th'];
                    $userCard['firstname_th'] = $TempUserCards['firstname_th'];
                    $userCard['lastname_th'] = $TempUserCards['lastname_th'];
                    $userCard['prefix_name_en'] = $TempUserCards['prefix_name_en'];
                    $userCard['firstname_en'] = $TempUserCards['firstname_en'];
                    $userCard['lastname_en'] = $TempUserCards['lastname_en'];
                    $userCard['department_name'] = $TempUserCards['department_name'];
                    $userCard['section_name'] = $TempUserCards['section_name'];
                    $userCard['position_name'] = $TempUserCards['position_name'];
                    $userCard['gender'] = $TempUserCards['gender'];
                    $userCard['blood_group'] = $TempUserCards['blood_group'];
                    $userCard['birthdate'] = $TempUserCards['birthdate'];
                    $userCard['date_issued'] = $TempUserCards['date_issued'];
                    $userCard['date_expiry'] = $TempUserCards['date_expiry'];
                    $userCard['signature'] = $TempUserCards['signature'];
                    $userCard['is_used'] = $TempUserCards['is_used'];
                    $userCard['user_type_id'] = $TempUserCards['user_type_id'];
                    $userCard['created_by'] = $this->Auth->user('id');
                    $userCard['user_id'] = $this->Auth->user('id');
                    // pr($userCard);die;
                    if ($this->UserCards->save($userCard)) {
                        $TempUserCards['is_used'] = 0;
                        $this->TempUserCards->save($TempUserCards);
                        $this->loadModel('UserEntities');
                        $UserEntities = $this->UserEntities->find('all', [
                                    'conditions' => [
                                        'user_id' => $this->Auth->user('id'),
                                        'entity_code' => $this->request->data['organize_code']
                                    ]
                                ])->first();
                        if (empty($UserEntities)) {
                            $UserEntities = $this->UserEntities->newEntity();
                            $UserEntities['entity_code'] = $this->request->data['organize_code'];
                            $UserEntities['user_id'] = $this->Auth->user('id');
                            $UserEntities['create_uid'] = $this->Auth->user('id');
                            $UserEntities['update_uid'] = $this->Auth->user('id');
                            $this->UserEntities->save($UserEntities);
                        }


                        $this->Flash->success(__('Save Complete'));
                        return $this->redirect(['controller' => 'Profiles', 'action' => 'index']);
                    }
                } else {
                    $this->Flash->error(__('ลงทะเบียนไว้อยู่แล้ว'));
                    // return $this->redirect(['controller' => 'UserCards', 'action' => 'viewCard']);
                    return $this->redirect(['controller' => 'Profiles', 'action' => 'index#box-add-card']);
                }
            } else {
                $this->Flash->error(__('ไม่พบข้อมูล'));
                // return $this->redirect(['controller' => 'UserCards', 'action' => 'viewCard']);
                return $this->redirect(['controller' => 'Profiles', 'action' => 'index#box-add-card']);
            }
        }
    }

}
