<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;

class HomesController extends AppController {

    public function index() {
        #prr('<br><br><br><br><br><br><br><br><br><br><br><br><br><br>');
        #prr($this->request->getHeaderLine('Authorization'));
        #prr($this->request->getHeaderLine('Accept-Language'));
        $this->viewBuilder()->layout('blank');

        $api_getNewsHome = Configure::read('Config.apiCommunication.getNewsHome'); #prd($api_getNewsHome);
        $http = new Client();
        $data = [];
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => $this->request->getHeaderLine('Authorization')
            #'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InRlc3QiLCJwYXNzd29yZCI6InRlc3QiLCJpcCI6IjExMC43Ny4xODIuNzkifQ.xqi1iam460V-DOXJ2wxVJsAccsh4V7iLL_1slLxy99A'
            ]
        ];
        #prr($options);
        #prd($http->post($api_getNewsHome,$data,$options)->body());
        $response = json_decode($http->post($api_getNewsHome, $data, $options)->body(), '_full');
        #debug($response);
        $this->set(compact(['response']));
        //$this->set(compact(['new_home_view']));
        #---------------------------------------------------------------------------------------------------------------
        $api_getCategoriesHome = Configure::read('Config.apiCommunication.getCategoriesHome'); #debug($api_getCategoriesHome);
        $Categories = json_decode($http->post($api_getCategoriesHome, $data, $options)->body(), '_full');
        #debug($Categories);
        $this->set(compact(['Categories']));
        #---------------------------------------------------------------------------------------------------------------
    }

    public function catalogue($categories_id = null, $user = null, $master_app_id = null) {
        $this->viewBuilder()->layout('blank');
        $http = new Client();
        $data = [];
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => $this->request->getHeaderLine('Authorization')
            //'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InRlc3QiLCJwYXNzd29yZCI6InRlc3QiLCJpcCI6IjExMC43Ny4xODIuNzkifQ.xqi1iam460V-DOXJ2wxVJsAccsh4V7iLL_1slLxy99A'
            //'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTI4OCwidXNlcm5hbWUiOiJOYW5hMyIsImlwIjoiMTI3LjAuMC4xIiwiJHRvcGljIjoiTmFuYTNiODAwMjVlNWYyZTUxZDVkMmY0ZTM1Y2E4YWFkNmI1MSJ9.Up1oxsqxWASpOmj9UsAuHISlKArT2EE5NRM0bB2NLIE'
            ]
        ];
        //debug($options);
        $api_core_profile = Configure::read('Config.apiCore.profile');
        $http = new Client();
        $response = $http->post($api_core_profile, $data, $options)->body();
        $response = json_decode($response, '_full'); #prr($response);

        $Profiles = $response['result']['Data'];
        //$Profiles['UserProfiles']['organize_id'] = 10;
        //$Profiles['UserProfiles']['dept_id'] = 10;
        #debug($Profiles);

        $this->set(compact('Profiles'));
        //$Profiles = $userProfile['result']['Data'];  
        //$this->loadModel('TimeAttendant');
        //print_r($Profiles);
        #$this->request->allowMethod('post');
        #$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));prd($token);
        #$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzMCwidXNlcm5hbWUiOiJ0ZXN0dG9uZyIsImlwIjoiMTI3LjAuMC4xIn0.37hXRDEhiwjsgqR8mcgbhxnKtAof3tIDahL2P6DnQ4I";
        #$user = JwtToken::decodeToken($token);prd($user);
        #$user = 59286;
        //debug($categories_id);
//        $Contracts = $this->Contracts->find('list',
//						[
//							'conditions' => ['user_id1' => $user->user_id],
//							'keyField' => 'id',
//							'valueField' => 'user_id2',
//						]
//						);
//        $this->loadModel('Users');
//        //if (!empty($user)) {
//        $Users = $this->Users->find('all')->toArray();
//        $this->set(compact('Users'));
//        debug($Users);exit();
        $this->loadModel('MasterCategoryBanners');
        $MasterCategoryBanners = $this->MasterCategoryBanners->get($categories_id, ['conditions' => ['is_used' => true], ['master_category_id' => $categories_id],
                    'order' => 'seq_no asc'])->toArray();
        //if(!empty($MasterCategoryBanners)) $MasterCategoryBanners = $MasterCategoryBanners->toArray();
        $this->set(compact('MasterCategoryBanners'));
        //debug($MasterCategoryBanners);exit();

        if ($Profiles['UserPersonals']['id']) {
            $this->loadModel('AppAccess');
            $AppAccess = $this->AppAccess->find('all', ['conditions' => ['user_id' => $Profiles['UserPersonals']['id']]]);
            if (!empty($AppAccess))
                $AppAccess = $AppAccess->toArray();
            $this->set(compact('AppAccess'));
            //debug($AppAccess);
            //debug($AppAccess[0]['user_id']);exit();
        }

        if ($categories_id !== true) {
            $this->loadModel('MasterApps');
            $MasterApps = $this->MasterApps->find('all', ['conditions' => ['master_category_id' => $categories_id],
                'order' => 'id asc']);
            if (!empty($MasterApps))
                $MasterApps = $MasterApps->toArray();
            $this->set(compact('MasterApps'));
        }


//        
        $this->loadModel('MasterCategories');
        $MasterCategories = $this->MasterCategories->get($categories_id, ['conditions' => ['is_used' => true], ['master_category_id' => $categories_id],
                    'order' => 'id asc'])->toArray();
        $this->set(compact('MasterCategories'));
//	//debug($MasterCategories); 

        $api_getNewsHome = Configure::read('Config.apiCommunication.getNewsHome'); #prd($api_getNewsHome);
        $http = new Client();
        $data = [];
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                //'Authorization' => $this->request->getHeaderLine('Authorization')
                'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzNSwidXNlcm5hbWUiOiJuYW40IiwiaXAiOiIxMTAuNzcuMTY4LjE4IiwiJHRvcGljIjoibmFuNDhkOTgyOWFjMzY0YThkMWY2MWQ1YzE2YjM0ZmZhMjlkIn0.n5dARXh8mIH-4IXGoHs8GpJgRJ44mVrt85KDaZLwISs'
            ]
        ];
        #prd($http->post($api_getNewsHome,$data,$options)->body());
        $response = json_decode($http->post($api_getNewsHome, $data, $options)->body(), '_full');
        #debug($response);
        $this->set(compact(['response']));
        //$this->set(compact(['new_home_view']));
    }

    public function alert() {
        $this->viewBuilder()->layout('blank');

        $api_getNewsHome = Configure::read('Config.apiCommunication.notification');
        $http = new Client();
        $data = [
            'master_organization_id' => '',
            'master_department_id' => '',
            'master_section_id' => '',
            'gender' => '',
            'birthdate' => '',
            'user_type' => '',
        ];
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => $this->request->getHeaderLine('Authorization')
            ]
        ];
        $response = json_decode($http->post($api_getNewsHome, $data, $options)->body(), '_full');
        #prd($response);
    }

    public function noti() {
        $this->viewBuilder()->layout('blank');

        $api_getNotificationType = Configure::read('Config.apiCommunication.getNotificationType'); #prd($api_getNotificationType);
        $http = new Client();
        $data = [];
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => $this->request->getHeaderLine('Authorization')
            //'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzNSwidXNlcm5hbWUiOiJuYW40IiwiaXAiOiIxMTAuNzcuMTY4LjE4IiwiJHRvcGljIjoibmFuNDhkOTgyOWFjMzY0YThkMWY2MWQ1YzE2YjM0ZmZhMjlkIn0.n5dARXh8mIH-4IXGoHs8GpJgRJ44mVrt85KDaZLwISs'
            ]
        ];
        #prd($http->post($api_getNotificationType,$data,$options)->body());
        $response = json_decode($http->post($api_getNotificationType, $data, $options)->body(), '_full');
        #debug($response);
        $this->set(compact(['response']));
    }

    public function promotion($notification_type_id = null) {
        $this->viewBuilder()->layout('blank');

        $api_getNotificationMessage = Configure::read('Config.apiCommunication.getNotificationMessage'); #debug($api_getNotificationMessage);
        $http = new Client();
        $data = ['result' => [
                'notification_type_id' => $notification_type_id,
//		'master_organization_id' => '',
//		'master_department_id' => '',
//		'master_section_id' => '',
//		'gender' => '',
//		'birthdate' => '',
//		'user_type' => '',				
        ]];

        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => $this->request->getHeaderLine('Authorization')
            #'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzNSwidXNlcm5hbWUiOiJuYW40IiwiaXAiOiIxMTAuNzcuMTY4LjE4IiwiJHRvcGljIjoibmFuNDhkOTgyOWFjMzY0YThkMWY2MWQ1YzE2YjM0ZmZhMjlkIn0.n5dARXh8mIH-4IXGoHs8GpJgRJ44mVrt85KDaZLwISs'
            ]
        ];
        $response = json_decode($http->post($api_getNotificationMessage, $data, $options)->body(), '_full');
        #debug($response);
        #debug($data);
        $this->set(compact(['data']));
        $this->set(compact(['response']));
    }

    public function contact() {
        //$this->viewBuilder()->layout('blank');

        $api_Contact = Configure::read('Config.apiCore.Contact'); #debug($api_Contact);
        $http = new Client();
        $data = [];
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => $this->request->getHeaderLine('Authorization')
            //'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzNSwidXNlcm5hbWUiOiJuYW40IiwiaXAiOiIxMTAuNzcuMTY4LjE4IiwiJHRvcGljIjoibmFuNDhkOTgyOWFjMzY0YThkMWY2MWQ1YzE2YjM0ZmZhMjlkIn0.n5dARXh8mIH-4IXGoHs8GpJgRJ44mVrt85KDaZLwISs'
            ]
        ];
        #prd($api_Contact);
        #prd($http->post($api_Contact,$data,$options)->body());
        $response = json_decode($http->post($api_Contact, $data, $options)->body(), '_full');
        #debug($response);
        $this->set(compact(['response']));
    }

}
