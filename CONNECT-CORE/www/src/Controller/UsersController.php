<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Cake\Auth\AbstractPasswordHasher;

class UsersController extends AppController {

    /**
     * 
     * Function initialize make for automatically trigger when constructor
     */
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['signin', 'signout', 'signup', 'verify', 'forgotPassword', 'createAccount', 'pinCode', 'pinCodepassword', 'registersendpin', 'forgotsendpin', 'changeForgotpassword', 'reautorize']);
    }

    public function index() {
        return $this->redirect(['controller' => 'Users', 'action' => 'signout']);
    }

    /**
     * 
     * Function signin / login make for user authentication show login form
     * @author  sarawutt.b
     * @since   20108/05/24 10:49:20
     * @license Pakgon
     * @return  void
     */
    public function signin() {
        //$this->viewBuilder()->layout('blank');

        // pr("abc");die;

        // $this->viewBuilder()->layout('signin');
        // if ($this->request->is('post')) {
        //     debug("success");die;
        //     $user = $this->Auth->identify();
        //     if ($user) {
        //         $this->Auth->setUser($user);
        //         return $this->redirect($this->Auth->redirectUrl());
        //     } else {
        //         debug("fail");die;
        //         $this->Flash->error(__('Invalid username or password Please try again!'));
        //     }
        // }
    }

    /**
     * 
     * Function process submit login form for each post data 
     * @return type
     */
    public function verify() {
        //$this->viewBuilder()->layout('blank');
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!empty($data)) {
                $users = $this->Users->find('all', [
                            'conditions' => ['username' => $data['data']['username'], 'is_used' => false, 'is_active' => false],
                                ]
                        )->first();
                if (!empty($users)) {
                    if (password_verify($data['data']['password'], $users['password'])) {
                        return $this->redirect(['controller' => 'Users', 'action' => 'pin_code', $users['token']]);
                    } else {
                        $this->Flash->error(__('Username or password failed'));
                        return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
                    }
                }
            }
            if (!empty($data)) {
                $data['data']['ip'] = $this->request->clientIp();
                $api_core_signin = Configure::read('Config.apiCore.signin');
                $http = new Client();
                try {
                    $response = $http->post($api_core_signin, $data['data'])->body();
                    $response = json_decode($response, '_full');

                    if (!empty($response)) {
                        if (strtolower(trim($response['status'])) == 'success') {
                            $response['result']['user']['id'] = $response['result']['user']['user_id'];

                            /**
                             * Keep log authorization
                             */
                            $this->log('AUTHORIZE SUCCESS::', 'debug');
                            $this->log('AUTHORIZE FORM DATA:: ' . json_encode($data), 'debug');
                            $this->log('AUTHORIZE JWT TOKEN:: ' . $response['result']['token'], 'debug');
                            $this->log('AUTHORIZE TOPIC:: ' . $response['result']['topic'], 'debug');
                            $this->log('AUTHORIZE AUTH USER:: ' . json_encode($response['result']['user']), 'debug');

                            $this->Auth->setUser($response['result']['user']);
                            $statusCode = '200';
                            $RedirectApplicationURL = Configure::read('RedirectApplicationURL');
                            $topic = $response['result']['topic'];
                            $token = $response['result']['token'];
                            $this->set(compact('statusCode', 'RedirectApplicationURL', 'topic', 'token'));

                            $param = [];
                            $param['topic'] = '/topics/' . $response['result']['topic'];
                            $param['title'] = '';
                            $param['message'] = '';
                            $param['badge'] = 1;

                            $api_notification = Configure::read('Config.apiCommunication.getNotification');
                            $http = new Client();
                            $options = [
                                'headers' => [
                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                ]
                            ];
                            $response = $http->post($api_notification, $param, $options)->body();
                        } else {


                            $this->Flash->error(__('Username or password failed'));
                            return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
                        }
                    } else {
                        $this->Flash->error(__('Username or password failed'));
                        return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
                    }
                } catch (\Exception $ex) {
                    $this->log('[ERROR] occurred :: ' . $ex->getFile(), 'debug');
                    $this->log($ex, 'debug');
                    $this->Flash->error(__('Has something wrong please contact Administrator!!!'));
                    return $this->redirect(['action' => 'signin']);
                }
            } else {
                $this->Flash->error(__('Data Empty'));
                return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
            }
        }
    }

    /**
     * 
     * Function mobile re-authorized use automatically by mobile application
     * @author  sarawutt.b
     * @return  void
     */
    public function reautorize() {
        $this->viewBuilder()->layout(false);
        $reauthorizeAPI = Configure::read('Config.apiCore.reauthorize');
        $http = new Client();
        $data['data']['username'] = 'pakgon.demo';
        $data['data']['password'] = 'deio$5679@qETY';
        $data['data']['ip'] = $this->request->clientIp();

        try {
            $response = $http->post(
                            $reauthorizeAPI, $data['data'], ['headers' => [
                            'Authorization' => $this->request->getHeaderLine('Authorization'),
                            'Accept-Language' => $this->request->getHeaderLine('Accept-Language')
                ]])->body();


            $response = json_decode($response, true);
            if (is_array($response) && array_key_exists('result', $response) && array_key_exists('user', $response['result']) && !empty($response['result']['user'])) {
                $this->loadModel('UserPersonals');
                $countUsers = $this->Users->find()->where(['id' => $response['result']['user']['user_id'], 'is_used' => true, 'is_active' => true])->all()->count();
                $countUserPersonal = $this->UserPersonals->find()->where(['user_id' => $response['result']['user']['user_id']])->all()->count();
                if (($countUsers > 0) && ($countUserPersonal > 0)) {
                    $response['result']['user']['id'] = $response['result']['user']['user_id'];
                    $this->log('REAUTHORIZE SUCCESS::', 'debug');
                    $this->log('REAUTHORIZE JWT:: ' . $this->request->getHeaderLine('Authorization'), 'debug');
                    $this->log('REAUTHORIZE AUTH USER:: ' . json_encode($response['result']['user']), 'debug');

                    $this->Auth->setUser($response['result']['user']);
                    $referer = ($this->request->hasHeader('Referer')) ? $this->request->getHeaderLine('Referer') : Configure::read('RedirectApplicationURL');
                    return $this->redirect($referer);
                } else {
                    $this->log('REAUTHORIZE FAILED::', 'debug');
                    $this->log('REAUTHORIZE JWT:: ' . $this->request->getHeaderLine('Authorization'), 'debug');
                    $this->log('REAUTHORIZE MESSAGE:: ' . $response['result']['message'], 'debug');
                    return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
                }
            } else {
                return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
            }


//            $this->loadModel('UserPersonals');
//            $countUsers = $this->Users->find()->where(['id' => $response['result']['user']['user_id'], 'is_used' => true, 'is_active' => true])->all()->count();
//            $countUserPersonal = $this->UserPersonals->find()->where(['user_id' => $response['result']['user']['user_id']])->all()->count();
//
//            if (($countUsers > 0) && ($countUserPersonal > 0)) {
//                $response['result']['user']['id'] = $response['result']['user']['user_id'];
//                $this->Auth->setUser($response['result']['user']);
//                $referer = ($this->request->hasHeader('Referer')) ? $this->request->getHeaderLine('Referer') : Configure::read('RedirectApplicationURL');
//                return $this->redirect($referer);
//            } else {
//                return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
//            }
//            if (array_key_exists('result', $response) && !empty($response['result']['user'])) {
//
//                $this->log('USER ID :: ' . $response['result']['user']['user_id']);
//                //Check for token is expires
//                $params = ['owner_id' => $response['result']['user']['user_id']];
//                $verifyResponse = $http->post(Configure::read('OAUTH2_PROVIDER.TOKEN_VERIFY'), $params)->body();
//
//                $this->log(' verify res :: ' . $verifyResponse);
//                $verifyResponse = json_decode($verifyResponse, true);
//                $tokenIsExpired = (array_key_exists('result', $verifyResponse['result']) && $verifyResponse['result']['result']['is_expires']);
//
//                $this->log('Reouth :: ' . $tokenIsExpired);
//                if (!$tokenIsExpired) {
//                    $response['result']['user']['id'] = $response['result']['user']['user_id'];
//                    $this->Auth->setUser($response['result']['user']);
//                    $referer = ($this->request->hasHeader('Referer')) ? $this->request->getHeaderLine('Referer') : Configure::read('RedirectApplicationURL');
//
//                    return $this->redirect($referer);
//                } else {
//
//                    $this->log('A');
//                    return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
//                }
//            } else {
//                $this->log('B');
//                return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
//            }
        } catch (\Exception $ex) {
            $this->log('[ERROR] occurred :: ' . $ex->getFile(), 'debug');
            $this->log($ex, 'debug');
            return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
        }
    }

    public function signup() {
        $isError;
        $isSave;
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->loadModel('UserPersonals');

            //Check username is unique
            if ($this->Users->exists(['username' => $this->request->data['username']])) {
                $this->Flash->error(__('Username {0} has already exists please change another.', $this->request->data['username']));
                //return $this->redirect($this->referer());
                $isError = 'true';
            }

            //Check Email dupplicated
            if ($this->UserPersonals->exists(['email' => $this->request->data['email']])) {
                $this->Flash->error(__('Email {0} has already exists please change another.', $this->request->data['email']));
                // return $this->redirect($this->referer());
                $isError = 'true';
            }

            if (empty($isError)) {
                $users = $this->Users->newEntity();
                $user_personals = $this->UserPersonals->newEntity();

                $this->request->data['master_country_id'] = 1; //Thailand
                $this->request->data['created_by'] = 0;
                $this->request->data['is_used'] = false;
                $this->request->data['is_active'] = false;
                $this->request->data['dynamic_key'] = 'dynamic_key';

                $this->request->data['dynamic_key_expiry'] = date('Y-m-d', strtotime('+3 day'));
                $this->request->data['token'] = Security::hash($this->request->data['username'] . date('Y-m-d h:i:s'), 'md5', true);
                $this->request->data['token_expiry'] = date('Y-m-d', strtotime('+3 day'));
                $digits = 4;
                $this->request->data['pin_code'] = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

                $this->request->data['accept'] = true;
                if (!empty($this->request->data['accept'])) {
                    $isSave = 'true';
                    $this->Users->getConnection()->begin();
                    
                    //Save to Users
                    $users = $this->Users->patchEntity($users, $this->request->getData());
                   
                    if(!$this->Users->save($users)){
                        $isSave = 'false';
                    }
                    //Save to User Personals
                    $user_personals['master_country_id'] = 1; //Thailand
                    $user_personals['master_province_id'] = $this->request->data['master_province_id'];
                    $user_personals['user_id'] = $users['id'];
                    $user_personals['firstname_th'] = $this->request->data['firstname'];
                    $user_personals['lastname_th'] = $this->request->data['lastname'];
                    $user_personals['created_by'] = 1;
                    $user_personals = $this->UserPersonals->patchEntity($user_personals, $this->request->getData());
                
                    if(!$this->UserPersonals->save($user_personals)){

                        $isSave = 'false';
                    }

                    if( $isSave == 'true'){
                        $this->Users->getConnection()->commit();
                        $data_notification = [];
                        $data_notification['email'] = $this->request->data['email'];
                        $data_notification['pin_code'] = $this->request->data['pin_code'];
                        $this->notification($data_notification);

                        return $this->redirect(['controller' => 'Users', 'action' => 'pinCode/' . $users['token']]);
                    }else{

                        $this->Users->getConnection()->rollback();
                        $this->Flash->error(__('The article could not be saved. Please, try again.'));
                    }             
  
                   
                } else if (empty($this->request->data['accept'])) {
                    $this->Flash->error(__('Please accept the Terms of Use.'));
                    return $this->redirect(['action' => 'signup']);
                }
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }//.if(empty($isError))
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    #---------------------------------------------------------------------------------------------------
    #Signup

    public function createAccount() {


//        if ($this->request->is('post')) {
//            $this->loadModel('UserPersonals');
//            
//            //Check username is unique
//            if ($this->Users->exists(['username' => $this->request->data['username']])) {
//                $this->Flash->error(__('Username {0} has already exists please change another.', $this->request->data['username']));
//                $this->request->data = $this->request->data;
//                return $this->redirect($this->referer());
//            }
//
//            //Check Email dupplicated
//            if ($this->UserPersonals->exists(['email' => $this->request->data['email']])) {
//                $this->Flash->error(__('Email {0} has already exists please change another.', $this->request->data['email']));
//                $this->request->data = $this->request->data;
//                return $this->redirect($this->referer());
//            }
//
//            $users = $this->Users->newEntity();
//            $user_personals = $this->UserPersonals->newEntity();
//
//            $this->request->data['master_country_id'] = 1; //Thailand
//            $this->request->data['created_by'] = 0;
//            $this->request->data['is_used'] = false;
//            $this->request->data['is_active'] = false;
//            $this->request->data['dynamic_key'] = 'dynamic_key';
//
//            $this->request->data['dynamic_key_expiry'] = date('Y-m-d', strtotime('+3 day'));
//            $this->request->data['token'] = Security::hash($this->request->data['username'] . date('Y-m-d h:i:s'), 'md5', true);
//            $this->request->data['token_expiry'] = date('Y-m-d', strtotime('+3 day'));
//            $digits = 4;
//            $this->request->data['pin_code'] = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
//
//            $this->request->data['accept'] = true;
//            if (!empty($this->request->data['accept'])) {
//                //Save to Users
//                $users = $this->Users->patchEntity($users, $this->request->getData());
//                $data_notification = [];
//                $data_notification['email'] = $this->request->data['email'];
//                $data_notification['pin_code'] = $this->request->data['pin_code'];
//                $this->notification($data_notification);
//                $this->Users->save($users);
//
//                //Save to User Personals
//                $user_personals['master_country_id'] = 1; //Thailand
//                $user_personals['master_province_id'] = $this->request->data['master_province_id'];
//                $user_personals['user_id'] = $users['id'];
//                $user_personals['firstname_th'] = $this->request->data['firstname'];
//                $user_personals['lastname_th'] = $this->request->data['lastname'];
//                $user_personals['created_by'] = 1;
//                $user_personals = $this->UserPersonals->patchEntity($user_personals, $this->request->getData());
//                $this->UserPersonals->save($user_personals);
//                return $this->redirect(['controller' => 'Users', 'action' => 'pinCode/' . $users['token']]);
//            } else if (empty($this->request->data['accept'])) {
//                $this->Flash->error(__('Please accept the Terms of Use.'));
//                return $this->redirect(['action' => 'signup']);
//            }
//            $this->Flash->error(__('The article could not be saved. Please, try again.'));
//        }
    }

//    public function notification($data = null) {
//        if (!empty($data)) {
//            $verify_code = $data['pin_code'];
//            $from_email = ['support@pakgon.com' => 'Support'];
//            $to_emails = [$data['email']];
//
//            $email = new Email();
//            $email->transport('gmail');
//
//            try {
//                $email->template('notification_signup', 'connect');
//                $email->from($from_email);
//                $email->to($to_emails);
//                $email->subject('Signup Connect Verify');
//                $email->emailFormat('html');
//                $email->viewVars(compact('verify_code'));
//                #$email->send($message);
//                $email->send();
//            } catch (Exception $e) {
//                echo 'Exception : ', $e->getMessage(), "\n";
//            }
//
//            #$this->httpStatusCode = 200;
//            #$this->apiResponse['message'] = 'Signup Connect Completed!!';
//        }
//    }

    public function notification($data = null) {
        if (!empty($data)) {
            $verifyCode = $data['pin_code'];
            $toEmails = $data['email'];
            $mailContent = array(
                'Messages' => array(
                    array(
                        "From" => array(
                            "Email" => Configure::read('MAILLER.FROM_EMAIL'),
                            "Name" => Configure::read('MAILLER.FROM_NAME')
                        ),
                        "To" => array(array(
                                "Email" => $toEmails
                            )),
                        'Subject' => 'Signup Connect Verify',
                        'TextPart' => 'Verification Code: ' . $verifyCode,
                        'HtmlPart' => 'Verification Code: ' . $verifyCode,
                    )
                )
            );

            $http = new Client();
            try {
                $response = $http->post(Configure::read('MAILLER.PROVIDER_URL'), json_encode($mailContent), [
                            'auth' => ['username' => Configure::read('MAILLER.USERNAME'), 'password' => Configure::read('MAILLER.PASSWORD')]
                                ]
                        )->body();
                return $response;
            } catch (Exception $e) {
                echo 'Exception : ', $e->getMessage(), "\n";
                return $e->getMessage();
            }

            #$this->httpStatusCode = 200;
            #$this->apiResponse['message'] = 'Signup Connect Completed!!';
        }
    }

    #---------------------------------------------------------------------------------------------------    

    function validateAccount($data = null) {

        $error = [];

        if (!empty($error)) {
            $this->set('error', $error);
            return false;
        } else {
            return true;
        }
    }

    // public function notification()
    // {
    //     $this->viewBuilder()->layout('blank');
    // }
    #Verify Pin Code
    public function pinCode($token = null) {
        //$this->viewBuilder()->layout('blank');
        $data = $this->request->data();
        if (!empty($data)) {

            $api_core_verify_pin_code = Configure::read('Config.apiCore.verifyPinCode');
            $http = new Client();
            $users = $this->Users->find('all', [
                        'conditions' => [
                            'Users.token' => $data['token'],
                        ]
                    ])->first();
            #$response = json_decode($http->post($api_core_verify_pin_code,$data)->body(),'_full');

            if (!empty($users)) {
                $usercode = $data['pin_code_1'] . $data['pin_code_2'] . $data['pin_code_3'] . $data['pin_code_4'];
                if ($users['pin_code'] == $usercode) {
                    $this->Flash->success(__('Verify Completed.'));
                    $users['is_used'] = true;
                    $users['is_active'] = true;
                    $users = $this->Users->patchEntity($users, $this->request->getData());
                    $this->Users->save($users);
                    return $this->redirect(['controller' => 'Users', 'action' => 'signin']);
                } else {
                    $this->Flash->error(__('Pin Code Invalid'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'pinCode/' . $token]);
                }
            }
        }
        $this->set(array('token' => $token, '_serialize' => array('token')));
    }

    /**
     * 
     * Function user forgot password
     * @return void
     */
    public function forgotPassword() {
        $this->viewBuilder()->layout('blank');
        $this->loadModel('UserPersonals');
        if ($this->request->is('post')) {
            $hasher = new DefaultPasswordHasher();
            $email = $this->request->data['email'];
            $user_personals = $this->UserPersonals->find('all', [
                        'conditions' => [
                            'UserPersonals.email' => $email
                        ]
                    ])->first();
            if (!empty($user_personals)) {

                $users = $this->Users->find('all', [
                            'conditions' => [
                                'Users.id' => $user_personals['user_id']
                            ]
                        ])->first();
                $digits = 4;
                $users['pin_pass'] = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
                $token = $users['token'];

                //--------------------ตัวส่ง Email ---------------------------------------------------------
                // pr($users['username']);die;
                $data_notification = [];
                $data_notification['email'] = $this->request->data['email'];
                $data_notification['pin_code'] = $users['pin_pass'] . '<br>Username:' . $users['username'];
                $this->notification($data_notification);
                //----------------------------------------------------------------------------------------
                $this->Users->save($users);

                $this->Flash->success(__('send password to email success'));
                return $this->redirect(['action' => 'pinCodepassword/' . $token]);
            } else {
                $this->Flash->error(__('Invalid Email.'));
                return $this->redirect(['action' => 'forgot-password']);
            }
        }
    }

    public function pinCodepassword($token = null) {
        $this->viewBuilder()->layout('blank');
        $users = $this->Users->find('all', [
                    'conditions' => [
                        'Users.token' => $token
                    ]
                ])->first();

        if ($this->request->is('post')) {
            $data = $this->request->data();
            $data['pin_pass'] = $data['pin_code_1'] . $data['pin_code_2'] . $data['pin_code_3'] . $data['pin_code_4'];
            if ($data['pin_pass'] == $users['pin_pass']) {
                $this->Flash->success(__('Pin Completed.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'changeForgotpassword/' . $token]);
            }
            $this->Flash->error(__('Pin Invalid'));
            return $this->redirect(['controller' => 'Users', 'action' => 'pinCodepassword/' . $token]);
        }
        $this->set(array('token' => $token, '_serialize' => array('token')));
    }

    /**
     * 
     * Function user request to change for own password
     * @return success redirect to login page
     */
    public function changePassword() {

        $id = $this->Auth->user('id');
        $this->viewBuilder()->layout('blank');
        $users = $this->Users->get($id);
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $hasher = new DefaultPasswordHasher();
            //$password = $this->request->data['password'];
            //$confirm_password = $this->request->data['confirm_password'];
            $oldpassword = $this->request->data['oldpassword'];
            //pr($this->request->data['password']);
            //$this->request->data['password'] = $hasher->hash($this->request->data['password']);
            $newPassword['password'] = $this->request->data['password'];
            //------- เทียบ password ที่เข้ารหัส ---------------password_verify($password,hashed_password)---------------------------
            //pr($newPassword['password']);
            if (password_verify($oldpassword, $users['password'])) {
                $users = $this->Users->patchEntity($users, $newPassword);
                $this->Users->save($users);
                $this->Flash->success(__('Password changed successfully.'));
                return $this->redirect(['action' => 'signout']);
            } else {
                $this->Flash->error(__('Invalid password.'));
                return $this->redirect(['action' => 'change-password']);
            }
        }
    }

    public function changeForgotpassword($token = null) {
        $this->viewBuilder()->layout('blank');
        $users = $this->Users->find('all', [
                    'conditions' => [
                        'Users.token' => $token
                    ]
                ])->first();
        if ($this->request->is('post')) {
            $hasher = new DefaultPasswordHasher();
            // $password = $this->request->data['password'];
            // $confirm_password = $this->request->data['confirm_password'];
            $newPassword['password'] = $this->request->data['password'];
            //------- เทียบ password ที่เข้ารหัส ---------------password_verify($password,hashed_password)---------------------------
            $users = $this->Users->patchEntity($users, $newPassword);
            $this->Users->save($users);
            $this->Flash->success(__('You are now logged out.'));
            return $this->redirect(['action' => 'signout']);
        }
        $this->set(array('token' => $token, '_serialize' => array('token')));
    }

    #---------------------------------------------------------------------------------------------------        

    public function forgotsendpin($token = null) {
        $this->loadModel('UserPersonals');
        $users = $this->Users->find('all', [
                    'conditions' => [
                        'Users.token' => $token
                    ]
                ])->first();
        $digits = 4;
        $users['pin_pass'] = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        $user_personals = $this->UserPersonals->find('all', [
                    'conditions' => [
                        'UserPersonals.user_id' => $users['id']
                    ]
                ])->first();
        //--------------------ตัวส่ง Email ---------------------------------------------------------
        //pr($user_personals);die;
        $data_notification = [];
        $data_notification['email'] = $user_personals['email'];
        $data_notification['pin_code'] = $users['pin_pass'] . '<br>' . " Username: " . $users['username'];
        $this->notification($data_notification);
        //----------------------------------------------------------------------------------------
        if ($this->Users->save($users)) {
            $this->Flash->success(__('send pin success.'));
            return $this->redirect(['action' => 'pin_codepassword/' . $token]);
        }
        $this->Flash->success(__('send pin false.'));
        return $this->redirect(['action' => 'pin_codepassword/' . $token]);
    }

    public function registersendpin($token = null) {
        $this->loadModel('UserPersonals');
        $users = $this->Users->find('all', [
                    'conditions' => [
                        'Users.token' => $token
                    ]
                ])->first();
        $digits = 4;
        $users['pin_code'] = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        $user_personals = $this->UserPersonals->find('all', [
                    'conditions' => [
                        'UserPersonals.user_id' => $users['id']
                    ]
                ])->first();
        //--------------------ตัวส่ง Email ---------------------------------------------------------
        //pr($user_personals);die;
        $data_notification = [];
        $data_notification['email'] = $user_personals['email'];
        $data_notification['pin_code'] = $users['pin_code'] . '<br>' . " Username: " . $users['username'];
        $this->notification($data_notification);
        //----------------------------------------------------------------------------------------
        if ($this->Users->save($users)) {
            $this->Flash->success(__('send pin success.'));
            return $this->redirect(['action' => 'pin_code/' . $token]);
        }
        $this->Flash->success(__('send pin false.'));
        return $this->redirect(['action' => 'pin_code/' . $token]);
    }

    /**
     * 
     * Function user logout / signout
     * @author sarawutt.b
     * @since  2018/05/22 16:44:20
     * @license PAKGON
     * @return void
     */
    public function signout() {
        $http = new Client();
        try {
            $result = $http->delete(Configure::read('OAUTH2_PROVIDER.TOKEN_DETETE') . '/' . $this->Auth->user('id'))->body();
            $this->Flash->success(__('You are now logged out.'));
            return $this->redirect($this->Auth->logout());
        } catch (\Exception $ex) {
            $this->log('[ERROR] occurred :: ' . $ex->getFile(), 'debug');
            $this->log($ex, 'debug');
            return $this->redirect($this->Auth->logout());
        }
    }

}
