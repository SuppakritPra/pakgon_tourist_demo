<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserNamesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserNamesTable Test Case
 */
class UserNamesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserNamesTable
     */
    public $UserNames;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_names',
        'app.faculties',
        'app.roles',
        'app.name_prefixes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserNames') ? [] : ['className' => UserNamesTable::class];
        $this->UserNames = TableRegistry::get('UserNames', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserNames);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
