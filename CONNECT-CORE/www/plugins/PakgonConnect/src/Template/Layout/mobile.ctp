<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo $this->Configure->read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>
        <?php
        echo $this->Html->css([
            'PakgonConnect./plugins/bootstrap/dist/css/bootstrap.min',
            'PakgonConnect./plugins/font-awesome/css/font-awesome.min',
            'PakgonConnect./plugins/Ionicons/css/ionicons.min',
            'PakgonConnect./plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min',
            'PakgonConnect./plugins/select2_v4/dist/css/select2.min',
            'PakgonConnect./plugins/select2_v4/dist/css/select2-bootstrap4.min',
            'PakgonConnect./plugins/OwlCarousel2/dist/assets/owl.carousel',
            'PakgonConnect./plugins/OwlCarousel2/dist/assets/owl.theme.default.min',
            'PakgonConnect./plugins/iCheck/all',
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
            'PakgonConnect./plugins/nprogress/nprogress.min',
            'PakgonConnect./plugins/toastr/build/toastr.min',
            'PakgonConnect./css/pakgon.main',
        ]);
        ?>
        <?php echo $this->fetch('css'); ?>
        <?php echo $this->Html->script('PakgonConnect./plugins/ckeditor/ckeditor'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        echo $this->Html->script([
            'PakgonConnect./plugins/jquery/jquery.min',
            'PakgonConnect./plugins/bootstrap/dist/js/bootstrap.bundle.min'
        ]);
        ?>   
    </head>
    <body>
        <!-- -->
        <div class="container">
            <?php echo $this->Flash->render(); ?>
            <?php echo $this->fetch('content'); ?>
            <?php echo $this->element('modal/modal'); ?>
        </div>
        <!-- ./wrapper -->

        <?php
        echo $this->Html->script([
            'PakgonConnect./plugins/slimScroll/jquery.slimscroll.min',
            'PakgonConnect./plugins/fastclick/fastclick.min',
            'PakgonConnect./plugins/jquery-validation/jQuery.start.min',
            'PakgonConnect./plugins/jquery-validation/jQuery.validate.min',
            'PakgonConnect./plugins/jquery-validation/jQuery.validate.addValidate',
            'PakgonConnect./plugins/select2_v4/dist/js/select2.min',
            'PakgonConnect./plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
            'PakgonConnect./plugins/iCheck/icheck.min',
            'PakgonConnect./plugins/nprogress/nprogress.min',
            'PakgonConnect./plugins/toastr/build/toastr.min',
            'PakgonConnect./plugins/moment/min/moment.min',
            'PakgonConnect./plugins/OwlCarousel2/dist/owl.carousel.min',
         
        ]);
        ?>

        <?php echo $this->element('utility/utilityScript'); ?>
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
