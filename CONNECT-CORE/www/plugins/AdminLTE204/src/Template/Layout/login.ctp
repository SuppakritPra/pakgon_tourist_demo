<?php

use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo Configure::read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>
        <?php
        echo $this->Html->css([
            'AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min',
            'AdminLTE204./plugins/font-awesome/css/font-awesome.min',
            'AdminLTE204./plugins/Ionicons/css/ionicons.min.css',
            'AdminLTE204.AdminLTE.min',
            'AdminLTE204./plugins/iCheck/square/blue',
            'AdminLTE204./plugins/select2/dist/css/select2.min',
            '/vendor/nprogress/nprogress.min',
            '/vendor/toastr/build/toastr.min',
            'main.style'
        ]);
        ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'); ?>
        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>
        <?php
        echo $this->Html->script([
            'AdminLTE204./plugins/jquery/dist/jquery.min',
            'AdminLTE204./plugins/bootstrap/dist/js/bootstrap.min'
        ]);
        ?>
    </head>
    <body class="container login-container">
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>

        <?php
        echo $this->Html->script([
            'AdminLTE204./plugins/jquery-slimscroll/jquery.slimscroll.min',
            'AdminLTE204./plugins/fastclick/lib/fastclick',
            'AdminLTE204./plugins/iCheck/icheck.min',
            '/vendor/jquery-validation/jQuery.start.min',
            '/vendor/jquery-validation/jQuery.validate.min',
            '/vendor/nprogress/nprogress.min',
            '/vendor/toastr/build/toastr.min',
            'AdminLTE204./plugins/select2/dist/js/select2.full.min'
        ]);
        ?>
        <?php echo $this->element('modal/modal'); ?>
        <?php //echo $this->element('utility/toastNotification'); ?>
        <?php echo $this->element('utility/utilityScript'); ?>
        <?php
        //Check for special javascript attachment
        if (!empty($customJs)) {
            $customJs = is_array($customJs) ? $customJs : array($customJs);
            foreach ($customJs as $js) {
                echo $this->Html->script($js);
            }
        }
        ?>
    </body>
</html>
