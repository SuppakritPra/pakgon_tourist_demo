<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!--User panel-->
        <?php echo $this->element('theme/aside/user-panel'); ?>

        <!--Form search-->
        <?php echo $this->element('theme/aside/form'); ?>

        <!-- Side bare menu-->
        <?php echo $this->element('theme/aside/sidebar-menu'); ?>
    </section>
    <!-- /.sidebar -->
</aside>