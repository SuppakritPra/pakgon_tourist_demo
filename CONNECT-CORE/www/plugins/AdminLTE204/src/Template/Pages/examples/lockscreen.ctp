<?php $this->layout = 'AdminLTE204.lockscreen'; ?>
<?php

use Cake\Core\Configure;
?>
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="<?php echo $this->Url->build('/'); ?>"><?php echo Configure::read('Theme.logo.large'); ?></a>
    </div>
    <!-- User name -->
    <div class="lockscreen-name">John Doe</div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <?php echo $this->Html->image('user1-128x128.jpg', ['alt' => 'User Image']); ?>
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <?php echo $this->Form->create('User', ['class' => 'lockscreen-credentials']); ?>
        <div class="input-group">
            <input type="password" class="form-control" placeholder="password">

            <div class="input-group-btn">
                <button type="button" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
        </div>
        </form>
        <?php echo $this->Form->end(); ?>
        <!-- /.lockscreen credentials -->

    </div>
    <!-- /.lockscreen-item -->
    <div class="help-block text-center">
        Enter your password to retrieve your session
    </div>
    <div class="text-center">
        <a href="login.html">Or sign in as a different user</a>
    </div>
    <div class="lockscreen-footer text-center">
        Copyright &copy; 2014-2016 <b><a href="https://adminlte.io" class="text-black">Almsaeed Studio</a></b><br>
        All rights reserved
    </div>
</div>
<!-- /.center -->
