//--------------------------------------------------------------------

function ChangeCountry(model) {

    var country_id = $('#country_id option:selected').val();
    var province_id = '';
    var district_id = '';
    $('#province_id').load('../../../../ApiCore/getProvince/' + country_id + '/' + model);
    $('#district_id').load('../../../../ApiCore/getDistrict/' + province_id + '/' + model);
    $('#zone_id').load('../../../../ApiCore/getZone/' + district_id + '/' + model);
    $('#zipcode').val('');

}

function ChangeProvince(model) {

    var province_id = $('#province_id option:selected').val();
    var district_id = '';
    $('#district_id').load('../../../../ApiCore/getDistrict/' + province_id + '/' + model);
    $('#zone_id').load('../../../../ApiCore/getZone/' + district_id + '/' + model);
    $('#zipcode').val('');

}

function ChangeDistrict(model) {

    var district_id = $('#district_id option:selected').val();
    $('#zone_id').load('../../../../ApiCore/getZone/' + district_id + '/' + model);
    $('#zipcode').val('');

}

function ChangeZone(model) {

    var zone_id = $('#zone_id').val();
    //$('#zipcode').val('../../../../ApiCore/getZipCode/' + id);
    $.post('../../../../ApiCore/getZipCode/' + zone_id + '/' + model, {}, function (data) {
        $('#zipcode').val(data);
    });

}