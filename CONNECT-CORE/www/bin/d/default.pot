# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2018-08-03 07:01+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/AppController.php:325
msgid "No file Selected"
msgstr ""

#: Controller/ProfilesController.php:132
msgid "Update Complete."
msgstr ""

#: Controller/ProfilesController.php:136
msgid "Email Address already used by others."
msgstr ""

#: Controller/ProfilesController.php:138
msgid "Not update. Please, try again."
msgstr ""

#: Controller/TestsController_backup.php:139
#: Controller/UserCardsController.php:178
msgid "The TempUserCards has been saved."
msgstr ""

#: Controller/TestsController_backup.php:141
#: Controller/UserCardsController.php:180
msgid "The TempUserCards could not be saved. Please, try again."
msgstr ""

#: Controller/UserCardsController.php:201
msgid "The user card has been saved."
msgstr ""

#: Controller/UserCardsController.php:205
msgid "The user card could not be saved. Please, try again."
msgstr ""

#: Controller/UserCardsController.php:224
msgid "The user card has been deleted."
msgstr ""

#: Controller/UserCardsController.php:226
msgid "The user card could not be deleted. Please, try again."
msgstr ""

#: Controller/UserCardsController.php:396
msgid "Save Complete"
msgstr ""

#: Controller/UserCardsController.php:400
msgid "ลงทะเบียนไว้อยู่แล้ว"
msgstr ""

#: Controller/UserCardsController.php:405
msgid "ไม่พบข้อมูล"
msgstr ""

#: Controller/UserPersonalsController.php:52;76
msgid "The user personal has been saved."
msgstr ""

#: Controller/UserPersonalsController.php:56;80
msgid "The user personal could not be saved. Please, try again."
msgstr ""

#: Controller/UserPersonalsController.php:97
msgid "The user personal has been deleted."
msgstr ""

#: Controller/UserPersonalsController.php:99
msgid "The user personal could not be deleted. Please, try again."
msgstr ""

#: Controller/UserprofilesController.php:63
msgid "The user profile has been saved."
msgstr ""

#: Controller/UserprofilesController.php:67
msgid "The user profile could not be saved. Please, try again."
msgstr ""

#: Controller/UserprofilesController.php:125;127;130;133;135
#: Template/UserProfiles/profile_setting.ctp:119;125;170;191;198;206
msgid " --- Please Select --- "
msgstr ""

#: Controller/UserprofilesController.php:151
msgid "The user profile has been deleted."
msgstr ""

#: Controller/UserprofilesController.php:153
msgid "The user profile could not be deleted. Please, try again."
msgstr ""

#: Controller/UsersController.php:94;98
msgid "Username or password failed"
msgstr ""

#: Controller/UsersController.php:104
msgid "Has something wrong please contact Administrator!!!"
msgstr ""

#: Controller/UsersController.php:108
msgid "Data Empty"
msgstr ""

#: Controller/UsersController.php:275
#: Template/Profiles/index.ctp:681
msgid "This email is already in the system."
msgstr ""

#: Controller/UsersController.php:278
msgid "Please accept the Terms of Use."
msgstr ""

#: Controller/UsersController.php:281
msgid "The article could not be saved. Please, try again."
msgstr ""

#: Controller/UsersController.php:386
msgid "Verify Completed."
msgstr ""

#: Controller/UsersController.php:393
msgid "Pin Code Invalid"
msgstr ""

#: Controller/UsersController.php:437
msgid "send password to email success"
msgstr ""

#: Controller/UsersController.php:440
msgid "Invalid Email."
msgstr ""

#: Controller/UsersController.php:458
msgid "Pin Completed."
msgstr ""

#: Controller/UsersController.php:461
msgid "Pin Invalid"
msgstr ""

#: Controller/UsersController.php:489;513;593
msgid "You are now logged out."
msgstr ""

#: Controller/UsersController.php:492
msgid "Invalid password."
msgstr ""

#: Controller/UsersController.php:544;574
msgid "send pin success."
msgstr ""

#: Controller/UsersController.php:547;577
msgid "send pin false."
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:21
#: Template/UserCards/create_cards.ctp:22
msgid "ไอดีใช้งาน"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:22
#: Template/UserCards/create_cards.ctp:23
#: Template/UserCards/view_card.ctp:162
msgid "Company Code"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:29
#: Template/UserCards/view_card.ctp:169
msgid "Ref 1"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:36
#: Template/UserCards/view_card.ctp:177
msgid "Ref 2"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:42
#: Template/UserCards/view_card.ctp:185
msgid "Check"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:74
#: Template/UserCards/view_card.ctp:515
msgid "Register with"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:74
#: Template/UserCards/view_card.ctp:516
msgid "already"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:89
#: Template/UserCards/view_card.ctp:530
msgid "Data not found"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:114
#: Template/UserCards/view_card.ctp:550
msgid "Registered"
msgstr ""

#: Template/Element/card_from/from_add_card.ctp:126
#: Template/UserCards/view_card.ctp:564
msgid "\"Did not find the information you need\", Please enter the correct information and press the check."
msgstr ""

#: Template/Element/modal/modal.ctp:15;35
#: Template/Element/modal/modal.min.ctp:1
#: View/Helper/BootstrapHelper.php:126;136
msgid "Close"
msgstr ""

#: Template/Element/modal/modal.ctp:34
#: Template/Element/modal/modal.min.ctp:1
msgid "Confirm"
msgstr ""

#: Template/Element/utility/breadcrumb.ctp:14
#: View/Helper/HtmlHelper.php:795
msgid "Home"
msgstr ""

#: Template/Element/utility/breadcrumb.ctp:24
msgid "Control panel"
msgstr ""

#: Template/Element/utility/pagination.ctp:9
#: Template/UserCards/index.ctp:87
#: Template/Users/index.ctp:89
msgid "first"
msgstr ""

#: Template/Element/utility/pagination.ctp:10
#: Template/UserCards/index.ctp:88
#: Template/Users/index.ctp:90
msgid "previous"
msgstr ""

#: Template/Element/utility/pagination.ctp:12
#: Template/UserCards/index.ctp:90
#: Template/Users/index.ctp:92
msgid "next"
msgstr ""

#: Template/Element/utility/pagination.ctp:13
#: Template/UserCards/index.ctp:91
#: Template/Users/index.ctp:93
msgid "last"
msgstr ""

#: Template/Element/utility/pagination.ctp:17
#: Template/UserCards/index.ctp:93
#: Template/Users/index.ctp:95
msgid "Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:12;15
msgid "Are you sure process the action ?"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:13;14
msgid "Application process status."
msgstr ""

#: Template/Element/utility/utilityScript.ctp:254
#: Template/Tests_backup/edit.ctp:37
#: Template/UserCards/add.ctp:37
#: View/Helper/PermissionHelper.php:377;389
msgid "Add"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:257
msgid "View more infomation"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:258
#: Template/UserCards/index.ctp:78
#: Template/Users/index.ctp:80
msgid "Edit"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:259
#: Template/UserCards/edit.ctp:11
#: Template/UserCards/index.ctp:79
#: Template/Users/index.ctp:81
msgid "Delete"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:266
msgid "Are you sure for delete ?"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:280
msgid "Successfully"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:304;305
#: Template/Layout/error.ctp:43
#: View/Helper/PermissionHelper.php:47;60
msgid "Back"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:342;359;377
msgid "OK"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:345;362;380
#: Template/Tests_backup/edit.ctp:166
#: Template/UserCards/add.ctp:169
msgid "Cancel"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:478
msgid "Please confirm for your process action."
msgstr ""

#: Template/Element/utility/utilityScript.ctp:508
msgid "Your process running to the statis."
msgstr ""

#: Template/Element/utility/utilityScript.ctp:592
msgid "System Actions"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:657;658
msgid "Master Department"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:682;683
msgid "Master Section"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:706
msgid "Master Province"
msgstr ""

#: Template/Element/utility/utilityScript.ctp:730
msgid "Master District"
msgstr ""

#: Template/Homes/catalogue.ctp:15;54
msgid "หมวดหมู่ - "
msgstr ""

#: Template/Homes/contact.ctp:4
msgid "รายชื่อติดต่อ"
msgstr ""

#: Template/Homes/index.ctp:10
msgid "หมวดหมู่"
msgstr ""

#: Template/Homes/index.ctp:36
msgid "การท่องเที่ยว"
msgstr ""

#: Template/Homes/index.ctp:42
msgid "การเงิน"
msgstr ""

#: Template/Homes/index.ctp:48
msgid "ออฟฟิต"
msgstr ""

#: Template/Homes/index.ctp:101
msgid "ข่าวสารใหม่ประจำวัน"
msgstr ""

#: Template/Homes/noti.ctp:24
#: Template/Homes/promotion.ctp:25
msgid ""
msgstr ""

#: Template/Layout/error.ctp:35
#: Template/Error/error400.ctp:36
#: Template/Error/error500.ctp:41
msgid "Error"
msgstr ""

#: Template/Profiles/index.ctp:59
msgid "user profile"
msgstr ""

#: Template/Profiles/index.ctp:70;78
msgid "username"
msgstr ""

#: Template/Profiles/index.ctp:86
msgid "firstname"
msgstr ""

#: Template/Profiles/index.ctp:101
msgid "lastname"
msgstr ""

#: Template/Profiles/index.ctp:116
msgid "firstnameEn"
msgstr ""

#: Template/Profiles/index.ctp:131
msgid "lastnameEn"
msgstr ""

#: Template/Profiles/index.ctp:146
msgid "birthdate"
msgstr ""

#: Template/Profiles/index.ctp:162
msgid "gender"
msgstr ""

#: Template/Profiles/index.ctp:167
msgid "male"
msgstr ""

#: Template/Profiles/index.ctp:172
msgid "female"
msgstr ""

#: Template/Profiles/index.ctp:179
msgid "blood group"
msgstr ""

#: Template/Profiles/index.ctp:184
#: Template/UserProfiles/profile_setting.ctp:60
msgid "A"
msgstr ""

#: Template/Profiles/index.ctp:189
#: Template/UserProfiles/profile_setting.ctp:63
msgid "B"
msgstr ""

#: Template/Profiles/index.ctp:194
#: Template/UserProfiles/profile_setting.ctp:66
msgid "AB"
msgstr ""

#: Template/Profiles/index.ctp:199
#: Template/UserProfiles/profile_setting.ctp:69
msgid "O"
msgstr ""

#: Template/Profiles/index.ctp:206
msgid "mobile"
msgstr ""

#: Template/Profiles/index.ctp:222
msgid "email"
msgstr ""

#: Template/Profiles/index.ctp:241
msgid "address"
msgstr ""

#: Template/Profiles/index.ctp:242
#: Template/UserProfiles/profile_setting.ctp:91
msgid "112/3 สุขุมวิท 22 ทองหล่อ กทม 10523"
msgstr ""

#: Template/Profiles/index.ctp:248
msgid "country"
msgstr ""

#: Template/Profiles/index.ctp:268
msgid "province"
msgstr ""

#: Template/Profiles/index.ctp:288
msgid "SAVE"
msgstr ""

#: Template/Profiles/index.ctp:310
#: Template/UserCards/view_card.ctp:21
msgid "id"
msgstr ""

#: Template/Profiles/index.ctp:316;370
#: Template/UserCards/view_card.ctp:81
msgid "Firstname"
msgstr ""

#: Template/Profiles/index.ctp:326;394
#: Template/UserCards/view_card.ctp:37;105
msgid "Position"
msgstr ""

#: Template/Profiles/index.ctp:349;400
#: Template/UserCards/view_card.ctp:60;111
msgid "Card issue"
msgstr ""

#: Template/Profiles/index.ctp:352;410
#: Template/UserCards/view_card.ctp:63;121
msgid "Old age"
msgstr ""

#: Template/Profiles/index.ctp:365
#: Template/UserCards/view_card.ctp:76
msgid "Company Information"
msgstr ""

#: Template/Profiles/index.ctp:376
#: Template/UserCards/view_card.ctp:87
msgid "Lastname"
msgstr ""

#: Template/Profiles/index.ctp:382
#: Template/UserCards/view_card.ctp:93
msgid "Company Name"
msgstr ""

#: Template/Profiles/index.ctp:388
#: Template/UserCards/view_card.ctp:99
msgid "Department"
msgstr ""

#: Template/Profiles/index.ctp:443
msgid "Show QR Code to get permission"
msgstr ""

#: Template/Profiles/index.ctp:723
msgid "Please enter your firstname."
msgstr ""

#: Template/Profiles/index.ctp:728
msgid "Please enter your lastname."
msgstr ""

#: Template/Profiles/index.ctp:734
msgid "Please enter your birthdate."
msgstr ""

#: Template/Profiles/index.ctp:740
msgid "Please enter your mobile."
msgstr ""

#: Template/Profiles/index.ctp:745
msgid "Please enter a 10 digit mobile number."
msgstr ""

#: Template/Profiles/index.ctp:751
msgid "Please enter your email address."
msgstr ""

#: Template/Profiles/index.ctp:757
msgid "Email address format wrong, Please enter the correct number."
msgstr ""

#: Template/Profiles/index.ctp:763
msgid "Please specify country"
msgstr ""

#: Template/Profiles/index.ctp:768
msgid "Please specify province"
msgstr ""

#: Template/Profiles/index_backup.ctp:24
#: Template/UserProfiles/index.ctp:23
msgid "ดูล่าสุด"
msgstr ""

#: Template/Profiles/setting_profile.ctp:67
msgid "แก้ไข"
msgstr ""

#: Template/Profiles/setting_profile.ctp:127
#: Template/UserProfiles/profile_setting.ctp:51
msgid "กรุณาระบุวันเดือนปีเกิด"
msgstr ""

#: Template/Tests_backup/edit.ctp:7
#: Template/UserCards/add.ctp:7
msgid "Organize"
msgstr ""

#: Template/Tests_backup/edit.ctp:74
#: Template/UserCards/add.ctp:77
msgid "checkAll"
msgstr ""

#: Template/Tests_backup/edit.ctp:157
#: Template/UserCards/add.ctp:160
#: View/Helper/PermissionHelper.php:401
msgid "Save"
msgstr ""

#: Template/Tests_backup/edit.ctp:161
#: Template/UserCards/add.ctp:164
msgid "Are you sure for save this Import ?"
msgstr ""

#: Template/UserCards/add.ctp:65
msgid "ลำดับทั้งหมด"
msgstr ""

#: Template/UserCards/create_cards.ctp:30
msgid "รหัสพนักงาน / รหัสนักศึกษา"
msgstr ""

#: Template/UserCards/create_cards.ctp:35
msgid "วันเกิด"
msgstr ""

#: Template/UserCards/edit.ctp:9
#: Template/UserCards/index.ctp:9;45
#: Template/UserCards/view.ctp:9
#: Template/Users/index.ctp:9;48
#: Template/Users/notification.ctp:9
msgid "Actions"
msgstr ""

#: Template/UserCards/edit.ctp:13
#: Template/UserCards/index.ctp:79
#: Template/UserCards/view.ctp:11
#: Template/Users/index.ctp:81
#: Template/Users/notification.ctp:11
#: View/Helper/PermissionHelper.php:291
msgid "Are you sure you want to delete # {0}?"
msgstr ""

#: Template/UserCards/edit.ctp:16
#: Template/UserCards/view.ctp:12
msgid "List User Cards"
msgstr ""

#: Template/UserCards/edit.ctp:17
#: Template/UserCards/index.ctp:11
#: Template/UserCards/view.ctp:14
msgid "List Users"
msgstr ""

#: Template/UserCards/edit.ctp:18
#: Template/UserCards/index.ctp:12
#: Template/UserCards/view.ctp:15
msgid "New User"
msgstr ""

#: Template/UserCards/edit.ctp:24
#: Template/UserCards/view.ctp:10
msgid "Edit User Card"
msgstr ""

#: Template/UserCards/edit.ctp:50
#: View/Helper/FormHelper.php:1927
msgid "Submit"
msgstr ""

#: Template/UserCards/index.ctp:10
#: Template/UserCards/view.ctp:13
msgid "New User Card"
msgstr ""

#: Template/UserCards/index.ctp:16
msgid "User Cards"
msgstr ""

#: Template/UserCards/index.ctp:77
#: Template/Users/index.ctp:79
#: View/Helper/PaginatorHelper.php:1220
msgid "View"
msgstr ""

#: Template/UserCards/view.ctp:11
msgid "Delete User Card"
msgstr ""

#: Template/UserCards/view.ctp:22
msgid "User"
msgstr ""

#: Template/UserCards/view.ctp:26
msgid "Card Code"
msgstr ""

#: Template/UserCards/view.ctp:30
msgid "Img Path"
msgstr ""

#: Template/UserCards/view.ctp:34
msgid "Prefix Name Th"
msgstr ""

#: Template/UserCards/view.ctp:38
msgid "Firstname Th"
msgstr ""

#: Template/UserCards/view.ctp:42
msgid "Lastname Th"
msgstr ""

#: Template/UserCards/view.ctp:46
msgid "Prefix Name En"
msgstr ""

#: Template/UserCards/view.ctp:50
msgid "Firstname En"
msgstr ""

#: Template/UserCards/view.ctp:54
msgid "Lastname En"
msgstr ""

#: Template/UserCards/view.ctp:58
msgid "Department Name"
msgstr ""

#: Template/UserCards/view.ctp:62
msgid "Section Name"
msgstr ""

#: Template/UserCards/view.ctp:66
msgid "Position Name"
msgstr ""

#: Template/UserCards/view.ctp:70
msgid "Gender"
msgstr ""

#: Template/UserCards/view.ctp:74
msgid "Blood Group"
msgstr ""

#: Template/UserCards/view.ctp:78
msgid "Signature"
msgstr ""

#: Template/UserCards/view.ctp:82
#: Template/Users/notification.ctp:106
msgid "Id"
msgstr ""

#: Template/UserCards/view.ctp:86
msgid "Organize Id"
msgstr ""

#: Template/UserCards/view.ctp:90
msgid "Created By"
msgstr ""

#: Template/UserCards/view.ctp:94
msgid "Modified By"
msgstr ""

#: Template/UserCards/view.ctp:98
msgid "Birthdate"
msgstr ""

#: Template/UserCards/view.ctp:102
msgid "Date Issued"
msgstr ""

#: Template/UserCards/view.ctp:106
msgid "Date Expiry"
msgstr ""

#: Template/UserCards/view.ctp:110
#: Template/Users/notification.ctp:114
msgid "Created"
msgstr ""

#: Template/UserCards/view.ctp:114
#: Template/Users/notification.ctp:118
msgid "Modified"
msgstr ""

#: Template/UserCards/view.ctp:118
msgid "Is Used"
msgstr ""

#: Template/UserCards/view.ctp:119
#: View/Helper/UtilityHelper.php:247
msgid "Yes"
msgstr ""

#: Template/UserCards/view.ctp:119
#: View/Helper/UtilityHelper.php:247
msgid "No"
msgstr ""

#: Template/UserCards/view_card.ctp:27
msgid "Name"
msgstr ""

#: Template/UserCards/view_card.ctp:161
msgid "ID Active"
msgstr ""

#: Template/UserCards/view_card.ctp:576
msgid "Please enter Company Code."
msgstr ""

#: Template/UserCards/view_card.ctp:581
msgid "Please enter Ref1."
msgstr ""

#: Template/UserCards/view_card.ctp:586
msgid "Please enter Ref2."
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:21;22
msgid "ชื่อผู้ใช้งาน"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:31
msgid "เพศ"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:34
msgid "ชาย"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:37
msgid "หญิง"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:44
msgid "ชื่อ - นามสกุล"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:57
msgid "กรุ๊ปเลือด"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:77;184
msgid "เบอร์โทรศัพท์"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:84
#: Template/Users/forgot_password_20180801.ctp:14
#: Template/Users/notification.ctp:58
msgid "Email"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:91
msgid "ที่อยู่อาศัย"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:102
msgid "สถานภาพทางการศึกษา"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:108
msgid "รหัสนักศึกษา"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:108
msgid "545223092010"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:113
msgid "มหาวิทยาลัย"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:113
msgid "มหาวิทยาลัยราชมงคล"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:119
msgid "คณะ"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:125
msgid "สาขาวิชาการศึกษา"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:136
msgid "สถานภาพทางการทำงาน"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:144
msgid "ประเภทงาน"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:147
msgid "เจ้าหน้าที่บริษัท"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:150
msgid "ธุรกิจส่วนตัว"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:153
msgid "รับราชการ"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:156
msgid "รัฐวิสาหกิจ"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:159
msgid "พนักงานชั่วคราว"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:170
msgid "ชื่อบริษัท/ห้างร้าน"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:176
msgid "ที่อยู่ที่ทำงาน"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:176
msgid "วิศวกรรมคอมพิวเตอร์"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:184
msgid "00-000-0000"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:191
msgid "ฝ่าย"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:198
msgid "แผนก"
msgstr ""

#: Template/UserProfiles/profile_setting.ctp:206
msgid "ตำแหน่ง"
msgstr ""

#: Template/Users/change_forgotpassword.ctp:3
msgid "change password"
msgstr ""

#: Template/Users/change_forgotpassword.ctp:7
#: Template/Users/change_password.ctp:7
msgid "กรุณา! ตรวจสอบอีเมลล์ของท่าน"
msgstr ""

#: Template/Users/change_forgotpassword.ctp:15
msgid "new password"
msgstr ""

#: Template/Users/change_forgotpassword.ctp:23
msgid "confirm password"
msgstr ""

#: Template/Users/change_forgotpassword.ctp:33
#: Template/Users/forgot_password_20180801.ctp:21
msgid "submit"
msgstr ""

#: Template/Users/change_forgotpassword.ctp:50;59
msgid "Please enter only English letters and numbers."
msgstr ""

#: Template/Users/change_forgotpassword.ctp:68
#: Template/Users/change_password.ctp:94
msgid "Please enter a new password."
msgstr ""

#: Template/Users/change_forgotpassword.ctp:73
#: Template/Users/change_password.ctp:99
msgid "Please enter a new password at least 8 characters."
msgstr ""

#: Template/Users/change_forgotpassword.ctp:78
#: Template/Users/change_password.ctp:104
msgid "Please enter your password."
msgstr ""

#: Template/Users/change_forgotpassword.ctp:83
#: Template/Users/change_password.ctp:109
msgid "Please enter at least 8 characters."
msgstr ""

#: Template/Users/change_forgotpassword.ctp:87
#: Template/Users/change_password.ctp:113
msgid "Please provide the same password."
msgstr ""

#: Template/Users/change_password.ctp:3
msgid "เปลี่ยนรหัสผ่าน"
msgstr ""

#: Template/Users/change_password.ctp:14
msgid "รหัสผ่านเก่า"
msgstr ""

#: Template/Users/change_password.ctp:22
msgid "รหัสผ่านใหม่"
msgstr ""

#: Template/Users/change_password.ctp:30
msgid "ยืนยันรหัสผ่าน"
msgstr ""

#: Template/Users/change_password.ctp:84
msgid "Please enter a old password."
msgstr ""

#: Template/Users/change_password.ctp:89
msgid "Please enter a old password at least 8 characters."
msgstr ""

#: Template/Users/forgot_password.ctp:2
#: Template/Users/signin.ctp:33
msgid "Forgot Password"
msgstr ""

#: Template/Users/forgot_password.ctp:6
msgid "mail@gmail.com"
msgstr ""

#: Template/Users/forgot_password.ctp:12
msgid "Get \"Password\""
msgstr ""

#: Template/Users/forgot_password.ctp:20
msgid "Contact : {0}"
msgstr ""

#: Template/Users/forgot_password_20180801.ctp:3
msgid "Forgot password"
msgstr ""

#: Template/Users/forgot_password_20180801.ctp:34
msgid "Please enter your email."
msgstr ""

#: Template/Users/forgot_password_20180801.ctp:40
msgid "Email address format wrong Please enter the correct."
msgstr ""

#: Template/Users/index.ctp:10
#: Template/Users/notification.ctp:13
msgid "New User Name"
msgstr ""

#: Template/Users/index.ctp:11
#: Template/Users/notification.ctp:14
msgid "List Faculties"
msgstr ""

#: Template/Users/index.ctp:12
#: Template/Users/notification.ctp:15
msgid "New Faculty"
msgstr ""

#: Template/Users/index.ctp:13
#: Template/Users/notification.ctp:16
msgid "List Roles"
msgstr ""

#: Template/Users/index.ctp:14
#: Template/Users/notification.ctp:17
msgid "New Role"
msgstr ""

#: Template/Users/index.ctp:15
#: Template/Users/notification.ctp:18
msgid "List Name Prefixes"
msgstr ""

#: Template/Users/index.ctp:16
#: Template/Users/notification.ctp:19
msgid "New Name Prefix"
msgstr ""

#: Template/Users/index.ctp:20
msgid "User Names"
msgstr ""

#: Template/Users/notification.ctp:10
msgid "Edit User Name"
msgstr ""

#: Template/Users/notification.ctp:11
msgid "Delete User Name"
msgstr ""

#: Template/Users/notification.ctp:12
msgid "List User Names"
msgstr ""

#: Template/Users/notification.ctp:26
msgid "Faculty"
msgstr ""

#: Template/Users/notification.ctp:30
msgid "Role"
msgstr ""

#: Template/Users/notification.ctp:34
msgid "Ref Code"
msgstr ""

#: Template/Users/notification.ctp:38
#: Template/Users/signin.ctp:10
msgid "Username"
msgstr ""

#: Template/Users/notification.ctp:42
msgid "Password"
msgstr ""

#: Template/Users/notification.ctp:46
msgid "Name Prefix"
msgstr ""

#: Template/Users/notification.ctp:50
msgid "First Name"
msgstr ""

#: Template/Users/notification.ctp:54
msgid "Last Name"
msgstr ""

#: Template/Users/notification.ctp:62
msgid "Office Phone"
msgstr ""

#: Template/Users/notification.ctp:66
msgid "Mobile Phone"
msgstr ""

#: Template/Users/notification.ctp:70
msgid "Address"
msgstr ""

#: Template/Users/notification.ctp:74
msgid "Moo"
msgstr ""

#: Template/Users/notification.ctp:78
msgid "Road"
msgstr ""

#: Template/Users/notification.ctp:82
msgid "Sub District"
msgstr ""

#: Template/Users/notification.ctp:86
msgid "District"
msgstr ""

#: Template/Users/notification.ctp:90
msgid "Province"
msgstr ""

#: Template/Users/notification.ctp:94
msgid "Zipcode"
msgstr ""

#: Template/Users/notification.ctp:98
msgid "Status"
msgstr ""

#: Template/Users/notification.ctp:102
msgid "Picture Path"
msgstr ""

#: Template/Users/notification.ctp:110
msgid "Birth Date"
msgstr ""

#: Template/Users/pin_code.ctp:3
#: Template/Users/pin_codepassword.ctp:3
msgid "Verify PIN"
msgstr ""

#: Template/Users/pin_code.ctp:16
msgid "4 digit PIN in the email address specified for access"
msgstr ""

#: Template/Users/pin_code.ctp:40
#: Template/Users/pin_codepassword.ctp:40
msgid "Re-send pin code"
msgstr ""

#: Template/Users/pin_codepassword.ctp:18
msgid "4 digit PIN in the email address specified to change the password."
msgstr ""

#: Template/Users/signin.ctp:19
msgid "********"
msgstr ""

#: Template/Users/signin.ctp:27
msgid "Login"
msgstr ""

#: Template/Users/signin.ctp:34
msgid "Register"
msgstr ""

#: Template/Users/signup.ctp:2;13
msgid "Registration"
msgstr ""

#: View/Helper/PermissionHelper.php:124
msgid "Grant / Revoke"
msgstr ""

#: View/Helper/PermissionHelper.php:291
msgid "Are you sure you want to delete ?"
msgstr ""

#: View/Helper/PermissionHelper.php:308
msgid "Action"
msgstr ""

#: View/Helper/PermissionHelper.php:311
msgid "Toggle Dropdown"
msgstr ""

#: View/Helper/PermissionHelper.php:412
msgid "Add Another File"
msgstr ""

#: Template/Element/footer.ctp:19
msgid "Powered By BackstageEL"
msgstr ""

#: Template/Element/nav-top.ctp:35
msgid "Profile"
msgstr ""

#: Template/Element/nav-top.ctp:39
msgid "Settings"
msgstr ""

#: Template/Element/nav-top.ctp:42
msgid "Help"
msgstr ""

#: Template/Element/nav-top.ctp:43
msgid "Log Out"
msgstr ""

#: Template/Element/aside/form.ctp:18
msgid "Search"
msgstr ""

#: Template/Element/aside/user-panel.ctp:22
msgid "Welcome"
msgstr ""

#: Template/Error/error400.ctp:37
msgid "The requested address {0} was not found on this server."
msgstr ""

#: Template/Error/error500.ctp:39
msgid "An Internal Error Has Occurred"
msgstr ""

#: Controller/Component/AuthComponent.php:494
msgid "You are not authorized to access that location."
msgstr ""

#: Controller/Component/CsrfComponent.php:157
#: Http/Middleware/CsrfProtectionMiddleware.php:190
msgid "Missing CSRF token cookie"
msgstr ""

#: Controller/Component/CsrfComponent.php:161
#: Http/Middleware/CsrfProtectionMiddleware.php:194
msgid "CSRF token mismatch."
msgstr ""

#: Error/ExceptionRenderer.php:258
msgid "Not Found"
msgstr ""

#: Error/ExceptionRenderer.php:260
msgid "An Internal Error Has Occurred."
msgstr ""

#: Http/Response.php:2400
msgid "The requested file contains `..` and will not be read."
msgstr ""

#: Http/Response.php:2411
msgid "The requested file was not found"
msgstr ""

#: I18n/Number.php:90
msgid "{0,number,#,###.##} KB"
msgstr ""

#: I18n/Number.php:92
msgid "{0,number,#,###.##} MB"
msgstr ""

#: I18n/Number.php:94
msgid "{0,number,#,###.##} GB"
msgstr ""

#: I18n/Number.php:96
msgid "{0,number,#,###.##} TB"
msgstr ""

#: I18n/Number.php:88
msgid "{0,number,integer} Byte"
msgid_plural "{0,number,integer} Bytes"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:80
msgid "{0} from now"
msgstr ""

#: I18n/RelativeTimeFormatter.php:80
msgid "{0} ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:83
msgid "{0} after"
msgstr ""

#: I18n/RelativeTimeFormatter.php:83
msgid "{0} before"
msgstr ""

#: I18n/RelativeTimeFormatter.php:114
msgid "just now"
msgstr ""

#: I18n/RelativeTimeFormatter.php:151
msgid "about a second ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:152
msgid "about a minute ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:153
msgid "about an hour ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:154;332
msgid "about a day ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:155;333
msgid "about a week ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:156;334
msgid "about a month ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:157;335
msgid "about a year ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:168
msgid "in about a second"
msgstr ""

#: I18n/RelativeTimeFormatter.php:169
msgid "in about a minute"
msgstr ""

#: I18n/RelativeTimeFormatter.php:170
msgid "in about an hour"
msgstr ""

#: I18n/RelativeTimeFormatter.php:171;346
msgid "in about a day"
msgstr ""

#: I18n/RelativeTimeFormatter.php:172;347
msgid "in about a week"
msgstr ""

#: I18n/RelativeTimeFormatter.php:173;348
msgid "in about a month"
msgstr ""

#: I18n/RelativeTimeFormatter.php:174;349
msgid "in about a year"
msgstr ""

#: I18n/RelativeTimeFormatter.php:304
msgid "today"
msgstr ""

#: I18n/RelativeTimeFormatter.php:370
msgid "%s ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:371
msgid "on %s"
msgstr ""

#: I18n/RelativeTimeFormatter.php:47;126;316
msgid "{0} year"
msgid_plural "{0} years"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:51;129;319
msgid "{0} month"
msgid_plural "{0} months"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:57;132;322
msgid "{0} week"
msgid_plural "{0} weeks"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:59;135;325
msgid "{0} day"
msgid_plural "{0} days"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:64;138
msgid "{0} hour"
msgid_plural "{0} hours"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:68;141
msgid "{0} minute"
msgid_plural "{0} minutes"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:72;144
msgid "{0} second"
msgid_plural "{0} seconds"
msgstr[0] ""
msgstr[1] ""

#: ORM/RulesChecker.php:57
msgid "This value is already in use"
msgstr ""

#: ORM/RulesChecker.php:104
msgid "This value does not exist"
msgstr ""

#: ORM/RulesChecker.php:128
msgid "The count does not match {0}{1}"
msgstr ""

#: Utility/Text.php:915
msgid "and"
msgstr ""

#: Validation/Validator.php:111
msgid "This field is required"
msgstr ""

#: Validation/Validator.php:112
msgid "This field cannot be left empty"
msgstr ""

#: Validation/Validator.php:1885
msgid "The provided value is invalid"
msgstr ""

#: View/Helper/FormHelper.php:1039
msgid "New %s"
msgstr ""

#: View/Helper/FormHelper.php:1042
msgid "Edit %s"
msgstr ""

#: View/Widget/DateTimeWidget.php:540
msgid "January"
msgstr ""

#: View/Widget/DateTimeWidget.php:541
msgid "February"
msgstr ""

#: View/Widget/DateTimeWidget.php:542
msgid "March"
msgstr ""

#: View/Widget/DateTimeWidget.php:543
msgid "April"
msgstr ""

#: View/Widget/DateTimeWidget.php:544
msgid "May"
msgstr ""

#: View/Widget/DateTimeWidget.php:545
msgid "June"
msgstr ""

#: View/Widget/DateTimeWidget.php:546
msgid "July"
msgstr ""

#: View/Widget/DateTimeWidget.php:547
msgid "August"
msgstr ""

#: View/Widget/DateTimeWidget.php:548
msgid "September"
msgstr ""

#: View/Widget/DateTimeWidget.php:549
msgid "October"
msgstr ""

#: View/Widget/DateTimeWidget.php:550
msgid "November"
msgstr ""

#: View/Widget/DateTimeWidget.php:551
msgid "December"
msgstr ""

