# PAKGON CORE CONNECT

PAKGON CORE THE CONNECT PROJECT

## Git Clone for the repository

```bash
$ cd D:/xampp7/htdocs/
$ mkdir CONNECT-CORE
$ cd CONNECT-CORE
$ git clone http://gitlab.pakgon.com/pakgon-connect/connect-core.git .
```

```bash
## Install Part Application Front end

$ cd www/app
$ composer install

## Install Part API

$ cd ..
$ cd api
$ composer install
```

## SETTING UP FOR V-HOST FOR FRONT END

```bash
<VirtualHost *:80>
  ServerName connect.pakgon.local
  ServerAlias connect.pakgon.local
  DocumentRoot "D:/xampp7/htdocs/CONNECT-CORE/www/app"
  ErrorLog "logs/connect.pakgon.local-error.log"
  CustomLog "logs/connect.pakgon.local-access.log" common
  <Directory "D:/xampp7/htdocs/CONNECT-CORE/www/app">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
</VirtualHost>
```

## HOST FILE FOR FRONT END

```bash
127.0.0.1       connect.pakgon.local
::1             connect.pakgon.local
```

## SETTING UP FOR V-HOST FOR CONNECT API

```bash
<VirtualHost *:80>
  ServerName api.connect.pakgon.local
  ServerAlias api.connect.pakgon.local
  DocumentRoot "D:/xampp7/htdocs/CONNECT-CORE/www/api"
  ErrorLog "logs/api.connect.pakgon.local-error.log"
  CustomLog "logs/api.connect.pakgon.local-access.log" common
  <Directory "D:/xampp7/htdocs/CONNECT-CORE/www/api">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
</VirtualHost>
```

## HOST FILE FOR CONNECT API

```bash
127.0.0.1       api.connect.pakgon.local
::1             api.connect.pakgon.local
```

## Current path are in bin directory

```bash
$ ./cake bake all [-c connection] [-t Pakgon]
```
