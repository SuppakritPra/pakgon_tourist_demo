<?php
namespace App\Controller;

use Cake\Utility\Security;
use Cake\Mailer\Email;
use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
class SignupsController extends ApiController
{

    public function index()
    {
        $this->request->allowMethod('post');

        $data = $this->request->data['data'];#prd($data);
	
	$data_validate = $this->validate($data);
	
	if(empty($data_validate)){
		$created_by = 1;
		$created = date('Y-m-d h:i:s'); 
		#--------------------------------------------------------------------
		$data['password'] = Security::hash($data['password'], 'sha256', true);
		
		$dataUsers = [];
		$dataUsers['username'] = $data['username']; 
		$dataUsers['password'] = $data['password']; 
		$dataUsers['point'] = 0; 
		$dataUsers['is_used'] = false; 
		$dataUsers['dynamic_key'] = 'dynamic_key'; 
		$dataUsers['dynamic_key_expiry'] = date('Y-m-d',strtotime('+3 day'));
		$dataUsers['token'] = Security::hash($data['username'].date('Y-m-d h:i:s'), 'md5', true);
		$dataUsers['token_expiry'] = date('Y-m-d',strtotime('+3 day'));
		$digits = 4;
		$dataUsers['pin_code'] = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
		
		$dataUsers['created_by'] = $created_by;
		$dataUsers['created'] = $created;
		#prr($dataUsers);
		#--------------------------------------------------------------------

		#--------------------------------------------------------------------
		$dataUserPersonals = [];
		if(!empty($data['person_card_no'])){
			$dataUserPersonals['person_card_no'] = $data['person_card_no'];
			$data['person_card_no'] = str_replace('-','',trim($data['person_card_no']));
		}
		#$dataUserPersonals['master_prefix_id'] = $data['master_prefix_id']; 
		$dataUserPersonals['firstname_th'] = $data['firstname']; 
		$dataUserPersonals['lastname_th'] = $data['lastname']; 
		$dataUserPersonals['firstname_en'] = $data['firstname']; 
		$dataUserPersonals['lastname_en'] = $data['lastname']; 
		$dataUserPersonals['gender'] = 'F';	#F:M 
		$dataUserPersonals['birthdate'] = date("Y-m-d", strtotime($data['birthdate'])); 
		$dataUserPersonals['moblie_no'] = $data['phone_no']; 
		$dataUserPersonals['phone_no'] = $data['phone_no']; 
		$dataUserPersonals['email'] = $data['email']; 	
		#$dataUserPersonals['user_id'] = $data['user_id']; 
		#$dataUserPersonals['blood_group'] = '';
		#$dataUserPersonals['address'] = $data['person_card_no'];

		$dataUserPersonals['created_by'] = $created_by;
		$dataUserPersonals['created'] = $created;
		#prr($dataUserPersonals);
		#--------------------------------------------------------------------
	
		#--------------------------------------------------------------------
		$dataUserProfiles = [];
		#$dataUserProfiles['user_id'] = $data['user_id']; 
		#$dataUserProfiles['organize_id'] = $data['organize_id']; 
		#$dataUserProfiles['dept_id'] = $data['dept_id']; 
		#$dataUserProfiles['section_id'] = $data['section_id']; 
		#$dataUserProfiles['card_code'] = $data['card_code']; 
		$dataUserProfiles['user_type_id'] = $data['user_type_id'] = 5; 
		#$dataUserProfiles['img_path'] = $data['img_path']; 
		$dataUserProfiles['is_used'] = true; 
		#$dataUserProfiles['position_org'] = $data['position_org']; 
		#$dataUserProfiles['position_edu'] = $data['position_edu']; 
		#$dataUserProfiles['address'] = $data['address'];
		#$dataUserProfiles['master_bussiness_type_id'] = $data['master_bussiness_type_id'];
	
		$dataUserProfiles['created_by'] = $created_by;
		$dataUserProfiles['created'] = $created;
		#prr($dataUserProfiles);
		#--------------------------------------------------------------------
		
		$this->loadModel('UserPersonals');
		$userPersonals = $this->UserPersonals->find('all',
								[
								'fields' => ['id', 'user_id'],
								'order' => ['id' => 'DESC'],
								'limit' => 1
								]
								)->toArray();
		#prr($userPersonals);	
	
		$user_id = $dataUserPersonals['user_id'] = $userPersonals[0]['id'] + 1;
		#--------------------------------------------------------------------
		#prr($dataUserPersonals);
		$this->loadModel('UserPersonals');
		$userPersonals = $this->UserPersonals->newEntity();
		$userPersonals = $this->UserPersonals->patchEntity($userPersonals, $dataUserPersonals);#prr($userPersonals);
		if ($this->UserPersonals->save($userPersonals)) {
		
			#--------------------------------------------------------------------
			#$dataUsers['id'] = $user_id;prr($dataUsers);
		
			$this->loadModel('Users');
			$users = $this->Users->newEntity();#prr($users);
			$users = $this->Users->patchEntity($users, $dataUsers);	#prr($users);
			$users->id = $user_id;
			#prd($users);
			if ($this->Users->save($users)) {
				$this->apiResponse['message'] =  'Insert User Succeed';
			}else{
				$this->responseStatus = 'Fail';
				#$this->apiResponse['type'] =  'error';
				$this->apiResponse['message'] =  'Insert User';
			}
			#--------------------------------------------------------------------
		
			#--------------------------------------------------------------------
			$dataUserProfiles['user_id'] = $user_id;
		
			$this->loadModel('UserProfiles');
			$userProfiles = $this->UserProfiles->newEntity();#prr($userProfiles);
			$userProfiles = $this->UserProfiles->patchEntity($userProfiles, $dataUserProfiles);#prr($userProfiles);
			if ($this->UserProfiles->save($userProfiles)) {
				$this->apiResponse['message'] =  'Insert User Profile Succeed';
			}else{
				$this->responseStatus = 'Fail';
				#$this->apiResponse['type'] =  'error';
				$this->apiResponse['message'] =  'Insert User Profile';
			}
			#--------------------------------------------------------------------
			
			#--------------------------------------------------------------------
			$dataUserHomeplaces['user_id'] = $user_id;
			if(!empty($data['person_card_no'])){			
				$dataUserHomeplaces['master_country_id'] = 1;
				$dataUserHomeplaces['master_province_id'] = intval(substr($data['person_card_no'],1,2));
				$dataUserHomeplaces['master_district_id'] = intval(substr($data['person_card_no'],3,2));
			}
			$dataUserHomeplaces['is_used'] = true; 

			$dataUserHomeplaces['created_by'] = $created_by;
			$dataUserHomeplaces['created'] = $created;		
			
			#$this->apiResponse['UserHomeplaces'] =  $dataUserHomeplaces;
			$this->loadModel('UserHomeplaces');
			$UserHomeplaces = $this->UserHomeplaces->newEntity();#prr($UserHomeplaces);
			$UserHomeplaces = $this->UserHomeplaces->patchEntity($UserHomeplaces, $dataUserHomeplaces);#prr($UserHomeplaces);
			if ($this->UserHomeplaces->save($UserHomeplaces)) {
				$this->apiResponse['data']['token'] =  $dataUsers['token'];
				$this->apiResponse['message'] =  'Insert User Home Place Succeed';
			}else{
				$this->responseStatus = 'Fail';
				#$this->apiResponse['type'] =  'error';
				$this->apiResponse['message'] =  'Insert User Home Place ';
			}
			#--------------------------------------------------------------------			
		
			$data_notification = [];
			$data_notification['email'] = $data['email'];
			$data_notification['pin_code'] = $dataUsers['pin_code'];
			$this->notification($data_notification);
			
	      		if($this->responseStatus != 'Fail') $this->apiResponse['message'] = 'Signup Connect Completed!!';
			
		}else{
			$this->apiResponse['type'] = 'error';
			$this->apiResponse['message'] =  'Insert User Personal';
		}
	}else{	
		$this->responseStatus = $data_validate['status'];	
		$this->apiResponse = $data_validate['result'];
	}

    }
    
    function validate($data = null){
    
    	$data_return = array();
	
	if(empty($data)){
		$data_return['status'] = 'Fail';
		$data_return['result']['code'] = 'error';
		$data_return['result']['message'] = __('Data Empty');
		return $data_return;
	}
	
    	if(!empty($data)){
		$data_return = array();
	    	$this->loadModel('Users');
		$users = $this->Users->find('all',
						[
						#'conditions' => ['is_used' => true,'username' => $data['username']],
						'conditions' => ['username' => $data['username']],
						'fields' => ['id', 'username'],
						'limit' => 1
						]
						)->toArray();
		#prr($users);
		if(!empty($users)){
			$data_return['status'] = 'Fail';
			$data_return['result']['code'] = 'error';
			$data_return['result']['message'] = __('User Duplicate');
		}
                //*******************************************************//
                $this->loadModel('UserPersonals');
		$userPersonals = $this->UserPersonals->find('all',
						[
						'conditions' => ['is_used' => true,'email' => $data['email']],
						'conditions' => ['email' => $data['email']],
						'fields' => ['id', 'email'],
						'limit' => 1
						]
						)->toArray();
		#prr($userPersonals);
		if(!empty($userPersonals)){
			$data_return['status'] = 'Fail';
			$data_return['result']['code'] = 'error';
			$data_return['result']['message'] = __('Email Duplicate');
		}
                //*******************************************************//
	}
	#prd($data_return);
	return $data_return;
    
    }

    public function verify()
    {
        $this->request->allowMethod('post');

        $data = $this->request->data;
	
	if(!empty($data)){	
		
		$this->loadModel('Users');
		$Users = $this->Users->find('all',
						[
							'conditions' => ['is_used' => false,'token' => $data['token']]
						]
					   )->first();

		if(!empty($Users)){
			$Users = $Users->toArray();

			$pin_code = $data['pin_code_1'].$data['pin_code_2'].$data['pin_code_3'].$data['pin_code_4'];
			if($Users['pin_code'] == $pin_code ){
				$user_id = $Users['id'];
				$dataUsers = [];
				$dataUsers['is_used'] = true;				
		
				$this->loadModel('Users');
				#$users = $this->Users->newEntity();
				$users = $this->Users->get($user_id);
				$users = $this->Users->patchEntity($users, $dataUsers);
				$this->Users->save($users);			
				
	    			$this->apiResponse['message'] =  'Verify Succeed';
			}else{
				$this->responseStatus = 'Fail';
				$this->apiResponse['message'] =  'Pin Code Invalid!';
			}
		}else{
			$this->responseStatus = 'Fail';
			$this->apiResponse['message'] =  'Profile Empty';
		}
	}else{
		$this->responseStatus = 'Fail';
		$this->apiResponse['message'] =  __('Data Empty');
	}

    }
    
    public function notification($data = null)
    {

    	if(!empty($data)){
	
	    	$verify_code = $data['pin_code'];
		$from_email = ['support@pakgon.com' => 'Support'];
		$to_emails = [$data['email']];

	        $email = new Email();
	        $email->transport('gmail');

	        try {
	            $email->template('notification_signup', 'connect');
	            $email->from($from_email);
	            $email->to($to_emails);
	            $email->subject('Signup Connect Verify');
	            $email->emailFormat('html');
	            $email->viewVars(compact('verify_code'));
	            #$email->send($message);
	            $email->send();

	        } catch (Exception $e) {
	            echo 'Exception : ',  $e->getMessage(), "\n";
	        }
	
	       #$this->httpStatusCode = 200;
	       #$this->apiResponse['message'] = 'Signup Connect Completed!!';
	
	}
	
    }    

}