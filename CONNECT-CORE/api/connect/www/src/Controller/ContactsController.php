<?php
namespace App\Controller;

use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
class ContactsController extends ApiController
{

    public function index()
    {
        $this->request->allowMethod('post');
	
	$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
	#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzMCwidXNlcm5hbWUiOiJ0ZXN0dG9uZyIsImlwIjoiMTI3LjAuMC4xIn0.37hXRDEhiwjsgqR8mcgbhxnKtAof3tIDahL2P6DnQ4I";

	$user = JwtToken::decodeToken($token);#prd($user);
	#$user->user_id = 1;
	#$user->user_id = 59281;

	$data = json_decode(file_get_contents("php://input"),true);#prr($data);
	
	$language = 'th';

	$this->loadModel('Contracts');
	$Contracts = $this->Contracts->find('list',
						[
							'conditions' => ['user_id1' => $user->user_id],
							'keyField' => 'id',
							'valueField' => 'user_id2',
						]
						);
	if(!empty($Contracts)) $Contracts = $Contracts->toArray();
	#prd($Contracts);

	$Profiles = [];
	if(!empty($Contracts)){
	
		$this->loadModel('Users');
		$Users = $this->Users->find('all',['contain' => ['UserPersonals', 'UserProfiles']])->where(['id IN'  => $Contracts]);					
		if(!empty($Users)) $Users = $Users->toArray();
		#prr($Users);
	
		$this->loadModel('MasterDepartments');
		$MasterDepartments = $this->MasterDepartments->find('list',
							[
								'keyField' => 'id',
								'valueField' => 'dept_name_'.$language,
								'order' => 'dept_name_'.$language.' asc'
							]
							);
		if(!empty($MasterDepartments)) $MasterDepartments = $MasterDepartments->toArray();
		#prr($MasterDepartments);	
	
		#$Profiles = [];
		foreach($Users as $User){
		
			$UserPersonal = $User['user_personals'];
			$UserProfile = $User['user_profiles'];
		
			#prr($UserPersonal);
			#prd($UserProfile);
		
			$Profiles[$User['id']]['name'] = $UserPersonal[0]['firstname_th'] .' ' . $UserPersonal[0]['lastname_th'];
			$Profiles[$User['id']]['moblie_no'] = $UserPersonal[0]['moblie_no'];
			$Profiles[$User['id']]['department'] = !empty($MasterDepartments[$UserProfile[0]['dept_id']])? $MasterDepartments[$UserProfile[0]['dept_id']]:'';
			$Profiles[$User['id']]['img_path'] = $UserProfile[0]['img_path'];

		}
		#prd($Profiles);
	}
	
	$this->apiResponse['Data']['Profiles'] = $Profiles;
    }

    public function barcode()
    {
        $this->request->allowMethod('post');

        pr($this->name);die;
    }
    
    public function  addFriend()
    {
    
	$this->request->allowMethod('post');
		
	$data = $this->request->data;#prd($data);
	#$data['code'] = "3|addFriend|eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzMCwidXNlcm5hbWUiOiJ0ZXN0dG9uZyIsImlwIjoiMTI3LjAuMC4xIn0.37hXRDEhiwjsgqR8mcgbhxnKtAof3tIDahL2P6DnQ4I";
	#prd($data['code']);
	#$this->apiResponse['Data']['message'] = $data;die;
		
	if(!empty($data)){

		$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
		#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
		#prd($token);

		$user = JwtToken::decodeToken($token);#prr($user);
		$user_id = $user->user_id;
		
		$arr_data = explode('|',$data['code']);#prd($arr_data);
		$contract_user = JwtToken::decodeToken($arr_data[2]);#prr($contract_user);
		$contract_user_id = $contract_user->user_id;
			
		$this->loadModel('Contracts');
		$Contracts = $this->Contracts->find('all',
						[
						'fields' => ['user_id1', 'user_id2'],
						'conditions' => ['user_id1' => $user_id, 'user_id2' => $contract_user_id],
						]
					   )->first();
		if(!empty($Contracts)) $Contracts = $Contracts->toArray();
		#prd($Contracts);

		if(empty($Contracts)){
		
			$created_by = 1;
			$created = date('Y-m-d h:i:s'); 
		
			$dataContracts = [];
			$dataContracts['user_id1'] = $user_id; 
			$dataContracts['user_id2'] = $contract_user_id;
			$dataContracts['is_used'] = true;
			$dataContracts['created_by'] = $created_by;
			$dataContracts['created'] = $created;
			#prr($dataContracts);			
			
			$this->loadModel('Contracts');
			$contracts = $this->Contracts->newEntity();
			$contracts = $this->Contracts->patchEntity($contracts, $dataContracts);#prr($contracts);
			if ($this->Contracts->save($contracts)) {
				$this->apiResponse['Data']['message'] =  'Create Contract Succeed';
			}else{
				$this->responseStatus = 'Fail';
				$this->apiResponse['Data']['message'] =  "Can't Create Contract";
			}
			
		}else{
			
			$this->responseStatus = 'Fail';
			$this->apiResponse['Data']['message'] = 'Duplicate Contract';
		
		}
			
	}else{
			
		$this->responseStatus = 'Fail';
		$this->apiResponse['Data']['message'] = 'Parameter Empty';
		
	}			
					
    }    

    public function error()
    {
        $throwException = true;

        if ($throwException) {
            throw new NotFoundException();
        }

        // your other logic will be here
        // and finally set your response
        // $this->apiResponse['you_response'] = 'your response data';
    }

}