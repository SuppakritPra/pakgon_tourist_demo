<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\I18n;
use RestApi\Controller\ApiController;
use Cake\Utility\Security;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
use Cake\Http\Client;

class ProfilesController extends ApiController {

    public function initialize() {
        parent::initialize();
        #$this->loadComponent('Array');
    }

    public function index() {
        $this->request->allowMethod('post');

        $token = str_replace('Bearer ', '', $this->request->getHeaderLine('Authorization')); #prd($token);
        #$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
        #prd($token);

        $user = JwtToken::decodeToken($token); #prd($user);
        #$user->user_id = 13112;


        $this->loadModel('Users');
        $Users = $this->Users->find('all', [
                    'conditions' => ['id' => $user->user_id],
                        ]
                )->first();
        if (!empty($Users))
            $Users = $Users->toArray();
        #prd($Users);		

        $this->loadModel('UserPersonals');
        $UserPersonals = $this->UserPersonals->find('all', [
                    'conditions' => ['user_id' => $user->user_id],
                        ]
                )->first();
        if (!empty($UserPersonals))
            $UserPersonals = $UserPersonals->toArray();
        #prr($UserPersonals);

        $this->loadModel('UserProfiles');
        $UserProfiles = $this->UserProfiles->find('all', [
            'conditions' => ['user_id' => $user->user_id],
            'order' => 'id asc'
                ]
        );
        if (!empty($UserProfiles))
            $UserProfiles = $UserProfiles->toArray();
        #prd($UserProfiles);

        $this->loadModel('UserHomeplaces');
        $UserHomeplaces = $this->UserHomeplaces->find('all', [
                    'conditions' => ['user_id' => $user->user_id],
                        ]
                )->first();
        if (!empty($UserHomeplaces))
            $UserHomeplaces = $UserHomeplaces->toArray();
        #prd($UserHomeplaces);

        $this->loadModel('UserDefaultLanguages');
        $UserDefaultLanguages = $this->UserDefaultLanguages->find('all', [
                    'conditions' => ['user_id' => $user->user_id],
                        ]
                )->first();
        if (!empty($UserDefaultLanguages))
            $UserDefaultLanguages = $UserDefaultLanguages->toArray();
        #prd($UserDefaultLanguages);	
        #----------------------------------------------------------------------

        $this->apiResponse['Data']['Users'] = $Users;
        $this->apiResponse['Data']['UserPersonals'] = $UserPersonals;
        $this->apiResponse['Data']['UserProfiles'] = $UserProfiles;
        $this->apiResponse['Data']['UserHomeplaces'] = $UserHomeplaces;
        $this->apiResponse['Data']['UserDefaultLanguages'] = $UserDefaultLanguages;
    }

    public function updateProfile() {
        $this->request->allowMethod('post');

        $data = $this->request->data['data'];
        prd($data);

        $data_validate = $this->validate($data);

        if (empty($data_validate)) {

            $dataUsers = [];

            #--------------------------------------------------------------------
            $dataUserPersonals = [];
            $dataUserPersonals['person_card_no'] = $data['person_card_no'];
            #$dataUserPersonals['master_prefix_id'] = $data['master_prefix_id']; 
            $dataUserPersonals['firstname_th'] = $data['firstname'];
            $dataUserPersonals['lastname_th'] = $data['lastname'];
            $dataUserPersonals['firstname_en'] = $data['firstname'];
            $dataUserPersonals['lastname_en'] = $data['lastname'];
            $dataUserPersonals['gender'] = 'F'; #F:M 
            $dataUserPersonals['birthdate'] = date("Y-m-d", strtotime($data['birthdate']));
            $dataUserPersonals['moblie_no'] = $data['phone_no'];
            $dataUserPersonals['phone_no'] = $data['phone_no'];
            $dataUserPersonals['email'] = $data['email'];
            #$dataUserPersonals['user_id'] = $data['user_id']; 
            #$dataUserPersonals['blood_group'] = '';
            $data['person_card_no'] = str_replace('-', '', trim($data['person_card_no']));
            #$dataUserPersonals['address'] = $data['person_card_no'];

            $dataUserPersonals['created_by'] = $created_by;
            $dataUserPersonals['created'] = $created;
            #prr($dataUserPersonals);
            #--------------------------------------------------------------------
            #--------------------------------------------------------------------
            $dataUserProfiles = [];
            #$dataUserProfiles['user_id'] = $data['user_id']; 
            #$dataUserProfiles['organize_id'] = $data['organize_id']; 
            #$dataUserProfiles['dept_id'] = $data['dept_id']; 
            #$dataUserProfiles['section_id'] = $data['section_id']; 
            #$dataUserProfiles['card_code'] = $data['card_code']; 
            $dataUserProfiles['user_type_id'] = $data['user_type_id'] = 5;
            #$dataUserProfiles['img_path'] = $data['img_path']; 
            $dataUserProfiles['is_used'] = true;
            #$dataUserProfiles['position_org'] = $data['position_org']; 
            #$dataUserProfiles['position_edu'] = $data['position_edu']; 
            #$dataUserProfiles['address'] = $data['address'];
            #$dataUserProfiles['master_bussiness_type_id'] = $data['master_bussiness_type_id'];

            $dataUserProfiles['created_by'] = $created_by;
            $dataUserProfiles['created'] = $created;
            #prr($dataUserProfiles);
            #--------------------------------------------------------------------

            $this->loadModel('UserPersonals');
            $userPersonals = $this->UserPersonals->find('all', [
                        'fields' => ['id', 'user_id'],
                        'order' => ['id' => 'DESC'],
                        'limit' => 1
                            ]
                    )->toArray();
            #prr($userPersonals);	

            $user_id = $dataUserPersonals['user_id'] = $userPersonals[0]['id'] + 1;
            #--------------------------------------------------------------------
            #prr($dataUserPersonals);
            $this->loadModel('UserPersonals');
            $userPersonals = $this->UserPersonals->newEntity();
            $userPersonals = $this->UserPersonals->patchEntity($userPersonals, $dataUserPersonals); #prr($userPersonals);
            if ($this->UserPersonals->save($userPersonals)) {

                #--------------------------------------------------------------------
                #$dataUsers['id'] = $user_id;prr($dataUsers);

                $this->loadModel('Users');
                $users = $this->Users->newEntity(); #prr($users);
                $users = $this->Users->patchEntity($users, $dataUsers); #prr($users);
                $users->id = $user_id;
                #prd($users);
                if ($this->Users->save($users)) {
                    $this->apiResponse['message'] = 'Insert User Succeed';
                } else {
                    $this->responseStatus = 'Fail';
                    #$this->apiResponse['type'] =  'error';
                    $this->apiResponse['message'] = 'Insert User';
                }
                #--------------------------------------------------------------------
                #--------------------------------------------------------------------
                $dataUserProfiles['user_id'] = $user_id;

                $this->loadModel('UserProfiles');
                $userProfiles = $this->UserProfiles->newEntity(); #prr($userProfiles);
                $userProfiles = $this->UserProfiles->patchEntity($userProfiles, $dataUserProfiles); #prr($userProfiles);
                if ($this->UserProfiles->save($userProfiles)) {
                    $this->apiResponse['message'] = 'Insert User Profile Succeed';
                } else {
                    $this->responseStatus = 'Fail';
                    #$this->apiResponse['type'] =  'error';
                    $this->apiResponse['message'] = 'Insert User Profile';
                }
                #--------------------------------------------------------------------
                #--------------------------------------------------------------------
                $dataUserHomeplaces['user_id'] = $user_id;
                $dataUserHomeplaces['master_country_id'] = 1;
                $dataUserHomeplaces['master_province_id'] = intval(substr($data['person_card_no'], 1, 2));
                $dataUserHomeplaces['master_district_id'] = intval(substr($data['person_card_no'], 3, 2));
                $dataUserHomeplaces['is_used'] = true;

                $dataUserHomeplaces['created_by'] = $created_by;
                $dataUserHomeplaces['created'] = $created;

                #$this->apiResponse['UserHomeplaces'] =  $dataUserHomeplaces;
                $this->loadModel('UserHomeplaces');
                $UserHomeplaces = $this->UserHomeplaces->newEntity(); #prr($UserHomeplaces);
                $UserHomeplaces = $this->UserHomeplaces->patchEntity($UserHomeplaces, $dataUserHomeplaces); #prr($UserHomeplaces);
                if ($this->UserHomeplaces->save($UserHomeplaces)) {
                    $this->apiResponse['data']['token'] = $dataUsers['token'];
                    $this->apiResponse['message'] = 'Insert User Home Place Succeed';
                } else {
                    $this->responseStatus = 'Fail';
                    #$this->apiResponse['type'] =  'error';
                    $this->apiResponse['message'] = 'Insert User Home Place ';
                }
                #--------------------------------------------------------------------			

                $data_notification = [];
                $data_notification['email'] = $data['email'];
                $data_notification['pin_code'] = $dataUsers['pin_code'];
                $this->notification($data_notification);

                if ($this->responseStatus != 'Fail')
                    $this->apiResponse['message'] = 'Signup Connect Completed!!';
            }else {
                $this->apiResponse['type'] = 'error';
                $this->apiResponse['message'] = 'Insert User Personal';
            }
        } else {
            $this->responseStatus = $data_validate['status'];
            $this->apiResponse = $data_validate['result'];
        }
    }

    function validate($data = null) {

        $data_return = array();

        if (empty($data)) {
            $data_return['status'] = 'Fail';
            $data_return['result']['code'] = 'error';
            $data_return['result']['message'] = __('Data Empty');
            return $data_return;
        }

        if (!empty($data)) {
            $data_return = array();
            $this->loadModel('Users');
            $users = $this->Users->find('all', [
                        #'conditions' => ['is_used' => true,'username' => $data['username']],
                        'conditions' => ['username' => $data['username']],
                        'fields' => ['id', 'username'],
                        'limit' => 1
                            ]
                    )->toArray();
            #prr($users);
            if (!empty($users)) {
                $data_return['status'] = 'Fail';
                $data_return['result']['code'] = 'error';
                $data_return['result']['message'] = __('User Duplicate');
            }
        }
        #prd($data_return);
        return $data_return;
    }

    //Oauth Version
//    public function info() {
//        $this->request->allowMethod('post');
//        $token = str_replace('Bearer ', '', $this->request->getHeaderLine('Authorization')); #prd($token);
//        $params = ['token' => $token];
//        $http = new Client();
//        $response = $http->post(Configure::read('OAUTH2_PROVIDER.TOKEN_INFO_USER'), $params)->body();
//        $response = json_decode($response, true);
//        
//        debug($response);
//        if (strtoupper($response['status']) != 'OK') {
//            $this->responseStatus = 'Fail';
//            $this->apiResponse['message'] = __('Not found user info');
//        } else {
//            $user_id = $response['result']['result']['user_id'];
//            $data = json_decode(file_get_contents("php://input"), true); #prr($data);
//            $accept_language = $this->request->getHeaderLine('Accept-Language'); #prd($accept_language);	#en;q=1.0  /  en-US,en;q=1.0
//            #$accept_language = 'en;q=1.0';
//            #$accept_language = 'en-US,en;q=1.0';
//            if (!empty($accept_language)) {
//                $parts = explode(';', $accept_language);
//                $languages = explode(',', $parts[0]);
//                sort($languages);
//                $locale = !empty($languages[1]) ? $languages[1] : strtoupper($languages[0]);
//                $language = $languages[0];
//            } else {
//                $locale = Configure::read('Config.defaultValue.locale');
//                $language = Configure::read('Config.defaultValue.language');
//            }
//
//            $this->loadModel('MasterLanguages');
//            $Languages = $this->MasterLanguages->find('all', [
//                        'conditions' => ['is_used' => true],
//                        'fields' => ['language_abbr', 'language_display'],
//                        'order' => 'seq_no asc'
//                            ]
//                    )->toArray();
//
//            $data = $this->request->getData();
//            $list_languages = ['TH' => 'TH', 'EN' => 'EN'];
//            $data['language'] = isset($data['language']) ? $data['language'] : '';
//            $language = !array_key_exists(strtoupper($data['language']), $list_languages) ? $language : $data['language'];
//            I18n::locale($locale);
//
//            $this->updateLanguage($user_id, $language);
//            $this->loadModel('UserPersonals');
//            $UserPersonals = $this->UserPersonals->find('all', [
//                        'conditions' => ['user_id' => $user_id],
//                            #'fields' => ['language_abbr', 'language_'.$language],
//                            #'order' => 'seq_no asc'
//                            ]
//                    )->first();
//            if (!empty($UserPersonals))
//                $UserPersonals = $UserPersonals->toArray();
//            #prd($UserPersonals);	
//            debug($user_id);
//debug($UserPersonals);exit;
//            $this->loadModel('UserProfiles');
//            $UserProfiles = $this->UserProfiles->find('all', [
//                        'conditions' => ['user_id' => $user_id],
//                            #'fields' => ['language_abbr', 'language_'.$language],
//                            #'order' => 'seq_no asc'
//                            ]
//                    )->first();
//            
//            
//            if (!empty($UserProfiles))
//                $UserProfiles = $UserProfiles->toArray();
//            #prd($UserProfiles);
//
//            $Languages = array_map(function($tag) {
//                $code = strtolower($tag['language_abbr']);
//                $language = array(
//                    'code' => $code,
//                    #'value' => $tag['language_'.$language]
//                    'value' => $tag['language_display']
//                );
//                return $language;
//            }, $Languages);
//            #prd($Languages);
//            #$Languages = ['label' => __('Language'),'list' => $Languages];
//            $Languages = ['label' => 'ภาษา', 'list' => $Languages];
//
//            $Menus = [
//                //['label' => __('Language'),'url' => ''],
//                ['label' => __('Signout'), 'url' => '']
//            ];
//
//            #----------------------------------------------------------------------
//            $suffixLang = strtolower($language);
//            $Profiles = [];
//            $Profiles['firstname'] = $UserPersonals['firstname_' . $suffixLang];
//            $Profiles['lastname'] = $UserPersonals['lastname_' . $suffixLang];
//            $Profiles['gender'] = $UserPersonals['gender'];
//            $Profiles['img_path'] = !empty($UserProfiles['img_path']) ? $UserProfiles['img_path'] : 'http://connect01.pakgon.com/img/connect/user/profile143x143.png';
//
//            $this->apiResponse['Data']['Profiles'] = $Profiles;
//            $this->apiResponse['Data']['Languages'] = $Languages;
//            $this->apiResponse['Data']['Menus'] = $Menus;
//        }
//    }

    public function info() {
        $this->request->allowMethod('post');

        $token = str_replace('Bearer ', '', $this->request->getHeaderLine('Authorization')); #prd($token);
        #$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";

        $data = json_decode(file_get_contents("php://input"), true); #prr($data);
        $accept_language = $this->request->getHeaderLine('Accept-Language'); #prd($accept_language);	#en;q=1.0  /  en-US,en;q=1.0
        #$accept_language = 'en;q=1.0';
        #$accept_language = 'en-US,en;q=1.0';
        if (!empty($accept_language)) {
            $parts = explode(';', $accept_language); #prr($parts);
            $languages = explode(',', $parts[0]);
            sort($languages);
            #prd($languages);

            $locale = !empty($languages[1]) ? $languages[1] : strtoupper($languages[0]);
            $language = $languages[0];
        } else {
            $locale = Configure::read('Config.defaultValue.locale');
            $language = Configure::read('Config.defaultValue.language');
        }

        $user = JwtToken::decodeToken($token); #prd($user);
        #$this->apiResponse['Data']['user'] = $user;
        #$this->apiResponse['Data']['accept_language'] = $accept_language;
        #$this->apiResponse['Data']['language'] = $language;

        $this->loadModel('MasterLanguages');
        $Languages = $this->MasterLanguages->find('all', [
                    'conditions' => ['is_used' => true],
                    #'fields' => ['language_abbr', 'language_'.$language],
                    'fields' => ['language_abbr', 'language_display'],
                    #'keyField' => 'language_abbr',
                    #'valueField' => 'language_display',
                    'order' => 'seq_no asc'
                        ]
                )->toArray();
        #$Languages = $masterLanguages->toArray();
        #prd($Languages);

        $data = $this->request->getData(); #prr($data);
        #prr(!array_key_exists(strtoupper($data['language']),['TH','EN']));
        $list_languages = ['TH' => 'TH', 'EN' => 'EN']; #prr($list_languages);
        $data['language'] = isset($data['language']) ? $data['language'] : '';
        #if(empty($list_languages[strtoupper($data['language'])])) $language = 'en';
        $language = !array_key_exists(strtoupper($data['language']), $list_languages) ? $language : $data['language'];
        #prd($language);	
        I18n::locale($locale);

        $this->updateLanguage($user->user_id, $language);

        $this->loadModel('UserPersonals');
        $UserPersonals = $this->UserPersonals->find('all', [
                    'conditions' => ['user_id' => $user->user_id],
                        #'fields' => ['language_abbr', 'language_'.$language],
                        #'order' => 'seq_no asc'
                        ]
                )->first();
        if (!empty($UserPersonals)) {
            $UserPersonals = $UserPersonals->toArray();
        }

        #prd($UserPersonals);		

        $this->loadModel('UserProfiles');
        $UserProfiles = $this->UserProfiles->find('all', [
                    'conditions' => ['user_id' => $user->user_id],
                        #'fields' => ['language_abbr', 'language_'.$language],
                        #'order' => 'seq_no asc'
                        ]
                )->first();
        if (!empty($UserProfiles)) {
            $UserProfiles = $UserProfiles->toArray();
        }

        #prd($UserProfiles);

        $Languages = array_map(function($tag) {
            $code = strtolower($tag['language_abbr']);
            $language = array(
                'code' => $code,
                #'value' => $tag['language_'.$language]
                'value' => $tag['language_display']
            );
            return $language;
        }, $Languages);
        #prd($Languages);
        #$Languages = ['label' => __('Language'),'list' => $Languages];
        $Languages = ['label' => 'ภาษา', 'list' => $Languages];

        /**
         * Comment by sarawutt.b on 2018/08/15
         * with reason not compatible with mobile
         */
//        $Menus = [
//            //['label' => __('Language'),'url' => ''],
//            ['label' => __('Signout'), 'url' => '']
//        ];

        $Menus = [
            ['code' => 'Change Password', 'label' => __('Change Password'), 'url' => Configure::read('CORE.CHANGE_PASSWORD_URL')],
            ['code' => 'Signout', 'label' => __('Signout'), 'url' => Configure::read('CORE.SIGNOUT_URL')]
        ];

        //Modifire by sarawutt.b
        //on 2018/07/09
        //Reason priority return expect lang if not null ortherwise return other
        $currentLange = strtolower($language);
        $Profiles = [];

        $Profiles['firstname'] = $UserPersonals["firstname_{$currentLange}"];
        $Profiles['lastname'] = $UserPersonals["lastname_{$currentLange}"];

        if (empty($Profiles['firstname']) || empty($Profiles['lastname'])) {
            $currentLange = ($currentLange == 'en') ? 'th' : 'en';
            $Profiles['firstname'] = $UserPersonals["firstname_{$currentLange}"];
            $Profiles['lastname'] = $UserPersonals["lastname_{$currentLange}"];
        }

        $Profiles['gender'] = $UserPersonals['gender'];
        $Profiles['img_path'] = !empty($UserProfiles['img_path']) ? $UserProfiles['img_path'] : Configure::read('DEFAULT_PROFILE_IMAGE');

        $this->apiResponse['Data']['Profiles'] = $Profiles;
        $this->apiResponse['Data']['Languages'] = $Languages;
        $this->apiResponse['Data']['Menus'] = $Menus;
    }

    public function updateLanguage($user_id, $language) {

        #prr($user_id);
        #prd($language);
        $this->loadModel('MasterLanguages');
        $MasterLanguages = $this->MasterLanguages->find('list', [
            'keyField' => 'language_abbr',
            'valueField' => 'id',
                ]
        );
        if (!empty($MasterLanguages))
            $MasterLanguages = $MasterLanguages->toArray();
        #prr($MasterLanguages);

        $this->loadModel('UserDefaultLanguages');
        $UserDefaultLanguages = $this->UserDefaultLanguages->find('all', [
                    'conditions' => ['user_id' => $user_id],
                        ]
                )->first();
        #if(!empty($UserDefaultLanguages)) $UserDefaultLanguages = $UserDefaultLanguages->toArray();
        #prd($UserDefaultLanguages);

        $dataUserDefaultLanguages = [];
        if (empty($UserDefaultLanguages)) {
            $UserDefaultLanguages = $this->UserDefaultLanguages->newEntity();
            $dataUserDefaultLanguages['user_id'] = $user_id;
            $dataUserDefaultLanguages['is_used'] = true;
            $dataUserDefaultLanguages['created'] = date('Y-m-d');
            $dataUserDefaultLanguages['created_by'] = $user_id;
        } else {
            $dataUserDefaultLanguages['id'] = $UserDefaultLanguages['id'];
        }
        $dataUserDefaultLanguages['master_language_id'] = !empty($MasterLanguages[strtoupper($language)]) ? $MasterLanguages[strtoupper($language)] : 2;
        #prd($dataUserDefaultLanguages);

        $UserDefaultLanguages = $this->UserDefaultLanguages->patchEntity($UserDefaultLanguages, $dataUserDefaultLanguages); #prr($UserDefaultLanguages);				
        $this->UserDefaultLanguages->save($UserDefaultLanguages);
    }

    public function forgetPassword() {
        $this->request->allowMethod('post');
        $data = $this->request->data; #debug($data);exit();

        $this->loadModel('Users');
        $users = $this->Users->find('all', [
                    'conditions' => ['username' => $data['username'], 'is_used' => true],
                    'limit' => 1
                        ]
                )->first();
        #debug($users);exit();
        #if(!empty($users)) $users = $users->toArray();
        #--------------------------------------------------------------------
        if (!empty($users)) {

            $data['password'] = Security::hash($data['password'], 'sha256', true); #debug($data);
            #$this->loadModel('Users');
            $users = $this->Users->find('all', [
                        'conditions' => ['username' => $data['username'], 'password' => $data['password'], 'is_used' => true],
                        'limit' => 1
                            ]
                    )->first();
            #prr($users);

            if (!empty($users)) {

                $users = $users->toArray();

                $user_id = $users['id'];
                $dataUsers = [];
                #$dataUsers['id'] = $users['id']; 
                $dataUsers['dynamic_key'] = 'dynamic_key';
                $dataUsers['dynamic_key_expiry'] = date('Y-m-d', strtotime('+3 day'));
                $dataUsers['token'] = Security::hash($data['username'] . date('Y-m-d h:i:s'), 'md5', true);
                $dataUsers['token_expiry'] = date('Y-m-d', strtotime('+3 day'));
                #prr($dataUsers);

                $this->loadModel('Users');
                #$users = $this->Users->newEntity();#prr($users);
                $users = $this->Users->get($user_id);
                $users = $this->Users->patchEntity($users, $dataUsers); #prr($users);
                $this->Users->save($users);

                $topic = $users['username'] . $dataUsers['token'];
                $user = ['user_id' => $users['id'], 'username' => $users['username'], 'ip' => $data['ip'], '$topic' => $topic];
                #$token = JwtToken::generateToken($user);

                $this->apiResponse['token'] = JwtToken::generateToken($user);
                $this->apiResponse['topic'] = $topic;
                $this->apiResponse['message'] = 'Signin Success.';
            } else {
                $this->responseStatus = 'Fail';
                $this->apiResponse['type'] = 'error';
                $this->apiResponse['message'] = 'Password Incorrect.';
            }
        } else {
            $this->responseStatus = 'Fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = 'User Incorrect.';
        }
    }

    public function verify() {
        $this->request->allowMethod('post');

        pr($this->name);
        die;
    }

    public function changePassword() {
        $this->request->allowMethod('post');
        debug('changePassword');
        pr($this->name);
        die;
    }

    public function setPassword() {
        $this->request->allowMethod('post');

        pr($this->name);
        die;
    }

    public function notification() {
        $this->request->allowMethod('post');

        pr($this->name);
        die;
    }

    public function barcode() {
        $this->request->allowMethod('post');

        pr($this->name);
        die;
    }

    public function error() {
        $throwException = true;

        if ($throwException) {
            throw new NotFoundException();
        }

        // your other logic will be here
        // and finally set your response
        // $this->apiResponse['you_response'] = 'your response data';
    }

    public function getCategoriesHome() {
        $this->loadModel('Api');
        if ($this->request->is('post')) {
            $status = 'Success';
            $api_profile = 'http://connect06.pakgon.com/api/Connects/profile';
            $http = new Client();
            $data = [];
            $options = ['headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => $this->request->getHeaderLine('Authorization')
                #'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InRlc3QiLCJwYXNzd29yZCI6InRlc3QiLCJpcCI6IjExMC43Ny4xODIuNzkifQ.xqi1iam460V-DOXJ2wxVJsAccsh4V7iLL_1slLxy99A'
                ]
            ];
            $tmpResponse = $http->post($api_profile, $data, $options)->body();
            $tmpResponse = json_decode($tmpResponse, true);
            $responseData = $tmpResponse['result']['Data'];
            $datas = $this->Api->getCategories(true, $responseData['Profiles']['language_id'], $responseData['Profiles']);
            $dataResponse = ['data' => $datas];
        } else {
            $status = 'Fail';
            $dataResponse = ['message' => 'data response can not be found'];
        }
        $this->set(['status' => $status, 'result' => $dataResponse, '_serialize' => ['status', 'result']]);
    }

}
