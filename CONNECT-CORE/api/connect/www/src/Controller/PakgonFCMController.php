<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\I18n;
use RestApi\Controller\ApiController as AppController;
use Cake\Utility\Security;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
use Cake\Http\Client;

/**
 * 
 * PakgonFCM Controller 
 * @author sarawutt.b
 */
class PakgonFCMController extends AppController {

    /**
     * 
     * Function make beacon send FCM notification
     * @author sarawutt.b
     * @return json result
     */
    public function beaconSendNotification() {
        $this->request->allowMethod(['post', 'put', 'patch']);
        if (!$this->request->is(['post', 'put', 'patch'])) {
            $this->responseStatus = 403;
            $this->apiResponse = ['type' => 'error', 'message' => $this->request->method() . 'not allow you must instread with "POST / PUT / PATCH"'];
            return;
        }

        $params = $this->request->getData();
        $token = str_replace('Bearer ', '', $this->request->getHeaderLine('Authorization'));
        if (!array_key_exists('beacon_value', $params) || empty($params['beacon_value'])) {
            $this->responseStatus = 'fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Please check "beacon_value" must be appear and not empty');
        }


        if (empty($token)) {
            $this->responseStatus = 'fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Please check "Authorization" must be appear and not empty');
            return;
        }

        try {
            $user = JwtToken::decodeToken($token);
            if (empty($user)) {
                $this->responseStatus = 'fail';
                $this->apiResponse['type'] = 'error';
                $this->apiResponse['message'] = __('Please check "Authorization" invalid token');
                return;
            } else {
                $this->loadModel('MasterBeaconDetects');
                $masterBeaconDetects = $this->MasterBeaconDetects->newEntity();
                $this->request->data['appname'] = 'SMART_TOURISM';
                $this->request->data['token'] = $token;
                $this->request->data['user_id'] = $user->user_id;
                $this->request->data['topic'] = $user->topic;
                $this->request->data['remote_address'] = $user->ip;
                $this->request->data['status'] = 'N';
                $this->request->data['create_uid'] = '999';
                $masterBeaconDetects = $this->MasterBeaconDetects->patchEntity($masterBeaconDetects, $this->request->getData());
                if ($this->MasterBeaconDetects->save($masterBeaconDetects)) {
                    $this->apiResponse['data'] = $user;
                    $this->apiResponse['type'] = 'ok';
                    $this->apiResponse['message'] = __('Successfully for your request');
                    return;
                }
            }
        } catch (UnexpectedValueException $e) {
            $this->responseStatus = 'fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Please check "Authorization" invalid token');
            return;
        }
    }

}
