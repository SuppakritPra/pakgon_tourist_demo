<?php
namespace App\Controller;

use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
class PointsController extends ApiController
{

    public function index()
    {
		$this->request->allowMethod('post');

		pr($this->name);die;
    }
    
    public function  addUserPoint()
    {
		$this->request->allowMethod('post');
		
		$data = $this->request->data;#prd($data);
		
		if(!empty($data)){

			$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
			#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
			#prd($token);

			$user = JwtToken::decodeToken($token);#prd($user);
			$user_id = $user->user_id;					
			
			$this->loadModel('Users');
			$Users = $this->Users->find('all',
							[
							'fields' => ['id', 'point'],
							'conditions' => ['id' => $user_id],
							]
						   )->first();
			if(!empty($Users)) $Users = $Users->toArray();
			#prd($Users);

			if(!empty($Users)){

				$point = $Users['point'] + $data['point'];

				$this->loadModel('Users');
				$users = $this->Users->newEntity();#prr($users);
				$users->id = $user_id;
				$users->point = $point;
				#prd($users);
				if ($this->Users->save($users)) {
					$this->apiResponse['Data']['message'] =  'Update Point Succeed';
				}else{
					$this->responseStatus = 'Fail';
					$this->apiResponse['Data']['message'] =  "Can't Update Point";
				}
			
				$Points = [];
				$Points['point'] = $point;
				$this->apiResponse['Data']['Points'] = $Points;
		
			}else{
			
				$this->responseStatus = 'Fail';
				$this->apiResponse['Data']['message'] = 'User Not Found';
		
			}
			
		}else{
			
			$this->responseStatus = 'Fail';
			$this->apiResponse['Data']['message'] = 'Parameter Empty';
		
		}			
		
    }

}