<?php

// Android Payload
//        $payload_android = [
//            'title' => $params['title'],
//            'body' => $params['message'],
//            'icon' => 'icon',
//            'sound' => 'sound',
//            'badge' => $param['badge'],
//            'click_action'=> $params['click_action']
//        ];
//iOS Payload
//        $payload_ios = [
//            'title' => $param['title'],
//            'body' => $param['message'],
//            'sound' => 'sound',
//            'badge' => $params['badge'],
//            'click_action' => $param['click_action']
//        ];
// Example Mapping parameter        
//        {
//            to:"/topics/news",
//            priority:"high",
//            notification:{
//                title:"Google I/O 2016",
//                text:"Firebase Clound Messaging (Server)",
//                sound:"default",
//                badge:1,
//                click_action:"OPEN_ACTIVITY_1"
//            },
//            data: {
//                picture_url:"http://opsbug.com/static/google-io.jpg"
//            }
//        }

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;

/**
 * PakgonFCMPusher component
 */
class PakgonFCMPusherComponent extends Component {

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * 
     * Function Pakgon Firebase Cloud Messaging (FCM)
     * @author sarawutt.b
     * @param type $params as array of push notification
     * @param type $paramDatas as array of send data
     * @return HTML body response
     */
    public function push($params = [], $paramDatas = []) {
        $headerOptions = ['headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key=' . Configure::read('FCM.PUSH.KEY')
        ]];

        $notification = [
            'title' => $params['title'],
            'body' => empty($params['message']) ? 'Pakgon push notification' : $params['message'],
            'icon' => empty($params['icon']) ? null : $params['icon'],
            'sound' => empty($params['sound']) ? null : $params['sound'],
            'badge' => empty($params['badge']) ? 1 : $params['badge'],
        ];

        $customData = [
            'title' => $params['title'],
            'body' => empty($params['message']) ? 'Pakgon push notification' : $params['message'],
            'badge' => empty($params['badge']) ? 1 : $params['badge'],
        ];

        if (!is_array($paramDatas)) {
            $paramDatas = array($paramDatas);
        }
        
        $customData = array_merge($customData, $paramDatas);

        $data = [
            'to' => $params['topic'],
            'notification' => $notification,
            'data' => $customData,
            'priority' => 'high'
        ];

        $http = new Client();
        $response = $http->post(Configure::read('FCM.PUSH.URL'), json_encode($data), $headerOptions)->body();
        return $response;
    }

    public function test() {
        return 'OK';
    }

}
