<?php
namespace App\Controller;

use Cake\Http\Client;
use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
use chillerlan\QRCode\Output\QRMarkup;
use chillerlan\QRCode\Output\QRMarkupOptions;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

class BarcodesController extends ApiController
{

    public function index()
    {
    
	#error_reporting(0);
	
	$this->request->allowMethod('post');
		
	$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
	#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
        
	$user = JwtToken::decodeToken($token);#prd($user);
	$user_id = $user->user_id;		
				
	echo '<style>

	.qrcode,
	.qrcode > p,
	.qrcode > p > b,
	.qrcode > p > i {
		margin:0;
		padding:0;
	}

	/* row element */
	.qrcode > p {
		height: 1.25mm;
		display: block;
	}

	/* column element(s) */
	.qrcode > p > b, .qrcode > p > i{
		display: inline-block;
		width: 1.25mm;
		height: 1.25mm;
	}

	.qrcode > p > b{
		background-color: #000;
	}

	.qrcode > p > i{
		background-color: #fff;
	}

	</style>';

	$qrStringOptions = new QRMarkupOptions;
	$qrStringOptions->type = QRCode::OUTPUT_MARKUP_HTML;

	$qrOptions = new QROptions;
	$qrOptions->typeNumber = QRCode::TYPE_10;
	$qrOptions->errorCorrectLevel = QRCode::ERROR_CORRECT_LEVEL_L;
	
	#$token = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxccccccccccccccccccccccvfsrgsdfgdfhdhhhhdfjnhdffsrghsdfhdtujgdss';	#limit
	#$token = 'test-token';
	$data = '3|addFriend|'.$token;
		
	$qr = new QRCode($data, new QRMarkup($qrStringOptions), $qrOptions);

	echo '<div class="qrcode">'.$qr->output().'</div>';die;

    }
    
    public function request(){
    
    	$this->request->allowMethod('post');
	
        $data = $this->request->data['data'];prr($data);
	
	$data_validate = $this->validate($data);
	
	if(empty($data_validate)){
			
		$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
		#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
        
		$user = JwtToken::decodeToken($token);prd($user);
		$user_id = $user->user_id;
		
		$dataQrcodes = [];
		$dataQrcodes['message'] = '2|addFriend|'.$token;		
	
		$this->loadModel('MasterApiQrcodes');
		$MasterApiQrcodes = $this->MasterApiQrcodes->find('all',
								[
									'conditions' => ['is_used' => true,'api_qrcode_name' => $arr_data['api_qrcode_name']],
								]
								)->first();
		if(!empty($MasterApiQrcodes)) $MasterApiQrcodes = $MasterApiQrcodes->toArray();
		#prd($MasterApiQrcodes);
	}else{
		$this->apiResponse = $data_validate;
	}	
	
    }
    
    public function verify(){    
    
    	$this->request->allowMethod('post');
	
        #$data = $this->request->data;prr($data);
	$data = json_decode(file_get_contents("php://input"),true);#prr($data);
	#$data['code'] = "3|addFriend|eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzMCwidXNlcm5hbWUiOiJ0ZXN0dG9uZyIsImlwIjoiMTI3LjAuMC4xIn0.37hXRDEhiwjsgqR8mcgbhxnKtAof3tIDahL2P6DnQ4I";
	
	$data_validate = $this->validate($data);#prd($data_validate);
	
	if(empty($data_validate)){
		
		$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
		#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
        
		$user = JwtToken::decodeToken($token);#prd($user);
		$user_id = $user->user_id;
		
		$arr_data = explode('|',$data['code']);#prd($arr_data);
		$arr_data['api_qrcode_name'] = $arr_data[1];

		$this->loadModel('MasterApiQrcodes');
		$MasterApiQrcodes = $this->MasterApiQrcodes->find('all',
								[
									'conditions' => ['is_used' => true,'api_qrcode_name' => $arr_data['api_qrcode_name']],
								]
								)->first();
		if(!empty($MasterApiQrcodes)) $MasterApiQrcodes = $MasterApiQrcodes->toArray();
		#prd($MasterApiQrcodes);

		if(!empty($MasterApiQrcodes)){
			
			$api_type = $MasterApiQrcodes['api_type'];
			$api_url_path = $MasterApiQrcodes['url_path'];
			
			if($api_type == 'api'){
							
				$http = new Client();
				$options = ['headers' => [
							 'Content-Type' => 'application/x-www-form-urlencoded',
							 'Authorization' => $this->request->getHeaderLine('Authorization')
							 ]
					   ];
				#prd($data);
				$response = json_decode($http->post($api_url_path,$data,$options)->body(),'_full');#prd($response);
				#$response = json_decode($response,'_full');#prd($response);
				
				$this->responseStatus = $response['status'];
				$this->apiResponse = $response['result'];

			}else if($api_type == 'message'){
			
				$this->apiResponse['Data']['url'] = $api_url_path;
				
			}		
			
		}else{
		
			$this->responseStatus = 'Fail';
			$this->apiResponse['Data']['message'] = 'Api Not Found';
			
		}
		
	}else{
		
		$this->responseStatus = 'Fail';
		$this->apiResponse['Data'] = $data_validate;
		
	}			

    }
    
    public function response(){
    
	$this->request->allowMethod('post');

	$this->httpStatusCode = 200;
	$this->apiResponse['Data']['Model'] = 'response data';    
    
    }
    
    function validate($data = null){
    
    	$data_return = array();
	
	if(empty($data)) $data_return['message'] = 'Parameter Empty';
	
	return $data_return;
    
    }        

    public function error()
    {
        $throwException = true;

        if ($throwException) {
            throw new NotFoundException();
        }

        // your other logic will be here
        // and finally set your response
        // $this->apiResponse['you_response'] = 'your response data';
    }

}