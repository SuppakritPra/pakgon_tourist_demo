<?php
namespace App\Controller;

use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
class ConnectsController extends ApiController
{

    public function index()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }
    
    public function profile()
    {
	$this->request->allowMethod('post');

	$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
	#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
	#prd($token);

	$user = JwtToken::decodeToken($token);#prd($user);
	$user_id = $user->user_id;
		
	$this->loadModel('UserPersonals');
	$UserPersonals = $this->UserPersonals->find('all',
						[
							'conditions' => ['user_id' => $user_id],
						]
						)->first();
	if(!empty($UserPersonals)) $UserPersonals = $UserPersonals->toArray();
	#prd($UserPersonals);

	$this->loadModel('UserProfiles');
	$UserProfiles = $this->UserProfiles->find('all',
						[
							'conditions' => ['user_id' => $user_id],
							'order' => ['id' => 'Desc']
						]
						)->first();
	if(!empty($UserProfiles)) $UserProfiles = $UserProfiles->toArray();
	#prd($UserProfiles);
		
	$this->loadModel('UserDefaultLanguages');
	$UserDefaultLanguages = $this->UserDefaultLanguages->find('all',
						[
							'conditions' => ['user_id' => $user_id],
						]
						)->first();
	if(!empty($UserDefaultLanguages)) $UserDefaultLanguages = $UserDefaultLanguages->toArray();
	#prd($UserDefaultLanguages);
			
	#----------------------------------------------------------------------
	$language = 'th';
	$Profiles = [];
	$Profiles['user_id'] = $UserPersonals['user_id'];
	$Profiles['firstname'] = $UserPersonals['firstname_'.$language];
	$Profiles['lastname'] = $UserPersonals['lastname_'.$language];	
	$Profiles['master_user_type_id'] = $UserProfiles['user_type_id'];
	$Profiles['master_organization_id'] = $UserProfiles['organize_id'];
	$Profiles['master_department_id'] = $UserProfiles['section_id'];
	$Profiles['master_section_id'] = $UserProfiles['section_id'];
	$Profiles['gender'] = $UserPersonals['gender'];
	$Profiles['birthdate'] = date('Y-m-d',strtotime($UserPersonals['birthdate']));
	$Profiles['language_id'] = !empty($UserDefaultLanguages['master_language_id'])? $UserDefaultLanguages['master_language_id']:2;
	$Profiles['img_path'] = $UserProfiles['img_path'];

	#----------------------------------------------------------------------
		
	$this->apiResponse['Data']['Profiles'] = $Profiles;
    }

    public function permission()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }

    public function access()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }

    public function language($language = 'th')
    {
	$this->request->allowMethod('post');
	
	$acceptsLanguages = $this->request->acceptLanguage();
	#$language = $acceptsLanguages[0];

	$this->loadModel('MasterLanguages');
	$Languages = $this->MasterLanguages->find('all',
						[
							'conditions' => ['is_used' => true],
							'fields' => ['language_abbr', 'language_'.$language],
							'order' => 'seq_no asc'
						]
						)->toArray();
	#$Languages = $masterLanguages->toArray();
	#prr($Languages);

	
	$Languages = array_map(function($tag) {
		return array(
			'code' => $tag['language_abbr'],
			'value' => $tag['language_th']
		);
	}, $Languages);
	
	
	#$Languages = $this->changekeyname($Languages,'code','language_abbr');
	#$Languages = $this->changekeyname($Languages,'value','language_'.$language);

	$this->httpStatusCode = 200;
	$this->apiResponse['Data']['Languages'] = $Languages;
    }
    
    function changekeyname($array, $newkey, $oldkey)
    {
	foreach ($array as $key => $value) 
	{
		if (is_array($value))
			$array[$key] = changekeyname($value,$newkey,$oldkey);
		else
		{
			$array[$newkey] =  $array[$oldkey];    
		}

	}
	unset($array[$oldkey]);          
	return $array;   
    }    
    	
    public function changeLanguage($language = 'th')
    {
	$this->request->allowMethod('post');
		
	$this->httpStatusCode = 200;
	$this->apiResponse['Languages'] = $Languages;
    }	

    public function notification()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }
    
    /*
    public function qrcode()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }    

    public function barcode()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }
    */

    public function location()
    {
	$this->request->allowMethod('post');

	pr($this->name);die;
    }

    public function device()
    {
    
	$this->request->allowMethod('post');
	    
	$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
	#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwibmFtZSI6InRlc3Qgc2VydmVyIiwiY2FyZF9pZCI6bnVsbH0.NPJwUrludB8wyGkaeVeCUDDrB1_yZaOqWNz-Nh42h9I";
	#prd($token);
	
	$data = json_decode(file_get_contents("php://input"),true);
	#$data['type'] = 'beacon';
	#$data['code'] = '5152554E-F99B-4A3B-86D0-947070693A78';
	#prr($data);

	$user = JwtToken::decodeToken($token);#prd($user);
	$user_id = $user->user_id;
		
	$this->loadModel('MasterBeaconConfigs');
	$MasterBeaconConfigs = $this->MasterBeaconConfigs->find('all',
						[
							#'contain' => ['MasterBeaconInstallTypes'],
							'conditions' => ['uuid' => $data['code']]
							
						]
						)->first();
	#prd($MasterBeaconConfigs);
	if(!empty($MasterBeaconConfigs)){		
		$MasterBeaconConfigs = $MasterBeaconConfigs->toArray();
	
		$Device = [];
		$Device['url_path'] = $MasterBeaconConfigs['url_path'];

		$this->apiResponse['Data']['Device'] = $Device;	
	}else{
		$this->responseStatus = 'Fail';
		$this->apiResponse['Data']['message'] = 'Device not found';	
	}	

    }

    public function error()
    {
        $throwException = true;

        if ($throwException) {
            throw new NotFoundException();
        }

        // your other logic will be here
        // and finally set your response
        // $this->apiResponse['you_response'] = 'your response data';
    }

}