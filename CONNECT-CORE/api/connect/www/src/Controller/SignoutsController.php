<?php
namespace App\Controller;

use RestApi\Controller\ApiController;

class SignoutsController extends ApiController
{

    public function index()
	{

		$this->request->allowMethod('post');

		$this->httpStatusCode = 200;
		$this->apiResponse['you_response'] = 'your response data';
		
    }

    public function error()
    {
        $throwException = true;

        if ($throwException) {
            throw new NotFoundException();
        }

        // your other logic will be here
        // and finally set your response
        // $this->apiResponse['you_response'] = 'your response data';
    }

}