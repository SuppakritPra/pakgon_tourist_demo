<?php
namespace App\Controller;

use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
class CategoriesController extends ApiController
{

    public function index()
    {
        $this->request->allowMethod('post');
	
	$token = str_replace('Bearer ','',$this->request->getHeaderLine('Authorization'));#prd($token);
	#$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OTIzMCwidXNlcm5hbWUiOiJ0ZXN0dG9uZyIsImlwIjoiMTI3LjAuMC4xIn0.37hXRDEhiwjsgqR8mcgbhxnKtAof3tIDahL2P6DnQ4I";

	$user = JwtToken::decodeToken($token);#prd($user);
	#$user->user_id = 1;
	#$user->user_id = 59281;
	$user->user_id = 59286;
	$data = json_decode(file_get_contents("php://input"),true);#prr($data);
	
	$language = 'th';

	$this->loadModel('Categories');
	$Categories = $this->Categories->find('list',
						[
							'conditions' => ['user_id1' => $user->user_id],
							'keyField' => 'id',
							'valueField' => 'user_id2',
						]
						);
	if(!empty($Categories)) $Categories = $Categories->toArray();
	prd($Categories);

	$Profiles = [];
	if(!empty($Categories)){
	
		$this->loadModel('Users');
		$Users = $this->Users->find('all',['contain' => ['UserPersonals', 'UserProfiles']])->where(['id IN'  => $Categories]);					
		if(!empty($Users)) $Users = $Users->toArray();
		#prr($Users);
	
		$this->loadModel('MasterDepartments');
		$MasterDepartments = $this->MasterDepartments->find('list',
							[
								'keyField' => 'id',
								'valueField' => 'dept_name_'.$language,
								'order' => 'dept_name_'.$language.' asc'
							]
							);
		if(!empty($MasterDepartments)) $MasterDepartments = $MasterDepartments->toArray();
		#prr($MasterDepartments);	
	
		#$Profiles = [];
		foreach($Users as $User){
		
			$UserPersonal = $User['user_personals'];
			$UserProfile = $User['user_profiles'];
		
			#prr($UserPersonal);
			#prd($UserProfile);
		
			$Profiles[$User['id']]['name'] = $UserPersonal[0]['firstname_th'] .' ' . $UserPersonal[0]['lastname_th'];
			$Profiles[$User['id']]['moblie_no'] = $UserPersonal[0]['moblie_no'];
			$Profiles[$User['id']]['department'] = !empty($MasterDepartments[$UserProfile[0]['dept_id']])? $MasterDepartments[$UserProfile[0]['dept_id']]:'';
			$Profiles[$User['id']]['img_path'] = $UserProfile[0]['img_path'];

		}
		#prd($Profiles);
	}
	
	$this->apiResponse['Data']['Profiles'] = $Profiles;
    }

   

}