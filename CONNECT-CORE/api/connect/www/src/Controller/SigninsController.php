<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Utility\Security;
use Cake\Mailer\Email;
use RestApi\Controller\ApiController;
//use RestApi\Utility\JwtToken;
use App\Utility\JwtToken;
use Cake\Auth\DefaultPasswordHasher;

class SigninsController extends ApiController {
//    public function index() {
//        $this->request->allowMethod('post');
//
//        $data = $this->request->data; #prd($data);
//        #--------------------------------------------------------------------	
//        $this->loadModel('Users');
//        $users = $this->Users->find('all', [
//                    'conditions' => ['username' => $data['username'], 'is_used' => true],
//                    'limit' => 1
//                        ]
//                )->first();
//        #prd($users);
//        #if(!empty($users)) $users = $users->toArray();
//        #--------------------------------------------------------------------
//        if (!empty($users)) {
//
//            $data['password'] = Security::hash($data['password'], 'sha256', true); #prr($data);
//            #$this->loadModel('Users');
//            $users = $this->Users->find('all', [
//                        'conditions' => ['username' => $data['username'], 'password' => $data['password'], 'is_used' => true],
//                        'limit' => 1
//                            ]
//                    )->first();
//            #prr($users);
//
//            if (!empty($users)) {
//
//                $users = $users->toArray();
//
//                $user_id = $users['id'];
//                $dataUsers = [];
//                #$dataUsers['id'] = $users['id']; 
//                $dataUsers['dynamic_key'] = 'dynamic_key';
//                $dataUsers['dynamic_key_expiry'] = date('Y-m-d', strtotime('+3 day'));
//                #$dataUsers['token'] = Security::hash($data['username'].date('Y-m-d h:i:s'), 'md5', true);
//                $dataUsers['token_expiry'] = date('Y-m-d', strtotime('+3 day'));
//                #prr($dataUsers);
//
//                $this->loadModel('Users');
//                #$users = $this->Users->newEntity();#prr($users);
//                $users = $this->Users->get($user_id);
//                $users = $this->Users->patchEntity($users, $dataUsers); #prr($users);
//                $this->Users->save($users);
//
//                #$topic = $users['username'].$dataUsers['token'];
//                $topic = $users['username'] . $users['token'];
//                $user = ['user_id' => $users['id'], 'username' => $users['username'], 'ip' => $data['ip'], 'topic' => $topic];
//                #$token = JwtToken::generateToken($user);
//
//                $this->apiResponse['token'] = JwtToken::generateToken($user);
//                $this->apiResponse['topic'] = $topic;
//                $this->apiResponse['message'] = 'Signin Success.';
//            } else {
//                $this->responseStatus = 'Fail';
//                $this->apiResponse['type'] = 'error';
//                $this->apiResponse['message'] = 'Password Incorrect.';
//            }
//        } else {
//            $this->responseStatus = 'Fail';
//            $this->apiResponse['type'] = 'error';
//            $this->apiResponse['message'] = 'User Incorrect.';
//        }
//    }

    /**
     * 
     * Function Connect user Authentication and verifire
     * @author sarawutt.b
     * @return string json
     */
    public function index() {
        $this->request->allowMethod('post');
        $data = $this->request->getData();

        if (!array_key_exists('username', $data) || empty($data['username'])) {
            $this->responseStatus = 'Fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Please check username must be not empty');
        }

        if (!array_key_exists('password', $data) || empty($data['password'])) {
            $this->responseStatus = 'Fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Please check password must be not empty');
        }

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'userModel' => 'Users',
                    'finder' => 'auth'
                ]
            ],
            'authorize' => ['Controller'],
            'unauthorizedRedirect' => $this->referer()// If unauthorized, return them to page they were just on
        ]);


        $user = $this->Auth->identify();
        if (!empty($user)) {
            $user_id = $user['id'];
            $dataUsers = [];
            $dataUsers['dynamic_key'] = 'dynamic_key';
            $dataUsers['dynamic_key_expiry'] = date('Y-m-d', strtotime('+3 day'));
            $dataUsers['token_expiry'] = date('Y-m-d', strtotime('+3 day'));

            $this->loadModel('Users');
            $user = $this->Users->get($user_id);
            $user = $this->Users->patchEntity($user, $dataUsers); #prr($user);
            $this->Users->save($user);

            $topic = $user['username'] . $user['token'];
            $user = ['user_id' => $user['id'], 'username' => $user['username'], 'ip' => $data['ip'], 'topic' => $topic];

            $this->apiResponse['token'] = JwtToken::generateToken($user);
            $this->apiResponse['topic'] = $topic;
            $this->apiResponse['user'] = $user;
            $this->apiResponse['message'] = 'Signin Success.';
        } else {
            $this->responseStatus = 'Fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Username / Password Incorrect.');
        }
    }

    public function verify() {
        $this->request->allowMethod('post');
        $data = $this->request->getData();
        $user = ['user_id' => 1, 'ip' => $data['ip']];
        $this->apiResponse['token'] = JwtToken::generateToken($user);
        $this->apiResponse['message'] = 'Logged in successfully.';
    }

    public function notification() {
        $from_email = ['kmitcom@gmail.com' => 'Tong'];
        $to_emails = ['chaowarin@pakgon.com', 'kmitcom@gmail.com'];

        $email = new Email();
        $email->transport('gmail');

        try {
            $email->template('notification', 'connect');
            $email->from($from_email);
            $email->to($to_emails);
            $email->subject('Pin Code for Activate Connect');
            $email->emailFormat('html');
            $email->viewVars(compact('pin'));
            #$email->send($message);
            $email->send();
        } catch (Exception $e) {
            echo 'Exception : ', $e->getMessage(), "\n";
        }

        $this->httpStatusCode = 200;
        $this->apiResponse['message'] = 'Send Pin Completed!!';
    }

    public function error() {
        $throwException = true;

        if ($throwException) {
            throw new NotFoundException();
        }

        // your other logic will be here
        // and finally set your response
        // $this->apiResponse['you_response'] = 'your response data';
    }

    /**
     * 
     * Function build 
     * @param type $msg
     */
    protected function buildError($msg = '') {
        $this->responseStatus = 'Fail';
        $this->apiResponse['type'] = 'error';
        $this->apiResponse['message'] = $msg;
    }

    /**
     * 
     * Function mobile re-authorized use automatically by mobile application
     * @author sarawutt.b
     */
    public function reautorize() {
        $this->request->allowMethod('post');
        $token = str_replace('Bearer ', '', $this->request->getHeaderLine('Authorization'));
        $data = json_decode(file_get_contents("php://input"), true);
        $user = JwtToken::decodeToken($token);
        $data = $this->request->getData();
        $this->loadModel('Users');
        $users = $this->Users->get($user->user_id);
        if (!empty($users)) {
            $users = $users->toArray();
            $this->httpStatusCode = 200;
            $this->apiResponse['type'] = 'success';
            $this->apiResponse['user'] = $user;
            $this->apiResponse['message'] = __('OK Authorized');
        } else {
            $this->httpStatusCode = 403;
            $this->responseStatus = 'Fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Error Unauthorized');
        }
    }

}
