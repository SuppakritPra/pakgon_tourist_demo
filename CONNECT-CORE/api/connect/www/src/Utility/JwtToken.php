<?php

namespace App\Utility;

use Cake\Core\Configure;
use Firebase\JWT\JWT;

/**
 * Duplicate from Multidots Library
 * and Use the same name to avoid error when replace the old codes in controllers
 * JWT token utility class.
 */
class JwtToken
{

    /**
     * Generates a token based on payload
     *
     * @param mixed $payload Payload data to generate token
     * @return string|bool Token or false
     */
    public static function generateToken($payload = null)
    {
        if (empty($payload)) {
            return false;
        }

        $token = JWT::encode($payload, Configure::read('ApiRequest.jwtAuth.cypherKey'), Configure::read('ApiRequest.jwtAuth.tokenAlgorithm'));

        return $token;
    }


    public static function decodeToken($token = null)
    {

        if (empty($token)) {
            return false;
        }

        $payload = JWT::decode($token, Configure::read('ApiRequest.jwtAuth.cypherKey'), [Configure::read('ApiRequest.jwtAuth.tokenAlgorithm')]);

        return $payload;

	}
}