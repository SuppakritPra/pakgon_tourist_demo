# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2017-11-16 09:48+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/ProfilesController.php:115
msgid "Signout"
msgstr ""

#: Controller/SignupsController.php:158
msgid "User Duplicate"
msgstr ""

#: Template/Layout/error.ctp:35
#: Template/Error/error400.ctp:36
#: Template/Error/error500.ctp:41
msgid "Error"
msgstr ""

#: Template/Layout/error.ctp:43
msgid "Back"
msgstr ""

#: Template/UserPersonals/add.ctp:9
#: Template/UserPersonals/edit.ctp:9
#: Template/UserPersonals/index.ctp:9;39
#: Template/UserPersonals/view.ctp:9
#: Template/UserProfiles/add.ctp:9
#: Template/UserProfiles/edit.ctp:9
#: Template/UserProfiles/index.ctp:9;38
#: Template/UserProfiles/view.ctp:9
#: Template/Users/add.ctp:9
#: Template/Users/edit.ctp:9
#: Template/Users/index.ctp:9;38
#: Template/Users/view.ctp:9;97;145;201
msgid "Actions"
msgstr ""

#: Template/UserPersonals/add.ctp:10
#: Template/UserPersonals/edit.ctp:16
#: Template/UserPersonals/view.ctp:12
#: Template/Users/add.ctp:13
#: Template/Users/edit.ctp:19
#: Template/Users/index.ctp:13
#: Template/Users/view.ctp:16
msgid "List User Personals"
msgstr ""

#: Template/UserPersonals/add.ctp:11
#: Template/UserPersonals/edit.ctp:17
#: Template/UserPersonals/index.ctp:11
#: Template/UserPersonals/view.ctp:14
#: Template/UserProfiles/add.ctp:11
#: Template/UserProfiles/edit.ctp:17
#: Template/UserProfiles/index.ctp:11
#: Template/UserProfiles/view.ctp:14
#: Template/Users/add.ctp:10
#: Template/Users/edit.ctp:16
#: Template/Users/view.ctp:12
msgid "List Users"
msgstr ""

#: Template/UserPersonals/add.ctp:12
#: Template/UserPersonals/edit.ctp:18
#: Template/UserPersonals/index.ctp:12
#: Template/UserPersonals/view.ctp:15
#: Template/UserProfiles/add.ctp:12
#: Template/UserProfiles/edit.ctp:18
#: Template/UserProfiles/index.ctp:12
#: Template/UserProfiles/view.ctp:15
#: Template/Users/index.ctp:10
#: Template/Users/view.ctp:13
msgid "New User"
msgstr ""

#: Template/UserPersonals/add.ctp:18
msgid "Add User Personal"
msgstr ""

#: Template/UserPersonals/add.ctp:38
#: Template/UserPersonals/edit.ctp:44
#: Template/UserProfiles/add.ctp:37
#: Template/UserProfiles/edit.ctp:43
#: Template/Users/add.ctp:37
#: Template/Users/edit.ctp:43
#: View/Helper/FormHelper.php:1919
msgid "Submit"
msgstr ""

#: Template/UserPersonals/edit.ctp:11
#: Template/UserPersonals/index.ctp:67
#: Template/UserProfiles/edit.ctp:11
#: Template/UserProfiles/index.ctp:65
#: Template/Users/edit.ctp:11
#: Template/Users/index.ctp:61
#: Template/Users/view.ctp:114;171;226
msgid "Delete"
msgstr ""

#: Template/UserPersonals/edit.ctp:13
#: Template/UserPersonals/index.ctp:67
#: Template/UserPersonals/view.ctp:11
#: Template/UserProfiles/edit.ctp:13
#: Template/UserProfiles/index.ctp:65
#: Template/UserProfiles/view.ctp:11
#: Template/Users/edit.ctp:13
#: Template/Users/index.ctp:61
#: Template/Users/view.ctp:11;114;171;226
msgid "Are you sure you want to delete # {0}?"
msgstr ""

#: Template/UserPersonals/edit.ctp:24
#: Template/UserPersonals/view.ctp:10
msgid "Edit User Personal"
msgstr ""

#: Template/UserPersonals/index.ctp:10
#: Template/UserPersonals/view.ctp:13
#: Template/Users/add.ctp:14
#: Template/Users/edit.ctp:20
#: Template/Users/index.ctp:14
#: Template/Users/view.ctp:17
msgid "New User Personal"
msgstr ""

#: Template/UserPersonals/index.ctp:16
msgid "User Personals"
msgstr ""

#: Template/UserPersonals/index.ctp:65
#: Template/UserProfiles/index.ctp:63
#: Template/Users/index.ctp:59
#: Template/Users/view.ctp:112;169;224
#: View/Helper/PaginatorHelper.php:1214
msgid "View"
msgstr ""

#: Template/UserPersonals/index.ctp:66
#: Template/UserProfiles/index.ctp:64
#: Template/Users/index.ctp:60
#: Template/Users/view.ctp:113;170;225
msgid "Edit"
msgstr ""

#: Template/UserPersonals/index.ctp:75
#: Template/UserProfiles/index.ctp:73
#: Template/Users/index.ctp:69
msgid "first"
msgstr ""

#: Template/UserPersonals/index.ctp:76
#: Template/UserProfiles/index.ctp:74
#: Template/Users/index.ctp:70
msgid "previous"
msgstr ""

#: Template/UserPersonals/index.ctp:78
#: Template/UserProfiles/index.ctp:76
#: Template/Users/index.ctp:72
msgid "next"
msgstr ""

#: Template/UserPersonals/index.ctp:79
#: Template/UserProfiles/index.ctp:77
#: Template/Users/index.ctp:73
msgid "last"
msgstr ""

#: Template/UserPersonals/index.ctp:81
#: Template/UserProfiles/index.ctp:79
#: Template/Users/index.ctp:75
msgid "Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total"
msgstr ""

#: Template/UserPersonals/view.ctp:11
msgid "Delete User Personal"
msgstr ""

#: Template/UserPersonals/view.ctp:22
#: Template/Users/view.ctp:127
msgid "Person Card No"
msgstr ""

#: Template/UserPersonals/view.ctp:26
#: Template/Users/view.ctp:129
msgid "Firstname Th"
msgstr ""

#: Template/UserPersonals/view.ctp:30
#: Template/Users/view.ctp:130
msgid "Lastname Th"
msgstr ""

#: Template/UserPersonals/view.ctp:34
#: Template/Users/view.ctp:131
msgid "Firstname En"
msgstr ""

#: Template/UserPersonals/view.ctp:38
#: Template/Users/view.ctp:132
msgid "Lastname En"
msgstr ""

#: Template/UserPersonals/view.ctp:42
#: Template/Users/view.ctp:133
msgid "Gender"
msgstr ""

#: Template/UserPersonals/view.ctp:46
#: Template/Users/view.ctp:135
msgid "Moblie No"
msgstr ""

#: Template/UserPersonals/view.ctp:50
#: Template/Users/view.ctp:136
msgid "Phone No"
msgstr ""

#: Template/UserPersonals/view.ctp:54
#: Template/Users/view.ctp:137
msgid "Email"
msgstr ""

#: Template/UserPersonals/view.ctp:58
#: Template/UserProfiles/view.ctp:22
msgid "User"
msgstr ""

#: Template/UserPersonals/view.ctp:62
#: Template/Users/view.ctp:143
msgid "Blood Group"
msgstr ""

#: Template/UserPersonals/view.ctp:66
#: Template/UserProfiles/view.ctp:34
#: Template/Users/view.ctp:144;199
msgid "Address"
msgstr ""

#: Template/UserPersonals/view.ctp:70
#: Template/UserProfiles/view.ctp:38
#: Template/Users/view.ctp:46;87;126;183
msgid "Id"
msgstr ""

#: Template/UserPersonals/view.ctp:74
#: Template/Users/view.ctp:128
msgid "Master Prefix Id"
msgstr ""

#: Template/UserPersonals/view.ctp:78
#: Template/UserProfiles/view.ctp:58
#: Template/Users/view.ctp:54;93;138;192
msgid "Created By"
msgstr ""

#: Template/UserPersonals/view.ctp:82
#: Template/UserProfiles/view.ctp:62
#: Template/Users/view.ctp:58;95;140;194
msgid "Modified By"
msgstr ""

#: Template/UserPersonals/view.ctp:86
#: Template/Users/view.ctp:134
msgid "Birthdate"
msgstr ""

#: Template/UserPersonals/view.ctp:90
#: Template/UserProfiles/view.ctp:78
#: Template/Users/view.ctp:70;94;139;193
msgid "Created"
msgstr ""

#: Template/UserPersonals/view.ctp:94
#: Template/UserProfiles/view.ctp:82
#: Template/Users/view.ctp:74;96;141;195
msgid "Modified"
msgstr ""

#: Template/UserProfiles/add.ctp:10
#: Template/UserProfiles/edit.ctp:16
#: Template/UserProfiles/view.ctp:12
#: Template/Users/add.ctp:15
#: Template/Users/edit.ctp:21
#: Template/Users/index.ctp:15
#: Template/Users/view.ctp:18
msgid "List User Profiles"
msgstr ""

#: Template/UserProfiles/add.ctp:18
msgid "Add User Profile"
msgstr ""

#: Template/UserProfiles/edit.ctp:24
#: Template/UserProfiles/view.ctp:10
msgid "Edit User Profile"
msgstr ""

#: Template/UserProfiles/index.ctp:10
#: Template/UserProfiles/view.ctp:13
#: Template/Users/add.ctp:16
#: Template/Users/edit.ctp:22
#: Template/Users/index.ctp:16
#: Template/Users/view.ctp:19
msgid "New User Profile"
msgstr ""

#: Template/UserProfiles/index.ctp:16
msgid "User Profiles"
msgstr ""

#: Template/UserProfiles/view.ctp:11
msgid "Delete User Profile"
msgstr ""

#: Template/UserProfiles/view.ctp:26
#: Template/Users/view.ctp:188
msgid "Card Code"
msgstr ""

#: Template/UserProfiles/view.ctp:30
#: Template/Users/view.ctp:190
msgid "Img Path"
msgstr ""

#: Template/UserProfiles/view.ctp:42
#: Template/Users/view.ctp:185
msgid "Organize Id"
msgstr ""

#: Template/UserProfiles/view.ctp:46
#: Template/Users/view.ctp:186
msgid "Dept Id"
msgstr ""

#: Template/UserProfiles/view.ctp:50
#: Template/Users/view.ctp:187
msgid "Section Id"
msgstr ""

#: Template/UserProfiles/view.ctp:54
#: Template/Users/view.ctp:189
msgid "User Type Id"
msgstr ""

#: Template/UserProfiles/view.ctp:66
#: Template/Users/view.ctp:196
msgid "Position Org"
msgstr ""

#: Template/UserProfiles/view.ctp:70
#: Template/Users/view.ctp:197
msgid "Position Edu"
msgstr ""

#: Template/UserProfiles/view.ctp:74
#: Template/Users/view.ctp:200
msgid "Master Bussiness Type Id"
msgstr ""

#: Template/UserProfiles/view.ctp:86
#: Template/Users/view.ctp:198
msgid "Enter Date"
msgstr ""

#: Template/UserProfiles/view.ctp:90
#: Template/Users/view.ctp:78;92;191
msgid "Is Used"
msgstr ""

#: Template/UserProfiles/view.ctp:91
#: Template/Users/view.ctp:79
msgid "Yes"
msgstr ""

#: Template/UserProfiles/view.ctp:91
#: Template/Users/view.ctp:79
msgid "No"
msgstr ""

#: Template/Users/add.ctp:11
#: Template/Users/edit.ctp:17
#: Template/Users/index.ctp:11
#: Template/Users/view.ctp:14
msgid "List User Homeplaces"
msgstr ""

#: Template/Users/add.ctp:12
#: Template/Users/edit.ctp:18
#: Template/Users/index.ctp:12
#: Template/Users/view.ctp:15
msgid "New User Homeplace"
msgstr ""

#: Template/Users/add.ctp:22
msgid "Add User"
msgstr ""

#: Template/Users/edit.ctp:28
#: Template/Users/view.ctp:10
msgid "Edit User"
msgstr ""

#: Template/Users/index.ctp:20
msgid "Users"
msgstr ""

#: Template/Users/view.ctp:11
msgid "Delete User"
msgstr ""

#: Template/Users/view.ctp:26
msgid "Username"
msgstr ""

#: Template/Users/view.ctp:30
msgid "Password"
msgstr ""

#: Template/Users/view.ctp:34
msgid "Dynamic Key"
msgstr ""

#: Template/Users/view.ctp:38
msgid "Token"
msgstr ""

#: Template/Users/view.ctp:42
msgid "Pin Code"
msgstr ""

#: Template/Users/view.ctp:50
msgid "Point"
msgstr ""

#: Template/Users/view.ctp:62
msgid "Dynamic Key Expiry"
msgstr ""

#: Template/Users/view.ctp:66
msgid "Token Expiry"
msgstr ""

#: Template/Users/view.ctp:83
msgid "Related User Homeplaces"
msgstr ""

#: Template/Users/view.ctp:88;142;184
msgid "User Id"
msgstr ""

#: Template/Users/view.ctp:89
msgid "Master Country Id"
msgstr ""

#: Template/Users/view.ctp:90
msgid "Master Province Id"
msgstr ""

#: Template/Users/view.ctp:91
msgid "Master District Id"
msgstr ""

#: Template/Users/view.ctp:122
msgid "Related User Personals"
msgstr ""

#: Template/Users/view.ctp:179
msgid "Related User Profiles"
msgstr ""

#: Template/Error/error400.ctp:37
msgid "The requested address {0} was not found on this server."
msgstr ""

#: Template/Error/error500.ctp:39
msgid "An Internal Error Has Occurred"
msgstr ""

#: Controller/Component/AuthComponent.php:496
msgid "You are not authorized to access that location."
msgstr ""

#: Controller/Component/CsrfComponent.php:157
#: Http/Middleware/CsrfProtectionMiddleware.php:190
msgid "Missing CSRF token cookie"
msgstr ""

#: Controller/Component/CsrfComponent.php:161
#: Http/Middleware/CsrfProtectionMiddleware.php:194
msgid "CSRF token mismatch."
msgstr ""

#: Error/ExceptionRenderer.php:258
msgid "Not Found"
msgstr ""

#: Error/ExceptionRenderer.php:260
msgid "An Internal Error Has Occurred."
msgstr ""

#: Http/Response.php:2400
msgid "The requested file contains `..` and will not be read."
msgstr ""

#: Http/Response.php:2411
msgid "The requested file was not found"
msgstr ""

#: I18n/Number.php:90
msgid "{0,number,#,###.##} KB"
msgstr ""

#: I18n/Number.php:92
msgid "{0,number,#,###.##} MB"
msgstr ""

#: I18n/Number.php:94
msgid "{0,number,#,###.##} GB"
msgstr ""

#: I18n/Number.php:96
msgid "{0,number,#,###.##} TB"
msgstr ""

#: I18n/Number.php:88
msgid "{0,number,integer} Byte"
msgid_plural "{0,number,integer} Bytes"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:80
msgid "{0} from now"
msgstr ""

#: I18n/RelativeTimeFormatter.php:80
msgid "{0} ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:83
msgid "{0} after"
msgstr ""

#: I18n/RelativeTimeFormatter.php:83
msgid "{0} before"
msgstr ""

#: I18n/RelativeTimeFormatter.php:114
msgid "just now"
msgstr ""

#: I18n/RelativeTimeFormatter.php:151
msgid "about a second ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:152
msgid "about a minute ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:153
msgid "about an hour ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:154;332
msgid "about a day ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:155;333
msgid "about a week ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:156;334
msgid "about a month ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:157;335
msgid "about a year ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:168
msgid "in about a second"
msgstr ""

#: I18n/RelativeTimeFormatter.php:169
msgid "in about a minute"
msgstr ""

#: I18n/RelativeTimeFormatter.php:170
msgid "in about an hour"
msgstr ""

#: I18n/RelativeTimeFormatter.php:171;346
msgid "in about a day"
msgstr ""

#: I18n/RelativeTimeFormatter.php:172;347
msgid "in about a week"
msgstr ""

#: I18n/RelativeTimeFormatter.php:173;348
msgid "in about a month"
msgstr ""

#: I18n/RelativeTimeFormatter.php:174;349
msgid "in about a year"
msgstr ""

#: I18n/RelativeTimeFormatter.php:304
msgid "today"
msgstr ""

#: I18n/RelativeTimeFormatter.php:370
msgid "%s ago"
msgstr ""

#: I18n/RelativeTimeFormatter.php:371
msgid "on %s"
msgstr ""

#: I18n/RelativeTimeFormatter.php:47;126;316
msgid "{0} year"
msgid_plural "{0} years"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:51;129;319
msgid "{0} month"
msgid_plural "{0} months"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:57;132;322
msgid "{0} week"
msgid_plural "{0} weeks"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:59;135;325
msgid "{0} day"
msgid_plural "{0} days"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:64;138
msgid "{0} hour"
msgid_plural "{0} hours"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:68;141
msgid "{0} minute"
msgid_plural "{0} minutes"
msgstr[0] ""
msgstr[1] ""

#: I18n/RelativeTimeFormatter.php:72;144
msgid "{0} second"
msgid_plural "{0} seconds"
msgstr[0] ""
msgstr[1] ""

#: ORM/RulesChecker.php:57
msgid "This value is already in use"
msgstr ""

#: ORM/RulesChecker.php:104
msgid "This value does not exist"
msgstr ""

#: ORM/RulesChecker.php:128
msgid "The count does not match {0}{1}"
msgstr ""

#: Utility/Text.php:902
msgid "and"
msgstr ""

#: Validation/Validator.php:111
msgid "This field is required"
msgstr ""

#: Validation/Validator.php:112
msgid "This field cannot be left empty"
msgstr ""

#: Validation/Validator.php:1885
msgid "The provided value is invalid"
msgstr ""

#: View/Helper/FormHelper.php:1039
msgid "New %s"
msgstr ""

#: View/Helper/FormHelper.php:1042
msgid "Edit %s"
msgstr ""

#: View/Helper/HtmlHelper.php:795
msgid "Home"
msgstr ""

#: View/Widget/DateTimeWidget.php:540
msgid "January"
msgstr ""

#: View/Widget/DateTimeWidget.php:541
msgid "February"
msgstr ""

#: View/Widget/DateTimeWidget.php:542
msgid "March"
msgstr ""

#: View/Widget/DateTimeWidget.php:543
msgid "April"
msgstr ""

#: View/Widget/DateTimeWidget.php:544
msgid "May"
msgstr ""

#: View/Widget/DateTimeWidget.php:545
msgid "June"
msgstr ""

#: View/Widget/DateTimeWidget.php:546
msgid "July"
msgstr ""

#: View/Widget/DateTimeWidget.php:547
msgid "August"
msgstr ""

#: View/Widget/DateTimeWidget.php:548
msgid "September"
msgstr ""

#: View/Widget/DateTimeWidget.php:549
msgid "October"
msgstr ""

#: View/Widget/DateTimeWidget.php:550
msgid "November"
msgstr ""

#: View/Widget/DateTimeWidget.php:551
msgid "December"
msgstr ""

