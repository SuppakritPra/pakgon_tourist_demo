<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\PakgonFCMPusherComponent;

/**
 * PakgonFCM shell command.
 * @author sarawutt.b
 */
class PakgonFCMShell extends Shell {

    public function initialize() {
        parent::initialize();
        $this->PakgonFCMPusher = new PakgonFCMPusherComponent(new ComponentRegistry(), []);
    }

    /**
     * main() method. use for default function
     * @author sarawutt.b
     * @return bool|int|null Success or error code.
     */
    public function main() {
        $this->BeaconPushNotification();
    }

    /**
     * 
     * Function loop for beacon to FCM prepare to push
     * @author sarawutt.b
     * @return void
     */
    public function BeaconPushNotification() {
        $this->out('Pakgon Beacon Push Notification');
        $this->loadModel('MasterBeaconDetects');
        //$masterBeaconDetects = $this->MasterBeaconDetects->find()->select(['token', 'beacon_value', 'topic', 'user_id', 'remote_address', 'created::DATE created'])->where(['status' => 'N'])->distinct()->order(['created::DATE' => 'asc'])->all()->toArray();
        $masterBeaconDetects = $this->MasterBeaconDetects->scanBeacon();
        foreach ($masterBeaconDetects as $masterBeaconDetect) {
            $push = [
                'title' => 'Pakgon Smart tourism',
                'message' => 'Push test Smart tourism',
                'badge' => 1,
                'topic' => '/topics/' . $masterBeaconDetect['topic']
            ];

            $customDatas = ['open_url' => 'http://commu-uat.connect.pakgon.com/feed/view/18'];
            debug($this->PakgonFCMPusher->push($push));
            //$this->PakgonFCMPusher->push($push, $customDatas);
            $this->updateBeaconPushToFCM($masterBeaconDetect);
        }
    }

    /**
     * 
     * Function update detect beacon user status / issue_date
     * @author sarawutt.b
     * @param  $masterBeaconDetect as a masterBeaconDetect
     * @return void
     */
    public function updateBeaconPushToFCM($masterBeaconDetect) {
        //$masterBeaconDetect->issue_date = date('Y-m-d H:i:s');
        //$masterBeaconDetect->status = 'A';
        //return $this->MasterBeaconDetects->save($masterBeaconDetect);
//        return $this->MasterBeaconDetects->updateAll([
//                    'status' => 'A',
//                    'issue_date' => date('Y-m-d H:i:s')
//                        ], ['token'=>$masterBeaconDetect->token,
//                            'beacon_value'=>$masterBeaconDetect->beacon_value,
//                            'topic'=>$masterBeaconDetect->topic,
//                            'created::DATE'=>$masterBeaconDetect->created
//        ]);
        return $this->MasterBeaconDetects->updateBeconAlreadyPushNotification($masterBeaconDetect);
    }

}
