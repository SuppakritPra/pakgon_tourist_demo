<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserHomeplaces Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\MasterCountriesTable|\Cake\ORM\Association\BelongsTo $MasterCountries
 * @property \App\Model\Table\MasterProvincesTable|\Cake\ORM\Association\BelongsTo $MasterProvinces
 * @property \App\Model\Table\MasterDistrictsTable|\Cake\ORM\Association\BelongsTo $MasterDistricts
 *
 * @method \App\Model\Entity\UserHomeplace get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserHomeplace newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserHomeplace[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserHomeplace|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserHomeplace patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserHomeplace[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserHomeplace findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserHomeplacesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_homeplaces');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MasterCountries', [
            'foreignKey' => 'master_country_id'
        ]);
        $this->belongsTo('MasterProvinces', [
            'foreignKey' => 'master_province_id'
        ]);
        $this->belongsTo('MasterDistricts', [
            'foreignKey' => 'master_district_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('is_used')
            ->allowEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['master_country_id'], 'MasterCountries'));
        $rules->add($rules->existsIn(['master_province_id'], 'MasterProvinces'));
        $rules->add($rules->existsIn(['master_district_id'], 'MasterDistricts'));

        return $rules;
    }
}
