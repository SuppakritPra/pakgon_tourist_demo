<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * MasterBeaconDetects Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\MasterBeaconDetect get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterBeaconDetect newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterBeaconDetect[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconDetect|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterBeaconDetect patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconDetect[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconDetect findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterBeaconDetectsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('master_beacon_detects');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * 
     * Function scan all beacon group by current date
     * @author sarawutt.b
     * @return array()
     */
    public function scanBeacon() {
        //$sql = "SELECT DISTINCT token, beacon_value, topic, user_id, remote_address, created::date FROM master.master_beacon_detects WHERE status = 'N' ORDER BY created ASC;";
        $sql = "SELECT DISTINCT token, beacon_value, topic, user_id, remote_address, date_trunc('minute',created)::TIMESTAMP AS created FROM master.master_beacon_detects WHERE status = 'N' AND date_trunc('minute',created)::TIMESTAMP < date_trunc('minute',(now() - interval '3 min'))::TIMESTAMP ORDER BY created ASC;";
        $results = ConnectionManager::get('db_master')->execute($sql)->fetchAll('assoc');
        return empty($results) ? [] : $results;
    }

    /**
     * 
     * Function update all beacon with already sent push notification
     * @author sarawutt.b
     * @param type $params as array of master_beacon_detect
     * @return boolean
     */
    public function updateBeconAlreadyPushNotification($params) {
        //$sql = "UPDATE master.master_beacon_detects SET status = 'A', issue_date = CURRENT_TIMESTAMP WHERE token = '{$params['token']}' AND beacon_value = '{$params['beacon_value']}' AND topic = '{$params['topic']}' AND created::DATE = '{$params['created']}'::DATE;";
        $sql = "UPDATE master.master_beacon_detects SET status = 'A', issue_date = CURRENT_TIMESTAMP WHERE token = '{$params['token']}' AND beacon_value = '{$params['beacon_value']}' AND topic = '{$params['topic']}' AND date_trunc('minute',created)::TIMESTAMP < date_trunc('minute',(now() - interval '3 min'))::TIMESTAMP;";
        $result = ConnectionManager::get('db_master')->execute($sql);
        return $result;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('appname')
                ->allowEmpty('appname');

        $validator
                ->scalar('token')
                ->requirePresence('token', 'create')
                ->notEmpty('token');

        $validator
                ->scalar('beacon_value')
                ->requirePresence('beacon_value', 'create')
                ->notEmpty('beacon_value');

        $validator
                ->scalar('remote_address')
                ->allowEmpty('remote_address');

        $validator
                ->scalar('status')
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->dateTime('issue_time')
                ->allowEmpty('issue_time');

        $validator
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName() {
        return 'db_master';
    }

}
