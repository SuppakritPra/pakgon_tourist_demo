<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterOrganizations Model
 *
 * @property \App\Model\Table\BussinessTypesTable|\Cake\ORM\Association\BelongsTo $BussinessTypes
 * @property \App\Model\Table\ArticleAccessesTable|\Cake\ORM\Association\HasMany $ArticleAccesses
 * @property \App\Model\Table\MasterDepartmentsTable|\Cake\ORM\Association\HasMany $MasterDepartments
 * @property \App\Model\Table\MasterOrganizationPositionsTable|\Cake\ORM\Association\HasMany $MasterOrganizationPositions
 *
 * @method \App\Model\Entity\MasterOrganization get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterOrganization newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterOrganization[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterOrganization|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterOrganization patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterOrganization[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterOrganization findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterOrganizationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_organizations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('BussinessTypes', [
            'foreignKey' => 'bussiness_type_id'
        ]);
        $this->hasMany('ArticleAccesses', [
            'foreignKey' => 'master_organization_id'
        ]);
        $this->hasMany('MasterDepartments', [
            'foreignKey' => 'master_organization_id'
        ]);
        $this->hasMany('MasterOrganizationPositions', [
            'foreignKey' => 'master_organization_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('org_name_th')
            ->allowEmpty('org_name_th');

        $validator
            ->scalar('org_name_en')
            ->allowEmpty('org_name_en');

        $validator
            ->scalar('org_abbr_th')
            ->allowEmpty('org_abbr_th');

        $validator
            ->scalar('org_abbr_en')
            ->allowEmpty('org_abbr_en');

        $validator
            ->integer('order_seq')
            ->allowEmpty('order_seq');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->integer('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['bussiness_type_id'], 'BussinessTypes'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
