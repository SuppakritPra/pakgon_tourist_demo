<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterBeaconConfigs Model
 *
 * @property \App\Model\Table\TravelLocationsTable|\Cake\ORM\Association\BelongsTo $TravelLocations
 * @property \App\Model\Table\BeaconInstallTypesTable|\Cake\ORM\Association\BelongsTo $BeaconInstallTypes
 *
 * @method \App\Model\Entity\MasterBeaconConfig get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterBeaconConfig newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterBeaconConfig[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconConfig|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterBeaconConfig patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconConfig[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconConfig findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterBeaconConfigsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('master_beacon_configs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TravelLocations', [
            'foreignKey' => 'travel_location_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('BeaconInstallTypes', [
            'foreignKey' => 'beacon_install_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
//       $validator
//            ->allowEmpty('id', 'create');
//
//        $validator
//            ->scalar('uuid')
//            ->requirePresence('uuid', 'create')
//            ->notEmpty('uuid');
//
//        $validator
//            ->scalar('beacon_desc')
//            ->requirePresence('beacon_desc', 'create')
//            ->notEmpty('beacon_desc');
//
//        $validator
//            ->scalar('url_path')
//            ->requirePresence('url_path', 'create')
//            ->notEmpty('url_path');
//
//        $validator
//            ->integer('created_by')
//            ->requirePresence('created_by', 'create')
//            ->notEmpty('created_by');
//
//        $validator
//            ->integer('modified_by')
//            ->allowEmpty('modified_by'); 

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['travel_location_id'], 'TravelLocations'));
        $rules->add($rules->existsIn(['beacon_install_type_id'], 'BeaconInstallTypes'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName() {
        return 'db_master';
    }

}
