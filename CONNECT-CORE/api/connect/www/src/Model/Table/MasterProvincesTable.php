<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterProvinces Model
 *
 * @property \App\Model\Table\MasterCountriesTable|\Cake\ORM\Association\BelongsTo $MasterCountries
 * @property \App\Model\Table\MasterDistrictsTable|\Cake\ORM\Association\HasMany $MasterDistricts
 * @property |\Cake\ORM\Association\HasMany $UserHomeplaces
 *
 * @method \App\Model\Entity\MasterProvince get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterProvince newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterProvince[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterProvince|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterProvince patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterProvince[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterProvince findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterProvincesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_provinces');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterCountries', [
            'foreignKey' => 'master_country_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('MasterDistricts', [
            'foreignKey' => 'master_province_id'
        ]);
        $this->hasMany('UserHomeplaces', [
            'foreignKey' => 'master_province_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('province_code')
            ->allowEmpty('province_code');

        $validator
            ->scalar('province_name_th')
            ->allowEmpty('province_name_th');

        $validator
            ->scalar('province_name_en')
            ->allowEmpty('province_name_en');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['master_country_id'], 'MasterCountries'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
