<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserPersonals Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $MasterPrefixes
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserPersonal get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserPersonal newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserPersonal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserPersonal|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserPersonal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserPersonal[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserPersonal findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserPersonalsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_personals');
        $this->setDisplayField('user_id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
		
		/*
        $this->belongsTo('MasterPrefixes', [
            'foreignKey' => 'master_prefix_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
		*/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('person_card_no')
            ->allowEmpty('person_card_no');

        $validator
            ->scalar('firstname_th')
            ->allowEmpty('firstname_th');

        $validator
            ->scalar('lastname_th')
            ->allowEmpty('lastname_th');

        $validator
            ->scalar('firstname_en')
            ->allowEmpty('firstname_en');

        $validator
            ->scalar('lastname_en')
            ->allowEmpty('lastname_en');

        $validator
            ->scalar('gender')
            ->allowEmpty('gender');

        $validator
            ->date('birthdate')
            ->allowEmpty('birthdate');

        $validator
            ->scalar('moblie_no')
            ->allowEmpty('moblie_no');

        $validator
            ->scalar('phone_no')
            ->allowEmpty('phone_no');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        $validator
            ->scalar('blood_group')
            ->allowEmpty('blood_group');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
		/*
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['master_prefix_id'], 'MasterPrefixes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
		*/
        return $rules;
    }
}
