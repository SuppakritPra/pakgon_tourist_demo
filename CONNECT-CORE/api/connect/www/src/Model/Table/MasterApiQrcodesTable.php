<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterApiQrcodes Model
 *
 * @property \App\Model\Table\MasterCategoriesTable|\Cake\ORM\Association\BelongsTo $MasterCategories
 *
 * @method \App\Model\Entity\MasterApiQrcode get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterApiQrcode newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterApiQrcode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterApiQrcode|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterApiQrcode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterApiQrcode[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterApiQrcode findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterApiQrcodesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_api_qrcodes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterCategories', [
            'foreignKey' => 'master_category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('api_qrcode_name')
            ->allowEmpty('api_qrcode_name');

        $validator
            ->scalar('url_path')
            ->requirePresence('url_path', 'create')
            ->notEmpty('url_path');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        $validator
            ->scalar('api_type')
            ->allowEmpty('api_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['master_category_id'], 'MasterCategories'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
