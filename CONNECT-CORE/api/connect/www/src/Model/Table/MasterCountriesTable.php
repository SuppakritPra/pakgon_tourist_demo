<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterCountries Model
 *
 * @property |\Cake\ORM\Association\HasMany $MasterProvinces
 * @property |\Cake\ORM\Association\HasMany $UserHomeplaces
 *
 * @method \App\Model\Entity\MasterCountry get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterCountry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterCountry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterCountry|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterCountry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterCountry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterCountry findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterCountriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_countries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('MasterProvinces', [
            'foreignKey' => 'master_country_id'
        ]);
        $this->hasMany('UserHomeplaces', [
            'foreignKey' => 'master_country_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('country_code')
            ->allowEmpty('country_code');

        $validator
            ->scalar('country_name_th')
            ->allowEmpty('country_name_th');

        $validator
            ->scalar('country_name_en')
            ->allowEmpty('country_name_en');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
