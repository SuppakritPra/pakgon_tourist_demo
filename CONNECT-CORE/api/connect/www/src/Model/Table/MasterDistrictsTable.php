<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterDistricts Model
 *
 * @property \App\Model\Table\MasterProvincesTable|\Cake\ORM\Association\BelongsTo $MasterProvinces
 * @property |\Cake\ORM\Association\HasMany $UserHomeplaces
 *
 * @method \App\Model\Entity\MasterDistrict get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterDistrict newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterDistrict[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterDistrict|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterDistrict patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterDistrict[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterDistrict findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterDistrictsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_districts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterProvinces', [
            'foreignKey' => 'master_province_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UserHomeplaces', [
            'foreignKey' => 'master_district_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('district_code')
            ->allowEmpty('district_code');

        $validator
            ->scalar('district_name_th')
            ->allowEmpty('district_name_th');

        $validator
            ->scalar('district_name_en')
            ->allowEmpty('district_name_en');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['master_province_id'], 'MasterProvinces'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
