<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterBeacons Model
 *
 * @property \App\Model\Table\MasterBeaconInstallTypesTable|\Cake\ORM\Association\BelongsTo $MasterBeaconInstallTypes
 * @property \App\Model\Table\MasterBeaconInstallationsTable|\Cake\ORM\Association\HasMany $MasterBeaconInstallations
 *
 * @method \App\Model\Entity\MasterBeacon get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterBeacon newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterBeacon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeacon|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterBeacon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeacon[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeacon findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterBeaconsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_beacons');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterBeaconInstallTypes', [
            'foreignKey' => 'master_beacon_install_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('MasterBeaconInstallations', [
            'foreignKey' => 'master_beacon_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
//        $validator
//            ->allowEmpty('id', 'create');
//
//        $validator
//            ->scalar('beacon_code')
//            ->requirePresence('beacon_code', 'create')
//            ->notEmpty('beacon_code');
//
//        $validator
//            ->scalar('beacon_name')
//            ->allowEmpty('beacon_name');
//
//        $validator
//            ->scalar('beacon_value')
//            ->requirePresence('beacon_value', 'create')
//            ->notEmpty('beacon_value');
//
//        $validator
//            ->boolean('is_used')
//            ->requirePresence('is_used', 'create')
//            ->notEmpty('is_used');
//
//        $validator
//            ->requirePresence('create_uid', 'create')
//            ->notEmpty('create_uid');
//
//        $validator
//            ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['master_beacon_install_type_id'], 'MasterBeaconInstallTypes'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'db_master';
    }
}
