<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterBeaconInstallations Model
 *
 * @property \App\Model\Table\MasterLocationAreasTable|\Cake\ORM\Association\BelongsTo $MasterLocationAreas
 * @property \App\Model\Table\MasterBeaconsTable|\Cake\ORM\Association\BelongsTo $MasterBeacons
 *
 * @method \App\Model\Entity\MasterBeaconInstallation get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterBeaconInstallation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterBeaconInstallation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconInstallation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterBeaconInstallation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconInstallation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterBeaconInstallation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterBeaconInstallationsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('master_beacon_installations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MasterLocationAreas', [
            'foreignKey' => 'master_location_area_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MasterBeacons', [
            'foreignKey' => 'master_beacon_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
//        $validator
//                ->allowEmpty('id', 'create');
//
//        $validator
//                ->scalar('remark')
//                ->allowEmpty('remark');
//
//        $validator
//                ->boolean('is_used')
//                ->requirePresence('is_used', 'create')
//                ->notEmpty('is_used');
//
//        $validator
//                ->requirePresence('create_uid', 'create')
//                ->notEmpty('create_uid');
//
//        $validator
//                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['master_location_area_id'], 'MasterLocationAreas'));
        $rules->add($rules->existsIn(['master_beacon_id'], 'MasterBeacons'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName() {
        return 'db_master';
    }

}
