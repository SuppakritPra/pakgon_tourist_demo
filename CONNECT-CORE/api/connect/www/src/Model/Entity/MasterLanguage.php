<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterLanguage Entity
 *
 * @property int $id
 * @property string $language_display
 * @property string $language_th
 * @property string $language_en
 * @property string $language_abbr
 * @property int $seq_no
 * @property bool $is_used
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $modified_by
 *
 * @property \App\Model\Entity\UserDefaultLanguage[] $user_default_languages
 */
class MasterLanguage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'language_display' => true,
        'language_th' => true,
        'language_en' => true,
        'language_abbr' => true,
        'seq_no' => true,
        'is_used' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'user_default_languages' => true
    ];
}
