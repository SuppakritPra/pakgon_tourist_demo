<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterOrganization Entity
 *
 * @property int $id
 * @property string $org_name_th
 * @property string $org_name_en
 * @property string $org_abbr_th
 * @property string $org_abbr_en
 * @property int $order_seq
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $bussiness_type_id
 *
 * @property \App\Model\Entity\BussinessType $bussiness_type
 * @property \App\Model\Entity\ArticleAccess[] $article_accesses
 * @property \App\Model\Entity\MasterDepartment[] $master_departments
 * @property \App\Model\Entity\MasterOrganizationPosition[] $master_organization_positions
 */
class MasterOrganization extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'org_name_th' => true,
        'org_name_en' => true,
        'org_abbr_th' => true,
        'org_abbr_en' => true,
        'order_seq' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'bussiness_type_id' => true,
        'bussiness_type' => true,
        'article_accesses' => true,
        'master_departments' => true,
        'master_organization_positions' => true
    ];
}
