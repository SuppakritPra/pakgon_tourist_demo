<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserHomeplace Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $master_country_id
 * @property int $master_province_id
 * @property int $master_district_id
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\MasterCountry $master_country
 * @property \App\Model\Entity\MasterProvince $master_province
 * @property \App\Model\Entity\MasterDistrict $master_district
 */
class UserHomeplace extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'master_country_id' => true,
        'master_province_id' => true,
        'master_district_id' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'user' => true,
        'master_country' => true,
        'master_province' => true,
        'master_district' => true
    ];
}
