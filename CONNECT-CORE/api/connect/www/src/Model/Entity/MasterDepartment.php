<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterDepartment Entity
 *
 * @property int $id
 * @property int $master_organization_id
 * @property string $dept_name_th
 * @property string $dept_name_en
 * @property string $dept_abbr_th
 * @property string $dept_abbr_en
 * @property int $order_seq
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\MasterOrganization $master_organization
 * @property \App\Model\Entity\ArticleAccess[] $article_accesses
 * @property \App\Model\Entity\MasterOrganizationPosition[] $master_organization_positions
 * @property \App\Model\Entity\MasterSection[] $master_sections
 */
class MasterDepartment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_organization_id' => true,
        'dept_name_th' => true,
        'dept_name_en' => true,
        'dept_abbr_th' => true,
        'dept_abbr_en' => true,
        'order_seq' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'master_organization' => true,
        'article_accesses' => true,
        'master_organization_positions' => true,
        'master_sections' => true
    ];
}
