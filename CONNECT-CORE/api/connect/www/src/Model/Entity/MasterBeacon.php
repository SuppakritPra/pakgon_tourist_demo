<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterBeacon Entity
 *
 * @property int $id
 * @property int $master_beacon_install_type_id
 * @property string $beacon_code
 * @property string $beacon_name
 * @property string $beacon_value
 * @property bool $is_used
 * @property \Cake\I18n\FrozenTime $created
 * @property int $create_uid
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $update_uid
 *
 * @property \App\Model\Entity\MasterBeaconInstallType $master_beacon_install_type
 * @property \App\Model\Entity\MasterBeaconInstallation[] $master_beacon_installations
 */
class MasterBeacon extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_beacon_install_type_id' => true,
        'beacon_code' => true,
        'beacon_name' => true,
        'beacon_value' => true,
        'is_used' => true,
        'created' => true,
        'create_uid' => true,
        'modified' => true,
        'update_uid' => true,
        'master_beacon_install_type' => true,
        'master_beacon_installations' => true
    ];

}
