<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contract Entity
 *
 * @property int $id
 * @property int $user_id1
 * @property int $user_id2
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 */
class Contract extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id1' => true,
        'user_id2' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true
    ];
}
