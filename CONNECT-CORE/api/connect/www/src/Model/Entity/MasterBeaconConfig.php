<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterBeaconConfig Entity
 *
 * @property int $id
 * @property string $uuid
 * @property int $travel_location_id
 * @property string $beacon_desc
 * @property int $beacon_install_type_id
 * @property string $url_path
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\TravelLocation $travel_location
 * @property \App\Model\Entity\BeaconInstallType $beacon_install_type
 */
class MasterBeaconConfig extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'travel_location_id' => true,
        'beacon_desc' => true,
        'beacon_install_type_id' => true,
        'url_path' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'travel_location' => true,
        'beacon_install_type' => true
    ];

}
