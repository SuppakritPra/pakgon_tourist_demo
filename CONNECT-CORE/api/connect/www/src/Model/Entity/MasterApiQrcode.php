<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterApiQrcode Entity
 *
 * @property int $id
 * @property string $api_qrcode_name
 * @property int $master_category_id
 * @property string $url_path
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $api_type
 *
 * @property \App\Model\Entity\MasterCategory $master_category
 */
class MasterApiQrcode extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'api_qrcode_name' => true,
        'master_category_id' => true,
        'url_path' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'api_type' => true,
        'master_category' => true
    ];
}
