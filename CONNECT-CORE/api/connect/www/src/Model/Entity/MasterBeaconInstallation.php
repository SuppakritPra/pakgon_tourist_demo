<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterBeaconInstallation Entity
 *
 * @property int $id
 * @property int $master_location_area_id
 * @property int $master_beacon_id
 * @property string $remark
 * @property bool $is_used
 * @property \Cake\I18n\FrozenTime $created
 * @property int $create_uid
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $update_uid
 *
 * @property \App\Model\Entity\MasterLocationArea $master_location_area
 * @property \App\Model\Entity\MasterBeacon $master_beacon
 */
class MasterBeaconInstallation extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_location_area_id' => true,
        'master_beacon_id' => true,
        'remark' => true,
        'is_used' => true,
        'created' => true,
        'create_uid' => true,
        'modified' => true,
        'update_uid' => true,
        'master_location_area' => true,
        'master_beacon' => true
    ];

}
