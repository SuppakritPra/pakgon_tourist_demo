<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterOrganizationPosition Entity
 *
 * @property int $id
 * @property int $master_organization_id
 * @property int $master_department_id
 * @property int $master_section_id
 * @property int $master_language_id
 * @property string $organization_position_name
 * @property bool $is_used
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\MasterOrganization $master_organization
 * @property \App\Model\Entity\MasterDepartment $master_department
 * @property \App\Model\Entity\MasterSection $master_section
 * @property \App\Model\Entity\MasterLanguage $master_language
 */
class MasterOrganizationPosition extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'master_organization_id' => true,
        'master_department_id' => true,
        'master_section_id' => true,
        'master_language_id' => true,
        'organization_position_name' => true,
        'is_used' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'master_organization' => true,
        'master_department' => true,
        'master_section' => true,
        'master_language' => true
    ];
}
