<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    #----------------------------------------------------------------------------------------------------
    $routes->connect('/Signups', ['controller' => 'Signups', 'action' => 'index', 'allowWithoutToken' => true]);
    $routes->connect('/Signups/verify', ['controller' => 'Signups', 'action' => 'verify', 'allowWithoutToken' => true]);
    $routes->connect('/Signins', ['controller' => 'Signins', 'action' => 'index', 'allowWithoutToken' => true]);
    $routes->connect('/Signins/verify', ['controller' => 'Signins', 'action' => 'verify', 'allowWithoutToken' => true]);
    $routes->connect('/Signins/reautorize', ['controller' => 'Signins', 'action' => 'reautorize', 'allowWithoutToken' => true]);
    $routes->connect('/Signins/notification', ['controller' => 'Signins', 'action' => 'notification', 'allowWithoutToken' => true]);
    $routes->connect('/Profiles/notification', ['controller' => 'Profiles', 'action' => 'notification', 'allowWithoutToken' => true]);
    $routes->connect('/Profiles/forgetPassword', ['controller' => 'Profiles', 'action' => 'forgetPassword', 'allowWithoutToken' => true]);
    $routes->connect('/Profiles/changePassword', ['controller' => 'Profiles', 'action' => 'changePassword', 'allowWithoutToken' => true]);

    #By Pass
    #$routes->connect('/Connects/language', ['controller' => 'Connects', 'action' => 'language', 'allowWithoutToken' => true]);
    $routes->connect('/Connects/profile', ['controller' => 'Connects', 'action' => 'profile', 'allowWithoutToken' => true]);
    $routes->connect('/Profiles', ['controller' => 'Profiles', 'action' => 'index', 'allowWithoutToken' => true]);
    $routes->connect('/Profiles/info', ['controller' => 'Profiles', 'action' => 'info', 'allowWithoutToken' => true]);
    $routes->connect('/Barcodes/response', ['controller' => 'Barcodes', 'action' => 'response', 'allowWithoutToken' => true]);
    $routes->connect('/Notifications/push', ['controller' => 'Notifications', 'action' => 'push', 'allowWithoutToken' => true]);
    $routes->connect('/Contacts', ['controller' => 'Contacts', 'action' => 'index', 'allowWithoutToken' => true]);
    #$routes->connect('/Contacts/addFriend', ['controller' => 'Contacts', 'action' => 'addFriend', 'allowWithoutToken' => true]);
    $routes->connect('/Points/addUserPoint', ['controller' => 'Points', 'action' => 'addUserPoint', 'allowWithoutToken' => true]);
    #----------------------------------------------------------------------------------------------------

    $routes->connect('/api/v1/post.beacon.send.notification', ['controller' => 'PakgonFCM', 'action' => 'beaconSendNotification', 'allowWithoutToken' => true]);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
