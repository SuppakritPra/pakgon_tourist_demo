<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterBeaconInstallationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterBeaconInstallationsTable Test Case
 */
class MasterBeaconInstallationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterBeaconInstallationsTable
     */
    public $MasterBeaconInstallations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_beacon_installations',
        'app.master_location_areas',
        'app.master_beacons',
        'app.master_beacon_install_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterBeaconInstallations') ? [] : ['className' => MasterBeaconInstallationsTable::class];
        $this->MasterBeaconInstallations = TableRegistry::get('MasterBeaconInstallations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterBeaconInstallations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
