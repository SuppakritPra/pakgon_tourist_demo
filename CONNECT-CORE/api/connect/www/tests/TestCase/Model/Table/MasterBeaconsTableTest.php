<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterBeaconsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterBeaconsTable Test Case
 */
class MasterBeaconsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterBeaconsTable
     */
    public $MasterBeacons;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_beacons',
        'app.master_beacon_install_types',
        'app.master_beacon_installations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterBeacons') ? [] : ['className' => MasterBeaconsTable::class];
        $this->MasterBeacons = TableRegistry::get('MasterBeacons', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterBeacons);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
