<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterBeaconDetectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterBeaconDetectsTable Test Case
 */
class MasterBeaconDetectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterBeaconDetectsTable
     */
    public $MasterBeaconDetects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_beacon_detects',
        'app.users',
        'app.user_personals',
        'app.user_profiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterBeaconDetects') ? [] : ['className' => MasterBeaconDetectsTable::class];
        $this->MasterBeaconDetects = TableRegistry::get('MasterBeaconDetects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterBeaconDetects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
