<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterLanguagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterLanguagesTable Test Case
 */
class MasterLanguagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterLanguagesTable
     */
    public $MasterLanguages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_languages',
        'app.user_default_languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterLanguages') ? [] : ['className' => MasterLanguagesTable::class];
        $this->MasterLanguages = TableRegistry::get('MasterLanguages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterLanguages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
