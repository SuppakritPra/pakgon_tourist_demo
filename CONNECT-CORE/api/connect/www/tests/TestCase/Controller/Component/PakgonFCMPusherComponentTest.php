<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\PakgonFCMPusherComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\PakgonFCMPusherComponent Test Case
 */
class PakgonFCMPusherComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\PakgonFCMPusherComponent
     */
    public $PakgonFCMPusher;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->PakgonFCMPusher = new PakgonFCMPusherComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PakgonFCMPusher);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
