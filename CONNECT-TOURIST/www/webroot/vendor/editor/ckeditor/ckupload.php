<?php

/**
 * 
 * Function used fro generate _VERSION_
 * @author  sarawutt.b
 * @return  biginteger of the version number
 */
function VERSION() {
    $parts = explode(' ', microtime());
    $micro = $parts[0] * 1000000;
    return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
}

$userfile_extn = substr($_FILES['upload']['name'], strrpos($_FILES['upload']['name'], '.') + 1);
$url = '/webroot/upload/ckuploads/' . VERSION() . ".{$userfile_extn}";

//extensive suitability check before doing anything with the file...
if (($_FILES['upload'] == "none") OR ( empty($_FILES['upload']['name']))) {
    $message = "No file uploaded.";
} else if ($_FILES['upload']["size"] == 0) {
    $message = "The file is of zero length.";
} else if (($_FILES['upload']["type"] != "image/pjpeg") AND ( $_FILES['upload']["type"] != "image/jpeg") AND ( $_FILES['upload']["type"] != "image/png") AND ( $_FILES['upload']["type"] != "image/gif")) {
    $message = "The image must be in either GIF , JPG or PNG format. Please upload a JPG or PNG instead.";
} else if (!is_uploaded_file($_FILES['upload']["tmp_name"])) {
    $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
} else if ($_FILES["upload"]["size"] > 1000000) {
    $message = "File size too big!";
} else if (file_exists($url . $_FILES["upload"]["name"])) {
    $message = $_FILES["upload"]["name"] . " already exists. ";
} else if (!is_uploaded_file($_FILES['upload']["tmp_name"])) {
    $message = "Invalid File!";
} else {
    $message = "";
    $uploadPath = $_SERVER['DOCUMENT_ROOT'] . $url;
    $move = move_uploaded_file($_FILES['upload']['tmp_name'], $uploadPath);
    if (!$move) {
        $message = "Error:: {$move} moving uploaded file to path {$uploadPath}. Check the script is granted Read/Write/Modify permissions.";
    }
}


if ($message != "") {
    $url = "";
    echo "<script>console.log('{$message}');</script>";
}

$funcNum = $_GET['CKEditorFuncNum'];
echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({$funcNum}, '{$url}', '{$message}');</script>";
?>