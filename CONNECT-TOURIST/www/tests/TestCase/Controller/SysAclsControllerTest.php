<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SysAclsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SysAclsController Test Case
 */
class SysAclsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sys_acls',
        'app.sys_controllers',
        'app.menus',
        'app.sys_actions',
        'app.menu_parents',
        'app.users',
        'app.roles',
        'app.positions',
        'app.name_prefixes',
        'app.company_taxes',
        'app.nationalities',
        'app.ethnicities',
        'app.user_homes',
        'app.provinces',
        'app.districts',
        'app.sub_districts'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
