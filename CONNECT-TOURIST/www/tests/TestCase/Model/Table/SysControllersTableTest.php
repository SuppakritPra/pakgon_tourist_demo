<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SysControllersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SysControllersTable Test Case
 */
class SysControllersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SysControllersTable
     */
    public $SysControllers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sys_controllers',
        'app.menus',
        'app.sys_actions',
        'app.menu_parents',
        'app.sys_acls'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SysControllers') ? [] : ['className' => SysControllersTable::class];
        $this->SysControllers = TableRegistry::get('SysControllers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SysControllers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findSysControllersList method
     *
     * @return void
     */
    public function testFindSysControllersList()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
