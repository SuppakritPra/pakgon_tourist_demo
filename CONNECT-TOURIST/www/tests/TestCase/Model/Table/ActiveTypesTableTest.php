<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ActiveTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ActiveTypesTable Test Case
 */
class ActiveTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ActiveTypesTable
     */
    public $ActiveTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.active_types',
        'app.user_active'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ActiveTypes') ? [] : ['className' => ActiveTypesTable::class];
        $this->ActiveTypes = TableRegistry::get('ActiveTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActiveTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
