<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubDistrictsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubDistrictsTable Test Case
 */
class SubDistrictsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubDistrictsTable
     */
    public $SubDistricts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sub_districts',
        'app.regions',
        'app.districts',
        'app.provinces',
        'app.zipcodes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SubDistricts') ? [] : ['className' => SubDistrictsTable::class];
        $this->SubDistricts = TableRegistry::get('SubDistricts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubDistricts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findSubDistrictsList method
     *
     * @return void
     */
    public function testFindSubDistrictsList()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
