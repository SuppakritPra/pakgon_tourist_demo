<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewTouristTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewTouristTable Test Case
 */
class ReviewTouristTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewTouristTable
     */
    public $ReviewTourist;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.review_tourist',
        'app.users',
        'app.tourist_attraction'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReviewTourist') ? [] : ['className' => ReviewTouristTable::class];
        $this->ReviewTourist = TableRegistry::get('ReviewTourist', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReviewTourist);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
