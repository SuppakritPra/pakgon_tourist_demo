<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserSubscribesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserSubscribesTable Test Case
 */
class UserSubscribesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserSubscribesTable
     */
    public $UserSubscribes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_subscribes',
        'app.users',
        'app.pages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserSubscribes') ? [] : ['className' => UserSubscribesTable::class];
        $this->UserSubscribes = TableRegistry::get('UserSubscribes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserSubscribes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
