<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubCategoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubCategoryTable Test Case
 */
class SubCategoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubCategoryTable
     */
    public $SubCategory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sub_category',
        'app.category'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SubCategory') ? [] : ['className' => SubCategoryTable::class];
        $this->SubCategory = TableRegistry::get('SubCategory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubCategory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
