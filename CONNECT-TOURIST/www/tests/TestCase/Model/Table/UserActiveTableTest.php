<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserActiveTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserActiveTable Test Case
 */
class UserActiveTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserActiveTable
     */
    public $UserActive;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_active',
        'app.users',
        'app.tourist_attraction',
        'app.active_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserActive') ? [] : ['className' => UserActiveTable::class];
        $this->UserActive = TableRegistry::get('UserActive', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserActive);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
