<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserActivesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserActivesTable Test Case
 */
class UserActivesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserActivesTable
     */
    public $UserActives;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_actives',
        'app.users',
        'app.tourist_attraction',
        'app.active_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserActives') ? [] : ['className' => UserActivesTable::class];
        $this->UserActives = TableRegistry::get('UserActives', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserActives);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
