<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TouristAttractionImageTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TouristAttractionImageTable Test Case
 */
class TouristAttractionImageTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TouristAttractionImageTable
     */
    public $TouristAttractionImage;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tourist_attraction_image',
        'app.tourist_attraction'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TouristAttractionImage') ? [] : ['className' => TouristAttractionImageTable::class];
        $this->TouristAttractionImage = TableRegistry::get('TouristAttractionImage', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TouristAttractionImage);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
