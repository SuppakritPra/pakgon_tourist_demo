<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TouristAttractionTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TouristAttractionTable Test Case
 */
class TouristAttractionTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TouristAttractionTable
     */
    public $TouristAttraction;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tourist_attraction',
        'app.category',
        'app.master_province',
        'app.review_tourist'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TouristAttraction') ? [] : ['className' => TouristAttractionTable::class];
        $this->TouristAttraction = TableRegistry::get('TouristAttraction', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TouristAttraction);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
