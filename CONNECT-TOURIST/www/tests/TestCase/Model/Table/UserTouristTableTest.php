<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserTouristTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserTouristTable Test Case
 */
class UserTouristTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserTouristTable
     */
    public $UserTourist;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_tourist'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserTourist') ? [] : ['className' => UserTouristTable::class];
        $this->UserTourist = TableRegistry::get('UserTourist', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserTourist);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
