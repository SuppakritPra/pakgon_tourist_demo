<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserHomesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserHomesTable Test Case
 */
class UserHomesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserHomesTable
     */
    public $UserHomes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_homes',
        'app.users',
        'app.roles',
        'app.sys_acls',
        'app.positions',
        'app.name_prefixes',
        'app.company_taxes',
        'app.nationalities',
        'app.ethnicities',
        'app.provinces',
        'app.districts',
        'app.sub_districts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserHomes') ? [] : ['className' => UserHomesTable::class];
        $this->UserHomes = TableRegistry::get('UserHomes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserHomes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findUserHomesList method
     *
     * @return void
     */
    public function testFindUserHomesList()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
