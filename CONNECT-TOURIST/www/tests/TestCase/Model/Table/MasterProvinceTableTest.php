<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterProvinceTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterProvinceTable Test Case
 */
class MasterProvinceTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterProvinceTable
     */
    public $MasterProvince;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_province',
        'app.region'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterProvince') ? [] : ['className' => MasterProvinceTable::class];
        $this->MasterProvince = TableRegistry::get('MasterProvince', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterProvince);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
