<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SysAclsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SysAclsTable Test Case
 */
class SysAclsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SysAclsTable
     */
    public $SysAcls;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sys_acls',
        'app.sys_controllers',
        'app.menus',
        'app.sys_actions',
        'app.menu_parents',
        'app.users',
        'app.roles',
        'app.positions',
        'app.name_prefixes',
        'app.company_taxes',
        'app.nationalities',
        'app.ethnicities',
        'app.user_homes',
        'app.provinces',
        'app.districts',
        'app.sub_districts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SysAcls') ? [] : ['className' => SysAclsTable::class];
        $this->SysAcls = TableRegistry::get('SysAcls', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SysAcls);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findSysAclsList method
     *
     * @return void
     */
    public function testFindSysAclsList()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
