<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentAttachmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentAttachmentsTable Test Case
 */
class DocumentAttachmentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentAttachmentsTable
     */
    public $DocumentAttachments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document_attachments',
        'app.reves'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DocumentAttachments') ? [] : ['className' => DocumentAttachmentsTable::class];
        $this->DocumentAttachments = TableRegistry::get('DocumentAttachments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentAttachments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findDocumentAttachmentsList method
     *
     * @return void
     */
    public function testFindDocumentAttachmentsList()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
