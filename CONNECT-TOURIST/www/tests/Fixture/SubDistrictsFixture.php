<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubDistrictsFixture
 *
 */
class SubDistrictsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'code' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'รหัสอ้างอิง ตำบล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ชื่อตำบล', 'precision' => null, 'fixed' => null],
        'name_eng' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => true, 'collate' => null, 'comment' => 'ชื่อตำบล อังกฤษ', 'precision' => null, 'fixed' => null],
        'region_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'รหัสอ้างอิงภูมิภาค', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'province_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'รหัสอ้างอิงจังหวัด', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'district_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'รหัสอ้างอิงอำเภอ', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'status' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'A', 'null' => false, 'collate' => null, 'comment' => 'A = Active, N = Inactive , D = Delete', 'precision' => null],
        'create_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'ผู้บันทึกข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'update_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'ผู้ปรับปรุงข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'code' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'name_eng' => 'Lorem ipsum dolor sit amet',
            'region_id' => 1,
            'province_id' => 1,
            'district_id' => 1,
            'status' => 'Lorem ipsum dolor sit ame',
            'create_uid' => 1,
            'update_uid' => 1,
            'created' => 1524223264,
            'modified' => 1524223264
        ],
    ];
}
