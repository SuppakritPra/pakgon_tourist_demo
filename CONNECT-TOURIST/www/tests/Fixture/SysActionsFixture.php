<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SysActionsFixture
 *
 */
class SysActionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'sys_controller_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'หมายเลขอ้างอิง คอนโทรเลอร์', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 256, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ชื่อแอคชั่น', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => true, 'collate' => null, 'comment' => 'รายละเอียด แอคชั่น', 'precision' => null, 'fixed' => null],
        'status' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'A', 'null' => false, 'collate' => null, 'comment' => ' N=New, A = Active, I = Inactive , D = Delete', 'precision' => null],
        'create_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'ผู้บันทึกข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'update_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'ผู้ปรับปรุงข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'sys_actions_controller_idx' => ['type' => 'index', 'columns' => ['id', 'sys_controller_id'], 'length' => []],
            'sys_actions_name_idx' => ['type' => 'index', 'columns' => ['name'], 'length' => []],
            'sys_actions_status_idx' => ['type' => 'index', 'columns' => ['status'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'sys_actions_uq_name_key' => ['type' => 'unique', 'columns' => ['sys_controller_id', 'name'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'sys_controller_id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'status' => 'Lorem ipsum dolor sit ame',
            'create_uid' => 1,
            'update_uid' => 1,
            'created' => 1524222812,
            'modified' => 1524222812
        ],
    ];
}
