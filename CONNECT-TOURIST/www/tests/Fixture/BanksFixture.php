<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BanksFixture
 *
 */
class BanksFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ชื่อธนาคาร', 'precision' => null, 'fixed' => null],
        'name_eng' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => true, 'collate' => null, 'comment' => 'ชื่อธนาคาร อังกฤษ', 'precision' => null, 'fixed' => null],
        'order_no' => ['type' => 'integer', 'length' => 10, 'default' => '0', 'null' => false, 'comment' => 'ลำดับที่', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'status' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'A', 'null' => false, 'collate' => null, 'comment' => 'A = Active, N = Inactive , D = Delete', 'precision' => null],
        'ref1' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'ref2' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'ref3' => ['type' => 'string', 'length' => 128, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'ref4' => ['type' => 'string', 'length' => 256, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'create_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'ผู้เพิ่มข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'update_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'ผู้บันทึกข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'name_eng' => 'Lorem ipsum dolor sit amet',
            'order_no' => 1,
            'status' => 'Lorem ipsum dolor sit ame',
            'ref1' => 1,
            'ref2' => 1,
            'ref3' => 'Lorem ipsum dolor sit amet',
            'ref4' => 'Lorem ipsum dolor sit amet',
            'create_uid' => 1,
            'update_uid' => 1,
            'created' => 1524223190,
            'modified' => 1524223190
        ],
    ];
}
