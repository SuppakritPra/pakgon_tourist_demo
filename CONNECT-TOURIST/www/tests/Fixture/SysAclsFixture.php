<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SysAclsFixture
 *
 */
class SysAclsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'sys_controller_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'หมายเลขอ้างอิง คอนโทรเลอร์', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'sys_action_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'หมายเลขอ้างอิง แอคชั่น', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'หมายเลขอ้างอิงผู้ใช้งาน', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'role_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'หมายเลขอ้างอิงกลุ่มผู้ใช้งาน', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'status' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'A', 'null' => false, 'collate' => null, 'comment' => 'N  = New, A = Active, I = Inactive , D = Delete', 'precision' => null],
        'create_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'ผู้บันทึกข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'update_uid' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'ผู้เพิ่มข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'sys_acls_user_id_role_id_idx' => ['type' => 'index', 'columns' => ['user_id', 'role_id'], 'length' => []],
            'sys_acls_status_idx' => ['type' => 'index', 'columns' => ['status'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'sys_controller_id' => 1,
            'sys_action_id' => 1,
            'user_id' => 1,
            'role_id' => 1,
            'status' => 'Lorem ipsum dolor sit ame',
            'create_uid' => 1,
            'update_uid' => 1,
            'created' => 1524222976,
            'modified' => 1524222976
        ],
    ];
}
