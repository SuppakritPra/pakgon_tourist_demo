<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserHomesFixture
 *
 */
class UserHomesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'user_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => 'เลขที่อ้างอิงเจ้าหน้าที่', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'address_type' => ['type' => 'string', 'length' => 50, 'default' => 'HOME', 'null' => false, 'collate' => null, 'comment' => 'ประเภทของที่อยู่ HOME,APARTMENT,CONDO,FLAT', 'precision' => null, 'fixed' => null],
        'is_home_registration' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'N', 'null' => false, 'collate' => null, 'comment' => 'เป็นที่อยู่ตามทะเบียนบ้านหรือไม่ Y = ใช่ N = ไม่ใช่', 'precision' => null],
        'address' => ['type' => 'string', 'length' => 1024, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ที่อยู่', 'precision' => null, 'fixed' => null],
        'province_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'หมายเลขอ้างอิงจังหวัด', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'district_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'หมายเลขอ้างอิงอำเภอ', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'sub_district_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => 'หมายเลขอ้างอิงตำบล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'zipcode' => ['type' => 'string', 'length' => 10, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'รหัสไปรษณีย์', 'precision' => null, 'fixed' => null],
        'status' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'A', 'null' => false, 'collate' => null, 'comment' => 'N  = New, A = Active, I = Inactive , D = Delete', 'precision' => null],
        'create_uid' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => 'ผู้เพิ่มข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'update_uid' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => 'ผู้แก้ไขข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'user_id' => 1,
            'address_type' => 'Lorem ipsum dolor sit amet',
            'is_home_registration' => 'Lorem ipsum dolor sit ame',
            'address' => 'Lorem ipsum dolor sit amet',
            'province_id' => 1,
            'district_id' => 1,
            'sub_district_id' => 1,
            'zipcode' => 'Lorem ip',
            'status' => 'Lorem ipsum dolor sit ame',
            'create_uid' => 1,
            'update_uid' => 1,
            'created' => 1524222890,
            'modified' => 1524222890
        ],
    ];
}
