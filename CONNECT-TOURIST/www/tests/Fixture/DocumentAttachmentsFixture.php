<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DocumentAttachmentsFixture
 *
 */
class DocumentAttachmentsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'document_type' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => true, 'collate' => null, 'comment' => 'ประเภทของเอกสาร', 'precision' => null, 'fixed' => null],
        'attachment_name' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ชื้อเอกสาร', 'precision' => null, 'fixed' => null],
        'attachment_name_origin' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ชื่อเอกสารตามเดิม', 'precision' => null, 'fixed' => null],
        'display_name' => ['type' => 'string', 'length' => 256, 'default' => null, 'null' => true, 'collate' => null, 'comment' => 'ชื่อเอกสารแสดงผล', 'precision' => null, 'fixed' => null],
        'attachment_extension' => ['type' => 'string', 'length' => 10, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'นามสกสกุลเอกสาร', 'precision' => null, 'fixed' => null],
        'attachment_path' => ['type' => 'string', 'length' => 512, 'default' => null, 'null' => false, 'collate' => null, 'comment' => 'ที่อยู่เอกสาร', 'precision' => null, 'fixed' => null],
        'ref_model' => ['type' => 'string', 'length' => 256, 'default' => null, 'null' => true, 'collate' => null, 'comment' => 'ชื่อ โมเดล หรือ เทเบิลอ้างอิง', 'precision' => null, 'fixed' => null],
        'ref_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => 'หมายเลข ID อ้างอิงของเทเบิลหรือ โมเดล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'status' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'default' => 'A', 'null' => false, 'collate' => null, 'comment' => 'A = Active, N = Inactive , D = Delete', 'precision' => null],
        'create_uid' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => 'ผู้บันทึกข้อมูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'update_uid' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => 'ผู้ปรับปรุงแก้ไขข้มูล', 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'document_type' => 'Lorem ipsum dolor sit amet',
            'attachment_name' => 'Lorem ipsum dolor sit amet',
            'attachment_name_origin' => 'Lorem ipsum dolor sit amet',
            'display_name' => 'Lorem ipsum dolor sit amet',
            'attachment_extension' => 'Lorem ip',
            'attachment_path' => 'Lorem ipsum dolor sit amet',
            'ref_model' => 'Lorem ipsum dolor sit amet',
            'ref_id' => 1,
            'status' => 'Lorem ipsum dolor sit ame',
            'create_uid' => 1,
            'update_uid' => 1,
            'created' => 1524222862,
            'modified' => 1524222862
        ],
    ];
}
