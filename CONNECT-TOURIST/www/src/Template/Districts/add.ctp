<?php
/**
  * 
  * add districts template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\District $district
  * @since   2018/04/20 18:20:51
  * @license pakgon.Ltd.
  */
?>
<div class="districts districts-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('District Management System => ( Add District )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($district, ['horizontal' => true]);
            echo $this->Form->control('code');
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('region_id', ['options' => $regions, 'empty' => true]);
            echo $this->Form->control('province_id', ['options' => $provinces]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/Districts/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add district ?'), 'data-confirm-title' => __('Confirm for add district ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
