<?php

 /**
  *
  * The template view for view as of districts controller the page show for districts information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\District $district
  * @since  2018-04-20 18:20:51
  * @license Pakgon.Ltd
  */
?>
<div class="districts view districts-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('District Management System => ({0} information)', h($district->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Districts Name'); ?></td>
                <td class="table-view-detail"><?php echo h($district->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Districts Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($district->name_eng); ?></td>
            </tr>
                                    <tr>
                <td class="table-view-label"><?php echo __('Region'); ?></td>
                <td class="table-view-detail"><?php echo $district->has('region') ? $this->Html->link($district->region->name, ['controller' => 'Regions', 'action' => 'view', $district->region->id]) : ''; ?></td>
            </tr>
                            <tr>
                <td class="table-view-label"><?php echo __('Province'); ?></td>
                <td class="table-view-detail"><?php echo $district->has('province') ? $this->Html->link($district->province->name, ['controller' => 'Provinces', 'action' => 'view', $district->province->id]) : ''; ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($district->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Districts Id'); ?></td>
                <td class="table-view-detail"><?php echo h($district->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Code'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($district->code); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($district->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($district->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($district->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($district->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
