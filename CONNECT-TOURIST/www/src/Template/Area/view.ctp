<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Area $area
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Area'), ['action' => 'edit', $area->area_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Area'), ['action' => 'delete', $area->area_id], ['confirm' => __('Are you sure you want to delete # {0}?', $area->area_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Area'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List District'), ['controller' => 'District', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New District'), ['controller' => 'District', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="area view large-9 medium-8 columns content">
    <h3><?= h($area->area_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Area Name') ?></th>
            <td><?= h($area->area_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('District') ?></th>
            <td><?= $area->has('district') ? $this->Html->link($area->district->id, ['controller' => 'District', 'action' => 'view', $area->district->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Area Id') ?></th>
            <td><?= $this->Number->format($area->area_id) ?></td>
        </tr>
    </table>
</div>
