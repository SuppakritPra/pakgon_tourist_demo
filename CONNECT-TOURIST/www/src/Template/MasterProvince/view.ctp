<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MasterProvince $masterProvince
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Master Province'), ['action' => 'edit', $masterProvince->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Master Province'), ['action' => 'delete', $masterProvince->id], ['confirm' => __('Are you sure you want to delete # {0}?', $masterProvince->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Master Province'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Master Province'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Region'), ['controller' => 'Region', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Region', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="masterProvince view large-9 medium-8 columns content">
    <h3><?= h($masterProvince->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Province Name Th') ?></th>
            <td><?= h($masterProvince->province_name_th) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Province Name En') ?></th>
            <td><?= h($masterProvince->province_name_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Logo Of Province') ?></th>
            <td><?= h($masterProvince->logo_of_province) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Region') ?></th>
            <td><?= $masterProvince->has('region') ? $this->Html->link($masterProvince->region->id, ['controller' => 'Region', 'action' => 'view', $masterProvince->region->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($masterProvince->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Center Lat') ?></th>
            <td><?= $this->Number->format($masterProvince->center_lat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Center Long') ?></th>
            <td><?= $this->Number->format($masterProvince->center_long) ?></td>
        </tr>
    </table>
</div>
