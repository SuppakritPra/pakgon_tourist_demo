<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MasterProvince $masterProvince
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $masterProvince->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $masterProvince->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Master Province'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Region'), ['controller' => 'Region', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Region', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="masterProvince form large-9 medium-8 columns content">
    <?= $this->Form->create($masterProvince) ?>
    <fieldset>
        <legend><?= __('Edit Master Province') ?></legend>
        <?php
            echo $this->Form->control('province_name_th');
            echo $this->Form->control('province_name_en');
            echo $this->Form->control('logo_of_province');
            echo $this->Form->control('region_id', ['options' => $region, 'empty' => true]);
            echo $this->Form->control('center_lat');
            echo $this->Form->control('center_long');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
