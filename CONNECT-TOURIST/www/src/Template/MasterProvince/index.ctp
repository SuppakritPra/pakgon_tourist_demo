<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MasterProvince[]|\Cake\Collection\CollectionInterface $masterProvince
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Master Province'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Region'), ['controller' => 'Region', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Region', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="masterProvince index large-9 medium-8 columns content">
    <h3><?= __('Master Province') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('province_name_th') ?></th>
                <th scope="col"><?= $this->Paginator->sort('province_name_en') ?></th>
                <th scope="col"><?= $this->Paginator->sort('logo_of_province') ?></th>
                <th scope="col"><?= $this->Paginator->sort('region_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('center_lat') ?></th>
                <th scope="col"><?= $this->Paginator->sort('center_long') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($masterProvince as $masterProvince): ?>
            <tr>
                <td><?= $this->Number->format($masterProvince->id) ?></td>
                <td><?= h($masterProvince->province_name_th) ?></td>
                <td><?= h($masterProvince->province_name_en) ?></td>
                <td><?= h($masterProvince->logo_of_province) ?></td>
                <td><?= $masterProvince->has('region') ? $this->Html->link($masterProvince->region->id, ['controller' => 'Region', 'action' => 'view', $masterProvince->region->id]) : '' ?></td>
                <td><?= $this->Number->format($masterProvince->center_lat) ?></td>
                <td><?= $this->Number->format($masterProvince->center_long) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $masterProvince->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $masterProvince->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $masterProvince->id], ['confirm' => __('Are you sure you want to delete # {0}?', $masterProvince->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
