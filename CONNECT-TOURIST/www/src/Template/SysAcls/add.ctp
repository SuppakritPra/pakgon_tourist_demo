<?php
/**
 * 
 * add sys acls template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SysAcl $sysAcl
 * @since   2018/04/20 18:16:17
 * @license pakgon.Ltd.
 */
?>
<div class="box box-warning">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('System ACLs Group'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#'); ?></th>
                        <th><?php echo __('Role Name'); ?></th>
                        <th style="width: 20%;text-align: center;"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($roles as $k => $v) : ?>
                        <tr>
                            <td class="nindex-black"><?php echo ++$k; ?></td>
                            <td><?php echo $this->Utility->dashEmpty($v->name); ?></td>
                            <td>
                                <?php echo $this->Permission->link(__('Grant / Revoke'), '/SysAcls/addPermission/' . $v->id, ['icon' => 'fa-gear']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->button(__('Back'), '/sysAcls/index', ['class' => 'btn-default pull-right separate-button']); ?>
        <?php echo $this->Permission->button(__("Add Roles"), "/roles/add/", ['icon' => 'fa-plus', 'class' => 'bg-purple btn-flat pull-right']); ?>
    </div>
</div>