<?php
/**
 * The template index for index as of sysAcls controller the page show for short sysAcls information.
 *
 * @author  sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SysAcl[]|\Cake\Collection\CollectionInterface $sysAcls
 * @since  2018-04-20 18:16:17
 * @license Pakgon.Ltd
 */
?>
<div class="sysAcls index">

    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sys Acl Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('sysAcls'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Sys Acl'), '/sysAcls/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Sys Acl'), '/sysAcls/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->


    <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sys Acls list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
            <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('Row no'); ?></th>
                        <th><?php echo $this->Paginator->sort('sys_controller_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('sys_action_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('role_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($sysAcls)): ?>
                        <?php foreach ($sysAcls as $index => $sysAcl): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                <td><?php echo $sysAcl->has('sys_controller') ? $this->Html->link($sysAcl->sys_controller->name, ['controller' => 'SysControllers', 'action' => 'view', $sysAcl->sys_controller->id]) : ''; ?></td>
                                <td><?php echo $sysAcl->has('sys_action') ? $this->Html->link($sysAcl->sys_action->name, ['controller' => 'SysActions', 'action' => 'view', $sysAcl->sys_action->id]) : ''; ?></td>
                                <td><?php echo $sysAcl->has('user') ? $this->Html->link($sysAcl->user->id, ['controller' => 'Users', 'action' => 'view', $sysAcl->user->id]) : '-'; ?></td>
                                <td><?php echo $sysAcl->has('role') ? $this->Html->link($sysAcl->role->name, ['controller' => 'Roles', 'action' => 'view', $sysAcl->role->id]) : ''; ?></td>
                                <td><?php echo $this->Utility->getMainStatus($sysAcl->status, true); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="6"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
