<?php
/**
 * 
 * The template for add page of sysAclsController.
 * @author  sarawutt.b
 * @var     \App\View\AppView $this
 * @var     \App\Model\Entity\SysAcl $sysAcl
 * @since   2018-03-13 15:40:32
 * @license Pakgon.Ltd
 */
?>
<div class="box box-primary sysAcls form hpanel hblue">
    <div class="box-header">
        <h3 class="box-title"><?php echo __("System ACLs (Add ACLs)") . ' ' . $this->Bootstrap->label($this->Utility->getRoleNameById($roleId), 'info', null, 'p'); ?></h3>
        <?php echo $this->Permission->linkBack('/SysAcls/add/acl'); ?>
        <?php echo $this->Permission->button(__("Add Cotrollers"), "/SysControllers/add/", ['class' => 'bg-orange btn-flat pull-right', 'icon' => 'fa-plus']); ?>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered dataTable table-add-details" role="grid">
                <thead>
                    <tr>
                        <th><?php echo __('Controllers / Actions available.'); ?></th>
                        <th>&nbsp;</th>
                        <th><?php echo __('Controllers / Actions permission allow.'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                            $totalAvailableMenu = count($availableMenus);
                            if (!empty($availableMenus)) :
                                echo $this->Form->create('SysAcl', ['url' => ['controller' => 'SysAcls', 'action' => 'addPermission', $roleId], 'name' => 'frmSystemACL', 'id' => 'frmSystemACL']);
                                echo $this->Form->input('role_id', ['type' => 'hidden', 'value' => $roleId]);
                                echo $this->Form->input('caction', [
                                    'templates' => ['checkboxWrapper' => '<div class="permission-lists acl-permission checkbox-table-striped checkbox">{{label}}</div>'],
                                    'type' => 'select',
                                    'multiple' => 'checkbox',
                                    'options' => $controllerPackages,
                                    'label' => false]
                                );
                                echo $this->Form->end();
                            endif;
                            ?>
                        </td>


                        <td>
                            <?php
                            $totalAlreadyAcl = count($completedControlPackages);
                            $totalAvailableMenu = ($totalAvailableMenu > $totalAlreadyAcl) ? $totalAvailableMenu : $totalAlreadyAcl;
                            $positionInMiddle = ceil($totalAvailableMenu / 2);
                            if ($totalAvailableMenu > 30) {
                                for ($i = 1; $i <= $totalAvailableMenu; $i++) {
                                    if (($i == 1) || ($i == $positionInMiddle) || ($i == $totalAvailableMenu)) {
                                        echo $this->Form->submit('===>>>', ['class' => 'btn btn-sm btn-success', 'onclick' => 'submitfrmSystemACL()', 'div' => 'text-center', 'id' => 'btnSystemACL', 'name' => 'btnSystemACL']);
                                        echo "<br/><br/>";
                                        echo $this->Form->submit("<<<===", ['class' => 'btn btn-sm btn-danger', 'onclick' => 'submitfrmUserACL()', 'div' => 'text-center', 'id' => 'btnUserACL', 'name' => 'btnUserACL']);
                                    } else {
                                        echo "<br/><br/>";
                                    }
                                }
                            } else {
                                echo $this->Form->submit("===>>>", ['class' => 'btn btn-sm btn-success', 'onclick' => 'submitfrmSystemACL()', 'div' => 'text-center', 'id' => 'btnSystemACL', 'name' => 'btnSystemACL']);
                                echo "<br/><br/>";
                                echo $this->Form->submit("<<<===", ['class' => 'btn btn-sm btn-danger', 'onclick' => 'submitfrmUserACL()', 'div' => 'text-center', 'id' => 'btnUserACL', 'name' => 'btnUserACL']);
                            }
                            ?>

                        </td>   


                        <td>
                            <?php
                            $completed = count($completedControlPackages);
                            if (!empty($completedControlPackages)) :
                                echo $this->Form->create('SysAcl', ['url' => ['controller' => 'SysAcls', 'action' => 'deletePermission', $roleId], 'name' => 'frmUserACL', 'id' => 'frmUserACL']);
                                echo $this->Form->input('role_id', ['type' => 'hidden', 'value' => $roleId]);
                                echo $this->Form->input('completedaction', [
                                    'templates' => ['checkboxWrapper' => '<div class="permission-lists acl-permission checkbox-table-striped checkbox">{{label}}</div>'],
                                    'type' => 'select',
                                    'multiple' => 'checkbox',
                                    'options' => $completedControlPackages,
                                    'label' => false
                                ]);
                                echo $this->Form->end();
                            endif;
                            ?>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <?php echo $this->Permission->button(__('Add Permission'), null, ['class' => 'btn btn-sm btn-success', 'onclick' => 'submitfrmSystemACL()', 'div' => false, 'id' => 'btnsubmitSystemACL', 'name' => 'btnsubmitSystemACL', 'icon' => 'fa-plus']); ?>
            </div>
            <div class="col-md-6 pull-right">
                <?php echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Permission->button(__('Delete Permission'), null, ['class' => 'btn btn-sm btn-danger pull-right btn-r-sep', 'onclick' => 'submitfrmUserACL()', 'div' => false, 'id' => 'btnsubmitUserACL', 'name' => 'btnsubmitUserACL', 'icon' => 'fa-trash']); ?>
            </div>
        </div>
    </div><!-- ./panel-body -->
</div>

<script type="text/javascript">
    $(function () {
        /**
         * Check all for no permission
         * @author sarawutt.b
         */
        $("input[name~='caction[]']").on('ifChanged', function (event) {
            console.log($(this).prop('checked'));
            var tmpCheck = ($(this).prop('checked') == true) ? 'check' : 'uncheck';
            var tmpVal = $(this).val();
            if (tmpVal == '') {
                $("input[name~='caction[]']").iCheck(tmpCheck);
            }
        });

        /**
         * Check all for revoke permission
         * @author sarawutt.b
         */
        $("input[name~='completedaction[]']").on('ifChanged', function (event) {
            console.log($(this).prop('checked'));
            var tmpCheck = ($(this).prop('checked') == true) ? 'check' : 'uncheck';
            var tmpVal = $(this).val();
            if (tmpVal == '') {
                $("input[name~='completedaction[]']").iCheck(tmpCheck);
            }
        });
    });

    function submitfrmSystemACL() {
        $("#frmSystemACL").submit();
    }
    function submitfrmUserACL() {
        $("#frmUserACL").submit();
    }
</script>