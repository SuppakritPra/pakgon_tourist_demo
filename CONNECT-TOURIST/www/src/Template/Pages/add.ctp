<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Page $page
 */

?>
<div class="articles form large-9 medium-8 columns content">
<?php echo $this->Form->create(false, ['url' => ['controller'=> 'Pages','action' => 'add'], 'id'=>'page_add', 'type' => 'file']);?>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo  __('Add Pages') ?></h3>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <?php echo $this->Form->input('page_name', ['id' => 'page_name', 'class'=> 'required', 'label'=>__('Pagename')] );?>
                    </div>
                    <div class="form-group">
                        <?php echo __('Page Logo') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
                        <?php echo $this->Form->input('page_logo', array(
                                'label' => false,
                                'type' => 'file',
                                'accept' => 'image/jpeg,image/jpg',
                                'id' => 'page_logo',
                                'class' => 'required'
                                ));
                        ?>
                        <span for="page_logo" id="error-page-logo" generated="true" class="help-block">โปรดระบุ</span>
                    </div>
                    <div class="form-group">
                        <?php echo __('Page Banner') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
                        <?php echo $this->Form->input('page_banner', array(
                                'label' => false,
                                'type' => 'file',
                                'accept' => 'image/jpeg,image/jpg',
                                 'id' => 'page_banner',
                                'class' => 'required'
                                ));
                        ?>
                        <span for="page_banner"  id="error-page-banner" generated="true" class="help-block">โปรดระบุ</span>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Description'); ?></label>
                        <?php echo $this->Form->textarea('page_desc', ['id' => 'page_desc', 'class'=> 'required', 'label'=>false, 
                        'placeholder' => __('Description')] );?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->input( __('Create'), ['type'=>'submit', 'class'=>'btn btn-save', 'div'=>['class'=>'col-sm-2'], 'label'=>false]); ?>
    </div>
</div>
<?php echo $this->Form->end();?>
</div>
<?php $this->append('scriptBottom');?>
<script>
    $(document).ready(function() {
        $('.help-block').hide();
        $('#page_logo').on('change', function(){
            console.log($('#page_logo').val());
            var pl = $('#page_logo').val();
            if(pl!=''){
                $('#page_logo').parent(".form-group").removeClass('has-error');
                    $('#page_logo').parent(".form-group").parent(".form-group").removeClass('has-error');
                    $('#error-page-logo').hide();
            }
        });
        $('#page_banner').on('change', function(){
            console.log($('#page_banner').val());
             var pl = $('#page_banner').val();
            if(pl!=''){
                $('#page_banner').parent( ".form-group" ).removeClass('has-error');
                    $('#page_banner').parent(".form-group").parent(".form-group").removeClass('has-error');
                    $('#error-page-banner').hide();
            }
        });

        $('#page_add').submit(function(event) {

            var pl = $('#page_logo').val();

            if(pl == ''){
                //has-error
                $('#page_logo').parent(".form-group").addClass('has-error');
                $('#page_logo').parent(".form-group").parent(".form-group").addClass('has-error');
                $('#error-page-logo').show();
            }
            var pb = $('#page_banner').val();
            if(pb==''){
                //has-error
                $('#page_banner').parent( ".form-group" ).addClass('has-error');
                $('#page_banner').parent( ".form-group" ).parent( ".form-group" ).addClass('has-error');
                $( '#error-page-banner').show();
            }
            if(pl == '' || pb == ''){
                return false;
            }
        });
    });
</script>
<?php $this->end();?>