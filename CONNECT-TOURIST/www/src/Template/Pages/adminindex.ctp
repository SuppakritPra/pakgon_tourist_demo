<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
    </ul>
</nav>
<style>
    /* .row-title-header{
        border-bottom: 2px solid #f4f4f4;
        padding-bottom: 5px;
        padding-left: 15px;
    }

    .list-page {
        margin-top: 15px;
        margin-bottom: 15px;
        font-weight: 550;
        color:black;
        font-size: 12px;
    }

    .list-page .page-name{
        margin-top: 5px;
    }

    .list-page a:link,
    .list-page a:visited ,
    .list-page a:hover,
    .list-page a:active
    {
        color:black;
    } */
    #plusModal{
        background-color: rgba(0, 0,0, 0.8);
    }

    .page-empty{
        color:#fff;
        font-size: 20px;
        margin-top: 15px;
    }
    
     #plusModal #search{
      color:#fff;
       font-size: 25px;
     }
     
    .box-ads {
       margin: auto;
	     padding: auto;
	     position: relative;
	     padding-top: 10px;
	     margin-top: 5px;
    } 

    
</style>
<!-- ///////////// My Admin Start ///////////// -->
 <?php if(!empty($pages)){?>   

    <div class="row-title-header">
        <h3 class="freed-title">
            <div class="box-icon-header">
                <span class="icon-header">
                    <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                </span> 
            </div>
            <?= __('My Admin') ?>
        </h3>
    </div>

    <div class="box-body">
        <?php 
            $page_count = 0; 
  
	    foreach ($pages as $page){
            	$logo = $page['page_logo'];
        ?>
            <div class="col-xs-4 list-page" align="center">
                <a class="text-page" href="/Feed/page/<?php echo $page->id; ?>">
                    <div>
                        <?php echo $this->Html->image("/upload/image_page_logo/".$logo, [
                            "class" => "img-circle img-responsive",
                        ]); ?>
                    </div>
                    <div class="page-name">
                        <?php echo $page->page_name ?>
                    </div>
                </a>
            </div>
        <?php 
	}?>
    </div>
    <?php }?>
<!-- ///////////// My Admin End ///////////// -->

<!-- ///////////// My Subscribe Start ///////////// -->
    <div class="row-title-header">
        <h3 class="freed-title">
            <div class="box-icon-header">
                <span class="icon-header">
                    <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                </span> 
            </div>
            <?php echo __('My Subscribe') ?>
        </h3>
    </div>

    <div class="box-body">
    <?php 
     if(!empty($pages_user)){
    	foreach ($pages_user as $page){
            	$logo = $page['page_logo'];
        ?>
            <div class="col-xs-4 list-page" align="center">
                <a class="text-page" href="/Feed/page/<?php echo $page->id; ?>">
                    <div>
                        <?php echo $this->Html->image("/upload/image_page_logo/".$logo, [
                            "class" => "img-circle img-responsive",
                        ]); ?>
                    </div>
                    <div class="page-name">
                        <?php echo $page->page_name ?>
                    </div>
                </a>
            </div>
        <?php 
	}
	}?>
    
        <div class="col-xs-4 list-page" align="center">
            <div class="plus-page" data-toggle="modal" data-target="#plusModal">
                <i class = "fa fa-search"></i>
            </div>
            <div class="page-name">
                <?php echo __("Search");?>
            </div>
        </div>
    </div>
<!-- ///////////// My Subscribe End ///////////// -->

<!-- ///////////// My ModalSearch Start ///////////// -->
 
    <div class="modal fade" id="plusModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
          
        <div class="modal-dialog box-ads ">
        
            <div class=" row">
                <div class="col-xs-9 col-sm-9 col-md-9">
                    <div class="input-search">
                        </br>
                        <label class="label-text-sub"><?php echo __("ค้นหาจากชื่อ");?></label>
                        <!-- <input type="text" name="search" id="searchBox"> -->
                        <?php echo $this->Form->text('page_name', ['id' => 'searchBox', 'name' => 'search_text', 'div' => FALSE, 'label' => 'FALSE','maxlength'=>100]); ?>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <div class="row-btn" style="100px">
                        </br></br>
                <a href="#" id="search"><?php echo __('ค้นหา');?></a>
                    
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row" id="list-pages">
                    <?php
                    if(!empty($data)){
                        $page_count = 0; 
                    foreach ($pages_user as $page):
                            $logo = $page['page_logo'];
                    ?>
                        <div class="col-xs-4 list-page" align="center">
                            <a class="text-page" href="/articles/index/<?php echo $page->id; ?>">
                                <div>
                                    <?php echo $this->Html->image("/upload/image_page_logo/".$logo, [
                                        "class" => "img-circle img-responsive",
                                    ]); ?>
                                </div>
                                <div class="page-name-search">
                                    <?php echo $page->page_name ?>
                                </div>
                            </a>
                        </div>
                    <?php endforeach;} ?>
                </div>
            </div>
        
        </div>
    </div>
<!-- ///////////// My ModalSearch End ///////////// -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#search').on('click',function(){
            var pageSearch = $('#searchBox').val();
           // console.log(pageSearch);
            $.post('/Pages/pagesearch',{'page_name':pageSearch},function(result) {
            // data = jQuery.parseJSON(result);
            console.log(result);
             $('#list-pages').html(result);
            });
	    return false;
        });
	 $('.plus-page').on('click',function(){
	 
	 });
    });
</script>