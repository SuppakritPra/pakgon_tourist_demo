<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Page[]|\Cake\Collection\CollectionInterface $pages
 */
?>
<div class="articles index large-9 medium-8 columns content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Pages List') ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $this->Html->link($this->Html->icon('plus')." ".__('Newpages'), 
                        ['action' => 'add'], 
                        ['class' => 'btn btn-sm btn-success btn-rounded waves-effect waves-light pull-right', 'escape' => false]) 
                        ?>
                        <br> <br>
                        <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align:center;"><?php echo __('No.') ?></th>
                                    <th scope="col" style="text-align:center;"><?php echo __('namepage') ?></th>
                                    <th scope="col" style="text-align:center;"><?php echo __('citie') ?></th>
                                    <th scope="col" style="text-align:center;"><?php echo __('countrie') ?></th>
                                    <th scope="col" style="text-align:center;" width="70px"><?php echo __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; foreach ($pages as $index => $page): $i++; ?>
                                <tr >
                                 <td><?= h($i) ?></td>
                                 <td><?= h($page->page_name) ?></td>
                                 <td><?= h($page['cmci']['city_name']) ?></td>
                                 <td><?= h($page['cmco']['country_name']) ?></td>
                                 <td>
                                    <?php $this->Html->templates(['icon' => '<i class="fa fa-{{type}}{{attrs.class}}"{{attrs}}></i>']);?>
                                    <?php echo $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $page->id], ['class' => 'btn btn-xs btn-info btn-rounded waves-effect waves-light', 'escape' => false]) ?>
                                    <?php echo $this->Form->postLink($this->Html->icon('trash'), ['action' => 'delete', $page->id], ['class' => 'btn btn-xs btn-danger btn-rounded waves-effect waves-light confirmModal action-delete','escape'=>false]) ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</div>   
</div>