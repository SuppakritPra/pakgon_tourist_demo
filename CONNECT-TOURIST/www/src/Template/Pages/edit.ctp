<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Page $page
 */
?>
<div class="articles form large-9 medium-8 columns content">
<?php echo $this->Form->create(false, ['url' => ['controller'=> 'Pages','action' => 'edit'], 'id'=>'page_edit', 'type' => 'file']);?>
<?php #pr($Pages_array);die; ?>
<?php echo $this->Form->hidden('Pages_id', ['value'=> $Pages_array[0]['id']]);?>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Edit Pages') ?></h3>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <?php echo $this->Form->input('page_name', [
                            'id' => 'page_name', 
                            'class'=> 'required', 
                            'label'=> __('namepage'), 
                            'value'=> empty($Pages_array[0]['page_name'])? '':$Pages_array[0]['page_name']
                        ]);?>
                    </div>
                    <div class="form-group">
                        <?php echo __('Page Logo') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
                        <?php 
                            echo $this->Form->input('page_logo', array(
                                'label' => false,
                                'type' => 'file',
                                'accept' => 'image/jpeg,image/jpg',
                                'id' => 'page_logo',
                                'class' => 'required'
                            ));
                        ?>
                        <span for="page_logo" id="error-page-logo" generated="true" class="help-block">โปรดระบุ</span>
                        <?php foreach ($Pages as $Page): #pr($Page);die;
                            $img = $Page['page_logo'];
                        ?>
                            <?php if(!empty($Page['page_logo'])){
                                $this->append('scriptBottom');?>
                                <script> $('#page_logo').attr("disabled",true);</script>
                                <?php echo $this->Form->hidden('have_page_logo',['id' => 'have_page_logo', 'value' => 'Y']); ?>
                                <?php $this->end(); ?>
                                <div class="row row-Page" id="row_page_logo">
                                    <div class="col-xs-10"> 
                                        <?php echo $this->Html->image("/upload/image_page_logo/".$img, [
                                            "height" => "50px"
                                        ]);?>
                                    </div>
                                    <div class="col-xs-2"> 
                                        <?php
                                        echo $this->Form->button($this->Html->icon('trash'), [
                                            'class' => 'btn btn-sm btn-danger btn-rounded waves-effect waves-light action-delete', 
                                            'escape' => false,
                                            'type' => 'button',
                                            'data-Page_id' => $Page['id'],
                                            'data-type' => 'page_logo',
                                            'id' => 'page_logo_img'
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            <?php }else{
                                echo $this->Form->hidden('have_page_logo',['id' => 'have_page_logo', 'value' => 'N']);
                            } ?> 
                        <?php endforeach; ?>
                    </div>
                    <div class="form-group">
                        <?php echo __('Page Banner') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
                        <?php echo $this->Form->input('page_banner', array(
                                'label' => false,
                                'type' => 'file',
                                'accept' => 'image/jpeg,image/jpg',
                                'id' => 'page_banner',
                                'class' => 'required'
                                ));
                        ?>
                        <span for="page_banner"  id="error-page-banner" generated="true" class="help-block">โปรดระบุ</span>
                        <?php foreach ($Pages as $Page): #pr($Page);die;
                            $img = $Page['page_banner'];
                        ?>
                            <?php if(!empty($Page['page_banner'])){
                                $this->append('scriptBottom');?>
                                <script> $('#page_banner').attr("disabled",true); var have_page_banner = 'Y';</script>
                                <?php echo $this->Form->hidden('have_page_banner',['id' => 'have_page_banner', 'value' => 'Y']); ?>
                                <?php $this->end(); ?>
                                <div class="row row-Page" id="row_page_banner">
                                    <div class="col-xs-10"> 
                                        <?php echo $this->Html->image("/upload/image_page_banner/".$img, [
                                            "height" => "50px"
                                        ]);?>
                                    </div>
                                    <div class="col-xs-2">
                                        <?php
                                        echo $this->Form->button($this->Html->icon('trash'), [
                                            'class' => 'btn btn-sm btn-danger btn-rounded waves-effect waves-light action-delete', 
                                            'escape' => false,
                                            'type' => 'button',
                                            'data-Page_id' => $Page['id'],
                                            'data-type' => 'page_banner',
                                            'id' => 'page_banner_img'
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            <?php }else{
                                echo $this->Form->hidden('have_page_banner',['id' => 'have_page_banner', 'value' => 'N']);
                            } ?> 
                        <?php endforeach; ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Description'); ?></label>
                        <?php echo $this->Form->textarea('page_desc', [
                            'id' => 'page_desc', 
                            'class'=> 'required', 
                            'label'=>false, 
                            'placeholder'=>__('Description'),
                            'value' => empty($Pages_array[0]['page_desc'])? '':$Pages_array[0]['page_desc']
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->input(__('Update'), ['type'=>'submit', 'class'=>'btn btn-save', 'div'=>['class'=>'col-sm-2'], 'label'=>false]); ?>
    </div>
</div>
<?php echo $this->Form->end();?>
</div>
<?php $this->append('scriptBottom');?>
<script>
    $(document).ready(function() {
        $("#page_logo").on('change',function(){
            $('#have_page_logo').val('N');
            if($("#page_logo").val()){
                var name_page_logo = document.getElementById("page_logo").files;
                console.log(name_page_logo[0].size);
                if(name_page_logo[0].size > 10250000){
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์รูปภาพขนาดไม่เกิน 10 MB');
                    document.getElementById("page-logo-input").value = '';
                }else{
                    $('#have_page_logo').val('Y');
                }
                console.log($('#have_page_logo').val());
            }
        });
        $("#page_banner").on('change',function(){
            $('#have_page_banner').val('N');
            if($("#page_banner").val()){
                var name_page_banner = document.getElementById("page_banner").files;
                console.log(name_page_banner[0].size);
                if(name_page_banner[0].size > 10250000){
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์รูปภาพขนาดไม่เกิน 10 MB');
                    document.getElementById("page-banner-input").value = '';
                }else{
                    $('#have_page_banner').val('Y');
                }
                console.log($('#have_page_logo').val());
            }
        });


        $('.help-block').hide();
        $('#page_logo').on('change', function(){
            var pl = $('#page_logo').val();
            if(pl!=''){
                $('#page_logo').parent(".form-group").removeClass('has-error');
                    $('#page_logo').parent(".form-group").parent(".form-group").removeClass('has-error');
                    $('#error-page-logo').hide();
            }
        });
        $('#page_banner').on('change', function(){
            var pl = $('#page_banner').val();
            if(pl!=''){
                $('#page_banner').parent( ".form-group" ).removeClass('has-error');
                $('#page_banner').parent(".form-group").parent(".form-group").removeClass('has-error');
                $('#error-page-banner').hide();
            }
        });

        $('#page_edit').submit(function(event) {
            var pl = $('#have_page_logo').val();
            if(pl == 'N'){
                //has-error
                $('#page_logo').parent(".form-group").addClass('has-error');
                $('#page_logo').parent(".form-group").parent(".form-group").addClass('has-error');
                $('#error-page-logo').show();
            }
            var pb = $('#have_page_banner').val();
            if(pb=='N'){
                //has-error
                $('#page_banner').parent( ".form-group" ).addClass('has-error');
                $('#page_banner').parent( ".form-group" ).parent( ".form-group" ).addClass('has-error');
                $( '#error-page-banner').show();
            }
            if(pl == 'N' || pb == 'N'){
                return false;
            }
        });

        var id='';
        $('.action-delete').on('click', function () {
            id = "#"+$(this).attr('id');
            $('#ConfirmModal').modal('show');
            $('#ConfirmModalBodyText').text('กรุณายืนยันการทำงาน ?');
            return false;
        });

        $('#btnConfirm').on('click', function () {
            console.log(id);
            var Page  = $(id).data('page_id');
            var type  = $(id).data('type');
            $.post('/pages/DeleteImgPages',{'data':Page,'type':type}).done(function(result) {
                console.log(result);
                // bin_this.remove();
                $('#'+result).removeAttr("disabled");
                $('#row_'+result).remove('');
                if(result == 'page_logo'){
                    console.log('yyyy');
                    $('#have_page_logo').val('N');
                }else if(result == 'page_banner'){
                    console.log('nnnn');
                    $('#have_page_banner').val('N');
                }
                console.log($('#have_page_logo').val());
                console.log($('#have_page_banner').val());
            });
        });
    });
</script>
<?php $this->end();?>
