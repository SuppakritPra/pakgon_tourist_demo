<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Page $page
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Page'), ['action' => 'edit', $page->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Page'), ['action' => 'delete', $page->id], ['confirm' => __('Are you sure you want to delete # {0}?', $page->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Page'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cities'), ['controller' => 'Cities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New City'), ['controller' => 'Cities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Page Admin'), ['controller' => 'PageAdmin', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Page Admin'), ['controller' => 'PageAdmin', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Page Entities'), ['controller' => 'PageEntities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Page Entity'), ['controller' => 'PageEntities', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pages view large-9 medium-8 columns content">
    <h3><?= h($page->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Page Name') ?></th>
            <td><?= h($page->page_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Page Status') ?></th>
            <td><?= h($page->page_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Page Type') ?></th>
            <td><?= h($page->page_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= $page->has('city') ? $this->Html->link($page->city->id, ['controller' => 'Cities', 'action' => 'view', $page->city->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= $page->has('country') ? $this->Html->link($page->country->id, ['controller' => 'Countries', 'action' => 'view', $page->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Page Logo') ?></th>
            <td><?= h($page->page_logo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Page Banner') ?></th>
            <td><?= h($page->page_banner) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Page Desc') ?></th>
            <td><?= h($page->page_desc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($page->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create Uid') ?></th>
            <td><?= $this->Number->format($page->create_uid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Update Uid') ?></th>
            <td><?= $this->Number->format($page->update_uid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($page->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($page->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Articles') ?></h4>
        <?php if (!empty($page->articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Article Title') ?></th>
                <th scope="col"><?= __('Article Intro') ?></th>
                <th scope="col"><?= __('Article Detail') ?></th>
                <th scope="col"><?= __('Lang Code') ?></th>
                <th scope="col"><?= __('Publish Date') ?></th>
                <th scope="col"><?= __('Expire Date') ?></th>
                <th scope="col"><?= __('Feed Flag') ?></th>
                <th scope="col"><?= __('Tags') ?></th>
                <th scope="col"><?= __('Beacon Uid') ?></th>
                <th scope="col"><?= __('Qr Uid') ?></th>
                <th scope="col"><?= __('Count Like') ?></th>
                <th scope="col"><?= __('Page Id') ?></th>
                <th scope="col"><?= __('Is Noti') ?></th>
                <th scope="col"><?= __('Create Uid') ?></th>
                <th scope="col"><?= __('Update Uid') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($page->articles as $articles): ?>
            <tr>
                <td><?= h($articles->id) ?></td>
                <td><?= h($articles->article_title) ?></td>
                <td><?= h($articles->article_intro) ?></td>
                <td><?= h($articles->article_detail) ?></td>
                <td><?= h($articles->lang_code) ?></td>
                <td><?= h($articles->publish_date) ?></td>
                <td><?= h($articles->expire_date) ?></td>
                <td><?= h($articles->feed_flag) ?></td>
                <td><?= h($articles->tags) ?></td>
                <td><?= h($articles->beacon_uid) ?></td>
                <td><?= h($articles->qr_uid) ?></td>
                <td><?= h($articles->count_like) ?></td>
                <td><?= h($articles->page_id) ?></td>
                <td><?= h($articles->is_noti) ?></td>
                <td><?= h($articles->create_uid) ?></td>
                <td><?= h($articles->update_uid) ?></td>
                <td><?= h($articles->created) ?></td>
                <td><?= h($articles->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articles', 'action' => 'view', $articles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articles', 'action' => 'edit', $articles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articles', 'action' => 'delete', $articles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Page Admin') ?></h4>
        <?php if (!empty($page->page_admin)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Page Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Can Delete Other Admin') ?></th>
                <th scope="col"><?= __('Create Uid') ?></th>
                <th scope="col"><?= __('Update Uid') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($page->page_admin as $pageAdmin): ?>
            <tr>
                <td><?= h($pageAdmin->id) ?></td>
                <td><?= h($pageAdmin->page_id) ?></td>
                <td><?= h($pageAdmin->user_id) ?></td>
                <td><?= h($pageAdmin->can_delete_other_admin) ?></td>
                <td><?= h($pageAdmin->create_uid) ?></td>
                <td><?= h($pageAdmin->update_uid) ?></td>
                <td><?= h($pageAdmin->created) ?></td>
                <td><?= h($pageAdmin->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PageAdmin', 'action' => 'view', $pageAdmin->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PageAdmin', 'action' => 'edit', $pageAdmin->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PageAdmin', 'action' => 'delete', $pageAdmin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pageAdmin->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Page Entities') ?></h4>
        <?php if (!empty($page->page_entities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Page Id') ?></th>
                <th scope="col"><?= __('Entity Code') ?></th>
                <th scope="col"><?= __('Create Uid') ?></th>
                <th scope="col"><?= __('Update Uid') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($page->page_entities as $pageEntities): ?>
            <tr>
                <td><?= h($pageEntities->id) ?></td>
                <td><?= h($pageEntities->page_id) ?></td>
                <td><?= h($pageEntities->entity_code) ?></td>
                <td><?= h($pageEntities->create_uid) ?></td>
                <td><?= h($pageEntities->update_uid) ?></td>
                <td><?= h($pageEntities->created) ?></td>
                <td><?= h($pageEntities->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PageEntities', 'action' => 'view', $pageEntities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PageEntities', 'action' => 'edit', $pageEntities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PageEntities', 'action' => 'delete', $pageEntities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pageEntities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
