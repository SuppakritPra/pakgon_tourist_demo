<?php

 /**
  *
  * The template view for view as of banks controller the page show for banks information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Bank $bank
  * @since  2018-04-20 18:19:50
  * @license Pakgon.Ltd
  */
?>
<div class="banks view banks-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Bank Management System => ({0} information)', h($bank->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Banks Name'); ?></td>
                <td class="table-view-detail"><?php echo h($bank->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Banks Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($bank->name_eng); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($bank->status, true); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Ref3'); ?></td>
                <td class="table-view-detail"><?php echo h($bank->ref3); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Ref4'); ?></td>
                <td class="table-view-detail"><?php echo h($bank->ref4); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Banks Id'); ?></td>
                <td class="table-view-detail"><?php echo h($bank->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Order No'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($bank->order_no); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Ref1'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($bank->ref1); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Ref2'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($bank->ref2); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($bank->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($bank->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($bank->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($bank->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
