<?php
/**
  * The template index for index as of banks controller the page show for short banks information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Bank[]|\Cake\Collection\CollectionInterface $banks
  * @since  2018-04-20 18:19:50
  * @license Pakgon.Ltd
  */
?>
<div class="banks index">
    
    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Bank Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('banks'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Bank'), '/banks/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Bank'), '/banks/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->

    
        <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Banks list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('Row no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name',__('Banks Name')); ?></th>
                                            <th><?php echo $this->Paginator->sort('name_eng',__('Banks Name Eng')); ?></th>
                                            <th><?php echo $this->Paginator->sort('order_no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('status'); ?></th>
                                            <th><?php echo $this->Paginator->sort('ref1'); ?></th>
                                            <th><?php echo $this->Paginator->sort('ref2'); ?></th>
                                            <th><?php echo $this->Paginator->sort('ref3'); ?></th>
                                            <th><?php echo $this->Paginator->sort('ref4'); ?></th>
                                            <th><?php echo $this->Paginator->sort('create_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('update_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($banks)): ?>
                    <?php foreach ($banks as $index => $bank): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                                                                                    <td><?php echo h($bank->name); ?></td>
                                                                                                                <td><?php echo h($bank->name_eng); ?></td>
                                                                                                                <td><?php echo $this->Number->format($bank->order_no); ?></td>
                                                                                                                <td><?php echo $this->Utility->getMainStatus($bank->status, true); ?></td>
                                                                                                                <td><?php echo $this->Number->format($bank->ref1); ?></td>
                                                                                                                <td><?php echo $this->Number->format($bank->ref2); ?></td>
                                                                                                                <td><?php echo h($bank->ref3); ?></td>
                                                                                                                <td><?php echo h($bank->ref4); ?></td>
                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($bank->create_uid); ?></td>
                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($bank->update_uid); ?></td>
                                                                                                                <td><?php echo $this->Utility->dateTimeISO($bank->created); ?></td>
                                                                                                                <td><?php echo $this->Utility->dateTimeISO($bank->modified); ?></td>
                                                                    <td class="actions">
                            <?php echo $this->Permission->getActions($bank->id); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="14"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
         </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
