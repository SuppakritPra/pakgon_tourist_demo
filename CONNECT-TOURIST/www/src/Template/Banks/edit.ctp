<?php
/**
  * 
  * edit banks template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Bank $bank
  * @since   2018/04/20 18:19:50
  * @license pakgon.Ltd.
  */
?>
<div class="banks banks-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Bank Management System => ( Add Bank )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($bank, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('order_no');
            echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]);
            echo $this->Form->control('ref1');
            echo $this->Form->control('ref2');
            echo $this->Form->control('ref3');
            echo $this->Form->control('ref4');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Edit'), '/Banks/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for edit bank ?'), 'data-confirm-title' => __('Confirm for edit bank ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
