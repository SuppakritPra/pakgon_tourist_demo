<?php
/**
  * 
  * add zipcodes template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Zipcode $zipcode
  * @since   2018/04/20 18:22:41
  * @license pakgon.Ltd.
  */
?>
<div class="zipcodes zipcodes-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Zipcode Management System => ( Add Zipcode )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($zipcode, ['horizontal' => true]);
            echo $this->Form->control('district_id', ['options' => $districts]);
            echo $this->Form->control('zipcode');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/Zipcodes/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add zipcode ?'), 'data-confirm-title' => __('Confirm for add zipcode ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
