<?php
/**
  * 
  * edit zipcodes template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Zipcode $zipcode
  * @since   2018/04/20 18:22:41
  * @license pakgon.Ltd.
  */
?>
<div class="zipcodes zipcodes-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Zipcode Management System => ( Add Zipcode )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($zipcode, ['horizontal' => true]);
            echo $this->Form->control('district_id', ['options' => $districts]);
            echo $this->Form->control('zipcode');
            echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Edit'), '/Zipcodes/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for edit zipcode ?'), 'data-confirm-title' => __('Confirm for edit zipcode ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
