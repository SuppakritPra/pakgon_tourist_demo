<?php
/**
  * The template index for index as of zipcodes controller the page show for short zipcodes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Zipcode[]|\Cake\Collection\CollectionInterface $zipcodes
  * @since  2018-04-20 18:22:40
  * @license Pakgon.Ltd
  */
?>
<div class="zipcodes index">
    
    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Zipcode Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('zipcodes'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Zipcode'), '/zipcodes/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Zipcode'), '/zipcodes/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->

    
        <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Zipcodes list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('Row no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('district_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('zipcode'); ?></th>
                                            <th><?php echo $this->Paginator->sort('status'); ?></th>
                                            <th><?php echo $this->Paginator->sort('create_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('update_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($zipcodes)): ?>
                    <?php foreach ($zipcodes as $index => $zipcode): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                                                                        <td><?php echo $zipcode->has('district') ? $this->Html->link($zipcode->district->name, ['controller' => 'Districts', 'action' => 'view', $zipcode->district->id]) : ''; ?></td>
                                                                                                                                    <td><?php echo h($zipcode->zipcode); ?></td>
                                                                                                                                <td><?php echo $this->Utility->getMainStatus($zipcode->status, true); ?></td>
                                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($zipcode->create_uid); ?></td>
                                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($zipcode->update_uid); ?></td>
                                                                                                                                <td><?php echo $this->Utility->dateTimeISO($zipcode->created); ?></td>
                                                                                                                                <td><?php echo $this->Utility->dateTimeISO($zipcode->modified); ?></td>
                                                                    <td class="actions">
                            <?php echo $this->Permission->getActions($zipcode->id); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="9"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
         </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
