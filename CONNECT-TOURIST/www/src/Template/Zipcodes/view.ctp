<?php

 /**
  *
  * The template view for view as of zipcodes controller the page show for zipcodes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Zipcode $zipcode
  * @since  2018-04-20 18:22:41
  * @license Pakgon.Ltd
  */
?>
<div class="zipcodes view zipcodes-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Zipcode Management System => ({0} information)', h($zipcode->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                            <tr>
                <td class="table-view-label"><?php echo __('District'); ?></td>
                <td class="table-view-detail"><?php echo $zipcode->has('district') ? $this->Html->link($zipcode->district->name, ['controller' => 'Districts', 'action' => 'view', $zipcode->district->id]) : ''; ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Zipcode'); ?></td>
                <td class="table-view-detail"><?php echo h($zipcode->zipcode); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($zipcode->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Zipcodes Id'); ?></td>
                <td class="table-view-detail"><?php echo h($zipcode->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($zipcode->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($zipcode->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($zipcode->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($zipcode->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
