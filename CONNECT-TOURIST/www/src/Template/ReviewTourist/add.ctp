<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReviewTourist $reviewTourist
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Review Tourist'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="reviewTourist form large-9 medium-8 columns content">
    <?= $this->Form->create($reviewTourist) ?>
    <fieldset>
        <legend><?= __('Add Review Tourist') ?></legend>
        <?php
            echo $this->Form->control('review');
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
