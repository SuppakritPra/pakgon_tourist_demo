<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReviewTourist[]|\Cake\Collection\CollectionInterface $reviewTourist
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Review Tourist'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="reviewTourist index large-9 medium-8 columns content">
    <h3><?= __('Review Tourist') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('review') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reviewTourist as $reviewTourist): ?>
            <tr>
                <td><?= $this->Number->format($reviewTourist->id) ?></td>
                <td><?= h($reviewTourist->review) ?></td>
                <td><?= $reviewTourist->has('user') ? $this->Html->link($reviewTourist->user->id, ['controller' => 'Users', 'action' => 'view', $reviewTourist->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $reviewTourist->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $reviewTourist->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $reviewTourist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reviewTourist->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
