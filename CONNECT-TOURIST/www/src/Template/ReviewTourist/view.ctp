<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReviewTourist $reviewTourist
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Review Tourist'), ['action' => 'edit', $reviewTourist->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Review Tourist'), ['action' => 'delete', $reviewTourist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reviewTourist->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Review Tourist'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Review Tourist'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="reviewTourist view large-9 medium-8 columns content">
    <h3><?= h($reviewTourist->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Review') ?></th>
            <td><?= h($reviewTourist->review) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $reviewTourist->has('user') ? $this->Html->link($reviewTourist->user->id, ['controller' => 'Users', 'action' => 'view', $reviewTourist->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($reviewTourist->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tourist Attraction') ?></h4>
        <?php if (!empty($reviewTourist->tourist_attraction)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Path Picture Tourist Attraction') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Review Tourist Id') ?></th>
                <th scope="col"><?= __('Detail Tourist') ?></th>
                <th scope="col"><?= __('Path Icon Marker') ?></th>
                <th scope="col"><?= __('Latitude') ?></th>
                <th scope="col"><?= __('Longtitude') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($reviewTourist->tourist_attraction as $touristAttraction): ?>
            <tr>
                <td><?= h($touristAttraction->id) ?></td>
                <td><?= h($touristAttraction->name) ?></td>
                <td><?= h($touristAttraction->category_id) ?></td>
                <td><?= h($touristAttraction->path_picture_tourist_attraction) ?></td>
                <td><?= h($touristAttraction->province_id) ?></td>
                <td><?= h($touristAttraction->review_tourist_id) ?></td>
                <td><?= h($touristAttraction->detail_tourist) ?></td>
                <td><?= h($touristAttraction->path_icon_marker) ?></td>
                <td><?= h($touristAttraction->latitude) ?></td>
                <td><?= h($touristAttraction->longtitude) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TouristAttraction', 'action' => 'view', $touristAttraction->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TouristAttraction', 'action' => 'edit', $touristAttraction->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TouristAttraction', 'action' => 'delete', $touristAttraction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $touristAttraction->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
