<?php

use Cake\I18n\Time;
$this->layout = 'PakgonConnect.blank';
if (!empty($Feeds)) {

    foreach ($Feeds as $feed) {
        ?>
        <div class="article-list">
            <!-- Post -->
            <div class="post">
                <div class="user-block">
                    <?php
                    $link = $this->Html->image('/upload/image_page_logo/' . $feed->page_logo);
                    $link .= '<span class="username">' . $feed->page_name . ' <span>';
                    echo $this->Html->link(
                            $link, ['controller' => 'feed', 'action' => 'page', $feed->page_id, '_full' => true], ['escape' => false]
                    );
                    ?>
                    <?php //echo $this->Html->image('/upload/image_page_logo/'.$feed->page_logo, ['class'=>'']); ?>
                          <!-- <span class="username">
                    <?php //echo $feed->page_name; ?>
                          <span> -->
                    <?php if (@$arrLike[$feed->id]) { ?>
                        <a class="pull-right like-true"> <i class="fa fa-thumbs-up margin-r-5"></i> Like </a>
                    <?php } else { ?> 
                        <a  class="pull-right btn-box-tool btn-like" data-article_id='<?php echo $feed->id; ?>'><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                    <?php } ?>
                    </span>
                    </span>
                    <span class="description">
        <?php
        $now = new Time($feed->publish_date);
        $publishDate = $now->i18nFormat('yyyy-MM-dd  HH:mm');
        echo substr($this->DateFormat->formatDateTimeThai($publishDate), 0, -3);
        ?>                      
                    </span>
                </div>
                <!-- /.user-block -->
                <p>
        <?php echo $feed->article_intro; ?>
                </p>
                <a href = "/feed/view/<?php echo $feed->id; ?>">
                    <div class="article-detail">
                        <div class="article-img"> <?php echo $this->Html->image('/upload/image_title/' . $feed->path, ['class' => 'img-responsive']); ?></div>
                        <span class="article-title"><?php echo $feed->article_title; ?></span>
                        <span class="text ellipsis">
                            <p class="concat">
        <?php echo $feed->article_intro; ?>
                            </p>
                        </span>
                    </div>
                </a>
            </div>
            <!-- /.post -->
            <div class="clearfix"></div>
        </div> 
        <!-- /.article-list -->
    <?php
    }
}?>