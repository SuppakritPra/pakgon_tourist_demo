<?php

use Cake\I18n\Time;

$this->layout = 'Ablepro6.mobile_slime';
$hasAudio = (array_key_exists('audio', $arrArticleAsset) && !empty($arrArticleAsset['audio']));
$hasVideo = (array_key_exists('video', $arrArticleAsset) && !empty($arrArticleAsset['video']));
?>
<div class="row">
    <div class="col">
        <div class="card bg-white p-relative">
            <div class="card-block post-timelines">
                <div class="media-middle friend-box float-left">
                    <a href="/feed/page/<?php echo $Article->page->id; ?>">
                        <?php echo $this->Html->image($Article->page->page_logo, ['class' => 'media-object rounded-circle page-logo img-page-logo']); ?>
                    </a>
                </div>

                <div class="friend-details pull-right">
                    <div class="col">
                        <?php if (@$arrLike[$Article->id]): ?>
                            <i class="fa fa-lg fa-thumbs-up like-true"></i>
                        <?php else: ?>
                            <a class="btn-like" data-article_id='<?php echo $Article->id; ?>'><i class="fa fa-lg fa-thumbs-o-up"></i></a>
                        <?php endif; ?>
                        <div class="count-like text-bold pull-right">
                            <?php echo ($Article->count_like < 1000) ? $Article->count_like : round($Article->count_like / 1000, 1) . " K"; ?>
                        </div>
                    </div>
                </div>

                <div class="chat-header text-page-name">
                    <?php echo $this->Utility->displayTitle($Article->page->page_name); ?>
                </div>
                <div class="text-muted social-time text-time-ago">
                    <?php
                    $now = new Time($Article->publish_date);
                    $publishDate = $now->i18nFormat('yyyy-MM-dd  HH:mm');
                    echo $this->Utility->displayTimeElapsed($publishDate);
                    ?>
                </div>
            </div>

            <div class="card-block">
                <img src="/upload/image_title/<?php echo @$arrArticleAsset['image_title'][0]['path']; ?>" class="img-fluid w-100" alt="<?php echo $Article->article_title; ?>">
            </div>

            <div class="card-block p-b-0">
                <div class="timeline-details">
                    <div class="text-article-title"><?php echo $Article->article_title; ?></div>
                </div>

                <div class="gap-article"></div>
                <div class="row timeline-details m-l-0">
                    <div class="col-9"></div>
                    <?php if ($hasAudio): ?>
                        <div class="col-1 audio-player">
                            <i class="fa fa-lg fa-microphone btn-article-asset" data-id="1" data-play="0"></i>
                        </div>
                    <?php endif; ?>

                    <?php if ($hasVideo): ?>
                        <div class="col-1 video-player">
                            <i class="fa fa-lg fa-play-circle btn-article-asset" data-id="2" data-play="0"></i>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="row">
                    <?php if ($hasAudio): ?>
                        <div class="col">
                            <div class="article-asset-file audio-asset">
                                <audio width="100%" controls class="hide-article-asset" id="1" style="display: none;">
                                    <source src="<?php echo '/upload/audio/' . $arrArticleAsset['audio'][0]['path']; ?>" type="audio/ogg">
                                    <source src="<?php echo '/upload/audio/' . $arrArticleAsset['audio'][0]['path']; ?>" type="audio/mpeg">
                                    <?php echo __('Your browser does not support.'); ?>
                                </audio>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($hasVideo): ?>
                        <div class="col">
                            <div class="article-asset-file video-asset">
                                <video width="100%" controls class="hide-article-asset"  id="2" style="display: none;">
                                    <source src="<?php echo '/upload/video/' . $arrArticleAsset['video'][0]['path']; ?>" type="video/mp4">
                                    <source src="<?php echo '/upload/video/' . $arrArticleAsset['video'][0]['path']; ?>" type="video/ogg">
                                    <?php echo __('Your browser does not support.') ?>
                                </video>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="card-block">
                <p class="text-article-detail"><?php echo $Article->article_detail; ?></p>
            </div>
        </div>
    </div>
</div>

<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">
    /**
     * 
     * Click like action 
     * @author sarawutt.b
     */
    $('.btn-like').click(function () {
        var $elClick = $(this);
        var $elCountLike = $elClick.closest('.col').find('.count-like');
        $.post('/feed/addlike.json', {article_id: $elClick.data('article_id')}, function (result) {
            console.log(result.response.status);
            if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) {
                $elClick.find('i').attr('class', 'fa fa-lg fa-thumbs-up');
                $elClick.attr('class', 'like-true');
                var countLike = parseInt($elCountLike.text()) || 0;
                $elCountLike.text(countLike + 1);
            }
        });
    });

    $(function () {
        //Find image incontent and add class to bootstrap responsive
        //$('.hide-article-asset').hide();
        $('img:not(.simple)').addClass('img-fluid');
        $('.btn-article-asset').on('click', function () {
            var xclass = ($(this).attr('class') === 'fa fa-lg fa-microphone btn-article-asset') ? 'fa fa-lg fa-microphone-slash btn-article-asset' : 'fa fa-lg fa-stop btn-article-asset';
            var play = $(this).data('play');
            var id = $(this).data('id');
            if (!play) {
                $(this).attr('class', xclass);
                $(this).data('play', 1);
                $('#' + id).show();
                var obj = document.getElementById(id);
                obj.play();
            } else {
                $('#' + id).hide();
                var yclass = ($(this).attr('class') === 'fa fa-lg fa-microphone-slash btn-article-asset') ? 'fa fa-lg fa-microphone btn-article-asset' : 'fa fa-lg fa-play-circle btn-article-asset';
                var obj = document.getElementById(id);
                obj.pause();
                $(this).attr('class', yclass);
                $(this).data('play', 0);
            }
        });
    });
</script>
<?php $this->end(); ?>