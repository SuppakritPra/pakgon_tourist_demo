<?php $this->layout = 'Ablepro6.mobile_slime'; ?>
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-xl-2 col-md-4 col-sm-6">
            <div class="preloader3 loader-block">
                <div class="circ1"></div>
                <div class="circ2"></div>
                <div class="circ3"></div>
                <div class="circ4"></div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('utility/geolocation'); ?>