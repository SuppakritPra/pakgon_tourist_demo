<?php

use Cake\I18n\Time;
use Cake\Core\Configure;

$this->layout = 'Ablepro6.mobile_slime';

echo $this->Html->css([
    'Ablepro6./plugins/OwlCarousel2/dist/assets/owl.carousel',
    'Ablepro6./plugins/OwlCarousel2/dist/assets/owl.theme.default.min',
]);
echo $this->Html->script([
    'Ablepro6./plugins/OwlCarousel2/dist/owl.carousel.min',
    '/vendor/jquery-match-height/dist/jquery.matchHeight-min'
]);
?>
<div class="container h-100" id="preloaderGeolocation">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-xl-2 col-md-4 col-sm-6">
            <div class="preloader3 loader-block">
                <div class="circ1"></div>
                <div class="circ2"></div>
                <div class="circ3"></div>
                <div class="circ4"></div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('utility/geolocation'); ?>
<div id="homeFeed" class="container">
    <!--Show home feed content-->
</div>

<script type="text/javascript">
    toasNotification('xxxxx');
</script>