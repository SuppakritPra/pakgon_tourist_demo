<?php

use Cake\I18n\Time;
use Cake\Core\Configure;

$this->layout = 'Ablepro6.mobile_slime';

echo $this->Html->css([
    'Ablepro6./plugins/OwlCarousel2/dist/assets/owl.carousel',
    'Ablepro6./plugins/OwlCarousel2/dist/assets/owl.theme.default.min',
]);
echo $this->Html->script([
    'Ablepro6./plugins/OwlCarousel2/dist/owl.carousel.min',
    '/vendor/jquery-match-height/dist/jquery.matchHeight-min'
]);
?>
<div class="row">
    <div class="col">
        <h3 class="text-bold show-geo-current-province"><?php echo $this->request->session()->read('currentProvince'); ?></h3>
    </div>
</div>
<?php echo $this->Bootstrap->gap(); ?>
<div class="row">
    <div class="owl-carousel owl-theme">
        <?php if (!$geoLocationPages->isEmpty()): ?>
            <?php foreach ($geoLocationPages as $k => $v): ?>
                <div class="col">
                    <div class="card card-trans">
                        <div class="card-img-top">
                            <a href="/Feed/page/<?php echo $v->id; ?>">
                                <?php echo $this->Html->image($v->page_card, ['class' => 'img-fluid', 'title' => $v->page_desc]); ?> 
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <!-- not founded result -->
        <?php endif; ?>
    </div> 
</div>

<?php if (!empty($hotDeals)): ?>
    <br/>
    <div class="row">
        <div class="col">
            <h3 class="text-bold pull-left"><?php echo __('Hot Deals'); ?></h3>
            <?php echo $this->Bootstrap->displayBarMenus($hotDealViewAllLink); ?>
        </div>
    </div>
    <?php echo $this->Bootstrap->gap(); ?>
    <div class="row">
        <div class="owl-carousel owl-theme">

            <?php foreach ($hotDeals as $k => $v): ?>
                <div class="col">
                    <div class="card card-trans">
                        <div class="card-img-top">
                            <a href="/Feed/view/<?php echo $v->id; ?>">
                                <?php echo $this->Html->image('/upload/image_title/' . $v->path, ['class' => 'img-fluid', 'title' => $v->article_title]); ?> 
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div> 
    </div>
<?php else: ?>
    <!-- not founded result -->
<?php endif; ?>


<?php if (!empty($feeds)): ?>
    <br/>
    <div class="row">
        <div class="col">
            <h3 class="text-bold pull-left"><?php echo __('Informations'); ?></h3>
            <?php echo $this->Bootstrap->displayBarMenus('/Feed/myFeed'); ?>
        </div>
    </div>
    <?php echo $this->Bootstrap->gap(); ?>
    <div class="row">
        <div class="owl-carousel owl-theme">

            <?php foreach ($feeds as $k => $v): ?>
                <div class="col">
                    <div class="card user-box assign-user rounded">
                        <a href="/Feed/view/<?php echo $v->id; ?>">
                            <?php echo $this->Html->image('/upload/image_title/' . $v->path, ['class' => 'img-fluid rounded', 'alt' => $v->article_intro . ' title image']); ?>
                        </a>
                        <div class="card-block post-timelines">
                            <div class="row">
                                <div class="col-3 media-middle photo-table p-l-5 m-l-5">
                                    <a href="/Feed/page/<?php echo $v->page_id; ?>">
                                        <?php echo $this->Html->image($v->page_logo, ['class' => 'media-object rounded-circle page-logo']); ?>
                                    </a>
                                </div>
                                <div class="col-9 media-body p-l-0">
                                    <a href="/Feed/view/<?php echo $v->id; ?>">
                                        <div class="f-15 chat-header text-bold">
                                            <?php echo trim($v->article_title); ?>
                                        </div>
                                    </a>
                                    <div class="text-muted social-time m-t-5">
                                        <?php
                                        $now = new Time($v->publish_date);
                                        $publishDate = $now->i18nFormat('yyyy-MM-dd  HH:mm');
                                        echo $this->Utility->displayTimeElapsed($publishDate);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <a href = "/Feed/view/<?php echo $v->id; ?>">
                                <h6 class="f-15 mb-2"><?php echo trim($v->article_intro); ?></h6>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php else: ?>
    <!-- not founded result -->
<?php endif; ?>

<?php if ($this->Utility->isEducations()): ?>
    <br/>
    <div class="row">
        <div class="col">
            <h3 class="text-bold text-left"><?php echo __('Educations'); ?></h3>
        </div>
    </div>
    <?php echo $this->Bootstrap->gap(); ?>
    <div class="row">
        <div class="owl-carousel owl-theme">
            <div class="col">
                <div class="card card-trans">
                    <div class="card-img-top">
                        <a href="<?php echo Configure::read('URL_LINK.FRONT_EDU'); ?>">
                            <?php echo $this->Html->image('/upload/img/card/999-bl.png', ['class' => 'img-fluid']); ?> 
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
<?php endif; ?>
<?php echo $this->Bootstrap->printBr(7); ?>
<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">
    $('.card').matchHeight();
    $('.owl-carousel').owlCarousel({
        dots: true,
        loop: false,
        margin: 10,
        nav: false,
        lazyLoad: true,
        center: false,
        responsive: {
            0: {
                //dotsEach: 5,
                items: 1
            },
            600: {
                //dotsEach: 3,
                items: 1
            },
            900: {
                //dotsEach: 3,
                items: 2
            },
            1200: {
                //dotsEach: 1,
                items: 3
            }
        }
    });

    $('.owl-dots.disabled').attr('class', 'owl-dots');
</script>
<?php $this->end(); ?>