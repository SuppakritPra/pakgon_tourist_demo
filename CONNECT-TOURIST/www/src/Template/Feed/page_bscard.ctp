<?php

use Cake\I18n\Time;

$this->layout = 'Ablepro6.mobile_slime';
?>
<div class="row">
    <div class="col">
        <div class="card text-center">
            <?php echo $this->Html->image($banner, ['class' => 'img-fluid h-50']); ?>
            <div class="card-block widget-user">
                <div class="user-block-1">
                    <?php echo $this->Html->image($pageLogo, ['class' => 'img-fluid img-page-logo']); ?>
                </div>
                <div class="user-name text-page-name">
                    <h5 class="text-bold"><?php echo $this->Utility->displayTitle($pageName); ?></h5>
                    <h5 class="txt-muted"><?php echo __('Following <span class="count-subscribe">{0}</span> Persons', $countFollowPage); ?></h5>
                </div>

                <div class="p-10"></div>
                <div class="row">
                    <?php if ($admin_page == 'Y'): ?>
                        <div class="col-12 text-left p-b-10">
                            <?php echo $this->Permission->button(__('Page Administrator'), "/Articles/index/{$pageId}", ['class' => 'btn btn-warning waves-effect waves-light btn-block']); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (strtolower($pageName) !== 'connect'): ?>
                        <div class="col-12 text-right">
                            <input type="hidden" name="page-id" value="<?php echo $pageId ?>" id="page_id">
                            <?php echo (!$isSubscribed) ? $this->Permission->button(__('Unsubscribe'), null, ['class' => 'btn btn-danger waves-effect waves-light btn-block', 'id' => 'subscribe']) : $this->Permission->button(__('Subscribe'), null, ['class' => 'btn btn-primary waves-effect waves-light btn-block', 'id' => 'subscribe']); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="p-10"></div>


<div class="row">
    <?php if (!empty($Feeds)): ?>
        <div class="col">
            <?php foreach ($Feeds as $feed): ?>
                <div class="card bg-white p-relative">
                    <div class="card-block post-timelines">
                        <div class="media-middle friend-box float-left">
                            <a href="/Feed/page/<?php echo $feed->page_id; ?>">
                                <?php echo $this->Html->image($feed->page_logo, ['class' => 'media-object rounded-circle page-logo img-page-logo']); ?>
                            </a>
                        </div>

                        <div class="friend-details pull-right">
                            <div class="col">
                                <?php if (@$arrLike[$feed->id]): ?>
                                    <i class="fa fa-lg fa-thumbs-up like-true"></i>
                                <?php else: ?>
                                    <a class="btn-like" data-article_id='<?php echo $feed->id; ?>'><i class="fa fa-lg fa-thumbs-o-up"></i></a>
                                <?php endif; ?>
                                <div class="count-like text-bold pull-right">
                                    <?php echo ($feed->count_like < 1000) ? $feed->count_like : round($feed->count_like / 1000, 1) . " K"; ?>
                                </div>
                            </div>
                        </div>

                        <div class="chat-header text-page-name">
                            <?php echo $this->Utility->displayTitle($feed->page_name); ?>
                        </div>

                        <div class="text-muted social-time text-time-ago">
                            <?php
                            $now = new Time($feed->publish_date);
                            $publishDate = $now->i18nFormat('yyyy-MM-dd  HH:mm');
                            //echo substr($this->DateFormat->formatDateTimeThai($publishDate), 0, -3);
                            echo $this->Utility->displayTimeElapsed($publishDate);
                            ?>
                        </div>
                    </div>

                    <div class="card-block">
                        <a href="/Feed/view/<?php echo $feed->id; ?>">
                            <img src="/upload/image_title/<?php echo $feed->path; ?>" class="img-fluid w-100 article-img-title" alt="<?php echo $feed->article_title; ?>">
                        </a>
                    </div>

                    <div class="card-block">
                        <a href = "/Feed/view/<?php echo $feed->id; ?>">
                            <div class="timeline-details">
                                <div class="text-article-title"><?php echo $feed->article_title; ?></div>
                                <div class="gap-article"></div>
                                <div class="text-article-intro"><?php echo $feed->article_intro; ?></div>
                            </div>
                        </a>
                    </div>
                </div>    
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">
    $(function () {
        /**
         * 
         * Click like action 
         * @author sarawutt.b
         */
        $('.btn-like').click(function () {
            var $elClick = $(this);
            var $elCountLike = $elClick.closest('.col').find('.count-like');
            $.post('/feed/addlike.json', {article_id: $elClick.data('article_id')}, function (result) {
                console.log(result.response.status);
                if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) {
                    $elClick.find('i').attr('class', 'fa fa-lg fa-thumbs-up');
                    $elClick.attr('class', 'like-true');
                    var countLike = parseInt($elCountLike.text()) || 0;
                    $elCountLike.text(countLike + 1);
                }
            });
        });

        /**
         * 
         * Click subscribe action
         * @author sarawutt.b
         */
        $('#subscribe').on('click', function () {
            var $elClick = $(this);
            var $elSubscribe = $('.count-subscribe');
            var countSubscribe = parseInt($elSubscribe.text()) || 0;
            $.post('/UserSubscribes/subscribeUnsubscribe.json', {page_id: $('#page_id').val()}, function (result) {
                console.log(result.response.status);
                var unsubscribeClass = 'btn btn-danger waves-effect waves-light btn-block';
                var subscribeClass = 'btn btn-primary waves-effect waves-light btn-block';
                if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) {
                    if ($elClick.attr('class') == unsubscribeClass) {
                        $elClick.attr('class', subscribeClass);
                        $elClick.text('<?php echo __('Subscribe'); ?>');
                        $elSubscribe.text(countSubscribe - 1);
                        //notiWarning('<?php //echo __('You has been unsubscribe this page successfully');                      ?>');
                    } else {
                        $elClick.attr('class', unsubscribeClass);
                        $elClick.text('<?php echo __('Unsubscribe'); ?>');
                        $elSubscribe.text(countSubscribe + 1);
                        //notiSuccess('<?php //echo __('You has been subscribe this page successfully');                      ?>');
                    }
                }
                return true;
            });
        });
    });
</script>
<?php echo $this->element('utility/beacon'); ?>
<?php $this->end(); ?>