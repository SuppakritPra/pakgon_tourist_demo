<?php

use Cake\I18n\Time;

$this->layout = 'Ablepro6.mobile_slime';
?>
<div class="row">
    <div class="col">
        <div class="card text-center">
            <?php echo $this->Html->image("/upload/img/myfeed/myfeed-bg.png", ['class' => 'img-fluid h-50']); ?>
            <div class="card-block widget-user">
                <div class="user-block-1">
                    <?php echo $this->Html->image("/upload/img/myfeed/myfeed-lg.png", ['class' => 'img-fluid']); ?>
                </div>
                <div class="user-name">
                    <h5 class="text-bold"><?php echo __('My Feed'); ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="p-10"></div>

<?php if (!empty($feeds)): ?>
    <?php foreach ($feeds as $k => $v): ?>
        <div class="row">
            <div class="col">
                <div class="card card-trans border-0">
                    <a href="/feed/view/<?php echo $v->id; ?>">
                        <?php echo $this->Html->image($v->article_card, ['class' => 'card-img-top img-fluid article-img-title', 'title' => $v->article_title]); ?> 
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">
    $(function () {
        /**
         * 
         * Click like action 
         * @author sarawutt.b
         */
        $('.btn-like').click(function () {
            var $elClick = $(this);
            var $elCountLike = $elClick.closest('.col').find('.count-like');
            $.post('/feed/addlike.json', {article_id: $elClick.data('article_id')}, function (result) {
                console.log(result.response.status);
                if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) {
                    $elClick.find('i').attr('class', 'fa fa-lg fa-thumbs-up');
                    $elClick.attr('class', 'like-true');
                    var countLike = parseInt($elCountLike.text()) || 0;
                    $elCountLike.text(countLike + 1);
                }
            });
        });
    });
</script>
<?php echo $this->element('utility/beacon'); ?>
<?php $this->end(); ?>