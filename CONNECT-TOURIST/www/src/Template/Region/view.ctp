<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region $region
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Region'), ['action' => 'edit', $region->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Region'), ['action' => 'delete', $region->id], ['confirm' => __('Are you sure you want to delete # {0}?', $region->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Region'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Region'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Master Province'), ['controller' => 'MasterProvince', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Master Province'), ['controller' => 'MasterProvince', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="region view large-9 medium-8 columns content">
    <h3><?= h($region->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Region Name') ?></th>
            <td><?= h($region->region_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($region->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Master Province') ?></h4>
        <?php if (!empty($region->master_province)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Province Name Th') ?></th>
                <th scope="col"><?= __('Province Name En') ?></th>
                <th scope="col"><?= __('Logo Of Province') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Center Lat') ?></th>
                <th scope="col"><?= __('Center Long') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($region->master_province as $masterProvince): ?>
            <tr>
                <td><?= h($masterProvince->id) ?></td>
                <td><?= h($masterProvince->province_name_th) ?></td>
                <td><?= h($masterProvince->province_name_en) ?></td>
                <td><?= h($masterProvince->logo_of_province) ?></td>
                <td><?= h($masterProvince->region_id) ?></td>
                <td><?= h($masterProvince->center_lat) ?></td>
                <td><?= h($masterProvince->center_long) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MasterProvince', 'action' => 'view', $masterProvince->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MasterProvince', 'action' => 'edit', $masterProvince->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MasterProvince', 'action' => 'delete', $masterProvince->id], ['confirm' => __('Are you sure you want to delete # {0}?', $masterProvince->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
