<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region $region
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Region'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Master Province'), ['controller' => 'MasterProvince', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Master Province'), ['controller' => 'MasterProvince', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="region form large-9 medium-8 columns content">
    <?= $this->Form->create($region) ?>
    <fieldset>
        <legend><?= __('Add Region') ?></legend>
        <?php
            echo $this->Form->control('region_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
