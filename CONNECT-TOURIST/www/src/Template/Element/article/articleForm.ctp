<?php

use Cake\Utility\Inflector;

$this->layout = 'default';
$action = strtolower($this->request->action);
$buttonClass = $textSaveBtn = $icon = null;
if ($action == 'add') {
    $btnClass = 'btn btn-success';
    $textSaveBtn = __('Save');
    $icon = 'fa fa-save';
} else {
    $btnClass = 'btn btn-warning';
    $textSaveBtn = __('Edit');
    $icon = 'fa fa-pencil';
}
?>

<div class="row">
    <div class="col-12">
        <div class="card portlets portlets-success">
            <div class="card-header text-uppercase">
                <?php echo __('{0} Article', Inflector::humanize($action)) ?>
                <div class="f-right">
                    <a href="javascript:;"><i class="icofont icofont-minus"></i></a>
                    <a href="javascript:;"><i class="icofont icofont-refresh"></i></a>
                    <a href="javascript:;"><i class="icofont icofont-close"></i></a>
                </div>
            </div>
            <div class="card-block">
                <?php echo $this->Form->create($article, ['type' => 'file', 'horizontal' => true]); ?>
                <?php echo $this->Form->control('page_id', ['type' => 'hidden', 'value' => $page_id]); ?>
                <?php echo $this->Form->control('article_title', ['type' => 'text', 'class' => 'required', 'maxlength' => 256, 'label' => ['class' => 'pakgon-label']]); ?>
                <?php echo $this->Form->control('article_intro', ['type' => 'textarea', 'maxlength' => 512, 'row' => 2, 'class' => 'required', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php echo $this->Form->control('article_detail', ['type' => 'textarea', 'class' => 'required', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php echo $this->Form->control('lang_code', ['options' => $this->Utility->findLocalList(), 'empty' => false, 'class' => 'required', 'id' => 'article-detail', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php echo $this->Form->control('expire_date', ['type' => 'text', 'append' => '<i class="fa fa-calendar"></i>', 'class' => 'required pull-right', 'id' => 'reservationtime', 'label' => ['class' => 'pakgon-label', 'text' => __('Publish Date') . ' - ' . __('Expire date')]]); ?>
                <?php echo $this->Form->control('feed_flag', ['type' => 'checkbox', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php echo $this->Form->control('is_noti', ['type' => 'checkbox', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php //echo $this->Form->control('category_id', ['options' => $this->Utility->findCategoriesList(), 'class' => 'required', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php //echo $this->Form->control('display_order', ['class' => 'required digits', 'type' => 'number', 'label' => ['class' => 'pakgon-label']]); ?>
                <?php echo $this->element('/utility/mediaupload'); ?>
                <?php echo $this->Permission->button($textSaveBtn, $this->request->here, ['type' => 'submit', 'class' => $btnClass, 'icon' => $icon]); ?>
                <?php echo $this->Permission->buttonBack('normal');?>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->append('css'); ?>
<?php echo $this->Html->css('/vendor/bootstrap-daterangepicker/daterangepicker.css'); ?>

<?php $this->end(); ?>
<?php $this->append('scriptBottom'); ?>
<?php echo $this->Html->script('/vendor/editor/ckeditor/ckeditor.js'); ?>
<?php //echo $this->Html->script('/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min');  ?>
<?php echo $this->Html->script('/vendor/moment/min/moment.min'); ?>
<?php echo $this->Html->script('/vendor/bootstrap-daterangepicker/daterangepicker'); ?>
<script type="text/javascript">
    $(function () {
//        CKEDITOR.replace('article-intro', {
//            toolbar: 'Standard'
//        });

        CKEDITOR.replace('article-detail', {
            toolbar: 'Standard'
        });

        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'DD/MM/YYYY HH:mm'
            }
        });

        $("#image-title").on('change', function () {
            if ($("#image-title").val()) {
                var name_sound = document.getElementById("image-title").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 10250000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 10 MB');
                    document.getElementById("image-title-input").value = '';
                }
            }
        });

        $("#image-detail").on('change', function () {
            if ($("#image-detail").val()) {
                var name_sound = document.getElementById("image-detail").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 10250000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 10 MB');
                    document.getElementById("image-detail-input").value = '';
                }
            }
        });

        $("#audio").on('change', function () {
            if ($("#audio").val()) {
                $('#name-for-audio').removeAttr("disabled");
                //document.getElementById("name-for-audio").value = 'เสียงบรรยาย';
                // var name_sound = document.getElementById("audio").files;
                // document.getElementById("name-for-audio").value = name_sound[0].name;
                var name_sound = document.getElementById("audio").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 10250000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 10 MB');
                    document.getElementById("audio-input").value = '';
                }
                //     name_sound = name_sound[0].name;
                //     name_sound_length = name_sound.length;
                //     name_sound = name_sound.substring(0, name_sound_length-4);
                //     document.getElementById("name-for-audio").value = name_sound;

            } else {
                $('#name-for-audio').attr("disabled", true);
                // document.getElementById("name-for-audio").value = '';
            }
        });
        $("#video").on('change', function () {
            if ($("#video").val()) {
                $('#name-for-video').removeAttr("disabled");
                //  document.getElementById("name-for-video").value = 'วีดิโอประกอบ';
                var name_sound = document.getElementById("video").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 61450000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 60 MB');
                    document.getElementById("video-input").value = '';
                }
            } else {
                $('#name-for-video').attr("disabled", true);
                // document.getElementById("name-for-video").value = '';
            }
        });
    });
</script>
<?php $this->end(); ?>