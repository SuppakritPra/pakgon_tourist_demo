<?php $this->layout = 'Ablepro6.html'; ?>
<style type="text/css">.col-md-4,.col-sm-6,.col-xl-2,.container{width:100%;padding-right:15px;padding-left:15px}body{background-color:#E5E5E5}/*! CSS Used from: http://commu-uat.pakgon.local:8083/ablepro6/bower_components/bootstrap/dist/css/bootstrap.min.css */*,::after,::before{box-sizing:border-box}.container{margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-md-4,.col-sm-6,.col-xl-2{position:relative;min-height:1px}@media (min-width:576px){.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}}@media (min-width:768px){.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}}@media (min-width:1200px){.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}}.justify-content-center{-ms-flex-pack:center!important;justify-content:center!important}.align-items-center{-ms-flex-align:center!important;align-items:center!important}.h-100{height:100%!important}@media print{*,::after,::before{text-shadow:none!important;box-shadow:none!important}.container{min-width:992px!important}}/*! CSS Used from: http://commu-uat.pakgon.local:8083/ablepro6/light-vertical/assets/css/main.slim.min.css */:focus{outline:0!important}.preloader3{width:130px;height:130px;text-align:center;font-size:10px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.preloader3>div{display:inline-block;border-radius:50%;margin:2px;background-color:#1b8bf9;height:10px;width:10px;-webkit-animation:stretchdelay .7s infinite ease-in-out;animation:stretchdelay .7s infinite ease-in-out}.preloader3 .circ2{-webkit-animation-delay:-.6s;animation-delay:-.6s}.preloader3 .circ3{-webkit-animation-delay:-.5s;animation-delay:-.5s}.preloader3 .circ4{-webkit-animation-delay:-.4s;animation-delay:-.4s}.loader-block{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}/*! CSS Used from: http://commu-uat.pakgon.local:8083/ablepro6/light-vertical/assets/css/responsive.min.css */@media only screen and (max-width:544px){.loader-block{justify-content:center;display:flex;text-align:center;margin:0 auto}}/*! CSS Used keyframes */@-webkit-keyframes stretchdelay{0%,100%,40%{-webkit-transform:translateY(-10px)}20%{-webkit-transform:translateY(-20px)}}@keyframes stretchdelay{0%,100%,40%{-webkit-transform:translateY(-10px)}20%{-webkit-transform:translateY(-20px)}}</style>
<div class="container h-100" id="preloaderGeolocation">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-xl-2 col-md-4 col-sm-6">
            <div class="preloader3 loader-block">
                <div class="circ1"></div>
                <div class="circ2"></div>
                <div class="circ3"></div>
                <div class="circ4"></div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Form->create(null, ['id' => 'frmGeolocation']); ?>
<?php echo $this->Form->control('geolocation_url', ['type' => 'hidden', 'id' => 'geolocationURL']); ?>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
    /**
     * 
     * Function submit form data posting for geolocation URL
     * @author sarawutt.b
     * @param string of geolocation
     */
    function submitFormData(url) {
        document.getElementById('geolocationURL').value = url || '';
        document.getElementById('frmGeolocation').submit();
    }
    /**
     * 
     * Function get geolocation by google geolocation
     * @author sarawutt.b
     */
    function positionCallback(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pos.lat + "%2C" + pos.lng + "&language=th&region=TH&sensor=false";
        submitFormData(url);
    }

    /**
     * 
     * Function get geolocation by google geolocation
     * @author sarawutt.b
     */
    window.onload = function () {
        var httpMethod = '<?php echo (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'HTTPS' : 'HTTP'; ?>';
        var delay = 30;
        if ((typeof Connect !== 'undefined') && (typeof Connect.geolocation !== 'undefined')) {
            setTimeout(function () {
                Connect.geolocation.getCurrentPosition('normal');
            }, delay);
        } else if (typeof geolocation !== 'undefined') {
            setTimeout(function () {
                geolocation.getCurrentPosition('normal');
            }, delay);
        } else if ((httpMethod == 'HTTPS') && (typeof navigator.geolocation !== 'undefined')) {
            navigator.geolocation.getCurrentPosition(positionCallback);
        } else {
            submitFormData();
        }
    };
</script>