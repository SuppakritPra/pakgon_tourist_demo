<?php

use Cake\I18n\Time;
use Cake\Core\Configure;

$this->layout = 'Ablepro6.mobile_slime';

echo $this->Html->css([
    'Ablepro6./plugins/OwlCarousel2/dist/assets/owl.carousel.min',
    'Ablepro6./plugins/OwlCarousel2/dist/assets/owl.theme.default.min',
]);
echo $this->Html->script([
    'Ablepro6./plugins/OwlCarousel2/dist/owl.carousel.min',
    '/vendor/jquery-match-height/dist/jquery.matchHeight-min'
]);
?>

<div class="row">
    <div class="col">
        <h3 class="text-bold show-geo-current-province pull-left"><?php echo $currentCityName; ?></h3>
        <h4 class="pull-right p-t-10"><a href="/pages/adminindex" class="text-primary text-bold" title="<?php echo __('Search for my page');?>"><i class="fa fa-2x fa-search"></i></a></h4>
    </div>
</div>
<?php echo $this->Bootstrap->gap(); ?>
<div class="row">
    <div class="owl-carousel owl-theme">
        <?php if (!$geoLocationPages->isEmpty()): ?>
            <?php foreach ($geoLocationPages as $k => $v): ?>
                <div class="col">
                    <div class="card card-trans">
                        <div class="card-img-top">
                            <a href="/feed/page/<?php echo $v->id; ?>">
                                <?php echo $this->Html->image($v->page_card, ['class' => 'img-fluid', 'title' => $v->page_desc]); ?> 
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <!-- not founded result -->
        <?php endif; ?>
    </div> 
</div>

<?php if (!empty($hotDeals)): ?>
    <br/>
    <div class="row">
        <div class="col">
            <h3 class="text-bold pull-left"><?php echo __('Hot Deals'); ?></h3>
            <?php echo $this->Bootstrap->displayBarMenus($hotDealViewAllLink); ?>
        </div>
    </div>
    <?php echo $this->Bootstrap->gap(); ?>
    <div class="row">
        <div class="owl-carousel owl-theme">
            <?php foreach ($hotDeals as $k => $v): ?>
                <div class="col">
                    <div class="card card-trans">
                        <div class="card-img-top">
                            <a href="/feed/view/<?php echo $v->id; ?>">
                                <?php echo $this->Html->image($v->article_card, ['class' => 'img-fluid article-img-title', 'title' => $v->article_title]); ?> 
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div> 
    </div>
<?php else: ?>
    <!-- not founded result -->
<?php endif; ?>


<?php if (!empty($feeds)): ?>
    <br/>
    <div class="row">
        <div class="col">
            <h3 class="text-bold pull-left"><?php echo __('Informations'); ?></h3>
            <?php echo $this->Bootstrap->displayBarMenus('/feed/myFeed'); ?>
        </div>
    </div>
    <?php echo $this->Bootstrap->gap(); ?>

    <div class="row">
        <div class="owl-carousel owl-theme">
            <?php foreach ($feeds as $k => $v): ?>
                <div class="col">
                    <div class="card card-trans">
                        <div class="card-img-top">
                            <a href="/feed/view/<?php echo $v->id; ?>">
                                <?php echo $this->Html->image($v->article_card, ['class' => 'img-fluid article-img-title', 'title' => $v->article_title]); ?> 
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div> 
    </div>

<?php else: ?>
    <!-- not founded result -->
<?php endif; ?>

<?php if ($this->Utility->isEducations()): ?>
    <br/>
    <div class="row">
        <div class="col">
            <h3 class="text-bold text-left"><?php echo __('Educations'); ?></h3>
        </div>
    </div>
    <?php echo $this->Bootstrap->gap(); ?>
    <div class="row">
        <div class="owl-carousel owl-theme">
            <div class="col">
                <div class="card card-trans">
                    <div class="card-img-top">
                        <a href="<?php echo Configure::read('URL_LINK.FRONT_EDU'); ?>">
                            <?php echo $this->Html->image('/upload/img/card/999-bl.png', ['class' => 'img-fluid']); ?> 
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
<?php endif; ?>

<?php //echo $this->element('utility/contact'); ?>
<?php echo $this->Bootstrap->printBr(3); ?>
<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">
    $('.card').matchHeight();
    $('.owl-carousel').owlCarousel({
        dots: true,
        loop: false,
        margin: 10,
        nav: false,
        lazyLoad: true,
        center: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            900: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    $('.owl-dots.disabled').attr('class', 'owl-dots');
</script>
<?php $this->end(); ?>