<!-- Modal message -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="largeModalTitleLabel"><!-- the title for confirm modal --></h4>
            </div>
            <div class="modal-body">
                <p id="largeModalBodyText"><!-- text confirm message for the modal --></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
            </div>
        </div>
    </div>
</div><!-- Modal message -->

<!-- Confirm Modal -->
<div class="modal fade" id="ConfirmModal" tabindex="-1" role="dialog" aria-labelledby="ConfirmModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="ConfirmModalTitleLabel"><!-- the title for confirm modal --></h4>
            </div>
            <div class="modal-body">
                <p id="ConfirmModalBodyText"><!-- text confirm message for the modal --></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-primary" data-dismiss="modal" id="btnConfirm"><?php echo __('Confirm'); ?></button>
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal" id="btnClose"><?php echo __('Close'); ?></button>
            </div>
        </div>
    </div>
</div><!-- /Confirm Modal -->

<!-- AJAX LOADING -->
<div id="loading-indicator" class="modal" style="display:none"><img src="/img/loading-load-text.gif" id="img-loading-indicator"/> LOADING </div>
<!--<div class="overlay" id="loading-indicator-2"><i class="fa fa-refresh fa-spin"></i> LOADING </div>-->