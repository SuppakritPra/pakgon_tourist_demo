<?php
/**
 * 
 * bloc users list.
 *
 * @author prasong.p
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @since   2018/06/25 13:35:04
 * @license pakgon.Ltd.
 */
use Cake\Core\Configure;
if ($user_profile->user_type_id != 5) {
    ?>
    <div class="row-groups" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="box-body">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active row">
                        <div class="col-xs-3" >
                            <a href="<?php echo Configure::read('URL_LINK.FRONT_EDU');?>">
                                <?php echo $this->Html->image('/img/icon/Education-icon.png', array('class' => 'img-responsive', 'style' => '-webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px;'), ['alt' => '']); ?>
                                <div class="text-center">
                                    <div class="text-cat" style='font-size:17px;color: #8d8d8d;'>
                                        <?php echo __('Education'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#">
                                <?php echo $this->Html->image('/img/icon/tourism-icon.png', array('class' => 'img-responsive', 'style' => '-webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px;'), ['alt' => '']); ?>
                                <div class="text-center">
                                    <div class="text-cat" style='font-size:17px;color: #8d8d8d;'>
                                        <?php echo __('Tourism'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#">
                                <?php echo $this->Html->image('/img/icon/Finance-icon.png', array('class' => 'img-responsive', 'style' => '-webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px;'), ['alt' => '']); ?>
                                <div class="text-center">
                                    <div class="text-cat" style='font-size:17px;color: #8d8d8d;'>
                                        <?php echo __('Finance'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#">
                                <?php echo $this->Html->image('/img/icon/health-icon.png', array('class' => 'img-responsive', 'style' => '-webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px;'), ['alt' => '']); ?>
                                <div class="text-center">
                                    <div class="text-cat" style='font-size:17px;color: #8d8d8d;'>
                                        <?php echo __('Health'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <!-- <div class="item">

                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <?php
}?>