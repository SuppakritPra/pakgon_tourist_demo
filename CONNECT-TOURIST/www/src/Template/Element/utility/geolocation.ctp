<?php echo $this->Form->control('geo-current-province', ['type' => 'hidden', 'id' => 'geo-current-province', 'name' => 'geo-current-province']); ?>
<div id="geocontent"></div>

<script type="text/javascript">
    /**
     | ------------------------------------------------------------------------------------------------------------------
     | Get Current location by Geolocation
     | @license Pakgon Ltd ,Company
     | ------------------------------------------------------------------------------------------------------------------
     */

    /**
     | ------------------------------------------------------------------------------------------------------------------
     | Function get position callback
     */
    function positionCallback(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        // ajax call google map api here

        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pos.lat + "%2C" + pos.lng + "&language=th&region=TH&sensor=false";
        /**
         * ------------------------------------------------------------------------------------------------------------------------------------------------
         * PHP call intread
         * ------------------------------------------------------------------------------------------------------------------------------------------------
         */
//alert('xxxx');
        $.post('/utilities/geolocation.json', {url: url}, function (response) {
//            console.log(typeof response);
//            console.log(response.response['results']);
//            alert('xxxx');
//            if (response.response.status == 'OK') {
//                var storableLocation = {
//                    trad_mart: {},
//                    subdistrict: {},
//                    district: {},
//                    province: {},
//                    country: {},
//                    postal: {}
//                };
//                for (var ac = 0; ac < response.response['results'][0].address_components.length; ac++) {
//                    var component = response.response['results'][0].address_components[ac];
//                    console.log(component.long_name);
//                    if (component.types.includes('airport') || component.types.includes('car_rental') || component.types.includes('establishment') || component.types.includes('point_of_interest') || component.types.includes('travel_agency')) {
//                        storableLocation.trad_mart.short_name = component.short_name;
//                        storableLocation.trad_mart.long_name = component.long_name;
//                    } else if (component.types.includes('sublocality_level_2')) {
//                        storableLocation.subdistrict.short_name = component.short_name;
//                        storableLocation.subdistrict.long_name = component.long_name;
//                    } else if (component.types.includes('administrative_area_level_2') || component.types.includes('sublocality_level_1')) {
//                        storableLocation.district.short_name = component.short_name;
//                        storableLocation.district.long_name = component.long_name;
//                    } else if (component.types.includes('administrative_area_level_1') || component.types.includes('locality')) {
//                        storableLocation.province.short_name = component.short_name;
//                        storableLocation.province.long_name = component.long_name;
//                    } else if (component.types.includes('country')) {
//                        storableLocation.country.short_name = component.short_name;
//                        storableLocation.country.long_name = component.long_name;
//                    } else if (component.types.includes('postal_code')) {
//                        storableLocation.postal.short_name = component.short_name;
//                        storableLocation.postal.long_name = component.long_name;
//                    }
//
//                }
//                ;
//
//                if (storableLocation.province.long_name !== '') {
//                    $("#geo-current-province").val(storableLocation.province.long_name);
//                    $(".show-geo-current-province").text('<?php //echo __('Province');  ?>' + storableLocation.province.long_name);
//                    Cookies.set('_COOKIE_CURRENT_LOCATION', storableLocation.province.long_name);
//                } else {
//                    //Add more show profile location
//                }
//                console.log(storableLocation);
//                location = '/feed/home';
//            }

        });
    }


    $(document).ready(function () {
        setTimeout(function () {
            if ((typeof Connect !== 'undefined') && (typeof Connect.geolocation !== 'undefined')) {
                Connect.geolocation.getCurrentPosition('nomal');
            } else if (typeof geolocation !== 'undefined') {
                geolocation.getCurrentPosition('nomal');
            } else if (typeof navigator.geolocation !== 'undefined') {
                navigator.geolocation.getCurrentPosition(positionCallback);
            } else {
                notiWarning('<?php echo __('Your browser not support'); ?>');
                return false;
            }
        }, 100);

    });
</script>