<script type="text/javascript">
    /**
     | ------------------------------------------------------------------------------------------------------------------
     | Get Current location by Geolocation
     | @license Pakgon Ltd ,Company
     | ------------------------------------------------------------------------------------------------------------------
     */
//
//    var mockGeo = {
//        "results": [
//            {
//                "address_components": [
//                    {
//                        "long_name": "ซอย หมู่บ้านชวนชม",
//                        "short_name": "ซอย หมู่บ้านชวนชม",
//                        "types": ["route"]
//                    },
//                    {
//                        "long_name": "ตำบล บางบัวทอง",
//                        "short_name": "ตำบล บางบัวทอง",
//                        "types": ["locality", "political"]
//                    },
//                    {
//                        "long_name": "อำเภอ บางบัวทอง",
//                        "short_name": "อ.บางบัวทอง",
//                        "types": ["administrative_area_level_2", "political"]
//                    },
//                    {
//                        "long_name": "นนทบุรี",
//                        "short_name": "จ.นนทบุรี",
//                        "types": ["administrative_area_level_1", "political"]
//                    },
//                    {
//                        "long_name": "ประเทศไทย",
//                        "short_name": "TH",
//                        "types": ["country", "political"]
//                    },
//                    {
//                        "long_name": "11110",
//                        "short_name": "11110",
//                        "types": ["postal_code"]
//                    }
//                ],
//                "formatted_address": "ซอย หมู่บ้านชวนชม ตำบล บางบัวทอง อำเภอ บางบัวทอง นนทบุรี 11110 ประเทศไทย",
//                "geometry": {
//                    "bounds": {
//                        "northeast": {
//                            "lat": 13.9603663,
//                            "lng": 100.3675036
//                        },
//                        "southwest": {
//                            "lat": 13.9597874,
//                            "lng": 100.3670984
//                        }
//                    },
//                    "location": {
//                        "lat": 13.9600769,
//                        "lng": 100.3673009
//                    },
//                    "location_type": "GEOMETRIC_CENTER",
//                    "viewport": {
//                        "northeast": {
//                            "lat": 13.9614258302915,
//                            "lng": 100.3686499802915
//                        },
//                        "southwest": {
//                            "lat": 13.9587278697085,
//                            "lng": 100.3659520197085
//                        }
//                    }
//                },
//                "place_id": "ChIJd1tdT7eO4jARNlsCoBKEuUw",
//                "types": ["route"]
//            },
//            {
//                "address_components": [
//                    {
//                        "long_name": "ตำบล บางบัวทอง",
//                        "short_name": "ตำบล บางบัวทอง",
//                        "types": ["locality", "political"]
//                    },
//                    {
//                        "long_name": "อำเภอ บางบัวทอง",
//                        "short_name": "อ.บางบัวทอง",
//                        "types": ["administrative_area_level_2", "political"]
//                    },
//                    {
//                        "long_name": "นนทบุรี",
//                        "short_name": "จ.นนทบุรี",
//                        "types": ["administrative_area_level_1", "political"]
//                    },
//                    {
//                        "long_name": "ประเทศไทย",
//                        "short_name": "TH",
//                        "types": ["country", "political"]
//                    },
//                    {
//                        "long_name": "11110",
//                        "short_name": "11110",
//                        "types": ["postal_code"]
//                    }
//                ],
//                "formatted_address": "ตำบล บางบัวทอง อำเภอ บางบัวทอง นนทบุรี 11110 ประเทศไทย",
//                "geometry": {
//                    "bounds": {
//                        "northeast": {
//                            "lat": 13.9892584,
//                            "lng": 100.4477782
//                        },
//                        "southwest": {
//                            "lat": 13.8978529,
//                            "lng": 100.3499936
//                        }
//                    },
//                    "location": {
//                        "lat": 13.9253961,
//                        "lng": 100.4133581
//                    },
//                    "location_type": "APPROXIMATE",
//                    "viewport": {
//                        "northeast": {
//                            "lat": 13.9892584,
//                            "lng": 100.4477782
//                        },
//                        "southwest": {
//                            "lat": 13.8978529,
//                            "lng": 100.3499936
//                        }
//                    }
//                },
//                "place_id": "ChIJc2CDHx-P4jARMJYMRTeSAQQ",
//                "types": ["locality", "political"]
//            },
//            {
//                "address_components": [
//                    {
//                        "long_name": "อำเภอ บางบัวทอง",
//                        "short_name": "อ.บางบัวทอง",
//                        "types": ["administrative_area_level_2", "political"]
//                    },
//                    {
//                        "long_name": "นนทบุรี",
//                        "short_name": "จ.นนทบุรี",
//                        "types": ["administrative_area_level_1", "political"]
//                    },
//                    {
//                        "long_name": "ประเทศไทย",
//                        "short_name": "TH",
//                        "types": ["country", "political"]
//                    }
//                ],
//                "formatted_address": "อำเภอ บางบัวทอง นนทบุรี ประเทศไทย",
//                "geometry": {
//                    "bounds": {
//                        "northeast": {
//                            "lat": 14.0006245,
//                            "lng": 100.4543823
//                        },
//                        "southwest": {
//                            "lat": 13.8623598,
//                            "lng": 100.3256949
//                        }
//                    },
//                    "location": {
//                        "lat": 13.9258302,
//                        "lng": 100.4112582
//                    },
//                    "location_type": "APPROXIMATE",
//                    "viewport": {
//                        "northeast": {
//                            "lat": 14.0006245,
//                            "lng": 100.4543823
//                        },
//                        "southwest": {
//                            "lat": 13.8623598,
//                            "lng": 100.3256949
//                        }
//                    }
//                },
//                "place_id": "ChIJWbUWb_yO4jARQFEMRTeSAQM",
//                "types": ["administrative_area_level_2", "political"]
//            },
//            {
//                "address_components": [
//                    {
//                        "long_name": "11110",
//                        "short_name": "11110",
//                        "types": ["postal_code"]
//                    },
//                    {
//                        "long_name": "นนทบุรี",
//                        "short_name": "จ.นนทบุรี",
//                        "types": ["administrative_area_level_1", "political"]
//                    },
//                    {
//                        "long_name": "ประเทศไทย",
//                        "short_name": "TH",
//                        "types": ["country", "political"]
//                    }
//                ],
//                "formatted_address": "นนทบุรี 11110 ประเทศไทย",
//                "geometry": {
//                    "bounds": {
//                        "northeast": {
//                            "lat": 14.000438,
//                            "lng": 100.4539279
//                        },
//                        "southwest": {
//                            "lat": 13.863107,
//                            "lng": 100.3264889
//                        }
//                    },
//                    "location": {
//                        "lat": 13.9354695,
//                        "lng": 100.383479
//                    },
//                    "location_type": "APPROXIMATE",
//                    "viewport": {
//                        "northeast": {
//                            "lat": 14.000438,
//                            "lng": 100.4539279
//                        },
//                        "southwest": {
//                            "lat": 13.863107,
//                            "lng": 100.3264889
//                        }
//                    }
//                },
//                "place_id": "ChIJWbUWb_yO4jARYP4wZTqSARw",
//                "types": ["postal_code"]
//            },
//            {
//                "address_components": [
//                    {
//                        "long_name": "นนทบุรี",
//                        "short_name": "จ.นนทบุรี",
//                        "types": ["administrative_area_level_1", "political"]
//                    },
//                    {
//                        "long_name": "ประเทศไทย",
//                        "short_name": "TH",
//                        "types": ["country", "political"]
//                    }
//                ],
//                "formatted_address": "นนทบุรี ประเทศไทย",
//                "geometry": {
//                    "bounds": {
//                        "northeast": {
//                            "lat": 14.1403089,
//                            "lng": 100.5677219
//                        },
//                        "southwest": {
//                            "lat": 13.7888688,
//                            "lng": 100.2623644
//                        }
//                    },
//                    "location": {
//                        "lat": 13.8621125,
//                        "lng": 100.5143528
//                    },
//                    "location_type": "APPROXIMATE",
//                    "viewport": {
//                        "northeast": {
//                            "lat": 14.1403089,
//                            "lng": 100.5677219
//                        },
//                        "southwest": {
//                            "lat": 13.7888688,
//                            "lng": 100.2623644
//                        }
//                    }
//                },
//                "place_id": "ChIJO6t2z8CE4jARpO5V399Gdpg",
//                "types": ["administrative_area_level_1", "political"]
//            },
//            {
//                "address_components": [
//                    {
//                        "long_name": "ประเทศไทย",
//                        "short_name": "TH",
//                        "types": ["country", "political"]
//                    }
//                ],
//                "formatted_address": "ประเทศไทย",
//                "geometry": {
//                    "bounds": {
//                        "northeast": {
//                            "lat": 20.465143,
//                            "lng": 105.636812
//                        },
//                        "southwest": {
//                            "lat": 5.613038,
//                            "lng": 97.343396
//                        }
//                    },
//                    "location": {
//                        "lat": 15.870032,
//                        "lng": 100.992541
//                    },
//                    "location_type": "APPROXIMATE",
//                    "viewport": {
//                        "northeast": {
//                            "lat": 20.465143,
//                            "lng": 105.636812
//                        },
//                        "southwest": {
//                            "lat": 5.613038,
//                            "lng": 97.343396
//                        }
//                    }
//                },
//                "place_id": "ChIJsU1CR_eNTTARAuhXB4gs154",
//                "types": ["country", "political"]
//            }
//        ],
//        "status": "OK"
//    };

    function positionCallback(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        // ajax call google map api here
        
        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pos.lat + "%2C" + pos.lng + "&language=th&region=TH&sensor=false";
        /**
         * ------------------------------------------------------------------------------------------------------------------------------------------------
         * PHP call intread
         * ------------------------------------------------------------------------------------------------------------------------------------------------
         */
//        var settings = {
//            "async": true,
//            "crossDomain": true,
//            "url": '/utilities/geolocation.json',
//            "method": "post",
//            "data": {url: url},
//            "headers": {
//                "accept-language": "th",
//            }
//        };

        $.post('/utilities/geolocation.json', {url: url}, function (response) {
            console.log(typeof response);
            console.log(response.response['results']);
            if (response.response.status == 'OK') {
                var storableLocation = {
                    trad_mart: {},
                    subdistrict: {},
                    district: {},
                    province: {},
                    country: {},
                    postal: {}
                };
                for (var ac = 0; ac < response.response['results'][0].address_components.length; ac++) {
                    var component = response.response['results'][0].address_components[ac];
                    console.log(component.long_name);
                    if (component.types.includes('airport') || component.types.includes('car_rental') || component.types.includes('establishment') || component.types.includes('point_of_interest') || component.types.includes('travel_agency')) {
                        storableLocation.trad_mart.short_name = component.short_name;
                        storableLocation.trad_mart.long_name = component.long_name;
                    } else if (component.types.includes('sublocality_level_2')) {
                        storableLocation.subdistrict.short_name = component.short_name;
                        storableLocation.subdistrict.long_name = component.long_name;
                    } else if (component.types.includes('administrative_area_level_2') || component.types.includes('sublocality_level_1')) {
                        storableLocation.district.short_name = component.short_name;
                        storableLocation.district.long_name = component.long_name;
                    } else if (component.types.includes('administrative_area_level_1') || component.types.includes('locality')) {
                        storableLocation.province.short_name = component.short_name;
                        storableLocation.province.long_name = component.long_name;
                    } else if (component.types.includes('country')) {
                        storableLocation.country.short_name = component.short_name;
                        storableLocation.country.long_name = component.long_name;
                    } else if (component.types.includes('postal_code')) {
                        storableLocation.postal.short_name = component.short_name;
                        storableLocation.postal.long_name = component.long_name;
                    }

                }
                ;
                
                if(storableLocation.province.long_name !== ''){
                    $("#geo-current-province").val(storableLocation.province.long_name);
                    $(".show-geo-current-province").text('<?php echo __('Province'); ?>' + storableLocation.province.long_name);
                }else{
                    //Add more show profile location
                }
                console.log(storableLocation);
            }

        });


        /**
         * ------------------------------------------------------------------------------------------------------------------------------------------------
         * Native java script call
         * ------------------------------------------------------------------------------------------------------------------------------------------------
         */
//        var settings = {
//            "async": true,
//            "crossDomain": true,
//            "url": url,
//            "method": "get",
//            "headers": {
//                "accept-language": "th",
//            }
//        };

//        $.ajax(settings).done(function (response) {
//            if (response.status == 'OK') {
//                var storableLocation = {
//                    trad_mart: {},
//                    subdistrict: {},
//                    district: {},
//                    province: {},
//                    country: {},
//                    postal: {}
//                };
//                for (var ac = 0; ac < response['results'][0].address_components.length; ac++) {
//                    var component = response['results'][0].address_components[ac];
//                    console.log(component.long_name);
//                    if (component.types.includes('airport') || component.types.includes('car_rental') || component.types.includes('establishment') || component.types.includes('point_of_interest') || component.types.includes('travel_agency')) {
//                        storableLocation.trad_mart.short_name = component.short_name;
//                        storableLocation.trad_mart.long_name = component.long_name;
//                    } else if (component.types.includes('sublocality') || component.types.includes('locality')) {
//                        storableLocation.subdistrict.short_name = component.short_name;
//                        storableLocation.subdistrict.long_name = component.long_name;
//                    } else if (component.types.includes('administrative_area_level_2')) {
//                        storableLocation.district.short_name = component.short_name;
//                        storableLocation.district.long_name = component.long_name;
//                    } else if (component.types.includes('administrative_area_level_1')) {
//                        storableLocation.province.short_name = component.short_name;
//                        storableLocation.province.long_name = component.long_name;
//                    } else if (component.types.includes('country')) {
//                        storableLocation.country.short_name = component.short_name;
//                        storableLocation.country.long_name = component.long_name;
//                    } else if (component.types.includes('postal_code')) {
//                        storableLocation.postal.short_name = component.short_name;
//                        storableLocation.postal.long_name = component.long_name;
//                    }
//
//                }
//                ;
//                $("#geo-current-province").val(storableLocation.province.long_name);
//                $(".show-geo-current-province").text('<?php //echo __('Province');  ?>' + storableLocation.province.long_name);
//                console.log(storableLocation);
//            }
//
//        });    


        /**
         * --------------------------------------------------------------------------------------------------------------------------------------------------------
         * Native javascript for test mock data
         * --------------------------------------------------------------------------------------------------------------------------------------------------------
         */
//        var response = mockGeo;
//        if (response.status == 'OK') {
//            var storableLocation = {
//                trad_mart: {},
//                subdistrict: {},
//                district: {},
//                province: {},
//                country: {},
//                postal: {}
//            };
//            for (var ac = 0; ac < response['results'][0].address_components.length; ac++) {
//                var component = response['results'][0].address_components[ac];
//                console.log(component.long_name);
//                if (component.types.includes('airport') || component.types.includes('car_rental') || component.types.includes('establishment') || component.types.includes('point_of_interest') || component.types.includes('travel_agency')) {
//                    storableLocation.trad_mart.short_name = component.short_name;
//                    storableLocation.trad_mart.long_name = component.long_name;
//                } else if (component.types.includes('sublocality') || component.types.includes('locality')) {
//                    storableLocation.subdistrict.short_name = component.short_name;
//                    storableLocation.subdistrict.long_name = component.long_name;
//                } else if (component.types.includes('administrative_area_level_2')) {
//                    storableLocation.district.short_name = component.short_name;
//                    storableLocation.district.long_name = component.long_name;
//                } else if (component.types.includes('administrative_area_level_1')) {
//                    storableLocation.province.short_name = component.short_name;
//                    storableLocation.province.long_name = component.long_name;
//                } else if (component.types.includes('country')) {
//                    storableLocation.country.short_name = component.short_name;
//                    storableLocation.country.long_name = component.long_name;
//                } else if (component.types.includes('postal_code')) {
//                    storableLocation.postal.short_name = component.short_name;
//                    storableLocation.postal.long_name = component.long_name;
//                }
//
//            }
//            ;
//            $("#geo-current-province").val(storableLocation.province.long_name);
//            $(".show-geo-current-province").text('<?php //echo __('Province');              ?>' + storableLocation.province.long_name);
//            console.log(storableLocation);
//        }
    }


    $(function () {
        setTimeout(function () {
            if (typeof navigator.geolocation !== 'undefined') {
                navigator.geolocation.getCurrentPosition(positionCallback);
            } else if (typeof Connect.geolocation !== 'undefined') {
                Connect.geolocation.getCurrentPosition(positionCallback);
            } else if (typeof geolocation !== 'undefined') {
                Connect.geolocation.getCurrentPosition(positionCallback);
            } else {
                notiWarning('<?php echo __('Your browser not support'); ?>');
                return false;
            }
        }, 100);

    });
</script>