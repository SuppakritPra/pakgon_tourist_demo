<?php

$btitle = isset($btitle) ? $btitle : 'Box Notification';
echo $this->Form->button($btitle, ['class' => 'btn btn-active pull-right', 'disabled' => true]);
?>

