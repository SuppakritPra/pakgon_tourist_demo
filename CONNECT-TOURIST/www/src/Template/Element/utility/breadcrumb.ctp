<?php

/**
 * application breadcrumb 
 * @author sarawutt.b
 */
use Cake\Utility\Inflector;
?>

<h1>
    <?php echo $this->fetch('title'); ?>
    <small><?php echo __('Control Panel'); ?></small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    <li><a href="<?php $this->Url->build(['cotroller' => $this->request->controller, 'action' => 'index']); ?>"><?php echo __(Inflector::humanize($this->request->controller)); ?></a></li>
    <li class="active"><?php echo __(Inflector::humanize($this->request->action)); ?></li>
</ol>
