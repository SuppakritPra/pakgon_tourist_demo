<?php
/**
 * 
 * Paginate page element display all paginate page in box-footer
 * @author
 */
$params = $this->Paginator->params();
$showSummary = isset($showSummary) ? $showSummary : true;
?>
<?php if ($params['pageCount'] > 1): ?>
    <div class="box-footer">
        <div class="paginator">
            <ul class="pagination pull-right">
                <?php echo $this->Paginator->options(array('url' => $this->passedArgs)); ?>
                <?php echo $this->Paginator->first('<< ' . __('first')); ?>
                <?php echo $this->Paginator->prev('< ' . __('previous')); ?>
                <?php echo $this->Paginator->numbers(['before' => '', 'after' => '']); ?>
                <?php echo $this->Paginator->next(__('next') . ' >'); ?>
                <?php echo $this->Paginator->last(__('last') . ' >>'); ?>
            </ul>

            <?php if ($showSummary === true): ?>
                <p><?php echo $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]); ?></p>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>