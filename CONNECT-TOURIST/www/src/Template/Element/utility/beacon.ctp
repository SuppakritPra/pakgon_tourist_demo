<script type="text/javascript">
    /**
     | ------------------------------------------------------------------------------------------------------------------
     | Packgon main package action eacl all in Warehouse system
     | @author  Prasong.P
     | @modifire by sarawutt.b
     | @since   2016/05/28 14:12:45
     | @license Pakgon Ltd ,Company
     | ------------------------------------------------------------------------------------------------------------------
     | ------------------------------------------------------------------------------------------------------------------
     */
    var _MessageDeviceNotFound = '<?php echo __('Device not foun'); ?>';
    var _urlGetArticleByBeacon = '/beacon/getArticleByBeacon';
    function getBeacon() {
        if (typeof Beacon !== 'undefined') {
            setTimeout(function () {
                Beacon.getData();
            }, 2500);
        } else {
            return false;
        }
    }
    function updateBeacon(beacon, token) {
        //console.log(beacons);
        //var beacon = '[{"device":"PK/4/3/O/7/3","distance":"10"},{"device":"PK/4/3/O/7/2","distance":"10"},{"device":"PK/4/3/O/7/1","distance":"10"}]';    

        if (beacon != '[]') {
            checkBeacon(beacon, token);
        } else {
            $('#text-status').text(_MessageDeviceNotFound);
        }
    }
    function checkBeacon(beacons, token) {
        console.log(beacons);
        $.post(_urlGetArticleByBeacon, {'beacons': beacons, 'token': token}).done(function (result) {
            console.log(result);
        });
    }
    $(document).ready(function () {
        //var beacon = '[{"device":"PK/4/3/O/7/3","distance":"10"},{"device":"PK/4/3/O/7/2","distance":"10"},{"device":"PK/4/3/O/7/1","distance":"10"}]';    
        //checkBeacon(beacon,'');
        if (typeof Beacon !== 'undefined') {
            getBeacon();
        }
    });
</script>