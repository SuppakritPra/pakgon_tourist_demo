<script type="text/javascript">
    /**
     | ------------------------------------------------------------------------------------------------------------------
     | Packgon common application action / trigger / layout /Dynamic UI
     | @author  sarawutt.b
     | @since   2016/03/02 14:22:35
     | @license Pakgon Ltd ,Company
     | ------------------------------------------------------------------------------------------------------------------
     | ------------------------------------------------------------------------------------------------------------------
     */

    var _modalTitle = '<?php echo __('Are you sure process the action ?'); ?>';
    var _modalMessageTitle = '<?php echo __('Application process status.'); ?>';
    var _appMessageTitle = '<?php echo __('Application process status.'); ?>';
    var _modalConfirmMessage = '<?php echo __('Are you sure process the action ?'); ?>';
    var _formDisabledOption = $('#formDisabledOption').val();
    /**
     * 
     * @var _currentController as a string of cakePHP current controller
     * @var _currentAction as a string of cakePHP current action
     * @var _currentURI as a string of cakePHP current URI
     */
    var _currentController = '<?php echo $this->request->controller; ?>';
    var _currentAction = '<?php echo $this->request->action; ?>';
    var _currentURI = '<?php echo $this->request->url; ?>';

    /**
     * Custom configure for jQuery Validation plugin
     */
//    $.validator.setDefaults({
//        ignore: ':hidden:not(select),*:not([name])',
//        errorElement: 'span',
//        errorClass: 'help-block',
//        errorPlacement: function (error, element) {
//            if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
//                error.insertBefore(element.parent());
//            } else if (element.prop('type') == 'text') {
//                error.insertAfter(element);
//            } else if (element.is('select')) {
//                error.insertAfter(element.siblings(".chosen-container"));
//            } else {
//                error.insertAfter(element);
//            }
//        },
//        highlight: function (element, errorClass, validClass) {
//            if (element.type === "radio") {
//                this.findByName(element.name).addClass(errorClass).removeClass(validClass);
//            } else if (element.type === "text") {
//                $(element).closest('div').removeClass('has-success has-feedback').addClass('has-error');
//            } else {
//                $(element).closest('div').removeClass('has-success has-feedback').addClass('has-error');
//            }
//        },
//        unhighlight: function (element, errorClass, validClass) {
//            if (element.type === "radio") {
//                this.findByName(element.name).removeClass(errorClass);
//            } else if (element.type === "text") {
//                $(element).closest('div').removeClass('has-error has-feedback');
//            } else {
//                $(element).closest('div').removeClass('has-error has-feedback');
//            }
//        }
//    });

    jQuery.validator.setDefaults({
        ignore: ':hidden:not(select),*:not([name])',
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            if (element.type === "radio") {
                this.findByName(element.name).addClass(errorClass).removeClass(validClass);
            } else {
                //$(element).closest('.form-group').removeClass('has-success has-feedback').addClass('has-error has-feedback');
                //$(element).closest('.form-group').find('i.fa').remove();
                //$(element).closest('.form-group').append('<i class="fa fa-exclamation fa-lg form-control-feedback"></i>');
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            if (element.type === "radio") {
                this.findByName(element.name).removeClass(errorClass).addClass(validClass);
            } else {
                //$(element).closest('.form-group').removeClass('has-error has-feedback').addClass('has-success has-feedback');
                //$(element).closest('.form-group').find('i.fa').remove();
                //$(element).closest('.form-group').append('<i class="fa fa-check fa-lg form-control-feedback"></i>');
            }
        }
    });

    $(function () {

        /**
         Fixed display select menu on the android device
         */
        var nua = navigator.userAgent
        var isAndroid = (nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1 && nua.indexOf('Chrome') === -1)
        if (isAndroid) {
            $('select.form-control').removeClass('form-control').css('width', '100%')
        }

        var _ajaxProgress = null;
        NProgress.start();
        setTimeout(function () {
            NProgress.done();
            $('.fade').removeClass('out');
        }, 1000);

        //remove cakephp default confirmation generate by postLink to false
        $('.action-delete').attr('onclick', false);
        /**
         * 
         * Function remove trigger whern chosen select validation change value
         * @author  sarawutt.b
         * @return boolean
         */
        $('select').change(function () {
            var self = $(this).closest('div');
            var _value = $(this).val();
            if ((_value != '') || (_value != undefined)) {
                //self.removeClass('has-error has-feedback').addClass('has-success');
                self.removeClass('has-error has-feedback');
                self.find('span.help-block').remove();
            }
            return true;
        });

        /**
         * Set navigation bar to slim scall
         */
        $(".navbar .menu").slimscroll({
            height: "200px",
            alwaysVisible: false,
            size: "3px"
        }).css("width", "100%");

        /**
         * Dynamic set current menu to active class
         * @author  sarawutt.b
         * @since   2016/05/25
         */
        var a = $('a[href="<?php echo $this->request->webroot . $this->request->url ?>"]');
        if (!a.parent().hasClass('treeview') && !a.parent().parent().hasClass('pagination')) {
            a.parent().addClass('active').parents('.treeview').addClass('active');
        }

        /**
         * build cake generate code to bootstrap style if input not has class form group  then add for this class
         */
        if (!$('div.input:not(.simple), div.text:not(.simple)').hasClass('form-group')) {
            $('div.input:not(.simple), div.text:not(.simple)').addClass('form-group');
        }

        /**
         * build cake generate code to bootstrap style if input not has class form group  then add for this class
         */
        if (!$('input[type="text"]:not(.simple),select:not(.simple)').hasClass('form-control')) {
            $('input[type="text"]:not(.simple),select:not(.simple)').addClass('form-control');
        }

//        $("#UserPicturePath").removeClass('form-control');
//        $('input[type="file"]').removeClass('form-control');

        /**
         * Generate placeholder to each input not has class simple
         * @author  sarawutt.b
         * @since   2018-03-30
         */
        $('input[type="text"]:not(.simple),input[type="email"]:not(.simple),input[type="tel"]:not(.simple),input[type="password"]:not(.simple)').each(function () {
            $this = $(this);
            $label = $('label[for="' + $this.attr('id') + '"]');
            if ($('input#' + $this.attr('id')).attr('placeholder') == undefined) {
                $('input#' + $this.attr('id')).attr('placeholder', $label.text());
            }
        });

        /**
         * Generate placeholder to each input not has class simple
         * @author  sarawutt.b
         * @since   2018-03-30
         */
        $('textarea:not(.simple)').each(function () {
            $this = $(this);
            $label = $('label[for="' + $this.attr('id') + '"]');
            if ($('textarea#' + $this.attr('id')).attr('placeholder') == undefined) {
                $('textarea#' + $this.attr('id')).attr('placeholder', $label.text());
            }
        });

        //Select2 
        //Adding by sarawutt.b
        $('select:not(.simple)').select2();
        $('input[type="text"].select2-input').attr('name', 'select-search[]');
        $('input[type="text"].select2-focusser').attr('name', 'select-searchx[]');
        $('input[type="text"].select2-offscreen').attr('name', 'select-searchy[]');
        $('input[type="submit"]:not(.simple)').addClass('btn btn-primary');
        $('input[type="reset"]:not(.simple)').addClass('btn btn-default');
        $('form').attr('role', 'form');


        /**
         * 
         * Function make bootstrap thai date B.E. Format
         * @author  Vorrarit.L
         * @return  void
         */
        thdatepickers = $('input[type="text"].thdatepicker,input[type="text"].th-datepicker,input[type="text"].datepicker');
        for (var i = 0; i < thdatepickers.length; i++) {
            var dp = $(thdatepickers[i]);
            var dpCopy = dp.clone();
            dp.prop('type', 'hidden');
            dpCopy.prop('name', 'thdp_' + dp.prop('name'));
            dpCopy.prop('id', 'thdp_' + dp.prop('id'));
            dpCopy.attr('data-provide', 'datepicker');
            dpCopy.attr('data-date-format', 'dd/mm/yyyy');
            dpCopy.attr('data-date-language', 'th-th');
            dpCopy.removeClass('thdatepicker');
            dpCopy.addClass('thdp_thdatepicker');
            dpCopy.insertAfter(dp);
            dpCopy.wrap('<div class="input-group"></div>').before('<div class="input-group-addon"><i class="fa fa-calendar"></i></div>');
            if (dp.val() != '') {
//                console.log(dp.val());
//                console.log(moment('2018-02-23').format('YYYY'));
//                console.log(moment("12-25-1995", "MM-DD-YYYY").format('YY-MM-DD'));
//                console.log(moment("12/25/2018", "MM-DD-YYYY").format('YYYY'));
//                console.log(moment(dp.val(), "DD/MM/YY").format('YYYY'));
                dpCopy.val(moment(dp.val(), "DD/MM/YY").format("DD/MM") + "/" + (moment(dp.val(), "DD/MM/YY").format("YYYY") * 1 + 543));
            }
            dpCopy.datepicker({toggleActive: false, autoclose: true}).on('changeDate', (function (e) {
                // console.log(e.target);
                //console.log('changeDate ' + e.target.value);
                if (e.target.value != '') {
                    this.val(moment(e.date).format('YYYY-MM-DD'));
                } else {
                    this.val('');
                }
            }).bind(dp)).on('hide', (function (e) {
                // console.log(e.target);
                //console.log('hide ' + e.target.value);
                if (e.target.value != '') {
                    this.val(moment(e.date).format('YYYY-MM-DD'));
                } else {
                    this.val('');
                }
            }).bind(dp)).on('show', (function (e) {
                e.stopPropagation();
            }));
        }

        /**
         * 
         * Function set minDate to seccond datepicker input (where has class datepicker-end | datetimepicker-end)
         * @author  sarawutt.b
         * @returns void
         */
        $('.datepicker-start,.datetimepicker-start').on('dp.change', function (selected) {
            $(".datepicker-end,.datetimepicker-end").data("DateTimePicker").setMinDate(selected.date);
        });

        if ($(".datepicker-end,.datetimepicker-end").length > 0) {
            $(".datepicker-end,.datetimepicker-end").datetimepicker().data("DateTimePicker").setMinDate($('.datepicker-start,.datetimepicker-start').val());
        }


        $("div.index table, div.related table,div.view-related table.table-view-related,.table-view-related,.table-short-information").addClass('table table-bordered table-striped');//.find('tr:first').css('background-color', '#CFCFCF');
        $("table.table-form-view").addClass('table table-striped');

        var tmpController = $(location).attr('pathname').split('/');
        var url = $(location).attr('protocol') + '//' + $(location).attr('host') + '/' + tmpController[1] + '/add';

        //index page has show delete button ask for confirm before delete
        //if actor click to confirm then delete data where condition correcly in the database
        $("a.action-delete").on('click', function () {
            $this = $(this);
            confirmModal('<?php echo __('Are you sure for delete ?'); ?>', function (result) {
                $("section.content div.alert").remove();
                console.log(result);
                if (result == true) {
                    var url = $(location).attr('protocol') + '//' + $(location).attr('host') + $this.prev().attr('action') + '.json';
                    console.log(url);
                    $.post(url, function (data, status) {
                        console.log(data);
                        $("section.content div.alert").remove();

                        try {
                            var tmpJson = $.parseJSON(data);
                        } catch (error) {
                            var tmpJson = {message: $.trim($(data).filter('p').text()), class: 'success'};
                        }

                        var msg = tmpJson.message || '<?php echo __('Successfully'); ?>';
                        var bclass = tmpJson.class || 'info';
                        if (status == 'success') {
                            if ($this.hasClass('btnGroupMenu')) {
                                $this.parent().parent().parent().parent().parent().remove();
                            } else {
                                $this.parent().parent().remove();
                            }
                            $("section.content div:first").before(buildTopAlertMessage(msg, bclass));
                            return true;
                        } else {
                            $("section.content div:first").before(buildTopAlertMessage(msg, 'danger'));
                            return false;
                        }
                        return;
                    });
                } else {
                    return false;
                }
            });
        });
        //Remove action link code generate by cakephp
        $("div.actions").remove();
        $("div.related").remove();
        //$("div.form div.submit").find('input[type="submit"]').after('<input type="button" class="btn btn-default btn-back" name="btn-back" value="<?php echo __('Back'); ?>" onclick="window.history.back();"/>');
        //$("div.view table").after('<div class="submit"><input type="button" class="btn btn-default btn-flat btn-back" name="btn-back" value="<?php echo __('Back'); ?>" onclick="window.history.back();"/></div>');

        /*
         * We are gonna initialize all checkbox and radio inputs to 
         * iCheck plugin in.
         * You can find the documentation at http://fronteed.com/iCheck/
         */
        $("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

//$('input').iCheck({
//                    checkboxClass: 'icheckbox_square-blue',
//                    radioClass: 'iradio_square-blue',
//                    increaseArea: '20%' /* optional */
//                });

        /**
         * Ajax trigger function start while process ajax
         */
        $(document).ajaxSend(function (event, request, settings) {
            NProgress.start();
            NProgress.inc();
        });

        $(document).ajaxComplete(function (event, request, settings) {
            NProgress.done();
        });

        /**
         * select2 empty select defaul option in cascade select fillter
         * @author sarawutt.b
         */
        $(".empty-select2").select2({
            placeholder: '<?php echo $this->Bootstrap->getTextEmptySelect(); ?>',
            data: {id: "", text: ""}
        });

//notification of cake buid css
//        $("#flashMessage").dialog({
//            title: _appMessageTitle,
//            modal: true,
//            width: '70%',
//            height: 'auto',
//            autoOpen: true,
//            dialogClass: 'alert-dialog-responsive',
//            closeOnEscape: true,
//            buttons: {
//                '<?php echo __('OK'); ?>': function () {
//                    $(this).dialog("close");
//                }
//            }
//        });

        //making for notification application message
        var appMessage = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin: 0 0 20px 0; padding: 0 .7em;"> <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><strong>Appication Message : </strong> ' + $("#appMessageMessage").text() + '</p></div></div>';
        $("#appMessageMessage").html(appMessage);

        //making for notification error message
        var errorMessage = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="margin: 0 0 20px 0; padding: 0 .7em;"> <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> <strong>ERROR Message : </strong> ' + $("#errorMessageMessage").text() + '</p></div></div>';
        $("#errorMessageMessageHTML").html(errorMessage);
//        $("#appMessage").dialog({
//            title: _appMessageTitle,
//            modal: true,
//            width: '70%',
//            height: 'auto',
//            autoOpen: false,
//            dialogClass: 'alert-dialog-responsive',
//            closeOnEscape: true,
//            buttons: {
//                '<?php echo __('OK'); ?>': function () {
//                    $(this).dialog("close");
//                }
//            }
//        });
//
//        $("#confirmDialog").dialog({
//            modal: true,
//            bgiframe: true,
//            width: '70%',
//            height: 'auto',
//            autoOpen: false,
//            dialogClass: 'alert-dialog-responsive',
//            closeOnEscape: true,
//            title: _modalTitle
//        });
        $("a.confirmButton").click(function (link) {
            link.preventDefault();
            var theHREF = $(this).attr("href");
            _modalTitle = ($(this).attr("data-confirm-title") || $(this).attr("btitle")) || _modalTitle;
            var theMESSAGE = ($(this).attr("data-confirm-message") || $(this).attr("rel")) || _modalConfirmMessage;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
            $('#confirmDialog').html('<P>' + theICON + theMESSAGE + '</P>');
            $("#confirmDialog").dialog('option', 'buttons', {
                '<?php echo __('OK'); ?>': function () {
                    window.location.href = theHREF;
                },
                '<?php echo __('Cancel'); ?>': function () {
                    $(this).dialog("close");
                }
            });
            $("#confirmDialog").dialog("open");
        });
        $("input.confirmButton,input[type='submit'].confirmButton").click(function (theINPUT) {
            theINPUT.preventDefault();
            var theFORM = $(theINPUT.target).closest("form");
            _modalTitle = ($(this).attr("data-confirm-title") || $(this).attr("btitle")) || _modalTitle;
            var theMESSAGE = ($(this).attr("data-confirm-message") || $(this).attr("rel")) || _modalConfirmMessage;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
            $('#confirmDialog').html('<P>' + theICON + theMESSAGE + '</P>');
            $("#confirmDialog").dialog('option', 'buttons', {
                '<?php echo __('OK'); ?>': function () {
                    theFORM.submit();
                },
                '<?php echo __('Cancel'); ?>': function () {
                    $(this).dialog("close");
                }
            });
            $("#confirmDialog").dialog("open");
        });
        $("button.confirmButton,input[type='button'].confirmButton").click(function (theINPUT) {
            theINPUT.preventDefault();
            var theFORM = $(theINPUT.target).closest("form");
            _modalTitle = ($(this).attr("data-confirm-title") || $(this).attr("btitle")) || _modalTitle;
            var theMESSAGE = ($(this).attr("data-confirm-message") || $(this).attr("rel")) || _modalConfirmMessage;
            var action = $(this).attr('action');
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
            $('#confirmDialog').html('<P>' + theICON + theMESSAGE + '</P>');
            $("#confirmDialog").dialog('option', 'buttons', {
                '<?php echo __('OK'); ?>': function () {
                    location = action;
                },
                '<?php echo __('Cancel'); ?>': function () {
                    $(this).dialog("close");
                }
            });
            $("#confirmDialog").dialog("open");
        });


        /**
         * 
         * Modal Confirm Dialog if the input is submit and has class confirmModal
         * @author  Sarawutt.b
         * @param   object theINPUT
         * @returns string HTML format               
         */
        $("input:submit.confirmModal,button:submit.confirmModal").click(function (theINPUT) {
            theINPUT.preventDefault();
            var title = ($(this).attr("data-confirm-title") || $(this).attr("btitle")) || _modalTitle;
            var theFORM = $(theINPUT.target).closest("form");
            var theMESSAGE = ($(this).attr("data-confirm-message") || $(this).attr("rel")) || _modalConfirmMessage;
            if (!theFORM.valid()) {
                return false;
            } else {
                $(".modal-title").text(title);
                confirmModal(theMESSAGE, function (result) {
                    if (result == true) {
                        theFORM.trigger('submit');
                        return true;
                    } else {
                        return false;
                    }
                });
            }
        });

        /**
         * 
         * Modal Confirm Dialog if the input is a link and has class confirmModal
         * @author  Sarawutt.b
         * @param   object link
         * @returns string HTML format               
         */
        $("a.confirmModal").click(function (link) {
            link.preventDefault();
            var title = ($(this).attr("data-confirm-title") || $(this).attr('btitle')) || _modalTitle;
            var theHREF = $(this).attr("href");
            var theMESSAGE = ($(this).attr("data-confirm-message") || $(this).attr("rel")) || _modalConfirmMessage;
            $(".modal-title").text(title);
            confirmModal(theMESSAGE, function (result) {
                if (result == true) {
                    window.location.href = theHREF;
                    return true;
                } else {
                    return false;
                }
            });
        });

        /**
         * 
         * Modal Confirm Dialog if the input is button and has class confirmModal
         * @author  Sarawutt.b
         * @param   object theINPUT
         * @returns string HTML format               
         */
        $("input[type='button'].confirmModal,button[type='button'].confirmModal").click(function (theINPUT) {
            theINPUT.preventDefault();
            var title = ($(this).attr("data-confirm-title") || $(this).attr('btitle')) || _modalTitle;
            var theFORM = $(theINPUT.target).closest("form");
            var theMESSAGE = ($(this).attr("data-confirm-message") || $(this).attr("rel")) || _modalConfirmMessage;
            var action = $(this).attr('action');
            $(".modal-title").text(title);
            confirmModal(theMESSAGE, function (result) {
                if (result == true) {
                    location = action;
                    return true;
                } else {
                    return false;
                }
            });
        });


    }); //End Jquery Syntax

    /**
     * 
     * Function Modal confirm display Modal dialog Confirmation
     * @author   Sarawutt.b
     * @param   {type} confirmMsg as string of confirm message
     * @param   {type} object function
     * @returns boolean true if confirm and false in otherwise
     */
    function confirmModal(confirmMsg, callback) {
        if ($.trim($(".modal-title").text()) == '') {
            $(".modal-title").text(_modalTitle);
        }
        $('#loading-indicator').hide();
        confirmMsg = confirmMsg || '<?php echo __('Please confirm for your process action.'); ?>';

        if (typeof callback == 'function') {
            $("#largeModalBodyText,#ConfirmModalBodyText").text(confirmMsg);
            $("#ConfirmModal").modal({show: true, backdrop: false, keyboard: false});
            $("#btnConfirm").click(function () {
                $("#ConfirmModal").modal({show: false});
                if (callback) {
                    callback(true);
                }
            });
            $("#btnClose").click(function () {
                $("#ConfirmModal").modal({show: false});
                if (callback) {
                    callback(false);
                }
            });
        } else {
            return true;
        }
        //callback = callback || callback();

        //console.log(callback);

    }
    window.confirm = confirmModal;

    /**
     * Function make notification bueaties flash top display
     * @author  Sarawutt.b
     * @param   string title of flash name 
     * @param   string msg of display notification
     * @param   string mode (success | error | warning)
     * @returns void
     */
    function buildTopNotification(title, msg, mode) {
        return '<div class="box box-' + mode + '"><div class="box-header with-border"><h3 class="box-title">' + title + '</h3></div><div class="box-body">' + msg + '</div></div>';
    }

    /**
     * Function make notification bueaties flash top display
     * @author  Sarawutt.b
     * @param   string msg of display notification
     * @param   string mode (success | error | warning)
     * @returns void
     */
    function buildTopAlertMessage(msg, mode) {
        return '<div class="alert alert-' + mode + '"><button type="button"class="close"data-dismiss="alert">&times;</button><span id="info-message">' + msg + '</span></div>';
    }

    /**
     * Function make notification bueaties dialog as center display
     * @author  Sarawutt.b
     * @param   string message of display notification
     * @returns void
     */
    function AppMessage(message) {
        $("#appMessage").html(message);
        $("#appMessage").dialog("open");
    }

    /**
     * Function make notification bueaties dialog as center display
     * @author  Sarawutt.b
     * @param   string confirmMsg of display notification with modal body section
     * @param   string modalTitle of display notification with modal title section
     * @returns void and display alert modal style
     */
    function modalMessage(confirmMsg, modalTitle) {
        $('#loading-indicator').hide();
        modalTitle = modalTitle || _modalMessageTitle;
        confirmMsg = confirmMsg || '<?php echo __('Your process running to the statis.'); ?>';
        $(".modal-title").text(modalTitle);
        $("#largeModalBodyText,#ConfirmModalBodyText").text(confirmMsg);
        $("#largeModal").modal('show');
    }

    /**
     * Function make notification bueaties flash top display
     * @author  Sarawutt.b
     * @param   string message of display notification
     * @returns void
     */
    function TopAppMessage(message) {
        var appMessage = '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin: 0 0 20px 0; padding: 0 .7em;"> <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><strong>Appication Message : </strong> ' + message + '</p></div></div>';
        $("#confirmDialog").html(appMessage);
    }

    /**
     * Function make notification bueaties flash top display(Error style)
     * @author  Sarawutt.b
     * @param   string message of display notification
     * @returns void
     */
    function TopAppError(message) {
        var errorMessage = '<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="margin: 0 0 20px 0; padding: 0 .7em;"> <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> <strong>ERROR Message : </strong> ' + message + '</p></div></div>';
        $("#confirmDialog").html(errorMessage);
    }

    /**
     * Function ajax get District from master data 
     * @author  Sarawutt.b
     * @param   parameter with dinamic province_id jQuery dinamic selecter get params
     * @since   2016/04/26 11:16:22
     * @returns display district filter by province
     */
    function getDistrict() {
        $.post("/Utils/findDistrict/" + $("#ProvinceId").val(), function (data) {
            $("#DistrictId").html(data);
            getSubDistrict();
        });
    }

    /**
     * Function ajax get Sub-district from master data 
     * @author  Sarawutt.b
     * @param   parameter with dinamic District jQuery dinamic selecter get params
     * @since   2016/04/26 11:16:22
     * @returns display district filter by district
     */
    function getSubDistrict() {
        $.post("/Utils/findSubDistrict/" + $("#DistrictId").val(), function (data) {
            $("#SubDistrictId").html(data);
            getZipcode();
        });
    }

    /**
     * Function ajax get zipcode from master data 
     * @author  Sarawutt.b
     * @param   parameter with dinamic Sub-district jQuery dinamic selecter get params
     * @since   2016/04/26 11:16:22
     * @returns display district filter by Sub-district
     */
    function getZipcode() {
        $.post("/Utils/findZipcode/" + $("#SubDistrictId").val(), function (data) {
            $("#zipCode").val(data);
            updateChosen();
        });
    }

    /**
     * Function find options list of system action with selected system controller id 
     * @author  Sarawutt.b
     * @since   2016/04/26 11:16:22
     * @returns display system action filter by controller id
     */
    function getSystemActionListBySystemControllerId() {
        $.getJSON('/Utilities/findActionsByControllersId/' + $("#sysControllerId").val() + '.json', function (result) {
            var data2 = [];
            $.each(result.results, function (k, item) {
                data2.push({id: item.id, text: item.text});
            });

            $("#SysActionId").select2({
                placeholder: '<?php echo __('System Actions'); ?>',
                data: data2
            });
            console.log((data2.length === 0));
            $("#SysActionId").attr('disabled', (data2.length === 0));
        });
    }

    /**
     * Function update selecte list build to chosen after re created with ajax content 
     * @author  Sarawutt.b
     * @since   2018-03-30
     * @returns display district filter by Sub-district
     */
    function updateChosen() {
        $(".chosen-select").trigger("chosen:updated");
    }

    /**
     * Number.prototype.format(n, x)
     * 
     * @param integer n: length of decimal
     * @param integer x: length of sections
     */
    Number.prototype.format = function (n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };

    /**
     * Converts number into currency format
     * @param {number} number   Number that should be converted.
     * @param {string} [decimalSeparator]    Decimal separator, defaults to '.'.
     * @param {string} [thousandsSeparator]    Thousands separator, defaults to ','.
     * @param {int} [nDecimalDigits]    Number of decimal digits, defaults to `2`.
     * @return {string} Formatted string (e.g. numberToCurrency(12345.67) returns '12,345.67')
     */
    function numberToCurrency(number, decimalSeparator, thousandsSeparator, nDecimalDigits) {
        decimalSeparator = decimalSeparator || '.';
        thousandsSeparator = thousandsSeparator || ',';
        nDecimalDigits = nDecimalDigits == null ? 2 : nDecimalDigits;

        var fixed = number.toFixed(nDecimalDigits), //limit/add decimal digits
                parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed); //separate begin [$1], middle [$2] and decimal digits [$4]

        if (parts) { //number >= 1000 || number <= -1000
            return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
        } else {
            return fixed.replace('.', decimalSeparator);
        }
    }

    /**
     * 
     * Function find department fillter by organization code
     * @author sarawutt.b
     */
    function findDepartmentByOrganizationCode() {
        $.getJSON('/Utilities/findDepartmentByOrganizationCode/' + $('#masterOrganization').val() + '.json', function (result) {
            var data2 = [];
            $.each(result.results, function (k, item) {
                data2.push({id: item.id, text: item.text});
            });

            $("#masterDepartment").select2({
                placeholder: '<?php echo __('Master Department'); ?>',
                placeholderOption: '<?php echo __('Master Department'); ?>',
                data: data2
            });
            if (_formDisabledOption == 9) {
                $("#masterDepartment").val('');
            }
            $("#masterDepartment").attr('disabled', ((data2.length === 0) || (_formDisabledOption == 1)));
            findSectionByDepartmentCode();
        });

    }

    /**
     * 
     * Function find master section by department fillter code
     * @author sarawutt.b
     */
    function findSectionByDepartmentCode() {
        $.getJSON('/Utilities/findSectionByDepartmentCode/' + $('#masterDepartment').val() + '.json', function (result) {
            var data2 = [];
            $.each(result.results, function (k, item) {
                data2.push({id: item.id, text: item.text});
            });
            $("#masterSection").select2({
                placeholder: '<?php echo __('Master Section'); ?>',
                placeholderOption: '<?php echo __('Master Section'); ?>',
                data: data2
            });
            if (_formDisabledOption == 9) {
                $("#masterSection").val('');
            }
            $("#masterSection").attr('disabled', ((data2.length === 0) || (_formDisabledOption == 1)));
        });
    }

    /**
     * 
     * Function find province by country code
     * @author sarawutt.b
     */
    function findProvinceByCountryCode() {
        $.getJSON('/Utilities/findProvinceByCountryCode/' + $('#countryCode').val() + '.json', function (result) {
            var data2 = [];
            $.each(result.results, function (k, item) {
                data2.push({id: item.id, text: item.text});
            });

            $("#provinceCode").select2({
                placeholder: '<?php echo __('Master Province'); ?>',
                data: data2
            });
            if (_formDisabledOption == 9) {
                $("#provinceCode").val('');
            }
            $("#provinceCode").attr('disabled', ((data2.length === 0) || (_formDisabledOption == 1)));
            findDistrictByProvinceCode();
        });
    }

    /**
     * 
     * Function find district by province code
     * @author sarawutt.b
     */
    function findDistrictByProvinceCode() {
        $.getJSON('/Utilities/findDistrictByProvinceCode/' + $('#provinceCode').val() + '.json', function (result) {
            var data2 = [];
            $.each(result.results, function (k, item) {
                data2.push({id: item.id, text: item.text});
            });

            $("#districtCode").select2({
                placeholder: '<?php echo __('Master District'); ?>',
                data: data2
            });
            $("#districtCode").attr('disabled', ((data2.length === 0) || (_formDisabledOption == 1)));
        });
    }
</script>