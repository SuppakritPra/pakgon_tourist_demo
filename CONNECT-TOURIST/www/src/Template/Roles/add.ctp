<?php
/**
  * 
  * add roles template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Role $role
  * @since   2018/04/20 18:12:12
  * @license pakgon.Ltd.
  */
?>
<div class="roles roles-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Role Management System => ( Add Role )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($role, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('description');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/Roles/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add role ?'), 'data-confirm-title' => __('Confirm for add role ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
