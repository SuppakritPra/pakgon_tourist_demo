<?php

 /**
  *
  * The template view for view as of roles controller the page show for roles information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Role $role
  * @since  2018-04-20 18:12:12
  * @license Pakgon.Ltd
  */
?>
<div class="roles view roles-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Role Management System => ({0} information)', h($role->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Roles Name'); ?></td>
                <td class="table-view-detail"><?php echo h($role->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Roles Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($role->name_eng); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Description'); ?></td>
                <td class="table-view-detail"><?php echo h($role->description); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($role->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Roles Id'); ?></td>
                <td class="table-view-detail"><?php echo h($role->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($role->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($role->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($role->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($role->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

<div class="related div-box-related">
    <?php if (!empty($role->users)): ?>
        <div class="box box-warning box-related">
            <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Related Users'), 'bicon' => 'fa-refresh', 'bcollapse' => true, 'bclose' => true]); ?>
            <div class="box-body table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-list-related">
                    <tr>
                        <th class="nindex"><?php echo __('Row no'); ?></th>
                                                                        <th><?php echo __('Username'); ?></th>
                                                                        <th><?php echo __('Password'); ?></th>
                                                                        <th><?php echo __('Pin Code'); ?></th>
                                                                        <th><?php echo __('Dynamic Key'); ?></th>
                                                                        <th><?php echo __('Dynamic Key Expiry'); ?></th>
                                                                        <th><?php echo __('Token'); ?></th>
                                                                        <th><?php echo __('Token Expiry'); ?></th>
                                                                        <th><?php echo __('Point'); ?></th>
                                                                        <th><?php echo __('Role Id'); ?></th>
                                                                        <th><?php echo __('Position Id'); ?></th>
                                                                        <th><?php echo __('Customer Type'); ?></th>
                                                                        <th><?php echo __('User Type'); ?></th>
                                                                        <th><?php echo __('Personal Card No Type'); ?></th>
                                                                        <th><?php echo __('Personal Card No'); ?></th>
                                                                        <th><?php echo __('Name Prefix Id'); ?></th>
                                                                        <th><?php echo __('Gender'); ?></th>
                                                                        <th><?php echo __('First Name'); ?></th>
                                                                        <th><?php echo __('Last Name'); ?></th>
                                                                        <th><?php echo __('First Name Eng'); ?></th>
                                                                        <th><?php echo __('Last Name Eng'); ?></th>
                                                                        <th><?php echo __('Company Name'); ?></th>
                                                                        <th><?php echo __('Company Tax Id'); ?></th>
                                                                        <th><?php echo __('Nationality Id'); ?></th>
                                                                        <th><?php echo __('Ethnicity Id'); ?></th>
                                                                        <th><?php echo __('Birth Date'); ?></th>
                                                                        <th><?php echo __('Phone No'); ?></th>
                                                                        <th><?php echo __('Mobile Phone'); ?></th>
                                                                        <th><?php echo __('Line Account'); ?></th>
                                                                        <th><?php echo __('Email'); ?></th>
                                                                        <th><?php echo __('Profile Image'); ?></th>
                                                                        <th><?php echo __('Status'); ?></th>
                                                                        <th><?php echo __('Can Login'); ?></th>
                                                                        <th><?php echo __('Login Fail'); ?></th>
                                                                        <th><?php echo __('Last Login'); ?></th>
                                                                        <th><?php echo __('Last Change Password'); ?></th>
                                                                        <th><?php echo __('Create Uid'); ?></th>
                                                                        <th><?php echo __('Update Uid'); ?></th>
                                                                        <th><?php echo __('Created'); ?></th>
                                                                        <th><?php echo __('Modified'); ?></th>
                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                    <?php if (!empty($role->users)): ?>
                    <?php foreach ($role->users as $index => $users): ?>
                        <tr>
                            <td class="nindex"><?php echo h(++$index); ?></td>
                                                                                                    <td><?php echo h($users->username); ?></td>
                                                                                                        <td><?php echo h($users->password); ?></td>
                                                                                                        <td><?php echo h($users->pin_code); ?></td>
                                                                                                        <td><?php echo h($users->dynamic_key); ?></td>
                                                                                                        <td><?php echo h($users->dynamic_key_expiry); ?></td>
                                                                                                        <td><?php echo h($users->token); ?></td>
                                                                                                        <td><?php echo h($users->token_expiry); ?></td>
                                                                                                        <td><?php echo h($users->point); ?></td>
                                                                                                        <td><?php echo h($users->role_id); ?></td>
                                                                                                        <td><?php echo h($users->position_id); ?></td>
                                                                                                        <td><?php echo h($users->customer_type); ?></td>
                                                                                                        <td><?php echo h($users->user_type); ?></td>
                                                                                                        <td><?php echo h($users->personal_card_no_type); ?></td>
                                                                                                        <td><?php echo h($users->personal_card_no); ?></td>
                                                                                                        <td><?php echo h($users->name_prefix_id); ?></td>
                                                                                                        <td><?php echo h($users->gender); ?></td>
                                                                                                        <td><?php echo h($users->first_name); ?></td>
                                                                                                        <td><?php echo h($users->last_name); ?></td>
                                                                                                        <td><?php echo h($users->first_name_eng); ?></td>
                                                                                                        <td><?php echo h($users->last_name_eng); ?></td>
                                                                                                        <td><?php echo h($users->company_name); ?></td>
                                                                                                        <td><?php echo h($users->company_tax_id); ?></td>
                                                                                                        <td><?php echo h($users->nationality_id); ?></td>
                                                                                                        <td><?php echo h($users->ethnicity_id); ?></td>
                                                                                                        <td><?php echo h($users->birth_date); ?></td>
                                                                                                        <td><?php echo h($users->phone_no); ?></td>
                                                                                                        <td><?php echo h($users->mobile_phone); ?></td>
                                                                                                        <td><?php echo h($users->line_account); ?></td>
                                                                                                        <td><?php echo h($users->email); ?></td>
                                                                                                        <td><?php echo h($users->profile_image); ?></td>
                                                                                                        <td><?php echo $this->Utility->getMainStatus($users->status, true); ?></td>
                                                                                                        <td><?php echo h($users->can_login); ?></td>
                                                                                                        <td><?php echo h($users->login_fail); ?></td>
                                                                                                        <td><?php echo h($users->last_login); ?></td>
                                                                                                        <td><?php echo h($users->last_change_password); ?></td>
                                                                                                        <td><?php echo $this->Utility->getUserFullnameById($users->create_uid); ?></td>
                                                                                                        <td><?php echo $this->Utility->getUserFullnameById($users->update_uid); ?></td>
                                                                                                        <td><?php echo $this->Utility->dateTimeISO($users->created); ?></td>
                                                                                                        <td><?php echo $this->Utility->dateTimeISO($users->modified); ?></td>
                                                                                    <td class="actions">
                                <?php echo $this->Permission->getActions($users->id, 'Users'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="41"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                        </tr>
                    <?php endif; ?>
                    </table>
            </div><!-- ./box-body -->
            <?php echo $this->element('utility/boxOptionFooter'); ?>
        </div><!-- ./box box-warning box-related -->
        <?php endif; ?>
</div><!-- ./related div-box-related -->
