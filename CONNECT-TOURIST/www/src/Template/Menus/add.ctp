<?php
/**
 * 
 * add menus template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Menu $menu
 * @since   2018/04/20 18:12:40
 * @license pakgon.Ltd.
 */
?>
<div class="menus menus-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Menu Management System => ( Add Menu )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php echo $this->Form->create($menu, ['horizontal' => true]); ?>
        <?php echo $this->Form->control('name', ['class' => 'required', 'label' => __('Name Menu')]); ?>
        <?php echo $this->Form->control('name_eng', ['class' => 'required', 'label' => __('Name Menu (Eng)')]); ?>
        <?php echo $this->Form->control('glyphicon', ['value' => 'fa fa-circle-o']); ?>
        <?php echo $this->Form->control('domain', ['class' => 'required', 'value' => $_SERVER['SERVER_NAME']]); ?>
        <?php echo $this->Form->control('port', ['class' => 'required digits', 'value' => $_SERVER['SERVER_PORT']]); ?>
        <?php echo $this->Form->control('sys_controller_id', ['options' => $sysControllers, 'id' => 'sysControllerId']); ?>
        <?php echo $this->Form->control('sys_action_id', ['type' => 'text', 'disabled' => true, 'id' => 'SysActionId']); ?>
        <?php echo $this->Form->control('url', ['class' => 'required']); ?>
        <?php echo $this->Form->control('order_display', ['class' => 'required digits', 'type' => 'text', 'value' => $this->Utility->countMenu() + 1]); ?>
        <?php echo $this->Form->control('menu_parent_id', ['options' => $this->Utility->findListParentMenu()]); ?>
        <?php echo $this->Form->control('child_no', ['class' => 'digits', 'type' => 'text']); ?>
        <?php echo $this->Form->control('badge', ['type' => 'textarea']); ?>
        <div class="box-footer">
            <?php echo $this->Permission->submit(__('Save'), '/Menus/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add menu ?'), 'data-confirm-title' => __('Confirm for add menu ?')]); ?>
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
<script type="text/javascript">
    $(function () {
//        getSystemActionListBySystemControllerId();
        $("#sysControllerId").change(function () {
            getSystemActionListBySystemControllerId();
            $("#url").val('');
        });

        $("#SysActionId").change(function () {
            //console.log($("#SysActionId").select2('data').text);
            var tmp = '/' + $("#sysControllerId option:selected").text() + '/' + $("#SysActionId").select2('data').text;
            $("#url").val(tmp);
        });
    });
</script>