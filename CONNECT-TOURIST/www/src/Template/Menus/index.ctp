<?php
/**
 * The template index for index as of menus controller the page show for short menus information.
 *
 * @author  sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Menu[]|\Cake\Collection\CollectionInterface $menus
 * @since  2018-04-20 18:12:40
 * @license Pakgon.Ltd
 */
?>
<div class="menus index">

    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Menu Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('menus'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Menu'), '/menus/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Menu'), '/menus/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->


    <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Menus list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
            <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('Row no'); ?></th>
                        <th><?php echo $this->Paginator->sort('name', __('Menus Name')); ?></th>
                        <th><?php echo $this->Paginator->sort('name_eng', __('Menus Name Eng')); ?></th>
                        <th><?php echo $this->Paginator->sort('glyphicon'); ?></th>
                        <th><?php echo $this->Paginator->sort('domain'); ?></th>
                        <th><?php echo $this->Paginator->sort('url'); ?></th>
                        <th><?php echo $this->Paginator->sort('order_display'); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($menus)): ?>
                        <?php foreach ($menus as $index => $menu): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                <td><?php echo h($menu->name); ?></td>
                                <td><?php echo h($menu->name_eng); ?></td>
                                <td class="text-center"><?php echo $this->Bootstrap->icon($menu->glyphicon, null, null); ?></td>
                                <td><?php echo h($menu->domain); ?></td>
                                <td><?php echo h($menu->url); ?></td>
                                <td><?php echo $this->Number->format($menu->order_display); ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($menu->id); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="18"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
