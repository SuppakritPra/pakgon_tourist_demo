<?php

 /**
  *
  * The template view for view as of menus controller the page show for menus information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Menu $menu
  * @since  2018-04-20 18:12:40
  * @license Pakgon.Ltd
  */
?>
<div class="menus view menus-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Menu Management System => ({0} information)', h($menu->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Menus Name'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Menus Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->name_eng); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Glyphicon'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->glyphicon); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Domain'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->domain); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Port'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->port); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Url'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->url); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($menu->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Menus Id'); ?></td>
                <td class="table-view-detail"><?php echo h($menu->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Sys Controller Id'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($menu->sys_controller_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Sys Action Id'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($menu->sys_action_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Order Display'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($menu->order_display); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Menu Parent Id'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($menu->menu_parent_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Child No'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($menu->child_no); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($menu->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($menu->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($menu->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($menu->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

    <div class="table-view-label view-has-text">
        <h4><?php echo __('Badge'); ?></h4>
        <?php echo $this->Text->autoParagraph(h($menu->badge)); ?>
    </div>
</div><!-- ./related div-box-related -->
