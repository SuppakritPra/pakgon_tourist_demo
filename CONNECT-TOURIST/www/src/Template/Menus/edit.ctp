<?php
/**
  * 
  * edit menus template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Menu $menu
  * @since   2018/04/20 18:12:40
  * @license pakgon.Ltd.
  */
?>
<div class="menus menus-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Menu Management System => ( Add Menu )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($menu, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('glyphicon');
            echo $this->Form->control('domain');
            echo $this->Form->control('port');
            echo $this->Form->control('sys_controller_id');
            echo $this->Form->control('sys_action_id');
            echo $this->Form->control('url');
            echo $this->Form->control('order_display');
            echo $this->Form->control('menu_parent_id');
            echo $this->Form->control('child_no');
            echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]);
            echo $this->Form->control('badge');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Edit'), '/Menus/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for edit menu ?'), 'data-confirm-title' => __('Confirm for edit menu ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
