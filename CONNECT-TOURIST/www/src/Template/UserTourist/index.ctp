<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserTourist[]|\Cake\Collection\CollectionInterface $userTourist
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Tourist'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userTourist index large-9 medium-8 columns content">
    <h3><?= __('User Tourist') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('point') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_used') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dynamic_key') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dynamic_key_expiry') ?></th>
                <th scope="col"><?= $this->Paginator->sort('token') ?></th>
                <th scope="col"><?= $this->Paginator->sort('token_expiry') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pin_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pin_pass') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_active') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userTourist as $userTourist): ?>
            <tr>
                <td><?= $this->Number->format($userTourist->id) ?></td>
                <td><?= h($userTourist->username) ?></td>
                <td><?= h($userTourist->password) ?></td>
                <td><?= $this->Number->format($userTourist->point) ?></td>
                <td><?= h($userTourist->is_used) ?></td>
                <td><?= h($userTourist->dynamic_key) ?></td>
                <td><?= h($userTourist->dynamic_key_expiry) ?></td>
                <td><?= h($userTourist->token) ?></td>
                <td><?= h($userTourist->token_expiry) ?></td>
                <td><?= $this->Number->format($userTourist->created_by) ?></td>
                <td><?= h($userTourist->created) ?></td>
                <td><?= $this->Number->format($userTourist->modified_by) ?></td>
                <td><?= h($userTourist->modified) ?></td>
                <td><?= h($userTourist->pin_code) ?></td>
                <td><?= h($userTourist->pin_pass) ?></td>
                <td><?= h($userTourist->is_active) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userTourist->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userTourist->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userTourist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userTourist->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
