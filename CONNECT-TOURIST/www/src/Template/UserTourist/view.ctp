<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserTourist $userTourist
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Tourist'), ['action' => 'edit', $userTourist->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Tourist'), ['action' => 'delete', $userTourist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userTourist->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Tourist'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Tourist'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userTourist view large-9 medium-8 columns content">
    <h3><?= h($userTourist->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($userTourist->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($userTourist->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dynamic Key') ?></th>
            <td><?= h($userTourist->dynamic_key) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Token') ?></th>
            <td><?= h($userTourist->token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pin Code') ?></th>
            <td><?= h($userTourist->pin_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pin Pass') ?></th>
            <td><?= h($userTourist->pin_pass) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userTourist->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Point') ?></th>
            <td><?= $this->Number->format($userTourist->point) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $this->Number->format($userTourist->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($userTourist->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dynamic Key Expiry') ?></th>
            <td><?= h($userTourist->dynamic_key_expiry) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Token Expiry') ?></th>
            <td><?= h($userTourist->token_expiry) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userTourist->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userTourist->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Used') ?></th>
            <td><?= $userTourist->is_used ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Active') ?></th>
            <td><?= $userTourist->is_active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
