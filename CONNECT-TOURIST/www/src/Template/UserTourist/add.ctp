<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserTourist $userTourist
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List User Tourist'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="userTourist form large-9 medium-8 columns content">
    <?= $this->Form->create($userTourist) ?>
    <fieldset>
        <legend><?= __('Add User Tourist') ?></legend>
        <?php
            echo $this->Form->control('username');
            echo $this->Form->control('password');
            echo $this->Form->control('point');
            echo $this->Form->control('is_used');
            echo $this->Form->control('dynamic_key');
            echo $this->Form->control('dynamic_key_expiry', ['empty' => true]);
            echo $this->Form->control('token');
            echo $this->Form->control('token_expiry', ['empty' => true]);
            echo $this->Form->control('created_by');
            echo $this->Form->control('modified_by');
            echo $this->Form->control('pin_code');
            echo $this->Form->control('pin_pass');
            echo $this->Form->control('is_active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
