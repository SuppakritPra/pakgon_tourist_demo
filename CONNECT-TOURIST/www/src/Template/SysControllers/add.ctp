<?php
/**
  * 
  * add sys controllers template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SysController $sysController
  * @since   2018/04/20 18:13:05
  * @license pakgon.Ltd.
  */
?>
<div class="sysControllers sysControllers-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sys Controller Management System => ( Add Sys Controller )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($sysController, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('description');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/SysControllers/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add sys controller ?'), 'data-confirm-title' => __('Confirm for add sys controller ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
