<?php
/**
  * 
  * edit sys controllers template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SysController $sysController
  * @since   2018/04/20 18:13:05
  * @license pakgon.Ltd.
  */
?>
<div class="sysControllers sysControllers-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sys Controller Management System => ( Add Sys Controller )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($sysController, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Edit'), '/SysControllers/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for edit sys controller ?'), 'data-confirm-title' => __('Confirm for edit sys controller ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
