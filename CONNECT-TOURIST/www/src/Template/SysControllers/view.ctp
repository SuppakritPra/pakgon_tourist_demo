<?php

 /**
  *
  * The template view for view as of sysControllers controller the page show for sysControllers information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SysController $sysController
  * @since  2018-04-20 18:13:05
  * @license Pakgon.Ltd
  */
?>
<div class="sysControllers view sysControllers-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sys Controller Management System => ({0} information)', h($sysController->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Sys Controllers Name'); ?></td>
                <td class="table-view-detail"><?php echo h($sysController->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Description'); ?></td>
                <td class="table-view-detail"><?php echo h($sysController->description); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($sysController->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Sys Controllers Id'); ?></td>
                <td class="table-view-detail"><?php echo h($sysController->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($sysController->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($sysController->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($sysController->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($sysController->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

<div class="related div-box-related">
    <?php if (!empty($sysController->menus)): ?>
        <div class="box box-warning box-related">
            <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Related Menus'), 'bicon' => 'fa-refresh', 'bcollapse' => true, 'bclose' => true]); ?>
            <div class="box-body table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-list-related">
                    <tr>
                        <th class="nindex"><?php echo __('Row no'); ?></th>
                                                                        <th><?php echo __('Sys Controllers Name'); ?></th>
                                                                        <th><?php echo __('Sys Controllers Name Eng'); ?></th>
                                                                        <th><?php echo __('Glyphicon'); ?></th>
                                                                        <th><?php echo __('Domain'); ?></th>
                                                                        <th><?php echo __('Port'); ?></th>
                                                                        <th><?php echo __('Sys Controller Id'); ?></th>
                                                                        <th><?php echo __('Sys Action Id'); ?></th>
                                                                        <th><?php echo __('Url'); ?></th>
                                                                        <th><?php echo __('Order Display'); ?></th>
                                                                        <th><?php echo __('Menu Parent Id'); ?></th>
                                                                        <th><?php echo __('Child No'); ?></th>
                                                                        <th><?php echo __('Status'); ?></th>
                                                                        <th><?php echo __('Create Uid'); ?></th>
                                                                        <th><?php echo __('Update Uid'); ?></th>
                                                                        <th><?php echo __('Created'); ?></th>
                                                                        <th><?php echo __('Modified'); ?></th>
                                                                        <th><?php echo __('Badge'); ?></th>
                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                    <?php if (!empty($sysController->menus)): ?>
                    <?php foreach ($sysController->menus as $index => $menus): ?>
                        <tr>
                            <td class="nindex"><?php echo h(++$index); ?></td>
                                                                                                    <td><?php echo h($menus->name); ?></td>
                                                                                                        <td><?php echo h($menus->name_eng); ?></td>
                                                                                                        <td><?php echo h($menus->glyphicon); ?></td>
                                                                                                        <td><?php echo h($menus->domain); ?></td>
                                                                                                        <td><?php echo h($menus->port); ?></td>
                                                                                                        <td><?php echo h($menus->sys_controller_id); ?></td>
                                                                                                        <td><?php echo h($menus->sys_action_id); ?></td>
                                                                                                        <td><?php echo h($menus->url); ?></td>
                                                                                                        <td><?php echo h($menus->order_display); ?></td>
                                                                                                        <td><?php echo h($menus->menu_parent_id); ?></td>
                                                                                                        <td><?php echo h($menus->child_no); ?></td>
                                                                                                        <td><?php echo $this->Utility->getMainStatus($menus->status, true); ?></td>
                                                                                                        <td><?php echo $this->Utility->getUserFullnameById($menus->create_uid); ?></td>
                                                                                                        <td><?php echo $this->Utility->getUserFullnameById($menus->update_uid); ?></td>
                                                                                                        <td><?php echo $this->Utility->dateTimeISO($menus->created); ?></td>
                                                                                                        <td><?php echo $this->Utility->dateTimeISO($menus->modified); ?></td>
                                                                                                        <td><?php echo h($menus->badge); ?></td>
                                                                                    <td class="actions">
                                <?php echo $this->Permission->getActions($menus->id, 'Menus'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="19"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                        </tr>
                    <?php endif; ?>
                    </table>
            </div><!-- ./box-body -->
            <?php echo $this->element('utility/boxOptionFooter'); ?>
        </div><!-- ./box box-warning box-related -->
        <?php endif; ?>
</div><!-- ./related div-box-related -->
