<?php
/**
  * 
  * edit regions template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Region $region
  * @since   2018/04/20 18:20:15
  * @license pakgon.Ltd.
  */
?>
<div class="regions regions-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Region Management System => ( Add Region )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($region, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Edit'), '/Regions/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for edit region ?'), 'data-confirm-title' => __('Confirm for edit region ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
