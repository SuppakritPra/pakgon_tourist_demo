<?php
/**
  * The template index for index as of provinces controller the page show for short provinces information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Province[]|\Cake\Collection\CollectionInterface $provinces
  * @since  2018-04-20 18:20:32
  * @license Pakgon.Ltd
  */
?>
<div class="provinces index">
    
    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Province Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('provinces'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Province'), '/provinces/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Province'), '/provinces/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->

    
        <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Provinces list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('Row no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('code'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name',__('Provinces Name')); ?></th>
                                            <th><?php echo $this->Paginator->sort('name_eng',__('Provinces Name Eng')); ?></th>
                                            <th><?php echo $this->Paginator->sort('region_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('status'); ?></th>
                                            <th><?php echo $this->Paginator->sort('create_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('update_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($provinces)): ?>
                    <?php foreach ($provinces as $index => $province): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                                                                                                    <td><?php echo $this->Number->format($province->code); ?></td>
                                                                                                                                <td><?php echo h($province->name); ?></td>
                                                                                                                                <td><?php echo h($province->name_eng); ?></td>
                                                                                                    <td><?php echo $province->has('region') ? $this->Html->link($province->region->name, ['controller' => 'Regions', 'action' => 'view', $province->region->id]) : ''; ?></td>
                                                                                                                                    <td><?php echo $this->Utility->getMainStatus($province->status, true); ?></td>
                                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($province->create_uid); ?></td>
                                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($province->update_uid); ?></td>
                                                                                                                                <td><?php echo $this->Utility->dateTimeISO($province->created); ?></td>
                                                                                                                                <td><?php echo $this->Utility->dateTimeISO($province->modified); ?></td>
                                                                    <td class="actions">
                            <?php echo $this->Permission->getActions($province->id); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="11"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
         </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
