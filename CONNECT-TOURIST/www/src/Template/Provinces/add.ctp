<?php
/**
  * 
  * add provinces template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Province $province
  * @since   2018/04/20 18:20:32
  * @license pakgon.Ltd.
  */
?>
<div class="provinces provinces-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Province Management System => ( Add Province )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($province, ['horizontal' => true]);
            echo $this->Form->control('code');
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('region_id', ['options' => $regions, 'empty' => true]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/Provinces/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add province ?'), 'data-confirm-title' => __('Confirm for add province ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
