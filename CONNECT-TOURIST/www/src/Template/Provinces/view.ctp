<?php

 /**
  *
  * The template view for view as of provinces controller the page show for provinces information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Province $province
  * @since  2018-04-20 18:20:32
  * @license Pakgon.Ltd
  */
?>
<div class="provinces view provinces-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Province Management System => ({0} information)', h($province->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Provinces Name'); ?></td>
                <td class="table-view-detail"><?php echo h($province->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Provinces Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($province->name_eng); ?></td>
            </tr>
                                    <tr>
                <td class="table-view-label"><?php echo __('Region'); ?></td>
                <td class="table-view-detail"><?php echo $province->has('region') ? $this->Html->link($province->region->name, ['controller' => 'Regions', 'action' => 'view', $province->region->id]) : ''; ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($province->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Provinces Id'); ?></td>
                <td class="table-view-detail"><?php echo h($province->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Code'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($province->code); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($province->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($province->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($province->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($province->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
