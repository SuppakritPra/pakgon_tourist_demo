    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
                height: 580px;
                width: 900px;
          }
    </style>

    <div id="map"></div>
    <script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 13.760342, lng: 100.597682},
          zoom: 9
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        // if (navigator.geolocation) {
        //   navigator.geolocation.getCurrentPosition(function(position) {
        //     var pos = {
        //       lat: position.coords.latitude,
        //       lng: position.coords.longitude
        //     };

        //     infoWindow.setPosition(pos);
        //     infoWindow.setContent('Location found.');
        //     infoWindow.open(map);
        //     map.setCenter(pos);
        //   }, function() {
        //     handleLocationError(true, infoWindow, map.getCenter());
        //   });
        // } else {
        //   // Browser doesn't support Geolocation
        //   handleLocationError(false, infoWindow, map.getCenter());
        // }

        var self = this;

                if (this.useCurrentPosition && navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(function (position) {

                        self.latitude = position.coords.latitude;
                        self.longitude = position.coords.longitude;
                        self.load(new google.maps.LatLng(self.latitude, self.longitude));

                    }, function () {
                        console.log("Error: The Geolocation service failed.");
                        self.load(new google.maps.LatLng(self.latitude, self.longitude));
                    });
                }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
    </script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQYrnkuaSXlh5gcYV6AkZSqaDc37A5deo&callback=initMap">
    </script>