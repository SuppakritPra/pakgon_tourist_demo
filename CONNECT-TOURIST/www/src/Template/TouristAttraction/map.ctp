<!DOCTYPE html>
<html>
	<head>
			<meta name="viewport" content="initial-scale=1.0">
			<meta charset="utf-8">
			<style>
							/* Always set the map height explicitly to define the size of the div
							* element that contains the map. */
							#map {
								height: 580px;
								width: 900px;
							}
						</style>
	</head>
	<body>
		<div id="map"></div>
			<script>
				var map;
				function initMap() {
					var latitude = 13.726677; // YOUR LATITUDE VALUE
			        var longitude = 100.514081; // YOUR LONGITUDE VALUE
			        var myLatLng = {lat: latitude, lng: longitude};

			        map = new google.maps.Map(document.getElementById('map'), {
			        	center: myLatLng,
			        	zoom: 10
			        });
			    }
			</script>

			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQYrnkuaSXlh5gcYV6AkZSqaDc37A5deo&callback=initMap"
			async defer></script>

	</body>
</html>