
<div class="horizontal">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
     <ul class="navbar horizontal" style="background-color:#b30000">
      <div class="col ">
        <div class="form-inline">
          <li role="presentation" class="active"><a href="/tourist-attraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
          <li role="presentation" ><a href="/tourist-attraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>
        </div>
      </div>
      <div class="col">
        <button type="button" class="btn btn-default navbar-btn pull-right"  style="text-decoration:none; color:#b30000;" >SIGN OUT</button>
      </div>
    </ul>

    <div class="bg-white p-relative horizontal">
      <div class="card-block post-timelines">
        <div align="center" >
          <div class="media-middle friend-box">
            <?php echo $this->Html->image($user[0]['image'], ['class' => ' rounded-circle img-fluid','alt'=>'user-header']); ?>
            <h1><?php echo $user[0]['username']; ?></h1>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="p-1"></div>
    <?php foreach($UserActive as $key=>$UserActives) { ?>
      <?php $datailike = $this->DateFormat->ilike($UserActives->tourist_attraction_id); ?>

      <?php foreach($datailike as $key=>$datailikes) {  ?> 
       <div class="bg-white p-relative horizontal">
        <div class="form-inline grid-item">
         <div class="col-xs-4 text-center">
           <img class="rounded-circle img-fluid" src="<?php echo $datailikes->path_picture_tourist_attraction; ?>" alt="user-header">
         </div>
         <div class="col-xs ">
           <h1 style="color:#0b0b0b"><?php echo $this->Html->link($datailikes->name, array('controller' => 'TouristAttraction','action'=> 'bloc'."/".$datailikes->id), array( 'style'=> 'color:#0b0b0b; font-size:35px; text-decoration:none;'));?></h1>
           <?php echo __("Province") . $datailikes['mp']['province_name_th']; ?>
         </div>
       </div>
     </div>
     <br>
     <?php $TouristAttraction_image = $this->DateFormat->Attraction($datailikes->id); 
     $i = 0; ?>

      <!-- ---------------------------------------------------------------------------------------------------- -->
      <div class="owl-carousel owl-theme">
           <?php foreach($TouristAttraction_image as $key=>$TouristAttraction_images) { ?> 
            <div class="item "> <img src="<?php  echo $TouristAttraction_images['path_image']; ?>" /> </div>
          <?php } ?>
        </div>


  <hr>
<?php  }  ?>
<?php  }  ?>
</div>
</div>
</div>





<style>
.CoverImage {

 cursor:default;
 position:relative;
 top:0px;
 left:0px;
 width: auto;
 height:300px;
 overflow:hidden;
}
.CoverImage2 {

 cursor:default;
 position:relative;
 top:0px;
 left:25px;
 right: 0px;
 width:250px;
 height:250px;
 overflow:hidden;
}

figure{
 width:100px; 
 overflow:hidden; 
 margin: auto;   
}
figure img{
 display:block; 
 max-width:150px; 
 margin: auto;
}

.btn2 {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  background-color: #fff;
  color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: 3px solid #b30000;
  cursor: pointer;
  border-radius: 25px;

}

.bottom-right {
  position: absolute;
  bottom: 3%;
  right: -46%;
}


/*----------------------------------------------*/

</style>
<script >
 $(function () {

  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:5,
    nav:true,
    dots:false,
    merge:true,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:2
      },
      1000:{
        items:3
      }
    }
  });
});
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
