
<style>
#map {
  height: 580px;
  width: 900px;
}
</style>
    <!-- <div class="row">
      <div class="col" id="map"></div> -->
      <div class="horizontal">
        <div class="row">
          <div class=" col-12 col-md-8 offset-md-2">
          <ul class="row navbar horizontal" style="background-color:#b30000">
            <div class="col ">
              <div class="form-inline horizontal">
                <li role="presentation" class="active"><a href="http://tourist-demo.pakgon.local:5052/TouristAttraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
                <li role="presentation" ><a href="http://tourist-demo.pakgon.local:5052/TouristAttraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>
              </div>
            </div>
            <div class="col">
              <button type="button" class="btn btn-default navbar-btn pull-right"  style="text-decoration:none; color:#b30000;" >SIGN OUT</button>
            </div>
          </ul>
        </div>
          <div class="col-12 col-md-8 offset-md-2" id="map">


          </div>
        </div>
      </div>

    </div>
    <script>
      function initMap() {
        var masterProvince = JSON.parse( '<?php echo json_encode($masterProvince); ?>' );
        Array.prototype.forEach.call(masterProvince, function(latlongzm) {
          var latitude = latlongzm['center_lat'];
          var longtitude = latlongzm['center_long'];
          var zoom = latlongzm['zoom'];
          var map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(latitude, longtitude),
            zoom: zoom
          });

          var infoWindow = new google.maps.InfoWindow({map:map});
          var touristAttraction = JSON.parse( '<?php echo json_encode($touristAttraction); ?>' );
            // touristAttraction.forEach(function(markerElem) {
              Array.prototype.forEach.call(touristAttraction, function(markerElem) {
              // var test = parseFloat(markerElem['latitude']);
              // var test1 = parseFloat(markerElem['longtitude']);

              ////////////////////////////////////////////////////////////////////////////////////////////////////
              var name = markerElem['name'];
              // var detail_tourist = markerElem['detail_tourist'];
              var path_icon_marker = markerElem['ctg']['path_icon_marker'];
              var point = new google.maps.LatLng(
                parseFloat(markerElem['latitude']),
                parseFloat(markerElem['longtitude']));

              // var infowincontent = document.createElement('div');
              // var strong = document.createElement('strong');
              // strong.textContent = name;
              // infowincontent.appendChild(strong);
              // infowincontent.appendChild(document.createElement('br'));

              // var text = document.createElement('text');
              // text.textContent = detail_tourist;
              // infowincontent.appendChild(text);
              // infowincontent.appendChild(document.createElement('br'));
              ////////////////////////////////////////////////////////////////////////////////////////////////////

              // test link in google map //

              // var id = document.createElement('id');
              // // id.textContent = "id from bloc = "+'999';
              // id.textContent = "test id = " + markerElem['id'];
              // infowincontent.appendChild(id);              

              // var id = document.createElement('id');
              // // id.href= "../tourist-attraction/bloc/";
              // // id.textContent = markerElem['id'];
              // id.textContent = "http://tourist-demo.pakgon.local:5052/tourist-attraction/bloc/" + markerElem['id'];
              // infowincontent.appendChild(id);              
              var url = "http://tourist-demo.pakgon.local:5052/tourist-attraction/bloc/" + markerElem['id'];



              // contentString = '<a href="/tourist-attraction/bloc/'+ markerElem['id'] + '" target="_blank">Go To The Detail Page</a>';

              /////////////////////////////


              var icon = {
                  url: path_icon_marker, // url
                  scaledSize: new google.maps.Size(50, 50), // scaled size
                  origin: new google.maps.Point(0,0), // origin
                  anchor: new google.maps.Point(0, 0) // anchor
                };

                var marker = new google.maps.Marker({
                  position: point,
                  icon: icon,
                  title: name,
                  map: map,
                  url: url
                });

              // geolocation mapmarker /////////////////////////////////////////
              if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                  var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                  };
                  var icon = {
                    url: '../icon/128x128/myicon-finder.jpg', // url
                    scaledSize: new google.maps.Size(50, 50), // scaled size
                    origin: new google.maps.Point(0,0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                  };
                  var marker = new google.maps.Marker({
                    position: pos,
                    title: "Me",
                    map: map
                    // icon: icon
                  });
                  // infoWindow.setPosition(pos);
                  // infoWindow.setContent('It Me.');
                  // infoWindow.open(map, marker);
                  // map.setCenter(pos);
                }, function() {
                  handleLocationError(true, infoWindow, map.getCenter());
                });
              } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
              }

            function addMarker(feature) {
              var marker = new google.maps.Marker({
                position: feature.position,
                icon: icons[feature.type].icon,
                map: map
              });
            }
            /////////////////////////////////////////////////////////////////////////////////

            marker.addListener('click', function() {
                // infoWindow.setContent(infowincontent);
                // infoWindow.open(map, marker);
                if (!!this.url) {
                  window.location.href = this.url;
                }
              });
          });
});

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
    'Error: The Geolocation service failed.' :
    'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

      // function downloadUrl(url, callback) {
      //   var request = window.ActiveXObject ?
      //       new ActiveXObject('Microsoft.XMLHTTP') :
      //       new XMLHttpRequest;

      //   request.onreadystatechange = function() {
      //     if (request.readyState == 4) {
      //       request.onreadystatechange = doNothing;
      //       callback(request, request.status);
      //     }
      //   };

      //   request.open('GET', url, true);
      //   request.send(null);
      // }

      // function doNothing() {}
    </script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQYrnkuaSXlh5gcYV6AkZSqaDc37A5deo&callback=initMap">
  </script>
  