<?php

use Cake\I18n\Time;

// $this->layout = 'Ablepro6.mobile_slime';
?>
<div class="row">
    <div class="col-lg-9 pull-lg-3 col-md-12">
        <div class="card text-center">
            <script src="/js/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              // $AutoPlay: 1,        
              // $Idle: 5000,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb052 .i {position:absolute;cursor:pointer;}
        .jssorb052 .i .b {fill:#000;fill-opacity:0.3;}
        .jssorb052 .i:hover .b {fill-opacity:.7;}
        .jssorb052 .iav .b {fill-opacity: 1;}
        .jssorb052 .i.idn {opacity:.3;}

        .jssora053 {display:block;position:absolute;cursor:pointer;}
        .jssora053 .a {fill:none;stroke:#fff;stroke-width:640;stroke-miterlimit:10;}
        .jssora053:hover {opacity:.8;}
        .jssora053.jssora053dn {opacity:.5;}
        .jssora053.jssora053ds {opacity:.3;pointer-events:none;}
    </style>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
            <div>
                <img data-u="image" src="<?php echo $touristAttraction_find_id['0']['path_picture_tourist_attraction'] ?>" />
            </div>
            <div>
                <img data-u="image" src="<?php echo $touristAttraction_find_id['0']['path_picture_tourist_attraction'] ?>" />
            </div>
            <<!-- div>
                <img data-u="image" src="test-product-test.png" />
            </div> -->
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <!-- <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div> -->
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
        </div>
    </div>
</div>

<div class="row">
    <?php if (!empty($touristAttraction_find_id)): ?>
        <div class="col-lg-9 pull-lg-3 col-md-12">
            <?php foreach ($touristAttraction_find_id as $touristAttraction_find_id): ?>
                <div class="bg-white p-relative">
                    <div class="card-block post-timelines">
                        <div class="media-middle friend-box float-left">
                            <a href="/tourist-attraction/bloc/<?php echo $touristAttraction_find_id->id; ?>">
                                <?php echo $this->Html->image($MasterProvince['0']['logo_of_province'], ['class' => 'media-object rounded-circle']); ?>
                            </a>
                        </div>
                        <div class="media-middle friend-box float-center">
                                <?php echo __("Province") . $MasterProvince['0']['province_name_th']; ?>
                        </div>
                        <div class="media-middle friend-box float-bottom">
                                <?php echo $this->Utility->thaiDateFullMonth($create_time); ?>

                            <div class="friend-details pull-right">
                                <div class="col">
                                    <?php //if (@$arrLike[$touristAttraction_find_id->id]): ?>
                                        <!--<a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-up like-true"></i>-->
                                    <?php //$checkarrlike = @$arrLike[$touristAttraction_find_id->id]; 
                                    //pr($checkarrlike);die;
                                    // pr($arrLike);die;
                                        if(!empty($arrLike)){
                                            $hasItems = 1;
                                        }else{
                                            $hasItems = 0;
                                        }
                                    ?>
                                    <?php if($hasItems): ?>
                                        <a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-up like-true"></i>
                                    <?php //else: ?>
                                        <!--<a class="btn-unlike" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-o-up"></i></a>-->
                                    <?php endif; ?>
                                    <div class="count-like text-bold pull-right">
                                        <?php echo ($touristAttraction_find_id->count_like < 1000) ? $touristAttraction_find_id->count_like : round($touristAttraction_find_id->count_like / 1000, 1) . " K"; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                    </div>

                </div>
                <div class="p-10"></div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

<?php $this->append('script'); ?>
<script type="text/javascript">
    console.log("javascript start");
    $(function () {
        console.log("get in function");
        // $('.btn-like').click(function () {
        //     var $elClick = $(this);
        //     var $elCountLike = $elClick.closest('.col').find('.count-like');
        //     $.post('/tourist-attraction/like.json', {tourist_attraction_id: $elClick.data('tourist_attraction_id')}, function (result) {
        //         console.log(result);die;
        //         var unlikeClass = 'fa fa-lg fa-thumbs-o-up';
        //         var likeClass = 'fa fa-lg fa-thumbs-up';
        //         if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) { 
        //             if ($response['status_like'] == 'y') {
        //                 $elClick.find('i').attr('class', likeClass);
        //                 $elClick.attr('class', 'like-true');
        //                 var countLike = parseInt($elCountLike.text()) || 0;
        //                 $elCountLike.text(countLike + 1);
        //             } else {
        //                 $elClick.find('i').attr('class', unlikeClass);
        //                 $elClick.attr('class', 'like-true');
        //                 var countLike = parseInt($elCountLike.text()) || 0;
        //                 $elCountLike.text(countLike - 1);
        //             }
        //         }
        //     });
        // });
        $('.btn-like').click(function () {
            var $elClick = $(this);
            var $elCountLike = $elClick.closest('.col').find('.count-like');
            $.post('/tourist-attraction/unlike.json', {tourist_attraction_id: $elClick.data('tourist_attraction_id')}, function (result) {
                console.log(result);
                if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) {
                    $elClick.find('a').attr('class', 'btn-unlike');
                    $elClick.find('a').attr('data', 'tourist_attraction_id');
                    $elClick.find('i').attr('class', 'fa fa-lg fa-thumbs-o-up');
                    var countLike = parseInt($elCountLike.text()) || 0;
                    $elCountLike.text(countLike + 1);
                }
            });
        });
        $('.btn-unlike').click(function () {
            var $elClick = $(this);
            var $elCountLike = $elClick.closest('.col').find('.count-like');
            $.post('/tourist-attraction/like.json', {tourist_attraction_id: $elClick.data('tourist_attraction_id')}, function (result) {
                console.log(result);
                if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) {
                    $elClick.find('a').attr('class', 'btn-like');
                    $elClick.find('a').attr('data', 'tourist_attraction_id');
                    $elClick.find('i').attr('class', 'fa fa-lg fa-thumbs-up like-true');
                    var countLike = parseInt($elCountLike.text()) || 0;
                    $elCountLike.text(countLike - 1);
                }
            });
        });
    });
</script>
<?php echo $this->element('utility/beacon'); ?>
<?php $this->end(); ?>