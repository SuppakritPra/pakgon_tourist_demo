
  <style>
    #map {
              height: 580px;
              width: 900px;
            }
  </style>
  <div id="map"></div>

  <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644}, //#กำหนดค่า center เริ่มต้นเเผนที่
          zoom: 6 //#กำหนดค่า zoom เริ่มต้นเเผนที่
        });
        var infoWindow = new google.maps.InfoWindow({map: map}); //#สร้างกล่องข้อความ

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            }; //#location พิกัดที่เราอยู่
            console.log(pos);
            infoWindow.setPosition(pos); //#เเสดงกล่องข้อความที่เราอยู่
            infoWindow.setContent('Location found.'); //#ข้อความที่เเสดงในกล่องข้อความ
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
          infoWindow.setPosition(pos);
          infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
          infoWindow.open(map);
      }
  </script>

<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQYrnkuaSXlh5gcYV6AkZSqaDc37A5deo&callback=initMap">
  </script>
*/