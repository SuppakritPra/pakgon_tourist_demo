
<div class="horizontal">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
     <ul class="navbar horizontal" style="background-color:#b30000;">
      <div class="col">
        <div class="form-inline">
          <li role="presentation" class="active"><a href="http://tourist-demo.pakgon.local:5052/TouristAttraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
          <li role="presentation" ><a href="http://tourist-demo.pakgon.local:5052/TouristAttraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>
        </div>
      </div>
      <div class="col">
        <button type="button" class="btn btn-default navbar-btn pull-right"  style="text-decoration:none; color:#b30000; border:none;" >SIGN OUT</button>
      </div>
    </ul>

    <div class="bg-white p-relative horizontal">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner">
         <?php foreach($image_banner as $key=>$image_banners) { ?> 

          <div class="item <?php echo ($key == 0) ? "carousel-item active" : ""; ?> ">
            <div class="CoverImage">
              <img src="<?php  echo $image_banners['banner_path']; ?>" alt="Image of every carousel"/>
            </div>
          </div>

        <?php } ?>
      </div>
    </div>
    <div class="container-fluid">
      <button class="btn2" type="button" title="ค้นหา" onclick="window.location.href = '/TouristAttraction/search_province'"><i class="fa fa-search"></i><?php echo $TouristAttraction==!null?$TouristAttraction[0]['mp']['province_name_th']:$masterProvince[0]['province_name_th']; ?></button>
      <div class="img-responsive" style="position: absolute; top: 88%; left: 92%;">
        <a href="/TouristAttraction/loopmapmarker/<?php echo $TouristAttraction==!null?$TouristAttraction[0]['mp']['id']:$masterProvince[0]['id']; ?>" title="แผนที่"><img class="i" src="/icon//map/map2.png"></a>
      </div>
    </div>
  </div>

  <div class="bg-gray p-relative horizontal">
    <div class="card-block post-timelines">
      <div class="container-fluid">
        <div class="form-inline grid-item ">
          <div class="col-8">
            <h3 style="color:#2b2b2b" >สถานที่ท่องเที่ยว</h3>
          </div>
          <div class="col-4">
            <?php if(!empty($TouristAttraction)) { ?>
              <?php echo $this->Html->link(__('ความนิยม'), array('controller' => 'TouristAttraction','action'=> 'index'."/".$TouristAttraction[0]['mp']['id']), array( 'class'=>'example_c  pull-right','style'=> 'text-decoration:none;','title'=>'เรียงตามความนิยม'));?>
            <?php }else { ?>
              <?php echo $this->Html->link(__('ความนิยม'), array('controller' => 'TouristAttraction','action'=> 'index'."/".$masterProvince[0]['id']), array( 'class'=>'example_c  pull-right','style'=> 'text-decoration:none;','title'=>'เรียงตามความนิยม'));?>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <?php if(!empty($TouristAttraction)){ ?>
    <?php foreach($TouristAttraction as $key=>$TouristAttractions) { ?> 
      <div class="bg-white p-relative horizontal">
        <div class="form-inline grid-item">
          <div class="col-xs-4 text-center">
           <img class="rounded-circle img-fluid" src="<?php echo $TouristAttractions->path_picture_tourist_attraction; ?>" alt="user-header">
         </div>
         <div class="col-xs ">
           <h3 style="color:#0b0b0b"><?php echo $this->Html->link($TouristAttractions->name, array('controller' => 'TouristAttraction','action'=> 'bloc'."/".$TouristAttractions->id), array( 'style'=> 'color:#0b0b0b; font-size:35px; text-decoration:none;'));?></h3>

           <h4 style= "line-height:10pt" ><?php echo __("Province") . $TouristAttractions['mp']['province_name_th']; ?></h4>

           <?php $review = $this->DateFormat->star($TouristAttractions->id); ?>
           <?php if ($review > 0 ) { ?>
            <!-- <p style="color:white ">.....</p> -->
            <p class="score" style="color:#fff; text-align:left;">
              <i class="fa fa-star-o " style="text-align:right;"></i><?php echo __($review); ?></p>

            <?php } ?>
          </div>


              <!-- <div class="col-sm-2 text-center">
                <p><font size="3"> 17 กม.</font></p>
              </div> -->

            </div>

            <br>
            <?php $TouristAttraction_image = $this->DateFormat->Attraction($TouristAttractions->id); 
            $i = 0; ?>

            <div class="owl-carousel owl-theme">
             <?php foreach($TouristAttraction_image as $key=>$TouristAttraction_images) { ?> 
              <div class="item "> <img src="<?php  echo $TouristAttraction_images['path_image']; ?>" /> </div>
            <?php } ?>
          </div>


          <!-- </div> -->
        </div>
        <!-- <br> -->
        <hr>

      <?php  }  ?>
    <?php }else { ?>
        <div class = "table-responsive">
                <table class="table m-b-0 photo-table page-table-not-found">
                    <tbody>
                        <tr>
                            <th style="text-align: center;">
                                <?php echo __('Province Not Have Tourist'); ?>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
    <?php } ?>
  </div>
</div>
</div>
















<style>
.CoverImage {

 cursor:default;
 position:relative;
 top:0px;
 left:0px;
 width: auto;
 height:300px;
 overflow:hidden;
}
.CoverImage2 {

 cursor:default;
 position:relative;
 top:0px;
 left:25px;
 right: 0px;
 width:250px;
 height:250px;
 overflow:hidden;
}

figure{
 width:100px; 
 overflow:hidden; 
 margin: auto;   
}
figure img{
 display:block; 
 max-width:150px; 
 margin: auto;
}

.btn2 {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  background-color: #fff;
  color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: 3px solid #b30000;
  cursor: pointer;
  border-radius: 25px;

}
/*.select2 {
  background: none;
  color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;

  }*/

  .bottom-right {
    position: absolute;
    bottom: 30%;
    right: -46%;
  }

  /*----------------------------------------------*/

  .score {
    border-radius: 1px;
    background: #b30000;
    padding: 5px 10px;
    width:59.7px;
    height: 25px;   


  }


  .i {
    height: auto;
    max-width: 100%;
    width: 100%;
  }

  .example_c {
    color: black;
    text-transform: uppercase;
    background: #D3D3D3;
    padding: 5px;
    border: 2px solid black ;
    border-radius: 6px;
    display: inline-block;
    text-decoration:none;
  }

  .example_c:hover {
    color:white ;
    background: #b30000;
    border-color: #D3D3D3 ;
    transition: all 0.4s ease 0s;
    text-decoration:none;
  }

</style>
<script >
 $(function () {

  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:5,
    nav:true,
    dots:false,
    merge:true,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:3
      },
      1000:{
        items:3
      }
    }
  });
});
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
