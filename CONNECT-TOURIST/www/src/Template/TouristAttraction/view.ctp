<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TouristAttraction $touristAttraction
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tourist Attraction'), ['action' => 'edit', $touristAttraction->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tourist Attraction'), ['action' => 'delete', $touristAttraction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $touristAttraction->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tourist Attraction'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tourist Attraction'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Category'), ['controller' => 'Category', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Category', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List MasterProvince'), ['controller' => 'MasterProvince', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New MasterProvince'), ['controller' => 'MasterProvince', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Review Tourist'), ['controller' => 'ReviewTourist', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Review Tourist'), ['controller' => 'ReviewTourist', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="touristAttraction view large-9 medium-8 columns content">
    <h3><?= h($touristAttraction->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($touristAttraction->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category') ?></th>
            <td><?= $touristAttraction->has('category') ? $this->Html->link($touristAttraction->category->name, ['controller' => 'Category', 'action' => 'view', $touristAttraction->category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Path Picture Tourist Attraction') ?></th>
            <td><?= h($touristAttraction->path_picture_tourist_attraction) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('MasterProvince') ?></th>
            <td><?= $touristAttraction->has('province') ? $this->Html->link($touristAttraction->province->id, ['controller' => 'MasterProvince', 'action' => 'view', $touristAttraction->province->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Review Tourist') ?></th>
            <td><?= $touristAttraction->has('review_tourist') ? $this->Html->link($touristAttraction->review_tourist->id, ['controller' => 'ReviewTourist', 'action' => 'view', $touristAttraction->review_tourist->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($touristAttraction->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Latitude') ?></th>
            <td><?= $this->Number->format($touristAttraction->latitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Longtitude') ?></th>
            <td><?= $this->Number->format($touristAttraction->longtitude) ?></td>
        </tr>
    </table>
</div>
