<div class="horizontal">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
     <ul class="navbar horizontal" style="background-color:#b30000;">
      <div class="col">
        <div class="form-inline">
          <li role="presentation" class="active"><a href="../TouristAttraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
          <li role="presentation" ><a href="http://tourist-demo.pakgon.local:5052/TouristAttraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>
      </div>
  </div>
  <div class="col">
    <button type="button" class="btn btn-default navbar-btn pull-right"  style="text-decoration:none; color:#b30000; border:none;" >SIGN OUT</button>
</div>
</ul>
<div class="bg-white p-relative horizontal">
    <br>
    <div class="col">
        <h3 class="text-left text-bold"><?php echo __('Search Province And TouristAttraction'); ?></h3>
    </div>

    <div class = "col-lg-9 pull-lg-3 col-md-12">
        <?php echo $this->Form->create('Search', ['id' => 'search_form']); ?>    

        <div class="input-group mb-3">
           <input type="text" name="search" id="search" class="form-control-lg" placeholder="<?php echo __('Search'); ?>" value = "<?php echo empty($search)? '':$search; ?>" aria-describedby="touristAttraction-search">
           <div class="input-group-append" id = "button_search">
             <span class="input-group-text" id="touristAttraction-search" > <i class="fa fa-2x fa fa-search pull-right" style="color: #b30000;"></i></span>
         </div>
     </div>

     <?php echo $this->Form->end(); ?>
 </div>
 <br>
 <?php 
 if(@$chkPost == true){
    ?>
    <div class="col">
        <h3 class="text-left text-bold"><?php echo __('Province And TouristAttraction Result'); ?></h3>
    </div>
    <?php 
    if(!empty($touristAttraction)){
        ?>
        <div class="bg-white p-relative horizontal">
            <div class = "table-responsive">
                <table class="table m-b-0 photo-table touristAttraction-table">
                    <tbody>
                            <?php if(!empty($chkSearchProvince)){ ?>
                                <?php foreach ($chkSearchProvince as $key => $chkSearchProvinces) { ?>
                                        <tr>
                                            <td>
                                                <a class="text-page" href="/tourist-attraction/index/<?php echo $chkSearchProvinces['id'];?>">
                                                    <?php echo $this->Html->image($chkSearchProvinces['logo_of_province'], [
                                                        "class" => "img-circle",
                                                    ]); ?>
                                                </a>
                                            </td>
                                            <td class="align-middle">
                                                <a class="text-page text-dark" href="/tourist-attraction/index/<?php echo $chkSearchProvinces['id']; ?>">
                                                    <div class="cut-text-multi">                
                                                        <?php echo $chkSearchProvinces['province_name_th']; ?>
                                                            <!-- <small>
                                                                <?php //echo $page->page_desc; ?> 
                                                            </small> -->
                                                        </div>
                                                    </a>
                                                </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            <?php 
                            foreach ($touristAttraction as $touristAttraction){
                                $provinceLogo = $touristAttraction['mmpv']['logo_of_province'];
                                $provinceName = $touristAttraction['mmpv']['province_name_th'];
                                    // $provinceId = $touristAttraction['mmpv']['id'];
                                ?>
                                <tr>
                                    <td>
                                        <a class="text-page" href="/tourist-attraction/bloc/<?php echo $touristAttraction->id; ?>">
                                            <?php echo $this->Html->image($provinceLogo, [
                                                "class" => "img-circle",
                                            ]); ?>
                                        </a>
                                    </td>
                                    <td class="align-middle">
                                        <a class="text-page text-dark" href="/tourist-attraction/bloc/<?php echo $touristAttraction->id; ?>">
                                            <div class="cut-text-multi">				
                                                <?php echo $provinceName; ?>                        
                                                <!-- <small>
                                                    <?php //echo $page->page_desc; ?> 
                                                </small> -->
                                            </div>
                                        </a>
                                    </td>
                                    <td class="align-middle">
                                        <a class="text-page text-dark" href="/tourist-attraction/bloc/<?php echo $touristAttraction->id; ?>">
                                            <div class="cut-text-multi">                
                                                <?php echo $touristAttraction->name; ?>                
                                                <!-- <small>
                                                    <?php //echo $page->page_desc; ?> 
                                                </small> -->
                                            </div>
                                        </a>
                                    </td>
                               <!-- <td class="align-middle">                                
                                <?php //echo (@$isSubscribed[$page->id]) ? $this->Permission->button(__('Unsubscribe'), null, ['class' => 'btn btn-danger waves-effect btn-block pull-right btn-subscribe', 'data-page_id'=> $page->id]) : $this->Permission->button(__('Subscribe'), null, ['class' => 'btn btn-primary waves-effect btn-block pull-right btn-subscribe', 'data-page_id'=> $page->id]); ?>
                            </td>-->
                                </tr>
                        <?php 
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php 
}else if(empty($touristAttraction)){
  ?>
    <div class="bg-white p-relative horizontal">
            <div class = "table-responsive">
                <table class="table m-b-0 photo-table touristAttraction-table">
                    <tbody>
                            <?php if(!empty($chkSearchProvince)){ ?>
                                <?php foreach ($chkSearchProvince as $key => $chkSearchProvinces) { ?>
                                        <tr>
                                            <td>
                                                <a class="text-page" href="/tourist-attraction/index/<?php echo $chkSearchProvinces['id']; ?>">
                                                    <?php echo $this->Html->image($chkSearchProvinces['logo_of_province'], [
                                                        "class" => "img-circle",
                                                    ]); ?>
                                                </a>
                                            </td>
                                            <td class="align-middle">
                                                <a class="text-page text-dark" href="/tourist-attraction/index/<?php echo $chkSearchProvinces['id']; ?>">
                                                    <div class="cut-text-multi">                
                                                        <?php echo $chkSearchProvinces['province_name_th']; ?>
                                                            <!-- <small>
                                                                <?php //echo $page->page_desc; ?> 
                                                            </small> -->
                                                        </div>
                                                    </a>
                                                </td>
                                        </tr>
                                    <?php } ?>
                                <?php }else{ ?>
                                      <tr>
                                          <th style="text-align: center;">
                                              <?php echo __('Page Not Found'); ?>
                                          </th>
                                      </tr>
                                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php 
}else{
    ?>
    <div class = "row">
        <div class = "col-lg-9 pull-lg-3 col-md-12">

            <div class = "table-responsive">
                <table class="table m-b-0 photo-table page-table-not-found">
                    <tbody>
                        <tr>
                            <th style="text-align: center;">
                                <?php echo __('Page Not Found'); ?>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}
}
?>
</div>
</div>
</div>
</div>

<?php $this->append('scriptBottom'); ?>
<script type="text/javascript">
    $(function () {

  //       /**
  //        * 
  //        * Click subscribe action
  //        * @author sarawutt.b
  //        * @edit By prasong
  //        */
  //       $('.btn-subscribe').on('click', function () {
  //           var $elClick = $(this);       
  //           $.post('/UserSubscribes/subscribeUnsubscribe.json', {page_id: $elClick.data('page_id') }, function (result) {
  //               var unsubscribeClass = 'btn btn-danger waves-effect btn-block pull-right btn-subscribe';
  //               var subscribeClass = 'btn btn-primary waves-effect btn-block waves-light pull-right btn-subscribe';
  //               if ((typeof result.response !== 'undefined') && (result.response.status == 'OK')) { 
  //                   if ($elClick.attr('class') == unsubscribeClass) {
  //                       $elClick.attr('class', subscribeClass);
  //                       $elClick.text('<?php echo __('Subscribe'); ?>');                 
  //                   } else {
  //                       $elClick.attr('class', unsubscribeClass);
  //                       $elClick.text('<?php echo __('Unsubscribe'); ?>');
  //                   }
  //               }
  //               return true;
  //           });
  //       });

  $('#button_search').on('click', function () {
    var str = $( "#search" ).val();
    if(str){
        $("#search_form").submit();
    }
});

        // $('#button_search').on('click', function () {
        //     if($( "#search" ).val('')){
        //           $("#search_form").submit();
        //           // window.location.replace('/tourist-attraction/searchProvince');
        //         }
        // });

    });
</script>
<style>


.page-table > tbody > tr:first-child td {
    border-top:0px !important;
}

.page-table > tbody tr td:first-child {
    padding-left:0px;
    padding-right:0px; 
    width:50px;
    text-align:center;
}
.page-table > tbody tr td:first-child img{
  width:100%;
}
//.page-table > tbody tr td:last-child {
 // max-width:100px;
 //    width:100px;
 //    padding:0px;   
 //}

 .page-table > tbody td button{
    margin-top:10px;
}
.page-table > tbody td a:hover{
    text-decoration: none;
}
//.page-table-not-found > tbody > tr > th {
 //   text-align:center;
 //}

 ::-webkit-input-placeholder { /* WebKit browsers */

    line-height: 2.3em;
}

@media only screen and (max-width: 480px){
	.table td, .table th {
     font-size: 16px;
     padding: 7px 10px;
 }
}

.cut-text-multi{

    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: -o-ellipsis-lastline;
}

</style>
<?php $this->end(); ?>
