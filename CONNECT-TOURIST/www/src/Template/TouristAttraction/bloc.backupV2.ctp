<?php

use Cake\I18n\Time;

// $this->layout = 'Ablepro6.mobile_slime';
?>
<div class="row">
    <div class="col-lg-9 pull-lg-3 col-md-12">
        <div class="card text-center">
            <script src="/js/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              // $AutoPlay: 1,        
              // $Idle: 5000,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            // var MAX_WIDTH = 980; //origin
            var MAX_WIDTH = 900;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb052 .i {position:absolute;cursor:pointer;}
        .jssorb052 .i .b {fill:#000;fill-opacity:0.3;}
        .jssorb052 .i:hover .b {fill-opacity:.7;}
        .jssorb052 .iav .b {fill-opacity: 1;}
        .jssorb052 .i.idn {opacity:.3;}

        .jssora053 {display:block;position:absolute;cursor:pointer;}
        .jssora053 .a {fill:none;stroke:#fff;stroke-width:640;stroke-miterlimit:10;}
        .jssora053:hover {opacity:.8;}
        .jssora053.jssora053dn {opacity:.5;}
        .jssora053.jssora053ds {opacity:.3;pointer-events:none;}
    </style>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <!---------------- test loop img src slider  ----------------- -->
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
            <!-- <div>
                <img data-u="image" src="<?php echo $touristAttraction_image_type_one['0']['path_image'] ?>" />
            </div>
            <div>
                <img data-u="image" src="<?php echo $touristAttraction_image_type_one['1']['path_image'] ?>" />
            </div>
            <div>
                <img data-u="image" src="<?php echo $touristAttraction_image_type_one['2']['path_image'] ?>" />
            </div>
            <div>
                <img data-u="image" src="<?php echo $touristAttraction_image_type_one['3']['path_image'] ?>" />
            </div> -->

            <div>
                <?php for ( $i = 0 ; $i < count($touristAttraction_image_type_one) ; $i++): ?>
                    <img data-u="image" src="<?php echo $touristAttraction_image_type_one[$i]['path_image'] ?>" >
                <?php endfor; ?>
            </div>

            <<!-- div>
                <img data-u="image" src="test-product-test.png" />
            </div> -->
        </div>
        <!------------------------------------------------------------->
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <!-- <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div> -->
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
        </div>
    </div>
</div>

<div class="row">
    <?php if (!empty($touristAttraction_find_id)): ?>
        <div class="col-lg-9 pull-lg-3 col-md-12">
            <?php foreach ($touristAttraction_find_id as $touristAttraction_find_id): ?>
                <div class="bg-white p-relative">
                    <div class="card-block post-timelines">
                        <div class="media-middle friend-box float-left">
                            <a href="/tourist-attraction/bloc/<?php echo $touristAttraction_find_id->id; ?>">
                                <?php echo $this->Html->image($touristAttraction_find_id['path_picture_tourist_attraction'], ['class' => 'media-object rounded-circle']); ?>
                            </a>
                        </div>
                        <div class="media-middle friend-box float-center">
                                <h2> <?php echo $touristAttraction_find_id['name']; ?> </h2>
                        </div>
                        <div class="media-middle friend-box float-center">
                                <?php echo __("Province") . $MasterProvince['0']['province_name_th']; ?>
                        </div>
                        <div class="media-middle friend-box float-bottom">  
                                <?php echo $this->Utility->thaiDateFullMonth($create_time); ?>

                            <div class="friend-details pull-right">
                                <div class="col">
                                    <?php //if (@$arrLike[$touristAttraction_find_id->id]): ?>
                                        <!--<a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-up like-true"></i>-->
                                    <?php if(!empty($arrLike)): ?>
                                        <a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-up like-true"></i>
                                        <!-- test -->
                                        <!--<a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-o-up"></i>-->
                                    <?php else: ?>
                                        <a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-o-up"></i></a>
                                    <?php endif; ?>
                                    <div class="count-like text-bold pull-right">
                                        <?php echo ($touristAttraction_find_id->count_like < 1000) ? $touristAttraction_find_id->count_like : round($touristAttraction_find_id->count_like / 1000, 1) . " K"; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-10"></div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
        <div class="col-lg-9 pull-lg-3 col-md-12">
            <div class="bg-white p-relative">
                    <div class="card-block post-timelines">
                        <div align="center" >
                            <a href="/tourist-attraction/star/<?php echo $touristAttraction_find_id->id; ?>">
                                <?php echo $this->Html->image("../icon/64x64/star.png", ['class' => 'img-fluid']); ?> 
                            </a>

                            <a href="/tourist-attraction/camera/<?php echo $touristAttraction_find_id->id; ?>">
                                <?php echo $this->Html->image("../icon/64x64/camera2.png", ['class' => 'img-fluid']); ?> 
                            </a>
                        </div>
                        <div align="center" >
                            <!-- <img src="../icon/star.png" height="100" width="100"> -->
                            <!-- <img src="../icon/camera.jpg" height="100" width="100"> -->
                        </div>
                    </div>
            </div>
        </div>
</div>
<br>
<div class="row">
        <div class="col-lg-9 pull-lg-3 col-md-12">
            <div class="bg-white p-relative">
                    <div class="card-block post-timelines">
                        <div align="center" >
                            <p >
                                <?php echo $touristAttraction_find_id['detail_tourist']; ?>
                            </p>
                        </div>
                        <div align="center" >
                            <!-- <img src="../icon/star.png" height="100" width="100"> -->
                            <!-- <img src="../icon/camera.jpg" height="100" width="100"> -->
                        </div>
                    </div>
            </div>
        </div>
</div>

<?php $this->append('script'); ?>
<script type="text/javascript">
    console.log("javascript start");
    $(function () {
        console.log("get in function");
        $('.btn-like').click(function () {
            var $elClick = $(this);
            var $elCountLike = $elClick.closest('.col').find('.count-like');
            $.post('/tourist-attraction/like', {tourist_attraction_id: $elClick.data('tourist_attraction_id')}, function (result) {
                result = jQuery.parseJSON(result);
                console.log(result);
                var tagIclass_like = "fa fa-lg fa-thumbs-up like-true";
                var tagIclass_unlike = "fa fa-lg fa-thumbs-o-up";
                if ((typeof result !== 'undefined') && (result.status == 'OK')) {
                    if ($elClick.find('i').attr('class') == tagIclass_unlike) {
                        $elClick.find('i').removeAttr('class', tagIclass_unlike);
                        $elClick.find('i').attr('class', tagIclass_like);
                        var countLike = parseInt($elCountLike.text()) || 0;
                        $elCountLike.text(countLike + 1);
                    } else if($elClick.find('i').attr('class') == tagIclass_like) {
                        $elClick.find('i').removeAttr('class', tagIclass_like);
                        $elClick.find('i').attr('class', tagIclass_unlike);
                        var countLike = parseInt($elCountLike.text()) || 0;
                        $elCountLike.text(countLike - 1);   
                    }
                }
            });
        });

    });
</script>
<?php echo $this->element('utility/beacon'); ?>
<?php $this->end(); ?>