<div class="horizontal">
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2">
			<ul class="navbar horizontal" style="background-color:#b30000">
				<div class="col">
					<div class="form-inline">
						<li role="presentation" class="active"><a href="/tourist-attraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
						<li role="presentation" ><a href="/tourist-attraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>

						<!-- <?php echo $this->Html->link("logout", array('controller' => 'TouristAttraction','action'=> 'logout'), array( 'style'=> 'color:#fff; text-decoration:none;'));?> -->
					</div>
				</div>
				<div class="col">
					<button type="button" class="btn btn-default navbar-btn pull-right"  style="text-decoration:none; color:#b30000;" >SING OUT</button>
				</div>
			</ul>
			<div class="bg-white p-relative horizontal">
				<div class="card-block post-timelines">
					<div class="form-inline grid-item">
						
						<?php if (!empty($touristAttraction_find_id)): ?>
							<?php foreach ($touristAttraction_find_id as $touristAttraction): ?>
								<div class="col-sm-4 text-center">
									<img class="rounded-circle img-fluid" src="<?php echo $touristAttraction['path_picture_tourist_attraction']; ?>" alt="user-header">
								</div>
								<div class="col-sm">
									<h1 style="color:#0b0b0b"><?php echo $this->Html->link($touristAttraction['name'], array('controller' => 'TouristAttraction','action'=> 'bloc'."/".$touristAttraction['id']), array( 'style'=> 'color:#0b0b0b; font-size:35px; text-decoration:none;'));?></h1>

									<h4 style= "line-height:10pt" ><?php echo __("Province") . $MasterProvince['0']['province_name_th']; ?></h4>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				
					<div class="text-center">
						<form class="text-center" id="form1" name="form1" method="post">
							<ul class="iVote ">
								<li class="text-center <?php echo $reviewTourist[0]['star'] >= 1 ? 'VoteD2': '' ; ?>"></li>
								<li class="text-center <?php echo $reviewTourist[0]['star'] >= 2 ? 'VoteD2': '' ; ?>"></li>
								<li class="text-center <?php echo $reviewTourist[0]['star'] >= 3 ? 'VoteD2': '' ; ?>"></li>
								<li class="text-center <?php echo $reviewTourist[0]['star'] >= 4 ? 'VoteD2': '' ; ?>"></li>
								<li class="text-center <?php echo $reviewTourist[0]['star'] >= 5 ? 'VoteD2': '' ; ?>"></li>
							</ul>
							<input name="hScore1" type="hidden" id="hScore1" value="0" />
							<span class="showVoteText"></span>      

							<br style="clear:both;" /> 
							<br style="clear:both;" /> 
							<button class="text-center example_c" type="submit"  id="save_star" ><?php echo __('save'); ?></button>
						</form>
							<!-- <input class="pull-right" type="submit" name="button" id="save_star" value="Submit" />
							<p class="pull-center">....</p>
							<p class="pull-center">....</p>
							<p class="pull-center">....</p> -->
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

<script>
	$(function(){
		var arrTextVote=new Array("Not so great","Quite good","Good","Great!","Excellent!");
		var check_variable_reviewTourist = JSON.parse('<?php echo json_encode($reviewTourist); ?>');

		if(check_variable_reviewTourist[0]['star'] == 0){
			$("ul.iVote li").mouseover(function(){
				var prObj=$(this).parent("ul");
				var Clto=prObj.children("li").index($(this));
				var Clto2=Clto;
				Clto+=1;
				prObj.children("li:gt("+Clto2+")").removeClass("VoteD");		
				prObj.children("li:lt("+Clto+")").addClass("VoteD");
				prObj.nextAll("span.showVoteText:eq(0)").html(arrTextVote[Clto2]);
				var oldScore=prObj.next("input").val();
				prObj.mouseout(function(){
					prObj.children("li").removeClass("VoteD");		
					prObj.nextAll("span.showVoteText:eq(0)").html("");
				});
			});
			$("ul.iVote li").click(function(){
				var prObj=$(this).parent("ul");
				if(prObj.children("li").hasClass("VoteD2")==false){			
					var Clto=prObj.children("li").index(this);
					var Clto2=Clto;
					Clto+=1;	
					prObj.next("input").val(Clto);	
					prObj.children("li:lt("+Clto+")").addClass("VoteD2");
					prObj.children("li:gt("+Clto+")").removeClass("VoteD");			
					prObj.children("li").unbind("mouseover");	
					prObj.unbind("mouseout");	
					prObj.nextAll("span.showVoteText:eq(0)")
					.html(arrTextVote[Clto2]);
				}
			});
		}
	});
</script>
<style type="text/css">

.iVote,.iVote li{
	display:block;
	margin:10px;
	padding:0px;
	list-style:none;
	float:left;
	top:50%;
	left:80%;
}
.iVote{
	clear:both;     
	float:left;
}
.iVote li,.iVote li.VoteD{
	display:block;
	width:16px;
	height:15px;
	position:relative;
	background: url(/img/star.gif) no-repeat 0 0;
	background-position: 0 -32px;
	margin-right:3px;
	cursor:pointer;
}
.iVote li.VoteD{
	background-position: 0 -64px;
}
.iVote li.VoteD2{
	background-position: 0 -48px;
}
.example_c {
    color: #b30000;
    text-transform: uppercase;
    background: white;
    padding: 10px;
    border: 2px solid #b30000 ;
    border-radius: 6px;
    display: inline-block;
  }

  .example_c:hover {
    color: #ffffff ;
    background: #b30000;
    border-color: #ffffff ;
    transition: all 0.4s ease 0s;
    text-decoration:none;
  }
</style> 