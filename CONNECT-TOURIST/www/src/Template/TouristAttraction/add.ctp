<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TouristAttraction $touristAttraction
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tourist Attraction'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Category'), ['controller' => 'Category', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Category', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List MasterProvince'), ['controller' => 'MasterProvince', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New MasterProvince'), ['controller' => 'MasterProvince', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Review Tourist'), ['controller' => 'ReviewTourist', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Review Tourist'), ['controller' => 'ReviewTourist', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="touristAttraction form large-9 medium-8 columns content">
    <?= $this->Form->create($touristAttraction) ?>
    <fieldset>
        <legend><?= __('Add Tourist Attraction') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('category_id', ['options' => $category, 'empty' => true]);
            echo $this->Form->control('latitude');
            echo $this->Form->control('longtitude');
            echo $this->Form->control('path_picture_tourist_attraction');
            echo $this->Form->control('province_id', ['options' => $province, 'empty' => true]);
            echo $this->Form->control('review_tourist_id', ['options' => $reviewTourist, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
