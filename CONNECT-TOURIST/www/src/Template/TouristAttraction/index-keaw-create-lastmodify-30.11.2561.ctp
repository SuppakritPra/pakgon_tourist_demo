
<div class="row">
  <div class="col-12 col-md-8 offset-md-2">
   <ul class="navbar" style="background-color:#b30000;">
    <div class="col-sm-10">
      <div class="form-inline">
        <li role="presentation" class="active"><a href="TouristAttraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
        <li role="presentation" ><a href="http://tourist-demo.pakgon.local:5052/TouristAttraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>
      </div>
    </div>
    <div class="col-sm-2">
      <button type="button" class="btn btn-default navbar-btn"  style="text-decoration:none; color:#b30000;" >SIGN OUT</button>
    </div>
  </ul>

  <div class="bg-white p-relative">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

      <div class="carousel-inner">
       <?php foreach($image_banner as $key=>$image_banners) { ?> 

        <div class="item <?php echo ($key == 0) ? "carousel-item active" : ""; ?> ">
          <div class="CoverImage">
            <img src="<?php  echo $image_banners['banner_path']; ?>" alt="Image of every carousel"/>
          </div>
        </div>

      <?php } ?>
    </div>
  </div>

  <!--<button class="btn2" type="button" onclick="window.location.href = '/TouristAttraction/search_province'"><i class="fa fa-search"></i><?php echo __('BANGKOK'); ?></button> -->
  <button class="btn2" type="button" onclick="window.location.href = '/TouristAttraction/search_province'"><i class="fa fa-search"></i><?php echo $TouristAttraction[0]['mp']['province_name_th']; ?></button>
  <div class="col-sm-12 bottom-right">
    <a href="TouristAttraction/loopmapmarker" ><i class="fa fa-map-o fa-stack-2x bottom-right"  style="color:#b30000;"></i></a>
  </div>
</div>

<div class="bg-gray p-relative">
  <div class="card-block post-timelines">
    <div class="form-inline">
      <div class="col-sm-10">
        <h1 style="color:#2b2b2b" >สถานที่ท่องเที่ยว</h1>
      </div>
      <div class="col-sm-2">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="text-decoration:none; pull-right; color:#2b2b2b" >ใกล้ฉัน</a>
        <ul class="dropdown-menu" >
          <li value="1"><a href="">ใกล้ฉัน</a></li>
          <li value="2"><a href="">ความนิยม</a></li>
        </ul>
        <?php echo $this->Form->create($touristAttraction); ?>
        <form>
          <select name='category' onchange='this.form.submit()'>
            <option selected >เลือก</option>
            <option value="1">ใกล้ฉัน</option>
            <option value="2">ความนิยม</option>
          </select>
          <noscript><input type="submit" value="Submit"></noscript>
        </form>
      </div>
    </div>
  </div>
</div>

<?php foreach($TouristAttraction as $key=>$TouristAttractions) { ?> 
  <div class="bg-white p-relative">
    <div class="card-block post-timelines">
      <div class="form-inline">
        <div class="col-sm-10">
          <div class="row">
             <div class="col-4">
                <div class="media-middle friend-box float-left">
           <?php echo $this->Html->image($TouristAttractions->path_picture_tourist_attraction, ['class' => 'img-fluid rounded-circle','style'=>' max-height:125px;']); ?>
         </div>
             </div>
             <div class="col-8">
                <h2 style="color:#0b0b0b"><?php echo $this->Html->link($TouristAttractions->name, array('controller' => 'TouristAttraction','action'=> 'bloc'."/".$TouristAttractions->id), array( 'style'=> 'color:#0b0b0b;  ; text-decoration:none;'));?></h2>

         <div class="media-middle friend-box float-center">
          <h5 style= " line-height:10pt" ><?php echo __("Province") . $TouristAttractions['mp']['province_name_th']; ?></h6>
        </div>

        <?php $review = $this->DateFormat->star($TouristAttractions->id); ?>
        <?php if ($review > 0 ) { ?>
          <div class="media-middle friend-box float-left " >
           
            <p class="score" style="color:#fff; text-align:left;">
              <i class="fa fa-star-o " style="text-align:right;"></i><?php echo __($review); ?></p>
            </div>
          <?php } ?>
             </div>
          </div>
        <!--  <div class="media-middle friend-box float-left">
           <?php echo $this->Html->image($TouristAttractions->path_picture_tourist_attraction, ['class' => 'media-object rounded-circle','style'=>'width:125px; height:125px;']); ?>
         </div> -->
         
        <!--  <h1 style="color:#0b0b0b"><B><?php echo $this->Html->link($TouristAttractions->name, array('controller' => 'TouristAttraction','action'=> 'bloc'."/".$TouristAttractions->id), array( 'style'=> 'color:#0b0b0b;  font-size:35px; text-decoration:none;'));?></B></h1>

         <div class="media-middle friend-box float-center">
          <p style= "font-size:20px; line-height:10pt" ><?php echo __("Province") . $TouristAttractions['mp']['province_name_th']; ?></p>
        </div>

        <?php $review = $this->DateFormat->star($TouristAttractions->id); ?>
        <?php if ($review > 0 ) { ?>
          <div class="media-middle friend-box float-left " >
           
            <p class="score" style="color:#fff; text-align:left;">
              <i class="fa fa-star-o " style="text-align:right;"></i><?php echo __($review); ?></p>
            </div>
          <?php } ?> -->

        </div>
        <div class="col-sm-2 text-center">
          <p><font size="3"> 17 กม.</font></p>
        </div>

      </div>
      
      <br>
      <?php $TouristAttraction_image = $this->DateFormat->Attraction($TouristAttractions->id); 
      $i = 0; ?>

      <div id="carouselControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <?php foreach($TouristAttraction_image as $key=>$TouristAttraction_images) { ?> 

            <div class="item <?php echo ($key == 0) ? "carousel-item active" : ""; ?> ">
              <div class="CoverImage">
                <img src="<?php  echo $TouristAttraction_images['path_image']; ?>" alt="Image of every carousel"/>
              </div>
            </div>

          <?php } ?>
        </div>
        <!-- ---------------------------------------------------------------------------------------------------- -->
        <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </div>
  </div>
  <br>
<?php  }  ?>
</div>
</div>
</div>





<style>
.CoverImage {

 cursor:default;
 position:relative;
 top:0px;
 left:0px;
 width: auto;
 height:300px;
 overflow:hidden;
}
.CoverImage2 {

 cursor:default;
 position:relative;
 top:0px;
 left:25px;
 right: 0px;
 width:250px;
 height:250px;
 overflow:hidden;
}

figure{
 width:100px; 
 overflow:hidden; 
 margin: auto;   
}
figure img{
 display:block; 
 max-width:150px; 
 margin: auto;
}

.btn2 {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  background-color: #fff;
  color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: 3px solid #b30000;
  cursor: pointer;
  border-radius: 25px;

}
/*.select2 {
  background: none;
  color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;

  }*/

  .bottom-right {
    position: absolute;
    bottom: 3%;
    right: -46%;
  }

  /*----------------------------------------------*/

  .score {
    border-radius: 1px;
    background: #b30000;
    padding: 5px 10px;
    width:59.7px;
    height: 25px;    
  }

</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
