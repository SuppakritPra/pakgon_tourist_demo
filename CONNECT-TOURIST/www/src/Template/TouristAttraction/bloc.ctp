<?php

use Cake\I18n\Time;

// $this->layout = 'Ablepro6.mobile_slime';
?>
<div class="horizontal">
  <div class="row">
    <!-- <div class="col-lg-9 pull-lg-3 col-md-12"> -->
      <div class="col-12 col-md-8 offset-md-2">
        <ul class="navbar horizontal" style="background-color:#b30000">
          <div class="col ">
            <div class="form-inline">
              <li role="presentation" class="active"><a href="/tourist-attraction/index" style="text-decoration:none; color:#fff;" >HOME</a></li>&nbsp; &nbsp;
              <li role="presentation" ><a href="/tourist-attraction/ilike" style=" text-decoration:none; color:#fff;">LIKE</a></li>
            </div>
          </div>
          <div class="col">
            <button type="button" class="btn btn-default navbar-btn pull-right"  style="text-decoration:none; color:#b30000;" >SIGN OUT</button>
          </div>
        </ul>

        <div class="card text-center">
          <?php if(!empty($touristAttraction_image_type_one)){ ?>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
               <?php foreach($touristAttraction_image_type_one as $key=>$touristAttraction_image_type_ones) { ?> 

                <div class="item <?php echo ($key == 0) ? "carousel-item active" : ""; ?> ">
                  <div class="CoverImage">
                    <img src="<?php  echo $touristAttraction_image_type_ones['path_image']; ?>" alt="Image of every carousel" />
                  </div>
                </div>

              <?php } ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        <?php } else{}?>

      </div>


      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>




      <?php if (!empty($touristAttraction_find_id)): ?>
        <!-- <div class="col-lg-9 pull-lg-3 col-md-12"> -->
          <?php foreach ($touristAttraction_find_id as $touristAttraction_find_id): ?>
            <div class="col">
              <div class="bg-white p-relative horizontal">
                <div class="form-inline grid-item">
                  <!-- <div class="col-3 card-block horizontal-card-img"> -->
                    <div class="col-xs-3 text-center">
                      <!-- <div class="mr-3 horizontal-card-img"> -->
                        <img class="  rounded-circle img-fluid" src="<?php echo $touristAttraction_find_id['path_picture_tourist_attraction']; ?>" alt="user-header">
                        <!-- </div> -->
                      </div>
                      <!-- </div> -->

                      <div class="col-xs-7 ">

                        <h3> <?php echo $touristAttraction_find_id['name']; ?> </h3>
                        <h5><?php echo __("Province") . $MasterProvince['0']['province_name_th']; ?></h5>
                        <h6><?php echo $this->Utility->thaiDateFullMonth($create_time); ?></h6>

                      </div>

                      <div class="col-xs-2 pull-right">  

                        <?php //if (@$arrLike[$touristAttraction_find_id->id]): ?>
                        <!--<a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-up like-true"></i>-->
                         <div class="count-like text-bold pull-right">
                          <?php echo ($touristAttraction_find_id->count_like < 1000) ? $touristAttraction_find_id->count_like : round($touristAttraction_find_id->count_like / 1000, 1) . " K"; ?>
                        </div>
                        <?php if(!empty($arrLike)): ?>
                          <a class="btn-like pull-right" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><img src="/icon/favorite_after.png" style="width:32px; height:32px; margin:5px;"></a>
                          <!-- test -->
                          <!--<a class="btn-like" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><i class="fa fa-lg fa-thumbs-o-up"></i>-->
                            <?php else: ?>
                              <a class="btn-like pull-right" data-tourist_attraction_id='<?php echo $touristAttraction_find_id->id; ?>'><img src="/icon/favorite_before.png" style="width:32px; height:32px;"></a>
                            <?php endif; ?>

                          </div>
                        </div>


                      </div>
                    </div>

                  <?php endforeach; ?>

                <?php endif; ?>
                <hr>
                <div align="center" >
                  <div class="sidebar-social">
                    <ul>
                     <?php if(empty($check_user_review)) { ?>
                        <li><a href="/tourist-attraction/star/<?php echo $touristAttraction_find_id->id; ?>" style="text-decoration:none;" title="Review" target="_blank" rel="nofollow">
                          <i class="fa fa-star-o fa-3x" style="color:#333333"><span>ให้คะแนน</span></i>
                        </a></li>
                      <?php }else { ?>
                        <li><a href="/tourist-attraction/star/<?php echo $touristAttraction_find_id->id; ?>" style="text-decoration:none;" title="Review" target="_blank" rel="nofollow">
                          <i class="fa fa-star-o fa-3x" style="color:#FAF74C"><span>ให้คะแนน</span></i>
                        </a></li>
                      <?php } ?>

                      &nbsp;
                      <li><a href="/tourist-attraction/camera/<?php echo $touristAttraction_find_id->id; ?>" style="text-decoration:none;" title="Images" target="_blank" rel="nofollow">
                        <i class="fa fa-camera-retro fa-3x" style=" color:#333333"><span>รูปภาพ</span></i>
                      </a></li>
                    </div>
                  </div>
                  <!-- <hr> -->
                  <div class="bg-white p-relative">
                    <div class="card-block post-timelines">
                      <!-- <div align="center" > -->
                        <h5><b>รายละเอียดสถานที่</b></h5>

                        <p style="text-indent: 2.5em;">
                          <?php echo $touristAttraction_find_id['detail_tourist']; ?>
                        </p>
                        <!-- </div> -->
                      </div>
                    </div>
                    <div class="card text-center">
                      <?php if(!empty($touristAttraction_image_type_two)){ ?>
                      
                          <div class="owl-carousel owl-theme">
                            <?php foreach($touristAttraction_image_type_two as $key=>$touristAttraction_image_type_twos) { ?> 
                              <div class="item "> <img src="<?php  echo $touristAttraction_image_type_twos['path_image']; ?>" /> </div>
                            <?php } ?>
                          </div>
                           <?php } else{}?>
                      </div>
                      <div class="bg-white p-relative">
                    <div class="card-block post-timelines">
                        <h5><b>ช่วงเวลาท่องเที่ยว</b></h5>
                        <p >
                          <?php echo $touristAttraction_find_id['time']; ?>
                        </p>
                        <h5><b>ติดต่อ-สอบถาม</b></h5>
                        <p >
                          <?php echo $touristAttraction_find_id['contact']; ?>
                        </p>
                        <h5><b>การเดินทาง</b></h5>
                        <p >
                          <?php echo $touristAttraction_find_id['itinerary']; ?>
                        </p>
                      </div>
                    </div>
                    </div>
                 
                </div>
              </div>
            </div>
          </div>

          <?php $this->append('script'); ?>
          <script type="text/javascript">
            $(function () {
              $('.btn-like').click(function () {
                var $elClick = $(this);
                var $elCountLike = $elClick.closest('.col').find('.count-like');
                $.post('/tourist-attraction/like', {tourist_attraction_id: $elClick.data('tourist_attraction_id')}, function(result){
                  result = jQuery.parseJSON(result);
                  var tagIclass_like = "/icon/favorite_after.png";
                  var tagIclass_unlike = "/icon/favorite_before.png";
                  if ((typeof result !== 'undefined') && (result.status == 'OK')) {
                    if ($elClick.find('img').attr('src') == tagIclass_unlike) {
                      $elClick.find('img').removeAttr('src', tagIclass_unlike);
                      $elClick.find('img').attr('src', tagIclass_like);
                      var countLike = parseInt($elCountLike.text()) || 0;
                      $elCountLike.text(countLike + 1);
                    } else if($elClick.find('img').attr('src') == tagIclass_like) {
                      $elClick.find('img').removeAttr('src', tagIclass_like);
                      $elClick.find('img').attr('src', tagIclass_unlike);
                      var countLike = parseInt($elCountLike.text()) || 0;
                      $elCountLike.text(countLike - 1);   
                    }
                  }
                });
              });

              $('.owl-carousel').owlCarousel({
                loop:true,
                margin:5,
                nav:true,
                dots:false,
                merge:true,
                responsive:{
                  0:{
                    items:1
                  },
                  600:{
                    items:1
                  },
                  1000:{
                    items:1
                  }
                }
              });

            });
          </script>
          <?php $this->end(); ?>

          <style>
          .CoverImage {

           position:relative;
           margin:0 auto;
           top:0px;
           left:0px;
           width: auto;
           height:300px;
           overflow:hidden;
         }
         .CoverImage2 {

           position:relative;
           margin:0 auto;
           top:0px;
           left:0px;
           width:980px;
           height:380px;
           overflow:hidden;
         }


         .sidebar-social {
          margin: 0;
          padding: 0;
        }

        .sidebar-social ul {
          margin: 0;
          padding: 5px;
        }

        .sidebar-social li {
          text-align: center;
          width: 20%;
          margin-bottom: 3px!important;
          background-color: #fff;
          display: inline-block;
          font-size: 10px;
          padding:0;
        }

        .sidebar-social i {
          display: block;
          margin: 0 auto 10px auto;
          width: 50px;
          height: 32px;
          margin: 10px auto 0;
          line-height: 15px;
          text-align: center;
          font-size: 20px;
          margin-top:0;
          padding-top:5px;
        }
        .sidebar-social a{
          text-decoration:none;
          width:100%;
          height:100%;
          display:block;
          margin:0;
          padding:0;
        }

        .sidebar-social a span{
          color:black;
          font-size:10px;
          padding:5px 0 10px 0;
          display:block;
          text-transform:uppercase;
          letter-spacing:1px;
        }

        .sidebar-social a:hover i.fa-star-o{ color: #b30000; }
        .sidebar-social a:hover i.fa-camera-retro { color:#b30000 }

      </style>

