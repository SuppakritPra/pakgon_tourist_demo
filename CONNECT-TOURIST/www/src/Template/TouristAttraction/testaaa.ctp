<?php
/**
<script type="text/javascript">

    function positionCallback(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        console.log(pos);
        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pos.lat + "%2C" + pos.lng + "&language=th&region=TH&sensor=false";
        $.post('/tourist-attraction/testbbb.json', {url: url}, function (response) {
            //Load Feed home Page
            //loadHomeFeed(response.geolocations.province.long_name);
        });
    }

    $(document).ready(function () {
        //var httpMethod = $.trim($('#http-method').attr('content')).toUpperCase();
        var httpMethod = 'HTTPS';
        var delay = 30;
        console.log(delay);
        if ((typeof Connect !== 'undefined') && (typeof Connect.geolocation !== 'undefined')) {
            setTimeout(function () {
                Connect.geolocation.getCurrentPosition('normal');
            }, delay);
        } else if (typeof geolocation !== 'undefined') {
            setTimeout(function () {
                geolocation.getCurrentPosition('normal');
            }, delay);
        } else if ((httpMethod == 'HTTPS') && (typeof navigator.geolocation !== 'undefined')) {
            navigator.geolocation.getCurrentPosition(positionCallback);
        } else {
            loadHomeFeed('กรุงเทพมหานคร');
        }
    });
</script>
*/

?>

<p><button onclick="geoFindMe()">Show my location</button></p>
<div id="out"></div>
<style type="text/css">
    body {
      padding: 20px;
      background-color:#ffffc9
  }

  p { margin : 0; }
</style>

<script type="text/javascript">

    function geoFindMe() {
      var output = document.getElementById("out");

      if (!navigator.geolocation){
        output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
        return;
    }

    function success(position) {
        var latitude  = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log(latitude);
        console.log(longitude);
        output.innerHTML = '<p>Latitude is ' + latitude + '° <br>Longitude is ' + longitude + '°</p>';

        var img = new Image();
        img.src = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=300x300&sensor=false";

        output.appendChild(img);
    }

    function error() {
        output.innerHTML = "Unable to retrieve your location";
    }

    output.innerHTML = "<p>Locating…</p>";

    navigator.geolocation.getCurrentPosition(success, error);
}

</script>