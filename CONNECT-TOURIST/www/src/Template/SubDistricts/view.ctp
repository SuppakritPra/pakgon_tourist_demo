<?php

 /**
  *
  * The template view for view as of subDistricts controller the page show for subDistricts information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SubDistrict $subDistrict
  * @since  2018-04-20 18:21:04
  * @license Pakgon.Ltd
  */
?>
<div class="subDistricts view subDistricts-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sub District Management System => ({0} information)', h($subDistrict->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Sub Districts Name'); ?></td>
                <td class="table-view-detail"><?php echo h($subDistrict->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Sub Districts Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($subDistrict->name_eng); ?></td>
            </tr>
                                    <tr>
                <td class="table-view-label"><?php echo __('Region'); ?></td>
                <td class="table-view-detail"><?php echo $subDistrict->has('region') ? $this->Html->link($subDistrict->region->name, ['controller' => 'Regions', 'action' => 'view', $subDistrict->region->id]) : ''; ?></td>
            </tr>
                            <tr>
                <td class="table-view-label"><?php echo __('Province'); ?></td>
                <td class="table-view-detail"><?php echo $subDistrict->has('province') ? $this->Html->link($subDistrict->province->name, ['controller' => 'Provinces', 'action' => 'view', $subDistrict->province->id]) : ''; ?></td>
            </tr>
                            <tr>
                <td class="table-view-label"><?php echo __('District'); ?></td>
                <td class="table-view-detail"><?php echo $subDistrict->has('district') ? $this->Html->link($subDistrict->district->name, ['controller' => 'Districts', 'action' => 'view', $subDistrict->district->id]) : ''; ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($subDistrict->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Sub Districts Id'); ?></td>
                <td class="table-view-detail"><?php echo h($subDistrict->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Code'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($subDistrict->code); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($subDistrict->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($subDistrict->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($subDistrict->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($subDistrict->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
