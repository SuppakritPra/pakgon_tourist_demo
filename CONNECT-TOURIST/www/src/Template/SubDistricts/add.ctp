<?php
/**
  * 
  * add sub districts template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SubDistrict $subDistrict
  * @since   2018/04/20 18:21:04
  * @license pakgon.Ltd.
  */
?>
<div class="subDistricts subDistricts-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Sub District Management System => ( Add Sub District )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($subDistrict, ['horizontal' => true]);
            echo $this->Form->control('code');
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('region_id', ['options' => $regions, 'empty' => true]);
            echo $this->Form->control('province_id', ['options' => $provinces]);
            echo $this->Form->control('district_id', ['options' => $districts]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/SubDistricts/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add sub district ?'), 'data-confirm-title' => __('Confirm for add sub district ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
