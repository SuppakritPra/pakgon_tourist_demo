<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Category'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Category'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sub Category'), ['controller' => 'SubCategory', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sub Category'), ['controller' => 'SubCategory', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tourist Attraction'), ['controller' => 'TouristAttraction', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="category view large-9 medium-8 columns content">
    <h3><?= h($category->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Sub Category') ?></h4>
        <?php if (!empty($category->sub_category)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->sub_category as $subCategory): ?>
            <tr>
                <td><?= h($subCategory->id) ?></td>
                <td><?= h($subCategory->name) ?></td>
                <td><?= h($subCategory->category_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SubCategory', 'action' => 'view', $subCategory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SubCategory', 'action' => 'edit', $subCategory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SubCategory', 'action' => 'delete', $subCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subCategory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tourist Attraction') ?></h4>
        <?php if (!empty($category->tourist_attraction)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Path Picture Tourist Attraction') ?></th>
                <th scope="col"><?= __('Province Id') ?></th>
                <th scope="col"><?= __('Review Tourist Id') ?></th>
                <th scope="col"><?= __('Detail Tourist') ?></th>
                <th scope="col"><?= __('Path Icon Marker') ?></th>
                <th scope="col"><?= __('Latitude') ?></th>
                <th scope="col"><?= __('Longtitude') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->tourist_attraction as $touristAttraction): ?>
            <tr>
                <td><?= h($touristAttraction->id) ?></td>
                <td><?= h($touristAttraction->name) ?></td>
                <td><?= h($touristAttraction->category_id) ?></td>
                <td><?= h($touristAttraction->path_picture_tourist_attraction) ?></td>
                <td><?= h($touristAttraction->province_id) ?></td>
                <td><?= h($touristAttraction->review_tourist_id) ?></td>
                <td><?= h($touristAttraction->detail_tourist) ?></td>
                <td><?= h($touristAttraction->path_icon_marker) ?></td>
                <td><?= h($touristAttraction->latitude) ?></td>
                <td><?= h($touristAttraction->longtitude) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TouristAttraction', 'action' => 'view', $touristAttraction->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TouristAttraction', 'action' => 'edit', $touristAttraction->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TouristAttraction', 'action' => 'delete', $touristAttraction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $touristAttraction->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
