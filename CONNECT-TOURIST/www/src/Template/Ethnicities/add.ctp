<?php
/**
  * 
  * add ethnicities template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ethnicity $ethnicity
  * @since   2018/04/20 18:19:28
  * @license pakgon.Ltd.
  */
?>
<div class="ethnicities ethnicities-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Ethnicity Management System => ( Add Ethnicity )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($ethnicity, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/Ethnicities/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add ethnicity ?'), 'data-confirm-title' => __('Confirm for add ethnicity ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
