<?php

 /**
  *
  * The template view for view as of ethnicities controller the page show for ethnicities information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ethnicity $ethnicity
  * @since  2018-04-20 18:19:28
  * @license Pakgon.Ltd
  */
?>
<div class="ethnicities view ethnicities-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Ethnicity Management System => ({0} information)', h($ethnicity->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Ethnicities Name'); ?></td>
                <td class="table-view-detail"><?php echo h($ethnicity->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Ethnicities Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($ethnicity->name_eng); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($ethnicity->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Ethnicities Id'); ?></td>
                <td class="table-view-detail"><?php echo h($ethnicity->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($ethnicity->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($ethnicity->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($ethnicity->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($ethnicity->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
