<?php
/**
  * 
  * edit ethnicities template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ethnicity $ethnicity
  * @since   2018/04/20 18:19:28
  * @license pakgon.Ltd.
  */
?>
<div class="ethnicities ethnicities-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Ethnicity Management System => ( Add Ethnicity )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($ethnicity, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
            echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]);
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Edit'), '/Ethnicities/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for edit ethnicity ?'), 'data-confirm-title' => __('Confirm for edit ethnicity ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
