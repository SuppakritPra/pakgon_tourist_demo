<?php
/**
  * The template index for index as of ethnicities controller the page show for short ethnicities information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ethnicity[]|\Cake\Collection\CollectionInterface $ethnicities
  * @since  2018-04-20 18:19:28
  * @license Pakgon.Ltd
  */
?>
<div class="ethnicities index">
    
    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Ethnicity Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('ethnicities'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Ethnicity'), '/ethnicities/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Ethnicity'), '/ethnicities/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->

    
        <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Ethnicities list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('Row no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name',__('Ethnicities Name')); ?></th>
                                            <th><?php echo $this->Paginator->sort('name_eng',__('Ethnicities Name Eng')); ?></th>
                                            <th><?php echo $this->Paginator->sort('status'); ?></th>
                                            <th><?php echo $this->Paginator->sort('create_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('update_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($ethnicities)): ?>
                    <?php foreach ($ethnicities as $index => $ethnicity): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                                                                                    <td><?php echo h($ethnicity->name); ?></td>
                                                                                                                <td><?php echo h($ethnicity->name_eng); ?></td>
                                                                                                                <td><?php echo $this->Utility->getMainStatus($ethnicity->status, true); ?></td>
                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($ethnicity->create_uid); ?></td>
                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($ethnicity->update_uid); ?></td>
                                                                                                                <td><?php echo $this->Utility->dateTimeISO($ethnicity->created); ?></td>
                                                                                                                <td><?php echo $this->Utility->dateTimeISO($ethnicity->modified); ?></td>
                                                                    <td class="actions">
                            <?php echo $this->Permission->getActions($ethnicity->id); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="9"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
         </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
