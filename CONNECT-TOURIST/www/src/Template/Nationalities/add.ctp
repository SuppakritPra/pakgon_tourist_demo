<?php
/**
  * 
  * add nationalities template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Nationality $nationality
  * @since   2018/04/20 18:18:52
  * @license pakgon.Ltd.
  */
?>
<div class="nationalities nationalities-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Nationality Management System => ( Add Nationality )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php 
            echo $this->Form->create($nationality, ['horizontal' => true]);
            echo $this->Form->control('name');
            echo $this->Form->control('name_eng');
        ?>
        <div class="box-footer">
            <?php 
                echo $this->Permission->submit(__('Save'), '/Nationalities/add', ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat confirmModal', 'data-confirm-message' => __('Are you sure for add nationality ?'), 'data-confirm-title' => __('Confirm for add nationality ?')]);
                echo $this->Permission->buttonBack();
                echo $this->Form->end(); 
            ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
