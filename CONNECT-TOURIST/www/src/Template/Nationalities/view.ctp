<?php

 /**
  *
  * The template view for view as of nationalities controller the page show for nationalities information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Nationality $nationality
  * @since  2018-04-20 18:18:52
  * @license Pakgon.Ltd
  */
?>
<div class="nationalities view nationalities-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Nationality Management System => ({0} information)', h($nationality->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Nationalities Name'); ?></td>
                <td class="table-view-detail"><?php echo h($nationality->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Nationalities Name Eng'); ?></td>
                <td class="table-view-detail"><?php echo h($nationality->name_eng); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Status'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getMainStatus($nationality->status, true); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Nationalities Id'); ?></td>
                <td class="table-view-detail"><?php echo h($nationality->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($nationality->create_uid); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->getUserFullnameById($nationality->update_uid); ?></td>
            </tr>
                                                <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($nationality->created); ?></td>
            </tr>
                                <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($nationality->modified); ?></td>
            </tr>
                            </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
