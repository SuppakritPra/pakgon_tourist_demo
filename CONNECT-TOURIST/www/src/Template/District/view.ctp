<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\District $district
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit District'), ['action' => 'edit', $district->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete District'), ['action' => 'delete', $district->id], ['confirm' => __('Are you sure you want to delete # {0}?', $district->id)]) ?> </li>
        <li><?= $this->Html->link(__('List District'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New District'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Area'), ['controller' => 'Area', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Area', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="district view large-9 medium-8 columns content">
    <h3><?= h($district->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('District Name') ?></th>
            <td><?= h($district->district_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($district->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Area') ?></h4>
        <?php if (!empty($district->area)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Area Id') ?></th>
                <th scope="col"><?= __('Area Name') ?></th>
                <th scope="col"><?= __('District Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($district->area as $area): ?>
            <tr>
                <td><?= h($area->area_id) ?></td>
                <td><?= h($area->area_name) ?></td>
                <td><?= h($area->district_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Area', 'action' => 'view', $area->area_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Area', 'action' => 'edit', $area->area_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Area', 'action' => 'delete', $area->area_id], ['confirm' => __('Are you sure you want to delete # {0}?', $area->area_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
