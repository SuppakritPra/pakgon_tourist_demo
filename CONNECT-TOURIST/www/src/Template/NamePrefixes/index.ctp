<?php
/**
  * The template index for index as of namePrefixes controller the page show for short namePrefixes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\NamePrefix[]|\Cake\Collection\CollectionInterface $namePrefixes
  * @since  2018-04-20 18:18:12
  * @license Pakgon.Ltd
  */
?>
<div class="namePrefixes index">
    
    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Name Prefix Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('namePrefixes'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Name Prefix'), '/namePrefixes/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Name Prefix'), '/namePrefixes/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->

    
        <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Name Prefixes list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('Row no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name',__('Name Prefixes Name')); ?></th>
                                            <th><?php echo $this->Paginator->sort('name_eng',__('Name Prefixes Name Eng')); ?></th>
                                            <th><?php echo $this->Paginator->sort('long_name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('status'); ?></th>
                                            <th><?php echo $this->Paginator->sort('create_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('update_uid'); ?></th>
                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($namePrefixes)): ?>
                    <?php foreach ($namePrefixes as $index => $namePrefix): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                                                                                    <td><?php echo h($namePrefix->name); ?></td>
                                                                                                                <td><?php echo h($namePrefix->name_eng); ?></td>
                                                                                                                <td><?php echo h($namePrefix->long_name); ?></td>
                                                                                                                <td><?php echo $this->Utility->getMainStatus($namePrefix->status, true); ?></td>
                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($namePrefix->create_uid); ?></td>
                                                                                                                <td><?php echo $this->Utility->getUserFullnameById($namePrefix->update_uid); ?></td>
                                                                                                                <td><?php echo $this->Utility->dateTimeISO($namePrefix->created); ?></td>
                                                                                                                <td><?php echo $this->Utility->dateTimeISO($namePrefix->modified); ?></td>
                                                                    <td class="actions">
                            <?php echo $this->Permission->getActions($namePrefix->id); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="10"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
         </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
