<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
//$this->viewBuilder()->setTheme('Ablepro6');
$this->theme = 'AdminLTE204';
$this->layout = 'mobile';
?>
<div class="articles form large-9 medium-8 columns content">
    <?php echo $this->Form->create($article, ['type' => 'file']) ?>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Add Article') ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo $this->Form->input('page_id', array(
                        'type' => 'hidden',
                        'value' => $page_id
                    ));
                    ?> 

                    <div class="form-group">
                        <?php echo __('Article title'); ?>
                        <?php echo $this->Form->control('article_title', ['required' => 'required', 'label' => false]); ?>
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('Article Intro'); ?>
                        <textarea name="article_intro" class="form-control" required="required" id="article-detail" rows="2" 'label' = false></textarea>    
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('Article Detail'); ?>
                        <textarea name="article_detail" id="article_detail" required="required" 'label' = false></textarea>  
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('language'); ?>
                        <?php $options = ['TH' => 'Thai', 'EN' => 'English', 'CN' => 'China']; ?>
                        <?php echo $this->Form->select('lang_code', $options, ['empty' => false, 'label' => false]); ?>  
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('Publish Date') . ' - ' . __('Expire date'); ?>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="reservationtime" required="required" name="expire_date" 'label' = false>
                        </div>
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('Feed Flag'); ?>
                        <?php
                        echo $this->Form->input('feed_flag', array(
                            'label' => false,
                            'type' => 'checkbox'
                        ));
                        ?> 
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('Is Noti'); ?><br>
                        <?php
                        echo $this->Form->input('is_noti', array(
                            'label' => false,
                            'type' => 'checkbox'
                        ));
                        ?>
                    </div>
                    <?php echo $this->element('table_indocumment');  ?>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <?php echo $this->Form->input(__('Save'), array('type' => 'submit', 'class' => 'btn btn-success', 'div' => array('class' => 'col-sm-2'), 'label' => false)); ?> 
        </div>
    </div>
    <!-- .box box-success-->
    <?php echo $this->Form->end(); ?> 
</div>

<?php $this->append('css'); ?>
<?php echo $this->Html->css('/vendor/bootstrap-daterangepicker/daterangepicker.css'); ?>

<?php $this->end(); ?>
<?php $this->append('scriptBottom'); ?>
<?php //echo $this->Html->script('/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min');  ?>
<?php echo $this->Html->script('/vendor/moment/min/moment.min'); ?>
<?php echo $this->Html->script('/vendor/bootstrap-daterangepicker/daterangepicker'); ?>
<script>
    CKEDITOR.replace('article_detail');
    function CKupdate() {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
    }

    // CKEDITOR.replace('content', {
    //     filebrowserBrowseUrl : '/browser/browse/type/all',
    //     filebrowserUploadUrl : '/browser/upload/type/all',
    //     filebrowserImageBrowseUrl : '/browser/browse/type/image',
    //     filebrowserImageUploadUrl : '/browser/upload/type/image',
    //     filebrowserWindowWidth  : 800,
    //     filebrowserWindowHeight : 500
    // });

    //Date range picker with time picker
    // $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY HH:mm' })
    $(function () {
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'DD/MM/YYYY HH:mm'
            }
        });
    });
    $(document).ready(function () {
        $("#image-title").on('change', function () {
            if ($("#image-title").val()) {
                var name_sound = document.getElementById("image-title").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 10250000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 10 MB');
                    document.getElementById("image-title-input").value = '';
                }
            }
        });
        $("#image-detail").on('change', function () {
            if ($("#image-detail").val()) {
                var name_sound = document.getElementById("image-detail").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 10250000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 10 MB');
                    document.getElementById("image-detail-input").value = '';
                }
            }
        });
        $("#audio").on('change', function () {
            if ($("#audio").val()) {
                $('#name-for-audio').removeAttr("disabled");
                //document.getElementById("name-for-audio").value = 'เสียงบรรยาย';
                // var name_sound = document.getElementById("audio").files;
                // document.getElementById("name-for-audio").value = name_sound[0].name;
                var name_sound = document.getElementById("audio").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 10250000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 10 MB');
                    document.getElementById("audio-input").value = '';
                }
                //     name_sound = name_sound[0].name;
                //     name_sound_length = name_sound.length;
                //     name_sound = name_sound.substring(0, name_sound_length-4);
                //     document.getElementById("name-for-audio").value = name_sound;

            } else {
                $('#name-for-audio').attr("disabled", true);
                // document.getElementById("name-for-audio").value = '';
            }
        });
        $("#video").on('change', function () {
            if ($("#video").val()) {
                $('#name-for-video').removeAttr("disabled");
                //  document.getElementById("name-for-video").value = 'วีดิโอประกอบ';
                var name_sound = document.getElementById("video").files;
                console.log(name_sound[0].size);
                if (name_sound[0].size > 61450000) {
                    $('#largeModal').modal('show');
                    $('#largeModalBodyText').text('กรุณาเพิ่มไฟล์ขนาดไม่เกิน 60 MB');
                    document.getElementById("video-input").value = '';
                }
            } else {
                $('#name-for-video').attr("disabled", true);
                // document.getElementById("name-for-video").value = '';
            }
        });
    });
</script>
<?php $this->end(); ?>