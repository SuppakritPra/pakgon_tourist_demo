<div class="articles index large-9 medium-8 columns content">
    <?php echo $this->Form->create('Search'); ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Search') ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <?php echo __('Article title'); ?>
                        <?php echo $this->Form->text('article_title', ['div' => FALSE, 'label' => 'FALSE', 'maxlength' => 300]); ?>    
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <?php echo __('Publish Date'); ?>
                        <?php
                        $check = !empty($publish_date_begin_set) ? "" . $publish_date_begin_set . "-" . $publish_date_end_set . "" : "";
                        echo $this->Form->control('expire_date', [
                            'label' => false,
                            'type' => 'text',
                            'id' => 'reservationtime',
                            'value' => $check,
                            'readonly' => 'readonly'
                        ]);
                        ?>  
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label><?php echo __('Show Expire'); ?></label>
                        <select name="show_expire" style="width: 100%;" class = "action_search">
                            <option value="0" <?php if (@$show_expire == 0) echo "selected" ?> ><?php echo __("Don't Show Expire"); ?></option>
                            <option value="1" <?php if (@$show_expire == 1) echo "selected" ?> ><?php echo __("Show Expire"); ?></option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Search'), array('div' => FALSE, 'name' => 'submit', 'escape' => false)); ?> 
        </div> 
    </div>
    <?php echo $this->Form->end(); ?> 

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Articles List') ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo $this->Html->link($this->Html->icon('plus') . " " . __('New'), ['action' => 'add', $pageid], ['class' => 'btn btn-sm btn-success btn-rounded waves-effect waves-light pull-right', 'escape' => false])
                    ?>
                    <br> <br>
                    <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?php echo __('Status') ?></th>
                                <th scope="col"><?php echo __('Article title') ?></th>
                                <th scope="col"><?php echo __('Publish Date') ?></th>
                                <th scope="col"><?php echo __('Expire date') ?></th>
                                <th scope="col"><?php echo __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($articles as $article):
                                // pr($article['publish_date']);
                                // pr($dateNow);die;

                                if (($article['publish_date'] >= $dateNow) && ($article['expire_date'] >= $dateNow)) {
                                    $statustext = "text-success";
                                    $statuslabel = "label-success";
                                    $statuslabeltext = "none publish";
                                    //ยังไม่ publish
                                } else if ($article['expire_date'] <= $dateNow) {
                                    $statustext = "text-muted";
                                    $statuslabel = "label-default";
                                    $statuslabeltext = "expire";
                                    //expire ตัวเทา
                                } else {
                                    $statustext = "text-primary";
                                    $statuslabel = "label-primary";
                                    $statuslabeltext = "publish";
                                    //publish สีเขียว
                                    ?>
                                <?php } ?>
                                <tr class= <?php echo $statustext ?>>
                                    <td><span class="label <?php echo $statuslabel ?>" ><?php echo $statuslabeltext ?></span></td>
                                    <td><?php echo $this->Html->link($article->article_title, ['action' => 'edit', $article->id]); ?></td>
                                    <td><?= h($article->publish_date) ?></td>
                                    <td><?= h($article->expire_date) ?></td>
                                    <td>
                                        <?php //$this->Html->templates(['icon' => '<i class="fa fa-{{type}}{{attrs.class}}"{{attrs}}></i>']); ?>
                                        <?php echo $this->Html->link('<i class="fa fa-pencil"></i>' . __('Edit'), ['action' => 'edit', $article->id], ['class' => 'btn btn-warning btn-rounded waves-effect waves-light', 'escape' => false]) ?>
                                        <?php echo $this->Form->postLink('<i class="fa fa-trash"></i>' . __('Delete'), ['action' => 'delete', $article->id], ['class' => 'btn btn-danger btn-rounded waves-effect waves-light confirmModal action-delete', 'escape' => false]) ?>
                                        <?php //echo $this->Permission->getActions($article->id); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>   
</div>
<?php $this->append('css'); ?>
<?php echo $this->Html->css('/vendor/bootstrap-daterangepicker/daterangepicker.css'); ?>
<?php $this->end(); ?>

<?php $this->append('scriptBottom'); ?>
<?php echo $this->Html->script('/vendor/moment/min/moment.min'); ?>
<?php echo $this->Html->script('/vendor/bootstrap-daterangepicker/daterangepicker'); ?>
<script type="text/javascript">
    $(function () {
        $('input[name="expire_date"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="expire_date"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + '-' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('input[name="expire_date"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    });
</script>
<?php $this->end(); ?>

