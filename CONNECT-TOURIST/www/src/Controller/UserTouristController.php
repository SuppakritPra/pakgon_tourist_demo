<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserTourist Controller
 *
 * @property \App\Model\Table\UserTouristTable $UserTourist
 *
 * @method \App\Model\Entity\UserTourist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserTouristController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $userTourist = $this->paginate($this->UserTourist);

        $this->set(compact('userTourist'));
    }

    /**
     * View method
     *
     * @param string|null $id User Tourist id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userTourist = $this->UserTourist->get($id, [
            'contain' => []
        ]);

        $this->set('userTourist', $userTourist);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userTourist = $this->UserTourist->newEntity();
        if ($this->request->is('post')) {
            $userTourist = $this->UserTourist->patchEntity($userTourist, $this->request->getData());
            if ($this->UserTourist->save($userTourist)) {
                $this->Flash->success(__('The user tourist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user tourist could not be saved. Please, try again.'));
        }
        $this->set(compact('userTourist'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Tourist id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userTourist = $this->UserTourist->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userTourist = $this->UserTourist->patchEntity($userTourist, $this->request->getData());
            if ($this->UserTourist->save($userTourist)) {
                $this->Flash->success(__('The user tourist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user tourist could not be saved. Please, try again.'));
        }
        $this->set(compact('userTourist'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Tourist id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userTourist = $this->UserTourist->get($id);
        if ($this->UserTourist->delete($userTourist)) {
            $this->Flash->success(__('The user tourist has been deleted.'));
        } else {
            $this->Flash->error(__('The user tourist could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
