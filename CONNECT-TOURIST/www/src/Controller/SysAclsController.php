<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * SysAcls Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\SysAclsTable $SysAcls
 * @method \App\Model\Entity\SysAcl[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:16:16
 * @license Pakgon.Ltd.
 */
class SysAclsController extends AppController {

    /**
     *
     * Index method make list for Sys Acl.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }

        $conditions = [];
        $order = ['SysAcls.modified' => 'asc', 'SysAcls.created' => 'asc', 'SysAcls.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());

            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(SysAcls.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(SysAcls.name_eng) ILIKE ' => "%{$name}%"];
            }

            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['SysAcls.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SysAcls.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SysAcls.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SysAcls.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(SysAcls.created) <= ' => $this->request->data['dateTo']);
            }
        }

        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['SysControllers', 'SysActions', 'Users', 'Roles']
        ];

        $sysAcls = $this->paginate($this->SysAcls);
        $this->set(compact('sysAcls'));
        $this->set('_serialize', ['sysAcls']);
    }

    /**
     *
     * Add method make for insert or add new Sys Acl.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */

    /**
     *
     * Add method make for insert or add new Sys Acl.
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018-03-13 15:40:31
     * @license Pakgon.Ltd
     */
    public function add() {
        $roles = $this->SysAcls->Roles->find()->where(['status' => 'A']);
        $this->set(compact('roles'));
        $this->set('_serialize', ['roles']);
    }

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function addPermission($roleId = null) {
        if ($this->request->is(['post', 'put', 'patch'])) {
            if (!empty($this->request->getData())) {
                $data = $this->request->getData();
                $affectedRows = 0;
                $createUid = $this->getAuthUserId();
                foreach ($data['caction'] as $k => $v) {
                    if (empty($v)) {
                        continue;
                    }
                    list($controllerId, $actionId) = explode('|', $v);
                    if ($this->SysAcls->exists(['sys_controller_id' => $controllerId, 'sys_action_id' => $actionId, 'role_id' => $roleId])) {
                        continue;
                    }
                    $sysAcl = $this->SysAcls->newEntity();
                    $sysAcl->sys_controller_id = $controllerId;
                    $sysAcl->sys_action_id = $actionId;
                    $sysAcl->role_id = $roleId;
                    $sysAcl->status = 'A';
                    $sysAcl->create_uid = $createUid;
                    if ($this->SysAcls->save($sysAcl)) {
                        $affectedRows++;
                    }
                }

                if ($affectedRows > 0) {
                    $this->Flash->success(__('The Role\'s ACL has been Added.'));
                    $this->redirect(['action' => 'addPermission', $roleId]);
                } else {
                    $this->Flash->error(__('The SysAcl could not be save. Please, try again.'));
                }
            }
        }//Process permission add submit
        //Complete Package render for package choossing
        $tmpComplete = $this->SysAcls->find('all', ['conditions' => ['SysAcls.role_id' => $roleId, 'SysAcls.status' => 'A']])->contain(['SysControllers' => ['sort' => 'name'], 'SysActions']);
        $completedControlPackages = ['' => __('All')];
        $completedMapPermission = [];
        foreach ($tmpComplete as $k => $v) {
            if (is_null($v->sys_action->id) or empty($v->sys_action->id)) {
                continue;
            }
            $completedControlPackages[$v->sys_controller->id . '|' . $v->sys_action->id] = __($v->sys_controller->name . '/' . $v->sys_action->name);
            $completedMapPermission[] = $v->sys_action->id;
        }

        //All package fill all to mullti chossing on LEFT side page
        $controllers = $this->SysAcls->SysActions->find('all', [
                    'conditions' => ['SysActions.id NOT IN' => $completedMapPermission, 'SysControllers.status' => 'A', 'SysActions.status' => 'A'],
                    'fields' => ['SysActions.id', 'SysActions.name', 'SysControllers.id', 'SysControllers.name']
                ])->contain(['SysControllers' => ['sort' => 'name']]);

        $availableMenus = $this->SysAcls->SysControllers->find('list')->where(['status' => 'A'])->order(['name'])->toArray();
        $controllerPackages = ['' => __('All')];
        $control = "";
        foreach ($controllers as $k => $v) {
            $controllerPackages[$v->sys_controller->id . '|' . $v->id] = __($v->sys_controller->name . '/' . $v->name);
        }
        //if not has new permission mus to map remove choossing all together
        if (count($controllerPackages) <= 1) {
            $controllerPackages = [];
        }
        $this->set(compact('controllerPackages', 'completedControlPackages', 'availableMenus', 'roleId'));
    }

    //Delete from permission page fill in list
    public function deletePermission($roleId) {
        if ($this->request->is(['post', 'put', 'patch'])) {
            $data = $this->request->getData();
            $deleteAffected = 0;
            foreach ($data['completedaction'] as $k => $v) {
                if (empty($v)) {
                    continue;
                }
                list($controllerId, $actionId) = explode('|', $v);
                if ($this->SysAcls->deleteAll(['role_id' => $roleId, 'sys_controller_id' => $controllerId, 'sys_action_id' => $actionId])) {
                    $deleteAffected++;
                }
            }
            if ($deleteAffected > 0) {
                $this->Flash->success(__('The SysAcl has been deleted.'));
            } else {
                $this->Flash->error(__('The SysAcl could not be deleted. Please, try again.'));
            }
            $this->redirect(['action' => 'addPermission', $roleId]);
        }
    }

    /**
     *
     * Delete method make for delete record of Sys Acl.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sys Acl id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->SysAcls->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sys acl, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $sysAcl = $this->SysAcls->get($id);
        $respond = [];

        if ($this->SysAcls->delete($sysAcl)) {
            $respond = $this->buildRequestRespond(__('The sys acl has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The sys acl could not be deleted. Please, try again.'), 'ERROR');
        }

        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }

}
