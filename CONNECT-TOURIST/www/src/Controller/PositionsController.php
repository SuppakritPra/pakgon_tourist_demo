<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Positions Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\PositionsTable $Positions
 * @method \App\Model\Entity\Position[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:11:51
 * @license Pakgon.Ltd.
 */
class PositionsController extends AppController
{

    /**
     *
     * Index method make list for Position.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:11:51
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Positions.modified' => 'asc', 'Positions.created' => 'asc', 'Positions.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Positions.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Positions.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Positions.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Positions.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Positions.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Positions.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Positions.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $positions = $this->paginate($this->Positions);
        $this->set(compact('positions'));
        $this->set('_serialize', ['positions']);
    }

    /**
     *
     * View method make for view information of Position.
     *
     * @author  sarawutt.b
     * @param   string|null $id Position id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:11:51
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Positions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested position, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $position = $this->Positions->get($id, [
            'contain' => ['Users']
        ]);
        $this->set('position', $position);
        $this->set('_serialize', ['position']);
    }

    /**
     *
     * Add method make for insert or add new Position.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:11:51
     * @license Pakgon.Ltd
     */
    public function add() {
        $position = $this->Positions->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $position = $this->Positions->patchEntity($position, $this->request->getData());
            if ($this->Positions->save($position)) {
                $this->Flash->success(__('The position has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The position could not be saved. Please, try again.'));
        }
        $this->set(compact('position'));
        $this->set('_serialize', ['position']);
    }

    /**
     *
     * Edit method make for update Position.
     *
     * @author  sarawutt.b
     * @param   string|null $id Position id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:11:51
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Positions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested position, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $position = $this->Positions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $position = $this->Positions->patchEntity($position, $this->request->getData());
            if ($this->Positions->save($position)) {
                $this->Flash->success(__('The position has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The position could not be update. Please, try again.'));
        }
        $this->set(compact('position'));
        $this->set('_serialize', ['position']);
    }


    /**
     *
     * Delete method make for delete record of Position.
     *
     * @author  sarawutt.b
     * @param   string|null $id Position id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:11:51
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Positions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested position, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $position = $this->Positions->get($id);
        $respond = [];
        
        if ($this->Positions->delete($position)) {
            $respond = $this->buildRequestRespond(__('The position has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The position could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
