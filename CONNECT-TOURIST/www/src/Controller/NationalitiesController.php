<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Nationalities Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\NationalitiesTable $Nationalities
 * @method \App\Model\Entity\Nationality[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:18:52
 * @license Pakgon.Ltd.
 */
class NationalitiesController extends AppController
{

    /**
     *
     * Index method make list for Nationality.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:18:52
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Nationalities.modified' => 'asc', 'Nationalities.created' => 'asc', 'Nationalities.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Nationalities.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Nationalities.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Nationalities.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Nationalities.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Nationalities.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Nationalities.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Nationalities.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $nationalities = $this->paginate($this->Nationalities);
        $this->set(compact('nationalities'));
        $this->set('_serialize', ['nationalities']);
    }

    /**
     *
     * View method make for view information of Nationality.
     *
     * @author  sarawutt.b
     * @param   string|null $id Nationality id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:18:52
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Nationalities->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested nationality, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $nationality = $this->Nationalities->get($id, [
            'contain' => []
        ]);
        $this->set('nationality', $nationality);
        $this->set('_serialize', ['nationality']);
    }

    /**
     *
     * Add method make for insert or add new Nationality.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:18:52
     * @license Pakgon.Ltd
     */
    public function add() {
        $nationality = $this->Nationalities->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $nationality = $this->Nationalities->patchEntity($nationality, $this->request->getData());
            if ($this->Nationalities->save($nationality)) {
                $this->Flash->success(__('The nationality has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The nationality could not be saved. Please, try again.'));
        }
        $this->set(compact('nationality'));
        $this->set('_serialize', ['nationality']);
    }

    /**
     *
     * Edit method make for update Nationality.
     *
     * @author  sarawutt.b
     * @param   string|null $id Nationality id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:18:52
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Nationalities->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested nationality, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $nationality = $this->Nationalities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $nationality = $this->Nationalities->patchEntity($nationality, $this->request->getData());
            if ($this->Nationalities->save($nationality)) {
                $this->Flash->success(__('The nationality has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The nationality could not be update. Please, try again.'));
        }
        $this->set(compact('nationality'));
        $this->set('_serialize', ['nationality']);
    }


    /**
     *
     * Delete method make for delete record of Nationality.
     *
     * @author  sarawutt.b
     * @param   string|null $id Nationality id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:18:52
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Nationalities->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested nationality, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $nationality = $this->Nationalities->get($id);
        $respond = [];
        
        if ($this->Nationalities->delete($nationality)) {
            $respond = $this->buildRequestRespond(__('The nationality has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The nationality could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
