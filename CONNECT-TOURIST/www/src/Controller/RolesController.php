<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Roles Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\RolesTable $Roles
 * @method \App\Model\Entity\Role[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:12:11
 * @license Pakgon.Ltd.
 */
class RolesController extends AppController
{

    /**
     *
     * Index method make list for Role.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:12:11
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Roles.modified' => 'asc', 'Roles.created' => 'asc', 'Roles.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Roles.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Roles.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Roles.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Roles.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Roles.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Roles.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Roles.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $roles = $this->paginate($this->Roles);
        $this->set(compact('roles'));
        $this->set('_serialize', ['roles']);
    }

    /**
     *
     * View method make for view information of Role.
     *
     * @author  sarawutt.b
     * @param   string|null $id Role id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:12:12
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Roles->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested role, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $role = $this->Roles->get($id, [
            'contain' => ['SysAcls', 'Users']
        ]);
        $this->set('role', $role);
        $this->set('_serialize', ['role']);
    }

    /**
     *
     * Add method make for insert or add new Role.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:12:12
     * @license Pakgon.Ltd
     */
    public function add() {
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $role = $this->Roles->patchEntity($role, $this->request->getData());
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The role could not be saved. Please, try again.'));
        }
        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }

    /**
     *
     * Edit method make for update Role.
     *
     * @author  sarawutt.b
     * @param   string|null $id Role id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:12:12
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Roles->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested role, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $role = $this->Roles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $role = $this->Roles->patchEntity($role, $this->request->getData());
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The role could not be update. Please, try again.'));
        }
        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }


    /**
     *
     * Delete method make for delete record of Role.
     *
     * @author  sarawutt.b
     * @param   string|null $id Role id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:12:12
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Roles->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested role, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $role = $this->Roles->get($id);
        $respond = [];
        
        if ($this->Roles->delete($role)) {
            $respond = $this->buildRequestRespond(__('The role has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The role could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
