<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * NamePrefixes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\NamePrefixesTable $NamePrefixes
 * @method \App\Model\Entity\NamePrefix[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:18:12
 * @license Pakgon.Ltd.
 */
class NamePrefixesController extends AppController
{

    /**
     *
     * Index method make list for Name Prefix.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:18:12
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['NamePrefixes.modified' => 'asc', 'NamePrefixes.created' => 'asc', 'NamePrefixes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(NamePrefixes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(NamePrefixes.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['NamePrefixes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(NamePrefixes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(NamePrefixes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(NamePrefixes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(NamePrefixes.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $namePrefixes = $this->paginate($this->NamePrefixes);
        $this->set(compact('namePrefixes'));
        $this->set('_serialize', ['namePrefixes']);
    }

    /**
     *
     * View method make for view information of Name Prefix.
     *
     * @author  sarawutt.b
     * @param   string|null $id Name Prefix id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:18:12
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->NamePrefixes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested name prefix, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $namePrefix = $this->NamePrefixes->get($id, [
            'contain' => []
        ]);
        $this->set('namePrefix', $namePrefix);
        $this->set('_serialize', ['namePrefix']);
    }

    /**
     *
     * Add method make for insert or add new Name Prefix.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:18:12
     * @license Pakgon.Ltd
     */
    public function add() {
        $namePrefix = $this->NamePrefixes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $namePrefix = $this->NamePrefixes->patchEntity($namePrefix, $this->request->getData());
            if ($this->NamePrefixes->save($namePrefix)) {
                $this->Flash->success(__('The name prefix has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The name prefix could not be saved. Please, try again.'));
        }
        $this->set(compact('namePrefix'));
        $this->set('_serialize', ['namePrefix']);
    }

    /**
     *
     * Edit method make for update Name Prefix.
     *
     * @author  sarawutt.b
     * @param   string|null $id Name Prefix id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:18:12
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->NamePrefixes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested name prefix, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $namePrefix = $this->NamePrefixes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $namePrefix = $this->NamePrefixes->patchEntity($namePrefix, $this->request->getData());
            if ($this->NamePrefixes->save($namePrefix)) {
                $this->Flash->success(__('The name prefix has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The name prefix could not be update. Please, try again.'));
        }
        $this->set(compact('namePrefix'));
        $this->set('_serialize', ['namePrefix']);
    }


    /**
     *
     * Delete method make for delete record of Name Prefix.
     *
     * @author  sarawutt.b
     * @param   string|null $id Name Prefix id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:18:12
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->NamePrefixes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested name prefix, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $namePrefix = $this->NamePrefixes->get($id);
        $respond = [];
        
        if ($this->NamePrefixes->delete($namePrefix)) {
            $respond = $this->buildRequestRespond(__('The name prefix has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The name prefix could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
