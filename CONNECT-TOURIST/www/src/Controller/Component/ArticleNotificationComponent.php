<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;

/**
 * ArticleNotification component
 */
class ArticleNotificationComponent extends Component {
     /**
     * 
     * Function Pakgon Firebase Cloud Messaging (FCM)
     * @author sarawutt.b
     * @param type $params as array of push notification
     * @param type $paramDatas as array of send data
     * @return HTML body response
     */
    public function push($params = [], $paramDatas = []) {
        $headerOptions = ['headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key=' . Configure::read('FCM.PUSH.KEY')
        ]];

        $notification = [
            'title' => $params['title'],
            'body' => empty($params['message']) ? 'Pakgon push notification' : $params['message'],
            'icon' => empty($params['icon']) ? null : $params['icon'],
            'sound' => empty($params['sound']) ? null : $params['sound'],
            'badge' => empty($params['badge']) ? 1 : $params['badge'],
        ];

        $customData = [
            'title' => $params['title'],
            'body' => empty($params['message']) ? 'Pakgon push notification' : $params['message'],
            'badge' => empty($params['badge']) ? 1 : $params['badge'],
        ];

        if (!is_array($paramDatas)) {
            $paramDatas = array($paramDatas);
        }
        
        $customData = array_merge($customData, $paramDatas);

        $data = [
            'to' => $params['topic'],
            'notification' => $notification,
            'data' => $customData,
            'priority' => 'high'
        ];

        $http = new Client();
        $response = $http->post(Configure::read('FCM.PUSH.URL'), json_encode($data), $headerOptions)->body();
        return $response;
    }


}