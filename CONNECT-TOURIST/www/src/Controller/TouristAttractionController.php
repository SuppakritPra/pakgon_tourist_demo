<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\DateTime;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;

/**
 * TouristAttraction Controller
 *
 * @property \App\Model\Table\TouristAttractionTable $TouristAttraction
 *
 * @method \App\Model\Entity\TouristAttraction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TouristAttractionController extends AppController
{
    // public $helpers = [
    //     'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
    //     'Html' => ['className' => 'Bootstrap.Html'],
    //     'Modal' => ['className' => 'Bootstrap.Modal'],
    //     'Navbar' => ['className' => 'Bootstrap.Navbar', 'autoActiveLink' => true],
    //     'Paginator',
    //     'Panel' => ['className' => 'Bootstrap.Panel']
    // ];

    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['add', 'edit','view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($id=null)
    {
        // if ($this->request->is('post')) {
        //     $data = $this->request->data();
        // }


        // $test = "https://maps.googleapis.com/maps/api/distancematrix/json?&origins=20.381991,99.867039&destinations=13.747076,100.535026&language=th&region=TH&key=AIzaSyBQYrnkuaSXlh5gcYV6AkZSqaDc37A5deo";
        
        // is work 100% //
        // $distance_data = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?&origins=20.381991,99.867039&destinations=13.747076,100.535026&language=th&region=TH&key=AIzaSyBQYrnkuaSXlh5gcYV6AkZSqaDc37A5deo');
        // $distance_arr = json_decode($distance_data);
        //////////////////
        $this->loadModel('TouristAttractionImage');
        $this->loadModel('MasterProvince');
        // $id = 1 ; // Bangkok

        $select_category = "";
        $check_near_far = 0;

        if ($this->request->is('post')) {
            $data = $this->request->data();
                if($data['category'] == "2"){
                    $select_category = 2;
                    $check_near_far = 0;

                    $user_id = $_SESSION['Auth']['User'][0]['id'];

                    $this->loadModel('UserTourist');
                    $id_province_user = $this->UserTourist->find('all')
                    ->select(['province_id'])
                    ->where(['UserTourist.id' => $user_id])
                    // ->join(['mp' => [
                    //         'table' => 'master.master_province',
                    //         'type' => 'left',
                    //         'conditions' => [
                    //             'mp.id = UserTourist.master_province_id'
                    //         ]
                    //     ]
                    // ])
                    ->toArray();

                    $this->loadModel('TouristAttractionImage');
                    $this->loadModel('MasterProvince');
                    $TouristAttraction = $this->TouristAttraction->find('all')
                    ->select(['mp.province_name_th','mp.center_lat','mp.center_long','TouristAttraction.path_picture_tourist_attraction','TouristAttraction.id','TouristAttraction.name',
                        'TouristAttraction.count_like','TouristAttraction.latitude','TouristAttraction.longtitude'])
                    ->where(['TouristAttraction.province_id'=> $id_province_user[0]['province_id'] ])
                    ->join(['mp' => [
                        'table' => 'master.master_province',
                        'type' => 'left',
                        'conditions' => [
                            'mp.id = TouristAttraction.province_id'
                        ]
                    ]
                ])
                    ->order(['TouristAttraction.count_like'=> 'DESC'])
                    ->toArray();

                    $image_banner = $this->MasterProvince->find('all')->select(['banner_path'])->where(['id' => '1'])
                    ->toArray();
                }
                elseif($data['category'] == "1"){
                    $select_category = 1;
                    $check_near_far = 1;

                    $user_id = $_SESSION['Auth']['User'][0]['id'];

                    $this->loadModel('UserTourist');
                    $id_province_user = $this->UserTourist->find('all')
                    ->select(['province_id'])
                    ->where(['UserTourist.id' => $user_id])
                    // ->join(['mp' => [
                    //         'table' => 'master.master_province',
                    //         'type' => 'left',
                    //         'conditions' => [
                    //             'mp.id = UserTourist.master_province_id'
                    //         ]
                    //     ]
                    // ])
                    ->toArray();

                    $this->loadModel('TouristAttractionImage');
                    $this->loadModel('MasterProvince');
                    $TouristAttraction = $this->TouristAttraction->find('all')
                    ->select(['mp.province_name_th','mp.center_lat','mp.center_long','TouristAttraction.path_picture_tourist_attraction',
                        'TouristAttraction.id','TouristAttraction.name','TouristAttraction.latitude','TouristAttraction.longtitude'])
                    ->where(['TouristAttraction.province_id'=> $id_province_user[0]['province_id'] ])
                    ->order(['TouristAttraction.id' => 'ASC'])
                    ->join(['mp' => [
                        'table' => 'master.master_province',
                        'type' => 'left',
                        'conditions' => [
                            'mp.id = TouristAttraction.province_id'
                        ]
                    ]
                ])
                    ->toArray();

                    $image_banner = $this->MasterProvince->find('all')->select(['banner_path'])->where(['id' => '1'])
                    ->toArray();

                }

        }elseif(!empty($id)){
            $check_near_far = 0;

            // $user_id = $_SESSION['Auth']['User'][0]['id'];

            // $this->loadModel('UserTourist');
            // $id_province_user = $this->UserTourist->find('all')
            // ->select(['province_id'])
            // ->where(['UserTourist.id' => $user_id])
            // // ->join(['mp' => [
            // //         'table' => 'master.master_province',
            // //         'type' => 'left',
            // //         'conditions' => [
            // //             'mp.id = UserTourist.master_province_id'
            // //         ]
            // //     ]
            // // ])
            // ->toArray();

            $this->loadModel('TouristAttractionImage');
            $this->loadModel('MasterProvince');
            $TouristAttraction = $this->TouristAttraction->find('all')
            ->select(['mp.id','mp.province_name_th','mp.center_lat','mp.center_long','TouristAttraction.path_picture_tourist_attraction','TouristAttraction.id','TouristAttraction.name',
                'TouristAttraction.count_like','TouristAttraction.latitude','TouristAttraction.longtitude'])
            ->where(['TouristAttraction.province_id'=> $id ])
            ->join(['mp' => [
                'table' => 'master.master_province',
                'type' => 'left',
                'conditions' => [
                    'mp.id = TouristAttraction.province_id'
                    ]
                ]
            ])
            ->order(['TouristAttraction.count_like'=> 'DESC'])
            ->toArray();

            $masterProvince = $this->MasterProvince->find('all')
            ->select(['MasterProvince.id','MasterProvince.province_name_th'])
            ->where(['MasterProvince.id' => $id])
            ->toArray();

            $image_banner = $this->MasterProvince->find('all')->select(['banner_path'])->where(['id' => '1'])
            ->toArray();
        }else{
            $check_near_far = 0;
            $user_id = $_SESSION['Auth']['User'][0]['id'];

            $this->loadModel('UserTourist');
            $id_province_user = $this->UserTourist->find('all')
            ->select(['province_id'])
            ->where(['UserTourist.id' => $user_id])
            // ->join(['mp' => [
            //         'table' => 'master.master_province',
            //         'type' => 'left',
            //         'conditions' => [
            //             'mp.id = UserTourist.master_province_id'
            //         ]
            //     ]
            // ])
            ->toArray();

            
            $TouristAttraction = $this->TouristAttraction->find('all')
            ->select(['mp.id','mp.province_name_th','mp.center_lat','mp.center_long','TouristAttraction.path_picture_tourist_attraction',
                'TouristAttraction.id','TouristAttraction.name','TouristAttraction.latitude','TouristAttraction.longtitude'])
            ->where(['TouristAttraction.province_id'=> $id_province_user[0]['province_id'] ])
            ->order(['TouristAttraction.id' => 'ASC'])
            ->join(['mp' => [
                'table' => 'master.master_province',
                'type' => 'left',
                'conditions' => [
                    'mp.id = TouristAttraction.province_id'
                ]
            ]
        ])
            ->toArray();

            $count_touristAttraction = count($TouristAttraction);

            $image_banner = $this->MasterProvince->find('all')->select(['banner_path'])->where(['id' => $id_province_user[0]['province_id']])
            ->toArray();
        }

        // if($id = "2"){

        //         $TouristAttraction = $this->TouristAttraction->find('all')
        //         ->select(['mp.province_name_th','TouristAttraction.path_picture_tourist_attraction','TouristAttraction.id','TouristAttraction.name','TouristAttraction.count_like'])
        //         ->join(['mp' => [
        //             'table' => 'master.master_province',
        //             'type' => 'left',
        //             'conditions' => [
        //                 'mp.id = TouristAttraction.province_id'
        //             ]
        //         ]
        //     ])
        //         ->order(['TouristAttraction.count_like'=> 'DESC'])
        //         ->toArray();

        //         $image_banner = $this->MasterProvince->find('all')->select(['banner_path'])->where(['id' => '1'])
        //         ->toArray();
        //     }
        //     if($category['category'] = "1"){



        //     }

        $this->set(compact('image_banner','TouristAttraction','count_touristAttraction','check_near_far','select_category','masterProvince'));
    }

    /**
     * View method
     *
     * @param string|null $id Tourist Attraction id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $touristAttraction = $this->TouristAttraction->get($id, [
            'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
        ]);

        $this->set('touristAttraction', $touristAttraction);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $touristAttraction = $this->TouristAttraction->newEntity();
        if ($this->request->is('post')) {
            $touristAttraction = $this->TouristAttraction->patchEntity($touristAttraction, $this->request->getData());
            if ($this->TouristAttraction->save($touristAttraction)) {
                $this->Flash->success(__('The tourist attraction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tourist attraction could not be saved. Please, try again.'));
        }
        $category = $this->TouristAttraction->Category->find('list', ['limit' => 200]);
        $masterProvince = $this->TouristAttraction->MasterProvince->find('list', ['limit' => 200]);
        $reviewTourist = $this->TouristAttraction->ReviewTourist->find('list', ['limit' => 200]);
        $this->set(compact('touristAttraction', 'category', 'masterProvince', 'reviewTourist'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tourist Attraction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $touristAttraction = $this->TouristAttraction->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $touristAttraction = $this->TouristAttraction->patchEntity($touristAttraction, $this->request->getData());
            if ($this->TouristAttraction->save($touristAttraction)) {
                $this->Flash->success(__('The tourist attraction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tourist attraction could not be saved. Please, try again.'));
        }
        $category = $this->TouristAttraction->Category->find('list', ['limit' => 200]);
        $masterProvince = $this->TouristAttraction->MasterProvince->find('list', ['limit' => 200]);
        $reviewTourist = $this->TouristAttraction->ReviewTourist->find('list', ['limit' => 200]);
        $this->set(compact('touristAttraction', 'category', 'masterProvince', 'reviewTourist'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tourist Attraction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $touristAttraction = $this->TouristAttraction->get($id);
        if ($this->TouristAttraction->delete($touristAttraction)) {
            $this->Flash->success(__('The tourist attraction has been deleted.'));
        } else {
            $this->Flash->error(__('The tourist attraction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function map()
    {
        $this->paginate = [
            'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
        ];

        // //=========================================================//   
        // Test for each google map marker // 
        $touristAttraction = $this->TouristAttraction->find('all')
        ->toArray();
        // //=========================================================//    

        $this->set(compact('touristAttraction'));
    }

    public function mapmarker()
    {
            // $this->viewBuilder()->setTheme('AdminLTE204');

        $this->paginate = [
            'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
        ];

        // //=========================================================//   
        // Test for each google map marker // 
        $touristAttraction = $this->TouristAttraction->find('all')
        ->toArray();
        // //=========================================================//    
        $this->loadModel('MasterProvince');
        $masterProvince = $this->MasterProvince->find('all')
        ->where(['id' => '1'])
        ->toArray();

        $count_touristAttraction = count($touristAttraction);

        $this->set(compact('touristAttraction','masterProvince','count_touristAttraction'));
        // $this->set(compact('touristAttraction'));
        // $this->set(compact('masterProvince'));
    }
    public function loopmapmarker($id = null){

       // $id = 1;

       $this->paginate = [
           'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
       ];

        // //=========================================================//   
        // Test for each google map marker // 


     $touristAttraction = $this->TouristAttraction->find('all')
     ->select(['TouristAttraction.id','TouristAttraction.name','TouristAttraction.latitude','TouristAttraction.longtitude','ctg.path_icon_marker'])
     ->where(['province_id' => $id])
     ->join(['ctg' => [
                    'table' => 'master.category',
                    'type' => 'left',
                    'conditions' => [
                        'ctg.id = TouristAttraction.category_id'
                    ]
                ]
            ])
     ->toArray();
    // $test = json_encode($touristAttraction);
    // //=========================================================//    
     $this->loadModel('MasterProvince');
     $masterProvince = $this->MasterProvince->find('all')
     ->where(['id' => $id])
     ->toArray();

       $count_touristAttraction = count($touristAttraction);

       $this->set(compact('touristAttraction','masterProvince','count_touristAttraction','id'));
        // $this->set(compact('touristAttraction'));
        // $this->set(compact('masterProvince'));
   }
    public function test(){
       //  $this->autoRender = false;
       // // if ($this->request->is('post')) {
       //  $touristAttraction = $this->TouristAttraction->find('all');
       //  $data = $touristAttraction->toArray();
       
       //  echo json_encode($data);
       //                //  }
       //  $test = [];
       //  $test["a"] = ["1","2","3"];
       //  $test["b"] = ["4","5","6"];
    }

    public function scanbeacon(){

        if ($this->request->is(['post'])) {

            $data = $this->request->data();
            $test = $data['selectBeacon'];

            switch($test) {
                case 'beacon0001':
                    header('Location: ../tourist-attraction/bloc/2'); exit;
                case 'beacon0002':
                    header('Location: ../tourist-attraction/bloc/3'); exit;
                case 'beacon0003':
                    header('Location: ../tourist-attraction/bloc/5'); exit;
                case 'beacon0004':
                    header('Location: ../tourist-attraction/bloc/6'); exit;
            }
        }

        $test_beacon = ['beacon0001','beacon0002','beacon0003','beacon0004'];
        $this->set(compact('test_beacon'));
    }

public function bloc($id = null){
    // id get from search torist , image have id tourist to parameter in function bloc
    // $id = 3; // id of touristAttraction & hide id this line for get from id parameter

    // $user_id = $this->Auth->user('id');
    // $user_id = $_SESSION['Auth']['User'][0]['id'];

    $this->paginate = [
        'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
    ];

    $touristAttraction_find_id = $this->TouristAttraction->find('all')
    ->select(['id','name','province_id','path_picture_tourist_attraction','create','count_like','detail_tourist'])
    ->where(['id' => $id])
    ->toArray();

    $this->loadModel('TouristAttractionImage');

    $touristAttraction_image_type_one = $this->TouristAttractionImage->find('all')
    ->select(['id','path_image','tourist_attraction_id','type_image'])
    ->where(['TouristAttractionImage.tourist_attraction_id' => $id,
        'TouristAttractionImage.type_image' => '1'
    ])
    ->toArray();

    $touristAttraction_image_type_two = $this->TouristAttractionImage->find('all')
    ->select(['id','path_image','tourist_attraction_id','type_image'])
    ->where(['TouristAttractionImage.tourist_attraction_id' => $id,
        'TouristAttractionImage.type_image' => '2'
    ])
    ->toArray();

    $now = new Time($touristAttraction_find_id['0']['create']);

    $create_time = $now->i18nFormat('dd-MM-yyyy');

    $this->loadModel('MasterProvince');
    $MasterProvince = $this->MasterProvince->find('all')
    ->select(['id','province_name_th','logo_of_province'])
    ->where(['id' => $touristAttraction_find_id['0']['province_id']])
    ->toArray();

    $count_touristAttraction = count($touristAttraction_find_id);

    // $checkuserId = $this->Auth->user('id');
    $checkuserId = $_SESSION['Auth']['User'][0]['id'];
    $this->loadModel('UserActive');
    $arrLike = $this->UserActive->findLike($checkuserId,$id);

    // $check_status_like = $this->UserActive->find('all',
    //     [
    //             'conditions' => [
    //                 'tourist_attraction_id' => $touristAttraction_find_id[0]['id'],
    //                 'user_id' => $userId
    //         ]
    //     ])
    // ->toArray();

    $this->loadModel('ReviewTourist');
    $user_id = $_SESSION['Auth']['User'][0]['id'];
    $tourist_attraction_id = $id;
    $check_user_review = $this->ReviewTourist->findUserReview($user_id,$tourist_attraction_id);


    $this->set(compact('touristAttraction','count_touristAttraction','MasterProvince',
        'touristAttraction_find_id','create_time','check_status_like','arrLike','touristAttraction_image_type_one',
        'touristAttraction_image_type_two','check_user_review'));
}
public function main(){
    // id get from search torist , image have id tourist to parameter in function bloc
    // $id = 3; // id of touristAttraction & hide id this line for get from id parameter

    $this->paginate = [
        'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
    ];

    $touristAttraction_image = $this->TouristAttraction->find('all')
    ->select('')
    ->toArray();


    $this->set(compact('touristAttraction','count_touristAttraction','MasterProvince',
        'touristAttraction_find_id','create_time','check_status_like','arrLike','touristAttraction_image_type_one',
        'touristAttraction_image_type_two'));
}

public function camera($id = null){
    // $id = 3;
    $this->paginate = [
        'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
    ];

    $touristAttraction_find_id = $this->TouristAttraction->find('all')
    ->select(['id','name','province_id','path_picture_tourist_attraction','create','count_like','detail_tourist'])
    ->where(['id' => $id])
    ->toArray();

    $this->loadModel('TouristAttractionImage');

    $getAllImage = $this->TouristAttractionImage->find('all')
    ->where(['tourist_attraction_id' => $id , 'type_image' => '2'])
    ->order(['id' => 'ASC'])
    ->toArray();


    $now = new Time($touristAttraction_find_id['0']['create']);
    $create_time = $now->i18nFormat('dd-MM-yyyy');

    $this->loadModel('MasterProvince');
    $MasterProvince = $this->MasterProvince->find('all')
    ->select(['id','province_name_th','logo_of_province'])
    ->where(['id' => $touristAttraction_find_id['0']['province_id']])
    ->toArray();

    $count_touristAttraction = count($touristAttraction_find_id);

    // $checkuserId = $this->Auth->user('id');
    $checkuserId = $_SESSION['Auth']['User'][0]['id'];
    $this->loadModel('UserActive');
    $arrLike = $this->UserActive->findLike($checkuserId,$id);

    $this->set(compact('touristAttraction','count_touristAttraction','MasterProvince',
        'touristAttraction_find_id','create_time','check_status_like','arrLike','getAllImage','csrf_token'));
}


public function searchProvince(){
    if ($this->request->is(['post'])) {
        $chkPost = true;
        $search = $this->request->data['search'];
        // masterProvince and TouristAttraction

        if(!empty($search)){
            $touristAttraction = $this->TouristAttraction->find('all')
            ->select([
                'id',
                'name',
                'mmpv.id',
                'mmpv.province_name_th',
                'mmpv.logo_of_province'
            ])
            ->Where([
                'name LIKE' => '%' . $search . '%',
            ])
            ->orWhere([
                'mmpv.province_name_th LIKE' => '%' . $search . '%',
            ])
            ->join([             
                'mmpv' => [
                    'table' => 'master.master_province',
                    'type' => 'left',
                    'conditions' => [
                        'mmpv.id = province_id'
                    ]
                ]
            ])
            ->toArray();

            $this->loadModel('MasterProvince');
            $chkSearchProvince = $this->MasterProvince->find('all')
            ->Where([
                'MasterProvince.province_name_th LIKE' => '%' . $search . '%',
            ])
            ->toArray();

        }else{
            $touristAttraction = null;
        }


            // $this->loadModel('UserSubscribes');
            // $isSubscribed = $this->UserSubscribes->find('list', [
            //             'conditions' => [
            //                 'user_id' => $this->Auth->user('id')
            //             ],
            //             'keyField' => 'page_id',
            //             'valueField' => 'page_id'
            //         ])->toArray();
    }

    $this->set(compact('touristAttraction', 'search', 'chkPost','chkSearchProvince'));
}


public function star($id=null){
    // $id = 3;
    $this->paginate = [
        'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
    ];

    $touristAttraction_find_id = $this->TouristAttraction->find('all')
    ->select(['id','name','province_id','path_picture_tourist_attraction','create','count_like','detail_tourist'])
    ->where(['id' => $id])
    ->toArray();

    $now = new Time($touristAttraction_find_id['0']['create']);
    $create_time = $now->i18nFormat('dd-MM-yyyy');

    $this->loadModel('MasterProvince');
    $MasterProvince = $this->MasterProvince->find('all')
    ->select(['id','province_name_th','logo_of_province'])
    ->where(['id' => $touristAttraction_find_id['0']['province_id']])
    ->toArray();

    $this->loadModel('ReviewTourist');
    $user_id = $_SESSION['Auth']['User'][0]['id'];
    $tourist_attraction_id = $id;
    $check_user_review = $this->ReviewTourist->findUserReview($user_id,$tourist_attraction_id);
    
    $reviewTourist = [];
    if(!empty($check_user_review)){
        $reviewTourist = $this->ReviewTourist->find('all')
        ->select('star')
        ->where([
            'user_id' => $user_id,
            'tourist_attraction_id' => $tourist_attraction_id
        ])
        ->toArray();
    }else{
        // $reviewTourist[0]['star'] = 0;
        $reviewTourist= [];
        $reviewTourist[0]['star'] = 0;
    }


    if ($this->request->is('post')) {
        $data = $this->request->data();

        $star = $data['hScore1'];
        
        $empty_array = [];

        if(!empty($check_user_review)){
            // $this->Flash->error(__('ReviewTourist Have User!!!'));
        }else{
            // $this->loadModel('ReviewTourist');
            $reviewTourist = $this->ReviewTourist->newEntity();
            $reviewTourist_update = $this->ReviewTourist->patchEntity($reviewTourist,$empty_array);
            $reviewTourist_update['user_id'] = $user_id;
            $reviewTourist_update['star'] = $star;
            $reviewTourist_update['tourist_attraction_id'] = $tourist_attraction_id;

            if ($this->ReviewTourist->save($reviewTourist_update)){
                // $this->Flash->success(__('Save User ReviewTourist Success'));
                // return $this->redirect(['action' => 'star','id' =>  $id]);
                return $this->redirect(array('action' => 'star', $id));
            }
        }
    }

    $this->set(compact('touristAttraction','MasterProvince',
        'touristAttraction_find_id','create_time','reviewTourist'));

}

public function addstar(){
    // $this->autoRender = false;
    // if ($this->request->is('post')) {
    //         // $data = json_decode(stripslashes($_POST['data']));

    //         $data = $this->request->data();

    //         // $user_id = $_SESSION['Auth']['User'][0]['id'];
    //         // $star = $data['data'];
    //         // $active_type_id = 1; // id like
    //         // $status_like = 'y';

    //         // $empty_array = [];

    //         // $UserActive = $this->UserActive->newEntity();

    //         //     $UserActive_update = $this->UserActive->patchEntity($UserActive,$data);

    //         //     if ($UserActive = $this->UserActive->save($UserActive_update)) {
    //         //         $response['status_like'] = $UserActive['status_like'];
    //         //     }
    //     }
    //     $data = json_encode($data);
    //     echo $data;
}

public function geolocation(){
    // $this->autoRender = false;
    // $response['status'] = 'FAILED';
    // if ($this->request->is('post')) {
    //     $data = $this->request->data();
    // }
    // $result = json_encode($data);
    // echo $result; 
}

public function geolocationv1(){

}

public function like(){
    $this->autoRender = false;
    $response['status'] = 'FAILED';
    if ($this->request->is('post')) {
        $request = $this->request->getData();
            $tourist_attraction_id = $request['tourist_attraction_id']; // id torist 
            $active_type_id = 1; // id like
            $status_like = 'y';
            // $user_id = $this->getAuthUserId(); // get id from users commu

            // $user_id = $this->Auth->user('id');
            $user_id = $_SESSION['Auth']['User'][0]['id'];
            $amountLink = [];

            $this->loadModel('UserActive');
            $userActive_check_conditions = $this->UserActive->find('all',
                [
                    'conditions' => [
                        'user_id' => $user_id,
                        'tourist_attraction_id' => $tourist_attraction_id,
                        'status_like' => $status_like
                    ]
                ])
            ->toArray();

            if (!empty($userActive_check_conditions)){ // disklike
                $this->loadModel('TouristAttraction');
                $TouristAttraction = $this->TouristAttraction->get($tourist_attraction_id);
                $TouristAttraction_update = $this->TouristAttraction->patchEntity($TouristAttraction, $amountLink);
                $TouristAttraction_update['count_like'] = $TouristAttraction['count_like'] - 1;

                $this->request->allowMethod(['post', 'delete']);
                $array_user_active = $this->UserActive->get($userActive_check_conditions[0]['id']);
                // $array_user_active = $this->UserActive->get($value);
                if ($this->UserActive->delete($array_user_active)) {
                    // $this->Flash->success(__('dislike success.'));
                } else {
                    // $this->Flash->error(__('dislike fail.'));
                }

                if ($TouristAttraction = $this->TouristAttraction->save($TouristAttraction_update)) {
                    $response['count_like'] = $TouristAttraction['count_like'];
                }

                $response['status'] = 'OK';
            }else if(empty($userActive_check_conditions)){
                $this->loadModel('TouristAttraction');
                $TouristAttraction = $this->TouristAttraction->get($tourist_attraction_id);
                $TouristAttraction_update = $this->TouristAttraction->patchEntity($TouristAttraction, $amountLink);
                $TouristAttraction_update['count_like'] = $TouristAttraction['count_like'] + 1;

                $this->loadModel('UserActive');

                $data = [];
                $data['user_id'] = $user_id;
                $data['tourist_attraction_id'] = $tourist_attraction_id;
                $data['create_uid'] = $user_id;
                $data['active_type_id'] = $active_type_id;
                $data['status_like'] = $status_like;

                $UserActive = $this->UserActive->newEntity();

                $UserActive_update = $this->UserActive->patchEntity($UserActive,$data);

                if ($UserActive = $this->UserActive->save($UserActive_update)) {
                    $response['status_like'] = $UserActive['status_like'];
                }

                if ($TouristAttraction = $this->TouristAttraction->save($TouristAttraction_update)) {
                    $response['count_like'] = $TouristAttraction->count_like;
                }

                $response['status'] = 'OK';
            }else {
                $response['status'] = 'FAILED';
            }
            
        }

        // $this->set(compact('response'));
        // $this->set('_serialize', ['response']);
        $data = json_encode($response);
        echo $data;
    }

    public function testaaa(){

    }   

    public function testbbb($currentCityName = null){
        $layout = 'html';
        $mainStart = microtime(true);
        if ($this->request->is(['post', 'put', 'patch']) || !empty($currentCityName)) {

            $this->updateAuthLogin();
            $outStartTime = microtime(true);
            if ($this->request->is(['post', 'put', 'patch'])) {
                $this->loadComponent('Utility');
                $startTime = microtime(true);
                $geolocations = $this->Utility->geolocation(trim($this->request->data['geolocation_url']));
                $finishTime = microtime(true);
                $this->log('GOOGLE GEOLOCATION LOAD TIME :: ' . number_format(($finishTime - $startTime), 4) . ' SECONDS', 'debug');
                $currentCityName = (array_key_exists('province', $geolocations) && !empty($geolocations['province']['long_name'])) ? $geolocations['province']['long_name'] : null;
            }

            $layout = 'mobile_slime';
        // $this->loadModel('userProfiles');
        // $this->loadModel('userActivites');
        // $this->loadModel('userEntities');
        // $this->loadModel('pageEntities');
        // $this->loadModel('userSubscribes');
        // $this->loadModel('Feeds');
        // $this->loadModel('Pages');
        // $this->loadModel('pageAdmin');
        // $this->loadModel('Cities');

        // $currentCity = null;
        // $userId = $this->getAuthUserId();
        // $userProfile = $this->userProfiles->findByUserId($userId)->first();
        // $currentCityName = trim($currentCityName);
        // $this->log('FEED 0:: USER AUTH:: ' . json_encode($this->Auth->user()), 'debug');

        // //IF EMPTY PROVINCE FROM GEOLOCATION LOAD USER PROFILE CITY
        // if (empty($currentCityName)) {
        //     $currentCity = $this->Cities->find()->where(['id' => $this->getAuthPersonalsCityId()])->first();
        //     $this->log('FEED A:: EMPTY GEOLOCATION THEN LOAD CONTENT BY PROFILE LOCATION::', 'debug');
        // } else {
        //     $currentCity = $this->Cities->find()->where(['city_name' => $currentCityName])->first();
        //     $this->log('FEED B:: HAS GEOLOCATION THEN LOAD CONTENT BY GEOLOCATION::', 'debug');
        // }

        // //CHECK IF EMPTY CURRENT CITY THEN LOAD DEFAULT BKK DEFAULT CITY
        // if (empty($currentCity)) {
        //     $currentCity = $this->Cities->find()->where(['id' => $this->_DEFAULT_CITI_ID])->first();
        //     $this->log('FEED C:: LOAD DEFAULT BKK PROVINCE INFO::', 'debug');
        // }

        // $currentCityName = $currentCity->city_name;
        // $connectPages = $this->Pages->findConnectPageList($userProfile);
        // $publicPages = $this->Pages->findPublicPageList($userProfile);
        // $subscribePages = $this->userSubscribes->findUserSubscribeFeed();
        // $allPageIds = array_merge($connectPages, $subscribePages);
        // $feeds = empty($allPageIds) ? [] : $this->Feeds->loadFeeds($allPageIds, ['limit' => 10], ['category_id not in' => 2]);

        // //IF LOCATION OR PROFILE INFORMATION IS EMPTY GET ALL CONTENT FROM DEFAULT CITY
        // $geoLocationPages = $this->Pages->findGeolocationPages($currentCity->id);
        // if ($geoLocationPages->isEmpty()) {
        //     $geoLocationPages = $this->Pages->findGeolocationPages($this->_DEFAULT_CITI_ID);
        //     $currentCity = $this->Cities->find()->where(['id' => $this->_DEFAULT_CITI_ID])->first();
        //     $currentCityName = $currentCity->city_name;
        //     $this->log('FEED C:: LOAD DEFAULT BKK EMPTY CONTENT::', 'debug');
        // }

        // $healthyPages = $this->Pages->findHealthyPages($currentCity->id);
        // $hotDealList = $this->Pages->findHotDealsPages($currentCity->id, true);
        // $hotDeals = $this->Feeds->findHotDealCardByPageId($hotDealList);
            $hotDealViewAllLink = (is_array($hotDeals) && !empty($hotDeals)) ? '/Feed/page/' . $hotDeals[0]['page_id'] : '#';

            $this->set(compact('feeds', 'geoLocationPages', 'healthyPages', 'hotDeals', 'hotDealViewAllLink', 'currentCityName', 'layout'));
            $this->set('_serialize', ['feeds', 'geoLocationPages', 'healthyPages', 'hotDeals', 'hotDealViewAllLink', 'currentCityName', 'layout']);
            $outStopTime = microtime(true);

            $this->log('LOCATION A:: FEED LOCATION CURRENT PROVINCE NAME:: ' . $currentCityName, 'debug');
            $this->log('LOCATION A:: FEED LOCATION CURRENT PROVINCE INFO :: ' . json_encode($currentCity), 'debug');
            $this->log('PAGE POST FEED/INDEX LOAD TIME :: ' . number_format(($outStopTime - $outStartTime), 4) . ' SECONDS', 'debug');
        }
        $mainStop = microtime(true);
        $this->log('PAGE MAIN FEED/INDEX LOAD TIME :: ' . number_format(($mainStop - $mainStart), 4) . ' SECONDS', 'debug');
    }


    public function ilike(){
        $user_id = $_SESSION['Auth']['User'][0]['id'];

        // $id = 1;
        $this->loadModel('UserActive');
        $this->loadModel('Users');

        $this->loadModel('UserTourist');
        $user = $this->UserTourist->find('all')
        ->where(['id' => $user_id])
        ->toArray();

        // $user = $this->Users->find('all')
        // ->where(['id' => $id])
        // ->toArray();


        $UserActive = $this->UserActive->find('all')
        ->select(['tourist_attraction_id'])
        ->where(['user_id' => $user_id])
        ->toArray();


        $this->loadModel('TouristAttractionImage');
        $this->loadModel('MasterProvince');
        $TouristAttraction = $this->TouristAttraction->find('all')
        ->where()
        ->toArray();

        $this->set(compact('UserActive','TouristAttraction','user'));

    }

    public function signout(){
        $this->autoRender = false;
        // //http://tourist-demo.pakgon.local:5052/tourist-attraction/ilike/
        // $this->Flash->success('You are now logged out.');
        // // $_SESSION = array();
        // // session_destroy();

        unset($_COOKIE['LCONNECT']);
        // unset($_COOKIE['_ga']);
        // unset($_COOKIE['_gid']);
        unset($_COOKIE['TOURIST']);
        // // setcookie('LCONNECT', null, -1, '/');
        // // setcookie('_ga', null, -1, '/');
        // // setcookie('_gid', null, -1, '/');
        // // setcookie('TOURIST', null, -1, '/');
        setcookie('LCONNECT', '', time() + (86400 * 30), '/'); // 1 day
        // setcookie('_ga', '', time() + (86400 * 30), '/');
        // setcookie('_gid', '', time() + (86400 * 30), '/');
        setcookie('TOURIST', '', time() + (86400 * 30), '/');
        // session_destroy();

        // session_start();
        // unset($_SESSION);
        // session_destroy();
        // session_write_close();
        // header('Location: /');

        // return $this->redirect($this->Auth->logout());

        // $user_id = $_SESSION['Auth']['User'][0]['id'];

        // foreach($_COOKIE AS $key => $value) {
        //     SETCOOKIE($key,$value,TIME()-10000,"/",".domain.com");
        // }

        // $http = new Client();
        // try {
        //     $http->delete(Configure::read('OAUTH2_PROVIDER.TOKEN_DETETE') . '/' . $_SESSION['Auth']['User'][0]['id'])->body();
        //     $this->Flash->success(__('You are now logged out.'));
        //     return $this->redirect($this->Auth->logout());
        // } catch (\Exception $ex) {
        //     $this->log('[ERROR] occurred :: ' . $ex->getFile(), 'debug');
        //     $this->log($ex, 'debug');
        //     return $this->redirect($this->Auth->logout());
        // }
        // return $this->redirect($this->Auth->logout());
    }

    public function randomString($lenght){
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $randomString = '';
        for ($j = 0; $j < $lenght; $j++) {
            $randomString .= $characters[rand(0, 25)];
        }
        return $randomString;
    }

    public function uploadMultiImage(){
        $this->autoRender = false;
        $response['status'] = 'FAILED';
        if ($this->request->is('post')) {
            $data = $this->request->data();
            $id = $data['id_tourist_attraction']; // id tourist_attraction
            unset($data['id_tourist_attraction']);
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////// form upload image ////////////////////////////////
            if(!empty($data)){
                $tmp_path_image = [];
                $tmp_name_image = [];
                $empty_array = [];
                // $images_arr = array();
                // foreach($data['images'] as $key=>$val){
                foreach($data as $key=>$val){
                    // foreach($_FILES['images']['name'] as $key=>$val){
                    // $image_name = $_FILES['images']['name'][$key];
                    // $tmp_name   = $_FILES['images']['tmp_name'][$key];
                    // $size       = $_FILES['images']['size'][$key];
                    // $type       = $_FILES['images']['type'][$key];
                    // $error      = $_FILES['images']['error'][$key];

                    // // File upload path
                    // $fileName = basename($_FILES['images']['name'][$key]);
                    // $targetFilePath = $targetDir . $fileName;

                    // // Check whether file type is valid
                    // $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                    // if(in_array($fileType, $allowTypes)){    
                    //     // Store images on the server
                    //     if(move_uploaded_file($_FILES['images']['tmp_name'][$key],SITE_ROOT.$targetFilePath)){
                    //         $images_arr[] = $targetFilePath;
                    //     }
                    // }

                    $this->loadModel('TouristAttractionImage');
                    $today = Time::now();
                    $dateNow = $today->i18nFormat('yyyyMMddHHmmss');
                    $randomString = $this->randomString(10);
                    $folder_tourist_image = 'upload/image_tourist';
                    $name_file_tourist_image = $randomString.$dateNow.$val['name'];
                    $tmp_name_image[$key] = $name_file_tourist_image;
                    $result_tourist_image = $this->uploadFiles($name_file_tourist_image,$folder_tourist_image,$val);
                    $result_tourist_image = ['abcd'];
                    if (!empty($result_tourist_image)){
                        $touristattractionimage = $this->TouristAttractionImage->newEntity();
                        $touristattractionimage = $this->TouristAttractionImage->patchEntity($touristattractionimage, $empty_array);
                        $touristattractionimage['path_image'] = '/' . $folder_tourist_image . '/' . $name_file_tourist_image;
                        // $tmp_path_image[$key] = $touristattractionimage['path_image'];
                        $touristattractionimage['tourist_attraction_id'] = $id;
                        $touristattractionimage['type_image'] = 2;
                        if($this->TouristAttractionImage->save($touristattractionimage)) {
                            // $this->Flash->success(__('Save success.'));
                            // return $this->redirect(['action' => 'camera']);
                        }else{
                            $this->Flash->error(__('Could not be saved. Please, try again.'));
                        }
                    }
                }
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            }      
            $data = json_encode($tmp_name_image);
            echo $data; 
        }

    }

    public function sortDataDistance(){
        $this->autoRender = false;
        $test = $this->request->data();
        if($this->request->is('post')){
        // $show = gettype($this->request->data());
            

        $gg = $this->request->data();
        $this->autoRender = false;        
        if($this->request->is('post')){
            // $collect_id = [];
            // $collect = [];

            // $test = $this->request->data();
            // $dataDecode = json_decode($test['data']);
            // $gg = $this->objectToArray($dataDecode[0]);

            // $count_decode_touristAttraction = count($dataDecode);
            //   foreach($data as $d){
            //      echo $d;
            //   }

            // $testString = $gg['data'][0] . $gg['data'][1] . $gg['data'][2];

            // echo $testString;
            // $data = json_encode($testString);
            // echo $data; 

            // $data = json_encode($gg);
            echo $data; 

            // $data = json_encode($_POST);
            // echo $data;

            // for($i = -1; $i < $count_decode_touristAttraction; $i++ ){
            //     // $collect_id[] = $dataDecode[$i]['id'];
            // }

            
            // for($i=-1; $i<$count_decode_touristAttraction; $i++ ){
            //     $collect_id[] = $dataDecode['id'];

            //     $touristAttraction = $this->TouristAttraction->find('all')
            //     ->where(['id' => $id])
            //     ->toArray();

            //     // $collect['id'] = $dataDecode['id'];
            //     // $collect['lat'] = $dataDecode['lat'];
            //     // $collect['lng'] = $dataDecode['lng'];
            //     // $collect['distance_matrix'] = $dataDecode['distance_matrix'];
            // }

            // foreach ($dataDecode as $key => $datadecode) {
            //     $touristAttraction = $this->TouristAttraction->find('all')
            //     ->where(['id' => $id])
            //     ->toArray();
            // }

            // $data = json_encode($dataDecode);
            // $data = json_encode($collect_id);
            // echo $data; 
            }

        }

    }

            // if(isset($_POST)){
            //     if(isset($_POST['data'])){
            //     $songData = $_POST['data'];
            //     }
            // }
    function objectToArray($d) {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }
        
        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map(__FUNCTION__, $d);
        }
        else {
            // Return array
            return $d;
        }
    }
    function arrayToObject($d) {
        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return (object) array_map(__FUNCTION__, $d);
        }
        else {
            // Return object
            return $d;
        }
    }

}