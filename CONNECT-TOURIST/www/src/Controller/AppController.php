<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Core\Configure;
use Cake\Http\Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    protected $respondCodes = ['OK' => '200', 'ERROR' => '500'];
    protected $selectEmptyMsg = '---- please select ----';

    /**
     *
     * Documentation 
     * Link : https://holt59.github.io/cakephp3-bootstrap-helpers/
     * Git : https://github.com/Holt59/cakephp3-bootstrap-helpers
     * @var type 
     */
    public $helpers = [
        'Utility',
        'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
        'Html' => ['className' => 'Bootstrap.Html'],
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Navbar' => ['className' => 'Bootstrap.Navbar']
    ];
    protected $serverName = null;
    protected $serverPort = null;
    protected $serverProtocal = NULL;
    protected $sessionUserId = null;
    protected $sessionRoleId = null;
    protected $L10n = null;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        I18n::setLocale('th_TH');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Utility');


        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */

        $clientId = Configure::read('OAUTH2_PROVIDER.CLIENT_ID');
        $redirectURI = Configure::read('OAUTH2_PROVIDER.REDIRECT_URI');
        $clientSecret = Configure::read('OAUTH2_PROVIDER.CLIENT_SECRET');
        $this->loadComponent('Auth', [
            'loginAction' => Configure::read('OAUTH2_PROVIDER.URL') . '/oauth?redir=oauth&client_id=' . $clientId . '&scope=GENERAL&redirect_uri=' . $redirectURI . '&response_type=code',
            'authenticate' => [
                'Muffin/OAuth2.OAuth' => [
                    'providers' => [
                        'generic' => [
                            'className' => 'League\OAuth2\Client\Provider\GenericProvider',
                            'options' => [
                                'clientId' => $clientId,
                                'clientSecret' => $clientSecret,
                            ],
                            'mapFields' => [
                                'username' => 'login',
                            ],
                        ],
                    ],
                ],
            ],
            'authorize' => ['Controller'],
            'unauthorizedRedirect' => $this->referer(),
            'loginRedirect' => ['controller' => 'feed', 'action' => 'index'],
            'logoutRedirect' => Configure::read('OAUTH2_PROVIDER.URL') . '/oauth?redir=oauth&client_id=' . $clientId . '&scope=GENERAL&redirect_uri=' . $redirectURI . '&response_type=code',
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');

        $this->loadModel('Commons');
    }

    /**
     * 
     * Function trigger before filter process
     * @author sarawutt.b
     * @param Event $event
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $this->Auth->allow(['login', 'logout']);
        
        $this->viewBuilder()->setTheme('Ablepro6');
        if ($this->request->param('action') === 'morefeed') {
            $this->viewBuilder()->layout('blank');
        } else {
            $this->viewBuilder()->layout('mobile');
        }


        if ($this->request->session()->check('token')) {
            $http = new Client();
            $params = ['oauth_token' => $this->request->session()->read('token.access_token')];

            try {
                $respons = $http->post(Configure::read('OAUTH2_PROVIDER.TOKEN_VERIFY'), $params)->body();
                $response = json_decode($respons, true);
                if (array_key_exists('result', $response) && array_key_exists('is_expires', $response['result']) && ($response['result']['result']['is_expires'])) {
                    $this->request->session()->delete('token');
                    return $this->redirect($this->Auth->logout());
                } else if (strtolower($response['status']) == 'fail') {
                    $this->log('[WARNONG] :: ' . $response['result']['message']);
                } else {
                    $this->log('[ERROR] :: ' . $respons);
                }
            } catch (\Exception $ex) {
                $this->log('[ERROR] occurred :: ' . $ex->getFile());
                $this->log($ex);
                return;
            }
        }

        /**
         * 
         * Set appication language this can be thai|english
         * @author Sarawutt.b
         * @since 2018-02-28
         * @return void
         */
        if ($this->request->session()->check('SessionLanguage') == false) {
            $this->request->session()->write('SessionLanguage', 'tha');
        }

        $this->response->header('Referer', 'http://porspch-tourist.pakgon.local:4042');

        //Check has existing to Authorize
        if ($this->Auth->user()) {
            
        } else {
            //debug('xxxx');exit;
            $this->request->session()->delete('Flash');
            $this->request->session()->delete('Flash.auth');
        }

        $this->serverName = $_SERVER['SERVER_NAME'];
        $this->serverPort = $_SERVER['SERVER_PORT'];
        $this->serverProtocal = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        //$this->sessionRoleId = $this->getAuthUserRoleId();
        //$this->sessionUserId = $this->getAuthUserId();
        //$this->set('menuListHtml', $this->getDynamicMenu());
    }

    /**
     * 
     * Function trigger before display on to the view front end
     * @author sarawutt.b
     * @param Event $event
     */
    public function beforeRender(Event $event) {
        //$this->viewBuilder()->setTheme('AdminLTE204');
        //$this->viewBuilder()->setTheme('PakgonConnect');
        $this->viewBuilder()->setTheme('Ablepro6');
        if ($this->request->param('action') === 'morefeed') {
            $this->viewBuilder()->layout('blank');
        } else {
            $this->viewBuilder()->layout('mobile');
        }
    }

    /**
     * 
     * Function check authorize
     * @author sarawutt.b
     * @param type $user
     * @return boolean
     */
    public function isAuthorized($user) {
        return true;
    }

    /**
     *
     * Function get Dinamic Menu read for dynamic menu where current domain, port, role, user
     * @author  sarawutt.b
     * @param   $pageLevel as a integer of page level posible value 1 | 2 
     * @return  string HTML of menu list
     * @return  array()
     */
    public function getDynamicMenu($pageLevel = 1) {
        $_FULL_URL = $this->serverProtocal . $this->serverName . ":" . $this->serverPort;
        $this->loadModel('Menus');
        $menuList = $this->Menus->getDynamicAclMenu($this->serverName, $this->serverPort, $this->sessionRoleId, $this->sessionUserId);
        $name_field = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $menuListHtml = "";
        foreach ($menuList as $k => $v) {
            if (strpos($v['url'], '#') !== false) {
                $menuListHtml .= "<li>
                                            <a href='#'>
                                                <span class='nav-label'>{$v[$name_field]}</span>
                                                <span class='fa arrow'></span>
                                            </a><ul class='nav nav-second-level'>";
                $childMenuLists = $this->Menus->getDynamicChildMenu($this->serverName, $this->serverPort, $this->sessionRoleId, $this->sessionUserId, $v['id']);
                foreach ($childMenuLists as $kk => $vv) {
                    $menuListHtml .= "<li><a href='{$this->internalPath}{$vv['url']}'> {$vv[$name_field]}</a></li>";
                }
                $menuListHtml .= "</ul></li>";
            } elseif ((strpos($v['url'], 'http://') !== false) || (strpos($v['url'], 'https://') !== false)) {
                $menuListHtml .= "<li><a href='{$v['url']}' target='_blank'><span>{$v[$name_field]}</span></a></li>";
            } else {
                $menuListHtml .= "<li><a href='{$_FULL_URL}{$v['url']}'><span>{$v[$name_field]}</span></a></li>";
            }
        }
        return $menuListHtml;
    }

    /**
     * Set language used this in mutiple language application concept
     * @author Sarawutt.b
     * @since 2016/03/21 10:23:33
     * @return void
     */
    public function _setLanguage() {
        $this->L10n = new L10n();
        $language = $this->request->session()->read('SessionLanguage');
        Configure::write('Config.language', $language);
        $this->L10n->get($language);
    }

    /**
     * 
     * Function get for current session user language
     * @author sarawutt.b
     * @return string
     */
    public function getCurrentLanguage() {
        return $this->request->session()->read('SessionLanguage');
    }

    /**
     *
     * This function for uload attachment excel file  from view of information style list
     * @author  sarawutt.b
     * @param   string name of target upload path
     * @param   array() file attribute option from upload form
     * @since   2017/10/30
     * @return  array()
     */
    function uploadFiles($name_file, $folder, $file, $itemId = null) {
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;
        if (!is_dir($folder_url)) {
            mkdir($folder_url);
        }
        //Bould new path if $itemId to be not null
        if ($itemId) {
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            $rel_url = $folder . '/' . $itemId;
            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }
        }
        //define for file type where it allow to upload
        $map = [
            'audio/mp3' => '.mp3',
            'video/mp4' => '.mp4',
            'image/bmp' => '.bmp',
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/excel' => '.xls',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx'
        ];
        //Bould file extension keep to the database
        $userfile_extn = substr($file['name'], strrpos($file['name'], '.') + 1);
        if (array_key_exists($file['type'], $map)) {
            $typeOK = true;
        }
        //Rename for the file if not change of the upload file makbe duplicate
        if ($typeOK) {
            switch ($file['error']) {
                case 0:
                    @unlink($folder_url . '/' . $name_file); //Delete the file if it already existing
                    $full_url = $folder_url . '/' . $name_file;
                    $url = $rel_url . '/' . $name_file;
                    $success = move_uploaded_file($file['tmp_name'], $url);
                    if ($success) {
                        $result['uploadPaths'][] = '/' . $url;
                        $result['uploadFileNames'][] = $name_file;
                        $result['uploadExts'][] = $userfile_extn;
                        $result['uploadOriginFileNames'][] = $file['name'];
                        $result['uploadFileTypes'][] = $file['type'];
                    } else {
                        $result['uploadErrors'][] = __("Error uploaded {$name_file}. Please try again.");
                    }
                    break;
                case 3:
                    $result['uploadErrors'][] = __("Error uploading {$name_file}. Please try again.");
                    break;
                case 4:
                    $result['noFiles'][] = __("No file Selected");
                    break;
                default:
                    $result['uploadErrors'][] = __("System error uploading {$name_file}. Contact webmaster.");
                    break;
            }
        } else {
            $permiss = '';
            foreach ($map as $k => $v) {
                $permiss .= "{$v}, ";
            }
            $result['uploadErrors'][] = __("{$name_file} cannot be uploaded. Acceptable file types in : %s", trim($permiss, ','));
        }
        return $result;
    }

    /**
     *
     * Function used fro generate _VERSION_
     * @author  sarawutt.b
     * @return  biginteger of the version number
     */
    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    /**
     *
     * Function used for generate UUID key patern
     * @author  sarawutt.b
     * @return  string uuid in version
     */
    public function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    /**
     * 
     * Function get for current session user authentication personals city id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user personals city id
     */
    protected function getAuthPersonalsCityId() {
        return $this->readAuth('Auth.UserProfiles.citie_id');
    }

    /**
     * 
     * Function get for current session user authentication personals user personals type
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user personals user personals type
     */
    protected function getAuthPersonalsUserType() {
        $options = ['' => 'UNDEFIGN', '1' => 'STUDENT', '2' => 'TEACHER', '3' => 'BOARDTEACHER', '4' => 'UOFFICER', '5' => 'PERSON'];
        $key = $this->readAuth('Auth.UserProfiles.user_type_id');
        return array_key_exists($key, $options) ? $options[$key] : $key;
    }

    /**
     * 
     * Function get for current session user authentication full name
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user full name
     */
    protected function getAuthFullname() {
        return $this->readAuth('Auth.User.first_name') . ' ' . $this->readAuth('Auth.User.last_name');
    }

    /**
     * 
     * Function get for current session user authentication user id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    protected function getAuthUserId() {
        return $this->readAuth('Auth.User.id');
    }

    /**
     * 
     * Function get for current session user authentication role id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    protected function getAuthUserRoleId() {
        return $this->readAuth('Auth.User.role_id');
    }

    /**
     * 
     * Function get for current session with user authentication
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication session info
     */
    protected function readAuth($name = null) {
        return $this->request->session()->read($name);
    }

    /**
     * Function get for empty option in DDL
     * @author sarawutt.b
     * @return array() of empty select DDL
     */
    public function getEmptySelect() {
        return ['' => __($this->selectEmptyMsg)];
    }

    /**
     * 
     * Function build for the respond for the request
     * @author  sarawutt.b
     * @param   type $message as a string of the respond message
     * @param   type $status as a string of status code possible value OK|ERROR
     * @return  []
     */
    protected function buildRequestRespond($message = 'successfully for the request', $status = 'OK') {
        $status = strtoupper($status);
        $class = ['OK' => 'success', 'ERROR' => 'error'];
        return ['message' => $message, 'status' => $status, 'code' => array_key_exists($status, $this->respondCodes) ? $this->respondCodes[$status] : '-1', 'class' => array_key_exists($status, $class) ? $class[$status] : 'default'];
    }

    /**
     * 
     * Function configure read wrapper function
     * @author sarawutt.b
     * @param string $key configure name
     * @param mix $default default value for configure if not set
     * @return mix
     */
    public function readConfigure($key, $default = null) {
        return Configure::read($key, $default);
    }

    /**
     * 
     * Function configure write wrapper function
     * @author sarawutt.b
     * @param string $key configure name
     * @param mix $val value of the key in configure
     * @return mix
     */
    public function writeConfigure($key, $val) {
        return Configure::write($key, $val);
    }

}
