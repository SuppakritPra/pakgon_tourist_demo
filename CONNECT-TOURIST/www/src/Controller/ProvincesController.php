<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Provinces Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\ProvincesTable $Provinces
 * @method \App\Model\Entity\Province[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:20:31
 * @license Pakgon.Ltd.
 */
class ProvincesController extends AppController
{

    /**
     *
     * Index method make list for Province.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:20:31
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Provinces.modified' => 'asc', 'Provinces.created' => 'asc', 'Provinces.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Provinces.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Provinces.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Provinces.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Provinces.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Provinces.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Provinces.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Provinces.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Regions']
        ];

        $provinces = $this->paginate($this->Provinces);
        $this->set(compact('provinces'));
        $this->set('_serialize', ['provinces']);
    }

    /**
     *
     * View method make for view information of Province.
     *
     * @author  sarawutt.b
     * @param   string|null $id Province id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:20:31
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Provinces->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested province, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $province = $this->Provinces->get($id, [
            'contain' => ['Regions', 'Districts', 'SubDistricts']
        ]);
        $this->set('province', $province);
        $this->set('_serialize', ['province']);
    }

    /**
     *
     * Add method make for insert or add new Province.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:20:31
     * @license Pakgon.Ltd
     */
    public function add() {
        $province = $this->Provinces->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $province = $this->Provinces->patchEntity($province, $this->request->getData());
            if ($this->Provinces->save($province)) {
                $this->Flash->success(__('The province has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The province could not be saved. Please, try again.'));
        }
        $regions = $this->Provinces->Regions->find('list', ['limit' => 200]);
        $this->set(compact('province', 'regions'));
        $this->set('_serialize', ['province']);
    }

    /**
     *
     * Edit method make for update Province.
     *
     * @author  sarawutt.b
     * @param   string|null $id Province id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:20:31
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Provinces->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested province, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $province = $this->Provinces->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $province = $this->Provinces->patchEntity($province, $this->request->getData());
            if ($this->Provinces->save($province)) {
                $this->Flash->success(__('The province has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The province could not be update. Please, try again.'));
        }
        $regions = $this->Provinces->Regions->find('list', ['limit' => 200]);
        $this->set(compact('province', 'regions'));
        $this->set('_serialize', ['province']);
    }


    /**
     *
     * Delete method make for delete record of Province.
     *
     * @author  sarawutt.b
     * @param   string|null $id Province id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:20:31
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Provinces->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested province, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $province = $this->Provinces->get($id);
        $respond = [];
        
        if ($this->Provinces->delete($province)) {
            $respond = $this->buildRequestRespond(__('The province has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The province could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
