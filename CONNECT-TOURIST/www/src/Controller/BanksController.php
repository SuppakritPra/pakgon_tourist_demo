<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Banks Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\BanksTable $Banks
 * @method \App\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:19:50
 * @license Pakgon.Ltd.
 */
class BanksController extends AppController
{

    /**
     *
     * Index method make list for Bank.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:19:50
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Banks.modified' => 'asc', 'Banks.created' => 'asc', 'Banks.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Banks.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Banks.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Banks.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Banks.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Banks.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Banks.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Banks.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $banks = $this->paginate($this->Banks);
        $this->set(compact('banks'));
        $this->set('_serialize', ['banks']);
    }

    /**
     *
     * View method make for view information of Bank.
     *
     * @author  sarawutt.b
     * @param   string|null $id Bank id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:19:50
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Banks->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested bank, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $bank = $this->Banks->get($id, [
            'contain' => []
        ]);
        $this->set('bank', $bank);
        $this->set('_serialize', ['bank']);
    }

    /**
     *
     * Add method make for insert or add new Bank.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:19:50
     * @license Pakgon.Ltd
     */
    public function add() {
        $bank = $this->Banks->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $bank = $this->Banks->patchEntity($bank, $this->request->getData());
            if ($this->Banks->save($bank)) {
                $this->Flash->success(__('The bank has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bank could not be saved. Please, try again.'));
        }
        $this->set(compact('bank'));
        $this->set('_serialize', ['bank']);
    }

    /**
     *
     * Edit method make for update Bank.
     *
     * @author  sarawutt.b
     * @param   string|null $id Bank id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:19:50
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Banks->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested bank, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $bank = $this->Banks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $bank = $this->Banks->patchEntity($bank, $this->request->getData());
            if ($this->Banks->save($bank)) {
                $this->Flash->success(__('The bank has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bank could not be update. Please, try again.'));
        }
        $this->set(compact('bank'));
        $this->set('_serialize', ['bank']);
    }


    /**
     *
     * Delete method make for delete record of Bank.
     *
     * @author  sarawutt.b
     * @param   string|null $id Bank id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:19:50
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Banks->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested bank, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $bank = $this->Banks->get($id);
        $respond = [];
        
        if ($this->Banks->delete($bank)) {
            $respond = $this->buildRequestRespond(__('The bank has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The bank could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
