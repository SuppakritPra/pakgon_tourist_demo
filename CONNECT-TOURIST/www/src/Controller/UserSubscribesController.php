<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserSubscribes Controller
 *
 * @property \App\Model\Table\UserSubscribesTable $UserSubscribes
 *
 * @method \App\Model\Entity\UserSubscribe[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserSubscribesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Pages']
        ];
        $userSubscribes = $this->paginate($this->UserSubscribes);

        $this->set(compact('userSubscribes'));
    }

    /**
     * View method
     *
     * @param string|null $id User Subscribe id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userSubscribe = $this->UserSubscribes->get($id, [
            'contain' => ['Users', 'Pages']
        ]);

        $this->set('userSubscribe', $userSubscribe);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addDel()
    {
        $this->autoRender = false;
        $user_id = $this->Auth->user('id');

        if ($this->request->is('post')){
            $page = $this->UserSubscribes->newEntity();
            $pageId= $this->request->data['page_id'];

            $Subscribe_page = $this->UserSubscribes->find('all',[
                'conditions' => [
                    'page_id' => $pageId,
                    'user_id' => $user_id
                ]
            ])->first();

            if(empty($Subscribe_page)){
                $Subscribe = $this->UserSubscribes->patchEntity($page, $this->request->data);
                $Subscribe['user_id'] = $user_id;
                $Subscribe['page_id'] = $pageId;
                $Subscribe['create_uid'] = $user_id;

                if($this->UserSubscribes->save($Subscribe)) {
                    $this->Flash->success(__('The page has been saved.'));
                    return $this->redirect(['controller'=>'feed','action' => 'index',$pageId]);
                }else{
                    $this->Flash->error(__('The page could not be saved. Please, try again.'));
                }
            }else if(!empty($Subscribe_page)){
                $userSubscribe = $this->UserSubscribes->get($Subscribe_page['id']);
                if ($this->UserSubscribes->delete($userSubscribe)) {
                    $this->Flash->success(__('The user subscribe has been deleted.'));
                } else {
                    $this->Flash->error(__('The user subscribe could not be deleted. Please, try again.'));
                }
                return $this->redirect(['controller'=>'feed','action' => 'index',$pageId]);
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id User Subscribe id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userSubscribe = $this->UserSubscribes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userSubscribe = $this->UserSubscribes->patchEntity($userSubscribe, $this->request->getData());
            if ($this->UserSubscribes->save($userSubscribe)) {
                $this->Flash->success(__('The user subscribe has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user subscribe could not be saved. Please, try again.'));
        }
        $users = $this->UserSubscribes->Users->find('list', ['limit' => 200]);
        $pages = $this->UserSubscribes->Pages->find('list', ['limit' => 200]);
        $this->set(compact('userSubscribe', 'users', 'pages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Subscribe id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userSubscribe = $this->UserSubscribes->get($id);
        if ($this->UserSubscribes->delete($userSubscribe)) {
            $this->Flash->success(__('The user subscribe has been deleted.'));
        } else {
            $this->Flash->error(__('The user subscribe could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
