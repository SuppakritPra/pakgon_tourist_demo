<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * SysControllers Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\SysControllersTable $SysControllers
 * @method \App\Model\Entity\SysController[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:13:04
 * @license Pakgon.Ltd.
 */
class SysControllersController extends AppController
{

    /**
     *
     * Index method make list for Sys Controller.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['SysControllers.modified' => 'asc', 'SysControllers.created' => 'asc', 'SysControllers.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(SysControllers.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(SysControllers.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['SysControllers.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SysControllers.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SysControllers.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SysControllers.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(SysControllers.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $sysControllers = $this->paginate($this->SysControllers);
        $this->set(compact('sysControllers'));
        $this->set('_serialize', ['sysControllers']);
    }

    /**
     *
     * View method make for view information of Sys Controller.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sys Controller id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->SysControllers->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sys controller, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $sysController = $this->SysControllers->get($id, [
            'contain' => ['Menus', 'SysAcls', 'SysActions']
        ]);
        $this->set('sysController', $sysController);
        $this->set('_serialize', ['sysController']);
    }

    /**
     *
     * Add method make for insert or add new Sys Controller.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function add() {
        $sysController = $this->SysControllers->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $sysController = $this->SysControllers->patchEntity($sysController, $this->request->getData());
            if ($this->SysControllers->save($sysController)) {
                $this->Flash->success(__('The sys controller has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sys controller could not be saved. Please, try again.'));
        }
        $this->set(compact('sysController'));
        $this->set('_serialize', ['sysController']);
    }

    /**
     *
     * Edit method make for update Sys Controller.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sys Controller id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->SysControllers->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sys controller, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $sysController = $this->SysControllers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $sysController = $this->SysControllers->patchEntity($sysController, $this->request->getData());
            if ($this->SysControllers->save($sysController)) {
                $this->Flash->success(__('The sys controller has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sys controller could not be update. Please, try again.'));
        }
        $this->set(compact('sysController'));
        $this->set('_serialize', ['sysController']);
    }


    /**
     *
     * Delete method make for delete record of Sys Controller.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sys Controller id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->SysControllers->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sys controller, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $sysController = $this->SysControllers->get($id);
        $respond = [];
        
        if ($this->SysControllers->delete($sysController)) {
            $respond = $this->buildRequestRespond(__('The sys controller has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The sys controller could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
