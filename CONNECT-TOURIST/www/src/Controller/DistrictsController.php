<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Districts Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\DistrictsTable $Districts
 * @method \App\Model\Entity\District[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:20:51
 * @license Pakgon.Ltd.
 */
class DistrictsController extends AppController
{

    /**
     *
     * Index method make list for District.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:20:51
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Districts.modified' => 'asc', 'Districts.created' => 'asc', 'Districts.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Districts.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Districts.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Districts.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Districts.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Districts.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Districts.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Districts.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Regions', 'Provinces']
        ];

        $districts = $this->paginate($this->Districts);
        $this->set(compact('districts'));
        $this->set('_serialize', ['districts']);
    }

    /**
     *
     * View method make for view information of District.
     *
     * @author  sarawutt.b
     * @param   string|null $id District id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:20:51
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Districts->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested district, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $district = $this->Districts->get($id, [
            'contain' => ['Regions', 'Provinces', 'SubDistricts', 'Zipcodes']
        ]);
        $this->set('district', $district);
        $this->set('_serialize', ['district']);
    }

    /**
     *
     * Add method make for insert or add new District.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:20:51
     * @license Pakgon.Ltd
     */
    public function add() {
        $district = $this->Districts->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $district = $this->Districts->patchEntity($district, $this->request->getData());
            if ($this->Districts->save($district)) {
                $this->Flash->success(__('The district has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The district could not be saved. Please, try again.'));
        }
        $regions = $this->Districts->Regions->find('list', ['limit' => 200]);
        $provinces = $this->Districts->Provinces->find('list', ['limit' => 200]);
        $this->set(compact('district', 'regions', 'provinces'));
        $this->set('_serialize', ['district']);
    }

    /**
     *
     * Edit method make for update District.
     *
     * @author  sarawutt.b
     * @param   string|null $id District id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:20:51
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Districts->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested district, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $district = $this->Districts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $district = $this->Districts->patchEntity($district, $this->request->getData());
            if ($this->Districts->save($district)) {
                $this->Flash->success(__('The district has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The district could not be update. Please, try again.'));
        }
        $regions = $this->Districts->Regions->find('list', ['limit' => 200]);
        $provinces = $this->Districts->Provinces->find('list', ['limit' => 200]);
        $this->set(compact('district', 'regions', 'provinces'));
        $this->set('_serialize', ['district']);
    }


    /**
     *
     * Delete method make for delete record of District.
     *
     * @author  sarawutt.b
     * @param   string|null $id District id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:20:51
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Districts->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested district, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $district = $this->Districts->get($id);
        $respond = [];
        
        if ($this->Districts->delete($district)) {
            $respond = $this->buildRequestRespond(__('The district has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The district could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
