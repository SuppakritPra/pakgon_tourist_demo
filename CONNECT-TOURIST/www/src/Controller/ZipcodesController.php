<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Zipcodes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\ZipcodesTable $Zipcodes
 * @method \App\Model\Entity\Zipcode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:22:40
 * @license Pakgon.Ltd.
 */
class ZipcodesController extends AppController
{

    /**
     *
     * Index method make list for Zipcode.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:22:40
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Zipcodes.modified' => 'asc', 'Zipcodes.created' => 'asc', 'Zipcodes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Zipcodes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Zipcodes.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Zipcodes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Zipcodes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Zipcodes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Zipcodes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Zipcodes.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Districts']
        ];

        $zipcodes = $this->paginate($this->Zipcodes);
        $this->set(compact('zipcodes'));
        $this->set('_serialize', ['zipcodes']);
    }

    /**
     *
     * View method make for view information of Zipcode.
     *
     * @author  sarawutt.b
     * @param   string|null $id Zipcode id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:22:40
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Zipcodes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested zipcode, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $zipcode = $this->Zipcodes->get($id, [
            'contain' => ['Districts']
        ]);
        $this->set('zipcode', $zipcode);
        $this->set('_serialize', ['zipcode']);
    }

    /**
     *
     * Add method make for insert or add new Zipcode.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:22:40
     * @license Pakgon.Ltd
     */
    public function add() {
        $zipcode = $this->Zipcodes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $zipcode = $this->Zipcodes->patchEntity($zipcode, $this->request->getData());
            if ($this->Zipcodes->save($zipcode)) {
                $this->Flash->success(__('The zipcode has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The zipcode could not be saved. Please, try again.'));
        }
        $districts = $this->Zipcodes->Districts->find('list', ['limit' => 200]);
        $this->set(compact('zipcode', 'districts'));
        $this->set('_serialize', ['zipcode']);
    }

    /**
     *
     * Edit method make for update Zipcode.
     *
     * @author  sarawutt.b
     * @param   string|null $id Zipcode id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:22:40
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Zipcodes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested zipcode, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $zipcode = $this->Zipcodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $zipcode = $this->Zipcodes->patchEntity($zipcode, $this->request->getData());
            if ($this->Zipcodes->save($zipcode)) {
                $this->Flash->success(__('The zipcode has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The zipcode could not be update. Please, try again.'));
        }
        $districts = $this->Zipcodes->Districts->find('list', ['limit' => 200]);
        $this->set(compact('zipcode', 'districts'));
        $this->set('_serialize', ['zipcode']);
    }


    /**
     *
     * Delete method make for delete record of Zipcode.
     *
     * @author  sarawutt.b
     * @param   string|null $id Zipcode id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:22:40
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Zipcodes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested zipcode, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $zipcode = $this->Zipcodes->get($id);
        $respond = [];
        
        if ($this->Zipcodes->delete($zipcode)) {
            $respond = $this->buildRequestRespond(__('The zipcode has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The zipcode could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
