<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;

/**
 * Description of AwesomeController
 *
 * @author sarawutt.b
 */
class UtilitiesController extends AppController {

    /**
     * 
     * Function initialize make for automatically trigger when contructure
     */
    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
    }

    public function index() {
        
    }

    /**
     * Finde system action thougth ajax with fillter
     * @params  $sysControllerId of action controllers on UUID format
     * @author  sarawutt.b
     * @Since   2018/03/08
     */
    function findActionsByControllersId($sysControllerId = '') {
        $this->loadModel('SysActions');
        $temps = $this->SysActions->findSysActionsList(['conditions' => ['sys_controller_id' => $sysControllerId, 'status' => 'A']]);
        $results = [];
        foreach ($temps as $k => $v) {
            if ($k === '') {
                continue;
            }
            $results[$k]['id'] = (int) $k;
            $results[$k]['text'] = $v;
        }
        unset($temps);
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    /**
     * 
     * Function find province by country code
     * @author sarawutt.b
     * @param type $countryCode as integer of province id
     */
    public function findProvinceByCountryCode($countryCode = null) {
        $this->loadModel('Provinces');
        $temps = $this->Provinces->findProvincesList(['conditions' => ['country_id' => $countryCode]]);
        $results = [];
        foreach ($temps as $k => $v) {
            if ($k === '') {
                continue;
            }
            $results[$k]['id'] = (int) $k;
            $results[$k]['text'] = $v;
        }
        unset($temps);
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    /**
     * 
     * Function find district by province code
     * @author sarawutt.b
     * @param type $provinceID as integer of province id
     */
    public function findDistrictByProvinceCode($provinceID = null) {
        $this->loadModel('Districts');
        $temps = $this->Districts->findDistrictsList(['conditions' => ['master_province_id' => $provinceID, 'status' => 'A']]);
        $results = [];
        foreach ($temps as $k => $v) {
            if ($k === '') {
                continue;
            }
            $results[$k]['id'] = (int) $k;
            $results[$k]['text'] = $v;
        }
        unset($temps);
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    /**
     * 
     * Function find department fillter by organization code
     * @author sarawutt.b
     * @param type $organizationCode as integer of ogganization id
     */
    public function findDepartmentByOrganizationCode($organizationCode = null) {
        $this->loadModel('Departments');
        $temps = $this->Departments->findDepartmentsList(['conditions' => ['master_organization_id' => $organizationCode, 'status' => 'A']]);
        $results = [];
        foreach ($temps as $k => $v) {
            if ($k === '') {
                continue;
            }
            $results[$k]['id'] = (int) $k;
            $results[$k]['text'] = $v;
        }
        unset($temps);
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    public function geolocation() {
        //$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=13.726722899999999%2C100.5141203&language=th&region=TH&sensor=false';
//        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=13.726722899999999%2C100.5141203&sensor=false&key=AIzaSyB3P7LQWDOp0Fc7JVVaMKQ2B4HqFYxo9Cw';
        if ($this->request->is(['post', 'put', 'patch'])) {
            try {
                $url = $this->request->data['url'] . Configure::read('API.GOOGLE_GEOLOCATION_KEY');
                $this->log('GEOLOCATION URL :: ' . $url);
                $http = new Client();
                $responseJson = $http->get($url)->body();
                $response = json_decode($responseJson, true);
                $this->log('GEOLOCAION DATA :: ' . json_encode($response));
                $status = strtoupper($response['status']);
                $geolocations = [];
                $currentProvince = '';
                if ($status === 'OK') {
                    $countComponent = count($response['results'][0]['address_components']);
                    for ($i = 0; $i < $countComponent; $i++) {
                        $components = $response['results'][0]['address_components'][$i];
                        if (in_array('street_number', $components['types'])) {
                            $geolocations['street_number']['short_name'] = trim($components['short_name']);
                            $geolocations['street_number']['long_name'] = trim($components['long_name']);
                        } elseif (in_array('route', $components['types'])) {
                            $geolocations['route']['short_name'] = trim($components['short_name']);
                            $geolocations['route']['long_name'] = trim($components['long_name']);
                        } elseif (in_array('airport', $components['types']) || in_array('car_rental', $components['types']) || in_array('establishment', $components['types']) || in_array('point_of_interest', $components['types']) || in_array('travel_agency', $components['types'])) {
                            $geolocations['trad_mart']['short_name'] = trim($components['short_name']);
                            $geolocations['trad_mart']['long_name'] = trim($components['long_name']);
                        } elseif (in_array('sublocality_level_2', $components['types']) || in_array('locality', $components['types'])) {
                            $geolocations['subdistrict']['short_name'] = trim($components['short_name']);
                            $geolocations['subdistrict']['long_name'] = trim($components['long_name']);
                        } elseif (in_array('administrative_area_level_2', $components['types']) || in_array('sublocality_level_1', $components['types'])) {
                            $geolocations['district']['short_name'] = trim($components['short_name']);
                            $geolocations['district']['long_name'] = trim($components['long_name']);
                        } elseif (in_array('administrative_area_level_1', $components['types'])) {
                            $geolocations['province']['short_name'] = trim($components['short_name']);
                            $geolocations['province']['long_name'] = $currentProvince = trim($components['long_name']);
                        } elseif (in_array('country', $components['types'])) {
                            $geolocations['country']['short_name'] = trim($components['short_name']);
                            $geolocations['country']['long_name'] = trim($components['long_name']);
                        } elseif (in_array('postal_code', $components['types'])) {
                            $geolocations['postal']['short_name'] = trim($components['short_name']);
                            $geolocations['postal']['long_name'] = trim($components['long_name']);
                        }
                    }

                    $this->request->session()->write('geolocation', $geolocations);
                    $this->request->session()->write('currentProvince', $currentProvince);
                    $this->log('GEOLOCATION INFO :: ' . json_encode($geolocations));
                    $this->log('CURRENT PROVINCE :: ' . $currentProvince);
                }
                $this->set(compact('response'));
                $this->set('_serialize', ['response']);
            } catch (\ErrorException $ex) {
                $this->log('[ERROR] occurred :: ' . $ex->getFile());
                $this->log($ex);
                return;
            }
        }
    }

}
