<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Filesystem\File;
use Cake\View\Helper\HtmlHelper;

class PagesController extends AppController
{

    //     public $helpers = [
    //     'Utility',
    //     'Form' => ['className' => 'Bootstrap.Form'],
    //     'Html' => ['className' => 'Bootstrap.Html'],
    //     'Modal' => ['className' => 'Bootstrap.Modal'],
    //     'Navbar' => ['className' => 'Bootstrap.Navbar'],
    //     'Paginator' => ['template' => 'paginator-template'],
    //     'Panel' => ['className' => 'Bootstrap.Panel']
    // ];

    public function index()
    {
        $user_id = $this->Auth->user('id');
        // debug($user_id);die;
        $this->loadModel('UserEntities'); 
        $userEntities = $this->UserEntities->find('all')
        ->select(['entity_code'])
        ->where(['user_id'=> $user_id])
        ->first();
        // debug($userEntities);die;
        // debug($userEntities['entity_code']);die;
        $this->loadModel('Cities');
        $this->loadModel('Countries');
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getQueryParams();
        }
        if (empty($this->request->getQueryParams())) {
            $this->request->data = $this->passedArgs;
        }
        
        // $this->loadModel('PageEntities');
        // $conditions = [];
        // $order = ['Pages.id' => 'desc'];
        if (!empty($this->request->getQueryParams())) {
            $this->request->data = $this->Utility->trim_all_data($this->request->getQueryParams());

            //Find by name or name_en
            // if (!empty($this->request->getQuery('name'))) {
            //     $conditions['OR'][] = ['LOWER(beacon_code) ILIKE ' => '%' . strtolower($this->request->data['name']) . '%'];
            // }       
            
            // $conditions[] = ['PageEntities.entity_code'=> $userEntities['entity_code']];
        }

        $this->paginate = [
            'contain' => ['Cities','Countries'],
            // 'conditions' => $conditions,
            // 'order' => $order
        ];

        $pages = $this->Pages->find('all')
        ->select([
            'id',
            'page_name',
            'cmci.city_name',
            'cmco.country_name',
        ])              
        ->join(
            [
                'cpen' => [
                    'table' => 'communtcation.page_entities',
                    'type' => 'left',
                    'conditions' => [
                        'cpen.page_id = Pages.id'
                    ]
                ],    
                'cmci' => [
                    'table' => 'communtcation.cities',
                    'type' => 'left',
                    'conditions' => [
                        'cmci.id = Pages.citie_id'
                    ]
                ],
                'cmco' => [
                    'table' => 'communtcation.countries',
                    'type' => 'left',
                    'conditions' => [
                        'cmco.id = Pages.countrie_id'
                    ]
                ],
                'cmco' => [
                    'table' => 'communtcation.countries',
                    'type' => 'left',
                    'conditions' => [
                        'cmco.id = Pages.countrie_id'
                    ]
                ]

            ])
        ->where(
            [
                'cpen.entity_code'=> $userEntities['entity_code'],
            ]
        )
        ->order(['Pages.id' => 'desc'])
        ->toArray();

        // $pages = $this->Pages->find('all')->toArray();
        // debug($pages);die;
        // $pages = $this->paginate($this->Pages);
        // debug($pages);die;
        $this->set(compact('pages'));
    }

    public function adminindex()
    {
        $user_id = $this->Auth->user('id');
        $this->loadModel('pageAdmin');
        $page_admin = $this->pageAdmin->find('list',
            [	
                'conditions' => [          
                    'user_id' => $user_id
                ],						
                'valueField' => 'page_id'
            ]
        )->toArray();
        $pages = array();
        if(!empty($page_admin)){
            $pages = $this->Pages->find('all')
            ->where(['page_status' => true,
                'id in' => $page_admin]);
        }

 	//---- page id for page public by subscribe
	 $this->loadModel('userSubscribes'); 
        $pages_subscribe = array();
        $pages_subscribe = $this->userSubscribes->find('list',
        [	
            'conditions' => [          
                'user_id ' => $user_id , 
            ],						
            'valueField' => 'page_id'
        ]);

        if(!empty($pages_subscribe)) $pages_subscribe = $pages_subscribe->toArray();
        
	$pages_user = array();
	if(!empty($pages_subscribe)){
	
		$pages_user = $this->Pages->find('all')
	        ->where(['page_status' => true,
	                'id in' => $pages_subscribe]);
	}
        $this->set(compact('pages', 'pages_user'));
        $this->set('_serialize', ['pages', 'pages_user']);
    }


    /**
     * View method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => ['Cities', 'Countries', 'Articles', 'PageAdmin', 'PageEntities']
        ]);

        $this->set('page', $page);
        $this->set('_serialize', ['page']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $this->loadModel('UserEntities');
            $UserEntities = $this->UserEntities->find('all', [
                'conditions' => [
                    'UserEntities.user_id' => $this->Auth->user('id')
                ]
            ])->first();
            $data = $this->request->data;
            $today = Time::now();
            $dateNow = $today->i18nFormat('yyyyMMddHHmmss');
            $randomString = $this->randomString(10);
            $folder_page_logo = 'upload/image_page_logo';
            $folder_page_banner = 'upload/image_page_banner';
            $name_file_page_logo = $randomString.$dateNow.$data['page_logo']['name'];
            $name_page_banner = $randomString.$dateNow.$data['page_banner']['name'];
            $result_page_logo = $this->uploadFiles($name_file_page_logo,$folder_page_logo,$data['page_logo']);
            $result_page_banner = $this->uploadFiles($name_page_banner,$folder_page_banner,$data['page_banner']);
            // pr($result_page_banner);die;
            if ($result_page_logo==true && $result_page_banner==true) {
                $page = $this->Pages->newEntity();
                $page = $this->Pages->patchEntity($page, $this->request->data);
                $page['page_status'] = 1;
                $page['page_type'] = 2;
                $page['citie_id'] = 0;
                $page['countrie_id'] = 0;
                $page['create_uid'] = $this->Auth->user('id');
                $page['page_logo'] = $result_page_logo['uploadFileNames'][0];
                $page['page_banner'] = $result_page_banner['uploadFileNames'][0];
                $page['page_desc'] = $data['page_desc'];
                // pr($page);die;
                if($this->Pages->save($page)) {
                    $this->loadModel('PageEntities');
                    $PageEntities = $this->PageEntities->newEntity();
                    $PageEntities['page_id'] = $page->id;
                    $PageEntities['entity_code'] = $UserEntities['entity_code'];
                    $PageEntities['create_uid'] = $this->Auth->user('id');
                    // pr($PageEntities);die;
                    $this->PageEntities->save($PageEntities);
                    $this->Flash->success(__('Save success.'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Could not be saved. Please, try again.'));
                }
            }else{
                $this->Flash->error(__('Could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $Pages = $this->Pages->find('all', [
            'conditions' => [
                'Pages.id' => $id
            ]
        ]);
        // pr($Pages);die;
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $page = $this->Pages->find('all', [
                'conditions' => [
                    'Pages.id' => $this->request->data['Pages_id']
                ]
            ])->first();
            $data = $this->request->data;
            $today = Time::now();
            $dateNow = $today->i18nFormat('yyyyMMddHHmmss');
            $randomString = $this->randomString(10);
            if(!empty($data['page_banner']['tmp_name']) && !empty($data['page_logo']['tmp_name'])){
                $folder_page_logo = 'upload/image_page_logo';
                $name_file_page_logo = $randomString.$dateNow.$data['page_logo']['name'];
                $result_page_logo = $this->uploadFiles($name_file_page_logo,$folder_page_logo,$data['page_logo']);
                $folder_page_banner = 'upload/image_page_banner';
                $name_page_banner = $randomString.$dateNow.$data['page_banner']['name'];
                $result_page_banner = $this->uploadFiles($name_page_banner,$folder_page_banner,$data['page_banner']);
                $page = $this->Pages->patchEntity($page, $data);
                $page['page_logo'] = $result_page_logo['uploadFileNames'][0];
                $page['page_banner'] = $result_page_banner['uploadFileNames'][0];
                $page['page_desc'] = $data['page_desc'];
                if($this->Pages->save($page)) {
                    $this->Flash->success(__('Save success.'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Could not be saved. Please, try again.'));
                }
            }else if (!empty($data['page_logo']['tmp_name'])) {
                $folder_page_logo = 'upload/image_page_logo';
                $name_file_page_logo = $randomString.$dateNow.$data['page_logo']['name'];
                $result_page_logo = $this->uploadFiles($name_file_page_logo,$folder_page_logo,$data['page_logo']);
                $page = $this->Pages->patchEntity($page, $data);
                $page['page_logo'] = $result_page_logo['uploadFileNames'][0];
                $page['page_desc'] = $data['page_desc'];
                if($this->Pages->save($page)) {
                    $this->Flash->success(__('Save success.'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Could not be saved. Please, try again.'));
                }
            }else if (!empty($data['page_banner']['tmp_name'])) {
                $folder_page_banner = 'upload/image_page_banner';
                $name_page_banner = $randomString.$dateNow.$data['page_banner']['name'];
                $result_page_banner = $this->uploadFiles($name_page_banner,$folder_page_banner,$data['page_banner']);
                $page = $this->Pages->patchEntity($page, $data);
                $page['page_banner'] = $result_page_banner['uploadFileNames'][0];
                $page['page_desc'] = $data['page_desc'];
                if($this->Pages->save($page)) {
                    $this->Flash->success(__('Save success.'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Could not be saved. Please, try again.'));
                }
            }else if(empty($data['page_banner']['tmp_name']) && empty($data['page_logo']['tmp_name'])){
                $page = $this->Pages->patchEntity($page, $data);
                $page['page_desc'] = $data['page_desc'];
                if($this->Pages->save($page)) {
                    $this->Flash->success(__('Save success.'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Could not be saved. Please, try again.'));
                }
            }else{
                $this->Flash->error(__('Could not be saved. Please, try again.'));
            }
        }
        $this->set('Pages', $Pages);
        $this->set('Pages_array', $Pages->toArray());
    }

    /**
     * Delete method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $page = $this->Pages->get($id);
        if ($this->Pages->delete($page)) {
            $this->Flash->success(__('Save success.'));
        } else {
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function randomString($lenght){
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $randomString = '';
        for ($j = 0; $j < $lenght; $j++) {
            $randomString .= $characters[rand(0, 25)];
        }
        return $randomString;
    }
    public function DeleteImgPages(){
        $this->autoRender = false;
        // pr($this->request->data);die;
        $data_type = '';
        $data_delete_page_id = $this->request->data['data'];
        $data_delete_type = $this->request->data['type'];

        $data_pages = $this->Pages->find('all', [
            'conditions' => [
                'Pages.id' => $data_delete_page_id
            ]
        ])->first();
        $page = $this->Pages->patchEntity($data_pages, $this->request->data);
        if($data_delete_type == 'page_logo'){
            $page['page_logo'] = null;
            $data_type = 'page_logo';
        }else if($data_delete_type == 'page_banner'){
            $page['page_banner'] = null;
            $data_type = 'page_banner';
        }
        if ($this->Pages->save($page)) {
            echo $data_type;
        }else{
            echo 'false';
        }
    }

    public function pagesearch(){
        $this->autoRender = false;
        $user_id = $this->Auth->user('id');
        $user_page = $this->request->data['page_name'];
        $whereName = 'upper(Pages.page_name) like '."upper('%".trim($user_page)."%')";
        //--- entity
        $this->loadModel('userEntities'); 
        $userEntities = $this->userEntities->find('list',
            [	
                'conditions' => [          
                    'user_id' =>   $user_id
                ],
                'keyField' => 'entity_code',
                'valueField' => 'entity_code'					            
            ])->toArray();

        $pages_public = array();
        $pages_public = $this->Pages->find('all')
        ->where(
            [
                'page_status'=> 1,
                'page_type in' => [0,9],
                $whereName
            ]
        )->toArray();

        $pages_private = array();
        if(!empty($userEntities)){
            $pages_private = $this->Pages->find('all')
            ->join(
                [
                    'cpen' => [
                        'table' => 'communtcation.page_entities',
                        'type' => 'left',
                        'conditions' => [
                            'cpen.page_id = Pages.id'
                        ]
                    ]
                ]
            )
            ->where(
                [
                    'Pages.page_status'=> 1,
                    'Pages.page_type' => 2,
                    'cpen.entity_code in'=> $userEntities,
                    $whereName
                ]
            )->toArray();
        }
        $arrAllPage = array();
        $arrAllPage = array_merge($pages_public,$pages_private);
        
        $Html = new HtmlHelper(new \Cake\View\View());
        $pagelist = "";
        $imgLogo = "";
        if(!empty($arrAllPage)){
            foreach( $arrAllPage as $page){

                $imgLogo = $Html->image("/upload/image_page_logo/".$page->page_logo, ["class" => " img-circle img-responsive","style"=>"width:95%"]);

                $pagelist .= '<div class="col-xs-4 list-page" align="center">';
                $pagelist .= '<a class="text-page" href="/feed/page/'.$page->id.'">';
                $pagelist .= '<div>'.$imgLogo.'</div>';
                $pagelist .= '<div class="page-name-search">'.$page->page_name.'</div>';
                $pagelist .= '</a>';
                $pagelist .= '</div>';

            }
        }else{
            $pagelist = '<div class="col-xs-12 page-empty">'.__('We did not find any results for '). '"'.$user_page.'"</div>';
        }

        // echo json_encode($Pages);
        echo $pagelist;
    }
}
