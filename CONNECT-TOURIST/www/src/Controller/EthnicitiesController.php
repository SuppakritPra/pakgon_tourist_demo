<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Ethnicities Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\EthnicitiesTable $Ethnicities
 * @method \App\Model\Entity\Ethnicity[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:19:28
 * @license Pakgon.Ltd.
 */
class EthnicitiesController extends AppController
{

    /**
     *
     * Index method make list for Ethnicity.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Ethnicities.modified' => 'asc', 'Ethnicities.created' => 'asc', 'Ethnicities.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Ethnicities.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Ethnicities.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Ethnicities.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Ethnicities.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Ethnicities.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Ethnicities.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Ethnicities.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $ethnicities = $this->paginate($this->Ethnicities);
        $this->set(compact('ethnicities'));
        $this->set('_serialize', ['ethnicities']);
    }

    /**
     *
     * View method make for view information of Ethnicity.
     *
     * @author  sarawutt.b
     * @param   string|null $id Ethnicity id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Ethnicities->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested ethnicity, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $ethnicity = $this->Ethnicities->get($id, [
            'contain' => []
        ]);
        $this->set('ethnicity', $ethnicity);
        $this->set('_serialize', ['ethnicity']);
    }

    /**
     *
     * Add method make for insert or add new Ethnicity.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function add() {
        $ethnicity = $this->Ethnicities->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $ethnicity = $this->Ethnicities->patchEntity($ethnicity, $this->request->getData());
            if ($this->Ethnicities->save($ethnicity)) {
                $this->Flash->success(__('The ethnicity has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ethnicity could not be saved. Please, try again.'));
        }
        $this->set(compact('ethnicity'));
        $this->set('_serialize', ['ethnicity']);
    }

    /**
     *
     * Edit method make for update Ethnicity.
     *
     * @author  sarawutt.b
     * @param   string|null $id Ethnicity id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Ethnicities->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested ethnicity, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $ethnicity = $this->Ethnicities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $ethnicity = $this->Ethnicities->patchEntity($ethnicity, $this->request->getData());
            if ($this->Ethnicities->save($ethnicity)) {
                $this->Flash->success(__('The ethnicity has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ethnicity could not be update. Please, try again.'));
        }
        $this->set(compact('ethnicity'));
        $this->set('_serialize', ['ethnicity']);
    }


    /**
     *
     * Delete method make for delete record of Ethnicity.
     *
     * @author  sarawutt.b
     * @param   string|null $id Ethnicity id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Ethnicities->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested ethnicity, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $ethnicity = $this->Ethnicities->get($id);
        $respond = [];
        
        if ($this->Ethnicities->delete($ethnicity)) {
            $respond = $this->buildRequestRespond(__('The ethnicity has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The ethnicity could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
