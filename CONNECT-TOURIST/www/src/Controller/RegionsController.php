<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Regions Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\RegionsTable $Regions
 * @method \App\Model\Entity\Region[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:20:14
 * @license Pakgon.Ltd.
 */
class RegionsController extends AppController
{

    /**
     *
     * Index method make list for Region.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:20:14
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Regions.modified' => 'asc', 'Regions.created' => 'asc', 'Regions.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Regions.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Regions.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Regions.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Regions.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Regions.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Regions.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Regions.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $regions = $this->paginate($this->Regions);
        $this->set(compact('regions'));
        $this->set('_serialize', ['regions']);
    }

    /**
     *
     * View method make for view information of Region.
     *
     * @author  sarawutt.b
     * @param   string|null $id Region id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:20:14
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Regions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested region, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $region = $this->Regions->get($id, [
            'contain' => ['Districts', 'Provinces', 'SubDistricts']
        ]);
        $this->set('region', $region);
        $this->set('_serialize', ['region']);
    }

    /**
     *
     * Add method make for insert or add new Region.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:20:14
     * @license Pakgon.Ltd
     */
    public function add() {
        $region = $this->Regions->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $region = $this->Regions->patchEntity($region, $this->request->getData());
            if ($this->Regions->save($region)) {
                $this->Flash->success(__('The region has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The region could not be saved. Please, try again.'));
        }
        $this->set(compact('region'));
        $this->set('_serialize', ['region']);
    }

    /**
     *
     * Edit method make for update Region.
     *
     * @author  sarawutt.b
     * @param   string|null $id Region id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:20:14
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Regions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested region, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $region = $this->Regions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $region = $this->Regions->patchEntity($region, $this->request->getData());
            if ($this->Regions->save($region)) {
                $this->Flash->success(__('The region has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The region could not be update. Please, try again.'));
        }
        $this->set(compact('region'));
        $this->set('_serialize', ['region']);
    }


    /**
     *
     * Delete method make for delete record of Region.
     *
     * @author  sarawutt.b
     * @param   string|null $id Region id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:20:14
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Regions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested region, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $region = $this->Regions->get($id);
        $respond = [];
        
        if ($this->Regions->delete($region)) {
            $respond = $this->buildRequestRespond(__('The region has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The region could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
