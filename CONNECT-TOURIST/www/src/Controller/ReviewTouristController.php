<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReviewTourist Controller
 *
 * @property \App\Model\Table\ReviewTouristTable $ReviewTourist
 *
 * @method \App\Model\Entity\ReviewTourist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReviewTouristController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $reviewTourist = $this->paginate($this->ReviewTourist);

        $this->set(compact('reviewTourist'));
    }

    /**
     * View method
     *
     * @param string|null $id Review Tourist id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reviewTourist = $this->ReviewTourist->get($id, [
            'contain' => ['Users', 'TouristAttraction']
        ]);

        $this->set('reviewTourist', $reviewTourist);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reviewTourist = $this->ReviewTourist->newEntity();
        if ($this->request->is('post')) {
            $reviewTourist = $this->ReviewTourist->patchEntity($reviewTourist, $this->request->getData());
            if ($this->ReviewTourist->save($reviewTourist)) {
                $this->Flash->success(__('The review tourist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The review tourist could not be saved. Please, try again.'));
        }
        $users = $this->ReviewTourist->Users->find('list', ['limit' => 200]);
        $this->set(compact('reviewTourist', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Review Tourist id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reviewTourist = $this->ReviewTourist->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reviewTourist = $this->ReviewTourist->patchEntity($reviewTourist, $this->request->getData());
            if ($this->ReviewTourist->save($reviewTourist)) {
                $this->Flash->success(__('The review tourist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The review tourist could not be saved. Please, try again.'));
        }
        $users = $this->ReviewTourist->Users->find('list', ['limit' => 200]);
        $this->set(compact('reviewTourist', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Review Tourist id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reviewTourist = $this->ReviewTourist->get($id);
        if ($this->ReviewTourist->delete($reviewTourist)) {
            $this->Flash->success(__('The review tourist has been deleted.'));
        } else {
            $this->Flash->error(__('The review tourist could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
