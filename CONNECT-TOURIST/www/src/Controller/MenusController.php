<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Menus Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\MenusTable $Menus
 * @method \App\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:12:40
 * @license Pakgon.Ltd.
 */
class MenusController extends AppController
{

    /**
     *
     * Index method make list for Menu.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['Menus.modified' => 'asc', 'Menus.created' => 'asc', 'Menus.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(Menus.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(Menus.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Menus.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Menus.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Menus.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Menus.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Menus.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['SysControllers', 'SysActions']
        ];

        $menus = $this->paginate($this->Menus);
        $this->set(compact('menus'));
        $this->set('_serialize', ['menus']);
    }

    /**
     *
     * View method make for view information of Menu.
     *
     * @author  sarawutt.b
     * @param   string|null $id Menu id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Menus->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested menu, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $menu = $this->Menus->get($id, [
            'contain' => ['SysControllers', 'SysActions']
        ]);
        $this->set('menu', $menu);
        $this->set('_serialize', ['menu']);
    }

    /**
     *
     * Add method make for insert or add new Menu.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function add() {
        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $sysControllers = $this->Menus->SysControllers->findSysControllersList();
        $sysActions = $this->Menus->SysActions->findSysActionsList();
        $this->set(compact('menu', 'sysControllers', 'sysActions', 'menuParents'));
        $this->set('_serialize', ['menu']);
    }

    /**
     *
     * Edit method make for update Menu.
     *
     * @author  sarawutt.b
     * @param   string|null $id Menu id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Menus->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested menu, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $menu = $this->Menus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be update. Please, try again.'));
        }
        $sysControllers = $this->Menus->SysControllers->find('list', ['limit' => 200]);
        $sysActions = $this->Menus->SysActions->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'sysControllers', 'sysActions'));
        $this->set('_serialize', ['menu']);
    }


    /**
     *
     * Delete method make for delete record of Menu.
     *
     * @author  sarawutt.b
     * @param   string|null $id Menu id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Menus->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested menu, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $menu = $this->Menus->get($id);
        $respond = [];
        
        if ($this->Menus->delete($menu)) {
            $respond = $this->buildRequestRespond(__('The menu has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The menu could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
