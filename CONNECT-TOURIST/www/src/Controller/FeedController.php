<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\I18n\Time;

/**
 * Feeds Controller
 *
 * @property \App\Model\Table\FeedsTable $Feeds
 *
 * @method \App\Model\Entity\Feed[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FeedController extends AppController {

    protected $_DEFAULT_CITI_ID = 1; //BKK

    /**
     * 
     * Function update for users profile Login
     * @author sarawutt.b
     * @return void
     */

    protected function updateAuthLogin() {
        try {
            //Create by sarawutt.b 
            //For reason keep token for check token is valid on oauth server
            if ($this->request->session()->check('token')) {
                $http = new Client();
                $authToken = $this->request->session()->read('token');
                $params = [
                    'client_id' => Configure::read('OAUTH2_PROVIDER.CLIENT_ID'),
                    'access_token' => $authToken['access_token']
                ];

                $response = $http->post(Configure::read('OAUTH2_PROVIDER.SERVER') . '/users/getOauthInfo.json?access_token=' . $authToken['access_token'], $params)->body();
                $response = json_decode($response, true);

                if (array_key_exists('user', $response['response'])) {
                    $this->loadModel('Users');
                    $this->loadModel('UserProfiles');
//                    $userInfo = $this->Users->findByUsername($response['response']['user'][0]['username'])->first();
//                    $userProfileInfo = $this->UserProfiles->findByUserId($userInfo->id)->first();

                    $userInfo = $this->Users->newEntity($response['response']['user'][0]);
                    $tmpProfile = (array_key_exists('personals', $response['response']) && !empty($response['response']['personals'])) ? $response['response']['personals'][0] : [];
                    $userProfiles['user_id'] = $userProfiles['create_uid'] = $userInfo->id;
                    $userProfiles['first_name'] = empty($tmpProfile['firstname_th']) ? '' : $tmpProfile['firstname_th'];
                    $userProfiles['last_name'] = empty($tmpProfile['lastname_th']) ? '' : $tmpProfile['lastname_th'];
                    $userProfiles['countrie_id'] = empty($tmpProfile['master_country_id']) ? 1 : $tmpProfile['master_country_id']; //TH IN DEFAULT
                    $userProfiles['citie_id'] = empty($tmpProfile['master_province_id']) ? 1 : $tmpProfile['master_province_id']; //BKK INDEFAULT
                    $userProfileInfo = $this->UserProfiles->newEntity($userProfiles);

                    //$this->Auth->setUser($userInfo);
                    $this->request->session()->write('Auth.UserPersonals', empty($response['response']['personals']) ? [] : $response['response']['personals'][0]);
                    $this->request->session()->write('Auth.UserProfiles', $userProfileInfo);
                    //debug($this->request->session()->read('Auth.UserProfiles'));exit;
                    $this->request->session()->write('User.Info.UserCards', $response['response']['userCards']);
                    return true;
                }
            }
        } catch (\Exception $ex) {
            $this->log('[ERROR] occurred :: ' . $ex->getMessage(), 'debug');
            $this->log($ex, 'debug');
            return false;
        }
    }

    /**
     * Home Feed load all content with geolocations
     * @author sarawutt.b
     */
    public function index($currentCityName = null) {
        $layout = 'html';
        $mainStart = microtime(true);
        if ($this->request->is(['post', 'put', 'patch']) || !empty($currentCityName)) {

            $this->updateAuthLogin();
            $outStartTime = microtime(true);
            if ($this->request->is(['post', 'put', 'patch'])) {
                $this->loadComponent('Utility');
                $startTime = microtime(true);
                $geolocations = $this->Utility->geolocation(trim($this->request->data['geolocation_url']));
                $finishTime = microtime(true);
                $this->log('GOOGLE GEOLOCATION LOAD TIME :: ' . number_format(($finishTime - $startTime), 4) . ' SECONDS', 'debug');
                $currentCityName = (array_key_exists('province', $geolocations) && !empty($geolocations['province']['long_name'])) ? $geolocations['province']['long_name'] : null;
            }

            $layout = 'mobile_slime';
            $this->loadModel('userProfiles');
            $this->loadModel('userActivites');
            $this->loadModel('userEntities');
            $this->loadModel('pageEntities');
            $this->loadModel('userSubscribes');
            $this->loadModel('Feeds');
            $this->loadModel('Pages');
            $this->loadModel('pageAdmin');
            $this->loadModel('Cities');

            $currentCity = null;
            $userId = $this->getAuthUserId();
            $userProfile = $this->userProfiles->findByUserId($userId)->first();
            $currentCityName = trim($currentCityName);
            $this->log('FEED 0:: USER AUTH:: ' . json_encode($this->Auth->user()), 'debug');

            //IF EMPTY PROVINCE FROM GEOLOCATION LOAD USER PROFILE CITY
            if (empty($currentCityName)) {
                $currentCity = $this->Cities->find()->where(['id' => $this->getAuthPersonalsCityId()])->first();
                $this->log('FEED A:: EMPTY GEOLOCATION THEN LOAD CONTENT BY PROFILE LOCATION::', 'debug');
            } else {
                $currentCity = $this->Cities->find()->where(['city_name' => $currentCityName])->first();
                $this->log('FEED B:: HAS GEOLOCATION THEN LOAD CONTENT BY GEOLOCATION::', 'debug');
            }

            //CHECK IF EMPTY CURRENT CITY THEN LOAD DEFAULT BKK DEFAULT CITY
            if (empty($currentCity)) {
                $currentCity = $this->Cities->find()->where(['id' => $this->_DEFAULT_CITI_ID])->first();
                $this->log('FEED C:: LOAD DEFAULT BKK PROVINCE INFO::', 'debug');
            }

            $currentCityName = $currentCity->city_name;
            $connectPages = $this->Pages->findConnectPageList($userProfile);
            $publicPages = $this->Pages->findPublicPageList($userProfile);
            $subscribePages = $this->userSubscribes->findUserSubscribeFeed();
            $allPageIds = array_merge($connectPages, $subscribePages);
            $feeds = empty($allPageIds) ? [] : $this->Feeds->loadFeeds($allPageIds, ['limit' => 10], ['category_id not in' => 2]);

            //IF LOCATION OR PROFILE INFORMATION IS EMPTY GET ALL CONTENT FROM DEFAULT CITY
            $geoLocationPages = $this->Pages->findGeolocationPages($currentCity->id);
            if ($geoLocationPages->isEmpty()) {
                $geoLocationPages = $this->Pages->findGeolocationPages($this->_DEFAULT_CITI_ID);
                $currentCity = $this->Cities->find()->where(['id' => $this->_DEFAULT_CITI_ID])->first();
                $currentCityName = $currentCity->city_name;
                $this->log('FEED C:: LOAD DEFAULT BKK EMPTY CONTENT::', 'debug');
            }

            $healthyPages = $this->Pages->findHealthyPages($currentCity->id);
            $hotDealList = $this->Pages->findHotDealsPages($currentCity->id, true);
            $hotDeals = $this->Feeds->findHotDealCardByPageId($hotDealList);
            $hotDealViewAllLink = (is_array($hotDeals) && !empty($hotDeals)) ? '/Feed/page/' . $hotDeals[0]['page_id'] : '#';

            $this->set(compact('feeds', 'geoLocationPages', 'healthyPages', 'hotDeals', 'hotDealViewAllLink', 'currentCityName', 'layout'));
            $this->set('_serialize', ['feeds', 'geoLocationPages', 'healthyPages', 'hotDeals', 'hotDealViewAllLink', 'currentCityName', 'layout']);
            $outStopTime = microtime(true);

            $this->log('LOCATION A:: FEED LOCATION CURRENT PROVINCE NAME:: ' . $currentCityName, 'debug');
            $this->log('LOCATION A:: FEED LOCATION CURRENT PROVINCE INFO :: ' . json_encode($currentCity), 'debug');
            $this->log('PAGE POST FEED/INDEX LOAD TIME :: ' . number_format(($outStopTime - $outStartTime), 4) . ' SECONDS', 'debug');
        }
        $mainStop = microtime(true);
        $this->log('PAGE MAIN FEED/INDEX LOAD TIME :: ' . number_format(($mainStop - $mainStart), 4) . ' SECONDS', 'debug');
    }

    /**
     * page method
     *
     * @return \Cake\Http\Response|void
     */
    public function page($pageId = null) {
        $user_id = $this->Auth->user('id');

        $this->loadModel('feeds');
        $Feeds = $this->feeds->find('all', array(
                    'limit' => 20,
                    'order' => 'publish_date DESC',
                    'conditions' => [
                        'page_id ' => $pageId
                    ],
                        )
                )->toArray();

        //------ user like page
        $this->loadModel('userActivites');
        $arrLike = $this->userActivites->find('list', [
                    'keyField' => 'article_id',
                    'valueField' => 'id'
                ])
                ->where([
                    'activity_type' => 'L',
                    'user_id' => $user_id
                ])
                ->toArray();

        $admin_page = 'N';

        $this->loadModel('pageAdmin');
        $page_admin = $this->pageAdmin->find('list', [
                    'conditions' => [
                        'page_id in ' => $pageId,
                        'user_id' => $user_id
                    ],
                    'valueField' => 'page_id'
                        ]
                )->toArray();

        if (count($page_admin)) {
            $admin_page = 'Y';
        } else {
            $admin_page = 'N';
        }

        $this->loadModel('pages');
        $page = $this->pages->find('all', [
                    'conditions' => [
                        'id' => $pageId
                    ]
                ])->toArray();

        $banner = $page[0]['page_banner'];
        $pageName = $page[0]['page_name'];
        $pageLogo = $page[0]['page_logo'];

        $this->loadModel('UserSubscribes');
        $isSubscribed = $this->UserSubscribes->find('all', [
                    'conditions' => [
                        'page_id' => $pageId,
                        'user_id' => $user_id
                    ]
                ])->isEmpty();

        $countFollowPage = $this->UserSubscribes->find()->where(['page_id' => $pageId])->all()->count();
        $this->set(compact('Feeds', 'admin_page', 'arrLike', 'user_id', 'page', 'banner', 'pageId', 'isSubscribed', 'pageName', 'pageLogo', 'countFollowPage'));
    }

    /**
     * 
     * Function my Feed display all auth user subscribe page's of the article
     * @author sarawutt.b
     * @return \Cake\Http\Response|void
     */
    public function myFeed() {
        $userId = $this->Auth->user('id');
        $this->loadModel('userProfiles');
        $this->loadModel('Pages');
        $this->loadModel('UserSubscribes');
        $this->loadModel('Feeds');

        $userProfile = $this->userProfiles->findByUserId($userId)->first();
        $connectPages = $this->Pages->findConnectPageList($userProfile);
        $subscribePageList = $this->UserSubscribes->findUserSubscribeFeed();
        $allPageIds = array_merge($connectPages, $subscribePageList);
        $feeds = $this->Feeds->loadFeeds($allPageIds);


        $this->loadModel('userActivites');
        $arrLike = $this->userActivites->findLike($userId);

//        debug($arrLike);
//        debug($feeds);
//        exit;
        $this->set(compact('feeds', 'arrLike'));
        $this->set('_serialize', ['feeds', 'arrLike']);
    }

    /**
     * 
     * Function Find view article details
     * @param type $id
     */
    public function view($id=null) {
        $user_id = $this->Auth->user('id');
        $arrArticleAsset = array();

         //--- chk article_id empty
        if(empty($id)){
            return $this->redirect(
                ['controller' => 'feed', 'action' => 'index']
            );
        }

        $this->loadModel('articles');
        
        //--- chk Record not found
        $chkArticle = $this->articles->find('all')->where(['id' => $id])->toArray();

        if(empty($chkArticle)){
            return $this->redirect(
                ['controller' => 'feed', 'action' => 'index']
            );
        }
        //--------

        $Article = $this->articles->get($id, [
            'contain' => ['Pages', 'articleAssets']
        ]);

        $this->loadModel('userActivites');
        $arrLike = $this->userActivites->find('list', [
                    'keyField' => 'article_id',
                    'valueField' => 'id'
                ])
                ->where([
                    'activity_type' => 'L',
                    'user_id' => $user_id,
                    'article_id' => $id
                ])
                ->toArray();


        // teeradone
        $read = $this->userActivites->find('all', [
                    'id',
                ])
                ->where([
                    'user_id' => $user_id,
                    'article_id' => $id,
                    'activity_type' => "R"
                ])
                ->first();
        if (empty($read)) {
            $read = $this->userActivites->newEntity();
            $reads['article_id'] = $id;
            $reads['user_id'] = $user_id;
            $reads['activity_type'] = "R";
            $reads['create_uid'] = $user_id;
            $read = $this->userActivites->patchEntity($read, $reads);
            $this->userActivites->save($read);
        }

        foreach ($Article->article_assets as $article_asset) {
            $arrArticleAsset[$article_asset->article_asset_type][] = ['name' => $article_asset->article_asset_name, 'path' => $article_asset->path];
        }

        $this->set(compact('Article', 'arrLike', 'arrArticleAsset'));
    }

    public function addlike() {
        $response['status'] = 'FAILED';
        if ($this->request->is('post')) {
            $user_id = $this->getAuthUserId();
            $request = $this->request->getData();
            $article_id = $request['article_id'];
            $this->loadModel('userActivites');
            $hasReccord = $this->userActivites->exists(['activity_type' => 'L', 'user_id' => $user_id, 'article_id' => $article_id]);
            if (!$hasReccord) {
                $amountLink = [];
                $response = [];
                $this->loadModel('Articles');
                $article = $this->Articles->get($article_id);
                $amountLink['count_like'] = $article['count_like'] + 1;
                $article = $this->Articles->patchEntity($article, $amountLink);
                if ($this->Articles->save($article)) {
                    $response['count_like'] = $article->count_like;
                }

                $data = [];
                $data['article_id'] = $article_id;
                $data['user_id'] = $user_id;
                $data['activity_type'] = 'L';
                $data['create_uid'] = $user_id;

                $user_activites = $this->userActivites->newEntity();
                $user_activites = $this->userActivites->patchEntity($user_activites, $data);
                if ($this->userActivites->save($user_activites)) {
                    
                }

                $response['status'] = 'OK';
            } else {
                $response['status'] = 'FAILED';
            }
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    public function morefeed() {
        if ($this->request->is('post')) {
            $page_ids = $this->request->data['page_ids'];
            $limit = 10;
            $offset = $this->request->data['page'] * $limit;
            $this->loadModel('feeds');
            $Feeds = $this->feeds->find('all', array(
                        'limit' => $limit,
                        'offset' => $offset,
                        'order' => 'publish_date DESC',
                        'conditions' => [
                            'page_id in ' => $page_ids
                        ],
                            )
                    )->toArray();
            $this->set(compact('Feeds'));
        }
    }

}
