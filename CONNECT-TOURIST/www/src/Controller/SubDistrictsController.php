<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * SubDistricts Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\SubDistrictsTable $SubDistricts
 * @method \App\Model\Entity\SubDistrict[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/04/20 18:21:04
 * @license Pakgon.Ltd.
 */
class SubDistrictsController extends AppController
{

    /**
     *
     * Index method make list for Sub District.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/04/20 18:21:04
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['SubDistricts.modified' => 'asc', 'SubDistricts.created' => 'asc', 'SubDistricts.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(SubDistricts.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(SubDistricts.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['SubDistricts.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SubDistricts.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SubDistricts.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(SubDistricts.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(SubDistricts.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Regions', 'Provinces', 'Districts']
        ];

        $subDistricts = $this->paginate($this->SubDistricts);
        $this->set(compact('subDistricts'));
        $this->set('_serialize', ['subDistricts']);
    }

    /**
     *
     * View method make for view information of Sub District.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sub District id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:21:04
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->SubDistricts->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sub district, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $subDistrict = $this->SubDistricts->get($id, [
            'contain' => ['Regions', 'Provinces', 'Districts']
        ]);
        $this->set('subDistrict', $subDistrict);
        $this->set('_serialize', ['subDistrict']);
    }

    /**
     *
     * Add method make for insert or add new Sub District.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/04/20 18:21:04
     * @license Pakgon.Ltd
     */
    public function add() {
        $subDistrict = $this->SubDistricts->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $subDistrict = $this->SubDistricts->patchEntity($subDistrict, $this->request->getData());
            if ($this->SubDistricts->save($subDistrict)) {
                $this->Flash->success(__('The sub district has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sub district could not be saved. Please, try again.'));
        }
        $regions = $this->SubDistricts->Regions->find('list', ['limit' => 200]);
        $provinces = $this->SubDistricts->Provinces->find('list', ['limit' => 200]);
        $districts = $this->SubDistricts->Districts->find('list', ['limit' => 200]);
        $this->set(compact('subDistrict', 'regions', 'provinces', 'districts'));
        $this->set('_serialize', ['subDistrict']);
    }

    /**
     *
     * Edit method make for update Sub District.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sub District id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/04/20 18:21:04
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->SubDistricts->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sub district, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $subDistrict = $this->SubDistricts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $subDistrict = $this->SubDistricts->patchEntity($subDistrict, $this->request->getData());
            if ($this->SubDistricts->save($subDistrict)) {
                $this->Flash->success(__('The sub district has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sub district could not be update. Please, try again.'));
        }
        $regions = $this->SubDistricts->Regions->find('list', ['limit' => 200]);
        $provinces = $this->SubDistricts->Provinces->find('list', ['limit' => 200]);
        $districts = $this->SubDistricts->Districts->find('list', ['limit' => 200]);
        $this->set(compact('subDistrict', 'regions', 'provinces', 'districts'));
        $this->set('_serialize', ['subDistrict']);
    }


    /**
     *
     * Delete method make for delete record of Sub District.
     *
     * @author  sarawutt.b
     * @param   string|null $id Sub District id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/04/20 18:21:04
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->SubDistricts->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested sub district, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $subDistrict = $this->SubDistricts->get($id);
        $respond = [];
        
        if ($this->SubDistricts->delete($subDistrict)) {
            $respond = $this->buildRequestRespond(__('The sub district has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The sub district could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
