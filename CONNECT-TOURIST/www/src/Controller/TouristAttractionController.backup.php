<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\DateTime;
use Cake\ORM\TableRegistry;

/**
 * TouristAttraction Controller
 *
 * @property \App\Model\Table\TouristAttractionTable $TouristAttraction
 *
 * @method \App\Model\Entity\TouristAttraction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TouristAttractionController extends AppController
{

    // public $helpers = [
    //     'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
    //     'Html' => ['className' => 'Bootstrap.Html'],
    //     'Modal' => ['className' => 'Bootstrap.Modal'],
    //     'Navbar' => ['className' => 'Bootstrap.Navbar', 'autoActiveLink' => true],
    //     'Paginator',
    //     'Panel' => ['className' => 'Bootstrap.Panel']
    // ];

    // public function initialize() {
    //     parent::initialize();
    //     $this->Auth->allow(['add', 'edit','index','map','mapmarker','view','loopmapmarker']); // temp
    // }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $this->viewBuilder()->setTheme('AdminLTE204');

        $this->paginate = [
        'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
        ];



        $test_numtok = "numtok";
        $test_talae = "talae";
        //=========================================================//
        $touristAttraction = $this->TouristAttraction->find('all')
        ->where([
            'name' => $test_talae
            ])
        ->toArray();
        // pr($touristAttraction);die;

        // pr($touristAttraction['0']['province_id']);die;

        $this->loadModel('MasterProvince');
        $masterProvince = $this->MasterProvince->find('all')
        ->where([
            'id' => $touristAttraction['0']['province_id']
            ])
        ->first();
        // //=========================================================//   
        // Test for each google map marker // 
        // $touristAttraction = $this->TouristAttraction->find('all')
        //     ->toArray();
        // pr($touristAttraction);die;
        // //=========================================================//    



        // pr($masterProvince);die;

        // $touristAttraction = $this->paginate($this->TouristAttraction);
        // pr($touristAttraction);die;

        $this->set(compact('touristAttraction','masterProvince'));
    }

    /**
     * View method
     *
     * @param string|null $id Tourist Attraction id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
            $touristAttraction = $this->TouristAttraction->get($id, [
                'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
                ]);

            $this->set('touristAttraction', $touristAttraction);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
            $touristAttraction = $this->TouristAttraction->newEntity();
            if ($this->request->is('post')) {
                $touristAttraction = $this->TouristAttraction->patchEntity($touristAttraction, $this->request->getData());
                if ($this->TouristAttraction->save($touristAttraction)) {
                    $this->Flash->success(__('The tourist attraction has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The tourist attraction could not be saved. Please, try again.'));
            }
            $category = $this->TouristAttraction->Category->find('list', ['limit' => 200]);
            $masterProvince = $this->TouristAttraction->MasterProvince->find('list', ['limit' => 200]);
            $reviewTourist = $this->TouristAttraction->ReviewTourist->find('list', ['limit' => 200]);
            $this->set(compact('touristAttraction', 'category', 'masterProvince', 'reviewTourist'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tourist Attraction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
            $touristAttraction = $this->TouristAttraction->get($id, [
                'contain' => []
                ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $touristAttraction = $this->TouristAttraction->patchEntity($touristAttraction, $this->request->getData());
                if ($this->TouristAttraction->save($touristAttraction)) {
                    $this->Flash->success(__('The tourist attraction has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The tourist attraction could not be saved. Please, try again.'));
            }
            $category = $this->TouristAttraction->Category->find('list', ['limit' => 200]);
            $masterProvince = $this->TouristAttraction->MasterProvince->find('list', ['limit' => 200]);
            $reviewTourist = $this->TouristAttraction->ReviewTourist->find('list', ['limit' => 200]);
            $this->set(compact('touristAttraction', 'category', 'masterProvince', 'reviewTourist'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tourist Attraction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
            $this->request->allowMethod(['post', 'delete']);
            $touristAttraction = $this->TouristAttraction->get($id);
            if ($this->TouristAttraction->delete($touristAttraction)) {
                $this->Flash->success(__('The tourist attraction has been deleted.'));
            } else {
                $this->Flash->error(__('The tourist attraction could not be deleted. Please, try again.'));
            }

            return $this->redirect(['action' => 'index']);
    }
    public function map()
    {
            // echo "test";die;
            $this->paginate = [
            'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
            ];

            // //=========================================================//   
            // Test for each google map marker // 
            $touristAttraction = $this->TouristAttraction->find('all')
            ->toArray();
            // pr($touristAttraction);die;
            // //=========================================================//    

            $this->set(compact('touristAttraction'));
    }

    public function mapmarker()
    {
            // $this->viewBuilder()->setTheme('AdminLTE204');

            $this->paginate = [
            'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
            ];

            // //=========================================================//   
            // Test for each google map marker // 
            $touristAttraction = $this->TouristAttraction->find('all')
            ->toArray();
            // pr($touristAttraction);die;
            // //=========================================================//    
            $this->loadModel('MasterProvince');
            $masterProvince = $this->MasterProvince->find('all')
            ->where(['id' => '1'])
            ->toArray();
            // pr($masterProvince);die;

            $count_touristAttraction = count($touristAttraction);
            // pr($count_touristAttraction);die;

            $this->set(compact('touristAttraction','masterProvince','count_touristAttraction'));
            // $this->set(compact('touristAttraction'));
            // $this->set(compact('masterProvince'));
    }
    public function loopmapmarker(){
           $this->paginate = [
           'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
           ];

            // //=========================================================//   
            // Test for each google map marker // 
           $touristAttraction = $this->TouristAttraction->find('all')
           ->toArray();
            // pr($touristAttraction);die;
         // $test = json_encode($touristAttraction);
            // //=========================================================//    
           $this->loadModel('MasterProvince');
           $masterProvince = $this->MasterProvince->find('all')
           ->where(['id' => '1'])
           ->toArray();
            // pr($masterProvince);die;

           $count_touristAttraction = count($touristAttraction);
            // pr($count_touristAttraction);die;

           $this->set(compact('touristAttraction','masterProvince','count_touristAttraction'));
            // $this->set(compact('touristAttraction'));
            // $this->set(compact('masterProvince'));
   }
    public function test(){
            $this->autoRender = false;
                   // if ($this->request->is('post')) {
            $touristAttraction = $this->TouristAttraction->find('all');
            $data = $touristAttraction->toArray();
                      //  pr($data);die;
            echo json_encode($data);
                  //  }
    }

    public function bloc($id = null){
            // id get from search torist , image have id tourist to parameter in function bloc
            $id = 3; // id of touristAttraction & hide id this line for get from id parameter

            $this->paginate = [
            'contain' => ['Category', 'MasterProvince', 'ReviewTourist']
            ];

            $touristAttraction_find_id = $this->TouristAttraction->find('all')
            ->select(['id','province_id','path_picture_tourist_attraction','create','count_like'])
            ->where(['id' => $id])
            ->toArray();

            $now = new Time($touristAttraction_find_id['0']['create']);
           // pr($now);
            $create_time = $now->i18nFormat('dd-MM-yyyy');

            $this->loadModel('MasterProvince');
            $MasterProvince = $this->MasterProvince->find('all')
            ->select(['id','province_name_th','logo_of_province'])
            ->where(['id' => $touristAttraction_find_id['0']['province_id']])
            ->toArray();

            $count_touristAttraction = count($touristAttraction_find_id);

            $checkuserId = $this->Auth->user('id');
            // pr($userId);die;
            $this->loadModel('UserActive');
            $arrLike = $this->UserActive->findLike($checkuserId);
            // pr($arrLike);die;

            // $check_status_like = $this->UserActive->find('all',
            //     [
            //             'conditions' => [
            //                 'tourist_attraction_id' => $touristAttraction_find_id[0]['id'],
            //                 'user_id' => $userId
            //         ]
            //     ])
            // ->toArray();
            // pr($check_status_like);
            // die;

            $this->set(compact('touristAttraction','count_touristAttraction','MasterProvince',
                'touristAttraction_find_id','create_time','check_status_like','arrLike'));
    }

    //     public function like() {
    //         $response['status'] = 'FAILED';
    //         if ($this->request->is('post')) {
    //             $request = $this->request->getData();
    //             $tourist_attraction_id = $request['tourist_attraction_id']; // id torist 
    //             $active_type_id = 1; // id like
    //             $status_like = 'y';
    //             $user_id = $this->getAuthUserId(); // get id from users commu
    //             $amountLink = [];
    //             pr("0");

    //             // $checkuserId = $this->Auth->user('id');
    //             // // pr($userId);die;
    //             // $this->loadModel('UserActive');
    //             // $arrLike = $this->UserActive->findLike($checkuserId);
    //             // pr($arrLike);
    //             // foreach ($arrLike as $key => $value) {
    //             //     pr($key);
    //             //     pr($value);
    //             // }
    //             // pr($value);

    //             $this->loadModel('UserActive');
    //             $userActive_check_conditions = $this->UserActive->find('all',
    //             [
    //                     'conditions' => [
    //                         'user_id' => $user_id,
    //                         'tourist_attraction_id' => $tourist_attraction_id,
    //                         'status_like' => $status_like
    //                 ]
    //             ])
    //             ->toArray();
    //             pr($userActive_check_conditions);

    //             if (!empty($tourist_attraction_id)) { // disklike
    //                 pr("1");
    //                 $this->loadModel('TouristAttraction');
    //                 $TouristAttraction = $this->TouristAttraction->get($tourist_attraction_id);
    //                 $TouristAttraction_update = $this->TouristAttraction->patchEntity($TouristAttraction, $amountLink);
    //                 $TouristAttraction_update['count_like'] = $TouristAttraction['count_like'] - 1;

    //                 if(!empty($userActive_check_conditions)){ // addlike
    //                     pr("1.1");
    //                     $this->request->allowMethod(['post', 'delete']);
    //                     $array_user_active = $this->UserActive->get($userActive_check_conditions[0]['id']);
    //                     // $array_user_active = $this->UserActive->get($value);
    //                     pr($array_user_active);
    //                     if ($this->UserActive->delete($array_user_active)) {
    //                         $this->Flash->success(__('dislike success.'));
    //                     } else {
    //                         $this->Flash->error(__('dislike fail.'));
    //                     }

    //                     if ($TouristAttraction = $this->TouristAttraction->save($TouristAttraction_update)) {
    //                         $response['count_like'] = $TouristAttraction->count_like;
    //                     }

    //                     $response['status'] = 'OK';
    //                 }
    //                 else if(empty($userActive_check_conditions)){
    //                     pr("1.2");
    //                     $this->loadModel('TouristAttraction');
    //                     $TouristAttraction = $this->TouristAttraction->get($tourist_attraction_id);
    //                     // pr($TouristAttraction);
    //                     $TouristAttraction_update = $this->TouristAttraction->patchEntity($TouristAttraction, $amountLink);
    //                     // pr($TouristAttraction_update);
    //                     $TouristAttraction_update['count_like'] = $TouristAttraction['count_like'] + 1;
    //                     // pr($TouristAttraction_update);

    //                     $this->loadModel('UserActive');

    //                     $data = [];
    //                     $data['user_id'] = $user_id;
    //                     $data['tourist_attraction_id'] = $tourist_attraction_id;
    //                     $data['create_uid'] = $user_id;
    //                     $data['active_type_id'] = $active_type_id;
    //                     $data['status_like'] = $status_like;

    //                     // pr($data);die;

    //                     $UserActive = $this->UserActive->newEntity();
    //                     // pr($UserActive);
    //                     $UserActive_update = $this->UserActive->patchEntity($UserActive,$data);
    //                     // pr($UserActive_update);die;

    //                     if ($UserActive = $this->UserActive->save($UserActive_update)) {
    //                         $response['status_like'] = $UserActive->status_like;
    //                     }

    //                     if ($TouristAttraction = $this->TouristAttraction->save($TouristAttraction_update)) {
    //                         $response['count_like'] = $TouristAttraction->count_like;
    //                     }

    //                     $response['status'] = 'OK';
    //                 }
    //             }else {
    //                 $response['status'] = 'FAILED';
    //             }
    //         }
    //         $this->set(compact('response'));
    //         $this->set('_serialize', ['response']);
    // }

    public function like() {
            $response['status'] = 'FAILED';
            if ($this->request->is('post')) {
                $request = $this->request->getData();
                $tourist_attraction_id = $request['tourist_attraction_id']; // id torist 
                $active_type_id = 1; // id like
                $status_like = 'y';
                $user_id = $this->getAuthUserId(); // get id from users commu
                pr("variable");

                if (!empty($tourist_attraction_id)) {
                    pr("addlike");
                    $amountLink = [];
                    $response = [];
                    $this->loadModel('TouristAttraction');
                    $TouristAttraction = $this->TouristAttraction->get($tourist_attraction_id);
                    $TouristAttraction_update = $this->TouristAttraction->patchEntity($TouristAttraction, $amountLink);
                    $TouristAttraction_update['count_like'] = $TouristAttraction['count_like'] + 1;

                    $this->loadModel('UserActive');

                    $data = [];
                    $data['user_id'] = $user_id;
                    $data['tourist_attraction_id'] = $tourist_attraction_id;
                    $data['create_uid'] = $user_id;
                    $data['active_type_id'] = $active_type_id;
                    $data['status_like'] = $status_like;

                    // pr($data);

                    $UserActive = $this->UserActive->newEntity();
                    // pr($UserActive);
                    $UserActive_update = $this->UserActive->patchEntity($UserActive,$data);
                    // pr($UserActive_update);

                    if ($UserActive = $this->UserActive->save($UserActive_update)) {
                        $response['status_like'] = $UserActive->status_like;
                    }

                    if ($TouristAttraction = $this->TouristAttraction->save($TouristAttraction_update)) {
                        $response['count_like'] = $TouristAttraction->count_like;
                    }

                    $response['status'] = 'OK';
                } else {
                    $response['status'] = 'FAILED';
                }

            }
            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
    }
    public function dislike() {
            $response['status'] = 'FAILED';
            if ($this->request->is('post')) {
                $request = $this->request->getData();
                $tourist_attraction_id = $request['tourist_attraction_id']; // id torist 
                $status_like = 'y';
                $user_id = $this->getAuthUserId(); // get id from users commu
                pr("dislike function");

                if (!empty($tourist_attraction_id)) {
                    $amountLink = [];
                    $response = [];
                    $this->loadModel('TouristAttraction');
                    $TouristAttraction = $this->TouristAttraction->get($tourist_attraction_id);
                    $TouristAttraction_update = $this->TouristAttraction->patchEntity($TouristAttraction, $amountLink);
                    $TouristAttraction_update['count_like'] = $TouristAttraction['count_like'] - 1;

                    $this->loadModel('UserActive');
                    $userActive_check_conditions = $this->UserActive->find('all',
                    [
                            'conditions' => [
                                'user_id' => $user_id,
                                'tourist_attraction_id' => $tourist_attraction_id,
                                'status_like' => $status_like
                        ]
                    ])
                    ->toArray();

                    /////////////////////////////////////////
                    // $useractive_update = $this->UserActive->get($userActive_check_conditions[0]['id']);
                    // $useractive_update = $this->UserActive->patchEntity($UserActive, $amountLink);
                    // $useractive_update['status_like'] = 'n';

                    // if ($UserActive = $this->UserActive->save($useractive_update)) {
                    //     $response['status_like'] = $UserActive->status_like;
                    // }
                    /////////////////////////////////////////
                    ///////////////////////////////////////// delete data row from id torist and check in useractive
                    if(!empty($userActive_check_conditions)){
                        $this->request->allowMethod(['post', 'delete']);
                        $array_user_active = $this->UserActive->get($userActive_check_conditions[0]['id']);
                        pr($array_user_active);
                        if ($this->UserActive->delete($array_user_active)) {
                            $this->Flash->success(__('dislike success.'));
                        } else {
                            $this->Flash->error(__('dislike fail.'));
                        }

                        if ($TouristAttraction = $this->TouristAttraction->save($TouristAttraction_update)) {
                            $response['count_like'] = $TouristAttraction->count_like;
                        }
                    }
                    /////////////////////////////////////////

                    $response['status'] = 'OK';
                } else {
                    $response['status'] = 'FAILED';
                }

            }
            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
    }

}
