<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Client;
use Cake\Core\Configure;

//use Cake\Auth\DefaultPasswordHasher;

/**
 *
 * Home Controller
 * @author  pakgon.Ltd
 * @since   2018-02-07 08:17:39
 * @license Pakgon.Ltd
 */
class HomeController extends AppController {

    /**
     * 
     * Function controller trigger when controller initialize
     * @author sarawutt.b
     */
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['callback']);
    }

    /**
     *
     * Home fore any loged users
     * @author  pakgon.Ltd
     * @return \Cake\Http\Response|void
     * @since   2018-02-07 08:17:39
     * @license Pakgon.Ltd
     */
    public function index() {
        
    }

    public function callback() {
        $params = [
            'grant_type' => 'authorization_code',
            'client_id' => Configure::read('OAUTH2_PROVIDER.CLIENT_ID'),
            'client_secret' => Configure::read('OAUTH2_PROVIDER.CLIENT_SECRET'),
            'redirect_uri' => Configure::read('OAUTH2_PROVIDER.REDIRECT_URI'),
            'code' => $this->request->query('code')
        ];

        try {
            $http = new Client();
            $response = $http->post(Configure::read('OAUTH2_PROVIDER.ACCESS_TOKEN_URL'), $params, ['timeout' => 120])->body();
            // pr($response);die;
            $response = json_decode($response, true);
            // pr($response);die;

            if (array_key_exists('access_token', $response)) {
                // pr("ok");
                $accessToken = $response['access_token'];
                $tokenType = $response['token_type'];
                $expiresIn = $response['expires_in'];
                $refreshToken = $response['refresh_token'];
                $http = new Client([
                    'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]);

                $response['expire'] = $response['expires_in'];

                //Create by sarawutt.b 
                //For reason keep token for check token is valid on oauth server
                $this->request->session()->write('token', $response);

                $params = [
                    'client_id' => Configure::read('OAUTH2_PROVIDER.CLIENT_ID'),
                    'access_token' => $accessToken
                ];

                $response = $http->post(Configure::read('OAUTH2_PROVIDER.SERVER') . '/users/getOauthInfo.json?access_token=' . $accessToken, $params)->body();
                // pr($response);die;
                $response = json_decode($response, true);
                // pr($response);die;
                if (array_key_exists('user', $response['response'])) {
                    // pr("ok na ja");die;
                    if (!empty($response['response'])) {
                        // pr("ok na ja");die;
                        // pr($response);die;

                        // auth for db default commu //
                        $this->loadModel('Users');
                        $user = $this->Users->find('all', [
                                    'conditions' => [
                                        'username' => $response['response']['user'][0]['username']
                                    ]
                                        ]
                                )->toArray();
                        // pr($user);die;
                        /////////////////////////

                        // auth for db_torist //
                        // $this->loadModel('UserTourist');
                        // $user = $this->UserTourist->find('all', [
                        //             'conditions' => [
                        //                 'username' => $response['response']['user'][0]['username']
                        //         ]
                        //     ]
                        // )->toArray();   
                        /////////////////////////

                        if (!empty($user)) {
                            // $user = $user->toArray();
                            // pr($user);die;
                            $this->Auth->setUser($user);
                            // $this->Auth->setUser($user);
                        } else {
                            echo '<script type="text/javascript">alert("' . __('Invalid username or password Please try again!') . '");</script>';
                            exit;
                        }
                    } else {
                        echo '<script type="text/javascript">alert("' . __('You not allow to the TouristAttraction if you want to the system contact to Administrator.') . '");</script>';
                        exit;
                    }
                }
                return $this->redirect('/');
            } else {
                return $this->redirect(Configure::read('OAUTH2_PROVIDER.AUTH_REDIRECT_LOGIN'));
            }
        } catch (\Exception $ex) {
            $this->log('[ERROR] occurred :: ' . $ex->getFile());
            $this->log($ex);
            echo '<script type="text/javascript">alert("' . __('Has something wrong please contact Administrator!!!') . '");</script>';
            exit;
        }
    }

    //  callback commu
    // public function callback() {
    //     $params = [
    //         'grant_type' => 'authorization_code',
    //         'client_id' => Configure::read('OAUTH2_PROVIDER.CLIENT_ID'),
    //         'client_secret' => Configure::read('OAUTH2_PROVIDER.CLIENT_SECRET'),
    //         'redirect_uri' => Configure::read('OAUTH2_PROVIDER.REDIRECT_URI'),
    //         'code' => $this->request->query('code')
    //     ];
    //     $http = new Client();

    //     try {
    //         $response = $http->post(Configure::read('OAUTH2_PROVIDER.ACCESS_TOKEN_URL'), $params, ['timeout' => 120])->body();
    //         // pr($response);
    //         $response = json_decode($response, true);
    //         // pr($response);
    //         // pr(array_key_exists('access_token', $response));die;
    //         if (array_key_exists('access_token', $response)) {
    //             pr('in');
    //             // pr($response);die;
    //             $accessToken = $response['access_token'];
    //             $tokenType = $response['token_type'];
    //             $expiresIn = $response['expires_in'];
    //             $refreshToken = $response['refresh_token'];
    //             $http = new Client([
    //                 'headers' => ['Authorization' => 'Bearer ' . $accessToken]
    //             ]);
    //             // pr($http);die;
    //             $response['expire'] = $response['expires_in'];

    //             //Create by sarawutt.b 
    //             //For reason keep token for check token is valid on oauth server
    //             $this->request->session()->write('token', $response);
    //             $params = [
    //                 'client_id' => Configure::read('OAUTH2_PROVIDER.CLIENT_ID'),
    //                 'access_token' => $accessToken
    //             ];
    //             // pr($params);die;

    //             $response = $http->post(Configure::read('OAUTH2_PROVIDER.SERVER') . '/users/getOauthInfo.json?access_token=' . $accessToken, $params)->body();
    //             // pr($response);die;
    //             $response = json_decode($response, true);

    //             $nUserProfiles = [];
    //             if (array_key_exists('user', $response['response'])) {
    //                 if (array_key_exists('userCards', $response['response'])) {
    //                     $this->request->session()->write('User.Info.UserCards', $response['response']['userCards']);
    //                 }

    //                 if (!empty($response['response'])) {

    //                     $this->loadModel('Users');
    //                     $this->Users->getConnection()->begin();
                        
    //                     $isSave = true;

    //                     $user = $this->Users->find('all', [
    //                                 'conditions' => [
    //                                     'username' => $response['response']['user'][0]['username']
    //                                 ]
    //                                     ]
    //                             )->toArray();
    //                     $this->loadModel('userProfiles');
    //                     $this->loadModel('userEntities');

    //                     if (!empty($user)) {//Update user and release of the occure users
    //                         $nUserProfiles = $this->userProfiles->find('all', [
    //                                     'conditions' => [
    //                                         'user_id' => $user[0]['id'],
    //                                     ]
    //                                         ]
    //                                 )->first();


    //                         $userProfiles = array();
    //                         $nUserProfiles = $this->userProfiles->patchEntity($nUserProfiles, $userProfiles);
                            
    //                         $nUserProfiles['update_uid'] = $user[0]['id'];
    //                         $nUserProfiles['first_name'] = @$response['response']['personals'][0]['firstname_th'];

    //                         $userProfiles['last_name'] = @$response['response']['personals'][0]['lastname_th'];
    //                         $userProfiles['user_type_id'] = @$response['response']['userCards'][0]['user_type_id'];

    //                         if (empty($response['response']['personals'][0]['master_country_id'])) {
    //                             $response['response']['personals'][0]['master_country_id'] = 1;
    //                         }
    //                         if (empty($response['response']['personals'][0]['master_province_id'])) {
    //                             $response['response']['personals'][0]['master_province_id'] = 1;
    //                         }

    //                         $nUserProfiles['countrie_id'] = @$response['response']['personals'][0]['master_country_id'];
    //                         $nUserProfiles['citie_id'] = @$response['response']['personals'][0]['master_province_id'];
                            
    //                         $nUser = array();
    //                         $nUser = $this->Users->patchEntity($user[0], $nUser);
    //                         $nUser['token'] = $response['response']['user'][0]['token'];

    //                         if (!$this->Users->save($nUser)) {
    //                             $isSave = false;  
    //                         }
                            
    //                         if (!$this->userProfiles->save($nUserProfiles)) {
    //                             $isSave = false;  
    //                         }

    //                         if($isSave==True){
    //                             $this->Users->getConnection()->commit();
    //                             $userComm = $nUser;
    //                         }else{
    //                             $this->Users->getConnection()->rollback();
    //                             $userComm = $nUser;
    //                         }

    //                         foreach ($response['response']['entities'] as $entities) {

    //                             $nUserEntities = $this->userEntities->find('all', [
    //                                         'conditions' => [
    //                                             'user_id' => $user[0]['id'],
    //                                             'entity_code' => $entities['entity_code'],
    //                                         ]
    //                                             ]
    //                                     )->first();

    //                             if (empty($nUserEntities)) {

    //                                 $UserEntity['user_id'] = $user[0]['id'];
    //                                 $UserEntity['entity_code'] = $entities['entity_code'];
    //                                 $UserEntity['create_uid'] = $user[0]['id'];

    //                                 $nUserEntity = $this->userEntities->newEntity();
    //                                 $nUserEntity = $this->userEntities->patchEntity($nUserEntity, $UserEntity);

    //                                 if (!$this->userEntities->save($nUserEntity)) {
    //                                     $isSave = false;
    //                                 }//.if ($this->userEntities->save($nUserEntity))
    //                             }//.if(empty($nUserEntities))   
    //                         }//.foreach($response['response']['entities']

    //                         $userComm = $this->Users->get($user[0]->id);
    //                     } else {//insert new user and release in to the commu database
    //                         //$hasher = new DefaultPasswordHasher();
    //                         //$password = '12345678';
    //                         $user['username'] = $response['response']['user'][0]['username'];
    //                         //$user['password'] = $hasher->hash($password);
    //                         $user['password'] = '12345678';
    //                         $user['create_uid'] = $response['response']['user'][0]['id'];
    //                         $user['token'] = $response['response']['user'][0]['token'];

    //                         $nUser = $this->Users->newEntity();
    //                         $nUser = $this->Users->patchEntity($nUser, $user);

    //                         if (!$this->Users->save($nUser)) {
    //                             $isSave = false;
    //                         }

    //                             $userProfiles['user_id'] = $nUser->id;
    //                             $userProfiles['create_uid'] = $nUser->id;
    //                             $userProfiles['first_name'] = @$response['response']['personals'][0]['firstname_th'];
    //                             $userProfiles['last_name'] = @$response['response']['personals'][0]['lastname_th'];

    //                             if (empty($response['response']['personals'][0]['master_country_id'])) {
    //                                 $response['response']['personals'][0]['master_country_id'] = 1;
    //                             }
    //                             if (empty($response['response']['personals'][0]['master_province_id'])) {
    //                                 $response['response']['personals'][0]['master_province_id'] = 1;
    //                             }

    //                             $userProfiles['countrie_id'] = @$response['response']['personals'][0]['master_country_id'];
    //                             $userProfiles['citie_id'] = @$response['response']['personals'][0]['master_province_id'];

    //                             $nUserProfiles = $this->userProfiles->newEntity();
    //                             $nUserProfiles = $this->userProfiles->patchEntity($nUserProfiles, $userProfiles);

    //                             if (!$this->userProfiles->save($nUserProfiles)) {

    //                                 $isSave = false;
    //                             }

    //                             if($isSave==True){
    //                                 $this->Users->getConnection()->commit();
    //                                 $userComm = $nUser;
    //                             }else{
    //                                 $this->Users->getConnection()->rollback();
    //                                 $userComm = $nUser;
    //                             }

    //                             foreach ($response['response']['entities'] as $entities) {
    //                                 $UserEntity['user_id'] = $nUser->id;
    //                                 $UserEntity['entity_code'] = $entities['entity_code'];
    //                                 $UserEntity['create_uid'] = $nUser->id;

    //                                 $nUserEntity = $this->userEntities->newEntity();
    //                                 $nUserEntity = $this->userEntities->patchEntity($nUserEntity, $UserEntity);

    //                                 if ($this->userEntities->save($nUserEntity)) {
    //                                     $isSave = false;
    //                                 }
    //                             }
    //                         // }
    //                     }
    //                 } else {
    //                     echo "null";
    //                 }

    //                 $this->Auth->setUser($userComm);
    //                 $this->request->session()->write('Auth.UserPersonals', empty($response['response']['personals']) ? [] : $response['response']['personals'][0]);
    //                 $this->request->session()->write('Auth.UserProfiles', $nUserProfiles);
    //             }
    //             return $this->redirect('/');
    //         } else {
    //             return $this->redirect(Configure::read('OAUTH2_PROVIDER.AUTH_REDIRECT_LOGIN'));
    //         }
    //     } catch (\Exception $ex) {
    //         $this->log('[ERROR] occurred :: ' . $ex->getFile(), 'debug');
    //         $this->log($ex, 'debug');
    //         return $this->redirect(Configure::read('OAUTH2_PROVIDER.AUTH_REDIRECT_LOGIN'));
    //     }
    // }

}
