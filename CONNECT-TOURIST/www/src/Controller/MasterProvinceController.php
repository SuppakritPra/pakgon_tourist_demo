<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MasterProvince Controller
 *
 * @property \App\Model\Table\MasterProvinceTable $MasterProvince
 *
 * @method \App\Model\Entity\MasterProvince[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MasterProvinceController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Region']
        ];
        $masterProvince = $this->paginate($this->MasterProvince);

        $this->set(compact('masterProvince'));
    }

    /**
     * View method
     *
     * @param string|null $id Master Province id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $masterProvince = $this->MasterProvince->get($id, [
            'contain' => ['Region']
        ]);

        $this->set('masterProvince', $masterProvince);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $masterProvince = $this->MasterProvince->newEntity();
        if ($this->request->is('post')) {
            $masterProvince = $this->MasterProvince->patchEntity($masterProvince, $this->request->getData());
            if ($this->MasterProvince->save($masterProvince)) {
                $this->Flash->success(__('The master province has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The master province could not be saved. Please, try again.'));
        }
        $region = $this->MasterProvince->Region->find('list', ['limit' => 200]);
        $this->set(compact('masterProvince', 'region'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Master Province id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $masterProvince = $this->MasterProvince->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $masterProvince = $this->MasterProvince->patchEntity($masterProvince, $this->request->getData());
            if ($this->MasterProvince->save($masterProvince)) {
                $this->Flash->success(__('The master province has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The master province could not be saved. Please, try again.'));
        }
        $region = $this->MasterProvince->Region->find('list', ['limit' => 200]);
        $this->set(compact('masterProvince', 'region'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Master Province id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $masterProvince = $this->MasterProvince->get($id);
        if ($this->MasterProvince->delete($masterProvince)) {
            $this->Flash->success(__('The master province has been deleted.'));
        } else {
            $this->Flash->error(__('The master province could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
