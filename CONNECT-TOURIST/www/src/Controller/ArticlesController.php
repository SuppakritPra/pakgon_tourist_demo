<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Filesystem\File;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 *
 * @method \App\Model\Entity\Article[] paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($pageid = null) {

        $wherer_3 = "";
        $wherer_4 = "";
        $publish_date_begin = null;
        $publish_date_end = null;
        $now = Time::now();
        $dateNow = $now->i18nFormat('yyyy-MM-dd 00:00:00');
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $article_title = $this->request->data['article_title'];
            $show_expire = $this->request->data['show_expire'];
            if (!empty($this->request->data['expire_date'])) {
                $article_between_date = explode("-", $this->request->data['expire_date']);
                $article['publish_date_begin'] = str_replace("/", "-", $article_between_date[0]) . "00:00:00";
                $article['publish_date_end'] = str_replace("/", "-", $article_between_date[1]) . "23:59:59";
                $publish_date_begin = $article['publish_date_begin'];
                $publish_date_begin = date("Y-m-d 00:00:00", strtotime($publish_date_begin));
                $publish_date_end = $article['publish_date_end'];
                $publish_date_end = date("Y-m-d 23:59:59", strtotime($publish_date_end));

                $publish_date_begin_set = trim($article_between_date[0]);
                $publish_date_end_set = trim(($article_between_date[1]));
                $publish_date_begin = "articles.publish_date >= '" . trim($publish_date_begin) . "'";
                $publish_date_end = "Articles.publish_date <= '" . trim($publish_date_end) . "'";
            }

            if ($show_expire == 1) {
                $dateNow = "";
            } else {
                $dateNow = "Articles.expire_date >= '" . $dateNow . "'";
            }

            if ($article_title) {
                $article_title = 'upper(Articles.article_title) like ' . "upper('%" . trim($article_title) . "%')";
            } else {
                $article_title = "";
            }
            $this->set('show_expire', $show_expire);
        } else {
            $article_title = "";
            $publish_date_begin = "";
            $publish_date_end = "";
            $dateNow = "Articles.expire_date >= '" . $dateNow . "'";
        }

        $this->loadModel('articles');
        $articles = $this->articles->find('all')
                ->where(
                [
                    'Articles.page_id' => $pageid,
                    $dateNow,
                    $article_title,
                    $publish_date_begin,
                    $publish_date_end
                ]
        );

        $this->set('dateNow', $now);
        $this->set(compact('articles', 'pageid', 'publish_date_begin_set', 'publish_date_end_set'));
        $this->set('_serialize', ['articles']);
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $article = $this->Articles->get($id, [
            'contain' => ['Pages', 'ArticleAssets', 'UserActivites']
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($page_id = null) {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $page_id = $this->request->data['page_id'];
            $article_between_date = explode("-", $this->request->data['expire_date']);
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            $public_date[0] = str_replace('/', '-', $article_between_date[0]);
            $public_date[0] = trim($public_date[0]) . ':00';
            $public_date[0] = new Time($public_date[0]);
            $public_date[1] = str_replace('/', '-', $article_between_date[1]);
            $public_date[1] = trim($public_date[1]) . ':00';
            $public_date[1] = new Time($public_date[1]);
            $article['publish_date'] = $public_date[0];
            $article['expire_date'] = $public_date[1];
            $article['page_id'] = $page_id;
            $article['create_uid'] = $this->Auth->user('id');
            if ($article['feed_flag']) {
                $article['feed_flag'] = 'Y';
            } else {
                $article['feed_flag'] = 'N';
            }

            if ($articles = $this->Articles->save($article)) {
                $this->articles_id = $articles->id;
                $isError = false;
                if (!$isError) {
                    $document_file = $this->__upload_or_file($this->request->data);
                    if ($document_file['isError']) {
                        $isError = true;
                        $errorMessage = __("Cannot save file.");
                    }
                }
                $this->Flash->success(__('Save success.'));
                return $this->redirect(['action' => 'index', $page_id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }

        $today = Time::now();
        $dateNow = $today->i18nFormat('yyyy-MM-dd HH:mm');
        $ts_timeNow = strtotime($dateNow);
        $this->set('today', $today);
        $pages = $this->Articles->Pages->find('list', ['limit' => 200]);
        $this->set(compact('article', 'pages', 'page_id'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);

        $this->loadModel('ArticleAssets');
        $article_assets = $this->ArticleAssets->find('all', [
            'conditions' => [
                'ArticleAssets.article_id' => $id
            ]
        ]);
        $this->request->data['article_id'] = $id;
        $publish = $article['publish_date'];
        $publishNew = $publish->i18nFormat('dd/MM/yyyy HH:mm');
        $expire = $article['expire_date'];
        $expireNew = $expire->i18nFormat('dd/MM/yyyy HH:mm');
        $page_id = $article->page_id;
        // pr($publishNew);die;

        if ($this->request->is(['post', 'put'])) {
            $article_between_date = explode("-", $this->request->data['expire_date']);

            $article = $this->Articles->patchEntity($article, $this->request->getData());

            $pd = str_replace('/', '-', $article_between_date[0]);
            $ed = str_replace('/', '-', $article_between_date[1]);

            $pd = trim($pd) . ':00';
            $ed = trim($ed) . ':00';

            $article['publish_date'] = new Time($pd);
            $article['expire_date'] = new Time($ed);

//            $page_id = $this->request->data['page_id'];

            if ($article['feed_flag'] == 1) {
                $article['feed_flag'] = 'Y';
            } else {
                $article['feed_flag'] = 'N';
            }

            if ($this->Articles->save($article)) {
                $document_file = $this->__upload_or_file($this->request->data);
                if ($document_file['isError']) {
                    $isError = true;
                    $errorMessage = __("Cannot save file.");
                }
                $this->Flash->success(__('Save success.'));

                return $this->redirect(['action' => 'index', $page_id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        //$pages = $this->Articles->Pages->find('list', ['limit' => 200]);
        //$langs = ['TH' => 'Thai', 'EN' => 'English', 'CN' => 'China'];

        $this->set(compact('article', 'publishNew', 'expireNew', 'article_assets', 'page_id'));
//        $this->set(compact('article', 'pages', 'langs', 'publishNew', 'expireNew', 'article_assets'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('Save success.'));
        } else {
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteArticleAssets($id = null) {

        $this->autoRender = false;

        if ($this->request->is('post')) {
            $this->loadModel('ArticleAssets');
            $request = $this->request->getData();
            $id = $request['id'];
            $article_assets = $this->ArticleAssets->get($id);
            $delete_files = $article_assets['path'];

            // if($article_assets['article_asset_type']=='image_title'){
            // }elseif($article_assets['article_asset_type']=='image_detail'){
            // }elseif($article_assets['article_asset_type']=='audio'){
            // }elseif($article_assets['article_asset_type']=='video'){
            // }

            $path_folder = "/upload/" . $article_assets['article_asset_type'] . "/";

            if ($this->ArticleAssets->delete($article_assets)) {
                $file = new File(WWW_ROOT . $path_folder . $delete_files, true, 0777);
                if ($file->delete()) {
                    echo 'image deleted.....';
                } else {
                    echo 'image no deleted.....';
                }
                //$this->Flash->success(__('The article has been deleted.'));
            } else {
                // $this->Flash->error(__('The article could not be deleted. Please, try again.'));
            }
        }

        echo 'true';
    }

    private function __upload_or_file($docRef) {
        //  pr($docRef);die;
        $today = Time::now();
        $dateNow = $today->i18nFormat('yyyyMMddHHmmss');
        $isError = false;
        if ($this->articles_id) {
            $articles_id = $this->articles_id;
        } else {
            $articles_id = $docRef['article_id'];
        }

        //upload of image_title
        if (!$isError && !empty($docRef['image_title']['tmp_name'])) {
            if (!empty($docRef['image_title'])) {
                $folder = 'upload/image_title';
                $randomString = $this->randomString(10);
                $name_file = $randomString . $dateNow . $docRef['image_title']['name'];
                $result = $this->uploadFiles($name_file, $folder, $docRef['image_title']);
                if (!$isError) {
                    $document_file = $this->Articles->ArticleAssets->newEntity();
                    $document_file['article_asset_type'] = 'image_title';
                    $document_file['article_asset_name'] = $result['uploadOriginFileNames'][0];
                    $document_file['path'] = $result['uploadFileNames'][0];
                    $document_file['article_id'] = $articles_id;
                    $document_file['create_uid'] = $this->Auth->user('id');
                    $document_file['update_uid'] = $this->Auth->user('id');

                    // debug($document_file);
                    // debug($this->Articles->ArticleAssets->save($document_file));
                    // die;
                    if (!$this->Articles->ArticleAssets->save($document_file)) {
                        $isError = true;
                        $errorMessage = __("Cannot save file.");
                    }
                }
            }
        }
        //upload of image_detail
        if (!$isError && !empty($docRef['image_detail'][0]['tmp_name'])) {
            if (!empty($docRef['image_detail'])) {
                $folder = 'upload/image_detail';
                $count = count($docRef['image_detail']);
                for ($i = 0; $i < $count - 1; $i++) {
                    $randomString = $this->randomString(10);
                    $name_file = $randomString . $dateNow . $docRef['image_detail'][$i]['name'];
                    $result = $this->uploadFiles($name_file, $folder, $docRef['image_detail'][$i]);
                    if (!$isError) {
                        $document_file = $this->Articles->ArticleAssets->newEntity();
                        $document_file['article_asset_type'] = 'image_detail';
                        $document_file['article_asset_name'] = $result['uploadOriginFileNames'][0];
                        $document_file['path'] = $result['uploadFileNames'][0];
                        $document_file['article_id'] = $articles_id;
                        $document_file['create_uid'] = $this->Auth->user('id');
                        $document_file['update_uid'] = $this->Auth->user('id');
                        if (!$this->Articles->ArticleAssets->save($document_file)) {
                            $isError = true;
                            $errorMessage = __("Cannot save file.");
                        }
                    }
                }
            }
        }
        //upload of sound
        if (!$isError && !empty($docRef['audio']['tmp_name'])) {
            if (!empty($docRef['audio'])) {
                $folder = 'upload/audio';
                $randomString = $this->randomString(10);
                $name_file = $randomString . $dateNow . $docRef['audio']['name'];
                $result = $this->uploadFiles($name_file, $folder, $docRef['audio']);
                if (!$isError) {
                    $document_file = $this->Articles->ArticleAssets->newEntity();
                    $document_file['article_asset_type'] = 'audio';
                    $document_file['article_asset_name'] = $docRef['name_for_audio'];
                    $document_file['path'] = $result['uploadFileNames'][0];
                    $document_file['article_id'] = $articles_id;
                    $document_file['create_uid'] = $this->Auth->user('id');
                    $document_file['update_uid'] = $this->Auth->user('id');
                    if (!$this->Articles->ArticleAssets->save($document_file)) {
                        $isError = true;
                        $errorMessage = __("Cannot save file.");
                    }
                }
            }
        }
        //upload of video
        if (!$isError && !empty($docRef['video']['tmp_name'])) {
            if (!empty($docRef['video'])) {
                $folder = 'upload/video';
                $randomString = $this->randomString(10);
                $name_file = $randomString . $dateNow . $docRef['video']['name'];
                $result = $this->uploadFiles($name_file, $folder, $docRef['video']);
                if (!$isError) {
                    $document_file = $this->Articles->ArticleAssets->newEntity();
                    $document_file['article_asset_type'] = 'video';
                    $document_file['article_asset_name'] = $docRef['name_for_video'];
                    $document_file['path'] = $result['uploadFileNames'][0];
                    $document_file['article_id'] = $articles_id;
                    $document_file['create_uid'] = $this->Auth->user('id');
                    $document_file['update_uid'] = $this->Auth->user('id');
                    if (!$this->Articles->ArticleAssets->save($document_file)) {
                        $isError = true;
                        $errorMessage = __("Cannot save file.");
                    }
                }
            }
        }
        // return ['isError'=>$isError,'errorMessage'=>$errorMessage];
    }

    public function randomString($lenght) {
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $randomString = '';
        for ($j = 0; $j < $lenght; $j++) {
            $randomString .= $characters[rand(0, 25)];
        }
        return $randomString;
    }

}
