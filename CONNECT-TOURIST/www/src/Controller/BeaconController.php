<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Http\Client;

class BeaconController extends AppController
{
    public $helpers = [
        'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
        'Html' => ['className' => 'Bootstrap.Html'],
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Navbar' => ['className' => 'Bootstrap.Navbar', 'autoActiveLink' => true],
        'Paginator',
        'Panel' => ['className' => 'Bootstrap.Panel']
    ];

    public function index( $user_id = 1 )
    {
        $user_id = $this->Auth->user('id');   
    }

    public function getArticleByBeacon(){
        
        $this->autoRender = false;

        $user_id = $this->Auth->user('id');

        if ($this->request->is('post')) {
            $now = Time::now();
            $dateNow = $now->i18nFormat('yyyy-MM-dd HH:MM:ss');
            $arrBeacons = json_decode($this->request->data['beacons']);
            //$token = $this->request->data['token'];
            $token = $this->request->session()->read('token');

            $beacon_var = array();
            foreach ($arrBeacons as $k => $beacon) {
                $beacon_var[] = $beacon->device;
            }

            $publish_date = "Articles.publish_date <= '".$dateNow."'";
            $expire_date = "Articles.expire_date >= '".$dateNow."'";

            $articleBeacons = array();
            $this->loadModel('articles');
            $articleBeacons = $this->articles->find('all')
                ->select([
                    'articles.id',
                    'articles.article_title',
                    'articles.article_intro'
                ])
                ->join([             
                    'cus' => [
                        'table' => 'communtcation.user_activites',
                        'type' => 'left',
                        'conditions' => [
                            'cus.article_id = articles.id',
                            'cus.activity_type' => 'B',
                            'cus.user_id'=>$user_id,
                        ],
                    ]
                ])
                ->where([
                    'articles.beacon_uid in'=> $beacon_var,
                    "(cus.activity_type != 'B' OR cus.activity_type is null)",
                    $publish_date, 
                    $expire_date,
                ])->toArray();

            $this->loadModel('userActivites');
            if(!empty($articleBeacons)){

                $countArticleBeacons = count($articleBeacons);

                foreach($articleBeacons as $articleBeacon ){

                    $new_user_activites = array();
                    $new_user_activites['user_id'] = $user_id;
                    $new_user_activites['article_id'] = $articleBeacon->id;
                    $new_user_activites['activity_type'] = 'B';
                    $new_user_activites['create_uid'] = $user_id;

                    $user_activites = $this->userActivites->newEntity();
                    $user_activites = $this->userActivites->patchEntity($user_activites, $new_user_activites);
                    if ($user_activites = $this->userActivites->save($user_activites)) {

                    //--- **** add table noti

                    }

                    $sentData = array();
                    $sentData['username'] = $this->Auth->user('username');
                    $sentData['token'] = $token;
                    $sentData['topic'] =  mb_substr($articleBeacon->article_title, 0, 20, 'utf-8')."...";
                    $sentData['shortDescription'] =  mb_substr($articleBeacon->article_intro, 0, 20, 'utf-8')."...";

                    $sentData['badge'] = $countArticleBeacons ;

                    $responseNoti = $this->sentNotificationFirebase($sentData);

                }//.foreach
            }
        }//.if ($this->request->is('post')) 
    }

    public function sentNotificationFirebase($Data){

        $param['topic'] = '/topics/'.$Data['username'].$Data['token'];
        $param['title'] = $Data['topic'];
        $param['message'] = $Data['shortDescription'];
        $param['icon'] = 'icon';
        $param['sound'] = 'sound';
        // $param['badge'] = $this->countBadge($Data);
        $param['badge'] = $Data['badge'];

        $api = 'http://connect05.pakgon.com/api/Notifications/push';
        $http = new Client();
        $data = [];
        $options = ['headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]];
        $response = $http->post($api, $param, $options)->body();
    }

 
    public function countBadge($Data){
        //     $Data['notificationType'] = 2;
        //     $Data['user_id'] = 59288;
        //    $Data['language_id'] = 1;
       
        $http = new Client();
        $api = 'http://connect06.pakgon.com/communication/api/getCountBadgeNotificationMessage.json';
        $options = ['headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',  
            ]
        ];
      
        $result = $http->post($api,$Data,$options)->body();
        return $result;  
    }

    
    
}