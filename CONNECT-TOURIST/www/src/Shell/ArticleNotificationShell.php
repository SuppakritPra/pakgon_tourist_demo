<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\ArticleNotificationComponent;


/**
 * ArticleNotification shell command.
 * @author sarawutt.b
 */
class ArticleNotificationShell extends Shell {

    public function initialize() {
        parent::initialize();
        $this->ArticleNotification = new ArticleNotificationComponent(new ComponentRegistry(), []);
    }
    /**
     * Does some routine installation tasks so people don't have to.
     *
     * @param \Composer\Script\Event $event The composer event object.
     * @throws \Exception Exception raised by validator.
     * @return void
     */
    public function main()
    {
        $Feeds = TableRegistry::get('Feeds');
        $feedsIsNoti = $Feeds->find('list',array(
            'conditions' => array(
                'is_noti'=>1
            ),
            'keyField' => 'id',
            'valueField' => 'id'
        ))->toArray();

        
        $trArticleNotifications = TableRegistry::get('ArticleNotifications');
        $ArticleNotifications = $trArticleNotifications->find('list',array(
            'conditions' => array(
                'article_id in'=> $feedsIsNoti
            ),
            'keyField' => 'article_id',
            'valueField' => 'article_id'
        ))->toArray();
        
        $result = array_diff($feedsIsNoti, $ArticleNotifications);
        
        if(!empty($result)){

            $trArticles = TableRegistry::get('Articles');
            $Articles = $trArticles->find('all',array(
                'conditions' => array(
                    'id in'=> $result
                )
            ));     

            foreach ($Articles as $Article){

                $trUsers = TableRegistry::get('users');
                $users = $trUsers->find('all')
                ->select([
                    'users.username',
                    'users.token',
                    //'cuss.page_id',
                ])				
                ->join([             
                    'cuss' => [
                        'table' => 'communtcation.user_subscribes',
                        'type' => 'inner',
                        'conditions' => [
                            'cuss.user_id = users.id',						
                        ],
                    ]
                ])
                ->where([
                    'cuss.page_id' => $Article->page_id
                ])
                ->toArray();

                foreach ($users as $user){
                    // $data=[];
                    // $data['username'] = $user['username'];
                    // $data['token'] = $user['token'];
                    // $data['article_title'] = $Article['article_title'];
                    // $data['article_intro'] = $Article['article_intro'];
                    // $data['article_id'] = $Article['id'];

                     $push = [
                        'title' => $Article['article_title'],
                        'message' => $Article['article_intro'],
                        'badge' => 1,
                        'topic' => '/topics/' . $user['username'].$user['token']
                    ];

                   // $customDatas = ['open_url' => 'http://commu-uat.connect.pakgon.com/feed/view/'.$Article['id']];

                   // $this->out(pr($push));
                    
                    $this->ArticleNotification->push($push);
                  //  $this->push($push, $customDatas);

                }  

                $ArticleNotifications = [];
                $ArticleNotifications['article_id'] = $Article->id;
                $ArticleNotifications['create_uid'] = 1;

               $newArticleNotifications = $trArticleNotifications->newEntity();
               $ArticleNotifications = $trArticleNotifications->patchEntity($newArticleNotifications, $ArticleNotifications);
                if($trArticleNotifications->save($ArticleNotifications)) {

               }
            }

        }
      
        //$this->out('Hello world.');
    }


   
   
}
