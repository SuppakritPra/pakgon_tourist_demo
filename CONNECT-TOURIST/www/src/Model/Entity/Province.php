<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Province Entity
 *
 * @property int $id
 * @property int $code
 * @property string $name
 * @property string $name_eng
 * @property int $region_id
 * @property string $status
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\District[] $districts
 * @property \App\Model\Entity\SubDistrict[] $sub_districts
 */
/**
 *
 * Province Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:20:31
 * @license Pakgon.Ltd
 */
class Province extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'name_eng' => true,
        'region_id' => true,
        'status' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'region' => true,
        'districts' => true,
        'sub_districts' => true
    ];
}
