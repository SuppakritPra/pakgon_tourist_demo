<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SysAcl Entity
 *
 * @property int $id
 * @property int $sys_controller_id
 * @property int $sys_action_id
 * @property int $user_id
 * @property int $role_id
 * @property string $status
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\SysController $sys_controller
 * @property \App\Model\Entity\SysAction $sys_action
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Role $role
 */
/**
 *
 * SysAcl Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:16:16
 * @license Pakgon.Ltd
 */
class SysAcl extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'sys_controller_id' => true,
        'sys_action_id' => true,
        'user_id' => true,
        'role_id' => true,
        'status' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'sys_controller' => true,
        'sys_action' => true,
        'user' => true,
        'role' => true
    ];
}
