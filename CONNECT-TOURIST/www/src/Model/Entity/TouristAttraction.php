<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TouristAttraction Entity
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property string $path_picture_tourist_attraction
 * @property int $province_id
 * @property int $review_tourist_id
 * @property string $detail_tourist
 * @property string $path_icon_marker
 * @property float $latitude
 * @property float $longtitude
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\MasterProvince $master_province
 * @property \App\Model\Entity\ReviewTourist $review_tourist
 */
class TouristAttraction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'category_id' => true,
        'path_picture_tourist_attraction' => true,
        'province_id' => true,
        'review_tourist_id' => true,
        'detail_tourist' => true,
        'path_icon_marker' => true,
        'latitude' => true,
        'longtitude' => true,
        'category' => true,
        'master_province' => true,
        'review_tourist' => true
    ];
}
