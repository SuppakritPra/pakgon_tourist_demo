<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActiveType Entity
 *
 * @property int $id
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $name_type
 *
 * @property \App\Model\Entity\UserActive[] $user_active
 */
class ActiveType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'name_type' => true,
        'user_active' => true
    ];
}
