<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReviewTourist Entity
 *
 * @property int $id
 * @property string $review
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\TouristAttraction[] $tourist_attraction
 */
class ReviewTourist extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'review' => true,
        'user_id' => true,
        'user' => true,
        'tourist_attraction' => true
    ];
}
