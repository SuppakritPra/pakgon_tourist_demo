<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PageAdmin Entity
 *
 * @property int $id
 * @property int $page_id
 * @property int $user_id
 * @property string $can_delete_other_admin
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Page $page
 * @property \App\Model\Entity\User $user
 */
class PageAdmin extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'page_id' => true,
        'user_id' => true,
        'can_delete_other_admin' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'page' => true,
        'user' => true
    ];
}
