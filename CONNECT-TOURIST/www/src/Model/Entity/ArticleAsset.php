<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticleAsset Entity
 *
 * @property int $id
 * @property string $article_asset_type
 * @property string $article_asset_name
 * @property string $path
 * @property int $article_id
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Article $article
 */
class ArticleAsset extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_asset_type' => true,
        'article_asset_name' => true,
        'path' => true,
        'article_id' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'article' => true
    ];
}
