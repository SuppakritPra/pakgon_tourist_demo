<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Feed Entity
 *
 * @property int $id
 * @property string $article_tittle
 * @property string $article_intro
 * @property string $article_detail
 * @property string $lang_code
 * @property \Cake\I18n\FrozenDate $publish_date
 * @property \Cake\I18n\FrozenDate $expire_date
 * @property int $count_like
 * @property string $is_noti
 * @property string $page_name
 */
class Feed extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_tittle' => true,
        'article_intro' => true,
        'article_detail' => true,
        'lang_code' => true,
        'publish_date' => true,
        'expire_date' => true,
        'count_like' => true,
        'is_noti' => true,
        'page_name' => true
    ];
}
