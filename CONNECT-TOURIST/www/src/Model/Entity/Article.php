<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property string $article_tittle
 * @property string $article_intro
 * @property string $article_detail
 * @property string $lang_code
 * @property \Cake\I18n\FrozenDate $publish_date
 * @property \Cake\I18n\FrozenDate $expire_date
 * @property string $feed_flag
 * @property string $tags
 * @property string $beacon_uid
 * @property string $qr_uid
 * @property int $count_like
 * @property int $page_id
 * @property string $is_noti
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Page $page
 * @property \App\Model\Entity\ArticleAsset[] $article_assets
 * @property \App\Model\Entity\UserActivite[] $user_activites
 */
class Article extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_title' => true,
        'article_intro' => true,
        'article_detail' => true,
        'lang_code' => true,
        'publish_date' => true,
        'expire_date' => true,
        'feed_flag' => true,
        'tags' => true,
        'beacon_uid' => true,
        'qr_uid' => true,
        'count_like' => true,
        'page_id' => true,
        'is_noti' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'page' => true,
        'article_assets' => true,
        'user_activites' => true
    ];
}
