<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserHome Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $address_type
 * @property string $is_home_registration
 * @property string $address
 * @property int $province_id
 * @property int $district_id
 * @property int $sub_district_id
 * @property string $zipcode
 * @property string $status
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Province $province
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\SubDistrict $sub_district
 */
/**
 *
 * UserHome Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:14:50
 * @license Pakgon.Ltd
 */
class UserHome extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'address_type' => true,
        'is_home_registration' => true,
        'address' => true,
        'province_id' => true,
        'district_id' => true,
        'sub_district_id' => true,
        'zipcode' => true,
        'status' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'province' => true,
        'district' => true,
        'sub_district' => true
    ];
}
