<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSubscribe Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $page_id
 * @property bool $is_subscribe_active
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Page $page
 */
class UserSubscribe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'page_id' => true,
        'is_subscribe_active' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'page' => true
    ];
}
