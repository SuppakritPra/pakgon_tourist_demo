<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SysController Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Menu[] $menus
 * @property \App\Model\Entity\SysAcl[] $sys_acls
 * @property \App\Model\Entity\SysAction[] $sys_actions
 */
/**
 *
 * SysController Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:13:04
 * @license Pakgon.Ltd
 */
class SysController extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'status' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'menus' => true,
        'sys_acls' => true,
        'sys_actions' => true
    ];
}
