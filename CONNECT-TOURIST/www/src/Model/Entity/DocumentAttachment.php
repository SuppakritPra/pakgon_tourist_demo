<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentAttachment Entity
 *
 * @property int $id
 * @property string $document_type
 * @property string $attachment_name
 * @property string $attachment_name_origin
 * @property string $display_name
 * @property string $attachment_extension
 * @property string $attachment_path
 * @property string $ref_model
 * @property int $ref_id
 * @property string $status
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Ref $ref
 */
/**
 *
 * DocumentAttachment Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:14:22
 * @license Pakgon.Ltd
 */
class DocumentAttachment extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'document_type' => true,
        'attachment_name' => true,
        'attachment_name_origin' => true,
        'display_name' => true,
        'attachment_extension' => true,
        'attachment_path' => true,
        'ref_model' => true,
        'ref_id' => true,
        'status' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'ref' => true
    ];
}
