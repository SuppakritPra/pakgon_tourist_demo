<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property string $page_name
 * @property string $page_status
 * @property string $page_type
 * @property int $citie_id
 * @property int $countrie_id
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\PageAdmin[] $page_admin
 * @property \App\Model\Entity\PageEntity[] $page_entities
 */
class Page extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'page_name' => true,
        'page_status' => true,
        'page_type' => true,
        'citie_id' => true,
        'countrie_id' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'city' => true,
        'country' => true,
        'articles' => true,
        'page_admin' => true,
        'page_entities' => true,
        'page_banner' => true,
        'page_desc' => true
    ];
}
