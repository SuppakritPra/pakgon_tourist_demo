<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterProvince Entity
 *
 * @property int $id
 * @property string $province_name_th
 * @property string $province_name_en
 * @property string $logo_of_province
 * @property int $region_id
 * @property float $center_lat
 * @property float $center_long
 *
 * @property \App\Model\Entity\Region $region
 */
class MasterProvince extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'province_name_th' => true,
        'province_name_en' => true,
        'logo_of_province' => true,
        'region_id' => true,
        'center_lat' => true,
        'center_long' => true,
        'region' => true,
        'banner_path' => true
    ];
}
