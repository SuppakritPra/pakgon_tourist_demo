<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TouristAttractionImage Entity
 *
 * @property int $id
 * @property string $path_image
 * @property int $tourist_attraction_id
 * @property int $type_image
 *
 * @property \App\Model\Entity\TouristAttraction $tourist_attraction
 */
class TouristAttractionImage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'path_image' => true,
        'tourist_attraction_id' => true,
        'type_image' => true,
        'tourist_attraction' => true
    ];
}
