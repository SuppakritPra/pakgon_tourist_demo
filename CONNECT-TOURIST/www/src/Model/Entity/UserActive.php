<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserActive Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $tourist_attraction_id
 * @property int $active_type_id
 * @property int $create_uid
 * @property int $update_uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $status_like
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\TouristAttraction $tourist_attraction
 * @property \App\Model\Entity\ActiveType $active_type
 */
class UserActive extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'tourist_attraction_id' => true,
        'active_type_id' => true,
        'create_uid' => true,
        'update_uid' => true,
        'created' => true,
        'modified' => true,
        'status_like' => true
    ];
}
