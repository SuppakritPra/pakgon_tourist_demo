<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserEntity Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $entity_code
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserEntity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'entity_code' => true,
        'create_uid' => true,
        'created' => true,
        'update_uid' => true,
        'modified' => true,
        'user' => true
    ];
}
