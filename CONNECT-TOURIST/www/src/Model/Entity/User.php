<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $point
 * @property bool $is_used
 * @property string $dynamic_key
 * @property \Cake\I18n\FrozenDate $dynamic_key_expiry
 * @property string $token
 * @property \Cake\I18n\FrozenDate $token_expiry
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $pin_code
 * @property string $pin_pass
 * @property bool $is_active
 *
 * @property \App\Model\Entity\ReviewTourist[] $review_tourist
 * @property \App\Model\Entity\UserActive[] $user_active
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'point' => true,
        'is_used' => true,
        'dynamic_key' => true,
        'dynamic_key_expiry' => true,
        'token' => true,
        'token_expiry' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'pin_code' => true,
        'pin_pass' => true,
        'is_active' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token'
    ];
}
