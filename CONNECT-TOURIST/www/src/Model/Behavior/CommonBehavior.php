<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of common
 *
 * @author sarawutt.b
 */

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Text;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;

class CommonBehavior extends Behavior {

    public $selectEmptyMsg = '---- please select ----';
    public $session = null;

    public function initialize(array $config) {
        $this->session = new Session();
    }

    /**
     *
     * Function load initial for needed model
     * @author  sarawutt.b
     * @param   type $tables as array of needed model list
     * @return  boolean true when finit process
     */
    protected function _loadInitialModel($tables = []) {
        if (!is_array($tables)) {
            $tables = array($tables);
        }
        foreach ($uses as $tatble) {
            $this->{$table} = TableRegistry::get($tatble);
        }
        return true;
    }

    /**
     * 
     * Function check for session and return the data in the session
     * @author  sarawutt.b
     * @param   $name as a string of session id
     * @return  mix string or array()
     */
    public function readSessionData($name = null) {
        return ($this->session->check($name)) ? $this->session->read($name) : null;
    }

    /**
     * Retrieve a single field value
     * 
     * @param  string $fieldName The name of the table field to retrieve.
     * @param  array $conditions An array of conditions for the find.
     * @return mixed The value of the specified field from the first row of the result set.
     */
    public function field($fieldName, array $conditions) {
        $field = $this->_table->alias() . '.' . $fieldName;
        $query = $this->_table->find()->select($field)->where($conditions);

        if ($query->isEmpty()) {
            return null;
        }
        return $query->first()->{$fieldName};
    }

    /**
     * 
     * Function get plain empty select
     * @author sarawutt.b
     * @return string empty select
     */
    public function getSelectEmptyMsg() {
        return $this->_getEmptySelect(false);
    }

    /**
     * Function get for empty option in DDL
     * @author sarawutt.b
     * @return array() of empty select DDL
     */
    public function getEmptySelect() {
        return $this->_getEmptySelect();
    }

    /**
     * 
     * Function get empty select get plain/array option empty
     * @author sarawutt.b
     * @param boolean $isArray 
     * @return mix
     */
    private function _getEmptySelect($isArray = true) {
        return ($isArray === true) ? ['' => __($this->selectEmptyMsg)] : __($this->selectEmptyMsg);
    }

    /**
     * 
     * Function used fro generate _VERSION_
     * @author  sarawutt.b
     * @return  biginteger of the version number
     */
    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    /**
     * 
     * Function used for generate UUID key patern
     * @author  sarawutt.b
     * @return  string uuid in version
     */
    function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    /**
     * 
     * Function get for current session user authentication full name
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user full name
     */
    protected function getAuthFullname() {
        return $this->readAuth('Auth.User.first_name') . ' ' . $this->readAuth('Auth.User.last_name');
    }

    /**
     * 
     * Function get for current session user authentication user id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user current session id
     */
    public function getAuthUserId() {
        return $this->session->read('Auth.User.id');
    }

    /**
     * 
     * Function get for auth role id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string at role id of auth role id
     */
    public function getAuthRoleId() {
        return $this->session->read('Auth.User.role_id');
    }

    /**
     * 
     * Function check is auth 
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  boolean true if successfully auth Otherwise return false
     */
    public function checkAuth() {
        return $this->session->check('Auth.User');
    }

    /**
     * 
     * Function get session i18n auth
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string
     */
    public function getCurrentLanguage() {
        return $this->session->read('SessionLanguage');
    }

    /**
     * 
     * Function get for current session with user authentication
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication session info
     */
    protected function readAuth($name = null) {
        return $this->session()->read($name);
    }

}
