<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocumentAttachments Model
 *
 * @property \App\Model\Table\RevesTable|\Cake\ORM\Association\BelongsTo $Reves
 *
 * @method \App\Model\Entity\DocumentAttachment get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentAttachment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentAttachment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentAttachment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentAttachment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentAttachment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentAttachment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * DocumentAttachments Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:14:22
 * @license Pakgon.Ltd
 */
class DocumentAttachmentsTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:14:22
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('system.document_attachments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->belongsTo('Reves', [
            'foreignKey' => 'ref_id'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:14:22
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('document_type')
                ->maxLength('document_type', 512)
                ->allowEmpty('document_type');

        $validator
                ->scalar('attachment_name')
                ->maxLength('attachment_name', 512)
                ->requirePresence('attachment_name', 'create')
                ->notEmpty('attachment_name');

        $validator
                ->scalar('attachment_name_origin')
                ->maxLength('attachment_name_origin', 512)
                ->requirePresence('attachment_name_origin', 'create')
                ->notEmpty('attachment_name_origin');

        $validator
                ->scalar('display_name')
                ->maxLength('display_name', 256)
                ->allowEmpty('display_name');

        $validator
                ->scalar('attachment_extension')
                ->maxLength('attachment_extension', 10)
                ->requirePresence('attachment_extension', 'create')
                ->notEmpty('attachment_extension');

        $validator
                ->scalar('attachment_path')
                ->maxLength('attachment_path', 512)
                ->requirePresence('attachment_path', 'create')
                ->notEmpty('attachment_path');

        $validator
                ->scalar('ref_model')
                ->maxLength('ref_model', 256)
                ->allowEmpty('ref_model');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->allowEmpty('create_uid');

        $validator
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/04/20 18:14:22
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['ref_id'], 'Reves'));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:14:22
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'system';
    }

    /**
     *
     * Function findDocumentAttachmentsList find for DocumentAttachments in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:14:22
     * @license Pakgon.Ltd
     */
    public function findDocumentAttachmentsList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
