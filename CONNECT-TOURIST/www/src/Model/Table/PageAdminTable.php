<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PageAdmin Model
 *
 * @property \App\Model\Table\PagesTable|\Cake\ORM\Association\BelongsTo $Pages
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\PageAdmin get($primaryKey, $options = [])
 * @method \App\Model\Entity\PageAdmin newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PageAdmin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PageAdmin|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PageAdmin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PageAdmin[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PageAdmin findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PageAdminTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('page_admin');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pages', [
            'foreignKey' => 'page_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('can_delete_other_admin')
                ->allowEmpty('can_delete_other_admin');

        $validator
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->requirePresence('update_uid', 'create')
                ->notEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['page_id'], 'Pages'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * 
     * Function list all page of the admin
     * @author sarawutt.b
     * @param type $userId as a integer of user admin id
     * @param type $pageIds as a list all of page id
     * @return type
     */
    public function findPageAdmins($userId = 0, $pageIds = []) {
        $options = [
            'conditions' => [
                'page_id in ' => $pageIds,
                'user_id' => $userId
            ],
            'keyField' => 'page_id',
            'valueField' => 'page_id'
        ];

        return $this->findPageAdminsList($options);
    }

    /**
     *
     * Function findPageAdminsList find for Roles in select list
     *
     * @author sarawutt.b
     * @param   array() find options
     * @param   boolean of has contain empty select
     * @return  array with empty select
     * @license Pakgon.Ltd
     */
    public function findPageAdminsList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'page_id', 'valueField' => 'page_id', 'order' => 'page_id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }

}
