<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TouristAttractionImage Model
 *
 * @property \App\Model\Table\TouristAttractionTable|\Cake\ORM\Association\BelongsTo $TouristAttraction
 *
 * @method \App\Model\Entity\TouristAttractionImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\TouristAttractionImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TouristAttractionImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TouristAttractionImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TouristAttractionImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TouristAttractionImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TouristAttractionImage findOrCreate($search, callable $callback = null, $options = [])
 */
class TouristAttractionImageTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tourist_attraction_image');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TouristAttraction', [
            'foreignKey' => 'tourist_attraction_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('path_image')
            ->maxLength('path_image', 255)
            ->allowEmpty('path_image');

        $validator
            ->integer('type_image')
            ->allowEmpty('type_image');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tourist_attraction_id'], 'TouristAttraction'));

        return $rules;
    }
    public static function defaultConnectionName() {
        return 'db_torist';
    }
    public function findIdTourist($touristId = 0) {
        // $this->loadModel('TouristAttractionImage');
        // $TouristAttraction_image = $this->TouristAttraction->find('all')
        // ->select(['TouristAttraction.id','TouristAttraction.name','TouristAttractionImage.path_image'])
        // ->where(['TouristAttractionImage' => ''])
        // // tourist_attraction_id
        // ->join([ 
        //     'TouristAttractionImage' => [
        //                 'table' => 'master.tourist_attraction_image',
        //                 'type' => 'left',
        //                 'conditions' => [
        //                     'TouristAttractionImage.tourist_attraction_id = TouristAttraction.id',
        //                     'TouristAttractionImage.type_image' => '2'       
        //                 ],
        //             ]
        //         ])
        // ->toArray();

        $options = [
            'table' => 'master.tourist_attraction_image',
            'type' => 'left',
            'conditions' => [
                        'TouristAttractionImage.tourist_attraction_id' => $touristId,
                        'TouristAttractionImage.type_image' => '2'       
                    ],
            'keyField' => 'id',
            'valueField' => 'path_image'
        ];
        return $this->find('list', $options)->toArray();
        // return $this->findIdImage($options);
    }
    // public function findIdImage($paramsOptions = [], $hasEmptySelect = false) {
    //     $selfOptions = ['keyField' => 'id', 'valueField' => 'path_image', 'order' => 'id'];
    //     $options = array_merge($selfOptions, $paramsOptions);
    //     return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();

    // public function Attraction($id=null){
    //     $this->loadModel('TouristAttractionImage');
    //     $this->loadModel('TouristAttraction');
    //     $TouristAttraction_image = $this->TouristAttraction->find('all')
    //     ->select(['TouristAttraction.id','TouristAttraction.name','TouristAttractionImage.path_image'])
    //     ->join([ 
    //         'TouristAttractionImage' => [
    //                     'table' => 'master.tourist_attraction_image',
    //                     'type' => 'left',
    //                     'conditions' => [
    //                         'TouristAttractionImage.tourist_attraction_id = TouristAttraction.id',
    //                         'TouristAttractionImage.type_image' => '2'       
    //                     ],
    //                 ]
    //             ])
    //     ->where(['tourist_attraction_id' => $id ])
    //     ->toArray();
    //     pr($TouristAttraction_image);die;
    //     return $TouristAttraction_image;
	
    // }
}
