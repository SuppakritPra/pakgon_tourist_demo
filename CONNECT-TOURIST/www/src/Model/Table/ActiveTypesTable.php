<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActiveTypes Model
 *
 * @property \App\Model\Table\UserActiveTable|\Cake\ORM\Association\HasMany $UserActive
 *
 * @method \App\Model\Entity\ActiveType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActiveType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActiveType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActiveType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActiveType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActiveType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActiveType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActiveTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('active_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('UserActive', [
            'foreignKey' => 'active_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('create_uid', 'create')
            ->notEmpty('create_uid');

        $validator
            ->allowEmpty('update_uid');

        $validator
            ->scalar('name_type')
            ->maxLength('name_type', 255)
            ->allowEmpty('name_type');

        return $validator;
    }
    public static function defaultConnectionName() {
        return 'db_torist';
    }
}
