<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterLanguages Model
 *
 * @property |\Cake\ORM\Association\HasMany $ArticleQuestionChoiceTranslations
 * @property |\Cake\ORM\Association\HasMany $ArticleQuestionTranslations
 * @property |\Cake\ORM\Association\HasMany $ArticleTranslations
 * @property |\Cake\ORM\Association\HasMany $MasterBusinessTypes
 * @property |\Cake\ORM\Association\HasMany $MasterOrganizationPositions
 * @property \App\Model\Table\UserDefaultLanguagesTable|\Cake\ORM\Association\HasMany $UserDefaultLanguages
 *
 * @method \App\Model\Entity\MasterLanguage get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterLanguage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterLanguage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterLanguage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterLanguage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterLanguage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterLanguage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MasterLanguagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_languages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('ArticleQuestionChoiceTranslations', [
            'foreignKey' => 'master_language_id'
        ]);
        $this->hasMany('ArticleQuestionTranslations', [
            'foreignKey' => 'master_language_id'
        ]);
        $this->hasMany('ArticleTranslations', [
            'foreignKey' => 'master_language_id'
        ]);
        $this->hasMany('MasterBusinessTypes', [
            'foreignKey' => 'master_language_id'
        ]);
        $this->hasMany('MasterOrganizationPositions', [
            'foreignKey' => 'master_language_id'
        ]);
        $this->hasMany('UserDefaultLanguages', [
            'foreignKey' => 'master_language_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('language_display')
            ->requirePresence('language_display', 'create')
            ->notEmpty('language_display');

        $validator
            ->scalar('language_th')
            ->allowEmpty('language_th');

        $validator
            ->scalar('language_en')
            ->allowEmpty('language_en');

        $validator
            ->scalar('language_abbr')
            ->requirePresence('language_abbr', 'create')
            ->notEmpty('language_abbr');

        $validator
            ->requirePresence('seq_no', 'create')
            ->notEmpty('seq_no');

        $validator
            ->boolean('is_used')
            ->requirePresence('is_used', 'create')
            ->notEmpty('is_used');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    
    public function findMasterLanguagesList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'language_display', 'conditions' => ['is_used' => true]];
        // $options = array_merge($selfOptions, $paramsOptions);
        return $this->find('list', $selfOptions)->toArray();
    }

    public static function defaultConnectionName()
    {
        return 'master';
    }
}
