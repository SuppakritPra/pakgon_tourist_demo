<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReviewTourist Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TouristAttractionTable|\Cake\ORM\Association\HasMany $TouristAttraction
 *
 * @method \App\Model\Entity\ReviewTourist get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReviewTourist newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReviewTourist[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReviewTourist|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReviewTourist patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReviewTourist[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReviewTourist findOrCreate($search, callable $callback = null, $options = [])
 */
class ReviewTouristTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('review_tourist');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [ // to use User_torist
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TouristAttraction', [
            'foreignKey' => 'review_tourist_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('review')
            ->maxLength('review', 255)
            ->allowEmpty('review');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public static function defaultConnectionName() {
        return 'db_torist';
    }
    public function findUserReview($userId = 0,$id = 0) {
        $options = [
            'conditions' => [
                'user_id' => $userId,
                'tourist_attraction_id' => $id
            ],
            'keyField' => 'tourist_attraction_id',
            'valueField' => 'id'
        ];
        return $this->findUserReviewList($options);
    }
    public function findUserReviewList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'tourist_attraction_id', 'valueField' => 'id', 'order' => 'tourist_attraction_id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }
}
