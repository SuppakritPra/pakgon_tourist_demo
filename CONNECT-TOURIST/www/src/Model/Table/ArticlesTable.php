<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \App\Model\Table\PagesTable|\Cake\ORM\Association\BelongsTo $Pages
 * @property \App\Model\Table\ArticleAssetsTable|\Cake\ORM\Association\HasMany $ArticleAssets
 * @property \App\Model\Table\UserActivitesTable|\Cake\ORM\Association\HasMany $UserActivites
 *
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pages', [
            'foreignKey' => 'page_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ArticleAssets', [
            'foreignKey' => 'article_id'
        ]);
        $this->hasMany('UserActivites', [
            'foreignKey' => 'article_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //     ->allowEmpty('id', 'create');
        // $validator
        //     ->scalar('article_tittle')
        //     ->requirePresence('article_tittle', 'create')
        //     ->notEmpty('article_tittle');
        // $validator
        //     ->scalar('article_intro')
        //     ->allowEmpty('article_intro');
        // $validator
        //     ->scalar('article_detail')
        //     ->requirePresence('article_detail', 'create')
        //     ->notEmpty('article_detail');
        // $validator
        //     ->scalar('lang_code')
        //     ->requirePresence('lang_code', 'create')
        //     ->notEmpty('lang_code');
        // $validator
        //     ->date('publish_date')
        //     ->requirePresence('publish_date', 'create')
        //     ->notEmpty('publish_date');
        // $validator
        //     ->date('expire_date')
        //     ->requirePresence('expire_date', 'create')
        //     ->notEmpty('expire_date');
        // $validator
        //     ->scalar('feed_flag')
        //     ->allowEmpty('feed_flag');
        // $validator
        //     ->scalar('tags')
        //     ->allowEmpty('tags');
        // $validator
        //     ->scalar('beacon_uid')
        //     ->allowEmpty('beacon_uid');
        // $validator
        //     ->scalar('qr_uid')
        //     ->allowEmpty('qr_uid');
        // $validator
        //     ->integer('count_like')
        //     ->allowEmpty('count_like');
        // $validator
        //     ->scalar('is_noti')
        //     ->allowEmpty('is_noti');
        // $validator
        //     ->requirePresence('create_uid', 'create')
        //     ->notEmpty('create_uid');
        // $validator
        //     ->requirePresence('update_uid', 'create')
        //     ->notEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['page_id'], 'Pages'));

        return $rules;
    }

}
