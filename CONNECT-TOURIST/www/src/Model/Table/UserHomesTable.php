<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserHomes Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ProvincesTable|\Cake\ORM\Association\BelongsTo $Provinces
 * @property \App\Model\Table\DistrictsTable|\Cake\ORM\Association\BelongsTo $Districts
 * @property \App\Model\Table\SubDistrictsTable|\Cake\ORM\Association\BelongsTo $SubDistricts
 *
 * @method \App\Model\Entity\UserHome get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserHome newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserHome[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserHome|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserHome patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserHome[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserHome findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * UserHomes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:14:50
 * @license Pakgon.Ltd
 */
class UserHomesTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:14:50
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('system.user_homes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Provinces', [
            'foreignKey' => 'province_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SubDistricts', [
            'foreignKey' => 'sub_district_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:14:50
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('address_type')
                ->maxLength('address_type', 50)
                ->requirePresence('address_type', 'create')
                ->notEmpty('address_type');

        $validator
                ->scalar('is_home_registration')
                ->maxLength('is_home_registration', 1)
                ->requirePresence('is_home_registration', 'create')
                ->notEmpty('is_home_registration');

        $validator
                ->scalar('address')
                ->maxLength('address', 1024)
                ->requirePresence('address', 'create')
                ->notEmpty('address');

        $validator
                ->scalar('zipcode')
                ->maxLength('zipcode', 10)
                ->requirePresence('zipcode', 'create')
                ->notEmpty('zipcode');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->allowEmpty('create_uid');

        $validator
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/04/20 18:14:50
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['province_id'], 'Provinces'));
        $rules->add($rules->existsIn(['district_id'], 'Districts'));
        $rules->add($rules->existsIn(['sub_district_id'], 'SubDistricts'));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:14:50
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'system';
    }

    /**
     *
     * Function findUserHomesList find for UserHomes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:14:50
     * @license Pakgon.Ltd
     */
    public function findUserHomesList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
