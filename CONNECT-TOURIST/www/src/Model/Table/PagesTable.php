<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\ArticlesTable|\Cake\ORM\Association\HasMany $Articles
 * @property \App\Model\Table\PageAdminTable|\Cake\ORM\Association\HasMany $PageAdmin
 * @property \App\Model\Table\PageEntitiesTable|\Cake\ORM\Association\HasMany $PageEntities
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PagesTable extends Table {

    protected $_PUBLIC_PAGE = 0;
    protected $_PRIVATE_PAGE = 1;
    protected $_CHILD_OF_PRIVATE = 2;
    protected $_CONNECT_PAGE = 9;
    //PAGE or BLOCK category
    protected $_CATE_LOCATION_BASED = 1;
    protected $_CATE_HEALTHY = 7;
    protected $_CATE_HOT_DEAL = 2;

    protected $_CONNECT_PAGE_ID = 7;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('pages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cities', [
            'foreignKey' => 'citie_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'countrie_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        // $this->hasMany('Articles', [
        //     'foreignKey' => 'page_id'
        // ]);
        // $this->hasMany('PageAdmin', [
        //     'foreignKey' => 'page_id'
        // ]);
        // $this->hasMany('PageEntities', [
        //     'foreignKey' => 'page_id'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //     ->allowEmpty('id', 'create');
        // $validator
        //     ->scalar('page_name')
        //     ->requirePresence('page_name', 'create')
        //     ->notEmpty('page_name');
        // $validator
        //     ->scalar('page_status')
        //     ->allowEmpty('page_status');
        // $validator
        //     ->scalar('page_type')
        //     ->allowEmpty('page_type');
        // $validator
        //     ->requirePresence('create_uid', 'create')
        //     ->notEmpty('create_uid');
        // $validator
        //     ->requirePresence('update_uid', 'create')
        //     ->notEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        //$rules->add($rules->existsIn(['citie_id'], 'Cities'));
        //$rules->add($rules->existsIn(['countrie_id'], 'Countries'));

        return $rules;
    }

    public static function defaultConnectionName() {
        return 'default';
    }

    /**
     * 
     * Function find page with healthy
     * @author sarawutt.b
     * @param type $currentCityId
     * @return type
     */
    public function findHealthyPages($currentCityId = 0) {
        return $this->find()->where(['page_status' => 1, 'page_type' => 0, 'category_id' => $this->_CATE_HEALTHY])->order(['display_order' => 'asc'])->all();
    }

    /**
     * 
     * Function find page with current location with Geolocation
     * @author sarawutt.b
     * @param type $currentCityId
     * @return type
     */
    public function findGeolocationPages($currentCityId = 0) {
        return $this->find()->where(['citie_id' => $currentCityId, 'page_status' => 1, 'page_type' => 0, 'category_id' => $this->_CATE_LOCATION_BASED])->limit(4)->order(['display_order' => 'asc'])->all();
    }

    /**
     * 
     * Function find page hot Deals with current location with Geolocation
     * @author sarawutt.b
     * @param type $currentCityId
     * @return type
     */
    public function findHotDealsPages($currentCityId = 0, $isFindList = false) {
        if ($isFindList === true) {
            return $this->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['citie_id' => $currentCityId, 'page_status' => 1, 'page_type' => 0, 'category_id' => $this->_CATE_HOT_DEAL])->limit(10)->order(['display_order' => 'asc'])->toArray();
        } else {
            return $this->find()->where(['citie_id' => $currentCityId, 'page_status' => 1, 'page_type' => 0, 'category_id' => $this->_CATE_HOT_DEAL])->limit(10)->order(['display_order' => 'asc'])->all();
        }
    }

    /**
     * 
     * Function list all page of the public page list
     * @author sarawutt.b
     * @param type $userProfile as a object user profile object
     * @return type
     */
    public function findPublicPageList($userProfile = null) {
        $options = [
            'conditions' => [
                'page_status' => 1,
                'page_type' => $this->_PUBLIC_PAGE,
                'countrie_id' => $userProfile->countrie_id,
                'citie_id' => $userProfile->citie_id
            ],
            'keyField' => 'id',
            'valueField' => 'id'
        ];
        return $this->findPagesList($options);
    }

    /**
     * 
     * Function list all page of the PAKGON connect
     * @author sarawutt.b
     * @param type $userProfile as a object user profile object
     * @return type
     */
    public function findConnectPageList($userProfile = null) {
        $options = [
            'conditions' => [
                'page_status' => 1,
                'page_type' => $this->_CONNECT_PAGE,
                'countrie_id' => $userProfile->countrie_id,
                'citie_id' => 0
            ],
            'keyField' => 'id',
            'valueField' => 'id'
        ];
        return $this->findPagesList($options);
    }

    /**
     *
     * Function findPagesList find for Roles in select list
     *
     * @author sarawutt.b
     * @param   array() find options
     * @param   boolean of has contain empty select
     * @return  array with empty select
     * @license Pakgon.Ltd
     */
    public function findPagesList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'pae_name', 'valueField' => 'id', 'order' => 'page_name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }

}
