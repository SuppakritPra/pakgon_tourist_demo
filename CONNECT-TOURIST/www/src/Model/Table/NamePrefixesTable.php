<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NamePrefixes Model
 *
 * @method \App\Model\Entity\NamePrefix get($primaryKey, $options = [])
 * @method \App\Model\Entity\NamePrefix newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NamePrefix[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NamePrefix|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NamePrefix patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NamePrefix[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NamePrefix findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * NamePrefixes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:18:11
 * @license Pakgon.Ltd
 */
class NamePrefixesTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:18:11
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('master.name_prefixes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:18:11
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->scalar('name_eng')
                ->maxLength('name_eng', 100)
                ->allowEmpty('name_eng');

        $validator
                ->scalar('long_name')
                ->maxLength('long_name', 512)
                ->requirePresence('long_name', 'create')
                ->notEmpty('long_name');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->integer('create_uid')
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->integer('update_uid')
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:18:11
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'master';
    }

    /**
     *
     * Function findNamePrefixesList find for NamePrefixes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:18:11
     * @license Pakgon.Ltd
     */
    public function findNamePrefixesList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'name', 'order' => ' name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
