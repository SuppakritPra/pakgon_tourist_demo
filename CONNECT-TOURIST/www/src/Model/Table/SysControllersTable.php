<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SysControllers Model
 *
 * @property \App\Model\Table\MenusTable|\Cake\ORM\Association\HasMany $Menus
 * @property \App\Model\Table\SysAclsTable|\Cake\ORM\Association\HasMany $SysAcls
 * @property \App\Model\Table\SysActionsTable|\Cake\ORM\Association\HasMany $SysActions
 *
 * @method \App\Model\Entity\SysController get($primaryKey, $options = [])
 * @method \App\Model\Entity\SysController newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SysController[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SysController|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SysController patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SysController[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SysController findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * SysControllers Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:13:04
 * @license Pakgon.Ltd
 */
class SysControllersTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('system.sys_controllers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->hasMany('Menus', [
            'foreignKey' => 'sys_controller_id'
        ]);
        $this->hasMany('SysAcls', [
            'foreignKey' => 'sys_controller_id'
        ]);
        $this->hasMany('SysActions', [
            'foreignKey' => 'sys_controller_id'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 256)
                ->requirePresence('name', 'create')
                ->notEmpty('name')
                ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->scalar('description')
                ->maxLength('description', 512)
                ->allowEmpty('description');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->integer('create_uid')
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->integer('update_uid')
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'system';
    }

    /**
     *
     * Function findSysControllersList find for SysControllers in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:13:04
     * @license Pakgon.Ltd
     */
    public function findSysControllersList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'name', 'order' => ' name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
