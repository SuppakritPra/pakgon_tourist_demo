<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ethnicities Model
 *
 * @method \App\Model\Entity\Ethnicity get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ethnicity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ethnicity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ethnicity|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ethnicity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ethnicity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ethnicity findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * Ethnicities Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:19:28
 * @license Pakgon.Ltd
 */
class EthnicitiesTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('master.ethnicities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 512)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->scalar('name_eng')
                ->maxLength('name_eng', 512)
                ->requirePresence('name_eng', 'create')
                ->notEmpty('name_eng');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->integer('create_uid')
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->integer('update_uid')
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'master';
    }

    /**
     *
     * Function findEthnicitiesList find for Ethnicities in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function findEthnicitiesList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'name', 'order' => ' name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
