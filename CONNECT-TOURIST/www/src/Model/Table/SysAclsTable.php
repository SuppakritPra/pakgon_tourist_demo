<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SysAcls Model
 *
 * @property \App\Model\Table\SysControllersTable|\Cake\ORM\Association\BelongsTo $SysControllers
 * @property \App\Model\Table\SysActionsTable|\Cake\ORM\Association\BelongsTo $SysActions
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\SysAcl get($primaryKey, $options = [])
 * @method \App\Model\Entity\SysAcl newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SysAcl[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SysAcl|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SysAcl patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SysAcl[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SysAcl findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * SysAcls Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:16:16
 * @license Pakgon.Ltd
 */
class SysAclsTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('system.sys_acls');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->belongsTo('SysControllers', [
            'foreignKey' => 'sys_controller_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SysActions', [
            'foreignKey' => 'sys_action_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->integer('create_uid')
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->integer('update_uid')
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['sys_controller_id'], 'SysControllers'));
        $rules->add($rules->existsIn(['sys_action_id'], 'SysActions'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'system';
    }

    /**
     *
     * Function findSysAclsList find for SysAcls in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:16:16
     * @license Pakgon.Ltd
     */
    public function findSysAclsList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
