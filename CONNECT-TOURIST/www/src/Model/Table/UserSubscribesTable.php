<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserSubscribes Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PagesTable|\Cake\ORM\Association\BelongsTo $Pages
 *
 * @method \App\Model\Entity\UserSubscribe get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserSubscribe newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserSubscribe[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscribe|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSubscribe patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscribe[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscribe findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserSubscribesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_subscribes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        // $this->belongsTo('Users', [
        //     'foreignKey' => 'user_id',
        //     'joinType' => 'INNER'
        // ]);
        // $this->belongsTo('Pages', [
        //     'foreignKey' => 'page_id',
        //     'joinType' => 'INNER'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //     ->allowEmpty('id', 'create');
        // $validator
        //     ->boolean('is_subscribe_active')
        //     ->allowEmpty('is_subscribe_active');
        // $validator
        //     ->requirePresence('create_uid', 'create')
        //     ->notEmpty('create_uid');
        // $validator
        //     ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        // $rules->add($rules->existsIn(['user_id'], 'Users'));
        // $rules->add($rules->existsIn(['page_id'], 'Pages'));

        return $rules;
    }

    /**
     *
     * Function findUserSubscribesList find for Roles in select list
     *
     * @author sarawutt.b
     * @param   array() find options
     * @param   boolean of has contain empty select
     * @return  array with empty select
     * @license Pakgon.Ltd
     */
    public function findUserSubscribesList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => 'id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }

    /**
     * 
     * Function find subscribe page by user id
     * @author sarawutt.b
     */
    public function findUserSubscribeFeed() {
        $userId = $this->getAuthUserId();
        $tmp = $this->find('list', ['keyField' => 'id', 'valueField' => 'page_id'])->where(['is_subscribe_active' => true, 'user_id' => $userId])->order(['created' => 'desc']);
        return ($tmp->isEmpty()) ? [] : $tmp->all()->toArray();
    }

}
