<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 * @property \App\Model\Table\SysAclsTable|\Cake\ORM\Association\HasMany $SysAcls
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Role get($primaryKey, $options = [])
 * @method \App\Model\Entity\Role newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Role[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Role|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Role patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Role[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Role findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * Roles Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:12:11
 * @license Pakgon.Ltd
 */
class RolesTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:12:11
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('system.roles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->hasMany('SysAcls', [
            'foreignKey' => 'role_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'role_id'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:12:11
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 256)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->scalar('name_eng')
                ->maxLength('name_eng', 256)
                ->requirePresence('name_eng', 'create')
                ->notEmpty('name_eng');

        $validator
                ->scalar('description')
                ->maxLength('description', 512)
                ->allowEmpty('description');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->integer('create_uid')
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->integer('update_uid')
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:12:11
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'system';
    }

    /**
     *
     * Function findRolesList find for Roles in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:12:11
     * @license Pakgon.Ltd
     */
    public function findRolesList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'name', 'order' => ' name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

    /**
     * 
     * Find Role information by role ID
     * @author  sarawutt.b
     * @param   type $id as integer of role id
     * @return  array()
     */
    public function getRoleNameById($id = null) {
        $tmp = $this->findById($id)->select(['name'])->first();
        return empty($tmp) ? [] : $tmp->toArray()['name'];
    }

}
