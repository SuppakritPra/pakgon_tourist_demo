<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Feeds Model
 *
 * @method \App\Model\Entity\Feed get($primaryKey, $options = [])
 * @method \App\Model\Entity\Feed newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Feed[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Feed|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Feed patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Feed[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Feed findOrCreate($search, callable $callback = null, $options = [])
 */
class FeedsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('feeds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Common');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //     ->allowEmpty('id', 'create');
        // $validator
        //     ->scalar('article_tittle')
        //     ->requirePresence('article_tittle', 'create')
        //     ->notEmpty('article_tittle');
        // $validator
        //     ->scalar('article_intro')
        //     ->allowEmpty('article_intro');
        // $validator
        //     ->scalar('article_detail')
        //     ->requirePresence('article_detail', 'create')
        //     ->notEmpty('article_detail');
        // $validator
        //     ->scalar('lang_code')
        //     ->requirePresence('lang_code', 'create')
        //     ->notEmpty('lang_code');
        // $validator
        //     ->date('publish_date')
        //     ->requirePresence('publish_date', 'create')
        //     ->notEmpty('publish_date');
        // $validator
        //     ->date('expire_date')
        //     ->requirePresence('expire_date', 'create')
        //     ->notEmpty('expire_date');
        // $validator
        //     ->integer('count_like')
        //     ->allowEmpty('count_like');
        // $validator
        //     ->scalar('is_noti')
        //     ->allowEmpty('is_noti');

        return $validator;
    }

    public function findfeeds($timeStudyAttendant = []) {
        $conn = ConnectionManager::get('default');
        $sql = "SELECT * FROM communtcation.feeds";
        $result = $conn->execute($sql);
        return $result->fetchAll('assoc');
    }

    /**
     * 
     * Function reed all feed by user profile
     * @author sarawutt.b
     * @param  array list of $pageIds
     * @return array
     */
    public function profileFeeds($pageIds = null) {
        if (is_null($pageIds) || !is_array($pageIds) || empty($pageIds)) {
            return [];
        }

        $results = $this->find()
                ->where(['page_id in' => $pageIds])
                ->order(['publish_date' => 'DESC']);
        return $results->isEmpty() ? [] : $results->all()->toArray();
    }

    /**
     * 
     * Function find for Hot Deal
     * @author sarawutt.b
     * @param type $pageID
     * @return type
     */
    public function findHotDealCardByPageId($pageID = 0) {
        return $this->loadFeeds($pageID);
    }

    /**
     * 
     * Function reed all feed of the PAKGON  Connect
     * @author sarawutt.b
     * @param  array list of $pageIds
     * @return array
     */
    public function loadFeeds($pageIds = null, $paramsOptions = null, $parmasConditions = null) {
        if (is_null($pageIds) || !is_array($pageIds) || empty($pageIds)) {
            return [];
        }

        $conditions = ['page_id in' => $pageIds];
        $options = ['limit' => null];
        if (is_array($paramsOptions) && !empty($paramsOptions)) {
            $options = array_merge($options, $paramsOptions);
        }
        
        if (is_array($parmasConditions) && !empty($parmasConditions)) {
            $conditions = array_merge($conditions, $parmasConditions);
        }
        
        $results = $this->find()
                ->where($conditions)
                ->limit($options['limit'])
                ->order(['publish_date' => 'DESC']);
        return $results->isEmpty() ? [] : $results->all()->toArray();
    }

}
