<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PageEntities Model
 *
 * @property \App\Model\Table\PagesTable|\Cake\ORM\Association\BelongsTo $Pages
 *
 * @method \App\Model\Entity\PageEntity get($primaryKey, $options = [])
 * @method \App\Model\Entity\PageEntity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PageEntity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PageEntity|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PageEntity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PageEntity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PageEntity findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PageEntitiesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('page_entities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->belongsTo('Pages', [
        //     'foreignKey' => 'page_id',
        //     'joinType' => 'INNER'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //     ->allowEmpty('id', 'create');
        // $validator
        //     ->scalar('entity_code')
        //     ->requirePresence('entity_code', 'create')
        //     ->notEmpty('entity_code');
        // $validator
        //     ->requirePresence('create_uid', 'create')
        //     ->notEmpty('create_uid');
        // $validator
        //     ->requirePresence('update_uid', 'create')
        //     ->notEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        // $rules->add($rules->existsIn(['page_id'], 'Pages'));

        return $rules;
    }

    /**
     *
     * Function findUserSubscribesList find for Roles in select list
     *
     * @author sarawutt.b
     * @param   array() find options
     * @param   boolean of has contain empty select
     * @return  array with empty select
     * @license Pakgon.Ltd
     */
    public function findPageEntitiesList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'page_id', 'valueField' => 'page_id', 'order' => 'page_id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }

}
