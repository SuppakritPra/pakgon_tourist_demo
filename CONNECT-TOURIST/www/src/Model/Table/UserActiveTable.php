<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserActive Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TouristAttractionTable|\Cake\ORM\Association\BelongsTo $TouristAttraction
 * @property \App\Model\Table\ActiveTypesTable|\Cake\ORM\Association\BelongsTo $ActiveTypes
 *
 * @method \App\Model\Entity\UserActive get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserActive newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserActive[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserActive|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserActive patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserActive[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserActive findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserActiveTable extends Table
{
    protected $_LIKE = 'y';
    protected $_STAR = '2';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_active');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TouristAttraction', [
            'foreignKey' => 'tourist_attraction_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ActiveTypes', [
            'foreignKey' => 'active_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('create_uid', 'create')
            ->notEmpty('create_uid');

        $validator
            ->allowEmpty('update_uid');

        $validator
            ->scalar('status_like')
            ->maxLength('status_like', 1)
            ->allowEmpty('status_like');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['tourist_attraction_id'], 'TouristAttraction'));
        $rules->add($rules->existsIn(['active_type_id'], 'ActiveTypes'));

        return $rules;
    }
    public static function defaultConnectionName() {
        return 'db_torist';
    }
    public function findLike($userId = 0,$id = 0) {
        $options = [
            'conditions' => [
                'status_like' => $this->_LIKE,
                'user_id' => $userId,
                'tourist_attraction_id' => $id
            ],
            'keyField' => 'tourist_attraction_id',
            'valueField' => 'id'
        ];
        return $this->findUserActiveList($options);
    }
    public function findUserActiveList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'tourist_attraction_id', 'valueField' => 'id', 'order' => 'tourist_attraction_id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }
}
