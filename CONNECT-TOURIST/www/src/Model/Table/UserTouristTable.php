<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserTourist Model
 *
 * @method \App\Model\Entity\UserTourist get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserTourist newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserTourist[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserTourist|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserTourist patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserTourist[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserTourist findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserTouristTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_tourist');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->belongsTo('MasterProvince', [
            'foreignKey' => 'province_id'
        ]);
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 100)
            ->allowEmpty('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 100)
            ->allowEmpty('password');

        $validator
            ->allowEmpty('point');

        $validator
            ->boolean('is_used')
            ->allowEmpty('is_used');

        $validator
            ->scalar('dynamic_key')
            ->maxLength('dynamic_key', 50)
            ->allowEmpty('dynamic_key');

        $validator
            ->date('dynamic_key_expiry')
            ->allowEmpty('dynamic_key_expiry');

        $validator
            ->scalar('token')
            ->maxLength('token', 50)
            ->allowEmpty('token');

        $validator
            ->date('token_expiry')
            ->allowEmpty('token_expiry');

        $validator
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->allowEmpty('modified_by');

        $validator
            ->scalar('pin_code')
            ->maxLength('pin_code', 4)
            ->allowEmpty('pin_code');

        $validator
            ->scalar('pin_pass')
            ->maxLength('pin_pass', 9)
            ->allowEmpty('pin_pass');

        $validator
            ->boolean('is_active')
            ->allowEmpty('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }
    public static function defaultConnectionName() {
        return 'db_torist';
    }
}
