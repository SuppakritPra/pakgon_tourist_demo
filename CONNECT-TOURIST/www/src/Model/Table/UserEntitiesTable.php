<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserEntities Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserEntity get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserEntity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserEntity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserEntity|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserEntity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserEntity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserEntity findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserEntitiesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_entities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('entity_code')
                ->maxLength('entity_code', 255)
                ->allowEmpty('entity_code');

        $validator
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->allowEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     *
     * Function findUserEntitiesList find for Roles in select list
     *
     * @author sarawutt.b
     * @param   array() find options
     * @param   boolean of has contain empty select
     * @return  array with empty select
     * @license Pakgon.Ltd
     */
    public function findUserEntitiesList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'entity_code', 'valueField' => 'entity_code', 'order' => 'entity_code'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }

}
