<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Menus Model
 *
 * @property \App\Model\Table\SysControllersTable|\Cake\ORM\Association\BelongsTo $SysControllers
 * @property \App\Model\Table\SysActionsTable|\Cake\ORM\Association\BelongsTo $SysActions
 * @property \App\Model\Table\MenuParentsTable|\Cake\ORM\Association\BelongsTo $MenuParents
 *
 * @method \App\Model\Entity\Menu get($primaryKey, $options = [])
 * @method \App\Model\Entity\Menu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Menu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Menu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Menu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Menu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Menu findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * Menus Table.
 *
 * @author  sarawutt.b 
 * @since   2018/04/20 18:12:40
 * @license Pakgon.Ltd
 */
class MenusTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('system.menus');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->belongsTo('SysControllers', [
            'foreignKey' => 'sys_controller_id'
        ]);
        $this->belongsTo('SysActions', [
            'foreignKey' => 'sys_action_id'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 256)
                ->allowEmpty('name');

        $validator
                ->scalar('name_eng')
                ->maxLength('name_eng', 256)
                ->allowEmpty('name_eng');

        $validator
                ->scalar('glyphicon')
                ->maxLength('glyphicon', 512)
                ->allowEmpty('glyphicon');

        $validator
                ->scalar('domain')
                ->maxLength('domain', 128)
                ->allowEmpty('domain');

        $validator
                ->scalar('port')
                ->maxLength('port', 6)
                ->allowEmpty('port');

        $validator
                ->scalar('url')
                ->maxLength('url', 128)
                ->allowEmpty('url');

        $validator
                ->allowEmpty('order_display');

        $validator
                ->requirePresence('child_no', 'create')
                ->notEmpty('child_no');

        $validator
                ->scalar('status')
                ->maxLength('status', 1)
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->integer('create_uid')
                ->requirePresence('create_uid', 'create')
                ->notEmpty('create_uid');

        $validator
                ->integer('update_uid')
                ->allowEmpty('update_uid');

        $validator
                ->scalar('badge')
                ->allowEmpty('badge');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['sys_controller_id'], 'SysControllers'));
        $rules->add($rules->existsIn(['sys_action_id'], 'SysActions'));
        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'system';
    }

    /**
     * 
     * Function list manu page level
     * @author sarawutt.b
     * @param type $key as a integer of page level of menu
     * @return mix 
     */
    public function listMenuPageLevel($key = 'xxx') {
        $options = $this->getEmptySelect() + array('1' => 'หน้าหลัก', '2' => 'หน้าอนุมัติ', '3' => 'หน้าอนุมัติขาขึ้น');
        return array_key_exists($key, $options) ? $options[$key] : $options;
    }

    public function getMenuType($key = null) {
        $opt = array('-1' => $this->select_empty_msg, '0' => __('ACL'), '1' => __('MENU'), '2' => __('ALLOW'));
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    /**
     * 
     * Function fine count menu used for count order indexing for display
     * @author sarawutt.b
     * @return integer
     */
    public function countMenu() {
        return $this->find()->count();
    }

    /**
     * 
     * Get dinamemic parent menu or top of the menu
     * @author sarawutt.b
     * @param type $domain as string domain name or IP address
     * @param type $port as integer of server port number
     * @param type $roleId as integer of the role ID
     * @param type $officerId as integer of the user ID
     * @return array()
     */
    public function getDynamicAclMenu($domain = null, $port = '80', $roleId = 0, $officerId = 0) {
        $port = (($port == '443') || ($port == '80')) ? "'443','80'" : "'{$port}'";
        $roleId = empty($roleId) ? 0 : $roleId;
        $officerId = empty($officerId) ? 0 : $officerId;
        $sql = "SELECT * FROM(
                    SELECT DISTINCT a.id, a.glyphicon, a.domain, a.port, a.url, a.name, a.name_eng, a.menu_parent_id, a.child_no, a.order_display
                    FROM backend.menus AS a
                    INNER JOIN (SELECT m.menu_parent_id
				FROM backend.menus as m
				INNER JOIN backend.sys_acls AS acl ON acl.sys_controller_id = m.sys_controller_id AND acl.sys_action_id = m.sys_action_id
				WHERE (acl.role_id = $roleId OR acl.officer_id = $officerId) AND domain='{$domain}' AND port in({$port}) AND m.is_used IS TRUE) AS b 
                            ON b.menu_parent_id = a.id
                    WHERE a.is_used IS TRUE AND (a.url = '#' OR a.url ILIKE 'http://%' OR a.url ILIKE 'https://%')
                    UNION 
                    SELECT a.id, a.glyphicon, a.domain, a.port, a.url, a.name, a.name_eng, a.menu_parent_id, a.child_no, a.order_display
                    FROM backend.menus as a
                    INNER JOIN backend.sys_acls AS acl ON acl.sys_controller_id = a.sys_controller_id AND acl.sys_action_id = a.sys_action_id
                    INNER JOIN (SELECT m.menu_parent_id
                                FROM backend.menus as m
                                INNER JOIN backend.sys_acls AS acl ON acl.sys_controller_id = m.sys_controller_id AND acl.sys_action_id = m.sys_action_id
                                WHERE (acl.role_id = $roleId OR acl.officer_id = $officerId) AND domain='{$domain}' AND port in({$port}) AND m.is_used IS TRUE) AS b  
                            ON b.menu_parent_id = a.id
                    WHERE (acl.role_id = $roleId OR acl.officer_id = $officerId) AND a.domain='{$domain}' AND a.port in({$port})
                        AND acl.is_used IS TRUE AND a.is_used IS TRUE AND a.menu_parent_id = 0
                ) AS tmenu
                ORDER BY menu_parent_id ASC, child_no ASC, order_display ASC;";
        $results = ConnectionManager::get('default')->execute($sql)->fetchAll('assoc');
        return is_array($results) ? $results : [];
    }

    /**
     * 
     * Find for dinamic choldrent manu
     * @author sarawutt.b
     * @param type $domain as string domain name or IP address
     * @param type $port as integer of server port number
     * @param type $roleId as integer of the role ID
     * @param type $officerId as integer of the user ID
     * @param type $menuParentId as integer of menu parent ID
     * @return array()
     */
    public function getDynamicChildMenu($domain = null, $port = '80', $roleId = 0, $officerId = 0, $menuParentId = -1) {
        $port = (($port == '443') || ($port == '80')) ? "'443','80'" : "'{$port}'";
        $roleId = empty($roleId) ? 0 : $roleId;
        $officerId = empty($officerId) ? 0 : $officerId;
        $sql = "SELECT m.id, glyphicon, domain, port, url, m.name, m.name_eng, menu_parent_id, child_no, order_display, badge
                FROM backend.menus as m
                INNER JOIN backend.sys_acls AS acl ON acl.sys_controller_id = m.sys_controller_id AND acl.sys_action_id = m.sys_action_id
                WHERE (acl.role_id = {$roleId} OR acl.officer_id = {$officerId}) AND domain='{$domain}' AND port IN({$port})
                    AND acl.is_used IS TRUE AND m.is_used IS TRUE AND menu_parent_id = {$menuParentId}
                ORDER BY menu_parent_id ASC, child_no ASC, order_display ASC;";
        $results = ConnectionManager::get('default')->execute($sql)->fetchAll('assoc');
        return is_array($results) ? $results : [];
    }

    /**
     * Find list of menus where the menu is parent menu 
     * @author  sarawutt.b
     * @param   integer of menu id
     * @since   2016/05/11
     * @return  array()
     */
    public function findListParentMenu($id = null) {
        $conditions = (is_null($id) || empty($id)) ? [] : ['id' => $id];
        return $this->getEmptySelect() + $this->find('list', ['conditions' => array_merge($conditions, ['url' => '#']), 'order' => ['menu_parent_id' => 'ASC', 'child_no' => 'ASC', 'order_display' => 'ASC', 'name' => 'ASC']])->toArray();
    }

    /**
     * 
     * Finf dinamic count for badge on the menu left side
     * @author  sarawutt.b
     * @param   string $sql of query excutetion
     * @param   string $statusList of project and plan status list
     * @param   character $badgeType of badge type T = Project and Plan
     * @return  integer of badge count
     */
    public function badgeFinder($sql = null, $statusList = null, $badgeType = 'T') {
        if ($badgeType === 'T') {
            $currentDepartmentId = $this->getCurrenSessionDepartmentId();
            $sql .= " WHERE {$statusList} AND {$currentDepartmentId} IN(department_id,cover_department_id,approved_department_id);";
        }

        try {
            $result = $this->query($sql);
            $count = @$result[0][0][key($result[0][0])];
            return is_numeric($count) ? $count : 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    /**
     *
     * Function findMenusList find for Menus in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:12:40
     * @license Pakgon.Ltd
     */
    public function findMenusList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'name', 'order' => ' name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
