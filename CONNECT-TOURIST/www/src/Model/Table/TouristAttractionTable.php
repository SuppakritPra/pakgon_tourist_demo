<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TouristAttraction Model
 *
 * @property \App\Model\Table\CategoryTable|\Cake\ORM\Association\BelongsTo $Category
 * @property \App\Model\Table\MasterProvinceTable|\Cake\ORM\Association\BelongsTo $MasterProvince
 * @property \App\Model\Table\ReviewTouristTable|\Cake\ORM\Association\BelongsTo $ReviewTourist
 *
 * @method \App\Model\Entity\TouristAttraction get($primaryKey, $options = [])
 * @method \App\Model\Entity\TouristAttraction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TouristAttraction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TouristAttraction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TouristAttraction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TouristAttraction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TouristAttraction findOrCreate($search, callable $callback = null, $options = [])
 */
class TouristAttractionTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tourist_attraction');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Category', [
            'foreignKey' => 'category_id'
        ]);
        $this->belongsTo('MasterProvince', [
            'foreignKey' => 'province_id'
        ]);
        // $this->belongsTo('ReviewTourist', [
        //     'foreignKey' => 'review_tourist_id'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        $validator
            ->scalar('path_picture_tourist_attraction')
            ->allowEmpty('path_picture_tourist_attraction');

        $validator
            ->scalar('detail_tourist')
            ->maxLength('detail_tourist', 255)
            ->allowEmpty('detail_tourist');

        $validator
            ->scalar('path_icon_marker')
            ->maxLength('path_icon_marker', 255)
            ->allowEmpty('path_icon_marker');

        $validator
            ->numeric('latitude')
            ->allowEmpty('latitude');

        $validator
            ->numeric('longtitude')
            ->allowEmpty('longtitude');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'Category'));
        $rules->add($rules->existsIn(['province_id'], 'MasterProvince'));
        // $rules->add($rules->existsIn(['review_tourist_id'], 'ReviewTourist'));
        return $rules;
    }

    public static function defaultConnectionName() {
        return 'db_torist';
    }

    
}
