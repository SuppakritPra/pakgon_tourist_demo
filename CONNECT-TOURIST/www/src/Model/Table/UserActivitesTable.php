<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserActivites Model
 *
 * @property \App\Model\Table\ArticlesTable|\Cake\ORM\Association\BelongsTo $Articles
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserActivite get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserActivite newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserActivite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserActivite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserActivite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserActivite[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserActivite findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserActivitesTable extends Table {

    protected $_LIKE = 'L';
    protected $_READ = 'R';
    protected $_BEACON = 'B';
    protected $_QR_CODE = 'Q';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('user_activites');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        // $validator
        //     ->allowEmpty('id', 'create');
        // $validator
        //     ->scalar('activity_type')
        //     ->allowEmpty('activity_type');
        // $validator
        //     ->requirePresence('create_uid', 'create')
        //     ->notEmpty('create_uid');
        // $validator
        //     ->requirePresence('update_uid', 'create')
        //     ->notEmpty('update_uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['article_id'], 'Articles'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * 
     * Function list all like page
     * @author sarawutt.b
     * @param type $userId as a integer of user admin id
     * @return type
     */
    public function findLike($userId = 0) {
        $options = [
            'conditions' => [
                'activity_type' => $this->_LIKE,
                'user_id' => $userId
            ],
            'keyField' => 'article_id',
            'valueField' => 'id'
        ];
        return $this->findUserActivitiesList($options);
    }

    /**
     *
     * Function findUserActivitiesList find for Roles in select list
     *
     * @author sarawutt.b
     * @param   array() find options
     * @param   boolean of has contain empty select
     * @return  array with empty select
     * @license Pakgon.Ltd
     */
    public function findUserActivitiesList($paramsOptions = [], $hasEmptySelect = false) {
        $selfOptions = ['keyField' => 'article_id', 'valueField' => 'id', 'order' => 'article_id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return ($hasEmptySelect) ? $this->getEmptySelect() + $this->find('list', $options)->toArray() : $this->find('list', $options)->toArray();
    }

}
