<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MasterProvince Model
 *
 * @property \App\Model\Table\RegionTable|\Cake\ORM\Association\BelongsTo $Region
 *
 * @method \App\Model\Entity\MasterProvince get($primaryKey, $options = [])
 * @method \App\Model\Entity\MasterProvince newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MasterProvince[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MasterProvince|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MasterProvince patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MasterProvince[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MasterProvince findOrCreate($search, callable $callback = null, $options = [])
 */
class MasterProvinceTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('master_province');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Region', [
            'foreignKey' => 'region_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('province_name_th')
            ->maxLength('province_name_th', 100)
            ->allowEmpty('province_name_th');

        $validator
            ->scalar('province_name_en')
            ->maxLength('province_name_en', 100)
            ->allowEmpty('province_name_en');

        $validator
            ->scalar('logo_of_province')
            ->maxLength('logo_of_province', 255)
            ->allowEmpty('logo_of_province');

        $validator
            ->numeric('center_lat')
            ->allowEmpty('center_lat');

        $validator
            ->numeric('center_long')
            ->allowEmpty('center_long');
        $validator
            ->scalar('banner_path')
            ->maxLength('banner_path', 255)
            ->allowEmpty('banner_path');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['region_id'], 'Region'));

        return $rules;
    }
    public static function defaultConnectionName() {
        return 'db_torist';
    }
}
