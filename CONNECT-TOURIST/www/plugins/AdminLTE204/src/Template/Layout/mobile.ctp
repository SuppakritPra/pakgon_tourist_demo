<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo $this->Configure->read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>
        <?php
        echo $this->Html->css([
            'AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min',
            'AdminLTE204./plugins/font-awesome/css/font-awesome.min',
            'AdminLTE204./plugins/Ionicons/css/ionicons.min',
            'AdminLTE204./plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min',
            'AdminLTE204./plugins/select2/dist/css/select2.min',
            'AdminLTE204./plugins/iCheck/all',
            'AdminLTE204.AdminLTE.min',
            'AdminLTE204.skins/_all-skins.min',
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
            '/vendor/nprogress/nprogress.min',
            '/vendor/toastr/build/toastr.min',
            'main.style',
            'AdminLTE204./plugins/OwlCarousel2/dist/assets/owl.carousel',
            'AdminLTE204./plugins/OwlCarousel2/dist/assets/owl.theme.default.min'
        ]);
        ?>
        <!-- AdminLTE204 Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <?php //echo $this->Html->css('AdminLTE204.skins/skin-'. Configure::read('Theme.skin') .'.min'); ?>
        <?php echo $this->Html->css('commun'); ?>
        <?php echo $this->fetch('css'); ?>

        <?php echo $this->Html->script('/vendor/ckeditor/ckeditor'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        echo $this->Html->script([
            'AdminLTE204./plugins/jquery/dist/jquery.min',
            'AdminLTE204./plugins/bootstrap/dist/js/bootstrap.min'
        ]);
        ?>   
    </head>
    <body>
        <!-- -->
        <div class="container">

            <!-- Content . Contains page content -->

            <div class="row">

                <!-- <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-12 "> -->
                <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 ccol-sm-12 col-xs-12 ">


                    <?php //echo $this->Flash->render(); ?>
                    <?php //echo $this->Flash->render('auth'); ?>
                    <?php echo $this->fetch('content'); ?>

                </div>

            </div>

            <!-- /.content -->

        </div>
        <!-- ./wrapper -->

        <?php
        echo $this->Html->script([
            'AdminLTE204./plugins/jquery-slimscroll/jquery.slimscroll.min',
            'AdminLTE204./plugins/fastclick/lib/fastclick',
            'AdminLTE204.adminlte.min',
            'AdminLTE204.demo',
            '/vendor/jquery-validation/jQuery.start.min',
            '/vendor/jquery-validation/jQuery.validate.min',
            '/vendor/jquery-validation/jQuery.validate.addValidate',
            'AdminLTE204./plugins/select2/dist/js/select2.full.min',
            'AdminLTE204./plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
            'AdminLTE204./plugins/iCheck/icheck.min',
            '/vendor/nprogress/nprogress.min',
            '/vendor/toastr/build/toastr.min',
            'AdminLTE204./plugins/moment/min/moment.min',
            'AdminLTE204./plugins/OwlCarousel2/dist/owl.carousel.min'
        ]);
        ?>
        <?php echo $this->element('modal/modal'); ?>
        <?php echo $this->element('utility/utilityScript'); ?>
        <?php echo $this->element('utility/gtag'); ?>
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
