<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo $this->Configure->read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>
        <?php
        echo $this->Html->css([
            'AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min',
            'AdminLTE204./plugins/font-awesome/css/font-awesome.min',
            'AdminLTE204./plugins/Ionicons/css/ionicons.min',
            'AdminLTE204./plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min',
            'AdminLTE204./plugins/select2/dist/css/select2.min',
            'AdminLTE204./plugins/iCheck/all',
            'AdminLTE204.AdminLTE.min',
            'AdminLTE204.skins/_all-skins.min',
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
            '/vendor/nprogress/nprogress.min',
            '/vendor/toastr/build/toastr.min',
            'main.style'
        ]);
        ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>

        <?php
        echo $this->Html->script([
            'AdminLTE204./plugins/jquery/dist/jquery.min',
            'AdminLTE204./plugins/bootstrap/dist/js/bootstrap.min'
        ]);
        ?>        
    </head>

    <body class="hold-transition skin-blue fixed sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <?php
            echo $this->element('theme/top-nav'); //Top nav Header
            echo $this->element('theme/aside-main-sidebar'); //Left side column. contains the sidebar
            ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $this->element('utility/breadcrumb'); //Display for breadcrumb ?>
                </section>
                <section class="content">
                    <?php echo $this->Flash->render(); //Display for flash notification?>
                    <?php echo $this->fetch('content'); //Fetch for content ?>
                </section>
            </div>
            <!-- /.content-wrapper -->

            <?php
            echo $this->element('theme/footer'); //Theme footer or display credit
            echo $this->element('theme/aside-control-sidebar'); //Control Sidebar
            ?>

            <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <?php
        echo $this->Html->script([
            'AdminLTE204./plugins/jquery-slimscroll/jquery.slimscroll.min',
            'AdminLTE204./plugins/fastclick/lib/fastclick',
            'AdminLTE204.adminlte.min',
            'AdminLTE204.demo',
            '/vendor/jquery-validation/jQuery.start.min',
            '/vendor/jquery-validation/jQuery.validate.min',
            '/vendor/jquery-validation/jQuery.validate.addValidate',
            'AdminLTE204./plugins/select2/dist/js/select2.full.min',
            'AdminLTE204./plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
            'AdminLTE204./plugins/iCheck/icheck.min',
            '/vendor/nprogress/nprogress.min',
            '/vendor/toastr/build/toastr.min',
            'AdminLTE204./plugins/moment/min/moment.min'
        ]);
        ?>
        <?php echo $this->element('modal/modal'); ?>
        <?php echo $this->element('utility/utilityScript'); ?>
        <?php echo $this->element('utility/gtag'); ?>
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
