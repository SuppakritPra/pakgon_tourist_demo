<?php echo __('Image Title') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
<?php 
    echo $this->Form->input('image_title', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'image/jpeg,image/bmp',
        'id' => 'image_title'
    ));
?>
<?php foreach ($article_assets as $article_asset):
    $img = $article_asset['path'];
?>

    <?php if($article_asset['article_asset_type'] == 'image_title'){
         $this->append('scriptBottom');?>
        <script> $('#image_title').attr("disabled",true);</script>
    <?php $this->end(); ?>
        <div class="row row-article_asset" id="row-article_asset_id_<?php echo $article_asset->id;?>">
        <div class="col-xs-10"> 
            <?php echo $this->Html->image("/upload/image_title/".$img, [
                "height" => "50px"
            ]);?>
        </div>
        <div class="col-xs-2"> 
            <?php
            echo $this->Form->button($this->Html->icon('trash'), [
                'class' => 'btn btn-sm btn-danger btn-rounded waves-effect waves-light action-delete', 
                'escape' => false,
                'type' => 'button',
                'data-article_asset_id' => $article_asset->id,
                'data-type' => 'image_title'
            ]);
            ?>
        </div>
    </div>
    <?php } ?> 
<?php endforeach; ?>

<?php echo __('Image Detial') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('image_detail[]', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'image/jpeg,image/bmp',
        'multiple' => 'multiple'
    ));
 ?>
<?php foreach ($article_assets as $article_asset):
    $img = $article_asset['path'];
?>

    <?php if($article_asset['article_asset_type'] == 'image_detail'){?>
        <div class="row row-article_asset" id="row-article_asset_id_<?php echo $article_asset->id;?>">
        <div class="col-xs-10"> 
            <?php echo $this->Html->image("/upload/image_detail/".$img, [
                "height" => "50px"
            ]);?>
        </div>
        <div class="col-xs-2">
            <?php echo $this->Form->button($this->Html->icon('trash'), [
                'class' => 'btn btn-sm btn-danger btn-rounded waves-effect waves-light action-delete', 
                'escape' => false,
                'type' => 'button',
                'data-article_asset_id' => $article_asset->id,
                'data-type' => 'image_detail'
            ]); ?> 
        </div>
    </div>
    <?php } ?>
<?php endforeach; ?>

<?php echo __('Audio') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('audio', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'audio/mpeg',
        'id' => 'audio'
    ));
?>

<?php echo __('NameForYourAudio') ?>
    <table class="table" cellpadding="0" cellspacing="0">
        <tbody>
            <?php 
            $audio='';
            foreach ($article_assets as $article_asset):
                $article_asset_name = $article_asset['article_asset_name'];
            ?>
                <tr>
                    <?php if($article_asset['article_asset_type'] == 'audio'){
                        $this->append('scriptBottom');
                    ?>
                        <script> $('#audio').attr("disabled",true);</script>
                    <?php $this->end(); ?>
                        <td align="center">
                        <!-- <?//php echo $img ;?> -->
                            <?php echo $this->Form->input('name_for_audio', array(
                                'label' => false,
                                'type' => 'text',
                                'required' => 'required',
                                'value' => $article_asset_name,
                                'id' => 'input_article_asset_id_'.$article_asset->id,
                                'disabled'
                                ));
                            ?>
                        </td>
                        <td class="actions" align="center">  
                            <?php echo $this->Form->button($this->Html->icon('trash'), [
                                'class' => 'btn btn-sm btn-danger btn-rounded waves-effect waves-light action-delete', 
                                'escape' => false,
                                'type' => 'button',
                                'data-article_asset_id' => $article_asset->id,
                                'data-type' => 'audio'
                            ]); ?> 
                        </td>

                    <?php 
                        $audio++;
                    }//.if($article_asset['article_asset_type'] == 'audio') ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php 
    if(!$audio){
        echo $this->Form->input('name_for_audio', array(
            'label' => false,
            'type' => 'text',
            'required' => 'required',
            'disabled'
        ));
    }
?>

<?php echo __('Video') ?><i><?php echo __(' (Size not over').' 60 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('video', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'video/mp4',
        'id' => 'video'
    ));
?>

<?php echo __('NameForYourVideo') ?>
    <table class="table" cellpadding="0" cellspacing="0">
        <tbody>
            <?php 
            $video = '';
            foreach ($article_assets as $article_asset):
                $article_asset_name = $article_asset['article_asset_name'];
            ?>
                <tr>
                    <?php if($article_asset['article_asset_type'] == 'video'){
                        $this->append('scriptBottom');
                        ?>
                                <script> $('#video').attr("disabled",true);</script>
                        <?php 
                            $this->end();
                        ?>
                        <td align="center">
                        <!-- <?//php echo $img ;?> -->
                        <?php echo $this->Form->input('name_for_video', array(
                            'label' => false,
                            'type' => 'text',
                            'required' => 'required',
                            'value' => $article_asset_name,
                            'id' => 'input_article_asset_id_'.$article_asset->id,
                            'disabled'
                            ));
                        ?>
                        </td>
                        <td class="actions" align="center">  
                        
                            <?php echo $this->Form->button($this->Html->icon('trash'), [
                                'class' => 'btn btn-sm btn-danger btn-rounded waves-effect waves-light action-delete', 
                                    'escape' => false,
                                    'type' => 'button',
                                    'data-article_asset_id' => $article_asset->id,
                                    'data-type' => 'video'
                            ]); ?> 

                        </td>
                    <?php 
                $video++;
                } ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

 <?php 
    if(!$video){
        echo $this->Form->input('name_for_video', array(
            'label' => false,
            'type' => 'text',
            'required' => 'required',
            'disabled'
        ));
    }
?>
                <!-- </td>
            </tr>
        </tbody>
    </table>
</div> -->

<style type="text/css">
    #table-for-add-file th{
        font-weight: bold;
        color: #3c8dbc;

    }
    #table-for-add-file th i{
        font-size: 22px;
    }
    .row-article_asset{
        border-bottom: 1px solid #f4f4f4;
        padding: 5px;
        margin: 5px;  
    }
   
</style>