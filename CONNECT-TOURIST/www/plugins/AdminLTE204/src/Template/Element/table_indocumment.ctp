<?php echo __('Image Title') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('image_title', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'image/jpeg,image/bmp'
        ));
?>
<?php echo __('Image Detial') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('image_detail[]', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'image/jpeg,image/bmp',
        'multiple' => 'multiple'
    ));
 ?>
<?php echo __('Audio') ?><i><?php echo __(' (Size not over').' 10 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('audio', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'audio/mpeg'
    ));
?>
<?php echo __('NameForYourAudio') ?>
<?php echo $this->Form->input('name_for_audio', array(
        'label' => false,
        'type' => 'text',
        'required' => 'required',
        'disabled'
    ));
?>
<?php echo __('Video') ?><i><?php echo __(' (Size not over').' 60 '.__('Mb.)') ?></i>
<?php echo $this->Form->input('video', array(
        'label' => false,
        'type' => 'file',
        'accept' => 'video/mp4'
    ));
?>
<?php echo __('NameForYourVideo') ?>
<?php echo $this->Form->input('name_for_video', array(
        'label' => false,
        'type' => 'text',
        'required' => 'required',
        'disabled'
    ));
?>