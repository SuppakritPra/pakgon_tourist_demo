<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo $this->Configure->read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>
        <?php
        echo $this->Html->css([
            'Ablepro6./light-vertical/assets/icon/icofont/css/icofont.css',
            'Ablepro6./light-vertical/assets/icon/ion-icon/css/ionicons.min.css',
            'Ablepro6./light-vertical/assets/icon/simple-line-icons/css/simple-line-icons.css',
            'Ablepro6./light-vertical/assets/css/font-awesome.min.css',
            'Ablepro6./bower_components/bootstrap/dist/css/bootstrap.min.css',
            'Ablepro6./files/plugins/timeline/css/style.css',
            'Ablepro6./light-vertical/assets/css/main.css',
            'Ablepro6./light-vertical/assets/css/responsive.css',
            'Ablepro6./plugins/nprogress/nprogress.min',
            'Ablepro6./plugins/toastr/build/toastr.min',
            'Ablepro6./plugins/select2_v4/dist/css/select2.min',
            'Ablepro6./plugins/select2_v4/dist/css/select2-bootstrap4.min',
            'Ablepro6./plugins/iCheck/all',
            'Ablepro6./css/pakgon.connect.main.css',
        ]);
        ?>
        <?php echo $this->fetch('css'); ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        echo $this->Html->script([
            'Ablepro6./bower_components/jquery/js/jquery.min.js',
            'Ablepro6./bower_components/bootstrap/dist/js/bootstrap.bundle.min.js',
            'Ablepro6./plugins/toastr/build/toastr.min',
        ]);
        ?> 
        <?php echo $this->element('utility/toastNotification'); ?>
    </head>
    <body>
        <div class="container-fluid">
            <br/>
            <div class="row">
                <div class="col">
                    <?php echo $this->Flash->render(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php echo $this->fetch('content'); ?>
                    <?php echo $this->element('modal/modal'); ?>
                </div>
            </div>
        </div>

        <!-- Warning Section Starts -->
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
              <div class="ie-warning">
                  <h1>Warning!!</h1>
                  <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                  <div class="iew-container">
                      <ul class="iew-download">
                          <li>
                              <a href="http://www.google.com/chrome/">
                                  <img src="/ablepro6/files/images/browser/chrome.png" alt="Chrome">
                                  <div>Chrome</div>
                              </a>
                          </li>
                          <li>
                              <a href="https://www.mozilla.org/en-US/firefox/new/">
                                  <img src="/ablepro6/files/images/browser/firefox.png" alt="Firefox">
                                  <div>Firefox</div>
                              </a>
                          </li>
                          <li>
                              <a href="http://www.opera.com">
                                  <img src="/ablepro6/files/images/browser/opera.png" alt="Opera">
                                  <div>Opera</div>
                              </a>
                          </li>
                          <li>
                              <a href="https://www.apple.com/safari/">
                                  <img src="/ablepro6/files/images/browser/safari.png" alt="Safari">
                                  <div>Safari</div>
                              </a>
                          </li>
                          <li>
                              <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                  <img src="/ablepro6/files/images/browser/ie.png" alt="">
                                  <div>IE (9 & above)</div>
                              </a>
                          </li>
                      </ul>
                  </div>
                  <p>Sorry for the inconvenience!</p>
              </div>
              <![endif]-->
        <!-- Warning Section Ends -->

        <?php
        echo $this->Html->script([
            'Ablepro6./files/plugins/waves/js/waves.min.js',
            'Ablepro6./bower_components/jquery-slimscroll/js/jquery.slimscroll.js',
            'Ablepro6./files/plugins/jquery.nicescroll/js/jquery.nicescroll.min.js',
            'Ablepro6./bower_components/classie/js/classie.js',
            'Ablepro6./plugins/nprogress/nprogress.min',
            'Ablepro6./plugins/moment/min/moment.min',
            'Ablepro6./plugins/jquery-validation/jQuery.start.min',
            'Ablepro6./plugins/jquery-validation/jQuery.validate.min',
            'Ablepro6./plugins/jquery-validation/jQuery.validate.addValidate',
            'Ablepro6./plugins/select2_v4/dist/js/select2.full.min',
            'Ablepro6./plugins/iCheck/icheck.min'
        ]);
        ?>
        <?php echo $this->element('utility/utilityScript'); ?>
        <?php echo $this->element('utility/gtag'); ?>
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>
