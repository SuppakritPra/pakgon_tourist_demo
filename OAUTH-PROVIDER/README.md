# PAKGON EDUCATION FRONTEND

PAKGON EDUCATION FRONTEND PROJECT

## Git Clone for the repository

```bash
$ cd D:/xampp7/htdocs/
$ mkdir CONNECT-EDU-FRONTEND
$ cd CONNECT-EDU-FRONTEND
$ git clone http://gitlab.pakgon.com/pakgon-connect/connect-edu-frontend.git .
```

```bash
$ cd www
$ composer install
```

## NPM Install Package 

```bash
$ cd www/webroot
$ npm install
```

## SETTING UP FOR V-HOST

```bash
<VirtualHost *:80>
  ServerName edu-sit.pakgon.local
  ServerAlias edu-sit.pakgon.local
  DocumentRoot "D:/xampp7/htdocs/CONNECT-EDU-FRONTEND/www"
  ErrorLog "logs/edu-sit.pakgon.local-error.log"
  CustomLog "logs/edu-sit.pakgon.local-access.log" common
  <Directory "D:/xampp7/htdocs/CONNECT-EDU-FRONTEND/www">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
</VirtualHost>
```

## HOST FILE

```bash
127.0.0.1       edu-sit.pakgon.local
::1             edu-sit.pakgon.local
```

## Current path are in bin directory

```bash
$ ./cake bake all [-c connection] [-t Pakgon]
```
