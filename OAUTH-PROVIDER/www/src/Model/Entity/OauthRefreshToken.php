<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OauthRefreshToken Entity
 *
 * @property string $refresh_token
 * @property string $oauth_token
 * @property int $expires
 */
/**
 *
 * OauthRefreshToken Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:03:50
 * @license Pakgon.Ltd
 */
class OauthRefreshToken extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'oauth_token' => true,
        'expires' => true
    ];
}
