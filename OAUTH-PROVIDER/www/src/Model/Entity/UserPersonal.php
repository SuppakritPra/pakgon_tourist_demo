<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserPersonal Entity
 *
 * @property int $id
 * @property string $person_card_no
 * @property int $master_prefix_id
 * @property string $firstname_th
 * @property string $lastname_th
 * @property string $firstname_en
 * @property string $lastname_en
 * @property string $gender
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property string $moblie_no
 * @property string $phone_no
 * @property string $email
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_id
 * @property string $blood_group
 * @property string $address
 *
 * @property \App\Model\Entity\MasterPrefix $master_prefix
 * @property \App\Model\Entity\User $user
 */
class UserPersonal extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'person_card_no' => true,
        'master_prefix_id' => true,
        'firstname_th' => true,
        'lastname_th' => true,
        'firstname_en' => true,
        'lastname_en' => true,
        'gender' => true,
        'birthdate' => true,
        'moblie_no' => true,
        'phone_no' => true,
        'email' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'user_id' => true,
        'blood_group' => true,
        'address' => true,
        'master_prefix' => true,
        'user' => true
    ];
}
