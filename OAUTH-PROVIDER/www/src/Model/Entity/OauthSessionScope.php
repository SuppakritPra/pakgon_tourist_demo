<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OauthSessionScope Entity
 *
 * @property int $id
 * @property int $session_id
 * @property string $scope_id
 *
 * @property \App\Model\Entity\Session $session
 * @property \App\Model\Entity\Scope $scope
 */
/**
 *
 * OauthSessionScope Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:04:33
 * @license Pakgon.Ltd
 */
class OauthSessionScope extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'session_id' => true,
        'scope_id' => true,
        'session' => true,
        'scope' => true
    ];
}
