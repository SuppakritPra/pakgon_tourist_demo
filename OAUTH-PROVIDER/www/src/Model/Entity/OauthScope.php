<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OauthScope Entity
 *
 * @property string $id
 * @property string $description
 */
/**
 *
 * OauthScope Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:04:14
 * @license Pakgon.Ltd
 */
class OauthScope extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'description' => true
    ];
}
