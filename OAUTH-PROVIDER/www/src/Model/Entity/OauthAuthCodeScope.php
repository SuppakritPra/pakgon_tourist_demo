<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OauthAuthCodeScope Entity
 *
 * @property int $id
 * @property string $auth_code
 * @property string $scope_id
 *
 * @property \App\Model\Entity\Scope $scope
 */
/**
 *
 * OauthAuthCodeScope Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:03:17
 * @license Pakgon.Ltd
 */
class OauthAuthCodeScope extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'auth_code' => true,
        'scope_id' => true,
        'scope' => true
    ];
}
