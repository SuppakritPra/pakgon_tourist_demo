<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Utility\Text;
use OAuthServer\Model\Entity\Client as UOauthClient;

/**
 * OauthClient Entity
 *
 * @property string $id
 * @property string $client_secret
 * @property string $name
 * @property string $redirect_uri
 * @property string $parent_model
 * @property int $parent_id
 *
 * @property \App\Model\Entity\ParentOauthClient $parent_oauth_client
 * @property \App\Model\Entity\ChildOauthClient[] $child_oauth_clients
 */

/**
 *
 * OauthClient Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 16:59:14
 * @license Pakgon.Ltd
 */
class OauthClient extends UOauthClient {
    
    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'client_secret' => true,
        'name' => true,
        'redirect_uri' => true,
        'parent_model' => true,
        'parent_id' => true,
        'parent_oauth_client' => true,
        'child_oauth_clients' => true
    ];

}
