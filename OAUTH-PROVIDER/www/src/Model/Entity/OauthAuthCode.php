<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OauthAuthCode Entity
 *
 * @property string $code
 * @property int $session_id
 * @property string $redirect_uri
 * @property int $expires
 *
 * @property \App\Model\Entity\Session $session
 */
/**
 *
 * OauthAuthCode Entity.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:03:28
 * @license Pakgon.Ltd
 */
class OauthAuthCode extends Entity
{

    /**
     *
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @author sarawutt.b
     * @var array
     */
    protected $_accessible = [
        'session_id' => true,
        'redirect_uri' => true,
        'expires' => true,
        'session' => true
    ];
}
