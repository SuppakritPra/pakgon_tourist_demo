<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthScopes Model
 *
 * @method \App\Model\Entity\OauthScope get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthScope newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthScope[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthScope|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthScope patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthScope[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthScope findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthScopes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:04:14
 * @license Pakgon.Ltd
 */
class OauthScopesTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('oauth_scopes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Common');
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
//        $validator
//                ->scalar('id')
//                ->maxLength('id', 40)
//                ->allowEmpty('id', 'create');
//
//        $validator
//                ->scalar('description')
//                ->maxLength('description', 200)
//                ->requirePresence('description', 'create')
//                ->notEmpty('description');

        return $validator;
    }

    /**
     *
     * Function findOauthScopesList find for OauthScopes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function findOauthScopesList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
