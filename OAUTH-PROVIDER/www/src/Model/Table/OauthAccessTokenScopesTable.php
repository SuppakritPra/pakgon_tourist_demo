<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthAccessTokenScopes Model
 *
 * @property \App\Model\Table\ScopesTable|\Cake\ORM\Association\BelongsTo $Scopes
 *
 * @method \App\Model\Entity\OauthAccessTokenScope get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthAccessTokenScope newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthAccessTokenScope[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthAccessTokenScope|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthAccessTokenScope patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAccessTokenScope[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAccessTokenScope findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthAccessTokenScopes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:02:16
 * @license Pakgon.Ltd
 */
class OauthAccessTokenScopesTable extends Table
{

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oauth_access_token_scopes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Common');
        $this->belongsTo('Scopes', [
            'foreignKey' => 'scope_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('oauth_token')
            ->maxLength('oauth_token', 40)
            ->requirePresence('oauth_token', 'create')
            ->notEmpty('oauth_token');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['scope_id'], 'Scopes'));

        return $rules;
    }
    
    /**
     *
     * Function findOauthAccessTokenScopesList find for OauthAccessTokenScopes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function findOauthAccessTokenScopesList($paramsOptions = []){
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }
}
