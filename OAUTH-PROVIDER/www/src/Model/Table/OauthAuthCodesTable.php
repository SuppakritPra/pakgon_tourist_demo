<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthAuthCodes Model
 *
 * @property \App\Model\Table\SessionsTable|\Cake\ORM\Association\BelongsTo $Sessions
 *
 * @method \App\Model\Entity\OauthAuthCode get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthAuthCode newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthAuthCode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthAuthCode|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthAuthCode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAuthCode[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAuthCode findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthAuthCodes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:03:28
 * @license Pakgon.Ltd
 */
class OauthAuthCodesTable extends Table
{

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oauth_auth_codes');
        $this->setDisplayField('code');
        $this->setPrimaryKey('code');
        $this->addBehavior('Common');
        $this->belongsTo('Sessions', [
            'foreignKey' => 'session_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('code')
            ->maxLength('code', 40)
            ->allowEmpty('code', 'create');

        $validator
            ->scalar('redirect_uri')
            ->maxLength('redirect_uri', 200)
            ->requirePresence('redirect_uri', 'create')
            ->notEmpty('redirect_uri');

        $validator
            ->integer('expires')
            ->requirePresence('expires', 'create')
            ->notEmpty('expires');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['session_id'], 'Sessions'));

        return $rules;
    }
    
    /**
     *
     * Function findOauthAuthCodesList find for OauthAuthCodes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function findOauthAuthCodesList($paramsOptions = []){
        $selfOptions = ['keyField' => 'id', 'valueField' => 'code', 'order' => ' code'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }
}
