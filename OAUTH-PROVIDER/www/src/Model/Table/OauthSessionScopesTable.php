<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthSessionScopes Model
 *
 * @property \App\Model\Table\SessionsTable|\Cake\ORM\Association\BelongsTo $Sessions
 * @property \App\Model\Table\ScopesTable|\Cake\ORM\Association\BelongsTo $Scopes
 *
 * @method \App\Model\Entity\OauthSessionScope get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthSessionScope newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthSessionScope[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthSessionScope|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthSessionScope patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthSessionScope[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthSessionScope findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthSessionScopes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:04:33
 * @license Pakgon.Ltd
 */
class OauthSessionScopesTable extends Table
{

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oauth_session_scopes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Common');
        $this->belongsTo('Sessions', [
            'foreignKey' => 'session_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Scopes', [
            'foreignKey' => 'scope_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['session_id'], 'Sessions'));
        $rules->add($rules->existsIn(['scope_id'], 'Scopes'));

        return $rules;
    }
    
    /**
     *
     * Function findOauthSessionScopesList find for OauthSessionScopes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function findOauthSessionScopesList($paramsOptions = []){
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }
}
