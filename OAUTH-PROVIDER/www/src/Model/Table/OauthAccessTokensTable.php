<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * OauthAccessTokens Model
 *
 * @property \App\Model\Table\SessionsTable|\Cake\ORM\Association\BelongsTo $Sessions
 *
 * @method \App\Model\Entity\OauthAccessToken get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthAccessToken newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthAccessToken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthAccessToken|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthAccessToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAccessToken[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAccessToken findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthAccessTokens Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:01:59
 * @license Pakgon.Ltd
 */
class OauthAccessTokensTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('oauth_access_tokens');
        $this->setDisplayField('oauth_token');
        $this->setPrimaryKey('oauth_token');
        $this->addBehavior('Common');
        $this->belongsTo('OauthSessions', [
            'foreignKey' => 'session_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * 
     * Function token logout delete token
     * @author sarawutt.b
     * @since   2018/07/06 12:02:15
     * @license PAKGON
     * @return array()
     */
    public function logoutDeleteToken($ownerId = null) {
        if (empty($ownerId)) {
            return [];
        }
        $sql = "UPDATE oauth_access_tokens SET expires = 0 WHERE session_id IN (
                        SELECT sess.id 
                        FROM oauth_access_tokens AS tok
                        INNER JOIN oauth_sessions AS sess ON sess.id = tok.session_id
                        WHERE sess.owner_id = '{$ownerId}' AND tok.expires > 0
                );";
        $results = ConnectionManager::get('default')->execute($sql);
        return empty($results) ? [] : $results;
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->scalar('oauth_token')
                ->maxLength('oauth_token', 40)
                ->allowEmpty('oauth_token', 'create');

        $validator
                ->integer('expires')
                ->requirePresence('expires', 'create')
                ->notEmpty('expires');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['session_id'], 'Sessions'));

        return $rules;
    }

    /**
     *
     * Function findOauthAccessTokensList find for OauthAccessTokens in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function findOauthAccessTokensList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'oauth_token', 'order' => ' oauth_token'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
