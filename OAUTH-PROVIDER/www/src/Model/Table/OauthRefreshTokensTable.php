<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthRefreshTokens Model
 *
 * @method \App\Model\Entity\OauthRefreshToken get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthRefreshToken newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthRefreshToken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthRefreshToken|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthRefreshToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthRefreshToken[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthRefreshToken findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthRefreshTokens Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:03:50
 * @license Pakgon.Ltd
 */
class OauthRefreshTokensTable extends Table
{

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:03:50
     * @license Pakgon.Ltd
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oauth_refresh_tokens');
        $this->setDisplayField('refresh_token');
        $this->setPrimaryKey('refresh_token');
        $this->addBehavior('Common');    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:03:50
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('refresh_token')
            ->maxLength('refresh_token', 40)
            ->allowEmpty('refresh_token', 'create');

        $validator
            ->scalar('oauth_token')
            ->maxLength('oauth_token', 40)
            ->requirePresence('oauth_token', 'create')
            ->notEmpty('oauth_token');

        $validator
            ->integer('expires')
            ->requirePresence('expires', 'create')
            ->notEmpty('expires');

        return $validator;
    }
    
    /**
     *
     * Function findOauthRefreshTokensList find for OauthRefreshTokens in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:03:50
     * @license Pakgon.Ltd
     */
    public function findOauthRefreshTokensList($paramsOptions = []){
        $selfOptions = ['keyField' => 'id', 'valueField' => 'refresh_token', 'order' => ' refresh_token'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }
}
