<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Users Model
 *
 * @property \App\Model\Table\AppAccessTable|\Cake\ORM\Association\HasMany $AppAccess
 * @property \App\Model\Table\UserDefaultLanguagesTable|\Cake\ORM\Association\HasMany $UserDefaultLanguages
 * @property \App\Model\Table\UserHomeplacesTable|\Cake\ORM\Association\HasMany $UserHomeplaces
 * @property \App\Model\Table\UserPersonListsTable|\Cake\ORM\Association\HasMany $UserPersonLists
 * @property \App\Model\Table\UserPersonalsTable|\Cake\ORM\Association\HasMany $UserPersonals
 * @property \App\Model\Table\UserProfilesTable|\Cake\ORM\Association\HasMany $UserProfiles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

/**
 *
 * Users Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 16:57:02
 * @license Pakgon.Ltd
 */
class UsersTable extends Table {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 16:57:02
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
//        $this->hasMany('AppAccess', [
//            'foreignKey' => 'user_id'
//        ]);
//        $this->hasMany('UserDefaultLanguages', [
//            'foreignKey' => 'user_id'
//        ]);
//        $this->hasMany('UserHomeplaces', [
//            'foreignKey' => 'user_id'
//        ]);
//        $this->hasMany('UserPersonLists', [
//            'foreignKey' => 'user_id'
//        ]);
        $this->hasMany('UserPersonals', [
            'foreignKey' => 'user_id',
            'joinType'=>'INNER'
        ]);
//        $this->hasMany('UserProfiles', [
//            'foreignKey' => 'user_id'
//        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 16:57:02
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator) {
//        $validator
//                ->allowEmpty('id', 'create');
//
//        $validator
//                ->scalar('username')
//                ->maxLength('username', 100)
//                ->allowEmpty('username');
//
//        $validator
//                ->scalar('password')
//                ->maxLength('password', 100)
//                ->allowEmpty('password');
//
//        $validator
//                ->allowEmpty('point');
//
//        $validator
//                ->boolean('is_used')
//                ->allowEmpty('is_used');
//
//        $validator
//                ->scalar('dynamic_key')
//                ->maxLength('dynamic_key', 50)
//                ->allowEmpty('dynamic_key');
//
//        $validator
//                ->date('dynamic_key_expiry')
//                ->allowEmpty('dynamic_key_expiry');
//
//        $validator
//                ->scalar('token')
//                ->maxLength('token', 50)
//                ->allowEmpty('token');
//
//        $validator
//                ->date('token_expiry')
//                ->allowEmpty('token_expiry');
//
//        $validator
//                ->requirePresence('created_by', 'create')
//                ->notEmpty('created_by');
//
//        $validator
//                ->allowEmpty('modified_by');
//
//        $validator
//                ->scalar('pin_code')
//                ->maxLength('pin_code', 4)
//                ->allowEmpty('pin_code');
//
        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 16:57:02
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }

    /**
     *
     * Returns the database connection name to use by default.
     *
     * @author sarawutt.b
     * @return string
     * @since   2018/05/10 16:57:02
     * @license Pakgon.Ltd
     */
    public static function defaultConnectionName() {
        return 'connect_core';
    }

    /**
     *
     * Function findUsersList find for Users in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 16:57:02
     * @license Pakgon.Ltd
     */
    public function findUsersList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

    /**
     * 
     * get auth user information
     * @author sarawutt.b
     * @param $params as a array of client_id and access_token must appear and not empty
     * @return array()
     */
    public function getOauthInfo($params = []) {
        $response = [];
        if (!array_key_exists('client_id', $params) || empty($params['client_id'])) {
            $response['code'] = 'error';
            $response['message'] = __('Please check for parameter "client_id" must appear and not empty.');
        } elseif (!array_key_exists('access_token', $params) || empty($params['access_token'])) {
            $response['code'] = 'error';
            $response['message'] = __('Please check for parameter "access_token" must appear and not empty.');
        } else {

            $clientId = $params['client_id'];
            $accessToken = $params['access_token'];

            $sql = "select *
                    from oauth_access_tokens as token
                    inner join oauth_sessions as session on token.session_id = session.id
                    where session.client_id = '{$clientId}' and token.oauth_token = '{$accessToken}'
                    ";
//            $sql = "select u.id, u.email
//                    from oauth_access_tokens as token
//                    inner join oauth_sessions as session on token.session_id = session.id
//                    inner join users as u on u.id::int = session.owner_id::int
//                    where session.client_id = '{$clientId}' and token.oauth_token = '{$accessToken}'
//                    ";
                    
// select u.username, uper.firstname_th, uper.lastname_th, uper.firstname_en, uper.lastname_en, uper.gender, uper.moblie_no, uper.phone_no, uper.email, uper.address, uper.blood_group, uper.master_nationality_id
//from core.users as u
//left join core.user_profiles as upro on upro.user_id = u.id
//left join core.user_personals as uper on uper.user_id = u.id
//where u.id = 59296                   
                    
            $tmp = ConnectionManager::get('default')->execute($sql)->fetchAll('assoc');
            
            $userId = $tmp[0]['owner_id'];
            
            
            $conn = ConnectionManager::get('connect_core');
            
            
            $response['code'] = 'success';
            
            //user information
            $sql = "SELECT id, username, token FROM core.users where id = {$userId};";
            $response['user'] = $conn->execute($sql)->fetchAll('assoc');
            
	    //User card information
            $sql = "SELECT id, user_id, organize_id, card_code, img_path, prefix_name_th, 
                    firstname_th, lastname_th, prefix_name_en, firstname_en, lastname_en, 
                    department_name, section_name, position_name, gender, blood_group, 
                    birthdate, date_issued, date_expiry, signature, is_used, user_type_id FROM core.user_cards where user_id = {$userId};";
            $response['userCards'] = $conn->execute($sql)->fetchAll('assoc');
            
            //user personals information
            $sql = "SELECT firstname_th, lastname_th, firstname_en, lastname_en,gender, moblie_no, phone_no, email, address, blood_group, master_nationality_id, master_country_id, master_province_id FROM core.user_personals WHERE user_id = {$userId};";
            $response['personals'] = $conn->execute($sql)->fetchAll('assoc');
            
            //user profile information
            $sql = "SELECT organize_id, dept_id, section_id, card_code, user_type_id, img_path, is_used, position_org, position_edu, enter_date, address, master_business_type_id, phone_no, master_organization_position_id FROM core.user_profiles WHERE user_id = {$userId};";
            $response['profiles'] = $conn->execute($sql)->fetchAll('assoc');            
            //user entity infomation
            $sql = "SELECT id, user_id, entity_code FROM core.user_entities WHERE user_id = {$userId};";
            $response['entities'] = $conn->execute($sql)->fetchAll('assoc');
        }
        return $response;
    }
}
