<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OAuthServer\Model\Table\ClientsTable as UOauthClientTable;

/**
 * OauthClients Model
 *
 * @property \App\Model\Table\OauthClientsTable|\Cake\ORM\Association\BelongsTo $ParentOauthClients
 * @property \App\Model\Table\OauthClientsTable|\Cake\ORM\Association\HasMany $ChildOauthClients
 *
 * @method \App\Model\Entity\OauthClient get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthClient newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthClient[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthClient|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthClient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthClient[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthClient findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthClients Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 16:59:14
 * @license Pakgon.Ltd
 */
class OauthClientsTable extends UOauthClientTable {

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Common');
    }
//
//    /**
//     *
//     * Default validation rules.
//     *
//     * @author sarawutt.b
//     * @param \Cake\Validation\Validator $validator Validator instance.
//     * @return \Cake\Validation\Validator
//     * @since   2018/05/10 16:59:14
//     * @license Pakgon.Ltd
//     */
//    public function validationDefault(Validator $validator) {
////        $validator
////                ->scalar('id')
////                ->maxLength('id', 20)
////                ->allowEmpty('id', 'create');
////
////        $validator
////                ->scalar('client_secret')
////                ->maxLength('client_secret', 40)
////                ->requirePresence('client_secret', 'create')
////                ->notEmpty('client_secret');
////
////        $validator
////                ->scalar('name')
////                ->maxLength('name', 200)
////                ->requirePresence('name', 'create')
////                ->notEmpty('name');
////
////        $validator
////                ->scalar('redirect_uri')
////                ->maxLength('redirect_uri', 255)
////                ->requirePresence('redirect_uri', 'create')
////                ->notEmpty('redirect_uri');
////
////        $validator
////                ->scalar('parent_model')
////                ->maxLength('parent_model', 200)
////                ->allowEmpty('parent_model');
//
//        return $validator;
//    }
//
//    /**
//     *
//     * Returns a rules checker object that will be used for validating
//     * application integrity.
//     *
//     * @author sarawutt.b
//     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
//     * @return \Cake\ORM\RulesChecker
//     * @since   2018/05/10 16:59:14
//     * @license Pakgon.Ltd
//     */
//    public function buildRules(RulesChecker $rules) {
//        return $rules;
//    }
//
    /**
     *
     * Function findOauthClientsList find for OauthClients in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function findOauthClientsList($paramsOptions = []) {
        $selfOptions = ['keyField' => 'id', 'valueField' => 'name', 'order' => ' name'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }

}
