<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * 
 * Common Logic without table use full for common business logic
 * @author sarawutt.b
 */
class CommonsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->setTable(false);
        $this->addBehavior('Common');
        $this->addBehavior('Common');
    }

    /**
     * Function get getPersonalCardType group list
     * @author sarawutt.b
     * @return array() list of bloog group with empty select
     */
    public function getPersonalCardType($key = 'xxx') {
        $options = ['C' => __('Citizen'), 'P' => __('Passport'), 'O' => __('Other')];
        return (array_key_exists($key, $options)) ? $options[$key] : $this->getEmptySelect() + $options;
    }

    /**
     * Function get gender list
     * @author sarawutt.b
     * @return array() list of gender with empty select
     */
    public function getGenderDDL($key = 'xxx') {
        $options = ['M' => __('Male'), 'F' => __('Female')];
        return (array_key_exists($key, $options)) ? $options[$key] : $this->getEmptySelect() + $options;
    }

    /**
     * Function get bloog group list
     * @author sarawutt.b
     * @return array() list of bloog group with empty select
     */
    public function getBloodGroupDDL($key = 'xxx') {
        $options = ['A' => __('A'), 'B' => __('B'), 'AB' => __('AB'), 'O' => __('O')];
        return (array_key_exists($key, $options)) ? $options[$key] : $this->getEmptySelect() + $options;
    }

    /**
     * 
     * Function dynamic display box class
     * @author  sarawutt.b
     * @param   type $index as integer of index box class
     * @return  string of box class name
     */
    public function bootstrapBoxClass($index = null) {
        $bclass = ['warning', 'success', 'info', 'default', 'primary', 'danger'];
        return array_key_exists($index, $bclass) ? $bclass[$index] : $bclass[$index % count($bclass)];
    }

    /**
     * 
     * Function secure make for decoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function secureDecodeParam($param = null) {
        return (Configure::read('CORE.ENABLED.SECUREMODE')) ? base64_decode($param) : $param;
    }

    /**
     * 
     * Function secure make for encoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function secureEncodeParam($param = null) {
        return (Configure::read('CORE.ENABLED.SECUREMODE')) ? base64_encode($param) : $param;
    }

    /**
     * 
     * Function cleansing all number with currency formate can be saving to the database | The function pass by refereces
     * @author sarawutt.b
     * @param type $params as a array contain all currency 
     * @return array() same params input except clean of the currency
     */
    public function unformatCurrencyParams(&$params = array()) {
        if (!is_array($params)) {
            $params = array($params);
        }
        $patern = '/($฿ ?)?([\d]{1,3},)+(.[\d]{0,2})?( ?$฿)?/';
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $kk => $vv) {
                    if (preg_match($patern, $vv) || (strpos($vv, '฿') !== false) || (strpos($vv, '$') !== false)) {
                        $params[$k][$kk] = $this->unformatCurrency($vv);
                    }
                }
            } else {
                if (preg_match($patern, $v) || (strpos($v, '฿') !== false) || (strpos($v, '$') !== false)) {
                    $params[$k] = $this->unformatCurrency($v);
                }
            }
        }
        return true;
    }

    /**
     * 
     * Unformate currency where receiving param formate before mathematic operate
     * @author  sarawutt.b
     * @param   type $currency as string in currency format
     * @return  double
     */
    public function unformatCurrency($currency = 0, $showPrecision = true) {
        $currency = (strpos($currency, '.') === false) ? $currency . '.00' : $currency;
        $tmp = preg_replace('/[^\d]/', '', $currency);
        return ($showPrecision === true) ? substr($tmp, 0, -2) . "." . substr($tmp, -2) : substr($tmp, 0, -2);
    }

    /**
     * 
     * Function get all of main status
     * @author  sarawutt.b
     * @param   sting $key of main status
     * @return  mix if found params $key return string with lable otherwise return array()
     */
    public function getMainStatus($key = 'xxx') {
        $opt = ['' => __($this->getSelectEmptyMsg()), 'A' => __('Active'), 'I' => __('Inactive')];
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    public function getSex($key = 'xxx') {
        $opt = ['' => __($this->getSelectEmptyMsg()), 'M' => __('Male'), 'F' => __('Fe-male')];
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    public function getUnlocked($key = 'xxx') {
        $opt = ['' => __($this->getSelectEmptyMsg()), '<4' => __('Use'), '>=4' => __('locked')];
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    /*
     * This function for check current user where logedin can use any cnotroller/action
     * @author Sarawutt.b
     * @since 20/10/2013
     * @params Permission Mode True in has permission FALSE in not has
     * @return void
     */

    public function isPermission($isPermission = true, $url = NULL) {
        $bresult = false;
        if (($isPermission === Configure::read('CORE.PERMISSION_MODE')) && ($this->checkAuth())) {
            $current_cotroller = strtolower($this->request->controller);
            $current_action = strtolower($this->request->action);
            $current_role_id = $this->getCurrenSessionRoleId();
            $current_user_id = $this->getCurrenSessionUserId();

            if (!empty($url)) {
                $urls = explode('/', $url);
                if (isset($urls[1]) && !empty($urls[1])) {
                    $current_cotroller = strtolower($urls[1]);
                }
                if (isset($urls[2]) && !empty($urls[2])) {
                    $current_action = strtolower($urls[2]);
                }
            }




            $sql = "SELECT COUNT(acl.id) AS count_permission 
                    FROM system.sys_acls AS acl
                    LEFT JOIN system.sys_actions AS _action ON _action.id=acl.sys_action_id
                    LEFT JOIN system.sys_controllers AS  controll ON controll.id=acl.sys_controller_id
                    WHERE acl.status='A' 
                        AND LOWER(controll.name)='{$current_cotroller}'
                        AND LOWER(_action.name)='{$current_action}'
                        AND (acl.role_id={$current_role_id} OR acl.user_id={$current_user_id});";
//            $result = $this->query($sql);
            $result = $this->connection()->execute($sql)->fetchAll('assoc');
            $bresult = ($result['count_permission'] > 0);
        } else {
            $bresult = true;
        }
        return $bresult;
    }

}
