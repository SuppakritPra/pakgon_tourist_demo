<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthAuthCodeScopes Model
 *
 * @property \App\Model\Table\ScopesTable|\Cake\ORM\Association\BelongsTo $Scopes
 *
 * @method \App\Model\Entity\OauthAuthCodeScope get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthAuthCodeScope newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthAuthCodeScope[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthAuthCodeScope|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthAuthCodeScope patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAuthCodeScope[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthAuthCodeScope findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthAuthCodeScopes Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:03:17
 * @license Pakgon.Ltd
 */
class OauthAuthCodeScopesTable extends Table
{

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oauth_auth_code_scopes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Common');
        $this->belongsTo('Scopes', [
            'foreignKey' => 'scope_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('auth_code')
            ->maxLength('auth_code', 40)
            ->requirePresence('auth_code', 'create')
            ->notEmpty('auth_code');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['scope_id'], 'Scopes'));

        return $rules;
    }
    
    /**
     *
     * Function findOauthAuthCodeScopesList find for OauthAuthCodeScopes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function findOauthAuthCodeScopesList($paramsOptions = []){
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }
}
