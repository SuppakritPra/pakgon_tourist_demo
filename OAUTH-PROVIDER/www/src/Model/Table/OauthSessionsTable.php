<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OauthSessions Model
 *
 * @property \App\Model\Table\OwnersTable|\Cake\ORM\Association\BelongsTo $Owners
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 *
 * @method \App\Model\Entity\OauthSession get($primaryKey, $options = [])
 * @method \App\Model\Entity\OauthSession newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OauthSession[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OauthSession|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OauthSession patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OauthSession[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OauthSession findOrCreate($search, callable $callback = null, $options = [])
 */

/**
 *
 * OauthSessions Table.
 *
 * @author  sarawutt.b 
 * @since   2018/05/10 17:07:15
 * @license Pakgon.Ltd
 */
class OauthSessionsTable extends Table
{

    /**
     *
     * Initialize method
     *
     * @author sarawutt.b
     * @param array $config The configuration for the Table.
     * @return void
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oauth_sessions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Common');
        $this->belongsTo('Owners', [
            'foreignKey' => 'owner_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     *
     * Default validation rules.
     *
     * @author sarawutt.b
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('owner_model')
            ->maxLength('owner_model', 200)
            ->requirePresence('owner_model', 'create')
            ->notEmpty('owner_model');

        $validator
            ->scalar('client_redirect_uri')
            ->maxLength('client_redirect_uri', 200)
            ->allowEmpty('client_redirect_uri');

        return $validator;
    }

    /**
     *
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @author sarawutt.b
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['owner_id'], 'Owners'));
        $rules->add($rules->existsIn(['client_id'], 'Clients'));

        return $rules;
    }
    
    /**
     *
     * Function findOauthSessionsList find for OauthSessions in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function findOauthSessionsList($paramsOptions = []){
        $selfOptions = ['keyField' => 'id', 'valueField' => 'id', 'order' => ' id'];
        $options = array_merge($selfOptions, $paramsOptions);
        return $this->getEmptySelect() + $this->find('list', $options)->toArray();
    }
}
