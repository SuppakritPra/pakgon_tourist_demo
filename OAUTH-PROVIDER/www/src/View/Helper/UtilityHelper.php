<?php

/**
  | ------------------------------------------------------------------------------------------------------------------
  | Utility Helper.
  | @author  sarawutt.b
  | @since   2018-03-30
  | @license Pakgon.Ltd
  |------------------------------------------------------------------------------------------------------------------------
 */

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class UtilityHelper extends Helper {

    public $helpers = array('Html', 'Form', 'Session', 'Bootstrap');
    private $_projectStyle = array('NL' => 'default', 'DL' => 'info', 'WA' => 'warning', 'WE0' => 'warning', 'WE' => 'warning', 'AL0' => 'warning', 'RN' => 'info', 'AL' => 'success', 'RL0' => 'danger', 'RL' => 'danger', 'CL' => 'danger', 'DE' => 'danger');
    private $_thai_month_arr = array(
        "0" => "",
        "1" => "มกราคม",
        "2" => "กุมภาพันธ์",
        "3" => "มีนาคม",
        "4" => "เมษายน",
        "5" => "พฤษภาคม",
        "6" => "มิถุนายน",
        "7" => "กรกฎาคม",
        "8" => "สิงหาคม",
        "9" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    );
    private $_thai_month_arr_short = array(
        "0" => "",
        "1" => "ม.ค.",
        "2" => "ก.พ.",
        "3" => "มี.ค.",
        "4" => "เม.ย.",
        "5" => "พ.ค.",
        "6" => "มิ.ย.",
        "7" => "ก.ค.",
        "8" => "ส.ค.",
        "9" => "ก.ย.",
        "10" => "ต.ค.",
        "11" => "พ.ย.",
        "12" => "ธ.ค."
    );

    public function dashEmpty($param = null) {
        return $this->emptyFormat($param, '-');
    }

    public function blankEmpty($param = null) {
        return $this->emptyFormat($param, '');
    }

    protected function emptyFormat($param = null, $format = '-') {
        $tmp = trim($param);
        return empty($tmp) ? $format : $tmp;
    }

    public function getCurrentLanguage() {
        return $this->readSession('SessionLanguage', 'tha');
    }

    public function readSession($name, $default = null) {
        return $this->request->session()->read($name, $default);
    }

    /**
     * 
     * Function get table class registry
     * @author sarawutt.b
     * @param type $tableName as a string name of loaded menu
     * @return object
     */
    protected function getTable($tableName = null) {
        return TableRegistry::get($tableName);
    }

    /**
     * 
     * Find Role infomation by role ID
     * @author  sarawutt.b
     * @param   type $id as integer of role id
     * @return  array()
     */
    public function getRoleNameById($id = null) {
        return $this->getTable('Roles')->getRoleNameById($id);
    }

    /**
     *
     * Function findNationalitiesList find for Nationalities in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:18:52
     * @license Pakgon.Ltd
     */
    public function findNationalitiesList($paramsOptions = []) {
        return $this->getTable('Nationalities')->{__FUNCTION__}($paramsOptions);
    }

    /**
     *
     * Function findEthnicitiesList find for Ethnicities in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:19:28
     * @license Pakgon.Ltd
     */
    public function findEthnicitiesList($paramsOptions = []) {
        return $this->getTable('Ethnicities')->{__FUNCTION__}($paramsOptions);
    }

    /**
     * Function get getPersonalCardType group list
     * @author sarawutt.b
     * @return array() list of bloog group with empty select
     */
    public function getPersonalCardType($key = 'xxx') {
        return $this->getTable('Commons')->{__FUNCTION__}($key);
    }

    /**
     *
     * Function findNamePrefixesList find for NamePrefixes in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:18:11
     * @license Pakgon.Ltd
     */
    public function findNamePrefixesList($paramsOptions = []) {
        return $this->getTable('NamePrefixes')->{__FUNCTION__}($paramsOptions);
    }

    /**
     *
     * Function findPositionsList find for Positions in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:11:51
     * @license Pakgon.Ltd
     */
    public function findPositionsList($paramsOptions = []) {
        return $this->getTable('Positions')->{__FUNCTION__}($paramsOptions);
    }

    /**
     *
     * Function findRolesList find for Roles in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:12:11
     * @license Pakgon.Ltd
     */
    public function findRolesList($paramsOptions = []) {
        return $this->getTable('Roles')->{__FUNCTION__}($paramsOptions);
    }

    /**
     *
     * Function findUsersTypeList find for Users Type in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:08:03
     * @license Pakgon.Ltd
     */
    public function findUsersTypeList($id = 'xxx') {
        return $this->getTable('Users')->{__FUNCTION__}($id);
    }

    /**
     * Function get gender list
     * @author sarawutt.b
     * @return array() list of gender with empty select
     */
    public function getGenderDDL($key = 'xxx') {
        return $this->getTable('Commons')->{__FUNCTION__}($key);
    }

    /**
     *
     * Function findCustomerTypeList find for customer Type in select list
     *
     * @author sarawutt.b
     * @return array with empty select
     * @since   2018/04/20 18:08:03
     * @license Pakgon.Ltd
     */
    public function findCustomerTypeList($id = 'xxx') {
        return $this->getTable('Users')->{__FUNCTION__}($id = 'xxx');
    }

    /**
     * 
     * Function make for bootstrap label
     * @author  sarawutt.b
     * @param   type $message as string of the content where you want wrap to the label
     * @param   type $class as string bootstrap class
     * @return  string
     */
    public function bootstrapLabel($message = '', $class = 'default') {
        return $this->Bootstrap->badge($message, $class, 'label');
    }

    /**
     * 
     * Function get for main system status
     * @author  sarawutt.b
     * @param   type $key as character of status key
     * @param   type $makeShow as boolean if true then make style otherwise pure status content display without style
     * @return  string
     */
    public function getMainStatus($key = 'xxx', $makeShow = false) {
        $stype = ['N' => 'active', 'A' => 'success', 'I' => 'danger', 'D' => 'warning'];
        return ($makeShow === true) ? $this->bootstrapLabel($this->getTable('Commons')->getMainStatus($key), $stype[$key]) : $this->getTable('Commons')->getMainStatus($key);
    }

    /**
     * 
     * Function get display for is_used 
     * @author  sarawutt.b
     * @param   type $param boolean of is used value
     * @return  mix
     */
    public function getIsUsed($param = false) {
        $styles = [true => 'success', false => 'danger'];
        $options = [true => __('Yes'), false => __('No')];
        return array_key_exists((string) $param, $options) ? $this->bootstrapLabel($options[$param], $styles[$param]) : $options;
    }

    /**
     * 
     * Function fine count menu used for count order indexing for display
     * @author sarawutt.b
     * @return integer
     */
    public function countMenu() {
        return $this->getTable('Menus')->{__FUNCTION__}();
    }

    /**
     * 
     * Function get get users full name
     * @author  sarawutt.b
     * @param   type $id
     * @param   type $paramsOptions
     * @return  type
     */
    public function getUserFullnameById($id = null, $paramsOptions = []) {
        return $this->getTable('Users')->{__FUNCTION__}($id);
    }

    /**
     * Find list of menus where the menu is parent menu 
     * @author  sarawutt.b
     * @param   integer of menu id
     * @since   2016/05/11
     * @return  array()
     */
    public function findListParentMenu($id = null) {
        return TableRegistry::get('Menus')->{__FUNCTION__}($id);
    }

    /**
     * 
     * Function get for current session user authentication full name
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user full name
     */
    public function getAuthFullname() {
        return $this->readAuth('Auth.User.first_name') . ' ' . $this->readAuth('Auth.User.last_name');
    }

    /**
     * 
     * Function get for current session user authentication user id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    public function getAuthUserId() {
        return $this->readAuth('Auth.User.id');
    }

    /**
     * 
     * Function get for current session user authentication role id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    public function getAuthUserRoleId() {
        return $this->readAuth('Auth.User.role_id');
    }

    /**
     * 
     * Function get for current session with user authentication
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication session info
     */
    public function readAuth($name = null) {
        return $this->request->session()->read($name);
    }

    /**
     * 
     * Function display date time format
     * @author  sarawutt.b
     * @param   type $date as string date or date time
     * @return  string in date format only
     */
    public function dateISO($date = null) { // Y-m-d
        return (is_null($date)) ? '-' : $this->getDynamicYear($date) . date('-m-d', strtotime($date));
    }

    /**
     * 
     * Function display date format
     * @author  sarawutt.b
     * @param   type $datetime as string date or date time
     * @return  string in date and time format
     */
    public function dateTimeISO($datetime = null) { // Y-m-d H:i:s
//        debug($datetime->i18nFormat([\IntlDateFormatter::FULL, \IntlDateFormatter::SHORT]));
        //debug($datetime->modify('+543 years')->i18nFormat('yyyy/MM/dd HH:mm:ss'));
        //debug($datetime->modify('+543 years')->i18nFormat(\IntlDateFormatter::FULL,'Asia/Bangkok','th-TH'));
        //exit;
        //return (is_null($datetime)) ? '-' : $this->getDynamicYear($datetime) . date('-m-d H:i:s', strtotime($datetime));
        return $datetime->modify('+543 years')->i18nFormat('yyyy/MM/dd HH:mm:ss');
    }

    /**
     * 
     * Function convert date or date and time to current locale
     * @author  sarawutt.b
     * @param   type $dateTime as string date or date time
     * @return  string in year format BU/BCC year
     */
    private function getDynamicYear($dateTime = null) {
        return ($this->getCurrentLanguage() == 'tha') ? date('Y', strtotime($dateTime)) + 543 : date('Y', strtotime($dateTime));
    }

    /**
     * 
     * Function display for thai date time
     * @param type $param as a string of date time
     * @param type $format as a string display options posible value DT|DTS|DSN|DS|DFM
     */
    public function displayThaiDate($param, $format = 'DS') {
        $format = strtoupper(trim($format));
        switch ($format) {
            case 'DT' :
                return $this->thaiDateTime($param);
                break;
            case 'DTS' :
                return $this->thaiDateTimeShort($param);
                break;
            case 'DSN' :
                return $this->thaiDateShortDashNumber($param);
                break;
            case 'DS' :
                return $this->thaiDateShort($param);
                break;
            case 'DFM' :
                return $this->thaiDateFullMonth($param);
                break;
            default :
                return '-';
        }
    }

    public function thaiDateTime($time) {   // 19 ธันวาคม 2556 เวลา 10:10:43
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= " " . $this->_thai_month_arr[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        $thai_date_return .= " เวลา " . date("H:i:s", $time);
        return $thai_date_return;
    }

    public function thaiDateTimeShort($time) {   // 19  ธ.ค. 2556 10:10:4
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= "&nbsp;&nbsp;" . $this->_thai_month_arr_short[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        $thai_date_return .= " " . date("H:i:s", $time);
        return $thai_date_return;
    }

    public function thaiDateShortDashNumber($time) {   // 19-12-56
        $time = strtotime($time);
        $thai_date_return = date("d", $time);
        $thai_date_return .= "-" . date("m", $time);
        $thai_date_return .= "-" . substr((date("Y", $time) + 543), -2);
        return $thai_date_return;
    }

    public function thaiDateShort($time) {   // 19  ธ.ค. 2556
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= " " . $this->_thai_month_arr_short[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        return $thai_date_return;
    }

    public function thaiDateFullMonth($time) {   // 19 ธันวาคม 2556
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= " " . $this->_thai_month_arr[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        return $thai_date_return;
    }

    /**
     * 
     * Function currency format display the format of the currency
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @return  string number in currency format
     */
    public function currencyFormat($currentcy = null) {
        return $this->_numberFormat($currentcy, 2, Configure::read('Zicure.Currency'));
    }

    /**
     * 
     * Function percent of the currency format display the format of the currency
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @return  string number in currency format
     */
    public function percentFormat($currentcy = null) {
        return $this->_numberFormat($currentcy, 2, '%');
    }

    /**
     * 
     * Function for display number in format number
     * @param   type $number as number or integer or decimal
     * @since   2016-10-10
     * @return  string number in format thousan separated
     */
    public function numberFormat($number = null, $precition = 2) {
        return $this->_numberFormat($number, $precition);
    }

    /**
     * 
     * Function currency format display for number in format
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @param   type $currencySymbol as string currency symbol
     * @param   type $precition as integer of amount precition
     * @since   2016-10-10
     * @return  string
     */
    private function _numberFormat($currentcy = null, $precition = 2, $currencySymbol = null) {
        $currentcy = (is_null($currentcy) || empty($currentcy)) ? 0 : $currentcy;
        return number_format($currentcy, $precition, '.', ',') . ' ' . $currencySymbol;
    }

    public function num2wordsThai($num) {
        $num = str_replace(",", "", $num);
        $num_decimal = explode(".", $num);
        $num = $num_decimal[0];
        $returnNumWord = '';
        $lenNumber = strlen($num);
        $lenNumber2 = $lenNumber - 1;
        $kaGroup = array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
        $kaDigit = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
        $kaDigitDecimal = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
        $ii = 0;
        for ($i = $lenNumber2; $i >= 0; $i--) {
            $kaNumWord[$i] = substr($num, $ii, 1);
            $ii++;
        }
        $ii = 0;
        for ($i = $lenNumber2; $i >= 0; $i--) {
            if (($kaNumWord[$i] == 2 && $i == 1) || ($kaNumWord[$i] == 2 && $i == 7)) {
                $kaDigit[$kaNumWord[$i]] = "ยี่";
            } else {
                if ($kaNumWord[$i] == 2) {
                    $kaDigit[$kaNumWord[$i]] = "สอง";
                }
                if (($kaNumWord[$i] == 1 && $i <= 2 && $i == 0) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 6)) {
                    if ($kaNumWord[$i + 1] == 0) {
                        $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
                    } else {
                        $kaDigit[$kaNumWord[$i]] = "เอ็ด";
                    }
                } elseif (($kaNumWord[$i] == 1 && $i <= 2 && $i == 1) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 7)) {
                    $kaDigit[$kaNumWord[$i]] = "";
                } else {
                    if ($kaNumWord[$i] == 1) {
                        $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
                    }
                }
            }
            if ($kaNumWord[$i] == 0) {
                if ($i != 6) {
                    $kaGroup[$i] = "";
                }
            }
            $kaNumWord[$i] = substr($num, $ii, 1);
            $ii++;
            $returnNumWord .= $kaDigit[$kaNumWord[$i]] . $kaGroup[$i];
        }
        if (isset($num_decimal[1])) {
            //$returnNumWord.="จุด";
            $returnNumWord .= " ";
            for ($i = 0; $i < strlen($num_decimal[1]); $i++) {
                $returnNumWord .= $kaDigitDecimal[substr($num_decimal[1], $i, 1)];
            }
            $returnNumWord .= " สตางค์";
        }
        return $returnNumWord;
    }

    /**
     * 
     * Function make for display user mobile phone
     * @author  sarawutt.b
     * @param   $phoneNumber string of phone number (0199991100)
     * @return  string of phone number format
     */
    public function displayMobilePhoneNumber($phoneNumber = '') {
        return ($phoneNumber) ? substr($phoneNumber, 0, 2) . '-' . substr($phoneNumber, 2, 4) . '-' . substr($phoneNumber, 6) : '-';
    }

    /**
     * 
     * Function make for display user phone number
     * @author  sarawutt.b
     * @param   $phoneNumber string of phone number (0299991100)
     * @return  string of phone number format
     */
    public function displayPhoneNumber($phoneNumber = '') {
        return ($phoneNumber) ? substr($phoneNumber, 0, 3) . '-' . substr($phoneNumber, 2, 3) . '-' . substr($phoneNumber, 5) : '-';
    }

    /**
     * 
     * Function make for display the citizen number
     * @author sarawutt.b
     * @param   $citizenNumber as string of the customer citizen number ex. 1-1234-12345-12-1
     * @return  string of citizen number
     */
    public function displayCitizenNo($citizenNumber = '') {
        return (($citizenNumber) && (strlen($citizenNumber) === 13)) ? substr($citizenNumber, 0, 1) . '-' . substr($citizenNumber, 1, 4) . '-' . substr($citizenNumber, 5, 5) . '-' . substr($citizenNumber, 10, 2) . '-' . substr($citizenNumber, 12) : $citizenNumber;
    }

    public function checkEmpty($params = null) {
        return empty($params) ? '-' : $params;
    }

}
