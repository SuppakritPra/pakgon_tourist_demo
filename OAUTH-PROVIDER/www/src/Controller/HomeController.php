<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Home Controller
 * @author  pakgon.Ltd
 * @since   2018-02-07 08:17:39
 * @license Pakgon.Ltd
 */
 
class HomeController extends AppController {

    /**
     *
     * Home fore any loged users
     * @author  pakgon.Ltd
     * @return \Cake\Http\Response|void
     * @since   2018-02-07 08:17:39
     * @license Pakgon.Ltd
     */
    public function index() {
        
    }
}
