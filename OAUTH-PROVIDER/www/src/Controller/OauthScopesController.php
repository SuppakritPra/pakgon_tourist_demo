<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthScopes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthScopesTable $OauthScopes
 * @method \App\Model\Entity\OauthScope[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:04:14
 * @license Pakgon.Ltd.
 */
class OauthScopesController extends AppController {

    /**
     *
     * Index method make list for Oauth Scope.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }

        $conditions = [];
        $order = ['OauthScopes.modified' => 'asc', 'OauthScopes.created' => 'asc', 'OauthScopes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());

            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthScopes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthScopes.name_eng) ILIKE ' => "%{$name}%"];
            }

            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthScopes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthScopes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthScopes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthScopes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthScopes.created) <= ' => $this->request->data['dateTo']);
            }
        }

        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $oauthScopes = $this->paginate($this->OauthScopes);
        $this->set(compact('oauthScopes'));
        $this->set('_serialize', ['oauthScopes']);
    }

    /**
     *
     * View method make for view information of Oauth Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Scope id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $oauthScope = $this->OauthScopes->get($id, [
            'contain' => []
        ]);
        $this->set('oauthScope', $oauthScope);
        $this->set('_serialize', ['oauthScope']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Scope.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthScope = $this->OauthScopes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthScope = $this->OauthScopes->patchEntity($oauthScope, $this->request->getData());
            if ($this->OauthScopes->save($oauthScope)) {
                $this->Flash->success(__('The oauth scope has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth scope could not be saved. Please, try again.'));
        }
        $this->set(compact('oauthScope'));
        $this->set('_serialize', ['oauthScope']);
    }

    /**
     *
     * Edit method make for update Oauth Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Scope id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $oauthScope = $this->OauthScopes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthScope = $this->OauthScopes->patchEntity($oauthScope, $this->request->getData());
            if ($this->OauthScopes->save($oauthScope)) {
                $this->Flash->success(__('The oauth scope has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth scope could not be update. Please, try again.'));
        }
        $this->set(compact('oauthScope'));
        $this->set('_serialize', ['oauthScope']);
    }

    /**
     *
     * Delete method make for delete record of Oauth Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Scope id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:04:14
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $oauthScope = $this->OauthScopes->get($id);
        $respond = [];

        if ($this->OauthScopes->delete($oauthScope)) {
            $respond = $this->buildRequestRespond(__('The oauth scope has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth scope could not be deleted. Please, try again.'), 'ERROR');
        }

        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }

}
