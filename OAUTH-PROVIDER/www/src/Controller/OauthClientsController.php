<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthClients Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthClientsTable $OauthClients
 * @method \App\Model\Entity\OauthClient[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 16:59:14
 * @license Pakgon.Ltd.
 */
class OauthClientsController extends AppController
{

    /**
     *
     * Index method make list for Oauth Client.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthClients.modified' => 'asc', 'OauthClients.created' => 'asc', 'OauthClients.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthClients.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthClients.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthClients.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthClients.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthClients.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthClients.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthClients.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
        ];

        $oauthClients = $this->paginate($this->OauthClients);
        $this->set(compact('oauthClients'));
        $this->set('_serialize', ['oauthClients']);
    }

    /**
     *
     * View method make for view information of Oauth Client.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Client id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthClients->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth client, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthClient = $this->OauthClients->get($id);
        $this->set('oauthClient', $oauthClient);
        $this->set('_serialize', ['oauthClient']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Client.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthClient = $this->OauthClients->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthClient = $this->OauthClients->patchEntity($oauthClient, $this->request->getData());
            if ($this->OauthClients->save($oauthClient)) {
                $this->Flash->success(__('The oauth client has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth client could not be saved. Please, try again.'));
        }
        $this->set(compact('oauthClient'));
        $this->set('_serialize', ['oauthClient']);
    }

    /**
     *
     * Edit method make for update Oauth Client.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Client id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthClients->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth client, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthClient = $this->OauthClients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthClient = $this->OauthClients->patchEntity($oauthClient, $this->request->getData());
            if ($this->OauthClients->save($oauthClient)) {
                $this->Flash->success(__('The oauth client has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth client could not be update. Please, try again.'));
        }
        $this->set(compact('oauthClient'));
        $this->set('_serialize', ['oauthClient']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Client.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Client id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 16:59:14
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthClients->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth client, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthClient = $this->OauthClients->get($id);
        $respond = [];
        
        if ($this->OauthClients->delete($oauthClient)) {
            $respond = $this->buildRequestRespond(__('The oauth client has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth client could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
