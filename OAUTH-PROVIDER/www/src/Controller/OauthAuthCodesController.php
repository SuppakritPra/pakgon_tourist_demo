<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthAuthCodes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthAuthCodesTable $OauthAuthCodes
 * @method \App\Model\Entity\OauthAuthCode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:03:28
 * @license Pakgon.Ltd.
 */
class OauthAuthCodesController extends AppController
{

    /**
     *
     * Index method make list for Oauth Auth Code.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthAuthCodes.modified' => 'asc', 'OauthAuthCodes.created' => 'asc', 'OauthAuthCodes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthAuthCodes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthAuthCodes.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthAuthCodes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAuthCodes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAuthCodes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAuthCodes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthAuthCodes.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Sessions']
        ];

        $oauthAuthCodes = $this->paginate($this->OauthAuthCodes);
        $this->set(compact('oauthAuthCodes'));
        $this->set('_serialize', ['oauthAuthCodes']);
    }

    /**
     *
     * View method make for view information of Oauth Auth Code.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Auth Code id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthAuthCodes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth auth code, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAuthCode = $this->OauthAuthCodes->get($id, [
            'contain' => ['Sessions']
        ]);
        $this->set('oauthAuthCode', $oauthAuthCode);
        $this->set('_serialize', ['oauthAuthCode']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Auth Code.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthAuthCode = $this->OauthAuthCodes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthAuthCode = $this->OauthAuthCodes->patchEntity($oauthAuthCode, $this->request->getData());
            if ($this->OauthAuthCodes->save($oauthAuthCode)) {
                $this->Flash->success(__('The oauth auth code has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth auth code could not be saved. Please, try again.'));
        }
        $sessions = $this->OauthAuthCodes->Sessions->find('list', ['limit' => 200]);
        $this->set(compact('oauthAuthCode', 'sessions'));
        $this->set('_serialize', ['oauthAuthCode']);
    }

    /**
     *
     * Edit method make for update Oauth Auth Code.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Auth Code id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthAuthCodes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth auth code, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAuthCode = $this->OauthAuthCodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthAuthCode = $this->OauthAuthCodes->patchEntity($oauthAuthCode, $this->request->getData());
            if ($this->OauthAuthCodes->save($oauthAuthCode)) {
                $this->Flash->success(__('The oauth auth code has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth auth code could not be update. Please, try again.'));
        }
        $sessions = $this->OauthAuthCodes->Sessions->find('list', ['limit' => 200]);
        $this->set(compact('oauthAuthCode', 'sessions'));
        $this->set('_serialize', ['oauthAuthCode']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Auth Code.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Auth Code id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:03:28
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthAuthCodes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth auth code, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAuthCode = $this->OauthAuthCodes->get($id);
        $respond = [];
        
        if ($this->OauthAuthCodes->delete($oauthAuthCode)) {
            $respond = $this->buildRequestRespond(__('The oauth auth code has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth auth code could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
