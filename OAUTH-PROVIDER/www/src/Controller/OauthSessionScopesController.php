<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthSessionScopes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthSessionScopesTable $OauthSessionScopes
 * @method \App\Model\Entity\OauthSessionScope[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:04:33
 * @license Pakgon.Ltd.
 */
class OauthSessionScopesController extends AppController
{

    /**
     *
     * Index method make list for Oauth Session Scope.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthSessionScopes.modified' => 'asc', 'OauthSessionScopes.created' => 'asc', 'OauthSessionScopes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthSessionScopes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthSessionScopes.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthSessionScopes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthSessionScopes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthSessionScopes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthSessionScopes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthSessionScopes.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Sessions', 'Scopes']
        ];

        $oauthSessionScopes = $this->paginate($this->OauthSessionScopes);
        $this->set(compact('oauthSessionScopes'));
        $this->set('_serialize', ['oauthSessionScopes']);
    }

    /**
     *
     * View method make for view information of Oauth Session Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Session Scope id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthSessionScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth session scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthSessionScope = $this->OauthSessionScopes->get($id, [
            'contain' => ['Sessions', 'Scopes']
        ]);
        $this->set('oauthSessionScope', $oauthSessionScope);
        $this->set('_serialize', ['oauthSessionScope']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Session Scope.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthSessionScope = $this->OauthSessionScopes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthSessionScope = $this->OauthSessionScopes->patchEntity($oauthSessionScope, $this->request->getData());
            if ($this->OauthSessionScopes->save($oauthSessionScope)) {
                $this->Flash->success(__('The oauth session scope has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth session scope could not be saved. Please, try again.'));
        }
        $sessions = $this->OauthSessionScopes->Sessions->find('list', ['limit' => 200]);
        $scopes = $this->OauthSessionScopes->Scopes->find('list', ['limit' => 200]);
        $this->set(compact('oauthSessionScope', 'sessions', 'scopes'));
        $this->set('_serialize', ['oauthSessionScope']);
    }

    /**
     *
     * Edit method make for update Oauth Session Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Session Scope id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthSessionScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth session scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthSessionScope = $this->OauthSessionScopes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthSessionScope = $this->OauthSessionScopes->patchEntity($oauthSessionScope, $this->request->getData());
            if ($this->OauthSessionScopes->save($oauthSessionScope)) {
                $this->Flash->success(__('The oauth session scope has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth session scope could not be update. Please, try again.'));
        }
        $sessions = $this->OauthSessionScopes->Sessions->find('list', ['limit' => 200]);
        $scopes = $this->OauthSessionScopes->Scopes->find('list', ['limit' => 200]);
        $this->set(compact('oauthSessionScope', 'sessions', 'scopes'));
        $this->set('_serialize', ['oauthSessionScope']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Session Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Session Scope id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:04:33
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthSessionScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth session scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthSessionScope = $this->OauthSessionScopes->get($id);
        $respond = [];
        
        if ($this->OauthSessionScopes->delete($oauthSessionScope)) {
            $respond = $this->buildRequestRespond(__('The oauth session scope has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth session scope could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
