<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthRefreshTokens Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthRefreshTokensTable $OauthRefreshTokens
 * @method \App\Model\Entity\OauthRefreshToken[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:03:51
 * @license Pakgon.Ltd.
 */
class OauthRefreshTokensController extends AppController
{

    /**
     *
     * Index method make list for Oauth Refresh Token.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:03:51
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthRefreshTokens.modified' => 'asc', 'OauthRefreshTokens.created' => 'asc', 'OauthRefreshTokens.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthRefreshTokens.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthRefreshTokens.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthRefreshTokens.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthRefreshTokens.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthRefreshTokens.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthRefreshTokens.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthRefreshTokens.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $oauthRefreshTokens = $this->paginate($this->OauthRefreshTokens);
        $this->set(compact('oauthRefreshTokens'));
        $this->set('_serialize', ['oauthRefreshTokens']);
    }

    /**
     *
     * View method make for view information of Oauth Refresh Token.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Refresh Token id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:03:51
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthRefreshTokens->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth refresh token, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthRefreshToken = $this->OauthRefreshTokens->get($id, [
            'contain' => []
        ]);
        $this->set('oauthRefreshToken', $oauthRefreshToken);
        $this->set('_serialize', ['oauthRefreshToken']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Refresh Token.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:03:51
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthRefreshToken = $this->OauthRefreshTokens->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthRefreshToken = $this->OauthRefreshTokens->patchEntity($oauthRefreshToken, $this->request->getData());
            if ($this->OauthRefreshTokens->save($oauthRefreshToken)) {
                $this->Flash->success(__('The oauth refresh token has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth refresh token could not be saved. Please, try again.'));
        }
        $this->set(compact('oauthRefreshToken'));
        $this->set('_serialize', ['oauthRefreshToken']);
    }

    /**
     *
     * Edit method make for update Oauth Refresh Token.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Refresh Token id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:03:51
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthRefreshTokens->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth refresh token, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthRefreshToken = $this->OauthRefreshTokens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthRefreshToken = $this->OauthRefreshTokens->patchEntity($oauthRefreshToken, $this->request->getData());
            if ($this->OauthRefreshTokens->save($oauthRefreshToken)) {
                $this->Flash->success(__('The oauth refresh token has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth refresh token could not be update. Please, try again.'));
        }
        $this->set(compact('oauthRefreshToken'));
        $this->set('_serialize', ['oauthRefreshToken']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Refresh Token.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Refresh Token id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:03:51
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthRefreshTokens->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth refresh token, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthRefreshToken = $this->OauthRefreshTokens->get($id);
        $respond = [];
        
        if ($this->OauthRefreshTokens->delete($oauthRefreshToken)) {
            $respond = $this->buildRequestRespond(__('The oauth refresh token has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth refresh token could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
