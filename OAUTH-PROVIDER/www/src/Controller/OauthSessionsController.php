<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthSessions Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthSessionsTable $OauthSessions
 * @method \App\Model\Entity\OauthSession[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:07:15
 * @license Pakgon.Ltd.
 */
class OauthSessionsController extends AppController
{

    /**
     *
     * Index method make list for Oauth Session.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthSessions.modified' => 'asc', 'OauthSessions.created' => 'asc', 'OauthSessions.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthSessions.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthSessions.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthSessions.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthSessions.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthSessions.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthSessions.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthSessions.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Owners', 'Clients']
        ];

        $oauthSessions = $this->paginate($this->OauthSessions);
        $this->set(compact('oauthSessions'));
        $this->set('_serialize', ['oauthSessions']);
    }

    /**
     *
     * View method make for view information of Oauth Session.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Session id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthSessions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth session, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthSession = $this->OauthSessions->get($id, [
            'contain' => ['Owners', 'Clients']
        ]);
        $this->set('oauthSession', $oauthSession);
        $this->set('_serialize', ['oauthSession']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Session.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthSession = $this->OauthSessions->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthSession = $this->OauthSessions->patchEntity($oauthSession, $this->request->getData());
            if ($this->OauthSessions->save($oauthSession)) {
                $this->Flash->success(__('The oauth session has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth session could not be saved. Please, try again.'));
        }
        $owners = $this->OauthSessions->Owners->find('list', ['limit' => 200]);
        $clients = $this->OauthSessions->Clients->find('list', ['limit' => 200]);
        $this->set(compact('oauthSession', 'owners', 'clients'));
        $this->set('_serialize', ['oauthSession']);
    }

    /**
     *
     * Edit method make for update Oauth Session.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Session id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthSessions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth session, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthSession = $this->OauthSessions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthSession = $this->OauthSessions->patchEntity($oauthSession, $this->request->getData());
            if ($this->OauthSessions->save($oauthSession)) {
                $this->Flash->success(__('The oauth session has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth session could not be update. Please, try again.'));
        }
        $owners = $this->OauthSessions->Owners->find('list', ['limit' => 200]);
        $clients = $this->OauthSessions->Clients->find('list', ['limit' => 200]);
        $this->set(compact('oauthSession', 'owners', 'clients'));
        $this->set('_serialize', ['oauthSession']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Session.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Session id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:07:15
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthSessions->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth session, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthSession = $this->OauthSessions->get($id);
        $respond = [];
        
        if ($this->OauthSessions->delete($oauthSession)) {
            $respond = $this->buildRequestRespond(__('The oauth session has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth session could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
