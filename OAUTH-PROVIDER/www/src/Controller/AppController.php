<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    protected $respondCodes = ['OK' => '200', 'ERROR' => '500'];
    protected $selectEmptyMsg = '---- please select ----';

    /**
     *
     * Documentation 
     * Link : https://holt59.github.io/cakephp3-bootstrap-helpers/
     * Git : https://github.com/Holt59/cakephp3-bootstrap-helpers
     * @var type 
     */
    public $helpers = [
        'Utility',
        'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
        'Html' => ['className' => 'Bootstrap.Html'],
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Navbar' => ['className' => 'Bootstrap.Navbar'],
        'Paginator' => ['template' => 'paginator-template'],
        'Panel' => ['className' => 'Bootstrap.Panel']
    ];
    protected $serverName = null;
    protected $serverPort = null;
    protected $serverProtocal = NULL;
    protected $sessionUserId = null;
    protected $sessionRoleId = null;
    protected $L10n = null;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        I18n::locale('th_TH');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Utility');

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');

        $this->loadComponent('Auth', [
            'loginRedirect' => ['controller' => 'users', 'action' => 'index'],
            'logoutRedirect' => ['controller' => 'users', 'action' => 'login'],
            'loginAction' => ['controller' => 'users', 'action' => 'login'],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ],
                'OAuthServer.OAuth'
            ],
            'authorize' => ['Controller'],
            'unauthorizedRedirect' => $this->referer()
        ]);
        $this->loadModel('Commons');
    }

    /**
     * 
     * Function trigger before filter process
     * @author sarawutt.b
     * @param Event $event
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $this->Auth->allow(['login', 'logout']);
        $this->viewBuilder()->setTheme('AdminLTE204');
        //$this->viewBuilder()->setTheme('PakgonConnect');
        /**
         * 
         * Set application language this can be Thai|English
         * @author sarawutt.b
         * @since 2018-02-28
         * @return void
         */
        if ($this->request->session()->check('SessionLanguage') == false) {
            $this->request->session()->write('SessionLanguage', 'tha');
        }

        $this->serverName = $_SERVER['SERVER_NAME'];
        $this->serverPort = $_SERVER['SERVER_PORT'];
        $this->serverProtocal = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $this->sessionRoleId = $this->getAuthUserRoleId();
        $this->sessionUserId = $this->getAuthUserId();
        //$this->set('menuListHtml', $this->getDynamicMenu());
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event) {
        // Note: These defaults are just to get started quickly with development
        // and should not be used in production. You should instead set "_serialize"
        // in each action as required.
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * 
     * Function check authorize
     * @author sarawutt.b
     * @param type $user
     * @return boolean
     */
    public function isAuthorized($user) {
        return true;
    }

    /**
     *
     * Function get Dinamic Menu read for dynamic menu where current domain, port, role, user
     * @author  sarawutt.b
     * @param   $pageLevel as a integer of page level posible value 1 | 2 
     * @return  string HTML of menu list
     * @return  array()
     */
    public function getDynamicMenu($pageLevel = 1) {
        $_FULL_URL = $this->serverProtocal . $this->serverName . ":" . $this->serverPort;
        $this->loadModel('Menus');
        $menuList = $this->Menus->getDynamicAclMenu($this->serverName, $this->serverPort, $this->sessionRoleId, $this->sessionUserId);
        $name_field = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $menuListHtml = "";
        foreach ($menuList as $k => $v) {
            if (strpos($v['url'], '#') !== false) {
                $menuListHtml .= "<li>
                                            <a href='#'>
                                                <span class='nav-label'>{$v[$name_field]}</span>
                                                <span class='fa arrow'></span>
                                            </a><ul class='nav nav-second-level'>";
                $childMenuLists = $this->Menus->getDynamicChildMenu($this->serverName, $this->serverPort, $this->sessionRoleId, $this->sessionUserId, $v['id']);
                foreach ($childMenuLists as $kk => $vv) {
                    $menuListHtml .= "<li><a href='{$this->internalPath}{$vv['url']}'> {$vv[$name_field]}</a></li>";
                }
                $menuListHtml .= "</ul></li>";
            } elseif ((strpos($v['url'], 'http://') !== false) || (strpos($v['url'], 'https://') !== false)) {
                $menuListHtml .= "<li><a href='{$v['url']}' target='_blank'><span>{$v[$name_field]}</span></a></li>";
            } else {
                $menuListHtml .= "<li><a href='{$_FULL_URL}{$v['url']}'><span>{$v[$name_field]}</span></a></li>";
            }
        }
        return $menuListHtml;
    }

    /**
     * Set language used this in mutiple language application concept
     * @author Sarawutt.b
     * @since 2016/03/21 10:23:33
     * @return void
     */
    public function _setLanguage() {
        $this->L10n = new L10n();
        $language = $this->request->session()->read('SessionLanguage');
        Configure::write('Config.language', $language);
        $this->L10n->get($language);
    }

    /**
     * 
     * Function get for current session user language
     * @author sarawutt.b
     * @return string
     */
    public function getCurrentLanguage() {
        return $this->request->session()->read('SessionLanguage');
    }

    /**
     *
     * This function for uload attachment excel file  from view of information style list
     * @author  sarawutt.b
     * @param   string name of target upload path
     * @param   array() file attribute option from upload form
     * @since   2017/10/30
     * @return  array()
     */
    function uploadFiles($folder, $file, $itemId = null) {
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;
        if (!is_dir($folder_url)) {
            mkdir($folder_url, 0777);
        }
        //Bould new path if $itemId to be not null
        if ($itemId) {
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            $rel_url = $folder . '/' . $itemId;
            if (!is_dir($folder_url)) {
                mkdir($folder_url, 0777);
            }
        }
        //define for file type where it allow to upload
        $map = [
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/excel' => '.xls',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx'
        ];
        //Bould file extension keep to the database
        $userfile_extn = substr($file['name'], strrpos($file['name'], '.') + 1);
        $typeOK = false;
        if (array_key_exists($file['type'], $map)) {
            $typeOK = true;
        }
        //debug($file);exit;
        //Rename for the file if not change of the upload file makbe duplicate
        $filename = $this->VERSION() . $map[$file['type']];
        //debug($filename);exit;
        if ($typeOK) {
            switch ($file['error']) {
                case 0:
                    @unlink($folder_url . '/' . $filename); //Delete the file if it already existing
                    $full_url = $folder_url . '/' . $filename;
                    $url = $rel_url . '/' . $filename;
                    $success = move_uploaded_file($file['tmp_name'], $url);
                    chmod($url, 0777);
                    if ($success) {
                        $result['path'] = $folder_url;
                        $result['uploadPaths'][] = '/' . $url;
                        $result['uploadFileNames'][] = $filename;
                        $result['uploadExts'][] = $userfile_extn;
                        $result['uploadOriginFileNames'][] = $file['name'];
                        $result['uploadFileTypes'][] = $file['type'];
                    } else {
                        $result['uploadErrors'][] = __("Error uploaded {$filename}. Please try again.");
                    }
                    break;
                case 3:
                    $result['uploadErrors'][] = __("Error uploading {$filename}. Please try again.");
                    break;
                case 4:
                    $result['noFiles'][] = __("No file Selected");
                    break;
                default:
                    $result['uploadErrors'][] = __("System error uploading {$filename}. Contact webmaster.");
                    break;
            }
        } else {
            $permiss = '';
            foreach ($map as $k => $v) {
                $permiss .= "{$v}, ";
            }
            $result['uploadErrors'][] = __("{$filename} cannot be uploaded. Acceptable file types in : %s", trim($permiss, ','));
        }
        return $result;
    }

    /**
     *
     * Function used fro generate _VERSION_
     * @author  sarawutt.b
     * @return  biginteger of the version number
     */
    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    /**
     *
     * Function used for generate UUID key patern
     * @author  sarawutt.b
     * @return  string uuid in version
     */
    public function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    /**
     * 
     * Function get for current session user authentication full name
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user full name
     */
    protected function getAuthFullname() {
        return $this->readAuth('Auth.User.first_name') . ' ' . $this->readAuth('Auth.User.last_name');
    }

    /**
     * 
     * Function get for current session user authentication user id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    protected function getAuthUserId() {
        return $this->readAuth('Auth.User.id');
    }

    /**
     * 
     * Function get for current session user authentication role id
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication user id
     */
    protected function getAuthUserRoleId() {
        return $this->readAuth('Auth.User.role_id');
    }

    /**
     * 
     * Function get for current session with user authentication
     * @author  sarawutt.b
     * @since   2018/02/06
     * @return  string of authentication session info
     */
    protected function readAuth($name = null) {
        return $this->request->session()->read($name);
    }

    /**
     * Function get for empty option in DDL
     * @author sarawutt.b
     * @return array() of empty select DDL
     */
    public function getEmptySelect() {
        return ['' => __($this->selectEmptyMsg)];
    }

    /**
     * 
     * Function build for the respond for the request
     * @author  sarawutt.b
     * @param   type $message as a string of the respond message
     * @param   type $status as a string of status code possible value OK|ERROR
     * @return  []
     */
    protected function buildRequestRespond($message = 'successfully for the request', $status = 'OK') {
        $status = strtoupper($status);
        $class = ['OK' => 'success', 'ERROR' => 'error'];
        return ['message' => $message, 'status' => $status, 'code' => array_key_exists($status, $this->respondCodes) ? $this->respondCodes[$status] : '-1', 'class' => array_key_exists($status, $class) ? $class[$status] : 'default'];
    }

    /**
     * 
     * Function configure read wrapper function
     * @author sarawutt.b
     * @param string $key configure name
     * @param mix $default default value for configure if not set
     * @return mix
     */
    public function readConfigure($key, $default = null) {
        return Configure::read($key, $default);
    }

    /**
     * 
     * Function configure write wrapper function
     * @author sarawutt.b
     * @param string $key configure name
     * @param mix $val value of the key in configure
     * @return mix
     */
    public function writeConfigure($key, $val) {
        return Configure::write($key, $val);
    }

}
