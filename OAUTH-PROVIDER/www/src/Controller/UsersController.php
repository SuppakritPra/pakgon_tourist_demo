<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * Users Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 16:57:03
 * @license Pakgon.Ltd.
 */
class UsersController extends AppController {
    /**
     *
     * Documentation 
     * Link : https://holt59.github.io/cakephp3-bootstrap-helpers/
     * Git : https://github.com/Holt59/cakephp3-bootstrap-helpers
     * @var type 
     */
//    public $helpers = [
//        'Utility',
//        'Form' => ['className' => 'Bootstrap.Form', 'useCustomFileInput' => true],
//        'Html' => ['className' => 'Bootstrap.Html'],
//        'Modal' => ['className' => 'Bootstrap.Modal'],
//        'Navbar' => ['className' => 'Bootstrap.Navbar'],
//        'Paginator' => ['template' => 'paginator-template'],
//        'Panel' => ['className' => 'Bootstrap.Panel']
//    ];

    /**
     * 
     * Function initialize make for automatically trigger when contructure
     */
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['add', 'logout']);
    }

    /**
     * 
     * Function login
     * @author sarawutt.b
     * @return void redirect to home page if pass authentication
     */
    public function login() {
        $this->viewBuilder()->setTheme(false);
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $redirectUri = $this->Auth->redirectUrl();
                if (array_key_exists('redir', $this->request->query) && ($this->request->query['redir'] === 'oauth')) {
                    $redirectUri = [
                        'plugin' => 'OAuthServer',
                        'controller' => 'OAuth',
                        'action' => 'authorize',
                        '?' => $this->request->query
                    ];
                }
                return $this->redirect($redirectUri);
            } else {
                $this->Flash->error(
                        __('Username or password failed'), 'default', [], 'auth'
                );
            }
        }
    }

    /**
     * 
     * Function Logout
     * @author  sarawutt.b
     * @return  void redirect to login page
     */
    public function logout() {
        $this->Flash->success(__('You are now logged out.'));
        return $this->redirect($this->Auth->logout());
    }

    /**
     *
     * Index method make list for User.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 16:57:03
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }

        $conditions = ['Users.is_used' => true];
        $order = ['Users.modified' => 'asc', 'Users.created' => 'asc', 'Users.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());

            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions[] = ['LOWER(Users.username) ILIKE ' => "%{$name}%"];
            }

            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['Users.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Users.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Users.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(Users.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(Users.created) <= ' => $this->request->data['dateTo']);
            }
        }

        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT')
        ];

        $users = $this->paginate($this->Users);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     *
     * View method make for view information of User.
     *
     * @author  sarawutt.b
     * @param   string|null $id User id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 16:57:03
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->Users->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested user, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $user = $this->Users->get($id);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     *
     * Add method make for insert or add new User.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 16:57:03
     * @license Pakgon.Ltd
     */
    public function add() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['is_used'] = true;
            $this->request->data['created_by'] = 1;
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     *
     * Edit method make for update User.
     *
     * @author  sarawutt.b
     * @param   string|null $id User id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 16:57:03
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->Users->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested user, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be update. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     *
     * Delete method make for delete record of User.
     *
     * @author  sarawutt.b
     * @param   string|null $id User id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 16:57:03
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->Users->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested user, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }

        $user = $this->Users->get($id);
        $respond = [];

        if ($this->Users->delete($user)) {
            $respond = $this->buildRequestRespond(__('The user has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The user could not be deleted. Please, try again.'), 'ERROR');
        }

        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }

    public function getOauthInfo() {
        $response = [];
        if ($this->request->is(['post'])) {
            $params = $this->request->getData();
            $response = $this->Users->getOauthInfo($params);
        }
        $this->set('response', $response);
        $this->set('_serialize', ['response']);
    }

}
