<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class UtilityComponent extends Component {

    //public $uses = array('Utility','Common');
    //public $components = array('Session','Utility');

    public function initialize(array $config) {
//        foreach ($this->uses as $model_name) {
//            App::import('Model', $model_name);
//            $model_class = "{$model_name}";
//            $this->$model_name = new $model_class();
//        }
    }

    /**
     * Trim all data in params
     * @author  sarawutt.b
     * @param   data Mix type as data where you want to trim
     * @return  []
     * @since   2013-02-16
     */
    public function trimAllData($data = null) {
        $return_data = [];
        if (!is_array($data)) {
            $data = array($data);
        }

        if (empty($data))
            return [];
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $kk => $vv) {
                    @$return_data[$k][$kk] = trim($vv);
                }
            } else {
                $return_data[$k] = trim($v);
            }
        }
        return $return_data;
    }

    /**
     * 
     * Function check if empty value then unset for the empty index | make clean insert | clean update | The function pass by refereces 
     * @author sarawutt.b
     * @param type $params as array where want to creansing data
     * @return boolean
     */
    public function clearEmptyRequestData(&$params = null) {
        if (!is_array($params)) {
            return true;
        }
        foreach ($params as $k => $v) {
            if (empty($v)) {
                unset($params[$k]);
            }
        }
        return true;
    }

    /**
     * 
     * Function cleansing all number with currency formate can be saving to the database | The function pass by refereces
     * @author sarawutt.b
     * @param type $params as a array contain all currency 
     * @return array() same params input except clean of the currency
     */
    public function unformatCurrencyParams(&$params = []) {
        return $this->Commons->unformatCurrencyParams($params);
    }

    /**
     * 
     * Unformate currentcy where receiving params formate befor matematic operate
     * @author  sarawutt.b
     * @param   type $currency as string in currentcy format
     * @return  double
     */
    public function unformatCurrency($currency = 0, $showPrecision = true) {
        return $this->Commons->unformatCurrency($currency, $showPrecision);
    }

    /**
     * 
     * Unformate currentcy no wanted the precision of it example 1,000,000 , 1,000
     * @author  sarawutt.b
     * @param   type $currency as string in currentcy format
     * @since   2016/09/28 07:00
     * @return  double
     */
    public function unformatCurrencyDecimal($currency = 0) {
        return $this->unformatCurrency($currency, false);
    }

    /**
     * 
     * Function currency format display the format of the currency
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @return  string number in currency format
     */
    public function currencyFormat($currentcy = null) {
        return $this->_numberFormat($currentcy, 2, Configure::read('Zicure.Currency'));
    }

    /**
     * 
     * Function for display number in format number
     * @param   type $number as number or integer or decimal
     * @since   2016-10-10
     * @return  string number in format thousan separated
     */
    public function numberFormat($number = null, $precition = 2) {
        return $this->_numberFormat($number, $precition);
    }

    /**
     * 
     * Function currency format display for number in format
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @param   type $currencySymbol as string currency symbol
     * @param   type $precition as integer of amount precition
     * @since   2016-10-10
     * @return  string
     */
    private function _numberFormat($currentcy = null, $precition = 2, $currencySymbol = null) {
        $currentcy = (is_null($currentcy) || empty($currentcy)) ? 0 : $currentcy;
        return number_format($currentcy, $precition, '.', ',') . $currencySymbol;
    }

    /**
     * 
     * Build arrray() for multiple pagination
     * @author  sarawutt.b
     * @param type $arr1 as array of result set in fine model
     * @param type $arr2 as array of result set in fine model
     * @return array()
     */
    public function mergeMultipleArrayPagination($arr1 = array(), $arr2 = array()) {
        $result = array();
        $index = 0;
        if (is_array($arr1)) {
            foreach ($arr1 as $k => $v) {
                $result[$index] = $v;
                $index++;
            }
        }

        if (is_array($arr2)) {
            foreach ($arr2 as $k => $v) {
                $result[$index] = $v;
                $index++;
            }
        }

        return $result;
    }

    /**
     * 
     * Function zecure make for decoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function zecureDecodeParam($param = null) {
        return $this->Commons->secureDecodeParam($param);
    }

    /**
     * 
     * Function zecure make for encoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function zecureEncodeParam($param = null) {
        return $this->Commons->secureEncodeParam($param);
    }

}

?>
