<?php

namespace App\Controller;

//use App\Controller\AppController;
use RestApi\Controller\ApiController as AppController;

/**
 * 
 * OauthAPI Controller 
 * @author sarawutt.b
 *
 * @method \App\Model\Entity\OauthAPI[] paginate($object = null, array $settings = [])
 */
class OauthAPIController extends AppController {

    /**
     * 
     * Function token verify used for check the oauth token is expires
     * @author sarawutt.b
     * @since   2018/05/22 15:06:23
     * @license PAKGON
     * @return string json
     */
    public function tokenVerify() {
        if (!$this->request->allowMethod('post', 'put', 'patch')) {
            $this->responseStatus = 403;
            $this->apiResponse = ['type' => 'error', 'result' => $this->request->method() . 'not allow you must instread with POST / PUT / PATCH'];
        }
        $data = $this->request->getData();

        if ((!array_key_exists('oauth_token', $data) || empty($data['oauth_token'])) && (!array_key_exists('owner_id', $data) || empty($data['owner_id']))) {
            $this->responseStatus = 'Fail';
            $this->apiResponse['type'] = 'error';
            $this->apiResponse['message'] = __('Please check "oauth_token" or "owner_id" must be not empty');
            return;
        } else {

            $conditions = [];
            if (array_key_exists('owner_id', $data)) {
                $this->loadModel('OauthSessions');
                $sessionList = $this->OauthSessions->find('list', [
                            'keyField' => 'id',
                            'valueField' => 'id'
                        ])->where(['owner_id' => $data['owner_id']])->toArray();
                $conditions['session_id IN'] = $sessionList;
            } elseif (array_key_exists('oauth_token', $data)) {
                $conditions['oauth_token'] = $data['oauth_token'];
            }

            //dd($conditions);

            $isExpired = true;
            $this->loadModel('OauthAccessTokens');
            $oauthAccessToken = $this->OauthAccessTokens->find()
                    ->select(['oauth_token'])
                    ->where([$conditions, 'expires > ' => time()])
                    ->contain(['OauthSessions' => ['fields' => ['id', 'owner_id']]])
                    ->all();

            //dd($oauthAccessToken->isEmpty());
            if (!($oauthAccessToken->isEmpty())) {
                $results = $oauthAccessToken->first();
                $this->loadModel('Users');
                $userId = $results->oauth_session->owner_id;
                $countUser = $this->Users->findByIdAndIsActiveAndIsUsed($userId, true, true)
                        ->select(['Users.id'])
                        ->where(['Users.id' => $userId, 'Users.is_active' => true, 'Users.is_used' => true])
                        ->innerJoinWith('UserPersonals', function($up) use ($userId) {
                            return $up->where(['UserPersonals.user_id' => $userId]);
                        })
                        ->count();

                if ($countUser < 1) {
                    $this->responseStatus = 'Fail';
                    $this->apiResponse['type'] = 'error';
                    $this->apiResponse['message'] = __('Please check "user_personals" information must be not exist');
                    return;
                }else{
                    $isExpired = false;
                }
//                $isExpired = ($countUser > 0) ? false : true;
            }

            $this->apiResponse = ['type' => 'success',
                'result' => [
                    'is_expires' => $isExpired,
                    'oauth_token' => array_key_exists('oauth_token', $data) ? $data['oauth_token'] : null,
                    'owner_id' => array_key_exists('owner_id', $data) ? $data['owner_id'] : null
            ]];
        }
    }

    /**
     * 
     * Function token logout delete token
     * @author sarawutt.b
     * @since   2018/05/22 15:24:22
     * @license PAKGON
     * @return string json
     */
    public function logoutDeleteToken($ownerId) {
        if (!($this->request->allowMethod(['delete']))) {
            $this->responseStatus = 403;
            $this->apiResponse = ['type' => 'error', 'result' => $this->request->method() . 'not allow you must instread with DELETE'];
        }
        $this->loadModel('OauthAccessTokens');
        $result = $this->OauthAccessTokens->logoutDeleteToken($ownerId);
        $this->apiResponse = ['type' => 'success',
            'result' => [
                'status' => !($result == false)
        ]];
    }

//    public function logoutDeleteToken($ownerId) {
//        if (!($this->request->allowMethod(['delete']))) {
//            $this->responseStatus = 403;
//            $this->apiResponse = ['type' => 'error', 'result' => $this->request->method() . 'not allow you must instread with DELETE'];
//        }
//
//        $this->loadModel('OauthSessions');
//        if ($this->OauthSessions->exists(['owner_id' => $ownerId])) {
//            $result = $this->OauthSessions->findByOwnerId($ownerId)->order(['id' => 'desc'])->first();
//        }
//        $this->loadModel('OAuthServer.AccessTokens');
//        $accessToken = $this->AccessTokens->findBySessionId($result->id)->first();
//        $accessToken->expires = 0;
//        $result = $this->AccessTokens->save($accessToken);
////        $results = $this->AccessTokens->delete($accessToken, ['cascade' => true]);
//        $this->apiResponse = ['type' => 'success',
//            'result' => [
//                'status' => !($result == false)
//        ]];
//    }

    public function getUserIdByToken() {
        $this->request->allowMethod(['post']);
        $this->loadModel('OauthAccessTokens');

        $params = $this->request->getData();
        $data = $this->OauthAccessTokens
                ->find()
                ->select(['OauthSessions.owner_id'])
                ->where(['OauthAccessTokens.oauth_token' => $params['token']])
                ->contain(['OauthSessions'])
                ->order(['OauthSessions.id desc'])
                ->first();

        $response = empty($data) ? [] : $data->toArray();
        $this->apiResponse = ['type' => 'success',
            'result' => [
                'user_id' => isset($response['OauthSessions']['owner_id']) ? $response['OauthSessions']['owner_id'] : -1
        ]];
    }

}
