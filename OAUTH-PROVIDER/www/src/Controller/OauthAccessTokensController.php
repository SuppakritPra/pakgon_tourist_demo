<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthAccessTokens Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthAccessTokensTable $OauthAccessTokens
 * @method \App\Model\Entity\OauthAccessToken[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:01:59
 * @license Pakgon.Ltd.
 */
class OauthAccessTokensController extends AppController
{

    /**
     *
     * Index method make list for Oauth Access Token.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthAccessTokens.modified' => 'asc', 'OauthAccessTokens.created' => 'asc', 'OauthAccessTokens.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthAccessTokens.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthAccessTokens.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthAccessTokens.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAccessTokens.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAccessTokens.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAccessTokens.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthAccessTokens.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Sessions']
        ];

        $oauthAccessTokens = $this->paginate($this->OauthAccessTokens);
        $this->set(compact('oauthAccessTokens'));
        $this->set('_serialize', ['oauthAccessTokens']);
    }

    /**
     *
     * View method make for view information of Oauth Access Token.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Access Token id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthAccessTokens->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth access token, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAccessToken = $this->OauthAccessTokens->get($id, [
            'contain' => ['Sessions']
        ]);
        $this->set('oauthAccessToken', $oauthAccessToken);
        $this->set('_serialize', ['oauthAccessToken']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Access Token.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthAccessToken = $this->OauthAccessTokens->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthAccessToken = $this->OauthAccessTokens->patchEntity($oauthAccessToken, $this->request->getData());
            if ($this->OauthAccessTokens->save($oauthAccessToken)) {
                $this->Flash->success(__('The oauth access token has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth access token could not be saved. Please, try again.'));
        }
        $sessions = $this->OauthAccessTokens->Sessions->find('list', ['limit' => 200]);
        $this->set(compact('oauthAccessToken', 'sessions'));
        $this->set('_serialize', ['oauthAccessToken']);
    }

    /**
     *
     * Edit method make for update Oauth Access Token.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Access Token id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthAccessTokens->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth access token, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAccessToken = $this->OauthAccessTokens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthAccessToken = $this->OauthAccessTokens->patchEntity($oauthAccessToken, $this->request->getData());
            if ($this->OauthAccessTokens->save($oauthAccessToken)) {
                $this->Flash->success(__('The oauth access token has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth access token could not be update. Please, try again.'));
        }
        $sessions = $this->OauthAccessTokens->Sessions->find('list', ['limit' => 200]);
        $this->set(compact('oauthAccessToken', 'sessions'));
        $this->set('_serialize', ['oauthAccessToken']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Access Token.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Access Token id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:01:59
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthAccessTokens->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth access token, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAccessToken = $this->OauthAccessTokens->get($id);
        $respond = [];
        
        if ($this->OauthAccessTokens->delete($oauthAccessToken)) {
            $respond = $this->buildRequestRespond(__('The oauth access token has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth access token could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
