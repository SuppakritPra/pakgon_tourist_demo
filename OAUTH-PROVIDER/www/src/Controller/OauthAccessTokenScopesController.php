<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthAccessTokenScopes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthAccessTokenScopesTable $OauthAccessTokenScopes
 * @method \App\Model\Entity\OauthAccessTokenScope[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:02:16
 * @license Pakgon.Ltd.
 */
class OauthAccessTokenScopesController extends AppController
{

    /**
     *
     * Index method make list for Oauth Access Token Scope.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthAccessTokenScopes.modified' => 'asc', 'OauthAccessTokenScopes.created' => 'asc', 'OauthAccessTokenScopes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthAccessTokenScopes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthAccessTokenScopes.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthAccessTokenScopes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAccessTokenScopes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAccessTokenScopes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAccessTokenScopes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthAccessTokenScopes.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Scopes']
        ];

        $oauthAccessTokenScopes = $this->paginate($this->OauthAccessTokenScopes);
        $this->set(compact('oauthAccessTokenScopes'));
        $this->set('_serialize', ['oauthAccessTokenScopes']);
    }

    /**
     *
     * View method make for view information of Oauth Access Token Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Access Token Scope id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthAccessTokenScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth access token scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAccessTokenScope = $this->OauthAccessTokenScopes->get($id, [
            'contain' => ['Scopes']
        ]);
        $this->set('oauthAccessTokenScope', $oauthAccessTokenScope);
        $this->set('_serialize', ['oauthAccessTokenScope']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Access Token Scope.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthAccessTokenScope = $this->OauthAccessTokenScopes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthAccessTokenScope = $this->OauthAccessTokenScopes->patchEntity($oauthAccessTokenScope, $this->request->getData());
            if ($this->OauthAccessTokenScopes->save($oauthAccessTokenScope)) {
                $this->Flash->success(__('The oauth access token scope has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth access token scope could not be saved. Please, try again.'));
        }
        $scopes = $this->OauthAccessTokenScopes->Scopes->find('list', ['limit' => 200]);
        $this->set(compact('oauthAccessTokenScope', 'scopes'));
        $this->set('_serialize', ['oauthAccessTokenScope']);
    }

    /**
     *
     * Edit method make for update Oauth Access Token Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Access Token Scope id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthAccessTokenScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth access token scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAccessTokenScope = $this->OauthAccessTokenScopes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthAccessTokenScope = $this->OauthAccessTokenScopes->patchEntity($oauthAccessTokenScope, $this->request->getData());
            if ($this->OauthAccessTokenScopes->save($oauthAccessTokenScope)) {
                $this->Flash->success(__('The oauth access token scope has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth access token scope could not be update. Please, try again.'));
        }
        $scopes = $this->OauthAccessTokenScopes->Scopes->find('list', ['limit' => 200]);
        $this->set(compact('oauthAccessTokenScope', 'scopes'));
        $this->set('_serialize', ['oauthAccessTokenScope']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Access Token Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Access Token Scope id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:02:16
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthAccessTokenScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth access token scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAccessTokenScope = $this->OauthAccessTokenScopes->get($id);
        $respond = [];
        
        if ($this->OauthAccessTokenScopes->delete($oauthAccessTokenScope)) {
            $respond = $this->buildRequestRespond(__('The oauth access token scope has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth access token scope could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
