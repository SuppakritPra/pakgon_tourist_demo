<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 *
 * OauthAuthCodeScopes Controller
 * @author  sarawutt.b
 * @property \App\Model\Table\OauthAuthCodeScopesTable $OauthAuthCodeScopes
 * @method \App\Model\Entity\OauthAuthCodeScope[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @since   2018/05/10 17:03:17
 * @license Pakgon.Ltd.
 */
class OauthAuthCodeScopesController extends AppController
{

    /**
     *
     * Index method make list for Oauth Auth Code Scope.
     *
     * @author  sarawutt.b
     * @return \Cake\Http\Response|void
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function index() {
        if (empty($this->passedArgs)) {
            $this->passedArgs = $this->request->getData();
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs;
        }
        
        $conditions = [];
        $order = ['OauthAuthCodeScopes.modified' => 'asc', 'OauthAuthCodeScopes.created' => 'asc', 'OauthAuthCodeScopes.id' => 'asc'];
        if (!empty($this->request->getData())) {
            $this->request->data = $this->Utility->trimAllData($this->request->getData());
            
            //Find by name or name_en
            if (!empty($this->request->data['name'])) {
                $name = strtolower($this->request->data['name']);
                $conditions['OR'][] = ['LOWER(OauthAuthCodeScopes.name) ILIKE ' => "%{$name}%"];
                $conditions['OR'][] = ['LOWER(OauthAuthCodeScopes.name_eng) ILIKE ' => "%{$name}%"];
            }
            
            //Find by status
            if (!empty($this->request->data['status'])) {
                $conditions[] = ['OauthAuthCodeScopes.status' => $this->request->data['status']];
            }

            //Find by created and modified
            if (!empty($this->request->data['dateFrom']) && empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAuthCodeScopes.created)' => $this->request->data['dateFrom']);
            } else if (empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAuthCodeScopes.created)' => $this->request->data['dateTo']);
            } else if (!empty($this->request->data['dateFrom']) && !empty($this->request->data['dateTo'])) {
                $conditions[] = array('DATE(OauthAuthCodeScopes.created) >= ' => $this->request->data['dateFrom']);
                $conditions[] = array('DATE(OauthAuthCodeScopes.created) <= ' => $this->request->data['dateTo']);
            }
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $this->readConfigure('PAGINATION.LIMIT'),
            'contain' => ['Scopes']
        ];

        $oauthAuthCodeScopes = $this->paginate($this->OauthAuthCodeScopes);
        $this->set(compact('oauthAuthCodeScopes'));
        $this->set('_serialize', ['oauthAuthCodeScopes']);
    }

    /**
     *
     * View method make for view information of Oauth Auth Code Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Auth Code Scope id.
     * @return  \Cake\Http\Response|void
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function view($id = null) {
        if (!$this->OauthAuthCodeScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth auth code scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAuthCodeScope = $this->OauthAuthCodeScopes->get($id, [
            'contain' => ['Scopes']
        ]);
        $this->set('oauthAuthCodeScope', $oauthAuthCodeScope);
        $this->set('_serialize', ['oauthAuthCodeScope']);
    }

    /**
     *
     * Add method make for insert or add new Oauth Auth Code Scope.
     *
     * @author  sarawutt.b 
     * @return  \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function add() {
        $oauthAuthCodeScope = $this->OauthAuthCodeScopes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['create_uid'] = $this->getAuthUserId();
            $oauthAuthCodeScope = $this->OauthAuthCodeScopes->patchEntity($oauthAuthCodeScope, $this->request->getData());
            if ($this->OauthAuthCodeScopes->save($oauthAuthCodeScope)) {
                $this->Flash->success(__('The oauth auth code scope has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth auth code scope could not be saved. Please, try again.'));
        }
        $scopes = $this->OauthAuthCodeScopes->Scopes->find('list', ['limit' => 200]);
        $this->set(compact('oauthAuthCodeScope', 'scopes'));
        $this->set('_serialize', ['oauthAuthCodeScope']);
    }

    /**
     *
     * Edit method make for update Oauth Auth Code Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Auth Code Scope id.
     * @return  \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws  \Cake\Network\Exception\NotFoundException When record not found.
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function edit($id = null) {
        if (!$this->OauthAuthCodeScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth auth code scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAuthCodeScope = $this->OauthAuthCodeScopes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['update_uid'] = $this->getAuthUserId();
            $oauthAuthCodeScope = $this->OauthAuthCodeScopes->patchEntity($oauthAuthCodeScope, $this->request->getData());
            if ($this->OauthAuthCodeScopes->save($oauthAuthCodeScope)) {
                $this->Flash->success(__('The oauth auth code scope has been update.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The oauth auth code scope could not be update. Please, try again.'));
        }
        $scopes = $this->OauthAuthCodeScopes->Scopes->find('list', ['limit' => 200]);
        $this->set(compact('oauthAuthCodeScope', 'scopes'));
        $this->set('_serialize', ['oauthAuthCodeScope']);
    }


    /**
     *
     * Delete method make for delete record of Oauth Auth Code Scope.
     *
     * @author  sarawutt.b
     * @param   string|null $id Oauth Auth Code Scope id.
     * @return  \Cake\Http\Response|null Redirects to index.
     * @throws  \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @since   2018/05/10 17:03:17
     * @license Pakgon.Ltd
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if (!$this->OauthAuthCodeScopes->exists(['id' => $id])) {
            $this->Flash->warning(__('Invalid not found requested oauth auth code scope, Please try again!.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $oauthAuthCodeScope = $this->OauthAuthCodeScopes->get($id);
        $respond = [];
        
        if ($this->OauthAuthCodeScopes->delete($oauthAuthCodeScope)) {
            $respond = $this->buildRequestRespond(__('The oauth auth code scope has been deleted.'), 'OK');
        } else {
            $respond = $this->buildRequestRespond(__('The oauth auth code scope could not be deleted. Please, try again.'), 'ERROR');
        }
        
        if ($this->request->is('ajax')) {
            echo json_encode($respond);
            exit;
        } else {
            $this->Flash->{$respond['class']}($respond['message']);
            return $this->redirect(['action' => 'index']);
        }
    }
}
