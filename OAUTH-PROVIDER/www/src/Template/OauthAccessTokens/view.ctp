<?php

 /**
  *
  * The template view for view as of oauthAccessTokens controller the page show for oauthAccessTokens information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAccessToken $oauthAccessToken
  * @since  2018-05-10 17:01:59
  * @license Pakgon.Ltd
  */
?>
<div class="oauthAccessTokens view oauthAccessTokens-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Access Token Management System => ({0} information)', h($oauthAccessToken->oauth_token)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Oauth Token'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAccessToken->oauth_token); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Session Id'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($oauthAccessToken->session_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Expires'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($oauthAccessToken->expires); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
