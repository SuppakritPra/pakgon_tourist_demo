<?php
/**
  * 
  * add oauth access tokens template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAccessToken $oauthAccessToken
  * @since   2018/05/10 17:01:59
  * @license pakgon.Ltd.
  */
?>
<div class="oauthAccessTokens oauthAccessTokens-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Access Token Management System => ( Add Oauth Access Token )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthAccessToken, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('session_id'); ?>
            <?php echo $this->Form->control('expires'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Save'), $this->request->here , ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for add oauth access token ?'), 'data-confirm-title' => __('Confirm for add oauth access token ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
