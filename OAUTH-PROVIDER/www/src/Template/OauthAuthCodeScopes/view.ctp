<?php

 /**
  *
  * The template view for view as of oauthAuthCodeScopes controller the page show for oauthAuthCodeScopes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAuthCodeScope $oauthAuthCodeScope
  * @since  2018-05-10 17:03:17
  * @license Pakgon.Ltd
  */
?>
<div class="oauthAuthCodeScopes view oauthAuthCodeScopes-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Auth Code Scope Management System => ({0} information)', h($oauthAuthCodeScope->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Auth Code'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAuthCodeScope->auth_code); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Scope Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAuthCodeScope->scope_id); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Oauth Auth Code Scopes Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAuthCodeScope->id); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
