<?php
/**
  * 
  * add oauth auth code scopes template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAuthCodeScope $oauthAuthCodeScope
  * @since   2018/05/10 17:03:17
  * @license pakgon.Ltd.
  */
?>
<div class="oauthAuthCodeScopes oauthAuthCodeScopes-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Auth Code Scope Management System => ( Add Oauth Auth Code Scope )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthAuthCodeScope, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('auth_code'); ?>
            <?php echo $this->Form->control('scope_id'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Save'), $this->request->here , ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for add oauth auth code scope ?'), 'data-confirm-title' => __('Confirm for add oauth auth code scope ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
