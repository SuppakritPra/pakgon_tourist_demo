<?php

 /**
  *
  * The template view for view as of oauthSessionScopes controller the page show for oauthSessionScopes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthSessionScope $oauthSessionScope
  * @since  2018-05-10 17:04:33
  * @license Pakgon.Ltd
  */
?>
<div class="oauthSessionScopes view oauthSessionScopes-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Session Scope Management System => ({0} information)', h($oauthSessionScope->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Scope Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSessionScope->scope_id); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Oauth Session Scopes Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSessionScope->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Session Id'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($oauthSessionScope->session_id); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
