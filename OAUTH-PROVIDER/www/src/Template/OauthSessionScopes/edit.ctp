<?php
/**
  * 
  * edit oauth session scopes template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthSessionScope $oauthSessionScope
  * @since   2018/05/10 17:04:33
  * @license pakgon.Ltd.
  */
?>
<div class="oauthSessionScopes oauthSessionScopes-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Session Scope Management System => ( Add Oauth Session Scope )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthSessionScope, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('session_id'); ?>
            <?php echo $this->Form->control('scope_id'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Edit'), $this->request->here , ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for edit oauth session scope ?'), 'data-confirm-title' => __('Confirm for edit oauth session scope ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
