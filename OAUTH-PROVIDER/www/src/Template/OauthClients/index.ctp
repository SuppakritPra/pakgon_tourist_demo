<?php
/**
 * The template index for index as of oauthClients controller the page show for short oauthClients information.
 *
 * @author  sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OauthClient[]|\Cake\Collection\CollectionInterface $oauthClients
 * @since  2018-05-10 16:59:14
 * @license Pakgon.Ltd
 */
?>
<div class="oauthClients index">

    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Client Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('oauthClients'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Oauth Client'), '/oauthClients/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Oauth Client'), '/oauthClients/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->


    <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Clients list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
            <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('Row no'); ?></th>
                        <th><?php echo $this->Paginator->sort('client_secret'); ?></th>
                        <th><?php echo $this->Paginator->sort('name', __('Oauth Clients Name')); ?></th>
                        <th><?php echo $this->Paginator->sort('redirect_uri'); ?></th>
                        <th><?php echo $this->Paginator->sort('parent_model'); ?></th>
                        <th><?php echo $this->Paginator->sort('parent_id'); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($oauthClients)): ?>
                        <?php foreach ($oauthClients as $index => $oauthClient): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                <td><?php echo h($oauthClient->client_secret); ?></td>
                                <td><?php echo h($oauthClient->name); ?></td>
                                <td><?php echo h($oauthClient->redirect_uri); ?></td>
                                <td><?php echo h($oauthClient->parent_model); ?></td>
                                <td><?php echo $oauthClient->has('parent_oauth_client') ? $this->Html->link($oauthClient->parent_oauth_client->name, ['controller' => 'OauthClients', 'action' => 'view', $oauthClient->parent_oauth_client->id]) : ''; ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($oauthClient->id); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
