<?php
/**
 * 
 * add oauth clients template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OauthClient $oauthClient
 * @since   2018/05/10 16:59:14
 * @license pakgon.Ltd.
 */
?>
<div class="oauthClients oauthClients-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Client Management System => ( Add Oauth Client )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php echo $this->Form->create($oauthClient, ['horizontal' => true]); ?>
        <?php echo $this->Form->control('name', ['class' => 'required']); ?>
        <?php echo $this->Form->control('redirect_uri', ['class' => 'required']); ?>
        <?php echo $this->Form->control('parent_model'); ?>
        <?php echo $this->Form->control('parent_id', ['type' => 'text']); ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Save'), $this->request->here, ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for add oauth client ?'), 'data-confirm-title' => __('Confirm for add oauth client ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
