<?php

 /**
  *
  * The template view for view as of oauthClients controller the page show for oauthClients information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthClient $oauthClient
  * @since  2018-05-10 16:59:14
  * @license Pakgon.Ltd
  */
?>
<div class="oauthClients view oauthClients-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Client Management System => ({0} information)', h($oauthClient->name)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Oauth Clients Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthClient->id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Client Secret'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthClient->client_secret); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Oauth Clients Name'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthClient->name); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Redirect Uri'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthClient->redirect_uri); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Parent Model'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthClient->parent_model); ?></td>
            </tr>
                                    <tr>
                <td class="table-view-label"><?php echo __('Parent Oauth Client'); ?></td>
                <td class="table-view-detail"><?php echo $oauthClient->has('parent_oauth_client') ? $this->Html->link($oauthClient->parent_oauth_client->name, ['controller' => 'OauthClients', 'action' => 'view', $oauthClient->parent_oauth_client->id]) : ''; ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

<div class="related div-box-related">
    <?php if (!empty($oauthClient->child_oauth_clients)): ?>
        <div class="box box-warning box-related">
            <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Related Oauth Clients'), 'bicon' => 'fa-refresh', 'bcollapse' => true, 'bclose' => true]); ?>
            <div class="box-body table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-list-related">
                    <tr>
                        <th class="nindex"><?php echo __('Row no'); ?></th>
                                                                        <th><?php echo __('Client Secret'); ?></th>
                                                                        <th><?php echo __('Oauth Clients Name'); ?></th>
                                                                        <th><?php echo __('Redirect Uri'); ?></th>
                                                                        <th><?php echo __('Parent Model'); ?></th>
                                                                        <th><?php echo __('Parent Id'); ?></th>
                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                    <?php if (!empty($oauthClient->child_oauth_clients)): ?>
                    <?php foreach ($oauthClient->child_oauth_clients as $index => $childOauthClients): ?>
                        <tr>
                            <td class="nindex"><?php echo h(++$index); ?></td>
                                                                                                    <td><?php echo h($childOauthClients->client_secret); ?></td>
                                                                                                        <td><?php echo h($childOauthClients->name); ?></td>
                                                                                                        <td><?php echo h($childOauthClients->redirect_uri); ?></td>
                                                                                                        <td><?php echo h($childOauthClients->parent_model); ?></td>
                                                                                                        <td><?php echo h($childOauthClients->parent_id); ?></td>
                                                                                    <td class="actions">
                                <?php echo $this->Permission->getActions($childOauthClients->id, 'OauthClients'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                        </tr>
                    <?php endif; ?>
                    </table>
            </div><!-- ./box-body -->
            <?php echo $this->element('utility/boxOptionFooter'); ?>
        </div><!-- ./box box-warning box-related -->
        <?php endif; ?>
</div><!-- ./related div-box-related -->
