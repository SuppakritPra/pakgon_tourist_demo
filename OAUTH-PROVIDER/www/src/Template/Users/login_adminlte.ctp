<?php $this->layout = 'AdminLTE204.login'; ?>

<?php echo $this->Form->create(); ?>
<div class="form-group has-feedback">
    <input type="text" class="form-control" placeholder="<?php echo __('Please enter your username');?>" name="username">
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <input type="password" class="form-control" placeholder="Password" name="password">
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
<!--    <div class="col-xs-8">
        <div class="checkbox icheck">
            <label>
                <input type="checkbox"> <?php echo __('Remember Me'); ?>
            </label>
        </div>
    </div>-->
    <!-- /.col -->
    <div class="col-xs-4 pull-right">
        <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Sign In'); ?></button>
    </div>
    <!-- /.col -->
</div>
<?php echo $this->Form->end(); ?>
