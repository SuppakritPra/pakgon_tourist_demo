<?php
/**
 * 
 * add users template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @since   2018/05/10 16:57:03
 * @license pakgon.Ltd.
 */
?>
<div class="users users-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('User Management System => ( Add User )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php echo $this->Form->create($user, ['horizontal' => true]); ?>
        <?php echo $this->Form->control('username', ['class' => 'required']); ?>
        <?php echo $this->Form->control('password', ['class' => 'required ckpassword', 'id' => 'password']); ?>
        <?php echo $this->Form->control('confirm_password', ['type' => 'password', 'class' => 'required', 'equalto' => '#password']); ?>

        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Save'), $this->request->here, ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for add user ?'), 'data-confirm-title' => __('Confirm for add user ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
