<?php
/**
  * 
  * edit users template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User $user
  * @since   2018/05/10 16:57:03
  * @license pakgon.Ltd.
  */
?>
<div class="users users-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('User Management System => ( Add User )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($user, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('username'); ?>
            <?php echo $this->Form->control('password'); ?>
            <?php echo $this->Form->control('point'); ?>
            <?php echo $this->Form->control('is_used'); ?>
            <?php echo $this->Form->control('dynamic_key'); ?>
            <?php echo $this->Form->control('dynamic_key_expiry', ['type' => 'text', 'class' => 'datepicker']); ?>
            <?php echo $this->Form->control('token'); ?>
            <?php echo $this->Form->control('token_expiry', ['type' => 'text', 'class' => 'datepicker']); ?>
            <?php echo $this->Form->control('created_by'); ?>
            <?php echo $this->Form->control('modified_by'); ?>
            <?php echo $this->Form->control('pin_code'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Edit'), $this->request->here , ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for edit user ?'), 'data-confirm-title' => __('Confirm for edit user ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
