<?php $this->layout = 'PakgonConnect.mobile'; ?>
<?php echo $this->element('common/topContent'); ?>
<div class="row">
    <div class="col">
        <?php echo $this->Form->create(null, ['name' => 'frmUserLogin', 'id' => 'frmUserLogin']); ?>
        <?php echo $this->Flash->render(); ?>
        <div class="row">
            <div class="col">
                <div class="form-group has-feedback">
                    <input class="form-control form-control-lg required username" maxlength="14" autocomplete="new-user" placeholder="<?php echo __('Username'); ?>" name="username" id='txtUsername'>
                    <!-- <i class="fa fa-user form-control-feedback"></i> -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group has-feedback">
                    <input type="password" class="form-control form-control-lg xckpassword required" autocomplete="new-password" placeholder="<?php echo __('********'); ?>" name="password" id='txtPassword'>
                    <!-- <i class="fa fa-lock form-control-feedback"></i> -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <?php echo $this->Permission->submit(__('Login'), $this->request->here, ['class' => 'btn btn-primary btn-lg btn-block', 'id' => 'btnLogin']); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>