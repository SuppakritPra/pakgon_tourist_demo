<?php
/**
 *
 * The template view for view as of users controller the page show for users information.
 *
 * @author  sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @since  2018-05-10 16:57:03
 * @license Pakgon.Ltd
 */
?>
<div class="users view users-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('User Management System => ({0} information)', h($user->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
            <tr>
                <td class="table-view-label"><?php echo __('Username'); ?></td>
                <td class="table-view-detail"><?php echo h($user->username); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Password'); ?></td>
                <td class="table-view-detail"><?php echo h($user->password); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Dynamic Key'); ?></td>
                <td class="table-view-detail"><?php echo h($user->dynamic_key); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Token'); ?></td>
                <td class="table-view-detail"><?php echo h($user->token); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Pin Code'); ?></td>
                <td class="table-view-detail"><?php echo h($user->pin_code); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Users Id'); ?></td>
                <td class="table-view-detail"><?php echo h($user->id); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Point'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($user->point); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Created By'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($user->created_by); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Modified By'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($user->modified_by); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Dynamic Key Expiry'); ?></td>
                <td class="table-view-detail"><?php echo h($user->dynamic_key_expiry); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Token Expiry'); ?></td>
                <td class="table-view-detail"><?php echo h($user->token_expiry); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Created'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($user->created); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Modified'); ?></td>
                <td class="table-view-detail"><?php echo $this->Utility->dateTimeISO($user->modified); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Is Used'); ?></td>
                <td class="table-view-detail"><?php echo $user->is_used ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
