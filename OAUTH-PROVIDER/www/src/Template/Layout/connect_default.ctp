<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Basic -->
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->fetch('title'); ?></title>	
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
        <!-- Vendor CSS -->
        <?php echo $this->Html->css('/connect/css/pakgon.css'); ?>
        <?php echo $this->Html->css('/connect/css/bootstrap/css/bootstrap.min'); ?>
        <?php echo $this->Html->css('/connect/css/font-awesome/css/font-awesome.min'); ?>
        <?php echo $this->Html->css('/connect/css/animate/animate.min'); ?>
        <?php echo $this->Html->css('/connect/css/simple-line-icons/css/simple-line-icons.min'); ?>
        <?php echo $this->Html->css('/connect/css/owl.carousel/assets/owl.carousel.min'); ?>
        <?php echo $this->Html->css('/connect/css/owl.carousel/assets/owl.theme.default.min'); ?>
        <?php echo $this->Html->css('/connect/css/magnific-popup/magnific-popup.min'); ?>
        <!-- Theme CSS -->
        <?php echo $this->Html->css('/connect/css/css/theme'); ?>
        <?php echo $this->Html->css('/connect/css/css/theme-elements'); ?>
        <?php echo $this->Html->css('/connect/css/css/theme-blog'); ?>
        <?php echo $this->Html->css('/connect/css/css/theme-shop'); ?>
        <!-- Skin CSS -->
        <?php echo $this->Html->css('/connect/css/css/skins/default'); ?>
        <?php echo $this->Html->script('/connect/js/master/style-switcher/style.switcher.localstorage'); ?>	
        <!-- Theme Custom CSS -->
        <?php echo $this->Html->css('/connect/css/css/custom'); ?>
        <!-- Head Libs -->
        <?php echo $this->Html->script('/connect/js/modernizr/modernizr.min'); ?>	
        <!-- APP  -->
        <?php echo $this->Html->script('/connect/js/custom/core'); ?>
        <?php echo $this->Html->css('/connect/css/pakgon.css'); ?>
        <style type="text/css">
            @font-face {
                font-family: 'Conv_supermarket';
                src: url('/font/fonts/supermarket.eot');
                src: local('☺'), url('/font/fonts/supermarket.woff') format('woff'), url('/font/fonts/supermarket.ttf') format('truetype'), url('/font/fonts/supermarket.svg') format('svg');
                font-weight: normal;
                font-style: normal;
            }
        </style>
        <?php echo $this->Html->script('/connect/js/jquery/jquery-1.11.3.min'); ?>
        <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
        <?php echo $this->Html->css('/connect/css/css/bootstrap-iso'); ?>
        <?php echo $this->Html->script('/connect/js/jquery/alex-date-time'); ?>
        <?php echo $this->Html->css('/connect/css/chosen_v151/bootstrap-chosen/bootstrap-chosen.css'); ?>
        <?php echo $this->Html->script('/connect/css/chosen_v151/chosen.jquery.min.js'); //Chosen select box?>
        <!-- Bootstrap Date-Picker Plugin -->	
        <?php echo $this->Html->css('/connect/css/bootstrap-datepicker-thai-thai/css/datepicker.css'); ?>	
        <?php echo $this->Html->script('/connect/css/datepicker/bootstrap-datepicker.js'); ?>	
        <?php echo $this->Html->script('/connect/css/datepicker/bootstrap-datepicker-thai.js'); ?>	
        <?php echo $this->Html->css('/connect/css/datepicker/datepicker3.css'); ?>	
        <!-------- Bootstrap Date-Picker Plugin -------->
        <!-------- Bootstrap Input mask Plugin --------->
        <?php echo $this->Html->script('/connect/js/input-mask/jquery.inputmask.min.js'); ?>	
        <?php echo $this->Html->script('/connect/js/input-mask/jquery.inputmask.phone.extensions.js'); ?>	
        <!-------- Bootstrap Input mask Plugin --------->

        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>
        <?php echo $this->fetch('script'); ?>
    </head>
    <body>
        <div class="body">
            <div role="main" class="main">
                <style>
                    .login-container{max-width: 420px; margin: auto;}
                </style>
                <?php echo $this->Flash->render(); ?>
                <div class="container login-container">
                    <?php echo $this->fetch('content') ?>
                </div>
            </div>
        </div>
        <?php echo $this->Html->script('/connect/js/cleave-phone.th'); ?>
        <?php echo $this->Html->script('/connect/js/cleave.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery.appear/jquery.appear.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery.easing/jquery.easing.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery-cookie/jquery-cookie.min'); ?>
        <?php //echo $this->Html->script('/connect/js/master/style-switcher/style.switcher'); ?>
        <?php echo $this->Html->script('/connect/css/bootstrap/js/bootstrap.min'); ?>
        <?php echo $this->Html->script('/connect/css/bootstrap/js/bootstrap-show-password'); ?>
        <?php echo $this->Html->script('/connect/js/common/common.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery.validation/jquery.validation.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery.easy-pie-chart/jquery.easy-pie-chart.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery.gmap/jquery.gmap.min'); ?>
        <?php echo $this->Html->script('/connect/js/jquery.lazyload/jquery.lazyload.min'); ?>
        <?php echo $this->Html->script('/connect/js/isotope/jquery.isotope.min'); ?>
        <?php echo $this->Html->script('/connect/js/owl.carousel/owl.carousel.min'); ?>
        <?php echo $this->Html->script('/connect/js/magnific-popup/jquery.magnific-popup.min'); ?>
        <?php echo $this->Html->script('/connect/js/vide/vide.min'); ?>	
        <!-- Theme Base, Components and Settings -->
        <?php echo $this->Html->script('/connect/js/theme'); ?>
        <!-- Theme Custom -->
        <?php echo $this->Html->script('/connect/js/custom'); ?>
        <!-- Theme Initialization Files -->
        <?php echo $this->Html->script('/connect/js/theme.init'); ?>

    </body>
</html>
