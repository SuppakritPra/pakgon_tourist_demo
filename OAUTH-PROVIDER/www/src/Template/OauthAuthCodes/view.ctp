<?php

 /**
  *
  * The template view for view as of oauthAuthCodes controller the page show for oauthAuthCodes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAuthCode $oauthAuthCode
  * @since  2018-05-10 17:03:28
  * @license Pakgon.Ltd
  */
?>
<div class="oauthAuthCodes view oauthAuthCodes-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Auth Code Management System => ({0} information)', h($oauthAuthCode->code)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Code'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAuthCode->code); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Redirect Uri'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAuthCode->redirect_uri); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Session Id'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($oauthAuthCode->session_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Expires'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($oauthAuthCode->expires); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
