<?php
/**
  * 
  * edit oauth auth codes template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAuthCode $oauthAuthCode
  * @since   2018/05/10 17:03:28
  * @license pakgon.Ltd.
  */
?>
<div class="oauthAuthCodes oauthAuthCodes-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Auth Code Management System => ( Add Oauth Auth Code )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthAuthCode, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('session_id'); ?>
            <?php echo $this->Form->control('redirect_uri'); ?>
            <?php echo $this->Form->control('expires'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Edit'), $this->request->here , ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for edit oauth auth code ?'), 'data-confirm-title' => __('Confirm for edit oauth auth code ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
