<?php
$btitle = isset($btitle) ? $btitle : 'Zicure';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
?>
<?php if (Configure::read('debug') > 0): ?>
    <br/>
    <div class="sql-debug box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => $btitle, 'bcollap' => $bcollap, 'bclose' => $bclose, 'bnoti' => $bnoti)); ?>
        <div class="box-body">
            <?php echo $this->element('sql_dump'); ?>
        </div>
    </div>
<?php endif; ?>