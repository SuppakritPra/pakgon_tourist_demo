<?php
$btitle = isset($btitle) ? $btitle : __('Box Option Header ($btitle)');
$bcollapse = isset($bcollapse) ? $bcollapse : false;
$bclose = isset($bclose) ? $bclose : false;
$blink = isset($blink) ? $blink : [];
$bnoti = isset($bnoti) ? $bnoti : null;
$bicon = isset($bicon) ? $this->Bootstrap->icon($bicon) : null;
?>

<!-- .box-header -->
<div class="box-header with-border">
    <h3 class="box-title"><?php echo $bicon;?><?php echo $btitle; ?></h3>
    <div class="box-tools pull-right">
        <?php if (!is_null($bnoti)): ?>
            <?php echo $this->element('notificationButton', array('btitle' => $bnoti)); ?>
        <?php endif; ?>

        <?php if ($bcollapse): ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        <?php endif; ?>

        <?php if (is_array($blink) && !empty($blink)): ?>
            <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <?php foreach ($blink as $k => $v): ?>
                        <li><a href="<?php echo $v; ?>"><?php echo $k; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
        <?php if ($bclose): ?>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        <?php endif; ?>
    </div>
</div>
<!-- /.box-header -->