<?php
/**
 * 
 * edit users template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @since   2018/04/20 18:08:04
 * @license pakgon.Ltd.
 */
$disabled = isset($disabled) ? $disabled : true;
?>
<div class="users users-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('User Management System => ( User Profile )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <div class="col-md-6">
            <?php echo $this->Form->create($user, ['type' => 'file', 'horizontal' => false, 'url' => ['action' => 'updateProfilePicture', $user->id]]); ?>
            <?php echo $this->Form->control('id'); ?>
            <?php echo $this->Html->image(empty($user->profile_image) ? 'u135.png' : $user->profile_image, ['class' => 'profile-img']); ?>
            <?php
            if (($disabled === false)):
                echo $this->Form->control('profile_image', ['id' => 'profile_image', 'label' => __('Profile Image'), 'type' => 'file', 'class' => 'required', 'accept' => 'image/gif,image/jpeg,image/png', 'value' => null]);
                echo $this->Permission->submit(__('Change Picture'), "/Users/changeImageProfile/{$user->id}", ['class' => 'btn btn-flat btn-primary confirmModal', 'rel' => __('Are you sure for change the profile picture ?'), 'disabled' => true, 'id' => 'btnSubmitChangePicture']);
                echo $this->Permission->buttonBack('normal');
            endif;
            ?>
            <?php echo $this->Form->end(); ?>

            <br/>
            <?php echo $this->Form->create($user, ['url' => ['action' => 'changePassword', $user->id]]); ?>
            <?php echo $this->Form->control('id', ['class' => 'required', 'id' => 'userID']); ?>
            <?php echo $this->Form->control('username', ['class' => 'required', 'disabled' => true]); ?>
            <?php
            if (($disabled === false)):
                echo $this->Form->control('password', ['label' => ['text' => __('New Password')], 'class' => 'required ckpassword', 'placeholder' => __('New Password'), 'value' => '', 'readonly' => @$disabled, 'id' => 'new_password']);
                echo $this->Form->control('confirm_password', ['class' => 'required', 'type' => 'password', 'equalTo' => '#new_password', 'placeholder' => __('Confirm Password'), 'readonly' => @$disabled, 'id' => 'confirm_password']);
                echo $this->Permission->submit(__('Change Password'), '/users/changPassword/' . $user->id, ['class' => 'btn btn-flat btn-warning confirmModal', 'data-confirm-message' => __('Are you sure for chang your password ?'), 'disabled' => true, 'id' => 'btnSubmitChangePassword']);
                echo $this->Permission->buttonBack('normal');
            endif;
            ?>
            <?php echo $this->Form->end(); ?>
            <br/>
        </div>

        <div class="col-md-6">
            <?php echo $this->Form->create($user, ['url' => ['action' => 'updateProfile', $user->id], 'id' => 'updateProfile']); ?>
            <?php echo $this->Form->control('id'); ?>
            <?php echo $this->Form->control('role_id', ['options' => $this->Utility->findRolesList(), 'class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('position_id', ['options' => $this->Utility->findPositionsList(), 'class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('customer_type', ['options' => $this->Utility->findCustomerTypeList(), 'class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('user_type', ['options' => $this->Utility->findUsersTypeList(), 'class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('personal_card_no_type', ['options' => $this->Utility->getPersonalCardType(), 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('personal_card_no', ['class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('name_prefix_id', ['options' => $this->Utility->findNamePrefixesList(), 'class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('gender', ['options' => $this->Utility->getGenderDDL(), 'class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('first_name', ['class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('last_name', ['class' => 'required', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('first_name_eng', ['disabled' => $disabled]); ?>
            <?php echo $this->Form->control('last_name_eng', ['disabled' => $disabled]); ?>
            <?php echo $this->Form->control('company_name', ['disabled' => $disabled]); ?>
            <?php echo $this->Form->control('company_tax_id', ['disabled' => $disabled]); ?>
            <?php echo $this->Form->control('nationality_id', ['class' => 'required', 'options' => $this->Utility->findNationalitiesList(), 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('ethnicity_id', ['class' => 'required', 'options' => $this->Utility->findEthnicitiesList(), 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('birth_date', ['type' => 'text', 'class' => 'datepicker', 'data-date-language' => 'th-th', 'data-date-format' => 'mm-dd-yyyy', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('phone_no', ['disabled' => $disabled]); ?>
            <?php echo $this->Form->control('mobile_phone', ['class' => 'required digits', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('line_account', ['disabled' => $disabled]); ?>
            <?php echo $this->Form->control('email', ['class' => 'email', 'disabled' => $disabled]); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus(), 'disabled' => $disabled]); ?>
            <div class="box-footer pull-right">
                <?php echo $this->Permission->submit(__('Edit'), '/Users/edit', ['icon' => 'fa-pencil ', 'class' => 'btn btn-lat btn-warning separate-button confirmModal', 'data-confirm-message' => __('Are you sure for edit user ?'), 'data-confirm-title' => __('Confirm for edit user ?'), 'disabled' => $disabled]); ?>
                <?php echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- /div.box box-footer -->
        </div>
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
<script type="text/javascript">
    var _dirtyProfileForm = $('FORM#updateProfile').serializeArray();
    $(function () {
        findProvinceByCountryCode();
        $('FORM#updateProfile input,FORM#updateProfile textarea').on('keyup', function () {
            checkFormUpdateProfile();
        });

        $("FORM#updateProfile select,FORM#updateProfile radio,FORM#updateProfile checkbox").on('change', function () {
            checkFormUpdateProfile();
        });

        ckChangeProfilePicture();
        ckChangePassword();

        //Check and enable for button change password
        $("#new_password,#confirm_password").keyup(function () {
            ckChangePassword();
        });

        //Check and enable button for submit change user profile picture
        $("#profile_image").change(function () {
            ckChangeProfilePicture();
        });
    });

    //Check form Update profile has changed derty value
    function checkFormUpdateProfile() {
        return true;
        //$('#btnSubmitChangeProfile').attr('disabled', false);
        //$('#btnSubmitChangeProfile').attr('disabled', (_dirtyProfileForm == $('FORM#edittingOfficerProfile').serializeArray()));
    }
    //Check and enable button for submit change user profile picture
    function ckChangeProfilePicture() {
        $('#btnSubmitChangePicture').attr('disabled', !($('#profile_image').val() != ''));
    }
    //Check and enable for button change password
    function ckChangePassword() {
        $("#btnSubmitChangePassword").attr('disabled', !(($("#new_password").val() != '') && ($("#confirm_password").val() != '')));
    }
</script>