<?php
/**
 * 
 * add oauth scopes template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OauthScope $oauthScope
 * @since   2018/05/10 17:04:14
 * @license pakgon.Ltd.
 */
?>
<div class="oauthScopes oauthScopes-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Scope Management System => ( Add Oauth Scope )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php echo $this->Form->create($oauthScope, ['horizontal' => true]); ?>
        <?php echo $this->Form->control('description'); ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Save'), $this->request->here, ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for add oauth scope ?'), 'data-confirm-title' => __('Confirm for add oauth scope ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
