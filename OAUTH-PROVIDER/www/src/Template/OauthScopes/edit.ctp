<?php
/**
 * 
 * edit oauth scopes template.
 *
 * @author sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OauthScope $oauthScope
 * @since   2018/05/10 17:04:14
 * @license pakgon.Ltd.
 */
?>
<div class="oauthScopes oauthScopes-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Scope Management System => ( Add Oauth Scope )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php echo $this->Form->create($oauthScope, ['horizontal' => true]); ?>
        <?php echo $this->Form->control('description'); ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Edit'), $this->request->here, ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for edit oauth scope ?'), 'data-confirm-title' => __('Confirm for edit oauth scope ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
