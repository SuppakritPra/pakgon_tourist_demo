<?php
/**
 *
 * The template view for view as of oauthScopes controller the page show for oauthScopes information.
 *
 * @author  sarawutt.b
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OauthScope $oauthScope
 * @since  2018-05-10 17:04:14
 * @license Pakgon.Ltd
 */
?>
<div class="oauthScopes view oauthScopes-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Scope Management System => ({0} information)', h($oauthScope->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
            <tr>
                <td class="table-view-label"><?php echo __('Oauth Scopes Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthScope->id); ?></td>
            </tr>
            <tr>
                <td class="table-view-label"><?php echo __('Description'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthScope->description); ?></td>
            </tr>
        </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->