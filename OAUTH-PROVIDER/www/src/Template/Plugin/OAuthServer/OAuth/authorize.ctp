<?php $this->layout = 'AdminLTE204.topself'; ?>
<?php echo $this->Html->css(['/vendor/nprogress/nprogress.min']); ?>
<?php echo $this->Form->create(null, ['id' => 'frmOauth']); ?>
<?php echo $this->Form->control('authorization', ['name' => 'authorization', 'id' => 'btnoAuthApproved', 'value' => 'Approve', 'type' => 'hidden']); ?>
<?php //echo $this->Form->submit(__('Approved'), ['id' => 'btnoAuthApproved', 'name' => 'authorization', 'class' => 'btn btn-flat btn-info']); ?>
<?php //echo $this->Form->submit(__('Deny'), ['id' => 'btnoAuthDeny', 'name' => 'authorization', 'class' => 'btn btn-flat btn-danger']); ?>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->script(['/vendor/nprogress/nprogress.min']); ?>
<script type="text/javascript">
    $(function () {
        NProgress.start();
        setTimeout(function () {
            NProgress.done();
            $('.fade').removeClass('out');
        }, 1000);

        $('form#frmOauth').submit();
    });
</script>