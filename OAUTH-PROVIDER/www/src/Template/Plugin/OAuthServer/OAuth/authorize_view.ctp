<?php $this->layout = 'AdminLTE204.topself'; ?>
<h1><?php echo $authParams['client']->getName(); ?> would like to access:</h1>
<ul>
    <?php foreach ($authParams['scopes'] as $scope): ?>
        <li>
            <?php echo $scope->getId(); ?>: <?php echo $scope->getDescription(); ?>
        </li>
    <?php endforeach; ?>
</ul>
<?php echo $this->Form->create(null, ['id' => 'frmOauth']); ?>
<input type="submit" value="Approve" name="authorization" id="btnoAuthApproved" class="btn btn-flat btn-info">
<input type="submit" value="Deny" name="authorization" id="btnoAuthDeny" class="btn btn-flat btn-danger">
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
    $(function () {
        $('#btnoAuthApproved').click();
    });
</script>