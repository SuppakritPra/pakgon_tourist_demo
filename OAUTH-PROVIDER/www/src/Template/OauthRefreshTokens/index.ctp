<?php
/**
  * The template index for index as of oauthRefreshTokens controller the page show for short oauthRefreshTokens information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthRefreshToken[]|\Cake\Collection\CollectionInterface $oauthRefreshTokens
  * @since  2018-05-10 17:03:51
  * @license Pakgon.Ltd
  */
?>
<div class="oauthRefreshTokens index">
    
    <!-- box-find -->
    <div class="box box-warning box-find">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Refresh Token Management System'), 'bicon' => 'fa-search', 'bclose' => true, 'bcollapse' => true]); ?>
        <?php
        $this->Form->setTemplates(
                [
                    'inputContainer' => '<div class="input col-md-6 {{type}} {{required}}"> {{content}} <span class="help">{{help}}</span></div>'
                ]
        );
        ?>
        <!-- box-body -->
        <div class="box-body">
            <?php echo $this->Form->create('oauthRefreshTokens'); ?>
            <?php echo $this->Form->control('name'); ?>
            <?php echo $this->Form->control('status', ['options' => $this->Utility->getMainStatus()]); ?>
            <?php echo $this->Form->control('dateFrom', ['type' => 'text', 'class' => 'datepicker date-start']); ?>
            <?php echo $this->Form->control('dateTo', ['type' => 'text', 'class' => 'datepicker date-end']); ?>
            <div class="box-footer">
                <?php echo $this->Permission->submit(__('Search Oauth Refresh Token'), '/oauthRefreshTokens/index', ['icon' => 'fa-search', 'name' => 'btnSubmitSearch', 'class' => 'btn-flat bg-orange']); ?>
                <?php echo $this->Permission->button(__('Add Oauth Refresh Token'), '/oauthRefreshTokens/add', ['icon' => 'fa-plus', 'name' => 'btnAddNew', 'class' => 'btn-flat bg-navy']); ?>
                <?php //echo $this->Permission->buttonBack(); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./box box-footer -->
        </div><!-- ./box box-body -->
    </div><!-- ./box box-warning box-find -->

    
        <!-- box-result -->
    <div class="box box-info box-result">
        <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Refresh Tokens list (Result)'), 'bicon' => 'fa-th-list', 'bclose' => true, 'bcollapse' => true]); ?>
        <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped table-list-index">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('Row no'); ?></th>
                                    <th><?php echo $this->Paginator->sort('refresh_token'); ?></th>
                                            <th><?php echo $this->Paginator->sort('oauth_token'); ?></th>
                                            <th><?php echo $this->Paginator->sort('expires'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($oauthRefreshTokens)): ?>
                    <?php foreach ($oauthRefreshTokens as $index => $oauthRefreshToken): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{{start}}') + $index; ?></td>
                                                                                    <td><?php echo h($oauthRefreshToken->refresh_token); ?></td>
                                                                                                                <td><?php echo h($oauthRefreshToken->oauth_token); ?></td>
                                                                                                                <td><?php echo $this->Number->format($oauthRefreshToken->expires); ?></td>
                                                                    <td class="actions">
                            <?php echo $this->Permission->getActions($oauthRefreshToken->refresh_token); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="5"><?php echo __($this->Configure->read('APP.DISPLAY.NO_RESULT')); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
         </div><!-- ./box box-body -->
        <?php echo $this->element('utility/pagination'); ?>
    </div><!-- /box box-info box-result -->
</div>   
