<?php
/**
  * 
  * edit oauth refresh tokens template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthRefreshToken $oauthRefreshToken
  * @since   2018/05/10 17:03:51
  * @license pakgon.Ltd.
  */
?>
<div class="oauthRefreshTokens oauthRefreshTokens-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Refresh Token Management System => ( Add Oauth Refresh Token )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthRefreshToken, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('oauth_token'); ?>
            <?php echo $this->Form->control('expires'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Edit'), $this->request->here , ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for edit oauth refresh token ?'), 'data-confirm-title' => __('Confirm for edit oauth refresh token ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
