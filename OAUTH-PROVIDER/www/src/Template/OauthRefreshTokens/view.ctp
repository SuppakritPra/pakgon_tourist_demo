<?php

 /**
  *
  * The template view for view as of oauthRefreshTokens controller the page show for oauthRefreshTokens information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthRefreshToken $oauthRefreshToken
  * @since  2018-05-10 17:03:51
  * @license Pakgon.Ltd
  */
?>
<div class="oauthRefreshTokens view oauthRefreshTokens-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Refresh Token Management System => ({0} information)', h($oauthRefreshToken->refresh_token)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Refresh Token'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthRefreshToken->refresh_token); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Oauth Token'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthRefreshToken->oauth_token); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Expires'); ?></td>
                <td class="table-view-detail"><?php echo $this->Number->format($oauthRefreshToken->expires); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
