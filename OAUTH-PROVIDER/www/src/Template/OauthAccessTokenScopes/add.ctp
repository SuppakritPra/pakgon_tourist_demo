<?php
/**
  * 
  * add oauth access token scopes template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAccessTokenScope $oauthAccessTokenScope
  * @since   2018/05/10 17:02:16
  * @license pakgon.Ltd.
  */
?>
<div class="oauthAccessTokenScopes oauthAccessTokenScopes-add form box box-primary box-submit box-add">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Access Token Scope Management System => ( Add Oauth Access Token Scope )'), 'bicon' => 'fa-plus', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthAccessTokenScope, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('oauth_token'); ?>
            <?php echo $this->Form->control('scope_id'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Save'), $this->request->here , ['icon' => 'fa-save ', 'class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for add oauth access token scope ?'), 'data-confirm-title' => __('Confirm for add oauth access token scope ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
