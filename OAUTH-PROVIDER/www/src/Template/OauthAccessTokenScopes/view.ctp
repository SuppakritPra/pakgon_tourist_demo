<?php

 /**
  *
  * The template view for view as of oauthAccessTokenScopes controller the page show for oauthAccessTokenScopes information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthAccessTokenScope $oauthAccessTokenScope
  * @since  2018-05-10 17:02:16
  * @license Pakgon.Ltd
  */
?>
<div class="oauthAccessTokenScopes view oauthAccessTokenScopes-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Access Token Scope Management System => ({0} information)', h($oauthAccessTokenScope->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Oauth Token'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAccessTokenScope->oauth_token); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Scope Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAccessTokenScope->scope_id); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Oauth Access Token Scopes Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthAccessTokenScope->id); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
