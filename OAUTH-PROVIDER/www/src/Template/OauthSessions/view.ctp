<?php

 /**
  *
  * The template view for view as of oauthSessions controller the page show for oauthSessions information.
  *
  * @author  sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthSession $oauthSession
  * @since  2018-05-10 17:07:15
  * @license Pakgon.Ltd
  */
?>
<div class="oauthSessions view oauthSessions-view box box-info box-view">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Session Management System => ({0} information)', h($oauthSession->id)), 'bicon' => 'fa-info', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body table-responsive">
        <table class="table table-bordered table-striped table-view-information">
                                <tr>
                <td class="table-view-label"><?php echo __('Owner Model'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSession->owner_model); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Owner Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSession->owner_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Client Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSession->client_id); ?></td>
            </tr>
                                        <tr>
                <td class="table-view-label"><?php echo __('Client Redirect Uri'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSession->client_redirect_uri); ?></td>
            </tr>
                                                        <tr>
                <td class="table-view-label"><?php echo __('Oauth Sessions Id'); ?></td>
                <td class="table-view-detail"><?php echo h($oauthSession->id); ?></td>
            </tr>
                                    </table>
    </div><!-- ./box-body -->
    <?php echo $this->element('utility/boxOptionFooter'); ?>
</div><!-- ./roles view rols-view box box-info box-view -->

</div><!-- ./related div-box-related -->
