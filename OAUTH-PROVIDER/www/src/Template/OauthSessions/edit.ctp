<?php
/**
  * 
  * edit oauth sessions template.
  *
  * @author sarawutt.b
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\OauthSession $oauthSession
  * @since   2018/05/10 17:07:15
  * @license pakgon.Ltd.
  */
?>
<div class="oauthSessions oauthSessions-edit form box box-warning box-submit box-edit">
    <?php echo $this->element('utility/boxOptionHeader', ['btitle' => __('Oauth Session Management System => ( Add Oauth Session )'), 'bicon' => 'fa-pencil', 'bcollapse' => true, 'bclose' => true]); ?>
    <div class="box-body">
        <?php    echo $this->Form->create($oauthSession, ['horizontal' => true]); ?>
            <?php echo $this->Form->control('owner_model'); ?>
            <?php echo $this->Form->control('owner_id'); ?>
            <?php echo $this->Form->control('client_id'); ?>
            <?php echo $this->Form->control('client_redirect_uri'); ?>
        ?>
        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Permission->submit(__('Edit'), $this->request->here , ['icon' => 'fa-pencil ', 'class' => 'btn btn-warning btn-flat pull-right confirmModal', 'data-confirm-message' => __('Are you sure for edit oauth session ?'), 'data-confirm-title' => __('Confirm for edit oauth session ?')]); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box box-footer -->
    </div><!-- /div.box box-body -->
</div><!-- /div.box box-primary -->
