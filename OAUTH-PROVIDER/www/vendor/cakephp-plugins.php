<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'AdminLTE204' => $baseDir . '/plugins/AdminLTE204/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Bootstrap' => $baseDir . '/vendor/holt59/cakephp3-bootstrap-helpers/',
        'BootstrapUI' => $baseDir . '/vendor/friendsofcake/bootstrap-ui/',
        'Crud' => $baseDir . '/vendor/friendsofcake/crud/',
        'CrudView' => $baseDir . '/vendor/friendsofcake/crud-view/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'OAuthServer' => $baseDir . '/vendor/uafrica/oauth-server/',
        'PakgonConnect' => $baseDir . '/plugins/PakgonConnect/',
        'RestApi' => $baseDir . '/vendor/multidots/cakephp-rest-api/'
    ]
];