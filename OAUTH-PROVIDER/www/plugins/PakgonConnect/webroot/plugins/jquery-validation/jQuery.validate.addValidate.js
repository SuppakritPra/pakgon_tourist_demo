/**
 | ------------------------------------------------------------------------------------------------------------------
 | Add for form data validations for another validate in HTML form
 | @author	 Sarawuttb
 | @since   2013/05/20 14:37:26
 | @license Pakgon Ltd ,Company
 | ------------------------------------------------------------------------------------------------------------------
 | 1. letters					,Example 'class'="letters"
 | 2. capital_letters			,Example 'class'="capital_letter"
 | 3. small_letters			,Example 'class'="small_letter"
 | 4. length					,Example 'length'=>13
 | 5. citizen_no				,Example 'class'=>'citizen_no'
 | 6. filesize					,Example 'filesize'=>1024
 | 7. extension				,Example 'extension'=>'txt|text|xls'
 | 8. accept					,Example 'accept'=>'audio/*,image/*'
 | 9. alphanumeric				,Example 'class'=>'alphanumeric'
 | 10. lettersonly				,Example 'class'=>'lettersonly'
 | 11. integer					,Example 'class'=>'integer'
 | 12. time					,Example 'class'=>'time'
 | 13. time12					,Example 'class'=>'time12'
 | 14. ipv4					,Example 'class'=>'ipv4'
 | 15. ipv6					,Example 'class'=>'ipv6'
 | 16. ckpassword           ,Example 'class'=>'ckpassword'
 | 17. chosen                                   ,Example class='chosen'
 | 18. ckquarter-max                            ,Example 'ckquarter-max' = '2'
 | 19. ckquarter-min                            ,Example 'ckquarter-min' = '2'
 | ------------------------------------------------------------------------------------------------------------------
 */
jQuery.validator.addMethod("eng_letters", function (value, element) {
    return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
}, "เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น.");
jQuery.validator.addMethod("thai_letters", function (value, element) {
    return this.optional(element) || /^[ก-๙]+$/ig.test(value);
}, "เฉพาะตัวอักษรภาษาไทยเท่านั้น.");
jQuery.validator.addMethod("letters", function (value, element) {
    return this.optional(element) || /^[a-zA-Zก-๙]+$/i.test(value);
}, "เฉพาะตัวอักษรภาษาไทย และอังกฤษเท่านั้น.");
jQuery.validator.addMethod("username", function (value, element) {
    return this.optional(element) || /^[a-z0-9._]+$/.test(value);
}, "ภาษาอังกฤษตัวเล็ก จุด ขีดล่างและตัวเลขเท่านั้น.");
jQuery.validator.addMethod("capital_letters", function (value, element) {
    return this.optional(element) || /^([A-Z])+$/.test(value);
}, "เฉพาะตัวอักษรตัวใหญ่เท่านั้น.");
jQuery.validator.addMethod("small_letters", function (value, element) {
    return this.optional(element) || /^([a-z])+$/.test(value);
}, "เฉพาะตัวอักษรตัวเล็กเท่านั้น.");
jQuery.validator.addMethod("length", function (value, element, param) {
    return this.optional(element) || (value.length == param);
}, jQuery.validator.format("ความยาวของข้อมูลต้องเท่ากับ {0}"));
jQuery.validator.addMethod("citizen_no", function (value, element) {
    var tmp = $.trim(value);
    if (tmp == '')
        return true;
    if (value.length !== 13) {
        return false;
    }
    var IDCard = value,
            ValOfPosition = 13,
            ValOfPosition13 = null,
            ValSum = null,
            digit13Check = null;
    if (IDCard.length !== 13 || isNaN(IDCard.charAt(IDCard.length - 1))) {
        return false;
    }
    ValOfPosition13 = Number(IDCard.charAt(IDCard.length - 1));
    for (var i = 0; i <= IDCard.length - 2; i++) {
        if (!isNaN(IDCard.charAt(i))) {
            ValSum += Number(IDCard.charAt(i)) * ValOfPosition;
        }
        ValOfPosition--;
    }
    digit13Check = Number(11 - (ValSum % 11));
    var digit13 = String(digit13Check);
    digit13 = digit13.length == 1 ? Number(digit13) : Number(digit13.charAt(1));
    return (ValOfPosition13 == digit13);
}, jQuery.validator.format('คุณกรอกหมายเลขบัตรประชาชนไม่ถูกต้อง'));
jQuery.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || ((element.files[0].size) <= param)
}, jQuery.validator.format("ขนาดไฟล์ที่ต้องน้อยกว่า หรือเท่ากับ {0} Byte"));
jQuery.validator.addMethod("extension", function (value, element, param) {
    param = typeof (param === "string") ? param.replace(/,/g, '|') : "png|jpe?g|gif";
    return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
}, jQuery.format("นามสกุลของไฟล์แนบต้องเป็น {0} เท่านั้น."));
jQuery.validator.addMethod("accept", function (value, element, param) {
    var typeParam = typeof (param === "string") ? param.replace(/\s/g, '').replace(/,/g, '|') : "image/*",
            optionalValue = this.optional(element),
            i,
            file;
    if (optionalValue) {
        return optionalValue;
    }
    if ($(element).attr("type") === "file") {
        typeParam = typeParam.replace(/\*/g, ".*");
        if (element.files && element.files.length) {
            for (i = 0; i < element.files.length; i++) {
                file = element.files[i];
                if (!file.type.match(new RegExp(".?(" + typeParam + ")$", "i"))) {
                    return false;
                }
            }
        }
    }
    return true;
}, jQuery.format("ประเภทของไฟล์แนบต้องเป็น {0} เท่านั้น"));
jQuery.validator.addMethod("alphanumeric", function (value, element) {
    return this.optional(element) || /^\w+$/i.test(value);
}, "ตัวอักษร หรือตัวเลขเท่านั้น");
jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value);
}, "ตัวอักษรเท่านั้น");
jQuery.validator.addMethod("integer", function (value, element) {
    return this.optional(element) || /^-?\d+$/.test(value);
}, "จำนวนเต็มบวก หรือจำนวนเต็มลบเท่านั้น");
jQuery.validator.addMethod("time", function (value, element) {
    return this.optional(element) || /^([01]\d|2[0-3])(:[0-5]\d){1,2}$/.test(value);
}, "เวลาระหว่าง  00:00 and 23:59 เท่านั้น");
jQuery.validator.addMethod("time12h", function (value, element) {
    return this.optional(element) || /^((0?[1-9]|1[012])(:[0-5]\d){1,2}(\ ?[AP]M))$/i.test(value);
}, "เวลาในรูปแบบ 12 ชั่วโมง AM/PM เท่านั้น");
jQuery.validator.addMethod("ipv4", function (value, element, param) {
    return this.optional(element) || /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test(value);
}, " IP v4 เท่านั้น");
jQuery.validator.addMethod("ipv6", function (value, element, param) {
    return this.optional(element) || /^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/i.test(value);
}, " IP v6 เท่านั้น");
jQuery.validator.addMethod("chosen", function (value, element) {
    return (value == null ? false : (value.length == 0 ? false : true))
}, "please select an option");
jQuery.validator.addMethod("dollarsscents", function (value, element) {
    return this.optional(element) || /^-?\d+(\.\d{0,2})?$/i.test(value);
}, "ทศนิยม 2 หลักเท่านั้น");
$.validator.addMethod("ckpassword", function (value, element) {
    return (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(value));
}, "ความยาวอย่างน้อย 6 ตัว ตัวอักษรเล็ก ใหญ่และตัวเลข");

/**
 * 
 * Chosen select validation for required
 * @author sarawutt.b
 */
$.validator.addMethod("chosen", function (value, element) {
    return (value == null ? false : (value.length == 0 ? false : true))
}, "โปรดเลือกรายการ");

/**
 * ตรวจสอบเมื่อมีการเลือกเปลี่ยนไตรมาส ที่สิ้นสุด
 * @author sarawutt.b
 */
jQuery.validator.addMethod("ckquarter-max", function (value, element, param) {
    return this.optional(element) || (parseInt(value) >= parseInt(param));
}, "ต้องมากกว่าหรือเท่ากับเดือน {0}");

/**
 * ตรวจสอบเมื่อมีการเลือกเปลี่ยนไตรมาส ที่สิ้นสุด
 * @author  sarawutt.b
 */
jQuery.validator.addMethod("ckquarter-min", function (value, element, param) {
    return this.optional(element) || (parseInt(value) <= parseInt(param));
}, "ต้องน้อยกว่าหรือเท่ากับเดือน {0}");