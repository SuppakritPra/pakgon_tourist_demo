<?php

use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo Configure::read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>

        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->css('AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min'); ?>
        <!-- Font Awesome -->
        <?php echo $this->Html->css('AdminLTE204./plugins/font-awesome/css/font-awesome.min'); ?>

        <!-- Ionicons -->
        <?php echo $this->Html->css('AdminLTE204./plugins/Ionicons/css/ionicons.min.css'); ?>

        <!-- Theme style -->
        <?php echo $this->Html->css('AdminLTE204.AdminLTE.min'); ?>

        <!-- iCheck -->
        <?php echo $this->Html->css('AdminLTE204./plugins/iCheck/square/blue'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'); ?>
        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo $this->Url->build(array('controller' => 'pages', 'action' => 'display', 'home')); ?>"><?php echo Configure::read('Theme.logo.large'); ?></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><?php echo __('Sign in to start your session'); ?></p>

                <p> <?php echo $this->Flash->render(); ?> </p>
                <p> <?php echo $this->Flash->render('auth'); ?> </p>
                <?php echo $this->fetch('content'); ?>

                <?php if (Configure::read('Theme.login.show_social')): ?>
                    <div class="social-auth-links text-center">
                        <p>- <?php echo __('OR'); ?> -</p>
                        <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> <?php echo __('Sign in using Facebook'); ?></a>
                        <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> <?php echo __('Sign in using Google+'); ?></a>
                    </div>
                    <!-- /.social-auth-links -->
                <?php endif; ?>

                <?php if (Configure::read('Theme.login.show_remember')): ?>
                    <a href="#">I forgot my password</a><br>
                <?php endif; ?>

                <?php if (Configure::read('Theme.login.show_register')): ?>
                    <a href="register.html" class="text-center">Register a new membership</a>
                <?php endif; ?>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
        <?php echo $this->Html->script('AdminLTE204./plugins/jquery/dist/jquery.min'); ?>
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->script('AdminLTE204./plugins/bootstrap/dist/js/bootstrap.min'); ?>
        <!-- iCheck -->
        <?php echo $this->Html->script('AdminLTE204./plugins/iCheck/icheck.min'); ?>
        <script type="text/javascript">
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' /* optional */
                });
            });
        </script>
    </body>
</html>
