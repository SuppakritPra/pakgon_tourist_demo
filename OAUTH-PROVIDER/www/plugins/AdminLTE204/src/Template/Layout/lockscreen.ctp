<?php

use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php echo Configure::read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Bootstrap 3.3.7 -->
        <?php echo $this->Html->css('AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min'); ?>
        <!-- Font Awesome -->
        <?php echo $this->Html->css('AdminLTE204./plugins/font-awesome/css/font-awesome.min'); ?>
        <!-- Ionicons -->
        <?php echo $this->Html->css('AdminLTE204./plugins/Ionicons/css/ionicons.min'); ?>
        <!-- Theme style -->
        <?php echo $this->Html->css('AdminLTE204.AdminLTE.min'); ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'); ?>
        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>
    </head>
    <body class="hold-transition lockscreen">

        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>

        <!-- jQuery 3 -->
        <?php echo $this->Html->script('AdminLTE204./plugins/jquery/dist/jquery.min'); ?>
        <!-- Bootstrap 3.3.7 -->
        <?php echo $this->Html->script('AdminLTE204./plugins/bootstrap/dist/js/bootstrap.min'); ?>
    </body>
</html>
