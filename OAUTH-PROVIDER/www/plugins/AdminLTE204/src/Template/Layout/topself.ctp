<?php

use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo Configure::read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->css('AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min'); ?>
        <!-- Font Awesome -->
        <?php echo $this->Html->css('AdminLTE204./plugins/font-awesome/css/font-awesome.min'); ?>

        <!-- Ionicons -->
        <?php echo $this->Html->css('AdminLTE204./plugins/Ionicons/css/ionicons.min.css'); ?>

        <!-- Theme style -->
        <?php echo $this->Html->css('AdminLTE204.AdminLTE.min'); ?>

        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <?php //echo $this->Html->css('skins/skin-' . Configure::read('Theme.skin') . '.min');  ?>
        <?php echo $this->Html->css('AdminLTE204.skins/_all-skins.min.css'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'); ?>
        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>

        <!-- jQuery 2.2.3 -->
        <?php echo $this->Html->script('/plugins/jquery/dist/jquery.min'); ?>
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->script('/plugins/bootstrap/dist/js/bootstrap.min'); ?>
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-blue layout-top-nav skin-red">
        <div class="wrapper">
            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <?php echo $this->Flash->render(); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
                <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->


        <!-- SlimScroll -->
        <?php echo $this->Html->script('/plugins/jquery-slimscroll/jquery.slimscroll.min'); ?>
        <!-- FastClick -->
        <?php echo $this->Html->script('/plugins/fastclick/lib/fastclick'); ?>
        <!-- AdminLTE App -->
        <?php echo $this->Html->script('adminlte.min'); ?>
        <?php echo $this->Html->script('demo'); ?>
        <!-- AdminLTE for demo purposes -->
        <?php echo $this->fetch('script'); ?>
    </body>
</html>
