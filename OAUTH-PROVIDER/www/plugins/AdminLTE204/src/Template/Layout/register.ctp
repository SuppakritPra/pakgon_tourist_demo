<?php

use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo Configure::read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->css('AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min'); ?>
        <!-- Font Awesome -->
        <?php echo $this->Html->css('AdminLTE204./plugins/font-awesome/css/font-awesome.min'); ?>

        <!-- Ionicons -->
        <?php echo $this->Html->css('AdminLTE204./plugins/Ionicons/css/ionicons.min.css'); ?>

        <!-- Theme style -->
        <?php echo $this->Html->css('AdminLTE204.AdminLTE.min'); ?>

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <?php echo $this->Html->css('AdminLTE204./plugins/iCheck/square/blue'); ?>

        <?php echo $this->fetch('css'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'); ?>
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-logo">
                <a href="<?php echo $this->Url->build('/'); ?>"><?php echo Configure::read('Theme.logo.large'); ?></a>
            </div>

            <div class="register-box-body">
                <p class="login-box-msg"><?php echo __('Register a new membership'); ?></p>

                <?php echo $this->fetch('content'); ?>

                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> <?php echo __('Sign up using Facebook'); ?></a>
                    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> <?php echo __('Sign up using Google+'); ?></a>
                </div>

                <a href="login.html" class="text-center">I already have a membership</a>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->

        <!-- jQuery 2.2.3 -->
        <?php echo $this->Html->script('AdminLTE204./plugins/jquery/dist/jquery.min'); ?>
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->script('AdminLTE204./plugins/bootstrap/dist/js/bootstrap.min'); ?>
        <!-- iCheck -->
        <?php echo $this->Html->script('AdminLTE204./plugins/iCheck/icheck.min'); ?>

        <script type="text/javascript">
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' /* optional */
                });
            });
        </script>

        <!-- AdminLTE for demo purposes -->
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>

    </body>
</html>
