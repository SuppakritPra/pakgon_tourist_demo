<?php

use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>
            <?php echo Configure::read('Theme.title'); ?> :
            <?php echo $this->fetch('title'); ?>
        </title>
        <?php echo $this->Html->meta('icon'); ?>

        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->css('AdminLTE204./plugins/bootstrap/dist/css/bootstrap.min'); ?>
        <!-- Font Awesome -->
        <?php echo $this->Html->css('AdminLTE204./plugins/font-awesome/css/font-awesome.min'); ?>

        <!-- Ionicons -->
        <?php echo $this->Html->css('AdminLTE204./plugins/Ionicons/css/ionicons.min.css'); ?>

        <!-- Theme style -->
        <?php echo $this->Html->css('AdminLTE204.AdminLTE.min'); ?>

        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <?php //echo $this->Html->css('skins/skin-' . Configure::read('Theme.skin') . '.min'); ?>
        <?php echo $this->Html->css('AdminLTE204.skins/_all-skins.min.css'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'); ?>
        <?php echo $this->fetch('meta'); ?>
        <?php echo $this->fetch('css'); ?>
    </head>
    <!-- ADD THE CLASS layout-boxed TO GET A BOXED LAYOUT -->
    <body class="hold-transition skin-blue layout-boxed sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <!-- Header Navbar: style can be found in header.less -->
            <?php echo $this->element('theme/top-nav'); ?>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <?php echo $this->element('theme/aside-main-sidebar'); ?>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $this->Flash->render(); ?>
                <?php echo $this->fetch('content'); ?>
            </div>
            <!-- /.content-wrapper -->

            <?php echo $this->element('theme/footer'); ?>

            <!-- Control Sidebar -->
            <?php echo $this->fetch('theme/aside-control-sidebar'); ?>
            <!-- /.control-sidebar -->

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <?php echo $this->Html->script('/plugins/jquery/dist/jquery.min'); ?>
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->script('/plugins/bootstrap/dist/js/bootstrap.min'); ?>
        <!-- SlimScroll -->
        <?php echo $this->Html->script('/plugins/jquery-slimscroll/jquery.slimscroll.min'); ?>
        <!-- FastClick -->
        <?php echo $this->Html->script('/plugins/fastclick/lib/fastclick'); ?>
        <!-- AdminLTE App -->
        <?php echo $this->Html->script('adminlte.min'); ?>
        <?php echo $this->Html->script('demo'); ?>
        <!-- AdminLTE for demo purposes -->
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".navbar .menu").slimscroll({
                    height: "200px",
                    alwaysVisible: false,
                    size: "3px"
                }).css("width", "100%");

                var a = $('a[href="<?php echo $this->request->webroot . $this->request->url ?>"]');
                if (!a.parent().hasClass('treeview') && !a.parent().parent().hasClass('pagination')) {
                    a.parent().addClass('active').parents('.treeview').addClass('active');
                }
            });
        </script>
    </body>
</html>
