<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> <?php echo $this->Configure->read('APP.VERSION'); ?>
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io"><?php echo __($this->Configure->read('APP.NAME'));?></a>.</strong> All rights
    reserved.
</footer>