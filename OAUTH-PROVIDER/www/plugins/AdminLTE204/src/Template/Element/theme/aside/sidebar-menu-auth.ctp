<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li>
        <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'index']); ?>">
            <i class="fa fa-user"></i> <span><?php echo __('Users management'); ?></span>
            <span class="pull-right-container">
                <small class="label pull-right bg-green"><?php echo __('users');?></small>
            </span>
        </a>
    </li>


    <li>
        <a href="<?php echo $this->Url->build(['controller' => 'OauthScopes', 'action' => 'index']); ?>">
            <i class="fa fa-tasks"></i> <span><?php echo __('Scope management'); ?></span>
            <span class="pull-right-container">
                <small class="label pull-right bg-blue"><?php echo __('scopes');?></small>
            </span>
        </a>
    </li>


    <li>
        <a href="<?php echo $this->Url->build(['controller' => 'OauthClients', 'action' => 'index']); ?>">
            <i class="fa fa-sitemap"></i> <span><?php echo __('Client Management'); ?></span>
            <span class="pull-right-container">
                <small class="label pull-right bg-red"><?php echo __('clients');?></small>
            </span>
        </a>
    </li>
</ul>